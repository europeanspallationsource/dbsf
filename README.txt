From: Todd Satogata <satogata@jlab.org>
Date: 25 Oct 2010

Here is a copy of dbsf that compiles and appears to work under the Scientific
Linux CODAC distribution. Here are a few helpful hints:

  (*) I'd suggest buying a copy of VMware Fusion or getting a free test
      license. I've got a free test license at the moment and Klemen can
      direct you at a tarred disk image if you don't have it already;
      the ess-dev user has password core09, and has sudo rights. Change
      the password after you log in, of course.

  (*) The first time you use VMware Fusion, you'll want to click the menu
      item Virtual Machine / Install VMware Tools. After you do this, you'll
      be able to turn shared folders on (the folder icon in the lower right
      after you boot the virtual machine and log in). I mounted my Mac
      desktop as a shared folder, and it appears under /mnt/hgfs/Desktop.
      That's probably one of the easier ways to get the dbsf.tgz image onto
      your Scientific Linux disk.

  (*) You'll need certain packages that aren't installed with the standard
      distribution, in particular yacc and flex. So type

        sudo yum install byacc.x86_64 flex

      I also installed emacs 'cuz I'm an emacs junkie. You can install other
      things if you like but I think these are the ones that are needed for
      dbsf build since Klemen graciously included the 64-bit mysql libraries
      (mysql-devel.x86_64) in his distribution.

  (*) Copy the dbsf directory to ~ and untar it:

        tar -xzvf dbsf.tgz

  (*) If you need to edit the environment, just edit the top-level ENV file:

        vi dbsf/ENV

  (*) Build it

        cd dbsf
        ./build

      I added a cp to the build script so it copies the executable for dbsf
      to the current directory if it builds properly. There are a lot of
      warnings, but that dbsf builds and runs. The server and login are in
      lambda/dbsf/dbsf/dbquery.c if you need to change them. Look for the
      routine open_db().

  (*) To delete all the X86 directories, type

        ./clean

This should be enough to get y'all started. I'd love to get this code into the
more standard development environment using more standard build tools than
the old Makefile/X86 stuff, but this is enough to get things going.

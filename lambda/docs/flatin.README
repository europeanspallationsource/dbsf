
        The FLATIN Functions

        J. A. Holt, Fermilab


The program DBSF can extract the database lattice information  in
two  types  of  flat formats.  One is a flat ascii format and the
other is a flat sds format.  The   "flatin"  function  opens  and
reads  the  flat  format file produced by dbsf, dynamically allo-
cates memory, fills the lattice structures and returns a  pointer
table   so  that  the  application  can  access  the  data.   The
"flatin_sds" function does the same thing except the  input  file
is a sds file.  Both functions have the same passing parameters:

    status = flatin( char * file_name, flatin_data *my_data);

where the "file_name" is the name of the flat file and  "my_data"
is a pointer to the lattice structures pointer table.

There are five structures which are used to describe  a  lattice.
They  are  parameter, element, lattice, legal_type, and row.  The
parameter structure as its name implies, holds the name and value
of all of the parameter extracted for a particular lattice.  Also
if an element has more than a length, strength and tilt, the  ad-
ditional values are stored in the parameter structure.

The element structure is the basic unit  of  the  lattice.   Each
element  has  a  name,  a  type name as defined in the legal_type
structure, and possibly a length, strength and tilt.   If  length
strength  or tilt is defined from a parameter then both the value
and index into the parameter structure variables are  filled  in.
If  there  are  additional parameters beyond length, strength and
tilt, then npars is nonzero and the more_index points to the  lo-
cation in the parameter structure where the additional parameters
are located.  These parameters are placed consecutively.

An array of lattice structues constitutes the  lattice.   In  the
structure  is an index to the pertinant element  structure and an
index to the element type.  The other variables are not  used  by
flatin.

The legal_type structure is  a  pre-initialized  structure  which
lists  all of the available element types and the characteristics
associated with that element.

The row structure is also a pre-initialized structure which  con-
tains  a list of the additional parameters for the elements which
have more than length, strength and tilt.


struct parameter {
  char name[NAME_MAX];
  double value; };

struct element {
  char  name[NAME_MAX];
  char  type[NAME_MAX];
  char  sub_type[NAME_MAX];
  double  length;
  double  strength;
  double  tilt;
  int    type_index;
  int    npars;
  int    length_index;
  int    strength_index;
  int    tilt_index;
  int    more_index;
  };

struct lattice {
  double s;
  int  element_index;
  int  type_index;
  int occurence;              /* not filled by flatin */
  int level;                  /* not filled by flatin */
  int hook_index;             /* not filled by flatin */
  int sense;                  /* not filled by flatin */
  };

struct legal_type {
  char    name[NAME_MAX];
  int     has_length;
  int     has_strength;
  int     has_tilt;
  int     has_more;
  };

struct row{
  char    name[NAME_MAX];
  int     cols;
  char    heading[COLUMN_MAX][NAME_MAX];
  };


The structures 'legal_type'  and  'row'  are  filled  with  fixed
values:


struct legal_type legal_type[] = {
  "",             0,0,0,0, /* NULL element */
  "drift",        1,0,0,0, /* length */
  "hmonitor",     1,0,0,0, /* length */
  "vmonitor",     1,0,0,0, /* length */
  "monitor",      1,0,0,0, /* length */
  "instrument",   1,0,0,0, /* length */
  "wiggler",      1,0,1,0, /* length, tilt */
  "rbend",        1,1,1,1, /* length, angle, tilt, more */
  "sbend",        1,1,1,1, /* length, angle, tilt, more */
  "quadrupole",   1,1,1,0, /* length, k1, tilt */
  "sextupole",    1,1,1,0, /* length, k2, tilt */
  "octupole",     1,1,1,0, /* length, k3, tilt */
  "multipole",    0,0,0,1, /* more */
  "solenoid",     1,1,0,0, /* length, ks */
  "rfcavity",     1,0,0,1, /* length, more */
  "elseparator",  1,1,1,0, /* length, e, tilt */
  "srot",         0,1,0,0, /* angle */
  "yrot",         0,1,0,0, /* angle */
  "hkick",        1,1,1,0, /* length, hkick, tilt */
  "vkick",        1,1,1,0, /* length, vkick, tilt */
  "kicker",       1,0,1,1, /* length, tilt, more */
  "marker",       0,0,0,0,
  "ecollimator",  1,0,0,1, /* length, more */
  "rcollimator",  1,0,0,1, /* length, more */
  "tbend",        1,1,1,1, /* length, angle, tilt, more */
  "thinlens",     0,0,0,1, /* more  */
  "tank",         1,0,0,1, /* length, more */
  "edge",         0,0,0,1, /* more  */
  "pmq",          1,0,0,1, /* length, more */
  "rfqcell",      1,0,0,1, /* length, more */
  "doublet",      1,0,0,1, /* length, more */
  "triplet",      0,0,0,1, /* more  */
  "rfgap",        0,0,0,1, /* more  */
  "special",      1,0,0,0, /* length */
  "rotation",     0,1,0,0, /* angle */
  "beamline",     1,0,0,0, /* length */
  "slot",         1,0,0,0, /* length */
  "end_of_list",  0,0,0,0
  };

struct row  row[] = {

"",0,"","", "", "", "", "", "", "", "", "", "", "", "",  "",  "",
"", "", "", "", "", "", "",

"ecollimator", 2,  "xsize", "ysize", "", "", "", "", "", "",  "",
"", "", "", "", "", "", "", "", "", "", "", "", "",

"kicker", 2,    "hkick", "vkick", "", "", "", "", "", "", "", "",
"", "", "", "", "", "", "", "", "", "", "", "",

"multipole", 20,   "k0l",  "k1l",  "k2l",  "k3l",  "k4l",  "k5l",
"k6l",  "k7l",  "k8l", "k9l", "t0", "t1", "t2", "t3", "t4", "t5",
"t6", "t7", "t8", "t9", "", "",

"rbend", 9,    "k1", "k2",  "k3",  "e1",  "e2",  "fint",  "hgap",
"h1", "h2", "", "", "", "", "", "", "", "", "", "", "", "", "",

"rcollimator", 2,  "xsize", "ysize", "", "", "", "", "", "",  "",
"", "", "", "", "", "", "", "", "", "", "", "", "",

"rfcavity",  7,     "volt",  "lag",  "harmon",   "betrf",   "pg",
"shunt", "tfill", "", "", "", "", "", "", "", "", "", "", "",
      "", "",  "", "",

"sbend", 9,    "k1", "k2",  "k3",  "e1",  "e2",  "fint",  "hgap",
"h1", "h2", "", "", "", "", "", "", "", "", "", "", "", "",
      "",

"tbend", 22,    "b0", "a0", "b1", "a1", "b2", "a2",  "b3",  "a3",
"b4", "a4", "b5", "a5", "b6", "a6", "b7", "a7", "b8", "a8", "b9",
"a9", "b10", "a10",

"thinlens", 3,    "xfocal", "yfocal", "zfocal", "", "",  "",  "",
"", "", "", "", "", "", "", "", "", "", "", "", "", "",
      "",

"doublet", 1,    "distance_between", "", "", "", "", "", "",  "",
"", "", "", "", "", "", "", "", "", "", "", "", "",
      "",

"triplet",  5,      "strength_outer_quad",   "length_outer_quad",
"distance_between","strength_inner_quad",    "length_inner_quad",
"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
      "",

"edge", 5,    "rotation_angle", "radius", "gap", "fringe_field1",
"fringe_field2","",  "",  "", "", "", "", "", "", "", "", "", "",
"", "", "", "",
      "",

"rfqcell", 4,    "V/(r**2)", "AV", "phase", "type", "",  "",  "",
"", "", "", "", "", "", "", "", "", "", "", "", "", "",
      "",

"rfgap",      5,          "effective_gap_voltage",       "phase",
"emittance_growth_flag",  "energy_gain_flag", "harmonic", "", "",
"", "", "", "", "", "", "", "", "", "", "", "", "", "",
      "",

"pmq", 2,    "inner_radius", "outer_radius", "", "", "", "",  "",
"", "", "", "", "", "", "", "", "", "", "", "", "", "",
      "",

"tank",   3,      "effective_accel_gradient",   "inj/exit_phase",
"number_of_iden_cavities",
 "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",  "",
"", "",
      ""

};



The pointer table has the following format:


#ifndef FLATIN_H

#define FLATIN_H

#include "lattice_struct.h"

typedef struct flatin_data_pointers {
  struct parameter *parameter_ptr;
  int number_of_parameters;
  struct element *element_ptr;
  int number_of_elements;
  struct lattice *lattice_ptr;
  int number_of_lattices;
  struct more_struct *more_ptr;
  int number_of_mores; } flatin_data;

#endif /* FLATIN_H */


A sample program using the flatin function call is:

/*   This demo demonstrates and tests flatin.c functions */


#include "flatin.h"

main(argc, argv)
     int argc; char **argv; {
  flatin_data my_flatin_data;
  char *file_name;
  char latfile[NAME_MAX];

  if ( argc != 2) {
    printf("Usage: flatin_demo  flat_file_name0);
    printf("flat_file_name is a beam line in 'flat' format0);
    exit(1);
  }
  else {
    file_name = *(argv+1);

    strcpy(latfile,file_name);
  }

  if (flatin(latfile,&my_flatin_data)) {
    printf("found4-5d parameters,4-5d elements,",
         my_flatin_data.number_of_parameters,
           my_flatin_data.number_of_elements);
    printf("5-5d elements in the beamline0,
                my_flatin_data.number_of_lattices);

  }

  exit(0);

}

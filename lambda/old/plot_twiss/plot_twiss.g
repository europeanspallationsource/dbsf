# $Header: /rap/lambda/plot_twiss/RCS/plot_twiss.g,v 1.7 1994/10/04 21:23:47 mackay Exp $
# This is a glish script for viewing the Twiss data from the Holy_Lattice
# files with "plot_twiss".

# Since we're going to use ahared memory see if ~/shardat is there.

if(dir_exists("$HOME/shardat"))
{

} else {
  print "Please make a directory called $HOME/shardat."
  exit
}   # If the dummy files don't exist we can create them there later.


# Find out what they want to look at.

if(len(argv) == 0)
  {
    print "Select on of the following by number:"
    print "1) a2yellow  for the AGS to yellow ring transfer line"
    print "2) a2blue    for the AGS to blue ring transfer line"
    print "3) yellow    for the yellow ring"
    print "4) blue      for the blue ring"
    print "Enter:"
    quack:= read()

    if( quack == "1" )
      {
	latticename   := "YTransfer"
	periodic := 0
      }  else if( quack == "2" )
      {
	latticename   := "BTransfer"
	periodic := 0
      } else if( quack == "3" )
      {
	latticename   := "Yellow"
	periodic := 1
      } else if( quack == "4" )
      {
	latticename   := "Blue"
	periodic := 1
      } else
      {
	print "oops!"
	exit
      }
  } else
  {
    print "I don't know how to handle arguments yet."
    exit
  }

twissname:=spaste("Twiss.",latticename)
holylat := spaste(environ.HOLY_LATTICE,"/",latticename,"/")



# Make sure everything is in shared memory

if(!file_exists(spaste("$HOME/shardat/",latticename)))
  {
     quack:=shell(spaste('touch $HOME/shardat/',latticename))
  }
if(!file_exists(spaste("$HOME/shardat/",twissname)))
  {
     quack:=shell(spaste('touch $HOME/shardat/',twissname))
  }

quack:= shell("echo ",latticename," | whatshm")
if(!len(quack))
  {
    quack:= shell("cd ",holylat,"; f2sm ",latticename)
  }
quack:= shell("echo ",twissname," | whatshm")
if(!len(quack))
  {
    quack:= shell("cd ",holylat,"; f2sm ",twissname)
  }

# Get the dataset name from the file.
beamlinename  := shell(paste("smd -s -n",latticename))


# OK.  Everything should be there.  Let's boogaloo!

pt:=client("plot_twiss -glish -geometry 800x360+1+1 -s ",twissname)
twiss:=shell(paste('twiss -s -i',latticename),async=T)

whenever twiss->established do
{
#  print "twiss:",$value

  if(periodic == 0)
    {
#     print "OOGABOOGA  not a periodic lattice"
      twiss->stdin(paste("read",spaste(holylat,"g.twiss.cmd\n")))
    }
}

sid_l:=client("sid -w -s -geometry +1+1",latticename)
sid_t:=client("sid -s -geometry +455+1",twissname)
element := 0  #since we start out looking at element 0

whenever sid_l->done do
{
  twiss->stdin("quit\n")
  pt->terminate()
  sid_t->terminate()
}
whenever sid_t->done do
{
  twiss->stdin("quit\n")
  pt->terminate()
  sid_l->terminate()
} 
whenever pt->done do
{
  sid_l->terminate()
  sid_t->terminate()
  twiss->stdin("quit\n")
}
whenever twiss->* do
{
#  print "twiss->*:",$agent,$name,$value
}
whenever sid_l->* do
{
#  print "sid_l",$agent,":",$name,"=",$value
}

whenever sid_l->MoveTo do
{
  words:=split($value)
  if(words[2]=="element")
    {
      element := words[3]
    }
}

whenever sid_l->Modify do
{
  words:=split($value)
  twiss->stdin(paste("update ",element,"\n"))
  await twiss->stdout
  if(periodic==1)
    {
      twiss->stdin("periodic\n")
      await twiss->stdout
    }
  twiss->stdin("twiss\n")
  await twiss->stdout

  pt->Refresh()
  sid_t->Redisplay()
}

whenever pt->MoveTo do
{
  words:=split($value,' ')
  if (words[1] == latticename)
    {
      if(words[2]=="lattice")
        {
          quack:=paste(beamlinename,words[2],words[3])
	  sid_l->MoveTo(quack)
        } else {
	  # ignore  the element
	}
    } else {
      sid_t->MoveTo($value)
    }
}


whenever sid_l->MoveTo do
{
# Ignore it
}


##############################

#=============================
func dir_exists(file)
{
  return as_integer(shell("(test -d",file, " && echo 1) || echo 0"))
}
#=============================
func file_exists(file)
{
  return as_integer(shell("(test -f",file, " && echo 1) || echo 0"))
}
#=============================
func read()
{
  return shell("read blatz;echo $blatz")
}

/* $Header: /rap/lambda/plot_twiss/RCS/plot_ellipse.c,v 1.7 1994/10/05 15:21:22 mackay Exp mackay $ */
/* plot_ellipse.c,v 0.0, 27 September, 1993 mackay */
/* Uses XWaldo to display on an X-terminal. */
/* This plots projections of the "Twiss" file hyperfootball onto the */
/* xx'-, yy'-, and  xy-planes (for a start). */

#include  <stdio.h>
#include  <math.h>
#include  <stdlib.h>
#include  <string.h>

#ifndef _AMIGA
#include  <unistd.h>
#else
#include <time.h>
#include <fcntl.h>
#endif

#include  <ctype.h>
#include  "XWaldo.h"
#include  "lattice_defines.h"

#ifndef _AMIGA
#include  <Sds/sdsgen.h>
#include  <flatin.h>
flatin_data lat_dat;
#endif

#include  "twiss_dict.h"
#include  "twiss.h"

optic *optifun = 0;
element *latelt = 0;
int got_lattice = 0;  /* flag for having got lattice via getLattice() */

#ifndef _AMIGA
twiss_ifile_name *twin;
sds_handle sds_twiss;
#endif

int ielt = 0;

int    numtwiss;
int    numelements;

char  latticefile[64] = "nephthys.a2blue";
char  twissfile[30]   = "Twiss";

#define NPT_ELLIPSE 25   /* Number of line segments to draw ellipse. */

int left   = 0;  /* window limits */
int right  = 250;
int top    = 0;
int bottom = 300;

int nstart, nend;

double zmax,zpmax;

#define LMARG     50
#define RMARG      5
#define TMARG      5
#define BMARG     15
#define LATHEIGHT 40
#define SMARG      2

/* Definitions for positioning graphics on PostScript page (72pts/in) */
int pslmarg      = 108; /* left edge of plots */
int psbottom     = 100; /* bottom of eta plot */
int pswidth      = 432; /* width of plots */
int pslatheight  =  60; /* height of lattice plot */
int psbetheight  = 216; /* height of eta and beta plots */
int psvskip      =   8; /* distance between plots */
FILE *psfile;
#define psmovea(x,y)     fprintf(psfile,"%f %f M\n",x,y)
#define psdrawa(x,y)     fprintf(psfile,"%f %f L\n",x,y)
#define psstroke()       fprintf(psfile,"S\n")
#define psout(string)    fprintf(psfile,"%s\n",string)
#define pslabel(string)  fprintf(psfile,"(%s) show\n",string)

int argc_save;
char **argv_save;
int shared_memory = FALSE;


/*-----------------------------*/
#ifndef _AMIGA
#ifdef GLISH
#include <CWrap/client_wrap.h>
#include <CWrap/value_wrap.h>

extern void (*user_event_handler)(char*, void*);
extern int client_client(int *, char**);
extern int gflag;
/* * * * * * * * * * * * * * */
void my_event_handler(char *name, void *value)
{
  int elem;

  if(strcmp(name,"Refresh")==0)
    {
      user_key(1,"r");
    }
  else if(strcmp(name,"terminate")==0)
    {
      client_post_event_string("done","");
      printf("quit\n");
      exit(0);
    }
  else if(strcmp(name,"MoveTo")==0)
    {
      elem = value_intval(value,1);
      if(elem>=0 && elem<numtwiss)ielt = elem;
      erase();
      user_refresh(0,0,0,0);
    }
  else
    {
      printf("plot_ellipse: my_event_handler got unknown event: %s\n",name);
    }
}
#endif
#endif
/*******************************/
void getTwiss(void)
{
  int n;
/*  int i,j; */

#ifndef _AMIGA

  sds_handle ob_index_twiss;

  sds_output_errors(SDS_ERROR);
  sds_output_proginfo(SDS_ERROR);

  if(optifun == 0)
    {
      if(shared_memory)
	{
	  sds_twiss = sds_access(twissfile,SDS_SHARED_MEM,SDS_READ);
        } else {
	  sds_twiss = sds_access(twissfile,SDS_FILE,SDS_READ);
	}

      if(sds_twiss<=0)
	{
	  fprintf(stderr,"Could not open file %s \n",twissfile);
	  if(shared_memory)
	    {
	      fprintf(stderr,"from shared memory.\n");
	    } else {
	      fprintf(stderr,"from disk.\n");
	    }
	  exit(0);
	}
    }

  optifun = (optic *)sds_obname2ptr(sds_twiss,"optic");
  ob_index_twiss = sds_name2ind(sds_twiss,"optic");
  numtwiss = sds_array_size(sds_twiss,ob_index_twiss);
  twin = (twiss_ifile_name *)sds_obname2ptr(sds_twiss,"flat_file");
 
  if(twin != 0)
    {
      strcpy(latticefile,twin->filename);
    } else {
      latticefile[0] = 0;
    }


#else

  int file;
  int numbytes;

#define BSIZE (400 * sizeof(optic))
  char buffer[BSIZE];

  strcpy(latticefile,"Who knows?");
  file = open("AmTwiss",O_RDONLY,0);
  numbytes = read(file,buffer,BSIZE);
  numtwiss = numbytes/sizeof(optic);
  optifun = (optic *)buffer;

#endif

  nstart = 0;
  nend = numtwiss;
  zmax  = 0;
  zpmax =0;

  for(n=0;n<numtwiss;n++)
    {
      double sigx = sqrt(optifun[n].bm_xx);
      double sigxp= sqrt(optifun[n].bm_xpxp);
      double sigy = sqrt(optifun[n].bm_yy);
      double sigyp= sqrt(optifun[n].bm_ypyp);
      if(zmax<sigx)zmax=sigx;
      if(zpmax<sigxp)zpmax=sigxp;
      if(zmax<sigy)zmax=sigy;
      if(zpmax<sigyp)zpmax=sigyp;
    }
  zmax=1.2*zmax;
  zpmax=1.2*zpmax;
  printf("zmax,zpmax: %f %f\n",zmax,zpmax);
}
/***********************************/

#ifndef _AMIGA
void getLattice()
{
  sds_handle sds_lattice;

  if(got_lattice != 0)return;
  got_lattice = 1;

  if(latticefile[0]==0)
    {
      fprintf(stderr,"getLattice: the SDS flat file name: %s is unknown.\n",
	latticefile);
      lat_dat.number_of_elements = 0;
      return;
    }
  if(shared_memory)
    {
      sds_lattice = sds_access(latticefile,SDS_SHARED_MEM,SDS_READ);
    } else {
      sds_lattice = sds_access(latticefile,SDS_FILE,SDS_READ);
    }

  if(sds_lattice<0)
    {
      fprintf(stderr,"Lattice file %s was not an SDS file.\n",latticefile);
      lat_dat.number_of_elements = 0;
      return;
    }

  raw_flatin_sds(sds_lattice,&lat_dat);
}
#endif
/***********************************/
void goodscale(double *min, double *max, double *gridspacing)
{
  int i;
  double l, x, scale, tmp;
#define NBREAKS 11
  double breaks[NBREAKS] =
    {1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 5.0, 6.0, 8.0, 10.0};
  double gridsize[NBREAKS] =
    {0.2, 0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0,  2.0};
  double gplus, gminus;

/*  printf("goodscale(min, max): (%f,%f) --> ",*min,*max); */

  if(*max<=0.0)
    {
      *max = 0.0;
    } else {
      l = log10(*max);
      scale = pow(10.0,floor(l));
      x = *max/scale;
      for (i=0;i<NBREAKS-1;i++)
	{
	  if(x<breaks[i])
	    {
	      x = breaks[i];
	      gplus = gridsize[i];
	      break;
	    }
	}
      *max = x*scale;
    }

  if(*min>=0.0)
    {
      *min = 0.0;
    } else {
      *min = -(*min);
      l = log10(*min);
      tmp = pow(10.0,floor(l));
      if(tmp>scale)scale = tmp;
      x = *min/scale;
      for (i=0;i<NBREAKS-1;i++)
	{
	  if(x<breaks[i])
	    {
	      x = breaks[i];
	      gminus = gridsize[i];
	      break;
	    }
	}
      *min = -x*scale;
    }
  if(gplus >= gminus)
    {
      *gridspacing = gplus*scale;
    } else {
      *gridspacing = gminus*scale;
    }
/*  printf("(%f,%f) with grid spacing = %f\n",*min,*max,*gridspacing); */
}
/***********************************/
void reread(void)
{
  getTwiss();
#ifndef _AMIGA
  getLattice();
#endif
}
/***********************************/
void user_start(int argc,char **argv)
{
  int c;

#ifndef _AMIGA
  sds_init();
#ifdef GLISH
  user_event_handler = my_event_handler;
  client_client(&argc, argv);
  gflag = 1;
#endif
#endif

  argc_save = argc;
  argv_save = argv;

  while( --argc >0 && (*++argv)[0] == '-' )
    {
      while( (c = *++argv[0]) )
	{
	  switch(c)
	    {
	    case 's':
	      shared_memory = TRUE;
	      break;
	    default:
	      shared_memory = FALSE;
	    }
	}
    }

  if(argc>0)
    {
      strcpy(twissfile,argv[0]);
    }

  if(shared_memory)
    {
      printf("Looking for data in shared memory file: %s\n",twissfile);
    } else {
      printf("Looking for data in disk file: %s\n",twissfile);
    }

  reread();
}
/***********************************/
void makegrid(double xmax, double xstep, double ymax, double ystep)
{
  double x,y;

  setpen(7);
  y = 0;
  while(y<ymax+ystep/2.0)
    {
      movea(-xmax,y);
      drawa(xmax,y);
      y = y + ystep;
    }
  y = 0;
  while(y>-ymax-ystep/2.0)
    {
      movea(-xmax,y);
      drawa(xmax,y);
      y = y - ystep;
    }
  x = 0;
  while(x<xmax+xstep/2.0)
    {
      movea(x,-ymax);
      drawa(x,ymax);
      x = x + xstep;
    }
  x = 0;
  while(x>-xmax-xstep/2.0)
    {
      movea(x,-ymax);
      drawa(x,ymax);
      x = x - xstep;
    }
  
}
/*************************************/
void do_ellipse(double b, double a, double g)
{
  double A,B,C;
  int n;
  double phi,dphi;
  double x,y;
  
  dphi = 2*acos(-1.0)/NPT_ELLIPSE;
  x = b;
  A = sqrt(x);
  B = a/A;
  C = sqrt(b*g-a*a)/A;
  
  movea(A,B);
  phi = dphi;
  for(n=0; n<NPT_ELLIPSE; n++)
    {
      x = A*cos(phi);
      y = B*cos(phi)-C*sin(phi);
      drawa(x,y);
      phi = phi+dphi;
    }
}
/***********************************/
void user_refresh(int nleft, int nright, int ntop, int nbottom)
{
  int dx, dy;
  int x1,x2,x3,x4,y1,y2,y3,y4;
  double b,a,g;
  char tempstring[NAME_MAX];
  int lind, eind;

  if(nleft!=nright && ntop!=nbottom)
    {
      left = nleft;
      right = nright;
      top = ntop;
      bottom = nbottom;
    }
  dx = right-left;
  dy = bottom-top;

  x1 = LMARG;
  x4 = dx-RMARG;
  x2 = x1+(dx-RMARG-x1-LMARG)/2;
  x3 = x2+LMARG;

  y4 = TMARG;
  y1 = dy-BMARG;
  y2 = y4+(dy-y4-BMARG-BMARG)/2;
  y3 = y2-BMARG;

#ifndef _AMIGA
  if(lat_dat.number_of_elements>0)
    {
      lind = optifun[ielt].lattice_index;
      eind = optifun[ielt].element_index;
      strcpy(tempstring,"atom: ");
      strcat(tempstring,lat_dat.element_ptr[eind].name);
      lblabs(150,12,tempstring,9);

      while((lat_dat.atom_ptr[--lind].level != 1) && (lind>-1));
      if(lind>-1)
	{
	  strcpy(tempstring,"slot: ");
	  strcat(tempstring,
		 lat_dat.element_ptr
		 [lat_dat.atom_ptr[lind].element_index].name
		 );
	  lblabs(20,12,tempstring,9);
	}
    }
#endif

  setregion(x1,x2,y3,y4);
  setvirtual(-zmax,zmax,-zpmax,zpmax);
  makegrid(zmax,5.,zpmax,5.);
  b = optifun[ielt].bm_xx;
  a = optifun[ielt].bm_xxp;
  g = optifun[ielt].bm_xpxp;
  setpen(2);
  do_ellipse(b,a,g);
  
  setregion(x1,x2,y1,y2);
  setvirtual(-zmax,zmax,-zpmax,zpmax);
  makegrid(zmax,5.,zpmax,5.);
  b = optifun[ielt].bm_yy;
  a = optifun[ielt].bm_yyp;
  g = optifun[ielt].bm_ypyp;
  setpen(3);
  do_ellipse(b,a,g);
  
  setregion(x3,x4,y1,y4);
  setvirtual(-zmax,zmax,-zmax,zmax);
  makegrid(zmax,5.,zmax,5.);
  b = optifun[ielt].bm_yy;
  a = optifun[ielt].bm_xy;
  g = optifun[ielt].bm_xx;
  setpen(4);
  do_ellipse(b,a,g);

  XWflush();
}
/***********************************/
void tell_select(void)
{
/*
  printf("MoveTo %s lattice %d\n",latticefile,selected_lattice);
  printf("MoveTo %s element %d\n",latticefile,selected_element);
  printf("MoveTo Twiss optic %d\n",selected_optic);
*/
}

/***********************************/
void tell_unselect(void)
{
/*  printf("Unselected element %d\n",selected_element); */
}
/***********************************/
void user_mouse(int mbutton,int xmd,int ymd,int xmu,int ymu)
{
/*
  double dummy, x;
  int i;

  unscale(xmd,0,&x,&dummy);
  if(ymd>=t_lattice && ymd<=b_lattice)
    {
      for (i=0; i<numtwiss; i++)
	{
	  if(x>optifun[i].pathlen)
	    {
	    } else {
	      selected_optic   = i;
	      selected_element = optifun[i].element_index;
	      selected_lattice = optifun[i].lattice_index;
	      break;
	    }
	}
      tell_select();
    } else {
      if(mbutton==1)
	{
	  smin = x;
	  for(i=0; i<numtwiss; i++)
	    {
	      if(smin<=optifun[i].pathlen)break;
	      nstart = i;
	    }
	  if(nstart>0)--nstart;
	} else if(mbutton==2) {
	  smax = x;
	  for(i=0; i<numtwiss; i++)
	    {
	      if(smax<=optifun[i].pathlen)break;
	      nend = i;
	    }
	  nend = nend+2;
	  if(nend>numtwiss)nend = numtwiss;
	} else {
	  smin = smin0;
	  smax = smax0;
	  nstart = 0;
	  nend   = numtwiss;
	}
    }
  erase();
  user_refresh((int) 0, (int) 0, (int) 0, (int) 0);
*/
}
/***********************************/
void user_key(int i, char *code)
{
  if(code[0]=='r')
    {
      reread();           /* read in the data again */
      erase();
      user_refresh((int) 0, (int) 0, (int) 0, (int) 0);
    }
  if(code[0]=='h')
    {
      printf("plot_ellipse: The following keystrokes are recognized:\n");
      printf("    h: type this message\n");
      printf("    b: move to previous lattice location\n");
      printf("    c: cycle from beginning to end of beam line\n");
      printf("    n: move to next lattice location\n");
/*    printf("    p: generate a PostScript plot to file 'plot_twiss.ps'\n");*/
      printf("    q: quit\n");
      printf("    r: refresh the plot (rereads the data)\n");
      printf("\n    e#: element number via xterm or glish only\n");
    }      
  if(code[0]=='p')
    {
      printf("plot_ellipse does not output a PostScript file at present.\n");
/*      psoutput();       !!! Later !!! */
    }
  if(code[0]=='n')
    {
      ielt++;
      if(ielt>numtwiss)ielt=numtwiss-1;
      erase();
      user_refresh((int) 0, (int) 0, (int) 0, (int) 0);
    }
  if(code[0]=='b')
    {
      ielt--;
      if(ielt<0)ielt=0;
      erase();
      user_refresh((int) 0, (int) 0, (int) 0, (int) 0);
    }
  if(code[0]=='c')
    {
      for(ielt=0; ielt<numtwiss; ielt++)
	{
	  erase();
	  user_refresh(0,0,0,0);
	}
    }
#ifndef GLISH
  if(code[0]=='e')
    {
      int elem = atoi(code+1);
      if(elem>=0 && elem<numtwiss)ielt = elem;
      erase();
      user_refresh(0,0,0,0);
    }
#endif
}

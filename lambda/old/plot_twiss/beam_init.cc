/* $Header: /usr/local/lambda/plot_twiss/RCS/beam_init.cc,v 1.4 1994/06/09 13:19:30 mackay Exp $ */
/* beam_init.cc:  A program for tweaking the Twiss sds file object "optic" start
** parameters.
*/

#include <iostream.h>
#include <String.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <Sds/sdsgen.h>


#include <beam_init.h>
struct beam_init_ptrs beam_init;

#include <Twiss_data.h>

#include <Glish/Client.h>

sds_handle sds_twiss;
String twissfile;
Twiss_data odp;

int sharmemflag = 0;

sds_handle sds_beam_init;
String beam_init_file("beam_init.sds");
int i_species = 0;

Client *bi;

/*========================================*/
int process_cmdline(int argc, char **argv)
{
  int i;

  if(argc > 1)
    {
      for(i=1;i<argc;i++)
	{
	  if(String(argv[i])=="-s")
	    {
	      sharmemflag = 1;
	  } else {
	    twissfile = argv[i];
	  }
	}
    }
  if(twissfile.length()<=0)
    {
      cerr << "Usage: " << argv[0] << " [-s] <Twissfile>\n";
      cerr << "    -s\tuse shared memory\n";
      cerr << "    <Twissfile> is the name of the twiss output SDS file\n";
      return 1;
    }
  return 0;
}
/*========================================*/
int get_beam_init_sds(void)
{
  String errmsg;

  if(sharmemflag)
    {
      sds_beam_init = sds_access(beam_init_file,SDS_SHARED_MEM,SDS_WRITE);
    } else {
      sds_beam_init = sds_access(beam_init_file,SDS_FILE,SDS_READ);
    }

  if(sds_beam_init < 0)
    {
      errmsg = String("get_beam_init_sds: Could not sds_access(") +
	beam_init_file + ")";
      sds_perror((char*)errmsg);
      return 1;
    }

  beam_init.beam_init_p = (struct beam_init*)
    sds_obname2ptr(sds_beam_init, "beam_init");
  sds_code code = sds_name2ind(sds_beam_init, "beam_init");
  beam_init.num_beam_init = sds_array_size(sds_beam_init, code);

  return 0;
}
/*========================================*/
int get_Twiss(void)
{
  String errmsg;

  if(sharmemflag)
    {
      sds_twiss = sds_access(twissfile,SDS_SHARED_MEM,SDS_WRITE);
    } else {
      sds_twiss = sds_access(twissfile,SDS_FILE,SDS_READ);
    }

  if(sds_twiss < 0)
    {
      errmsg = String("get_Twiss: Could not sds_access(") + twissfile + ")";
      sds_perror((char*)errmsg);
      return 1;
    }

  int answer = twiss_in_sds(sds_twiss, &odp);

  cout << "odp.flat_file_ptr = " << (char*)odp.flat_file_ptr << endl;
  cout << "odp.number_of_optic=" << odp.number_of_optic << endl;
  cout << "odp.optic_ptr[0].beta_x = " << odp.optic_ptr[0].beta_x << endl;

  return answer;
}
/*========================================*/
void dump_beam_init(int i)
{  /* Just a check */
  struct beam_init &b = beam_init.beam_init_p[i];

  cout << "species[" << i << "0] = " << b.species << endl;
  cout << "  v_epsN_95 = " << b.v_epsN_95 << endl;
  cout << "  h_epsN_95 = " << b.h_epsN_95 << endl;
  cout << "  sigma_delta = " << b.sigma_delta << endl;
  cout << "  gamma = " << b.gamma << endl;
}
/*========================================*/
void recalc(int i)
{
  const double conv = 5.991;  // 5.991 * emit_rms = emit_95%

  struct beam_init &b = beam_init.beam_init_p[i];
  optic &o = odp.optic_ptr[0];

  double gamma = b.gamma;
  double voverc = sqrt(1-1/(gamma*gamma));
  double h_epsrms = b.h_epsN_95/gamma/voverc/conv;
  double v_epsrms = b.v_epsN_95/gamma/voverc/conv;
  double sigu2 = b.sigma_delta*b.sigma_delta;
  double sigz2 = b.sigma_z*b.sigma_z;

  o.beta_x = b.beta_x;
  o.alfa_x = b.alpha_x;
  o.gamma_x = (1+b.alpha_x*b.alpha_x)/b.beta_x;
  o.beta_y = b.beta_y;
  o.alfa_y = b.alpha_y;
  o.gamma_y = (1+b.alpha_y*b.alpha_y)/b.beta_y;
  o.eta_x = b.eta_x;
  o.eta_xp = b.eta_xp;
  o.eta_y = b.eta_y;
  o.eta_yp = b.eta_yp;
//  o.eta_z = b.eta_z; // This does not exist in beam_init.sds

  o.bm_xx = h_epsrms*b.beta_x + b.eta_x*b.eta_x*sigu2;
  o.bm_xxp = -h_epsrms*b.alpha_x + b.eta_x*b.eta_xp*sigu2;
  o.bm_xpxp = h_epsrms*o.gamma_x
    + b.eta_xp*b.eta_xp*sigu2;
  o.bm_xu = b.eta_x*sigu2;
  o.bm_xpu = b.eta_xp*sigu2;

  o.bm_yy = h_epsrms*b.beta_y + b.eta_y*b.eta_y*sigu2;
  o.bm_yyp = -h_epsrms*b.alpha_y + b.eta_y*b.eta_yp*sigu2;
  o.bm_ypyp = h_epsrms*o.gamma_y
    + b.eta_yp*b.eta_yp*sigu2;
  o.bm_yu = b.eta_y*sigu2;
  o.bm_ypu = b.eta_yp*sigu2;

  o.bm_zz = sigz2;
  o.bm_uu = sigu2;

}
/*========================================*/
void eventloop(void)
{ // This function should be replace by a glish event loop, but let's develop
  // the algorithm first.
  int i;
  GlishEvent *ev;

  while((ev = bi->NextEvent()))
    {
      if(!strcmp(ev->name,"terminate"))
	{
	  return;
	}
      else if(!strcmp(ev->name,"Select"))
	{
	  i = ev->value->IntVal();

	  if( (i < 0) || (i >= beam_init.num_beam_init) )
	    {
	      cerr << "? eventloop: i(species) out of range 0->"
		<< beam_init.num_beam_init << endl;
	    } else {
	      i_species = i;
	      dump_beam_init(i_species);
	    }
	}
      else if(!strcmp(ev->name,"Recalc"))
	{
	  recalc(i_species);  // recalculate
	  bi->PostEvent("Modify","");
	}
      else if(!strcmp(ev->name,"help"))
	{
	  cout << "At the moment the possible events are:\n";
	  cout << "\tterminate\n";
	  cout << "\thelp\t\tTypes this message\n";
	  cout << "\tSelect #\tSelects species #.\n";
	  cout << "\tRecalc\t\tRecalculates the initial twiss values.\n";
	  cout << "\t\t\t This generates a Modify event.\n";
	}
      else
	{
	  bi->Unrecognized();
	}
    }
}
/*========================================*/
int main(int argc, char ** argv)
{
  bi = new Client(argc,argv);

  if(process_cmdline(argc,argv))return 1;

  sds_init();
  if(get_beam_init_sds())return 1;
  if(get_Twiss())return 1;

  eventloop();

  delete bi;

  return 0;
}

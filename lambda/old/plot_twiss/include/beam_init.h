#ifndef SYB_beam_init_h
#define SYB_beam_init_h 1
/* $Header: /usr/local/lambda/plot_twiss/include/RCS/beam_init.h,v 1.2 1994/05/18 21:44:43 mackay Exp $ */
/* beam_init.h:  struct definitions for sybase tables  in group: beam_init  of database: ags_to_rhic
 Starting parameters of the beam from the AGS
*/
/* Each table has the following definitions:
**   1) an object template for reading the SDS data.
**   2) a name list declaration for writing SDS data.
**   3) a type list struct for writing the SDS data.
*/

struct beam_init
  {
    char species[21];
    double h_epsN_95;
    double v_epsN_95;
    double sigma_delta;
    double sigma_z;
    double gamma;
    double beta_x;
    double alpha_x;
    double eta_x;
    double eta_xp;
    double beta_y;
    double alpha_y;
    double eta_y;
    double eta_yp;
  };

#ifdef db_group_alloc_goober
char nm_beam_init[] = "species,h_epsN_95,v_epsN_95,sigma_delta,sigma_z,gamma,beta_x,alpha_x,eta_x,eta_xp,beta_y,alpha_y,eta_y,eta_yp";
#endif


#ifdef db_group_alloc_goober
struct type_list tl_beam_init[] =
  {
    {(sds_code)21,SDS_STRING},
    {(sds_code)1,SDS_DOUBLE},
    {(sds_code)1,SDS_DOUBLE},
    {(sds_code)1,SDS_DOUBLE},
    {(sds_code)1,SDS_DOUBLE},
    {(sds_code)1,SDS_DOUBLE},
    {(sds_code)1,SDS_DOUBLE},
    {(sds_code)1,SDS_DOUBLE},
    {(sds_code)1,SDS_DOUBLE},
    {(sds_code)1,SDS_DOUBLE},
    {(sds_code)1,SDS_DOUBLE},
    {(sds_code)1,SDS_DOUBLE},
    {(sds_code)1,SDS_DOUBLE},
    {(sds_code)1,SDS_DOUBLE},
    {(sds_code)0,SDS_RETLIST},
    {(sds_code)0,SDS_ENDLIST}
  };
#endif

/* The list of tables in this group. */
#ifdef db_group_alloc_goober
char gnl_beam_init[] = "beam_init";

#endif

/* Define a struct of ptrs to the SDS tables in group: beam_init */

struct beam_init_ptrs
  {
    struct beam_init *beam_init_p;
    long num_beam_init;
  };
#endif

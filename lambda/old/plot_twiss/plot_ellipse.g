# $Header: /rap/lambda/plot_twiss/RCS/plot_ellipse.g,v 1.4 1994/10/04 21:43:35 mackay Exp mackay $
# This is a glish script for viewing the Twiss data from the Holy_Lattice
# files with "plot_twiss".

lelement := 0  #since we start out looking at element 0
telement := 0  #since we start out looking at element 0

# Load files into shared memory.
twissname:='Twiss.YTransfer'
latticename:='YTransfer'
loadshm:=shell("cd $HOLY_LATTICE/YTransfer;f2sm YTransfer")
loadshm:=shell("cd $HOLY_LATTICE/YTransfer;f2sm Twiss.YTransfer")
loadshm:=shell("f2sm beam_init.sds")
beamlinename  := shell(paste("smd -s -n",latticename))

# Start up the clients
sid_l:=client("sid -w -s -glish -geometry +1+1",latticename)
sid_t:=client("sid -s -glish -geometry +455+1",twissname)
sid_b:=client("sid -w -s -glish -geometry +1+1 -l16 beam_init.sds")

twiss:=shell(paste('twiss -s -i',latticename),async=T)
bi:=client("beam_init -s Twiss.YTransfer")
pt:=client("plot_twiss -geometry 800x360+1+1 -s ",twissname)
pe:=client("plot_ellipse -geometry 300x300+800+1 -s ",twissname)
#bi:=client("beam_init -s Twiss.YTransfer", suspend = T)

# -- -- -- -- -- -- -- -- -- -- -- --

# OK.  Everything should be there.  Let's boogaloo!

whenever twiss->stdout do
{
  print $value
  pt->Refresh()
  sid_t->Redisplay()
  pe->Refresh()
}

whenever sid_l->MoveTo do
{
  words:=split($value)
  if(words[2]=="element")
    {
      lelement := words[3]
    }
}

whenever sid_t->MoveTo do
{
  words:=split($value)
  if(words[2]=="optic")
    {
      telement := words[3]
      pe->MoveTo(words[3])
    }
}

whenever sid_l->Modify do
{
  words:=split($value)
  print "update",lelement
  twiss->stdin(paste("update ",lelement,"\n"))
  await twiss->stdout
  twiss->stdin("football\n")
  await twiss->stdout

  pt->Refresh()
  sid_t->Redisplay()
  pe->Refresh()
}

whenever pt->MoveTo do
{
  words:=split($value,' ')
  print $value
  if (words[1] == latticename)
    {
      if(words[2]=="lattice")
        {
	  quack:=paste(beamlinename,words[2],words[3])
	  sid_l->MoveTo(quack)
        } else {
	  # ignore  the element
	}
      } else {
        sid_t->MoveTo($value)
	pe->MoveTo(words[4])
    }
}

whenever sid_b->MoveTo do
{
  words:=split($value)
  bi->Select(words[3])
  bi->Recalc()
}

whenever sid_b->Modify do
{
  bi->Recalc()
}

whenever bi->Modify do
{
  twiss->stdin("football")
}

whenever sid_l->MoveTo do
{
}

whenever sid_l->* do
{
}

whenever sid_l->done do
{
  twiss->stdin("quit\n")
  pt->terminate()
  sid_t->terminate()
  sid_b->terminate()
  bi->terminate()
  pe->terminate()
}
whenever sid_t->done do
{
  twiss->stdin("quit\n")
  pt->terminate()
  sid_l->terminate()
  sid_b->terminate()
  bi->terminate()
  pe->terminate()
} 
whenever sid_b->done do
{
  twiss->stdin("quit\n")
  pt->terminate()
  sid_l->terminate()
  sid_t->terminate()
  bi->terminate()
  pe->terminate()
} 

whenever bi->done do
{
  print "Got a bi->done event."
}
whenever bi->fail do
{
  print "Got a bi->fail event."
}

whenever twiss->fail do
{
  print $agent," failed."
}
whenever bi->fail do
{
  print $agent," failed."
}
whenever sid_l->fail do
{
  print $agent," failed."
}
whenever sid_b->fail do
{
  print $agent," failed."
}
whenever sid_t->fail do
{
  print $agent," failed."
}
whenever pt->fail do
{
  print $agent," failed."
}
whenever pe->fail do
{
  print $agent," failed."
}




##############################

#=============================
func dir_exists(file)
{
  return as_integer(shell("(test -d",file, " && echo 1) || echo 0"))
}
#=============================
func file_exists(file)
{
  return as_integer(shell("(test -f",file, " && echo 1) || echo 0"))
}
#=============================
func read()
{
  return shell("read blatz;echo $blatz")
}

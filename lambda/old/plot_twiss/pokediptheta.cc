/* $Header: /usr/local/lambda/plot_twiss/RCS/pokediptheta.cc,v 1.1 1994/05/11 19:45:36 mackay Exp $ */
/* pokedipst.cc:  A program for tweaking the Twiss sds file object "optic" start
** parameters.
*/

#include <iostream.h>
#include <String.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <Sds/sdsgen.h>
#include <flatin.h>

sds_handle sds_lattice;
String lattice_file("YTransfer");
int i_species = 0;

String element_name;
int param_ind;
double value;

flatin_data fdp;

/*========================================*/
int process_cmdline(int argc, char **argv)
{
  int i;

  if(argc > 2)
    {
//      param_ind = atoi(argv[1]);
      element_name = argv[1];
      value = atof(argv[2]);
      return 0;
    }
  cerr << "Usage: " << argv[0] << " <param_ind> <value>\n";
  return 1;
}
/*========================================*/
int get_lattice_sds(void)
{
  String errmsg;
  sds_code code;

  sds_lattice = sds_access((char*)lattice_file,SDS_SHARED_MEM,SDS_WRITE);

  if(sds_lattice < 0)
    {
      errmsg = String("get_lattice__sds: Could not sds_access(") +
	lattice_file + ")";
      sds_perror((char*)errmsg);
      return 1;
    }

  fdp.parameter_ptr = (parameter *) sds_obname2ptr(sds_lattice, "parameter");
  code = sds_obname2ind(sds_lattice, "parameter");
  fdp.number_of_parameters = sds_array_size(sds_lattice, code);

  fdp.element_ptr = (element *) sds_obname2ptr(sds_lattice, "element");
  code = sds_obname2ind(sds_lattice, "element");
  fdp.number_of_elements = sds_array_size(sds_lattice, code);

  return 0;
}
/*========================================*/
void poke(void)
{
  int i;

  for(i=0;i<fdp.number_of_elements;i++)
    {
      if(element_name==(String)fdp.element_ptr[i].name)
	{
	  if(String(fdp.element_ptr[i].type)!=String("sbend"))
	    {
	      cerr << "? " << element_name << " is not an sbend.\n";
	      return;
	    }
	  double theta = fdp.element_ptr[i].strength;
	  double l = fdp.element_ptr[i].length;
	  double lchord = 2*l*sin(theta/2)/theta;
	  fdp.element_ptr[i].length = lchord*value/2/sin(value/2);
	  fdp.element_ptr[i].strength = value;
	  double e12 = value/2;
	  
	  int j = fdp.element_ptr[i].more_index+3;
	  if(fdp.parameter_ptr[j].name==(String)"e1")
	    {
	      fdp.parameter_ptr[j].value = value/2;
	    } else {
	      cout << "Are you sure you know what you're doing?" << endl;
	      cout << " " << element_name << " doesn't appear to have an e1.\n";
	    }
	  if(fdp.parameter_ptr[j+1].name==(String)"e2")
	    {
	      fdp.parameter_ptr[j+1].value = value/2;
	    } else {
	      cout << "Are you sure you know what you're doing?" << endl;
	      cout << " " << element_name << " doesn't appear to have an e1.\n";
	    }
	  cout << "Changed " << element_name << "(length,strength,e1,e2) to "
	    << fdp.element_ptr[i].length << ", "
	    << fdp.element_ptr[i].strength << ", "
	    << fdp.parameter_ptr[j].value << ", "
	    << fdp.parameter_ptr[j+1].value << endl;
	  return;
	}
    }
  cerr << "Could not find element: " << element_name << endl;
}
/*========================================*/
int main(int argc, char ** argv)
{

  if(process_cmdline(argc,argv))return 1;

  sds_init();
  if(get_lattice_sds())return 1;

  poke();

  return 0;
}

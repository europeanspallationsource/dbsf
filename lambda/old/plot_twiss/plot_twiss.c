/* $Header: /rap/lambda/plot_twiss/RCS/plot_twiss.c,v 1.18 1997/04/22 19:39:43 mackay Exp mackay $ */
/* plot_twiss.c,v 0.0, 29 July, 1993 mackay */

#define PS_GRAY 1

#include  <stdio.h>
#include  <math.h>
#include  <stdlib.h>
#include  <string.h>

#ifndef _AMIGA
#include  <unistd.h>
#else
#include <time.h>
#include <fcntl.h>
#endif

#include  <ctype.h>
#include  "XWaldo.h"


#ifndef _AMIGA
#include  <lattice_defines.h>
#include  <flatin.h>
  flatin_data lat_dat;
#include  <Sds/sdsgen.h>
#include  <twiss_dict.h>
#include  <twiss.h>
#else
#include  "lattice_defines.h"
#include  "flatin.h"
#include  "twiss_dict.h"
#include  "twiss.h"
#endif

optic *optifun = 0;
element *latelt = 0;
int got_lattice = 0;  /* flag for having got lattice via getLattice() */

int    numtwiss;
int    numelements;


char  latticefile[64] = "nephthys.a2blue";
char  twissfile[120]   = "Twiss";

double smin0,smax0;
double smin,smax,beta_min,beta_max,eta_min,eta_max;
double maxbeta_x,maxbeta_y;
double maxeta_x,maxeta_y;
double mineta_x,mineta_y;
double beta_grid,eta_grid;

int selected_optic   = -1;
int selected_element = -1;
int selected_lattice = -1;

int  nstart, nend;

int left   = 0;  /* window limits */
int right  = 250;
int top    = 0;
int bottom = 300;
int t_beta,b_beta,t_lattice,b_lattice,t_eta,b_eta;

#define LMARG     50
#define RMARG     20
#define TMARG     15
#define BMARG     15
#define LATHEIGHT 40
#define SMARG      2

/* Definitions for positioning graphics on PostScript page (72pts/in) */
int pslmarg      = 108; /* left edge of plots */
int psbottom     = 100; /* bottom of eta plot */
int pswidth      = 432; /* width of plots */
int pslatheight  =  60; /* height of lattice plot */
int psbetheight  = 216; /* height of eta and beta plots */
int psvskip      =   8; /* distance between plots */
FILE *psfile;
#define psmovea(x,y)     fprintf(psfile,"%f %f M\n",x,y)
#define psdrawa(x,y)     fprintf(psfile,"%f %f L\n",x,y)
#define psstroke()       fprintf(psfile,"S\n")
#define psout(string)    fprintf(psfile,"%s\n",string)
#define pslabel(string)  fprintf(psfile,"(%s) show\n",string)

int argc_save;
char **argv_save;
int shared_memory = FALSE;

/*-----------------------------*/
#ifndef _AMIGA
#ifdef GLISH
#include <CWrap/client_wrap.h>
extern void (*user_event_handler)(const char*, void*);
extern int client_client(int *, char**);
extern int gflag;
/* * * * * * * * * * * * * * */
void my_event_handler(const char *name, void *value)
{
  if(strcmp(name,"Refresh")==0)
    {
      user_key(1,"r");
    }
  else if(strcmp(name,"terminate")==0)
    {
      client_post_event_string("done","");
      printf("quit\n");
      exit(0);
    }
  else
    {
      printf("plot_twiss: my_event_handler got unknown event: %s\n",name);
    }
}
#endif
#endif
/*******************************/
void getTwiss(void)
{
  int n;
/*  int i,j; */

#ifndef _AMIGA

  twiss_ifile_name *twin;
  static sds_handle sds_twiss;
  sds_handle ob_index_twiss;

  sds_output_errors(SDS_ERROR);
  sds_output_proginfo(SDS_ERROR);

  if(optifun == 0)
    {
      if(shared_memory)
	{
	  sds_twiss = sds_access(twissfile,SDS_SHARED_MEM,SDS_READ);
        } else {
	  sds_twiss = sds_access(twissfile,SDS_FILE,SDS_READ);
	}

      if(sds_twiss<=0)
	{
	  fprintf(stderr,"Could not open file %s \n",twissfile);
	  if(shared_memory)
	    {
	      fprintf(stderr,"from shared memory.\n");
	    } else {
	      fprintf(stderr,"from disk.\n");
	    }
	  exit(0);
	}
    }

  optifun = (optic *)sds_obname2ptr(sds_twiss,"optic");
  ob_index_twiss = sds_name2ind(sds_twiss,"optic");
  numtwiss = sds_array_size(sds_twiss,ob_index_twiss);
  twin = (twiss_ifile_name *)sds_obname2ptr(sds_twiss,"flat_file");
 
  if(twin != 0)
    {
      strcpy(latticefile,twin->filename);
    } else {
      latticefile[0] = 0;
    }


#else

  int file;
  int numbytes;

#define BSIZE (400 * sizeof(optic))
  char buffer[BSIZE];

  strcpy(latticefile,"Who knows?");
  file = open("AmTwiss",O_RDONLY,0);
  numbytes = read(file,buffer,BSIZE);
  numtwiss = numbytes/sizeof(optic);
  optifun = (optic *)buffer;
  close(file);

#endif

  nstart = 0;
  nend = numtwiss;

  maxbeta_x = 0;
  maxbeta_y = 0;
  maxeta_x = 0;
  maxeta_y = 0;
  mineta_x = 0;  /* Even though min(eta) may be >0 we include the axis */
  mineta_y = 0;
  
  for(n=0;n<numtwiss;n++)
    {
      if(maxbeta_x<optifun[n].beta_x) maxbeta_x = optifun[n].beta_x;
      if(maxbeta_y<optifun[n].beta_y) maxbeta_y = optifun[n].beta_y;
      if(maxeta_x<optifun[n].eta_x) maxeta_x = optifun[n].eta_x;
      if(maxeta_y<optifun[n].eta_y) maxeta_y = optifun[n].eta_y;
      if(mineta_x>optifun[n].eta_x) mineta_x = optifun[n].eta_x;
      if(mineta_y>optifun[n].eta_y) mineta_y = optifun[n].eta_y;
    }
}
/***********************************/

#ifndef _AMIGA
void getLattice()
{
  sds_handle sds_lattice;

  if(got_lattice != 0)return;
  got_lattice = 1;

  if(latticefile[0]==0)
    {
      fprintf(stderr,"getLattice: the SDS flat file name: %s is unknown.\n",
	latticefile);
      lat_dat.number_of_elements = 0;
      return;
    }
  if(shared_memory)
    {
      sds_lattice = sds_access(latticefile,SDS_SHARED_MEM,SDS_READ);
    } else {
      sds_lattice = sds_access(latticefile,SDS_FILE,SDS_READ);
    }

  if(sds_lattice<0)
    {
      fprintf(stderr,"Lattice file %s was not an SDS file.\n",latticefile);
      lat_dat.number_of_elements = 0;
      return;
    }

  raw_flatin_sds(sds_lattice,&lat_dat);
}
#endif

/***********************************/
void goodscale(double *min, double *max, double *gridspacing)
{
  int i;
  double l, x, scale, tmp;
#define NBREAKS 11
  double breaks[NBREAKS] =
    {1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 5.0, 6.0, 8.0, 10.0};
  double gridsize[NBREAKS] =
    {0.2, 0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0,  2.0};
  double gplus, gminus;

/*  printf("goodscale(min, max): (%f,%f) --> ",*min,*max); */
  gplus = 0;
  gminus = 0;

  if(*max<=0.0)
    {
      *max = 0.0;
    } else {
      l = log10(*max);
      scale = pow(10.0,floor(l));
      x = *max/scale;
      for (i=0;i<NBREAKS;i++)
	{
	  if(x<breaks[i])
	    {
	      x = breaks[i];
	      gplus = gridsize[i];
	      break;
	    }
	}
      *max = x*scale;
    }

  if(*min>=0.0)
    {
      *min = 0.0;
    } else {
      *min = -(*min);
      l = log10(*min);
      tmp = pow(10.0,floor(l));
      if(tmp>scale)scale = tmp;
      x = *min/scale;
      for (i=0;i<NBREAKS;i++)
	{
	  if(x<breaks[i])
	    {
	      x = breaks[i];
	      gminus = gridsize[i];
	      break;
	    }
	}
      *min = -x*scale;
    }
  if(gplus >= gminus)
    {
      *gridspacing = gplus*scale;
    } else {
      *gridspacing = gminus*scale;
    }
/*  printf("(%f,%f) with grid spacing = %f\n",*min,*max,*gridspacing); */
}
/***********************************/
void reread(void)
{
  getTwiss();
#ifndef _AMIGA
  getLattice();
#endif

  smin0 = optifun[0].pathlen;
  smax0 = optifun[numtwiss-1].pathlen;
  smin = smin0;
  smax = smax0;
  beta_min = 0;
  beta_max = maxbeta_x;
  if(beta_max<maxbeta_y) beta_max = maxbeta_y;
  goodscale(&beta_min,&beta_max,&beta_grid);

  eta_min = mineta_x;
  if(eta_min>mineta_y)eta_min = mineta_y;
  eta_max = maxeta_x;
  if(eta_max<maxeta_y) eta_max = maxeta_y;
  goodscale(&eta_min,&eta_max,&eta_grid);
}
/***********************************/
void user_start(int argc,char **argv)
{
  int c;

#ifndef _AMIGA
  sds_init();
#ifdef GLISH
  user_event_handler = my_event_handler;
  client_client(&argc, argv);
  gflag = 1;
#endif
#endif

  argc_save = argc;
  argv_save = argv;

  while( --argc >0 && (*++argv)[0] == '-' )
    {
      while( (c = *++argv[0]) )
	{
	  switch(c)
	    {
	    case 's':
	      shared_memory = TRUE;
	      break;
	    default:
	      shared_memory = FALSE;
	    }
	}
    }

  if(argc>0)
    {
      strcpy(twissfile,argv[0]);
    }

  if(shared_memory)
    {
      printf("Looking for data in shared memory file: %s\n",twissfile);
    } else {
      printf("Looking for data in disk file: %s\n",twissfile);
    }
  reread();
}
/***********************************/
void makegrid(double min, double max, double gridstep)
{
  double y;

  y = gridstep;
  while(y<max)
    {
      movea(smin,y);
      drawa(smax,y);
      y = y + gridstep;
    }
  y = 0;
  while(y>min)
    {
      movea(smin,y);
      drawa(smax,y);
      y = y - gridstep;
    }
}
/***********************************/
void psgrid(double min, double max, double gridstep)
{
  double y;

  y = gridstep;
  while(y<max)
    {
      psmovea(smin,y);
      psdrawa(smax,y);
      psstroke();
      y = y + gridstep;
    }
  y = 0;
  while(y>min)
    {
      psmovea(smin,y);
      psdrawa(smax,y);
      psstroke();
      y = y - gridstep;
    }
}
/***********************************/
void plot_beta(void)
{
  int n;
  char string[20];
  char tempstring[NAME_MAX];
  int lind;

  lblabs(50,12,latticefile,9);
  setpen(4);
  lblabs(200,12,"H",9);
  setpen(3);
  lblabs(220,12,"V",9);
  setpen(1);

  if( (selected_optic > -1) && (lat_dat.number_of_elements > 0))
    {
/* Display the atomic name of this element */
      strcpy(tempstring,"atom: ");
      strcat(tempstring,lat_dat.element_ptr[selected_element].name);
      lblabs(400,12,tempstring,9);

/* Display the name of the slot containing this element */
      lind = selected_lattice;
      if(lind<0)
	{
/*	  fprintf(stderr,"refresh: bad lattice index\n"); */
        } else {
	  while((lat_dat.atom_ptr[--lind].level) != 1 && (lind>-1));
	  if(lind>-1)
	    {
	      strcpy(tempstring,"slot: ");
	      strcat(tempstring,
		     lat_dat.element_ptr
		     [lat_dat.atom_ptr[lind].element_index].name
		     );
	      lblabs(300,12,tempstring,9);
	    }
	}
    }

  setregion(left+LMARG,right-RMARG,t_beta,b_beta);
  setvirtual(smin,smax,beta_min,beta_max);

  clip(1);
  setpen(7);
  makegrid(beta_min,beta_max,beta_grid);
  clip(0);
  setpen(1);
  boxregion();

  labela(smin,(double)0,"0",4);
  sprintf(string,"%5.0f",beta_max);
  labela(smin,beta_max,string,4);
  labela(smin,(beta_max+beta_min)/2.0,"beta(m)",4);

  clip(1);

  setpen(4);
  for(n=nstart; n<nend; n++)
    {
      if(n==nstart)movea(optifun[n].pathlen,optifun[n].beta_x);
      drawa(optifun[n].pathlen,optifun[n].beta_x);
    }
  setpen(3);
  for(n=nstart; n<nend; n++)
    {
      if(n==nstart)movea(optifun[n].pathlen,optifun[n].beta_y);
      drawa(optifun[n].pathlen,optifun[n].beta_y);
    }
  setpen(1);
  clip(0);
}
/***********************************/
void plot_eta(void)
{
  int n;
  char string[20];

  setregion(left+LMARG,right-RMARG,t_eta,b_eta);
  setvirtual(smin,smax,eta_min,eta_max);

  setpen(7);
  clip(1);
  makegrid(eta_min,eta_max,eta_grid);
  setpen(10);
  clip(0);
  boxregion();

  sprintf(string,"%6.1f",eta_min);
  labela(smin,eta_min,string,4);
  sprintf(string,"%6.1f",eta_max);
  labela(smin,eta_max,string,4);
  labela(smin,(eta_max+eta_min)/2.0,"eta(m)  ",4);

  sprintf(string,"%6.1f",smin);
  labela(smin,eta_min,string,2);
  sprintf(string,"%6.1f",smax);
  labela(smax,eta_min,string,2);
  labela((smax+smin)/2.0,eta_min,"s(m)",2);

  if(eta_min<0.0)
    {
      labela(smin,(double)0,"0",4);
    }
  clip(1);

  setpen(4);
  for(n=nstart; n<nend; n++)
    {
      if(n==nstart)movea(optifun[n].pathlen,optifun[n].eta_x);
      drawa(optifun[n].pathlen,optifun[n].eta_x);
    }
  setpen(3);
  for(n=nstart; n<nend; n++)
    {
      if(n==nstart)movea(optifun[n].pathlen,optifun[n].eta_y);
      drawa(optifun[n].pathlen,optifun[n].eta_y);
    }
  setpen(1);
  clip(0);
}
/***********************************/
void plot_lattice(void)
{
  int ielt, itype;
  int n;
  double s0, s1, y;

  setpen(1);
  setregion(left+LMARG,right-RMARG,t_lattice,b_lattice);
  setvirtual(smin,smax,-0.1,5.);
  boxregion();

  s0 = smin;
  movea(s0,0.);

  clip(1);
  for(n=nstart; n<nend; n++)
    {
      ielt = optifun[n].lattice_index;
      itype = optifun[n].type_index;
      s1 = optifun[n].pathlen;
      switch (itype)
	{
	case (ATTR_RBEND):
	  y = 1.0;
	  break;
	case (ATTR_SBEND):
	  y = 1.0;
	  break;
	case (ATTR_QUADRUPOLE):
	  y = 2.0;
	  break;
	case (ATTR_SEXTUPOLE):
	  y = 3.0;
	  break;
	case (ATTR_OCTUPOLE):
	  y = 4.0;
	  break;
	case (ATTR_VKICK):
	  y = 0.5;
	  break;
	case (ATTR_HKICK):
	  y = 0.5;
	  break;
	case (ATTR_KICKER):
	  y = 0.5;
	  break;
	default:
	  y = 0;
	}
      if( (selected_optic == n) || (selected_lattice==ielt) )
	{
	  drawa(s0,0.);
	  if(selected_lattice == ielt)
	    {
	      setpen(4);
	    }
	  else if(selected_optic == n)
	    {
	      setpen(3);
	    }
	}
      drawa(s0,y);
      drawa(s1,y);
      if( (selected_optic == n) || (selected_lattice == ielt) )
	{
	  drawa(s1,0.);
	  setpen(1);
	}
      s0 = s1;
    }
  clip(0);
}
/***********************************/
void pssetpen(int pen)
{
#ifdef PS_GRAY
  switch (pen)
    {
    case (0):
      psout("1 setgray [] 0 setdash");  /* solid white */
      break;
    case (1):
      psout("0 setgray [] 0 setdash");  /* solid black */
      break;
    case (2):
/*      psout("0 setgray [2 4 4] 0 setdash");*/ /* dashed black */
      psout("0 setgray [] 0 setdash");  /* solid black */
      break;
    case (3):
      psout("0 setgray [2] 0 setdash"); /* dotted black */
      break;
    case (4):
      psout("0 setgray [3 3 1 3] 0 setdash"); /* dot-dashed black */
      break;
    case (5):
      psout("0 setgray [5 5 2 5] 0 setdash"); /* dot-dashed black */
      break;
    case (6):
      psout("0 setgray [5 2 2 2] 0 setdash"); /* dot-dashed black */
      break;
    case (7):
      psout("0.3 setgray [1 8] 0 setdash"); /* lightly dotted */
      break;
    default:
      psout("0 setgray [] 0 setdash");  /* solid black */
    }
#else
  switch (pen)
    {
    case (0):
      psout("1 setgray");                   /* solid white */
      break;
    case (1):
      psout("0 setgray [] 0 setdash");      /* solid black */
      break;
    case (2):
      psout("1 0 0 setrgbcolor");           /* red */
      break;
    case (3):
      psout("0 0 1 setrgbcolor");           /* blue */
      break;
    case (4):
      psout("0 0.8 0 setrgbcolor");         /* green */
      break;
    case (5):
      psout("1 0 1 setrgbcolor");           /* magenta */
      break;
    case (6):
      psout("0 0.9 0.9 setrgbcolor");       /* cyan */
      break;
    case (7):
      psout("0.75 0.7 0 setrgbcolor");      /* gold */
      break;
    default:
      psout("0 setgray");                   /* solid black */
    }
#endif
}
/***********************************/
void psibox(int width, int height)
{
  fprintf(psfile,"0 0 moveto\n");
  fprintf(psfile,"0 %d rlineto\n",height);
  fprintf(psfile,"%d 0 rlineto\n",width);
  fprintf(psfile,"0 %d rlineto\n",-height);
  fprintf(psfile,"%d 0 rlineto\n",-width);
  fprintf(psfile,"stroke\n");
}
/***********************************/
void psclip(int width, int height)
{
  fprintf(psfile,"initclip\n");
  fprintf(psfile,"0 0 moveto\n");
  fprintf(psfile,"0 %d rlineto\n",height);
  fprintf(psfile,"%d 0 rlineto\n",width);
  fprintf(psfile,"0 %d rlineto\n",-height);
  fprintf(psfile,"%d 0 rlineto\n",-width);
  fprintf(psfile,"clip\n");
}
/***********************************/
void psplotbeta(void)
{
  int n, npath;
  char string[20];
  double xscale, yscale;
  double temp;

  psout("GS");
  fprintf(psfile,"%d %d translate\n",pslmarg,
	  psbottom+psbetheight+psvskip+pslatheight+psvskip);

  psibox(pswidth,psbetheight);

/* put on some labels */
  psout("/Times-Roman findfont 15 scalefont setfont");
  temp = 4.0;
  psmovea(5.0,psbetheight + temp);
  pslabel(latticefile);

  setpen(1);
  psmovea(200.0,psbetheight + temp);

  pslabel("H");
  pssetpen(4);
  psmovea(200.0+15.0,psbetheight+5.0+temp);
  psout("30.0 0 rlineto S\n");
  pssetpen(1);

  psmovea(260.0,psbetheight + temp);
  pslabel("V");
  pssetpen(3);
  psmovea(200.0+75.0,psbetheight+5.0+temp);
  psout("30.0 0 rlineto S\n");
  pssetpen(1);

/* label the vertical axis (horiz gets done in psploteta) */
  temp = -5.0;
  psmovea(-5.0,temp);
  psout("(0) dup stringwidth pop neg 0 rmoveto show");

  sprintf(string,"%5.0f",beta_max);
  psmovea(-5.0,psbetheight+temp);
  fprintf(psfile,"(%s) dup stringwidth pop neg 0 rmoveto show\n",string);

  psout("GS");
  psmovea(-20.0,psbetheight/2.0-20.0);
  psout("90 rotate");
  psout("/Symbol findfont 15 scalefont setfont");
  pslabel("b");
  psout("/Times-Roman findfont 15 scalefont setfont");
  pslabel(" [m]");
  psout("GR");

  psclip(pswidth,psbetheight);

  psout("GS");
  
  xscale = ((double) pswidth)/(smax-smin);
  yscale = ((double) psbetheight)/(beta_max-beta_min);
  fprintf(psfile,"%f %f scale\n",xscale,yscale);
  fprintf(psfile,"%f %f translate\n",-smin,-beta_min);

  psgrid(beta_min,beta_max,beta_grid);

  pssetpen(4);

  npath = 0;
  for(n=nstart; n<nend; n++)
    {
      if(npath<=0)
	{
	  if(n!=nstart)
	    {
	      psdrawa(optifun[n].pathlen,optifun[n].beta_x);
	      psout("S");
	    }
	  psmovea(optifun[n].pathlen,optifun[n].beta_x);
	  npath = 50;
	}
      psdrawa(optifun[n].pathlen,optifun[n].beta_x);
      --npath;
    }
  psstroke();
  
  pssetpen(3);
  npath = 0;
  for(n=nstart; n<nend; n++)
    {
      if(npath<=0)
	{
	  if(n!=nstart)
	    {
	      psdrawa(optifun[n].pathlen,optifun[n].beta_y);
	      psout("S");
	    }
	  psmovea(optifun[n].pathlen,optifun[n].beta_y);
	  npath = 50;
	}
      psdrawa(optifun[n].pathlen,optifun[n].beta_y);
      --npath;
    }
  psstroke();
  
  pssetpen(1);

  psout("GR");
  psout("GR");
}
/***********************************/
void psploteta(void)
{
  int n, npath;
  char string[20];
  double xscale, yscale;
  double temp;

  psout("GS");
  fprintf(psfile,"%d %d translate\n",pslmarg,psbottom);
  psibox(pswidth,psbetheight);

/* label the vertical axis */
  psout("/Times-Roman findfont 15 scalefont setfont");
  temp = -5.0;
  sprintf(string,"%6.1f",eta_min);
  psmovea(-5.0,temp+1.0);
  fprintf(psfile,"(%s) dup stringwidth pop neg 0 rmoveto show\n",string);

  sprintf(string,"%6.1f",eta_max);
  psmovea(-5.0,psbetheight+temp);
  fprintf(psfile,"(%s) dup stringwidth pop neg 0 rmoveto show\n",string);

  if(eta_min<0.0)
    {
      psmovea(-5.0,-eta_min*psbetheight/(eta_max-eta_min) + temp);
      psout("(0) dup stringwidth pop neg 0 rmoveto show");
    }

  psout("GS");
  psmovea(-20.0,psbetheight/2.0-20.0);
  psout("90 rotate");
  psout("/Symbol findfont 15 scalefont setfont");
  pslabel("h");
  psout("/Times-Roman findfont 15 scalefont setfont");
  pslabel(" [m]");
  psout("GR");

/* label the bottom axis */
  temp = -15.0;
  psmovea(0.0,temp);
  sprintf(string,"%6.1f",smin);
/*  fprintf(psfile,"(%s) dup stringwidth pop 2 div neg 0 rmoveto show\n",
	  string);
*/
  pslabel(string);
  psmovea((double) pswidth,temp);
  sprintf(string,"%6.1f",smax);
  fprintf(psfile,"(%s) dup stringwidth pop neg 0 rmoveto show\n",
	  string);
  psmovea((double) pswidth/2.0,temp);
  psout("(s [m]) dup stringwidth pop 2 div neg 0 rmoveto show");


  psclip(pswidth,psbetheight);

  xscale = ((double) pswidth)/(smax-smin);
  yscale = ((double) psbetheight)/(eta_max-eta_min);
  fprintf(psfile,"%f %f scale\n",xscale,yscale);
  fprintf(psfile,"%f %f translate\n",-smin,-eta_min);

  psgrid(eta_min,eta_max,eta_grid);

  pssetpen(4);
  npath = 0;
  for(n=nstart; n<nend; n++)
    {
      if(npath<=0)
	{
	  if(n!=nstart)
	    {
	      psdrawa(optifun[n].pathlen,optifun[n].eta_x);
	      psout("S");
	    }
	  psmovea(optifun[n].pathlen,optifun[n].eta_x);
	  npath = 50;
	}
      psdrawa(optifun[n].pathlen,optifun[n].eta_x);
      --npath;
    }
  psstroke();
  pssetpen(3);
  npath = 0;
  for(n=nstart; n<nend; n++)
    {
      if(npath<=0)
	{
	  if(n!=nstart)
	    {
	      psdrawa(optifun[n].pathlen,optifun[n].eta_y);
	      psout("S");
	    }
	  psmovea(optifun[n].pathlen,optifun[n].eta_y);
	  npath = 50;
	}
      psdrawa(optifun[n].pathlen,optifun[n].eta_y);
      --npath;
    }
  psstroke();
  pssetpen(1);

  psout("GR");
}
/***********************************/
void psplotlattice(void)
{
  int ielt, itype, n, npath;
  double s0, s1, y;
  double xscale, yscale;

  psout("GS");
  fprintf(psfile,"%d %d translate\n",pslmarg,psbottom+psbetheight+psvskip);
  psibox(pswidth,pslatheight);
  psclip(pswidth,pslatheight);

  xscale = ((double) pswidth)/(smax-smin);
  yscale = ((double) pslatheight)/5.1;
  fprintf(psfile,"%f %f scale\n",xscale,yscale);
  fprintf(psfile,"%f %f translate\n",-smin,0.1);


  s0 = smin;
  npath = 0;
  for(n=nstart; n<nend; n++)
    {
      ielt = optifun[n].lattice_index;
      itype = optifun[n].type_index;
      s1 = optifun[n].pathlen;
      switch (itype)
	{
	case (ATTR_RBEND):
	  y = 1.0;
	  break;
	case (ATTR_SBEND):
	  y = 1.0;
	  break;
	case (ATTR_QUADRUPOLE):
	  y = 2.0;
	  break;
	case (ATTR_SEXTUPOLE):
	  y = 3.0;
	  break;
	case (ATTR_OCTUPOLE):
	  y = 4.0;
	  break;
	case (ATTR_VKICK):
	  y = 0.5;
	  break;
	case (ATTR_HKICK):
	  y = 0.5;
	  break;
	case (ATTR_KICKER):
	  y = 0.5;
	  break;
	default:
	  y = 0;
	}
      if(npath<=0)
	{
	  if(n!=nstart)
	    {
	      psdrawa(s0,0.0);
	      psout("S");
	    }
	  psmovea(s0,0.0);
	  npath = 30;
	} else {
	  psdrawa(s0,0.0);
	  --npath;
	}
      psdrawa(s0,y);
      psdrawa(s1,y);
      s0 = s1;
    }
  psstroke();

  psout("GR");
}
/***********************************/
void user_refresh(int nleft, int nright, int ntop, int nbottom)
{
  int h;

  if(nleft!=nright && ntop!=nbottom)
    {
      left = nleft;
      right = nright;
      top = ntop;
      bottom = nbottom;

      t_beta = top + TMARG;
      b_eta  = bottom - BMARG;
      h = (bottom-top-TMARG-BMARG-LATHEIGHT)/2;
      t_lattice = t_beta+h;
      b_lattice = b_eta-h;
      b_beta = t_lattice-SMARG;
      t_eta = b_lattice+SMARG;
    }

  plot_beta();
  plot_eta();
  plot_lattice();
  XWflush();
}
/***********************************/
void tell_select(int mbutton)
{
#ifdef GLISH
  char str[60];

  if(mbutton==1)
    {
      sprintf(str,"%s lattice %d\n",latticefile,selected_lattice);
      client_post_event_string("MoveTo",str);
      sprintf(str,"%s element %d\n",latticefile,selected_element);
      client_post_event_string("MoveTo",str);
    } else {
      sprintf(str,"%s optic %d\n",twissfile,selected_optic);
      client_post_event_string("MoveTo",str);
    }
#else
  if(mbutton==1)
    {
      printf("MoveTo %s lattice %d\n",latticefile,selected_lattice);
      printf("MoveTo %s element %d\n",latticefile,selected_element);
    } else {
      printf("MoveTo %s optic %d\n",twissfile,selected_optic);
    }
#endif
}
/***********************************/
void tell_unselect(void)
{
/*  printf("Unselected element %d\n",selected_element); */
}
/***********************************/
void user_mouse(int mbutton,int xmd,int ymd,int xmu,int ymu)
{
  double dummy, x;
  int i;

  unscale(xmd,0,&x,&dummy);
  if(ymd>=t_lattice && ymd<=b_lattice)
    {
      for (i=0; i<numtwiss; i++)
	{
	  if(x>optifun[i].pathlen)
	    {
	    } else {
	      if(mbutton==1)
		{
		  selected_element = optifun[i].element_index;
		  selected_lattice = optifun[i].lattice_index;
		} else {
		  selected_optic   = i;
		}
	      break;
	    }
	}
      tell_select(mbutton);
    } else {
      if(mbutton==1)
	{
	  smin = x;
	  for(i=0; i<numtwiss; i++)
	    {
	      if(smin<=optifun[i].pathlen)break;
	      nstart = i;
	    }
	  if(nstart>0)--nstart;
	} else if(mbutton==2) {
	  smax = x;
	  for(i=0; i<numtwiss; i++)
	    {
	      if(smax<=optifun[i].pathlen)break;
	      nend = i;
	    }
	  nend = nend+2;
	  if(nend>numtwiss)nend = numtwiss;
	} else {
	  smin = smin0;
	  smax = smax0;
	  nstart = 0;
	  nend   = numtwiss;
	}
    }
  erase();
  user_refresh(0,0,0,0);
}
/***********************************/
void psoutput(void)
{
  char time_str[30];

  time_t t = time(0);
  strcpy(time_str,ctime(&t));
  time_str[24]=0;

  psfile = fopen("plot_twiss.ps","w");

  fprintf(psfile,"%s\n","%!PS-Adobe-3.0");

  fprintf(psfile,"%s\n","%%BoundingBox: 76 82 540 624");

  fprintf(psfile,"%s\n","%%Creator: plot_twiss");
  fprintf(psfile,"%s%s\n","%%CreationDate: ",time_str);
  fprintf(psfile,"%s%s\n","%%For: ",getenv("USER"));
  fprintf(psfile,"%s\n","%%Pages: 1");
  fprintf(psfile,"%s\n","%%EndComments");

  fprintf(psfile,"%s\n","%%BeingProlog");
  fprintf(psfile,"%s\n","5 dict begin");
  fprintf(psfile,"%s\n","/L { lineto } bind def");
  fprintf(psfile,"%s\n","/M { moveto } bind def");
  fprintf(psfile,"%s\n","/S { stroke } bind def");
  fprintf(psfile,"%s\n","/GR { grestore } bind def");
  fprintf(psfile,"%s\n","/GS { gsave } bind def");
  fprintf(psfile,"%s\n","%%EndProlog");

  fprintf(psfile,"%s\n","%%BeginSetup");
  fprintf(psfile,"0 setlinewidth\n");
  fprintf(psfile,"%s\n","%%EndSetup");
  fprintf(psfile,"%s\n","%%Page: 1 1");

  psplotbeta();
  psploteta();
  psplotlattice();

  fprintf(psfile,"%s\n","showpage");
  fprintf(psfile,"%s\n","%%Trailer");
  fprintf(psfile,"%s\n","end");
  fclose(psfile);
}
/***********************************/
void user_key(int i, char *code)
{
  double xmin, xmax;

  if(code[0]=='r')
    {
      xmin = smin;        /* preserve the current limits */
      xmax = smax;
      reread();           /* read in the data again */
      smin = xmin;        /* put back the limits */
      smax = xmax;
      erase();
      user_refresh(0,0,0,0);
    }
  if(code[0]=='u')
    {
      tell_unselect();
      selected_lattice = -1;
      selected_element = -1;
      selected_optic = -1;
      plot_lattice();
    }
  if(code[0]=='a')
    {
      smin = smin0;
      smax = smax0;
      nstart = 0;
      nend = numtwiss;
      erase();
      user_refresh(0,0,0,0);
    }
  if(code[0]=='h')
    {
      printf("plot_twiss: The following keystrokes are recognized:\n");
      printf("    a: zoom out to full scale (same a button 3)\n");
      printf("    h: type this message\n");
      printf("    p: generate a PostScript plot to file 'plot_twiss.ps'\n");
      printf("    q: quit\n");
      printf("    r: refresh the plot (rereads the data)\n");
/*      printf("    u: unselect an element if it was selected\n"); */
    }      
  if(code[0]=='p')
    {
      psoutput();
    }
}
/**********************************/
int main(int argc, char** argv)
{
#ifdef GLISH
  XWaldoGlishEventLoop(argc ,argv);
#else
  XWaldoEventLoop(argc ,argv);
#endif
  return 0;
}

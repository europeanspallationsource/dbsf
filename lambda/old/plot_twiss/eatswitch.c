/* $Header: $ */
/* This is a function to look for a cmdline switch and remove it from the
** argument list.  It returns a 1 if the switch was there or a 0 if not.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*======================*/
int eatswitch(int *argc, char** argv, char* swtch)
{
  int i, j;

  if(*argc>1)
    {
      for(i=1;i<*argc;i++)
	{
	  if(strcmp(argv[i],swtch)==0)
	    {
	      for(j=i;j<*argc-1;j++)
		{
		  argv[j] = argv[j+1];
		}
	      (*argc)--;
	      return 1;
	    }
	}
    }
  return 0;
}
/*=======================*/
/*=======================*/
/* The following code is for testing the function.
#ifdef debuggery
/*=======================*/
int main(int argc, char** argv)
{
  int i;

  printf("eatswitch('-ooga') = %d\n", eatswitch(&argc, argv, "-ooga"));

  for(i=0;i<argc;i++)
    {
      printf("%2d: %s\n", i, argv[i]);
    }
  return 0;
}
#endif

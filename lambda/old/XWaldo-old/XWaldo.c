/* $Header: /rap/lambda/XWaldo/RCS/XWaldo.c,v 1.5 1994/10/05 13:46:42 mackay Exp $ */
char string[10];  /* For debugging purposes only. */
/* #define WHOBEI 1 */

/* Do we want to use the Xtoolkit version ?  If not zap the following line */
/* #define USE_XT 1 */   /* can use the -DUSE_XT switch to gcc */
/* USE_XT and GLISH are defined in include/XWaldo.h except for the Amiga */

/*-------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <XWaldo.h>

int read(int fd, char *buf, int n);

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA     /*==========================*/

#define     INTUI_V36_NAMES_ONLY 1        /* don't load obsolete names */
#include    <exec/types.h>
#include    <exec/memory.h>
#include    <intuition/intuitionbase.h>
#include    <graphics/gfxmacros.h>
#include    <graphics/displayinfo.h>
#include    <intuition/screens.h>

#include    <clib/dos_protos.h>
#include    <clib/exec_protos.h>
#include    <clib/graphics_protos.h>
#include    <clib/intuition_protos.h>
#include    <clib/layers_protos.h>

struct   Screen    *XW_display    = NULL;
struct   RastPort  *XW_rp         = NULL;
struct   Window    *XW_window     = NULL;
struct   Library   *IntuitionBase = NULL;
struct   GfxBase   *GfxBase       = NULL;
struct   Library   *LayersBase    = NULL;

struct   Region    *XW_clipregion = NULL;

#else   /*%%%%%%%%%%%%%%%%%%%%*/


#ifdef USE_XT
#include    <X11/Intrinsic.h>    /* Intrinsics Defs */
#include    <X11/StringDefs.h>  /* Standard String Defs */
#include    <X11/Xaw/Box.h> /* Athena Command Widget ??? */

#ifdef GLISH
#include <CWrap/client_wrap.h>
void MainEventLoop(XtAppContext app_context, Display *display, int gflag);
int gflag = 0;
#endif

#endif


#include    <X11/Xlib.h>
#include    <X11/Xutil.h>

Display     *XW_display    = NULL;
Window       XW_window;
Visual      *XW_visual;
GC           XW_gc;
XSizeHints   XW_hint;
XWMHints     XW_wmhint;
XFontStruct *XW_font_info;

char        *fontname = "6x13";

XEvent              XW_event;
KeySym              XW_key;

Screen  *XW_screen_ptr;
long     XW_screen;    /* This refers to an X screen number not an Amiga
			  Screen (see XW_display above). */

#define NCOLORS 8
char xcolorname[][NCOLORS] =
{
  "black", "white", "red", "green", "yellow", "cyan", "magenta", "orange3"
  };
XColor  XW_color[NCOLORS];
XColor  XW_exact[NCOLORS];
Colormap XW_cmap;


#endif            /*==========================*/
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

int  XW_clipped = 0;

unsigned long       XW_fgnd, XW_bgnd;

unsigned int        XW_width;
unsigned int        XW_height;
unsigned int        XW_border;
unsigned int        XW_depth;

int  XW_number_of_colors = 1;


/* Define some stuff global to these routines, but not the user routines. */
#define DEF_WIN_LEFT   0
#define DEF_WIN_TOP    0
#define DEF_WIN_WIDTH  400
#define DEF_WIN_HEIGHT 250

double txmin = 0.0;    /* virtual limits of plot region */
double txmax = 10.0;
double tymin = 0.0;
double tymax = 10.0;
long kwidth  = DEF_WIN_WIDTH;  /* actual width of the whole window */
long kheight = DEF_WIN_HEIGHT; /* actual height of the whole window */
long kxmin   = DEF_WIN_LEFT;   /* window limits of plot region */
long kxmax   = DEF_WIN_WIDTH;
long kymin   = DEF_WIN_HEIGHT;
long kymax   = DEF_WIN_TOP;
double dixbydx;        /* scaling factors between window & virtual coords */
double diybydy;

long kxbeam;           /* current ix coordinate of beam in window coords */
long kybeam;           /* current iy coordinate of beam in window coords */
double txbeam;         /* current x coordinate of beam in virtual coords */
double tybeam;         /* current y coordinate of beam in virtual coords */
double tximag;         /* unclipped virtual x coordinate of last motion */
double tyimag;         /* unclipped virtual y coordinate of last motion */


/* for debuggery */
#define CLIPBUG 1

#ifdef CLIPBUG
static long lowx  = 0;
static long highx = 0;
static long lowy  = 0;
static long highy = 0;
#endif

/*============================*/

/*==========================*/
void XWsetscale(void)
{
  dixbydx = (double)(kxmax - kxmin) / (txmax - txmin);
  diybydy = (double)(kymax - kymin) / (tymax - tymin);
}
/*==========================*/
void XWsetregion(long x0, long x1, long y0, long y1)
{
  kxmin = x0;
  kxmax = x1;
  kymax = y0;
  kymin = y1;
  XWsetscale();
}
/*==========================*/
void XWsetvirtual(double x0, double x1, double y0, double y1)
{
  txmin = x0;
  txmax = x1;
  tymin = y0;
  tymax = y1;
  XWsetscale();
}
/*==========================*/
void XWscale(double x, double y, long *ix, long *iy)
{
  *ix = kxmin + (x-txmin) * dixbydx;
  *iy = kymin + (y-tymin) * diybydy;
} /* XWscale */
/*==========================*/
void XWunscale(long ix, long iy, double *x, double *y)
{
  *x = txmin + (ix-kxmin) / dixbydx;
  *y = tymin + (iy-kymin) / diybydy;
}
/*=========================*/
void XWmovabs(long ix, long iy)
{
#ifdef _AMIGA
  Move(XW_rp,ix,iy);
#endif
  kxbeam = ix;
  kybeam = iy;
}
/*=========================*/
void XWdrwabs(long ix, long iy)
{
#ifdef _AMIGA
  Draw(XW_rp,ix,iy);
#else
  XDrawLine(XW_display,XW_window,XW_gc,kxbeam,kybeam,ix,iy);
#endif
  kxbeam = ix;
  kybeam = iy;


#ifdef CLIPBUG
  if(kxbeam < lowx) lowx = kxbeam;
  if(kybeam < lowy) lowy = kybeam;
  if(kxbeam > highx) highx = kxbeam;
  if(kybeam > highy) highy = kybeam;
  if(ix < lowx) lowx = ix;
  if(iy < lowy) lowy = iy;
  if(ix > highx) highx = ix;
  if(iy > highy) highy = iy;
#endif
  



}
/*==========================*/
void XWmovrel(long ix, long iy)
{
  kxbeam = kxbeam + ix;
  kybeam = kybeam + iy;
}
/*=========================*/
void XWdrwrel(long ix, long iy)
{
  XWdrwabs(ix+kxbeam,iy+kybeam);
}
/*=========================*/
void XWmovea(double x, double y)
{
  long ix, iy;

  XWscale(x,y,&ix,&iy);
  tximag = x;
  tyimag = y;
  XWmovabs(ix,iy);
}
/*=========================*/
/*=========================*/
void XWdrawa(double x, double y)
{
  long ix, iy;

  XWscale(x,y,&ix,&iy);
  XWdrwabs(ix,iy);

  tximag = x;
  tyimag = y;
}
/*=========================*/
void XWmover(double x, double y)
{
  XWmovea(tximag+x, tyimag+y);
}
/*==========================*/
void XWdrawr(double x,double y)
{
  XWdrawa(tximag+x, tyimag+y);
}
/*==========================*/
void XWlblabs(long ix, long iy, char *text, int ipos)
{
  int xl,xr,yb,yu;
  int dx, dy;

#ifdef _AMIGA
  struct TextExtent te;

  TextExtent(XW_rp,text,strlen(text),&te);
  xl = te.te_Extent.MinX;
  xr = te.te_Extent.MaxX;
  yu = te.te_Extent.MinY;
  yb = te.te_Extent.MaxY;
  
#else

  int direction, ascent, descent;
  XCharStruct overall;

  XTextExtents(XW_font_info,text,strlen(text),&direction,&ascent,&descent
	       ,&overall);
  xl = overall.lbearing;
  xr = overall.rbearing;
  yu = -overall.ascent;
  yb = overall.descent;

#endif

  switch (ipos)
    {
    case (1):
      dx = -xr;
      dy = -yu;
      break;

    case (2):
      dx = -(xl + xr)/2;
      dy = -yu;
      break;

    case (3):
      dx = xl;
      dy = -yu;
      break;

    case (4):
      dx = -xr;
      dy = -(yu + yb)/2;
      break;

    case (5):
      dx = -(xl + xr)/2;
      dy = -(yu + yb)/2;
      break;

    case (6):
      dx = -xl;
      dy = -(yu + yb)/2;
      break;

    case (7):
      dx = -xr;
      dy = -yb;
      break;

    case (8):
      dx = -(xl + xr)/2;
      dy = -yb;
      break;

    case (9):
      dx = -xl;
      dy = -yb;
      break;
    }

#ifdef _AMIGA
  XWmovabs(ix+dx,iy+dy);
  Text(XW_rp,text,strlen(text));
#else
  XDrawString(XW_display,XW_window,XW_gc,ix+dx,iy+dy,text,strlen(text));
#endif
  XWmovabs(ix,iy);
}
/*==========================*/
void XWlblrel(long ix, long iy, char *text, int ipos)
{
  XWlblabs(ix+kxbeam,iy+kxbeam,text,ipos);
}
/*==========================*/
void XWlabela(double x, double y, char *text, int ipos)
{
  long ix, iy;

  XWscale(x,y,&ix,&iy);
  XWlblabs(ix,iy,text,ipos);
}
/*==========================*/
void XWlabelr(double x, double y, char *text, int ipos)
{
  XWlabela(tximag+x,tyimag+y,text,ipos);
}
/*==========================*/

void XWlinea(double x0, double y0, double x1, double y1)
{
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
  XWmovea(x0,y0);
  XWdrawa(x1,y1);
#else    /*%%%%%%%%%%%%*/
  long ix0 = 0;
  long iy0 = 0;
  long ix1 = 0;
  long iy1 = 0;

  XWscale(x0,y0,&ix0,&iy0);
  XWscale(x1,y1,&ix1,&iy1);
  XDrawLine(XW_display,XW_window,XW_gc,ix0,iy0,ix1,iy1);
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
  }
/*================================*/
void XWfinish(void)
{
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
  if(XW_clipregion != NULL)
    {
      XW_clipregion = InstallClipRegion(XW_window->WLayer,NULL);
      if(XW_clipregion != NULL) DisposeRegion(XW_clipregion);
    }
  if(XW_window     != NULL) CloseWindow(XW_window);
  if(LayersBase    != NULL) CloseLibrary(LayersBase);
  if(GfxBase       != NULL) CloseLibrary((struct Library *)GfxBase);
  if(IntuitionBase != NULL) CloseLibrary((struct Library *)IntuitionBase);
#else       /*%%%%%%%%%%%%%%%*/
#ifdef USE_XT
#else
  XFreeGC(XW_display,XW_gc);
  XDestroyWindow(XW_display,XW_window);
  XCloseDisplay(XW_display);
#endif
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
}
/*=========================*/
void XW_bomb(char *errmess)
{
  fprintf(stderr,"%s\n",errmess);
  XWfinish();
  exit(0);
}
/*=========================*/
void XWerase(void)
{
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
  SetRast(XW_rp,0);
  RefreshWindowFrame(XW_window);
#else
  XClearWindow(XW_display,XW_window);
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
}
/*=========================*/
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
void XWunclipWindow(struct Window *win)
{
  struct Region     *old_region;

/* Remove any old region by installing a NULL region,
** then dispose of the old region if one was installed.
*/
  if (NULL != (old_region = InstallClipRegion(win->WLayer, NULL)))
    DisposeRegion(old_region);
}
#else
void XWunclipWindow(Window win)
{
}
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
/*=========================*/
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
void XWclipWindow(struct Window *win,
	     long minX, long minY, long maxX, long maxY)
{
/* see pp 723-724 of Amiga RKRM Libraries */
  struct Rectangle rect;

  rect.MinX = minX;   /* set up the limits for the clip */
  rect.MaxX = maxX;
  rect.MinY = minY;
  rect.MaxY = maxY;

  if(XW_clipregion!=NULL)
    {
      XWunclipWindow(XW_window); /* trash any old clip region */
    }

  XW_clipregion = NewRegion(); /* get new region and OR in the limits */

  if(XW_clipregion != NULL)
    {
      if(NULL == OrRectRegion(XW_clipregion,&rect))
	{
	  DisposeRegion(XW_clipregion);
	  XW_clipregion = NULL;
	}
    }
  InstallClipRegion(XW_window->WLayer, XW_clipregion);
}
#else   /*%%%%%%%%%%%%%%%%*/
void XWclipWindow(Window win,
			  long minX, long minY, long maxX, long maxY)
{
  XRectangle  rect[1];

  rect[0].x = minX;
  rect[0].y = minY;
  rect[0].width = maxX - minX;
  rect[0].height = maxY - minY;
  XSetClipRectangles(XW_display,XW_gc,(int) 0,(int) 0,rect,(int) 1,YSorted);
}
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
/*=========================*/
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
void XWclipWindowToBorders(struct Window *win)
{
  XWclipWindow(win,
	       win->BorderLeft,
	       win->BorderTop,
	       win->Width  - win->BorderRight  - 1,
	       win->Height - win->BorderBottom - 1);
}
#else   /*%%%%%%%%%%%%%%%%*/
void XWclipWindowToBorders(Window win)
{
  XSetClipMask(XW_display,XW_gc,None);
}
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
/*=========================*/
void XWsetclip(int on)
{
  if(on)
    {
      XWclipWindow(XW_window, kxmin, kymax, kxmax, kymin);
      XW_clipped = 1;
    } else {
      XWclipWindowToBorders(XW_window);
      XW_clipped = 0;
    }
}  
/*=========================*/
void XWboxregion(void)
{
  XWmovabs(kxmin,kymin);
  XWdrwabs(kxmin,kymax);
  XWdrwabs(kxmax,kymax);
  XWdrwabs(kxmax,kymin);
  XWdrwabs(kxmin,kymin);
}
/*=========================*/
void XWgetsomecolors(void)
{
#ifdef _AMIGA   /*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
/* Color #0 is the background color that to which we erase. */
/* All of the colors will be whatever has been defined for the WorkBench */
/* screen. */

  struct DrawInfo *screeninfo;

  screeninfo = GetScreenDrawInfo(XW_display);
  if(screeninfo==0)
    {
      fprintf(stderr,"XWgetsomecolors could not GetScreenDrawInfo\n");
      XW_number_of_colors = 1;
    } else {
      XW_number_of_colors = pow(2.0,(double)screeninfo->dri_Depth);
    }
  SetAPen(XW_rp,1);   /* start with color #1 */


#else           /*%%%%%%%%%%%%%*/

  int depth;
  int class;
  int i;

  depth = DefaultDepth(XW_display,XW_screen);

  XW_visual = DefaultVisual(XW_display,XW_screen);
  class = XW_visual->class;
  if(depth == 1)
    {
      
      return;  /* a monochrome screen */
    }
  if(class != PseudoColor)
    {
      return;  /* for now assume a monochrome screen */
    }

  XW_cmap = DefaultColormap(XW_display,XW_screen);

  for(i=0; i<NCOLORS; i++)
    {
      if(XAllocNamedColor(XW_display,XW_cmap,
			  xcolorname[i],&XW_color[i],&XW_exact[i])==0)
	 {
	   fprintf(stderr,"XWgetsomecolors: Could not allocate '%s'",
		   xcolorname[i]);
	 }
      XW_number_of_colors = i+1;
    }

#endif          /*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
}
/*========================*/
void XWsetpen(int pen)
{
  int i;

  if(pen>=0 && pen<XW_number_of_colors)
    {
      i = pen;
    } else {
      i = 1;
    }
#ifdef _AMIGA       /*%%%%%%%%%%%%%%%%%%%%%%%*/
  SetAPen(XW_rp,i);
#else               /*%%%%%%%%%*/
  XSetForeground(XW_display,XW_gc,XW_color[i].pixel);
#endif              /*%%%%%%%%%%%%%%%%%%%%%%%*/
}
/*=========================*/
void XWinit(int argc, char **argv)
{
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA  /* for Intuition version */
  static char windowtitle[] = "XWaldo";

  struct TagItem win_tags[]=
    {
      {WA_Left,  DEF_WIN_LEFT},
      {WA_Top,   DEF_WIN_TOP},
      {WA_Width, DEF_WIN_WIDTH},
      {WA_Height,DEF_WIN_HEIGHT},
      {WA_MinWidth, 20},
      {WA_MinHeight,20},
      {WA_MaxWidth, 640},        /* Perhaps this should not be defined */
      {WA_MaxHeight,400},        /* Perhaps this should not be defined */
      {WA_CloseGadget,TRUE},
      {WA_SizeGadget,TRUE},
      {WA_DragBar,TRUE},
      {WA_DepthGadget,TRUE},
      {WA_RMBTrap,TRUE},         /* Trap the right mouse (menu) button */
      {WA_Title,(long) windowtitle},
      {WA_IDCMP,IDCMP_CLOSEWINDOW | IDCMP_MOUSEBUTTONS |
	        IDCMP_VANILLAKEY | IDCMP_RAWKEY |
		IDCMP_REFRESHWINDOW },
      {TAG_DONE}
    };


  IntuitionBase = OpenLibrary("intuition.library",37);
  if(IntuitionBase==NULL)
    {
      XW_bomb("Could not open 'intuition.library' version>=37.\n");
    }

  GfxBase = (struct GfxBase *)OpenLibrary("graphics.library",37);
  if(GfxBase==NULL)
    {
      XW_bomb("Could not open 'graphics.library' version>=37.\n");
    }

  LayersBase = OpenLibrary("layers.library",37);
  if(LayersBase==NULL)
    {
      XW_bomb("Could not open 'layers.library' version>=37.\n");
    }

  if(XW_display = LockPubScreen("Workbench"))
    {
      XW_window = OpenWindowTagList(NULL,win_tags);
      UnlockPubScreen(NULL, XW_display);
    } else {
      XW_bomb("XWinit: Could not lock public screen.\n");
    }

  if(XW_window == 0)
    {
      XW_bomb("XWinit: Could not open window.\n");
    } else {
      XW_rp = XW_window->RPort;
    }
  XWclipWindowToBorders(XW_window);
  XW_clipped = 0;

  SetDrMd(XW_rp,JAM1); /* Fix text to only add to plot,i.e., ignore BgPen */

#else       /*%%%%%%%%%%%%%%%*/

/* initialization */    /*  straight Xlib verion:  We don't need no stinking */
                        /*  widgets! */
  XW_display = XOpenDisplay("");
  XW_screen  = DefaultScreen(XW_display);

/* default pixel values */
  XW_bgnd = BlackPixel(XW_display,XW_screen);
  XW_fgnd = WhitePixel(XW_display,XW_screen);

/* default program-specified window position and size */
  XW_hint.x = 200;
  XW_hint.y = 300;
  XW_hint.width = DEF_WIN_WIDTH;
  XW_hint.height = DEF_WIN_HEIGHT;
  XW_hint.flags = PPosition | PSize;
  XW_wmhint.flags = InputHint;
  XW_wmhint.input = True;

/* window creation */
  XW_window = XCreateSimpleWindow(XW_display,
				  DefaultRootWindow(XW_display),
				  XW_hint.x,XW_hint.y,XW_hint.width,
				  XW_hint.height,5,XW_fgnd,
				  XW_bgnd);

#ifdef WHOBEI
  fprintf(stderr,"XW_display=0x%x\n",XW_display);
  fprintf(stderr,"XW_window =0x%x\n",XW_window);
#endif

  XSetStandardProperties(XW_display,XW_window,argv[0],argv[0]
			 ,None,argv,argc,&XW_hint);
  XSetWMHints(XW_display,XW_window,&XW_wmhint);

/* Get the font information */
  if((XW_font_info = XLoadQueryFont(XW_display,"fixed")) == NULL)
    {
      fprintf(stderr,"XWinit: could not find font '%s'.\n",fontname);
      exit(0);
    }

/* GC creation and initialization */
  XW_gc = XCreateGC(XW_display,XW_window,0,0);
  XSetBackground(XW_display,XW_gc,XW_bgnd);
  XSetForeground(XW_display,XW_gc,XW_fgnd);

/* set up the font */
  XSetFont(XW_display,XW_gc,XW_font_info->fid);

/* input event selection */
  XSelectInput(XW_display,XW_window
	       ,ButtonPressMask|KeyPressMask|ExposureMask);

/* window mapping */
  XMapRaised(XW_display,XW_window);

/* solicit both ButtonPress and ButtonRelease events */
/**  XSelectInput(XW_display,XW_window,
	       ButtonPressMask | ButtonReleaseMask);   ?? doesn't work **/

#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

/* set the scaling of the window */
  XWsetscale();

/* set up some colors */
  XWgetsomecolors();
}
/*=============================*/
int eatswitch(int *argc, char** argv, char* swtch)
{ /* This function scans the argument list for 'swtch' and removes it from
  ** the list.  It returns a 1 if 'swtch' was found, or a 0 if not.
  */

  int i, j;

  if(*argc>1)
    {
      for(i=1;i<*argc;i++)
	{
	  if(strcmp(argv[i],swtch)==0)
	    {
	      for(j=i;j<*argc-1;j++)
		{
		  argv[j] = argv[j+1];
		}
	      (*argc)--;
	      return 1;
	    }
	}
    }
  return 0;
}
/*============================*/
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
void main(int argc, char **argv)
{
  long i;
  char text[10];
  long done;   /* Flag for signaling the end of event loop */


  struct IntuiMessage *msg;
  unsigned long class;
  unsigned short code;
  short mousex, mousey;
  int xmup, ymup, xmdown, ymdown;
  int mup, mbutton;

/* Initialize the window. */
  XWinit(argc,argv);

  done = FALSE;
  mup=0;

  user_start(argc,argv);

  user_refresh((int) (XW_window->BorderLeft),
	       (int) (XW_window->Width        - XW_window->BorderRight),
	       (int) (XW_window->BorderTop),
	       (int) (XW_window->Height       - XW_window->BorderBottom));

  while(!done)
    {
      WaitPort(XW_window->UserPort);
      while((!done)
	    && (msg = (struct IntuiMessage *)GetMsg(XW_window->UserPort)))
	{
	  class = msg->Class;
	  code  = msg->Code;
	  mousex= msg->MouseX;
	  mousey= msg->MouseY;
	  switch (class)
	    {
	    case IDCMP_CLOSEWINDOW:
	      done = TRUE;
	      break;
	    case IDCMP_MOUSEBUTTONS:
	      if(code&IECODE_UP_PREFIX)
		{
		  xmup=mousex;
		  ymup=mousey;
		  mup=1;
		} else {
		  xmdown=mousex;
		  ymdown=mousey;
		  mup=0;
		}
	      switch (code&0x7f)
		{
		case IECODE_LBUTTON:
		  mbutton=1;
		  break;
		case IECODE_MBUTTON:
		  mbutton=2;
		  break;
		case IECODE_RBUTTON:
		  mbutton=3;
		  break;
		default:
		  mbutton=0;
		  break;
		}
	      if(mup)
		{

#ifdef TWOBUTTON
		  if(mbutton==3)
		    {
		      mbutton=2;
		    }
#endif

		  user_mouse(mbutton,xmdown,ymdown,xmup,ymup);
		}
	      break;
	    case IDCMP_REFRESHWINDOW:
	      if(XW_clipped==0)
		{
		  XWclipWindowToBorders(XW_window);
		}
	      XWerase();
	      user_refresh((int) (XW_window->BorderLeft),
			   (int) (XW_window->Width
				  - XW_window->BorderRight),
			   (int) (XW_window->BorderTop),
			   (int) (XW_window->Height
				  - XW_window->BorderBottom));

	      break;
	    case IDCMP_RAWKEY:
	      break;
	    case IDCMP_VANILLAKEY:
	      if(code=='q')
		{
		  done=1;
		} else {
		  text[0] = (char) code;
		  text[1] = (char) 0;
		  i = 1;
		  user_key(i, text);
		}
	      break;
	    }
	  ReplyMsg((struct Message *)msg);
	}
    }      
  XWfinish();
}
/*==================*/
#else    /*%%%%%%%%%%%%%%%%%%*/
/*==================*/
#ifndef USE_XT
void main(int argc, char **argv)
{

  long i;
  char text[10];
  long done;

  Window XW_root;
  int XW_locx,XW_locy;
  int xmup, ymup, xmdown, ymdown;
  int mbutton;

  Status status;

  char name[] = "XWaldo";

/* initialize the window */
  XWinit(argc,argv);

/* main event-reading loop */
  done = 0;

  user_start(argc,argv);

  while(done==0) {
/* read the next event */
    XNextEvent(XW_display,&XW_event);
    switch(XW_event.type) {

    case Expose:		/* repaint on expose window */
      if(XW_event.xexpose.count==0) {
	status = XGetGeometry(XW_display,XW_window,&XW_root,
			      &XW_locx,&XW_locy,
			      &XW_width,&XW_height,
			      &XW_border,&XW_depth);
	if(status){
	  XWsetregion(0,XW_width,0,XW_height);
	  user_refresh((int) 0, XW_width, (int) 0, XW_height);
	}
      }
     break;

    case MappingNotify:		/* process keyboard mapping changes */
      XRefreshKeyboardMapping((XMappingEvent *) &XW_event);
      break;

    case ButtonPress:		/* process mouse-button presses */
      xmdown = XW_event.xbutton.x;
      ymdown = XW_event.xbutton.y;

/* Note there is a problem here */

    case ButtonRelease:		/* process mouse-button releases */
      xmup = XW_event.xbutton.x;
      ymup = XW_event.xbutton.y;
      switch (XW_event.xbutton.button)
	{
	case Button1:
	  mbutton = 1;
	  break;

	case Button2:
	  mbutton = 2;
	  break;

	case Button3:
	  mbutton = 3;
	  break;

	case Button4:
	  mbutton = 4;
	  break;

	case Button5:
	  mbutton = 5;
	  break;

	default:
	  mbutton = 0;
	}

      user_mouse(mbutton, xmdown, ymdown, xmup, ymup);
      break;

    case KeyPress:		/* process keyboard input */
      i = XLookupString((XKeyEvent *)&XW_event,text,10,&XW_key,0);
      if(i==1&&text[0]=='q'){
	done = 1;
      } else {
	user_key(i,text);
      }
      break;

    } /* switch(XW_event.type) */

  } /* while (done==0) */

/* termination */
  XWfinish();
  exit(0);
}

#else  /*% % % % % % % % % % % % %*/

        /* for Xt version */  /* Glish needs the damn widgets */
        /* What can I say?  X is rather stupid in not having a simple */
        /* stdin XEvent type, ay what? */
/*============================*/
void Mouse1_Proc(Widget w, XButtonEvent *event,
		String *params, Cardinal num_params)
{
  int x,y;

  x = event->x;
  y = event->y;

  user_mouse((int)1,x,y,x,y);
}
/*============================*/
void Mouse2_Proc(Widget w, XButtonEvent *event,
		String *params, Cardinal num_params)
{
  int x,y;

  x = event->x;
  y = event->y;

  user_mouse((int)2,x,y,x,y);
}
/*============================*/
void Mouse3_Proc(Widget w, XButtonEvent *event,
		String *params, Cardinal num_params)
{
  int x,y;

  x = event->x;
  y = event->y;

  user_mouse((int)3,x,y,x,y);
}
/*============================*/
void Quit(Widget w, XKeyEvent *event,
	      String *params, Cardinal num_params)
{
  printf("quit\n");
#ifdef GLISH
  client_post_event_string("done","");
#endif
  exit(0);
}
/*============================*/
void Key_Proc(Widget w, XKeyEvent *event,
	      String *params, Cardinal num_params)
{
  int i;
  char text[10];

  i = XLookupString(event,text,10,&XW_key,0);
  user_key(i,text);
}
/*============================*/
void Expose_Proc(Widget w, XExposeEvent *event,
		 String *params, Cardinal num_params)
{
  Status status;
  Window XW_root;
  int XW_locx,XW_locy;

  if(event->count==0)
    {
      status = XGetGeometry(XtDisplay(w),XtWindow(w),&XW_root,
			    &XW_locx,&XW_locy,
			    &XW_width,&XW_height,
			    &XW_border,&XW_depth);

      if(status)
	{
	  XWsetregion(0,XW_width,0,XW_height);
	  XWerase();
	  user_refresh((int)0,XW_width,(int)0,XW_height);
	}
    }
}
/*============================*/
void Stdin_Proc(XtPointer client_data, int *fid, XtInputId *id)
{
  char buf[BUFSIZ];
  int nbytes;

  if((nbytes = read(*fid, buf, BUFSIZ)) == -1)
    fprintf(stderr,"Stdin_Proc: 'read' error\n");
  if(nbytes>0)
    {
      if(buf[0]=='q')exit(0);
      user_key(nbytes,buf);
    }
}
/*============================*/
void XWflush(void)
{
  XFlush(XW_display);
}
/*============================*/
void main(int argc, char **argv)
{
  XtAppContext app_context;
  Widget topLevel, plot_window;
  static XtActionsRec stufftodo[] =
    {
      {"zoomleft",  Mouse1_Proc},
      {"zoomright", Mouse2_Proc},
      {"unzoom",    Mouse3_Proc},
      {"expose",    Expose_Proc},
      {"quit",      Quit},
      {"key",       Key_Proc}
    };

#ifdef QUACK
  String trans = "#augment\n\
     <Expose>:            expose()\n\
     <Configure>:         expose()\n\
     <Key>q:              quit()\n\
     <Key>:               key()";  /*\n\
     <Btn1Down>,<Btn1Up>: zoomleft()\n\
     <Btn2Down>,<Btn2Up>: zoomright()\n\
     <Btn3Down>,<Btn3Up>: unzoom()"; */
#else
  String trans = "#augment\n\
     <Expose>:            expose()\n\
     <Configure>:         expose()\n\
     <Key>q:              quit()\n\
     <Key>:               key()\n\
     <Btn1Down>,<Btn1Up>: zoomleft()\n\
     <Btn2Down>,<Btn2Up>: zoomright()\n\
     <Btn3Down>,<Btn3Up>: unzoom()";
#endif






  topLevel = XtVaAppInitialize
    (&app_context,  /* Application Context */
     "XWaldo",      /* Application Class   */
     NULL, 0,       /* command line option list */
     &argc, argv,   /* command line args */
     NULL,          /* for missing app-defaults file */
     NULL);         /* terminate varargs list */



  plot_window = XtVaCreateManagedWidget
    ("plotwindow",  /* widget name */
     boxWidgetClass,/* widget class from */
     topLevel,      /* parent widget */
/*     XtNtranslations, XtParseTranslationTable(trans), */
     NULL);         /* terminate varargs list */

  XtAugmentTranslations(plot_window,XtParseTranslationTable(trans));
  


  XtAppAddActions(app_context, stufftodo, XtNumber(stufftodo));

  XtRealizeWidget(topLevel);

  XW_display    = XtDisplay(plot_window);
  XW_window     = XtWindow(plot_window);
  XW_screen_ptr = XtScreen(plot_window);
  XW_screen     = XScreenNumberOfScreen(XW_screen_ptr);

  XW_gc      = XCreateGC(XW_display, XW_window, 0,0); /* mask, &values); */
  XW_bgnd = BlackPixel(XW_display,XW_screen);
  XW_fgnd = WhitePixel(XW_display,XW_screen);

  XSetWindowBackground(XW_display,XW_window,XW_bgnd);

  XSetBackground(XW_display,XW_gc,XW_bgnd);
  XSetForeground(XW_display,XW_gc,XW_fgnd);

  /* set up the font */
  if((XW_font_info = XLoadQueryFont(XW_display,fontname)) == NULL)
    {
      fprintf(stderr,"main: Could not find font '%s'.\n",fontname);
      exit(0);
    }
  XSetFont(XW_display,XW_gc,XW_font_info->fid);

  XWsetscale();          /* set the scaling of the window */
  XWgetsomecolors();     /* set up some colors */

  user_start(argc,argv);

#ifdef GLISH
  MainEventLoop(app_context, XW_display, gflag);
#else
  XtAppAddInput(app_context, fileno(stdin), XtInputReadMask, Stdin_Proc, NULL);
  XtAppMainLoop(app_context);
#endif
}
#endif
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

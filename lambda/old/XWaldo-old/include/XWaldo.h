/* $Header: /rap/lambda/XWaldo/include/RCS/XWaldo.h,v 1.4 1994/10/05 13:46:42 mackay Exp $ */
#ifndef _AMIGA
#define USE_XT 1
#define GLISH 1
#endif

extern  void XWinit(int argc,char **argv);
extern  void XWfinish(void);
extern  void XWflush(void);

extern  void XWsetregion(long x0, long x1, long y0, long y1);
extern  void XWsetvirtual(double x0, double y0, double x1, double y1);
extern  void XWsetclip(int on);
extern  void XWscale(double x, double y, long *ix, long *iy);
extern  void XWunscale(long ix, long iy, double *x, double *y);

extern  void XWboxregion(void);
extern  void XWerase(void);
extern  void XWsetpen(int pen);

extern  void XWmovabs(long ix, long iy);
extern  void XWmovrel(long ix, long iy);
extern  void XWmovea(double x, double y);
extern  void XWmover(double x, double y);

extern  void XWdrwabs(long ix, long iy);
extern  void XWdrwrel(long ix, long iy);
extern  void XWdrawa(double x, double y);
extern  void XWdrawr(double x, double y);

/* In the following the integer "pos" takes values from           789   */
/* 1 to 9 which indicate the location of the text relative        456   */
/* to (ix,iy).  A 5 means center the text, whereas a 3 means      123   */
/* align the text horizontally with (ix,iy), but place it below         */
/* the point.  The numbers to the right indicate the relative positions.*/

extern  void XWlblabs(long ix, long iy, char *text, int pos);
extern  void XWlblrel(long ix, long iy, char *text, int pos);
extern  void XWlabela(double x, double y, char *text, int pos);
extern  void XWlabelr(double x, double y, char *text, int pos);

extern  void XWlinea(double x0, double y0, double x1, double y1);

#define movabs(ix,iy) XWmovabs(ix,iy)
#define movrel(ix,iy) XWmovrel(ix,iy)
#define movea(x,y) XWmovea(x,y)
#define mover(x,y) XWmover(x,y)
#define drwabs(ix,iy) XWdrwabs(ix,iy)
#define drwrel(ix,iy) XWdrwrel(ix,iy)
#define drawa(x,y) XWdrawa(x,y)
#define drawr(x,y) XWdrawr(x,y)
#define lblabs(x,y,text,ipos) XWlblabs(x,y,text,ipos)
#define lblrel(x,y,text,ipos) XWlblrel(x,y,text,ipos)
#define labela(x,y,text,ipos) XWlabela(x,y,text,ipos)
#define labelr(x,y,text,ipos) XWlabelr(x,y,text,ipos)
#define setregion(ix0,ix1,iy0,iy1) XWsetregion(ix0,ix1,iy0,iy1)
#define setvirtual(ix0,ix1,iy0,iy1) XWsetvirtual(ix0,ix1,iy0,iy1)
#define scale(x,y,ix,iy) XWscale(x,y,ix,iy)
#define unscale(ix,iy,x,y) XWunscale(ix,iy,x,y)
#define erase XWerase
#define clip(on) XWsetclip(on)
#define boxregion XWboxregion
#define setpen(pen) XWsetpen(pen)


/*  HERE are the USER supplied function prototypes */

extern void user_start(int argc, char **argv);
extern void user_mouse(int mbutton, int xmdown,int ymdown,int xmup,int ymup);
extern void user_refresh(int left, int right, int bottom, int top);
extern void user_key(int i, char *code);

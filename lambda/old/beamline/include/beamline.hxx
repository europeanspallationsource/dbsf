/*************************************************************************
**************************************************************************
**************************************************************************
******                                                                ****
******  BEAMLINE:  C++ objects for design and analysis  .             ****
******             of beamlines, storage rings, and                   ****
******             synchrotrons.                                      ****
******                                                                ****
******  Copyright (c) 1991  Universities Research Association, Inc.   ****
******                All Rights Reserved                             ****
******                                                                ****
******  Author:    Leo Michelotti                                     ****
******                                                                ****
******             Fermilab                                           ****
******             P.O.Box 500                                        ****
******             Mail Stop 345                                      ****
******             Batavia, IL   60510                                ****
******                                                                ****
******             Phone: (708) 840 4956                              ****
******             Email: michelot@hazel.fnal.gov                     ****
******                    michelotti@adcalc.fnal.gov                  ****
******                    almond::michelotti                          ****
******                                                                ****
******  Release    Version 1.0 : << not released >>                   ****
******  Dates:                                                        ****
******                                                                ****
**************************************************************************
**************************************************************************
******                                                                ****
******  This material resulted from work developed under a            ****
******  Government Contract and is subject to the following           ****
******  license: The Government retains a paid-up, nonexclusive,      ****
******  irrevocable worldwide license to reproduce, prepare           ****
******  derivative works, perform publicly and display publicly       ****
******  by or for the Government, including the right to distribute   ****
******  to other Government contractors.  Neither the United          ****
******  States nor the United States Department of Energy, nor        ****
******  any of their employees, makes any warranty, express or        ****
******  implied, or assumes any legal liability or responsibility     ****
******  for the accuracy, completeness or usefulness of any           ****
******  information, apparatus, product, or process disclosed, or     ****
******  represents that its use would not infringe privately owned    ****
******  rights.                                                       ****
******                                                                ****
******  These files are made avaliable for use by specific            ****
******  individuals or a specific group.  They are not to be          ****
******  reproduced or redistributed.                                  ****
******                                                                ****
******  These files are provided "as is" with no warranties of any    ****
******  kind including the warranties of design,                      ****
******  merchantibility and fitness for a particular purpose,         ****
******  or arising from a course of dealing, usage or trade           ****
******  practice.                                                     ****
******                                                                ****
******  These files are provided with no support or obligation on     ****
******  the part of U.R.A. or Fermilab to assist in their use,        ****
******  correction, modification or enhancement.                      ****
******                                                                ****
******  Neither U.R.A. nor Fermilab shall be liable with              ****
******  respect to the infringement of copyrights, trade              ****
******  secrets or any patents by these files or any part             ****
******  thereof.                                                      ****
******                                                                ****
******  In no event will U.R.A., Fermilab, or the author(s) be        ****
******  liable for any lost revenue or profits or other               ****
******  special, indirect and consequential damages, even if          ****
******  they have been advised of the possibility of such             ****
******  damages.                                                      ****
******                                                                ****
**************************************************************************
*************************************************************************/

#ifndef BEAMLINE_HXX
#define BEAMLINE_HXX

#include <complex.h>
#include <stdio.h>
#include "mxyzptlk.hxx"  // Differential algebra functionality
#include "slist.hxx"
#include "threeVector.hxx"

// .......................... Defined parameters
const int BMLN_dynDim       = 6;

enum  BMLN_formatType { flatFormat,
                        madFormat,
                        synchFormat,
                        transportFormat };

enum  BMLN_elType     { other,
                        bmln,       // Type beamline
                        drft,       //      drift
                        sctr,       //      sector
                        rbnd,       //      rbend
                        sbnd,       //      sbend
                        qd,         //      quadrupole
                        sext,       //      sextupole
                        octp,       //      octupole
                        thnMlt,     //      thinMultipole
                        thnOct,     //      thinOctupole
                        ptch,       //      patch
                        thnQd,      //      thinQuad
                        thnSx,      //      thinSextupole
                        rfcvt,      //      rfcavity
                        mrkr,       //      marker
                        mntr,       //      monitor
                        hmntr,      //      hmonitor
                        vmntr,      //      vmonitor
                        hkck,       //      hkick
                        vkck,       //      vkick
                        srt,        //      srot
                        JetthnQd,   //      JetthinQuad
                        JetQuad,    //      JetQuadrupole
                        offQuad     //      OffsetQuadrupole
                      };

struct BMLN_posInfo {
    threeVector inPoint;     // This struct defines the upstream and
    threeVector inAxes[3];   // downstream faces of the magnet's slot.
    threeVector outPoint;
    threeVector outAxes[3];
    BMLN_posInfo( );
    BMLN_posInfo( BMLN_posInfo& );
    BMLN_posInfo& operator=( const BMLN_posInfo& );   // ??? Does this struct 
                                                // ??? need a destructor?
    friend ostream& operator<<(ostream&, BMLN_posInfo&);

    } ;

// .............................. Particle classes
class Proton {
private:
  friend   class bmlnElmnt;
  friend   class beamline;
  friend   class drift;
  friend   class hkick;
  friend   class hmonitor;
  friend   class marker;
  friend   class monitor;
  friend   class octupole;
  friend   class quadrupole;
  friend   class rbend;
  friend   class rfcavity;
  friend   class sbend;
  friend   class sector;
  friend   class sextupole;
  friend   class srot;
  friend   class patch;
  friend   class thinQuad;
  friend   class thinSextupole;
  friend   class thinOctupole;
  friend   class thinDecapole;
  friend   class thinMultipole;
  friend   class vkick;
  friend   class vmonitor;
  friend   class JetthinQuad;
  friend   class JetQuadrupole;
  friend   class offsetQuadrupole;
  double   state [ BMLN_dynDim ];
  double   energy;    // [eV]
  double   momentum;  // [eV / c ]
  double   mass;      // [eV * c^2 ]
public:
  Proton();
  Proton( double /* energy [eV] */ );
  Proton( double /* energy [eV] */, double* /* state */ );
  ~Proton();
  void setState( double* );
  void getState( double* );
};

class JetProton {
private:
  friend   class bmlnElmnt;
  friend   class beamline;
  friend   class drift;
  friend   class hkick;
  friend   class hmonitor;
  friend   class marker;
  friend   class monitor;
  friend   class octupole;
  friend   class quadrupole;
  friend   class rbend;
  friend   class rfcavity;
  friend   class sbend;
  friend   class sector;
  friend   class sextupole;
  friend   class srot;
  friend   class patch;
  friend   class thinQuad;
  friend   class thinSextupole;
  friend   class thinOctupole;
  friend   class thinDecapole;
  friend   class thinMultipole;
  friend   class vkick;
  friend   class vmonitor;
  friend   class JetthinQuad;
  friend   class JetQuadrupole;
  friend   class offsetQuadrupole;
  LieOperator state;
public:
  JetProton();
  ~JetProton();
  void setState( double* );
  void setState( LieOperator& );
  void getState( LieOperator& );
  void getState( Jet* );
};


class ProtonBunch : public slist
{
public:
  ProtonBunch();
  ProtonBunch( int, int, char, double, double );
  ~ProtonBunch();
};


// ................................ Beamline classes

struct lattFunc {
  double arcLength;
  struct {
    double hor;
    double ver;
  } tune;
  struct {
    double hor;
    double ver;
  } beta;
  struct {
    double hor;
    double ver;
  } alpha;
  struct {
    double hor;
    double ver;
  } psi;
  double transMat [ BMLN_dynDim ] [ BMLN_dynDim ] ;
} ;

class circuit;     // Put here so bmlnElmnt can refer to circuit.

class bmlnElmnt; 
struct bmlnElmntData {
  BMLN_elType     type;
  bmlnElmnt*      address;
  char*           name;
  int             depth;
  double          length;
  double          strength;
  double          xOffset;
  double          yOffset;
  double          tilt;
  double          aperture;
  BMLN_posInfo    geometry;
  char            more;

  bmlnElmntData();
  bmlnElmntData( bmlnElmntData& );
  virtual ~bmlnElmntData();
  virtual void eliminate();
  virtual void* clone();

  virtual void writeTo( FILE* );
  // ??? REMOVE virtual void readFrom( FILE* );
};


class bmlnElmnt
{
protected:
  char*        ident;      // Name identifier of the element.
  BMLN_elType  whatItIs;   // Type of element.
  double       length;     // Length of object [ meters ]
  double       strength;   // Interpretation depends on object.
  double       baseStrength; // Strength set by declaration or
                           //   by .setStrength
  int          depth;      // Depth of objects below this element.
  double       xOffset;    // Keep the magnet offset here
  double       yOffset;
  double       tilt;
  double       aperture;
  struct {
    double   iToB;         // Conversion factor for current through
                           // magnet in amperes to field at 1 inch
    circuit* crctPtr;      // Pointer to circuit attached to magnet.
    } field;
  BMLN_posInfo geometry;

  void setCurrent( double );

  virtual void image( int,           // depth of image
                      slist*,        // list to append responses to
                      BMLN_posInfo*  // current geometry, updated
                    );
  virtual void peekAt();
  virtual void geomToEnd   ( BMLN_posInfo& );
  virtual void geomToStart ( BMLN_posInfo& );
  virtual void eliminate();

  friend class beamline;
  friend class circuit;
  friend class patch;
  friend beamline& operator-( beamline& );

public:
  bmlnElmnt();
  bmlnElmnt( char*      n /* name     */
           );
  bmlnElmnt( double     l /* length   */   
           );
  bmlnElmnt( double     l /* length   */, 
             double     s /* strength */   
           );
  bmlnElmnt( char*      n /* name     */, 
             double     l /* length   */   
           );
  bmlnElmnt( char*      n /* name     */, 
             double     l /* length   */, 
             double     s /* strength */   
           );
  bmlnElmnt( bmlnElmnt& a );
  bmlnElmnt( bmlnElmntData& );
  virtual ~bmlnElmnt();

  void*        dataHook;   // Carries data as service to application program.
  bmlnElmnt& operator=( const bmlnElmnt& );
  void propagate( ProtonBunch& );  // ??? Needs to be virtual???
  virtual void propagate( Proton& );
  virtual void propagate( JetProton& );
  bmlnElmntData* image();
  void image( bmlnElmntData* );
  // ??? REMOVE virtual complex intrDepolRes( int /* resonance number */ );
  void set( bmlnElmntData& );
  void setStrength( double );

  virtual void transRel( double, double );
  virtual void transOffset( double, double );
  virtual void setTilt(double);
  virtual void setAperture(double);
} ;


class hkick : public bmlnElmnt
{
private:
  void image( int, slist*, BMLN_posInfo* );
public:
  hkick(        /* Assumes zero kick */ );
  hkick( double /* kick size in radians */ );
  hkick( char * /* name */  /* Assumes zero kick */ );
  hkick( char * /* name */, double /* kick size in radians */ );
  hkick( hkick& );
  hkick( bmlnElmntData& );
  ~hkick();
  void propagate( Proton& );
  void propagate( JetProton& );
  // ??? REMOVE complex intrDepolRes( int );
};


class octupole : public bmlnElmnt
{
private:
  void image( int, slist*, BMLN_posInfo* );
public:
  octupole();
  octupole( char *  /* name */ );
  octupole( double, /* length   */
            double  /* strength */ );
  octupole( char*,  /* name     */
            double, /* length   */
            double  /* strength */ );
  octupole( double /* length */ );
  octupole( char * /* name */, double /* length */ );
  octupole( octupole& );
  octupole( bmlnElmntData& );
  ~octupole();
  void propagate( Proton& );
  void propagate( JetProton& );
  // ??? REMOVE complex intrDepolRes( int );
};


// 
// *** class thinRFCavity written by Jian Ping Shan
// *** April 4, 1994 
//

class rfcavity : public bmlnElmnt
{
private:
  double w_rf;			// RF frequency [MHz]  // ??? What??
  double phi_s;                 // synchronous phase
  double sin_phi_s;             // sine of synchronous phase
  double eV;                    // max energy gain per turn [eV]
  double Q;                     // quality factor
  double R;                     // shunt impedance
public:
  rfcavity( double,   // RF frequency [MHz]
            double,   // max energy gain per turn [eV]
            double,   // synchronous phase [radians]
	    double,   // Q 
	    double    // R shunt impedance 
            );
  rfcavity( char *,   // Name
	    double,   // RF frequency [MHz]
            double,   // max energy gain per turn [eV]
            double,   // synchronous phase [radians]
	    double,   // Q 
	    double    // R shunt impedance 
            );
  rfcavity( rfcavity& );
  rfcavity( bmlnElmntData& );
  ~rfcavity();

  void rfcavity::propagate( Proton& );
  void rfcavity::propagate( JetProton& );
};


class srot : public bmlnElmnt
{
private:
  void image( int, slist*, BMLN_posInfo* );
public:
  srot();
  srot( double /* length */ );
  srot( char * /* name */ );
  srot( char * /* name */, double /* length */ );
  srot( srot& );
  srot( bmlnElmntData& );
  ~srot();

  void propagate( Proton& );
  void propagate( JetProton& );
};


class vkick : public bmlnElmnt
{
private:
  void image( int, slist*, BMLN_posInfo* );
public:
  vkick(        /* Assumes zero kick */ );
  vkick( double /* kick size in radians */ );
  vkick( char * /* name */ /* Assumes zero kick */ );
  vkick( char * /* name */, double /* kick size in radians */ );
  vkick( vkick& );
  vkick( bmlnElmntData& );
  ~vkick();
  void propagate( Proton& );
  void propagate( JetProton& );
  // ??? REMOVE complex intrDepolRes( int );
};


struct monitorData;

class monitor : public bmlnElmnt
{
protected:
  FILE*    outputFile;
  void     image( int, slist*, BMLN_posInfo* );
  char     onOff;
  double*  rgr;
  double*  xData;
  double*  yData;
  int   npts;

public:
  monitor();                   // Data to be written to standard output
  monitor( char* );            // Name identifier.
  monitor( FILE* );            // Data written to file.
  monitor( char*, FILE* );     //                      ... and name.
  monitor( bmlnElmntData& );
  ~monitor();
  // ---------------------
  void setOutputFile( FILE* );
  void on();
  void off();
  void setBPMData(double*,double*,int);
  double getXData(int);
  double getYData(int);
  int getNbrPoints();
  double operator[]( int );    // Readout of data
  // ??? REMOVE complex intrDepolRes( int ); // ??? Get rid of these!!!
  // ---------------------
  void propagate( Proton&   );
  void propagate( JetProton& );
} ;

class hmonitor : public monitor
{
public:
  hmonitor();                   // Data to be written to standard output
  hmonitor( char* );            // Name identifier.
  hmonitor( FILE* );            // Data written to file.
  hmonitor( char*, FILE* );     // Data written to file ... and name.
  hmonitor( bmlnElmntData& );
  ~hmonitor();
  double operator[]( int );    // Readout of data
  void propagate( Proton&   );
  void propagate( JetProton& );
} ;
struct monitorData : public bmlnElmntData {
  double* xData;   
  double* yData;      
  int     npts;

  monitorData();

  monitorData( monitorData& );
  ~monitorData();
  void eliminate();
  void* clone();

  void writeTo( FILE* );
  // ??? REMOVE void readFrom( FILE* );
};


class vmonitor : public monitor
{
public:
  vmonitor();                   // Data to be written to standard output
  vmonitor( char* );            // Name identifier.
  vmonitor( FILE* );            // Data written to file.
  vmonitor( char*, FILE* );     // Data written to file ... and name.
  vmonitor( bmlnElmntData& );
  ~vmonitor();
  double operator[]( int );    // Readout of data
  void propagate( Proton&   );
  void propagate( JetProton& );
} ;

class marker : public bmlnElmnt
{
private:
  void image( int, slist*, BMLN_posInfo* );
public:
  marker();                   // Data to be written to standard output
  marker( char* );            // Name identifier.
  marker( marker& );
  marker( bmlnElmntData& );
  ~marker();

  void propagate( Proton&   );
  void propagate( JetProton& );
  // ??? REMOVE complex intrDepolRes( int );
} ;

class drift : public bmlnElmnt
{
private:
  void image( int, slist*, BMLN_posInfo* );
public:
  drift();
  drift( double );             // length of drift in meters
  drift( char* );              // name
  drift( char*,                // name
         double );             // length of drift in meters
  drift( bmlnElmntData& );
  ~drift();
  // ??? REMOVE void propagate( Proton&   ); 
  // ??? REMOVE void propagate( JetProton& );
  // ??? REMOVE complex intrDepolRes( int );
} ;


  struct rbendData;
class rbend : public bmlnElmnt
{
private:
  double rho;        // bend radius [ meters ]
  double K1;         // B' / B rho  [ meters^-2 ]  Error field.
                     //   Notation is made compatible with MAD.
  void image( int, slist*, BMLN_posInfo* );
public:
  rbend( double,     // bend angle  [ radians ]
         double );   // length      [ meters ]

  rbend( double,     // bend angle  [ radians ]
         double,     // length      [ meters ]
         double );   // B' / B rho  [ meters^-2 ]  Error field.
                     //   Notation is made compatible with MAD.
  rbend( char*,      // name
         double,     // bend angle  [ radians ]
         double );   // length      [ meters ]

  rbend( char*,      // name
         double,     // bend angle  [ radians ]
         double,     // length      [ meters ]
         double );   // B' / B rho  [ meters^-2 ]  Error field.
                     //   Notation is made compatible with MAD.
  rbend( rbendData& );
  ~rbend();

  void geomToEnd   ( BMLN_posInfo& );
  void geomToStart ( BMLN_posInfo& );
  void eliminate();

  void propagate( Proton&   p );
  void propagate( JetProton& p );
  rbendData* image();
  // ??? REMOVE complex intrDepolRes( int );
} ;

struct rbendData : public bmlnElmntData {
  double rho;     // bend radius [ meters ]
  double K1;      // B' / B rho  [ meters^-2 ]  Error field.
  rbendData();

  rbendData( rbendData& );
  ~rbendData();
  void eliminate();
  void* clone();

  void writeTo( FILE* );
  // ??? REMOVE void readFrom( FILE* );
};


  struct sbendData;
class sbend : public bmlnElmnt
{
private:
  double rho;        // bend radius [ meters ]
  double K1;         // B' / B rho  [ meters^-2 ]  Error field.
                     //   Notation is made compatible with MAD.
  void image( int, slist*, BMLN_posInfo* );
public:
  sbend( double,     // bend angle  [ radians ]    // ??? Order must
         double );   // length      [ meters ]     // ??? be changed.

  sbend( double,     // bend angle  [ radians ]
         double,     // length      [ meters ]
         double );   // B' / B rho  [ meters^-2 ]  Error field.
                     //   Notation is made compatible with MAD.

  sbend( char*,      // name
         double,     // bend angle  [ radians ]
         double );   // length      [ meters ]

  sbend( char*,      // name
         double,     // bend angle  [ radians ]
         double,     // length      [ meters ]
         double );   // B' / B rho  [ meters^-2 ]  Error field.
                     //   Notation is made compatible with MAD.
  sbend( sbendData& );
  ~sbend();

  void geomToEnd   ( BMLN_posInfo& );
  void geomToStart ( BMLN_posInfo& );
  void eliminate();

  void propagate( Proton&   p );
  void propagate( JetProton& p );
  sbendData* image();
  // ??? REMOVE complex intrDepolRes( int );
} ;

struct sbendData : public bmlnElmntData {
  double rho;     // bend radius [ meters ]
  double K1;      // B' / B rho  [ meters^-2 ]  Error field.

  sbendData();
  sbendData( sbendData& );
  ~sbendData();
  void eliminate();
  void* clone();

  void writeTo( FILE* );
  // ??? REMOVE void readFrom( FILE* );
};


  struct sectorData;
class sector : public bmlnElmnt
{
private:
  char   mapType;
  Jet*    map;
  double betaH     [2];  // 0 = entry;  1 = exit
  double alphaH    [2];
  double deltaPsiH;
  double betaV     [2];
  double alphaV    [2];
  double deltaPsiV;
  double mapMatrix[BMLN_dynDim][BMLN_dynDim];
  void image( int, slist*, BMLN_posInfo* );
  double (*DeltaT) ( const double );
  Jet     (*JetDeltaT) ( const Jet& );
public:
  sector( double* betaH,  double* alphaH,  double* psiH,
          double* betaV,  double* alphaV,  double* psiV, double length );
  sector( char*, double* betaH,  double* alphaH,  double* psiH,
                 double* betaV,  double* alphaV,  double* psiV, double length );

  sector(             Jet*, double );
  sector( char* name, Jet*, double );
  sector( sectorData& );
  ~sector();
  void geomToEnd   ( BMLN_posInfo& );
  void geomToStart ( BMLN_posInfo& );
  void eliminate();

  void propagate( Proton& );
  void propagate( JetProton& );
  // ??? REMOVE complex intrDepolRes( int );
  sectorData* image();
  void setFrequency( double (*)( const double ) );
  void setFrequency( Jet (*)( const Jet& ) );
} ;

struct sectorData : public bmlnElmntData {
  char   mapType;
  Jet*    map;
  double betaH     [2];
  double alphaH    [2];
  double deltaPsiH;
  double betaV     [2];
  double alphaV    [2];
  double deltaPsiV;
  double mapMatrix[BMLN_dynDim][BMLN_dynDim];

  sectorData();
  sectorData( sectorData& );
  ~sectorData();
  void eliminate();
  void* clone();

  void writeTo( FILE* );
  // ??? REMOVE void readFrom( FILE* );
};


  struct quadrupoleData;
class quadrupole : public bmlnElmnt
{
private:
  double mapMatrix[BMLN_dynDim][BMLN_dynDim];
  void image( int, slist*, BMLN_posInfo* );
public:
  quadrupole();
  quadrupole( char*  n,    // name
              double l,    // length,             in meters
              double s );  // strength = B'/Brho, in meters^-2
  quadrupole( double l,    // length,             in meters
              double s );  // strength = B'/Brho, in meters^-2
  quadrupole( quadrupoleData& );
  ~quadrupole();
  void setStrength( double s );
  void propagate( Proton&   p );
  void propagate( JetProton& p );
  void peekAt();
  // ??? REMOVE complex intrDepolRes( int );
  quadrupoleData* image();
  void eliminate();
} ;

struct quadrupoleData : public bmlnElmntData {
  double mapMatrix[BMLN_dynDim][BMLN_dynDim];

  quadrupoleData();
  quadrupoleData( quadrupoleData& );
  ~quadrupoleData();
  void eliminate();
  void* clone();

  void writeTo( FILE* );
  // ??? REMOVE void readFrom( FILE* );
};

  struct offsetQuadrupoleData;
class offsetQuadrupole : public bmlnElmnt
{
private:
  double mapMatrix[BMLN_dynDim][BMLN_dynDim];
  void image( int, slist*, BMLN_posInfo* );
public:
  offsetQuadrupole();
  offsetQuadrupole( char*  n,    // name
              double l,    // length,             in meters
              double s );  // strength = B'/Brho, in meters^-2
  offsetQuadrupole( double l,    // length,             in meters
              double s );  // strength = B'/Brho, in meters^-2
  offsetQuadrupole( offsetQuadrupoleData& );
  ~offsetQuadrupole();
  void setStrength( double s );
  void propagate( Proton&   p );
  void propagate( JetProton& p );
  void peekAt();
  // ??? REMOVE complex intrDepolRes( int );
  offsetQuadrupoleData* image();
  void eliminate();
} ;

struct offsetQuadrupoleData : public bmlnElmntData {
  double mapMatrix[BMLN_dynDim][BMLN_dynDim];

  offsetQuadrupoleData();
  offsetQuadrupoleData( offsetQuadrupoleData& );
  ~offsetQuadrupoleData();
  void eliminate();
  void* clone();

  void writeTo( FILE* );
  // ??? REMOVE void readFrom( FILE* );
};

  struct JetQuadrupoleData;
class JetQuadrupole : public bmlnElmnt
{
private:
  Jet mapMatrix[BMLN_dynDim][BMLN_dynDim];
  Jet JetStrength;
  void image( int, slist*, BMLN_posInfo* );
public:
  JetQuadrupole();
  JetQuadrupole( char*  n,    // name
              const double l,    // length,             in meters
              const double s, const int );  // strength = B'/Brho, in meters^-2
  JetQuadrupole( const double l,    // length,             in meters
              const double s, const int index );  // strength = B'/Brho, in meters^-2
  JetQuadrupole( JetQuadrupoleData& );
  ~JetQuadrupole();
  void setStrength( double s );
  void propagate( Proton&   p );
  void propagate( JetProton& p );
  void peekAt();
  // ??? REMOVE complex intrDepolRes( int );
  JetQuadrupoleData* image();
  void eliminate();
} ;

struct JetQuadrupoleData : public bmlnElmntData {
  Jet mapMatrix[BMLN_dynDim][BMLN_dynDim];

  JetQuadrupoleData();
  JetQuadrupoleData( JetQuadrupoleData& );
  ~JetQuadrupoleData();
  void eliminate();
  void* clone();

  void writeTo( FILE* );
  // ??? REMOVE void readFrom( FILE* );
};


  struct patchData;
class patch : public bmlnElmnt
{
private:
  friend class beamline;
  patch();
  patch( bmlnElmnt&, bmlnElmnt& );
  patch( patchData& );
  ~patch();
  void image( int, slist*, BMLN_posInfo* );
public:
  void geomToEnd   ( BMLN_posInfo& );
  void geomToStart ( BMLN_posInfo& );
  void propagate( Proton& p );   // ??? Is it really necessary
  void propagate( JetProton& );   // ??? to make these be public?
  patchData* image();
  void eliminate();
  // ??? REMOVE complex intrDepolRes( int );
} ;

struct patchData : public bmlnElmntData
{
  patchData();
  patchData( patchData& );
  ~patchData();
  void eliminate();
  void* clone();

  void writeTo( FILE* );
  // ??? REMOVE void readFrom( FILE* );
};

  struct thinQuadData;
class thinQuad : public bmlnElmnt
{
private:
  double f;  // horizontal focal length in meters
             // positive f means horizontal
             // focussing
  void image( int, slist*, BMLN_posInfo* );
public:
  thinQuad( double );    // focal length in meters; + = focussing
  thinQuad( char*,       // name
            double );    // focal length in meters; + = focussing
  thinQuad( thinQuadData& );
  ~thinQuad();
  void setStrength( double );  // sets the focal length to the argument
  void propagate( Proton& p );
  void propagate( JetProton& );
  thinQuadData* image();
  void eliminate();
  // ??? REMOVE complex intrDepolRes( int );
} ;

struct thinQuadData : public bmlnElmntData
{
  double          f;    // ??? This should be the reciprocal of 
                        // ??? the focal length.
  thinQuadData();
  thinQuadData( thinQuadData& );
  ~thinQuadData();
  void eliminate();
  void* clone();

  void writeTo( FILE* );
  // ??? REMOVE void readFrom( FILE* );
};

struct JetthinQuadData;
class JetthinQuad : public bmlnElmnt
{
private:
  Jet f;      // horizontal focal length in meters
             // positive f means horizontal
             // focussing
  void image( int, slist*, BMLN_posInfo* );
public:
  JetthinQuad( const double, const int );
  JetthinQuad(       char*,       // name
              const double,      // focal length in meters; + = focussing
              const int );       // index of focal length parameter (> 6)
  JetthinQuad( JetthinQuadData& );
  ~JetthinQuad();
  void setStrength( double );  // sets the focal length to the argument
  void propagate( Proton& p );
  void propagate( JetProton& );
  JetthinQuadData* image();
  void eliminate();
} ;

struct JetthinQuadData : public bmlnElmntData
{
  double          f;    // ??? This should be the reciprocal of 
                        // ??? the focal length.
  JetthinQuadData();
  JetthinQuadData( JetthinQuadData& );
  ~JetthinQuadData();
  void eliminate();
  void* clone();

  void writeTo( FILE* );
  // ??? REMOVE void readFrom( FILE* );
};

class thinSextupole : public bmlnElmnt
{
private:
  void image( int, slist*, BMLN_posInfo* );
public:
  thinSextupole( double /* strength */ );
  thinSextupole( char*  /* name */,
                 double /* strength */ );
  thinSextupole( bmlnElmntData& );
  ~thinSextupole();
  void setStrength( double );
  void propagate( Proton& p );
  void propagate( JetProton& );
  // ??? REMOVE complex intrDepolRes( int );
} ;

class thinOctupole : public bmlnElmnt
{
private:
  void image( int, slist*, BMLN_posInfo* );
public:
  thinOctupole( double /* strength */ );
  thinOctupole( char*  /* name */,
                double /* strength */ );
  thinOctupole( bmlnElmntData& );
  ~thinOctupole();
  void setStrength( double );
  void propagate( Proton& p );
  void propagate( JetProton& );
  // ??? REMOVE complex intrDepolRes( int );
} ;

class thinDecapole : public bmlnElmnt
{
private:
  void image( int, slist*, BMLN_posInfo* );
public:
  thinDecapole( double /* strength */ );
  thinDecapole( char*  /* name */,
                double /* strength */ );
  thinDecapole( bmlnElmntData& );
  ~thinDecapole();
  void setStrength( double );
  void propagate( Proton& p );
  void propagate( JetProton& );
  // ??? REMOVE complex intrDepolRes( int );
} ;

class thinMultipole : public bmlnElmnt
{
private:
  void image( int, slist*, BMLN_posInfo* );
public:
  thinMultipole( double /* strength */ );
  thinMultipole( char*  /* name */,
                double /* strength */ );
  thinMultipole( bmlnElmntData& );
  ~thinMultipole();
  void setStrength( double );
  void propagate( Proton& p );
  void propagate( JetProton& );
  // ??? REMOVE complex intrDepolRes( int );
} ;

class sextupole : public bmlnElmnt
{
private:
  void image( int, slist*, BMLN_posInfo* );
public:
  sextupole( double, /* length   */
             double  /* strength */ );
  sextupole( char*,  /* name     */
             double, /* length   */
             double  /* strength */ );
  sextupole( bmlnElmntData& );
  ~sextupole();
  void propagate( Proton& p );
  void propagate( JetProton& );
  // ??? REMOVE complex intrDepolRes( int );
} ;


  class beamlineImage;
class beamline : public bmlnElmnt, public dlist
{
private:
  lattFunc*         arrayLattFuncs;   // Around the entire beamline
  BMLN_formatType   fileType;
  lattFunc          latticeFunctions; // At the start of the beamline
  double            nominalEnergy;    // In GeV
  int               numElem;          // Number of elements in the beamline
  char              twissDone;
  void image( int, slist*, BMLN_posInfo* );
  void zap();

public:

 // CONSTRUCTORS AND DESTRUCTOR

  beamline();
  beamline( bmlnElmnt* );
  beamline( beamline& );
  beamline( char* );                  // Initializes a beamline from a
                                      //  flat format file whose name is
                                      //  the argument.
  beamline( beamlineImage* );         // Creates a beamline from the 
                                      //  list representation written 
                                      //  by beamline::image.  In the 
                                      //  process it empties the list.
  beamline( char*, bmlnElmnt* );
  beamline( char*, beamline* );
  beamline( char*, char* );           // Initializes a beamline from a
                                      //  flat format file whose name is
                                      //  the second argument.
  beamline( char*, slist* );          // Creates a beamline from the 
                                      //  list representation written 
                                      //  by beamline::image.  ??? UNWRITTEN
  beamline( FILE* );                  // Reading persistent object stored
                                      //  in a binary file.
  ~beamline();
  void eliminate();


 // APPEND AND INSERT

  void insert( beamline& );           // ??? Needs modification for geometry
  void insert( drift& );
  void insert( hkick& );
  void insert( hmonitor& );
  void insert( marker& );
  void insert( monitor& );
  void insert( octupole& );
  void insert( quadrupole& );
  void insert( rbend& );
  void insert( rfcavity& );
  void insert( sbend& );
  void insert( sector& );             // ??? Needs modification for geometry
  void insert( sextupole& );
  void insert( srot& );               // ??? Needs modification for geometry
  void insert( thinQuad& );
  void insert( thinSextupole& );
  void insert( vkick& );
  void insert( vmonitor& );
  void insert( JetthinQuad& );
  void insert( JetQuadrupole& );
  void insert( offsetQuadrupole& );
                                      // ??? Needs modification for geometry

  void insert( bmlnElmnt* q );
  void insert( bmlnElmnt& q );
  void insert( beamline*  q );        // ??? Needs modification for geometry

  void append( beamline& );           // ??? Needs modification for geometry
  void append( drift& );
  void append( hkick& );
  void append( hmonitor& );
  void append( marker& );
  void append( monitor& );
  void append( octupole& );
  void append( quadrupole& );
  void append( rbend& );
  void append( rfcavity& );
  void append( sbend& );
  void append( sector& );             // ??? Needs modification for geometry
  void append( sextupole& );
  void append( srot& );               // ??? Needs modification for geometry
  void append( thinQuad& );
  void append( thinSextupole& );
  void append( thinOctupole& );
  void append( thinDecapole& );
  void append( vkick& );
  void append( vmonitor& );
  void append( JetthinQuad& );
  void append( JetQuadrupole& );
  void append( offsetQuadrupole& );
                                      // ??? Needs modification for geometry
  void append( bmlnElmnt* q );
  void append( bmlnElmnt& q );
  void append( beamline*  q );        // ??? Needs modification for geometry

  beamline& operator^( bmlnElmnt& );  // Alters the beamline
  beamline& operator^( beamline& );
  beamline& operator+( bmlnElmnt& );  // Does not alter the beamline
  beamline& operator+( beamline& );
  beamline& operator-( beamline& );
  friend beamline& operator-( beamline& );


 // EDITING LATTICE

  sector* makeSector ( int );        // Returns a pointer to a new
                                     // sector equivalent to the entire
                                     // beamline.  The argument is the
                                     // degree of the map.
  sector* makeSector ( bmlnElmnt&,   // Returns a pointer to a new sector
                       bmlnElmnt&,   // equivalent to everything between
                       int );        // the first two arguments( exclusive).
  void sectorize     ( int );        // Alters the object itself.
  void sectorize     ( bmlnElmnt&,   // Alters the object: everything between
                       bmlnElmnt&,   // the bmlnElmnt arguments replaced by a map.
                       int );        // <-- This argument is the degree of the map.

  void putAbove( bmlnElmnt& x, bmlnElmnt& y ); // Insert y above (before;  upstream of) x
  void putBelow( bmlnElmnt& x, bmlnElmnt& y ); // Insert y below (after, downstream of) x

  beamline remove( bmlnElmnt&, bmlnElmnt& );


 // GEOMETRY 

  void geomToEnd   ( BMLN_posInfo& );
  void geomToStart ( BMLN_posInfo& );
  int  patchGeometry();
  void resetGeometry();


 // PROPAGATE PROTONS

  void propagate( Proton& );        
  void propagate( JetProton& );
  void propagate( ProtonBunch& );
  void propLattFunc( );
  void propLattFunc( FILE* );
  void propLattFunc( char* );


 // EDIT PROPERTIES

  void setEnergy( double /* Nominal energy in GeV */ );
  void setReader( BMLN_formatType );
  void unTwiss();


 // ANALYSIS

  lattFunc twiss();        // Computes lattice functions all the
                           // way around the ring.
                           // Kludged version; will be replaced.
  lattFunc twiss( char );  // Only computes lattice functions at
                           // the beginning of the ring.
                           // Kludged version; will be replaced.
  lattFunc twiss(lattFunc&);
  // ??? REMOVE complex intrDepolRes( int );



 // QUERIES

  void   peekAt();
  beamlineImage  image( int );
  lattFunc whatIsLattice( int );     // After element n, 0 <= n.
  int    howMany();
  int    countHowMany();
  int    howDeep();


 // CLONING AND STORING

  beamline& operator=( const beamline& );
  beamline* flatten();               // Produces a flattened version of itself.

  void   writeTo( FILE* );           // Stores beamline as persistent object;
                                     //  argument is opened file.
  void   writeTo( char* );           // Stores beamline as persistent object; 
                                     //  argument is name of unopened file.
  void   writeLattFunc( );
  void   writeLattFunc( FILE* );
  void   writeLattFunc( char* );

} ;  // End class beamline

extern beamline& operator*( int, beamline& );
extern beamline& operator*( int, bmlnElmnt& );
extern beamline& operator*( beamline&, int );
extern beamline& operator*( bmlnElmnt&, int );
extern beamline& operator^( bmlnElmnt&, bmlnElmnt& );
extern beamline& operator-( beamline& );


struct beamlineData : public bmlnElmntData
{
  int             numElem;          // Number of elements in the beamline

  beamlineData();
  beamlineData( beamlineData& );
  ~beamlineData();
  void eliminate();
  void* clone();

  void writeTo( FILE* );
};


class beamlineImage {
private:
  slist bak;

  beamlineImage();
  friend class beamline;

public:
  slist img;

  beamlineImage( FILE* );
  beamlineImage( beamlineImage& );
  ~beamlineImage();
  void reset();    // If the user has manipulated img, the reset()
                  //  method restores it to its original state.
  void zap();      // Destroys all XXXXData objects.
  beamlineImage& operator=( const beamlineImage& );
                  // WARNING: there may be stray XXXXData objects
                  //  hanging around after this if ::zap() is not
                  //  used first.
};

class circuit : public dlist
{
private:
  double current;       // current in amperes
  char onOffSwitch;     // switch to turn current on and off
  char* ident;          // name of circuit
  int numElm;           // number of elements in the circuit
public:
  circuit();
  circuit( char * );
  circuit( bmlnElmnt* );
  circuit( char*, bmlnElmnt* );
  ~circuit();
  void switchOn();
  void switchOff();
  void setCurrent( double );
  void changeCurrent( double );
  char* getName();
  double getCurrent();

// Only append things which have current in them.

  void append( hkick& );
  void append( octupole& );
  void append( quadrupole& );
  void append( rbend& );
  void append( rfcavity& );
  void append( sbend& );
  void append( sextupole& );
  void append( thinQuad& );
  void append( thinSextupole& );
  void append( thinOctupole& );
  void append( thinDecapole& );
  void append( vkick& );
  void append( bmlnElmnt* q );
} ;


class beamlineOverseer
{
private:
  dlist setOfBeamlines;
  dlist setOfProbes;
} ;

#endif BEAMLINE_HXX

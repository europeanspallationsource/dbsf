#ifndef BEAMLINE_INC
#define BEAMLINE_INC

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "numrecip.hxx"
#include "beamline.hxx"
#include "flatin.h"

const double pi    = 3.14159265358979323846;
const double twoPi = 6.28318530717958647693;

#define NOTKNOWN   -123456.789
#define PROTONMASS 938.2796e6;

#ifndef BMLN_SMALL
#define BMLN_SMALL    1.0e-14 // Used by DA::addTerm to decide
                              //   removal of a DAterm.
#endif  BMLN_SMALL

// **************************************************
//  External declarations
// **************************************************

extern lattFunc BMLN_lattBuff;    // Introduced for use by resonance
extern double   BMLN_energyBuff;  // calculating functions.
extern double*  BMLN_dblBuff;

extern "C" {
  int flatin( char*, flatin_data* ); 
}

#endif BEAMLINE_INC

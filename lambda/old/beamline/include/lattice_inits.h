#ifndef LATTICE_INITS_H
#define LATTICE_INITS_H

legal_struct legal_type[] = { 
  "",             0,0,0,0, /* NULL element */
  "drift",        1,0,0,0, /* length */
  "hmonitor",     1,0,0,0, /* length */
  "vmonitor",     1,0,0,0, /* length */
  "monitor",      1,0,0,0, /* length */
  "instrument",   1,0,0,0, /* length */
  "wiggler",      1,0,1,0, /* length, tilt */
  "rbend",        1,1,1,1, /* length, angle, tilt, more */
  "sbend",        1,1,1,1, /* length, angle, tilt, more */
  "quadrupole",   1,1,1,0, /* length, k1, tilt */
  "sextupole",    1,1,1,0, /* length, k2, tilt */
  "octupole",     1,1,1,0, /* length, k3, tilt */
  "multipole",    0,0,0,1, /* more */
  "solenoid",     1,1,0,0, /* length, ks */
  "rfcavity",     1,0,0,1, /* length, more */
  "elseparator",  1,1,1,0, /* length, e, tilt */
  "srot",         0,1,0,0, /* angle */
  "yrot",         0,1,0,0, /* angle */
  "hkick",        1,1,1,0, /* length, hkick, tilt */
  "vkick",        1,1,1,0, /* length, vkick, tilt */
  "kicker",       1,0,1,1, /* length, tilt, more */
  "marker",       0,0,0,0, 
  "ecollimator",  1,0,0,1, /* length, more */
  "rcollimator",  1,0,0,1, /* length, more */
  "tbend",        1,1,1,1, /* length, angle, tilt, more */
  "thinlens",     0,0,0,1, /* more  */
  "tank",         1,0,0,1, /* length, more */
  "edge",         0,0,0,1, /* more  */
  "pmq",          1,0,0,1, /* length, more */
  "rfqcell",      1,0,0,1, /* length, more */
  "doublet",      1,0,0,1, /* length, more */
  "triplet",      0,0,0,1, /* more  */
  "rfgap",        0,0,0,1, /* more  */
  "special",      1,0,0,0, /* length */
  "rotation",     0,1,0,0, /* angle */
  "beamline",     1,0,0,0, /* length */
  "slot",         1,0,0,0, /* length */
  "end_of_list",  0,0,0,0
  };

row_struct row[] = {
  "",0,"","", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "",
  "ecollimator", 2,  "xsize", "ysize", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "kicker", 2,    "hkick", "vkick", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "",
  "multipole", 20,  "k0l", "k1l", "k2l", "k3l", "k4l", "k5l", "k6l",
  "k7l", "k8l", "k9l", "t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7",
  "t8", "t9", "", "",
  "rbend", 9,    "k1", "k2", "k3", "e1", "e2", "fint", "hgap", "h1",
  "h2", "", "", "", "", "", "", "", "", "", "", "", "", "",
  "rcollimator", 2,  "xsize", "ysize", "", "", "", "", "", "", "",
  "", "", "", "", "", "", "", "", "", "", "", "", "",
  "rfcavity", 7,    "volt", "lag", "harmon", "betrf", "pg", "shunt",
  "tfill", "", "", "", "", "", "", "", "", "", "", "",
  "", "",  "", "",
  "sbend", 9,    "k1", "k2", "k3", "e1", "e2", "fint", "hgap", "h1",
  "h2", "", "", "", "", "", "", "", "", "", "", "", "",
  "",
  "tbend", 22,    "b0", "a0", "b1", "a1", "b2", "a2", "b3", "a3",
  "b4", "a4", "b5", "a5", "b6", "a6", "b7", "a7", "b8", "a8",
  "b9", "a9", "b10", "a10",
  
  "thinlens", 3,    "xfocal", "yfocal", "zfocal", "", "", "", "", "",
  "", "", "", "", "", "", "", "", "", "", "", "", "",
  "",
  "doublet", 1,    "distance_between", "", "", "", "", "", "", "",
  "", "", "", "", "", "", "", "", "", "", "", "", "",
  "",
  "triplet", 5,    "strength_outer_quad", "length_outer_quad", "distance_between", 
  "strength_inner_quad", "length_inner_quad", "", "", "",
  "", "", "", "", "", "", "", "", "", "", "", "", "",
  "",
  "edge", 5,    "rotation_angle", "radius", "gap", "fringe_field1", "fringe_field2","", "", "",
  "", "", "", "", "", "", "", "", "", "", "", "", "",
  "",
  "rfqcell", 4,    "V/(r**2)", "AV", "phase", "type", "", "", "", "",
  "", "", "", "", "", "", "", "", "", "", "", "", "",
  "",
  "rfgap", 5,    "effective_gap_voltage", "phase", "emittance_growth_flag", 
  "energy_gain_flag", "harmonic", "", "", "",
  "", "", "", "", "", "", "", "", "", "", "", "", "",
  "",
  "pmq", 2,    "inner_radius", "outer_radius", "", "", "", "", "", "",
  "", "", "", "", "", "", "", "", "", "", "", "", "",
  "",
  "tank", 3,    "effective_accel_gradient", "inj/exit_phase", "number_of_iden_cavities",
  "", "", "", "", "",
  "", "", "", "", "", "", "", "", "", "", "", "", "",
  ""
  };

#endif  /* LATTICE_INITS_H */

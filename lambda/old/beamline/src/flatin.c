/*************************************************************************/

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "flatin.h"
#include "lattice_inits.h"

FILE    *logFile;
FILE    *flattice;

int     flatin(lattice, data_ptr)
     char *lattice;
     flatin_data *data_ptr;
{
  time_t flatin_time_stamp;

  /* ----------------------------------------------------------------------
     Open the flat lattice file for input, and a log file for recording the 
     progress of "flatin".   */
  
  if ((flattice = fopen(lattice, "r")) == NULL) {
    fprintf(stderr,"flatin: could not find file %s!\n", lattice);
    return(0);
  }
  else {
    logFile = fopen("flatin.log","w");
    time(&flatin_time_stamp);
    fprintf(logFile,"%s",ctime(&flatin_time_stamp));
    fprintf(logFile,"reading lattice from flat file %s\n", lattice);
  }
  
  /* ----------------------------------------------------------------------
     Get parameters from the flat lattice file, and fill the PARAMETER struct 
     with their names and values.    */

  data_ptr->legal_type_ptr = legal_type;
  data_ptr->number_of_legal_types = LEGAL_TYPE_COUNT;
  data_ptr->row_ptr = row;
  data_ptr->number_of_rows = NUMBER_OF_COLUMN_ENTRIES;
  
  data_ptr->number_of_parameters = 0;
  fprintf(logFile,"\nPARAMETERS\n\n");

  while (get_parameter(data_ptr)) {
    data_ptr->number_of_parameters++;
  }

  /* ----------------------------------------------------------------------
     Get information about the ideal properties of elements, possibly 
     including systematic and average multipole statistics.  Fill the ELEMENT 
     struct with linear information, and fill the more struct with 
     aditional characteristics, if any.       */
  
  data_ptr->number_of_elements = 0;
  fprintf(logFile,"\nELEMENTS\n\n");

  while (get_element(data_ptr)) {
    data_ptr->number_of_elements++;
  }

  /* ----------------------------------------------------------------------
     Get information about the sequence of lattice elements, and fill the
     ATOM struct.  */
 
  data_ptr->number_of_atoms = 0; 
  fprintf(logFile,"\nBEAM LINE ATOMIC SEQUENCE\n\n");

  while (get_atom(data_ptr)) {
    data_ptr->number_of_atoms++;
  }
  
  /* ----------------------------------------------------------------------
     Log some abbreviated statistics summarising the lattice */
  
  fprintf(logFile,"\nSTATISTICS\n\n");

  fprintf(logFile,"%-5d parameters were found.\n",data_ptr->number_of_parameters);
  fprintf(logFile,"%-5d elements were found.\n",data_ptr->number_of_elements);
  fprintf(logFile,"%-5d atoms in the lattice.\n",data_ptr->number_of_atoms);

  fprintf(logFile,"\nThe total length was %.10f meters\n\n",
          data_ptr->atom_ptr[data_ptr->number_of_atoms-1].s);

  fclose(flattice);
  fclose(logFile);

  return(1);
  
}
/*************************************************************************/
int findelm(data_ptr,name)
     flatin_data *data_ptr;
     char *name;
{
  int   id;
  
  for (id = 0 ; id < data_ptr->number_of_elements ; id++) {
    if (!strcmp(name,data_ptr->element_ptr[id].name)) {
      return(id) ;
    }
  }     
  fprintf(stderr,"flatin: could not find an element called %s \n", name);
  fprintf(logFile,  "flatin: could not find an element called %s \n", name);
  exit(1);
  
}
/*************************************************************************/
int findpar(data_ptr,name)
     flatin_data *data_ptr;
     char *name;
{
  int   id;

  if(data_ptr == NULL) return;

  for (id = 0 ; id < data_ptr->number_of_parameters ; id++) {
    if (!strcmp(name,data_ptr->parameter_ptr[id].name)) {
      return(id) ;
    }
  }     
  fprintf(stderr,"flatin: could not find a parameter called %s \n", name);
  fprintf(logFile,  "flatin: could not find a parameter called %s \n", name);
  exit(1);
  
}
/*************************************************************************/
int findrow(data_ptr,name)
     flatin_data *data_ptr;
     char *name;
{
  int   id;

  if(data_ptr == NULL) return;

  for (id = 0 ; id < 9 ; id++) {
    if (!strcmp(name,row[id].name)) {
      return(id) ;
    }
  }     
  fprintf(stderr,"flatin: could not find a row name called %s \n", name);
  fprintf(logFile,  "flatin: could not find a row name called %s \n", name);
  exit(1);
  
}
/*************************************************************************/
int findtype(name)
     char *name;
{
  char  type[5];
  int   i;
  int   tid;
  
  strncpy(type, name, 4);
  for (i = 0 ; i < 4 ; i++) {
    if (isupper(type[i])) type[i] = tolower(type[i]) ;
  }
  type[4] = '\0';
  
  for (tid = 0 ; 1 ; tid++) {
    if (!strncmp(type,legal_type[tid].name,4)) {
      return(tid);
    }
    else if (!strcmp("end_of_list",legal_type[tid].name)) {
      fprintf(stderr,"flatin: strange magnet type %s !\n", name);
      fprintf(logFile,  "flatin: strange magnet type %s !\n", name);
      exit(1);
    }
  }
  
}

/*************************************************************************/
int get_element(data_ptr)
flatin_data *data_ptr;
{
  char  name[NAME_MAX];
  double        value;
  float tmp_float;
  int   tmp_int;
  int   pid;
  int   tid;
  int   rid;
  int i;
  
  /* ------------------------------------------------------------------------
     get the element name     */
  
  nextstr(name);    

  if (!strncmp(name,"end",3) || !strncmp(name,"END",3)) {
    return(0);
  } else {
    if(data_ptr->number_of_elements == 0)
      data_ptr->element_ptr = (element_struct *)malloc(sizeof(element_struct));
    else
      data_ptr->element_ptr = (element_struct *)realloc(data_ptr->element_ptr,
			        (data_ptr->number_of_elements+1)*sizeof(element_struct));
    if(data_ptr->element_ptr == NULL) {
      printf("unable to allocate memory for element table\n");
      fprintf(logFile,"unable to allocate memory for element table\n");
      return(0);
    }
    /* fprintf(logFile, "%-16s", name); */
    strcpy(data_ptr->element_ptr[data_ptr->number_of_elements].name,name);
  }
  
  nextstr(name);
  tid = findtype(name);
  data_ptr->element_ptr[data_ptr->number_of_elements].type_index = tid;

  /* fprintf(logFile, "%-16s", legal_type[tid].name); */
  
  /* ------------------------------------------------------------------------
     get the element length   */
  
  if (!legal_type[tid].has_length) {
    data_ptr->element_ptr[data_ptr->number_of_elements].length = 0.0;
    data_ptr->element_ptr[data_ptr->number_of_elements].length_index = 0;
  }
  else {
    nextstr(name);
    if (sscanf(name, "%lf", &value) ==1) {
      data_ptr->element_ptr[data_ptr->number_of_elements].length = value;
      data_ptr->element_ptr[data_ptr->number_of_elements].length_index = 0;
      /* fprintf(logFile, "%19.14f", value); */
    }
    else {
      pid = findpar(data_ptr,name);
      data_ptr->element_ptr[data_ptr->number_of_elements].length = data_ptr->parameter_ptr[pid].value;
      data_ptr->element_ptr[data_ptr->number_of_elements].length_index = pid;
      /* fprintf(logFile, "%19.14f", data_ptr->parameter_ptr[pid].value); */
    }
  }
  
  /* ------------------------------------------------------------------------
     get the element strength */
  
  if (!legal_type[tid].has_strength) {
    data_ptr->element_ptr[data_ptr->number_of_elements].strength = 0.0;
    data_ptr->element_ptr[data_ptr->number_of_elements].strength_index = 0;
  }
  else {
    nextstr(name);
    if (sscanf(name, "%lf", &value) ==1) {
      data_ptr->element_ptr[data_ptr->number_of_elements].strength = value;
      data_ptr->element_ptr[data_ptr->number_of_elements].strength_index = 0;
      /* fprintf(logFile, "%19.14f", value); */
    }
    else {
      pid = findpar(data_ptr,name);
      data_ptr->element_ptr[data_ptr->number_of_elements].strength =
		 data_ptr->parameter_ptr[pid].value;
      data_ptr->element_ptr[data_ptr->number_of_elements].strength_index = pid;
      /* fprintf(logFile, "%19.14f", data_ptr->parameter_ptr[pid].value); */
    }
  }
  
  /* ------------------------------------------------------------------------
     get the element tilt     */
  
  if (!legal_type[tid].has_tilt) {
    data_ptr->element_ptr[data_ptr->number_of_elements].tilt = 0.0;
    data_ptr->element_ptr[data_ptr->number_of_elements].tilt_index = 0;
  }
  else {
    nextstr(name);
    if (sscanf(name, "%lf", &value) ==1) {
      data_ptr->element_ptr[data_ptr->number_of_elements].tilt = value;
      data_ptr->element_ptr[data_ptr->number_of_elements].tilt_index = 0;  
      /* fprintf(logFile,"%19.14f", value); */
    }
    else {
      pid = findpar(data_ptr,name);
      data_ptr->element_ptr[data_ptr->number_of_elements].tilt = 
		data_ptr->parameter_ptr[pid].value;
      data_ptr->element_ptr[data_ptr->number_of_elements].tilt_index = pid;
      /* fprintf(logFile, "%19.14f", data_ptr->parameter_ptr[pid].value); */
    }
  }
  
  /* ------------------------------------------------------------------------
       get more data here if any  */

  if(legal_type[tid].has_more) {

    rid = findrow(data_ptr,legal_type[tid].name);

    nextstr(name);
    if(sscanf(name, "%f",&tmp_float) == 1) {
      tmp_int = (int)tmp_float;
      data_ptr->element_ptr[data_ptr->number_of_elements].npars = tmp_int;
      data_ptr->element_ptr[data_ptr->number_of_elements].more_index = 
	data_ptr->number_of_parameters;
    if(data_ptr->number_of_parameters == 0)
      data_ptr->parameter_ptr = (parameter_struct *)malloc(tmp_int *
							   sizeof(parameter_struct));
    else
      data_ptr->parameter_ptr = (parameter_struct *)realloc(data_ptr->parameter_ptr,
			        (data_ptr->number_of_parameters+tmp_int)*
							    sizeof(parameter_struct));
    if(data_ptr->parameter_ptr == NULL) {
      printf("unable to allocate memory for parameter table\n");
      fprintf(logFile,"unable to allocate memory for parameter table\n");
      return(0);
    }

    for(i=0; i<tmp_int; i++) {
      nextstr(name);
      if(sscanf(name,"%lf",&value) == 1) {
	data_ptr->parameter_ptr[data_ptr->number_of_parameters+i].value = value;
        strcpy(data_ptr->parameter_ptr[data_ptr->number_of_parameters+i].name,
	       row[rid].heading[i]);
      } else {
	pid = findpar(data_ptr,name);
	data_ptr->parameter_ptr[data_ptr->number_of_parameters+i].value = 
	  data_ptr->parameter_ptr[pid].value;
        strcpy(data_ptr->parameter_ptr[data_ptr->number_of_parameters+i].name,
	       data_ptr->parameter_ptr[pid].name);
      }

    }

    data_ptr->number_of_parameters += tmp_int;
    }

  }
  /* fprintf(logFile,"\n"); */

  return(1);
}

/*************************************************************************/

int get_parameter(data_ptr)
flatin_data *data_ptr;
{
  char  name[NAME_MAX];
  double        value;
  
  /* --------------------------------------------------------------------------
     get the parameter name  */
  
  nextstr(name);

  if (!strncmp(name,"end",3) || !strncmp(name,"END",3)) {
    return(0);
  } else {
    if(data_ptr->number_of_parameters == 0)
      data_ptr->parameter_ptr = (parameter_struct *)malloc(sizeof(parameter_struct));
    else
      data_ptr->parameter_ptr = (parameter_struct *)realloc(data_ptr->parameter_ptr,
			        (data_ptr->number_of_parameters+1)*sizeof(parameter_struct));
    if(data_ptr->parameter_ptr == NULL) {
      printf("unable to allocate memory for parameter table\n");
      fprintf(logFile,"unable to allocate memory for parameter table\n");
      return(0);
    }
     strcpy(data_ptr->parameter_ptr[data_ptr->number_of_parameters].name,name);
    fprintf(logFile,  "%-15s", name);
  }
  
  /* -----------------------------------------------------------------------
     get the parameter value */
  
  nextstr(name);

  sscanf(name, "%lf", &value);
  
  data_ptr->parameter_ptr[data_ptr->number_of_parameters].value = value;
  fprintf(logFile, "= %19.14f\n", value);
  
  return(1);
}

/*************************************************************************/

int get_atom(data_ptr)
     flatin_data *data_ptr;
{
  char  name[NAME_MAX];
  int   id;
  double arc_length, bend_angle;
  element_struct *this_element;

  
  /* -----------------------------------------------------------------------
   */
  
  nextstr(name);
  if (!strncmp(name,"end",3) || !strncmp(name,"END",3)) {
    return(0);
  }  else {
    if(data_ptr->number_of_atoms == 0)
      data_ptr->atom_ptr = (atom_struct *)malloc(sizeof(atom_struct));
    else
      data_ptr->atom_ptr = (atom_struct *)realloc(data_ptr->atom_ptr,
			        (data_ptr->number_of_atoms+1)*sizeof(atom_struct));
    if(data_ptr->atom_ptr == NULL) {
      printf("unable to allocate memory for lattice table\n");
      fprintf(logFile,"unable to allocate memory for lattice table\n");
      return(0);
    }

    /* fprintf(logFile,"%s\n",name); */
    id = findelm(data_ptr,name);
    this_element = &data_ptr->element_ptr[id];
    data_ptr->atom_ptr[data_ptr->number_of_atoms].element_index = id;
    data_ptr->atom_ptr[data_ptr->number_of_atoms].type_index = this_element->type_index;

    if (!strcmp(legal_type[this_element->type_index].name,"rbend") &&
	((bend_angle = data_ptr->element_ptr[id].strength) != 0.0)) {
      arc_length = ((0.5 * bend_angle) / sin(0.5 * bend_angle)) * this_element->length;
    } else {
      arc_length = this_element->length;
    }
    if(data_ptr->number_of_atoms == 0)
      data_ptr->atom_ptr[data_ptr->number_of_atoms].s = arc_length;
    else
      data_ptr->atom_ptr[data_ptr->number_of_atoms].s = arc_length +
	data_ptr->atom_ptr[data_ptr->number_of_atoms-1].s;
  }
  return(1);
}

/*************************************************************************/

int nextstr(name)
     char *name;
{
  char  buff[NAME_MAX];
  
  /*    Always read at least one new line.  Skip blank lines.  "name" points
        only to the first item on a line.       */
  
  strcpy(buff,"\n");
  while (sscanf(buff, "%s", name) == EOF) {
    if (!fgets(buff,NAME_MAX,flattice)) {
      fprintf(stderr,"flatin: EOF while trying to read string !\n");
      fprintf(logFile,  "flatin: EOF while trying to read string !\n");
      exit(1);
    }
  }
  
  return(1);
}

/*************************************************************************/

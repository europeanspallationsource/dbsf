#include <stdlib.h>
#include <stdio.h>
#include "beamline.rsc"

main( int argc, char** argv ) {
 Jet::setup( 6, 1, 6 );

 if( argc != 4 ) {
  printf( "fodo  <length of drift>  <focal length of F> <number of cells>\n" );
  exit(0);
 }
 
 drift    O (   atof( argv[1] ) );

 double focalLength = atof( argv[2] );
 thinQuad F (   focalLength );
 thinQuad D ( - focalLength );
 
 int numCells = atoi( argv[3] );

 beamline* flatVersion = ( numCells*( F^O^D^O ) ).flatten();

 lattFunc info = flatVersion->twiss();

 int n = flatVersion->howMany() - 1;
 printf( "Tune: horizontal = %lf, vertical = %lf\n",  
         flatVersion->whatIsLattice( n ).psi.hor / 6.2831853072,
         flatVersion->whatIsLattice( n ).psi.ver / 6.2831853072
       );
 printf( "Beta_H: at F = %lf,  at D = %lf\n", 
         flatVersion->whatIsLattice( 0 ).beta.hor,
         flatVersion->whatIsLattice( 2 ).beta.hor
       );
 printf( "Beta_V: at F = %lf,  at D = %lf\n", 
         flatVersion->whatIsLattice( 0 ).beta.ver,
         flatVersion->whatIsLattice( 2 ).beta.ver
       );

}

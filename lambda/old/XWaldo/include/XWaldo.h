/* $Header: /rap/lambda/XWaldo/include/RCS/XWaldo.h,v 1.1 1995/08/18 22:50:22 mackay Exp $ */

#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN extern
#endif

/* - - - - - */
#ifndef _AMIGA
#define USE_XT 1
#endif  /* #ifndef _AMIGA */

/* - - - - - */
#ifdef _X86_
#endif


EXTERN  void XWinit(int argc,char **argv);
EXTERN  void XWfinish(void);
EXTERN  void XWflush(void);

EXTERN  void XWsetregion(long x0, long x1, long y0, long y1);
EXTERN  void XWsetvirtual(double x0, double y0, double x1, double y1);
EXTERN  void XWsetclip(int on);
EXTERN  void XWscale(double x, double y, long *ix, long *iy);
EXTERN  void XWunscale(long ix, long iy, double *x, double *y);

EXTERN  void XWboxregion(void);
EXTERN  void XWerase(void);
EXTERN  void XWsetpen(int pen);

EXTERN  void XWmovabs(long ix, long iy);
EXTERN  void XWmovrel(long ix, long iy);
EXTERN  void XWmovea(double x, double y);
EXTERN  void XWmover(double x, double y);

EXTERN  void XWdrwabs(long ix, long iy);
EXTERN  void XWdrwrel(long ix, long iy);
EXTERN  void XWdrawa(double x, double y);
EXTERN  void XWdrawr(double x, double y);

/* In the following the integer "pos" takes values from           789   */
/* 1 to 9 which indicate the location of the text relative        456   */
/* to (ix,iy).  A 5 means center the text, whereas a 3 means      123   */
/* align the text horizontally with (ix,iy), but place it below         */
/* the point.  The numbers to the right indicate the relative positions.*/

EXTERN  void XWlblabs(long ix, long iy, char *text, int pos);
EXTERN  void XWlblrel(long ix, long iy, char *text, int pos);
EXTERN  void XWlabela(double x, double y, char *text, int pos);
EXTERN  void XWlabelr(double x, double y, char *text, int pos);

EXTERN  void XWlinea(double x0, double y0, double x1, double y1);

#define movabs(ix,iy) XWmovabs(ix,iy)
#define movrel(ix,iy) XWmovrel(ix,iy)
#define movea(x,y) XWmovea(x,y)
#define mover(x,y) XWmover(x,y)
#define drwabs(ix,iy) XWdrwabs(ix,iy)
#define drwrel(ix,iy) XWdrwrel(ix,iy)
#define drawa(x,y) XWdrawa(x,y)
#define drawr(x,y) XWdrawr(x,y)
#define lblabs(x,y,text,ipos) XWlblabs(x,y,text,ipos)
#define lblrel(x,y,text,ipos) XWlblrel(x,y,text,ipos)
#define labela(x,y,text,ipos) XWlabela(x,y,text,ipos)
#define labelr(x,y,text,ipos) XWlabelr(x,y,text,ipos)
#define setregion(ix0,ix1,iy0,iy1) XWsetregion(ix0,ix1,iy0,iy1)
#define setvirtual(ix0,ix1,iy0,iy1) XWsetvirtual(ix0,ix1,iy0,iy1)
#define scale(x,y,ix,iy) XWscale(x,y,ix,iy)
#define unscale(ix,iy,x,y) XWunscale(ix,iy,x,y)
#define erase XWerase
#define clip(on) XWsetclip(on)
#define boxregion XWboxregion
#define setpen(pen) XWsetpen(pen)


/*  HERE are the USER supplied function prototypes */

EXTERN void user_start(int argc, char **argv);
EXTERN void user_mouse(int mbutton, int xmdown,int ymdown,int xmup,int ymup);
EXTERN void user_refresh(int left, int right, int bottom, int top);
EXTERN void user_key(int i, char *code);
EXTERN void XWaldoEventLoop(int argc, char** argv);
EXTERN void XWaldoGlishEventLoop(int argc, char** argv);

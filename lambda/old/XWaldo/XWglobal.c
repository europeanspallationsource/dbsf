/* $Header: /rap/lambda/XWaldo/RCS/XWglobal.c,v 1.4 1995/08/24 23:12:37 mackay Exp $ */
char string[10];  /* For debugging purposes only. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <XWaldo.h>
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA     /*==========================*/

#define     INTUI_V36_NAMES_ONLY 1        /* don't load obsolete names */
#include    <exec/types.h>
#include    <exec/memory.h>
#include    <intuition/intuitionbase.h>
#include    <graphics/gfxmacros.h>
#include    <graphics/displayinfo.h>
#include    <intuition/screens.h>

#include    <clib/dos_protos.h>
#include    <clib/exec_protos.h>
#include    <clib/graphics_protos.h>
#include    <clib/intuition_protos.h>
#include    <clib/layers_protos.h>

struct   Screen    *XW_display    = NULL;
struct   RastPort  *XW_rp         = NULL;
struct   Window    *XW_window     = NULL;
struct   Library   *IntuitionBase = NULL;
struct   GfxBase   *GfxBase       = NULL;
struct   Library   *LayersBase    = NULL;

struct   Region    *XW_clipregion = NULL;

#else   /*%%%%%%%%%%%%%%%%%%%%*/


#ifdef USE_XT
#include    <X11/Intrinsic.h>    /* Intrinsics Defs */
#include    <X11/StringDefs.h>  /* Standard String Defs */
#include    <X11/Xaw/Box.h> /* Athena Command Widget ??? */

int gflag = 0; /* for XWaldoGlishEventLoop() */

#endif


#include    <X11/Xlib.h>
#include    <X11/Xutil.h>

Display     *XW_display    = NULL;
Window       XW_window;
Visual      *XW_visual;
GC           XW_gc;
XSizeHints   XW_hint;
XWMHints     XW_wmhint;
XFontStruct *XW_font_info;

char        *fontname;

XEvent              XW_event;
KeySym              XW_key;

Screen  *XW_screen_ptr;
long     XW_screen;    /* This refers to an X screen number not an Amiga
			  Screen (see XW_display above). */

#define NCOLORS 8
char xcolorname[][NCOLORS] =
{
  "black", "white", "red", "green", "yellow", "cyan", "magenta", "orange3"
};
XColor   XW_color[NCOLORS];
XColor   XW_exact[NCOLORS];
Colormap XW_cmap;


#endif            /*==========================*/
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/


int  XW_clipped = 0;

unsigned long       XW_fgnd, XW_bgnd;

unsigned int        XW_width;
unsigned int        XW_height;
unsigned int        XW_border;
unsigned int        XW_depth;

int  XW_number_of_colors = 1;


/* Define some stuff global to these routines, but not the user routines. */
const int DEF_WIN_LEFT   = 0;
const int DEF_WIN_TOP    = 0;
const int DEF_WIN_WIDTH  = 400;
const int DEF_WIN_HEIGHT = 250;

double txmin = 0.0;    /* virtual limits of plot region */
double txmax = 10.0;
double tymin = 0.0;
double tymax = 10.0;
long kwidth;   /* actual width of the whole window */
long kheight;  /* actual height of the whole window */
long kxmin;    /* window limits of plot region */
long kxmax;
long kymin;
long kymax;
double dixbydx;        /* scaling factors between window & virtual coords */
double diybydy;

long kxbeam;           /* current ix coordinate of beam in window coords */
long kybeam;           /* current iy coordinate of beam in window coords */
double txbeam;         /* current x coordinate of beam in virtual coords */
double tybeam;         /* current y coordinate of beam in virtual coords */
double tximag;         /* unclipped virtual x coordinate of last motion */
double tyimag;         /* unclipped virtual y coordinate of last motion */


/*============================*/
void getargfont(int argc, char **argv)
{
  int i;

  fontname = malloc(100);
  strncpy(fontname, "6x13", 5);
  if(argc<3)return;
  for(i=0;i<argc-1;i++)
    {
      if(strstr(argv[i], "-font")!=NULL)
	{
	  strncpy(fontname, argv[i+1], 100);
	  return;
	}
    }
}
/*============================*/
void XWglobal(void)
{
  txmin = 0.0;    /* starting virtual limits of plot region */
  txmax = 10.0;
  tymin = 0.0;
  tymax = 10.0;
  kwidth  = DEF_WIN_WIDTH;
  kheight = DEF_WIN_HEIGHT;
  kxmin   = DEF_WIN_LEFT;
  kxmax   = DEF_WIN_WIDTH;
  kymin   = DEF_WIN_HEIGHT;
  kymax   = DEF_WIN_TOP;
}
/*============================*/
/* These only need to be defined if we are compiling for XT */

#ifdef USE_XT
void Mouse1_Proc(Widget w, XButtonEvent *event,
		String *params, Cardinal num_params)
{
  int x,y;

  x = event->x;
  y = event->y;

  user_mouse((int)1,x,y,x,y);
}
/*============================*/
void Mouse2_Proc(Widget w, XButtonEvent *event,
		String *params, Cardinal num_params)
{
  int x,y;

  x = event->x;
  y = event->y;

  user_mouse((int)2,x,y,x,y);
}
/*============================*/
void Mouse3_Proc(Widget w, XButtonEvent *event,
		String *params, Cardinal num_params)
{
  int x,y;

  x = event->x;
  y = event->y;

  user_mouse((int)3,x,y,x,y);
}
/*============================*/
void Key_Proc(Widget w, XKeyEvent *event,
	      String *params, Cardinal num_params)
{
  int i;
  char text[10];

  i = XLookupString(event,text,10,&XW_key,0);
  user_key(i,text);
}
/*============================*/
void Expose_Proc(Widget w, XExposeEvent *event,
		 String *params, Cardinal num_params)
{
  Status status;
  Window XW_root;
  int XW_locx,XW_locy;

  if(event->count==0)
    {
      status = XGetGeometry(XtDisplay(w),XtWindow(w),&XW_root,
			    &XW_locx,&XW_locy,
			    &XW_width,&XW_height,
			    &XW_border,&XW_depth);

      if(status)
	{
	  XWsetregion(0,XW_width,0,XW_height);
	  XWerase();
	  user_refresh((int)0,XW_width,(int)0,XW_height);
	}
    }
}
/*============================*/
void Stdin_Proc(XtPointer client_data, int *fid, XtInputId *id)
{
  char buf[BUFSIZ];
  int nbytes;

  if((nbytes = read(*fid, buf, BUFSIZ)) == -1)
    fprintf(stderr,"Stdin_Proc: 'read' error\n");
  if(nbytes>0)
    {
      if(buf[0]=='q')exit(0);
      user_key(nbytes,buf);
    }
}
/*============================*/
void XWflush(void)
{
  XFlush(XW_display);
}
#endif

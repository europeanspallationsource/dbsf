/* $Header: /rap/lambda/XWaldo/RCS/XWaldoEventLoop.c,v 1.4 1995/08/24 23:12:37 mackay Exp $ */
char string[10];  /* For debugging purposes only. */
/* #define WHOBEI 1 */

/* Do we want to use the Xtoolkit version ?  If not zap the following line */
/* #define USE_XT 1 */   /* can use the -DUSE_XT switch to gcc */
/* USE_XT and GLISH are defined in include/XWaldo.h except for the Amiga */

/*-------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <XWaldo.h>

/* Some prototypes */
int read(int fd, char *buf, int n);
void XWsetscale(void);
void XWgetsomecolors(void);
void XWglobal(void);

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA     /*==========================*/

#define     INTUI_V36_NAMES_ONLY 1        /* don't load obsolete names */
#include    <exec/types.h>
#include    <exec/memory.h>
#include    <intuition/intuitionbase.h>
#include    <graphics/gfxmacros.h>
#include    <graphics/displayinfo.h>
#include    <intuition/screens.h>

#include    <clib/dos_protos.h>
#include    <clib/exec_protos.h>
#include    <clib/graphics_protos.h>
#include    <clib/intuition_protos.h>
#include    <clib/layers_protos.h>

extern struct   Screen    *XW_display;
extern struct   RastPort  *XW_rp;
extern struct   Window    *XW_window;
extern struct   Library   *IntuitionBase;
extern struct   GfxBase   *GfxBase;
extern struct   Library   *LayersBase;

extern struct   Region    *XW_clipregion;

#else   /*%%%%%%%%%%%%%%%%%%%%*/


#ifdef USE_XT
#include    <X11/Intrinsic.h>    /* Intrinsics Defs */
#include    <X11/StringDefs.h>  /* Standard String Defs */
#include    <X11/Xaw/Box.h> /* Athena Command Widget ??? */

#endif


#include    <X11/Xlib.h>
#include    <X11/Xutil.h>

extern Display     *XW_display;
extern Window       XW_window;
extern Visual      *XW_visual;
extern GC           XW_gc;
extern XSizeHints   XW_hint;
extern XWMHints     XW_wmhint;
extern XFontStruct *XW_font_info;

extern char        *fontname;

extern XEvent       XW_event;
extern KeySym       XW_key;

extern Screen  *XW_screen_ptr;
extern long     XW_screen;    /* This refers to an X screen number not an Amiga
			         Screen (see XW_display above). */

#define NCOLORS 8
extern char xcolor[][NCOLORS];
extern XColor  XW_color[NCOLORS];
extern XColor  XW_exact[NCOLORS];
extern Colormap XW_cmap;


#endif            /*==========================*/
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

extern int  XW_clipped;

extern unsigned long       XW_fgnd, XW_bgnd;

extern unsigned int        XW_width;
extern unsigned int        XW_height;
extern unsigned int        XW_border;
extern unsigned int        XW_depth;

extern int  XW_number_of_colors;


/* Define some stuff global to these routines, but not the user routines. */
extern const int DEF_WIN_LEFT;
extern const int DEF_WIN_TOP;
extern const int DEF_WIN_WIDTH;
extern const int DEF_WIN_HEIGHT;

extern double txmin;;    /* virtual limits of plot region */
extern double txmax;
extern double tymin;
extern double tymax;
extern long kwidth;      /* actual width of the whole window */
extern long kheight;     /* actual height of the whole window */
extern long kxmin;       /* window limits of plot region */
extern long kxmax;
extern long kymin;
extern long kymax;
extern double dixbydx;   /* scaling factors between window & virtual coords */
extern double diybydy;

long kxbeam;           /* current ix coordinate of beam in window coords */
long kybeam;           /* current iy coordinate of beam in window coords */
double txbeam;         /* current x coordinate of beam in virtual coords */
double tybeam;         /* current y coordinate of beam in virtual coords */
double tximag;         /* unclipped virtual x coordinate of last motion */
double tyimag;         /* unclipped virtual y coordinate of last motion */


/*============================*/
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
void XWaldoEventLoop(int argc, char **argv)
{
  long i;
  char text[10];
  long done;   /* Flag for signaling the end of event loop */


  struct IntuiMessage *msg;
  unsigned long class;
  unsigned short code;
  short mousex, mousey;
  int xmup, ymup, xmdown, ymdown;
  int mup, mbutton;

/* Initialize the window. */
  XWglobal();
  XWinit(argc,argv);

  done = FALSE;
  mup=0;

  user_start(argc,argv);

  user_refresh((int) (XW_window->BorderLeft),
	       (int) (XW_window->Width        - XW_window->BorderRight),
	       (int) (XW_window->BorderTop),
	       (int) (XW_window->Height       - XW_window->BorderBottom));

  while(!done)
    {
      WaitPort(XW_window->UserPort);
      while((!done)
	    && (msg = (struct IntuiMessage *)GetMsg(XW_window->UserPort)))
	{
	  class = msg->Class;
	  code  = msg->Code;
	  mousex= msg->MouseX;
	  mousey= msg->MouseY;
	  switch (class)
	    {
	    case IDCMP_CLOSEWINDOW:
	      done = TRUE;
	      break;
	    case IDCMP_MOUSEBUTTONS:
	      if(code&IECODE_UP_PREFIX)
		{
		  xmup=mousex;
		  ymup=mousey;
		  mup=1;
		} else {
		  xmdown=mousex;
		  ymdown=mousey;
		  mup=0;
		}
	      switch (code&0x7f)
		{
		case IECODE_LBUTTON:
		  mbutton=1;
		  break;
		case IECODE_MBUTTON:
		  mbutton=2;
		  break;
		case IECODE_RBUTTON:
		  mbutton=3;
		  break;
		default:
		  mbutton=0;
		  break;
		}
	      if(mup)
		{

#ifdef TWOBUTTON
		  if(mbutton==3)
		    {
		      mbutton=2;
		    }
#endif

		  user_mouse(mbutton,xmdown,ymdown,xmup,ymup);
		}
	      break;
	    case IDCMP_REFRESHWINDOW:
	      if(XW_clipped==0)
		{
		  XWclipWindowToBorders(XW_window);
		}
	      XWerase();
	      user_refresh((int) (XW_window->BorderLeft),
			   (int) (XW_window->Width
				  - XW_window->BorderRight),
			   (int) (XW_window->BorderTop),
			   (int) (XW_window->Height
				  - XW_window->BorderBottom));

	      break;
	    case IDCMP_RAWKEY:
	      break;
	    case IDCMP_VANILLAKEY:
	      if(code=='q')
		{
		  done=1;
		} else {
		  text[0] = (char) code;
		  text[1] = (char) 0;
		  i = 1;
		  user_key(i, text);
		}
	      break;
	    }
	  ReplyMsg((struct Message *)msg);
	}
    }      
  XWfinish();
}
/*==================*/
#else    /*%%%%%%%%%%%%%%%%%%*/
/*==================*/
#ifndef USE_XT
void XWaldoEventLoop(int argc, char **argv)
{

  long i;
  char text[10];
  long done;

  Window XW_root;
  int XW_locx,XW_locy;
  int xmup, ymup, xmdown, ymdown;
  int mbutton;

  Status status;

  char name[] = "XWaldo";

/* initialize the window */
  XWglobal();
  XWinit(argc,argv);

/* main event-reading loop */
  done = 0;

  user_start(argc,argv);

  while(done==0) {
/* read the next event */
    XNextEvent(XW_display,&XW_event);
    switch(XW_event.type) {

    case Expose:		/* repaint on expose window */
      if(XW_event.xexpose.count==0) {
	status = XGetGeometry(XW_display,XW_window,&XW_root,
			      &XW_locx,&XW_locy,
			      &XW_width,&XW_height,
			      &XW_border,&XW_depth);
	if(status){
	  XWsetregion(0,XW_width,0,XW_height);
	  user_refresh((int) 0, XW_width, (int) 0, XW_height);
	}
      }
     break;

    case MappingNotify:		/* process keyboard mapping changes */
      XRefreshKeyboardMapping((XMappingEvent *) &XW_event);
      break;

    case ButtonPress:		/* process mouse-button presses */
      xmdown = XW_event.xbutton.x;
      ymdown = XW_event.xbutton.y;

/* Note there is a problem here */

    case ButtonRelease:		/* process mouse-button releases */
      xmup = XW_event.xbutton.x;
      ymup = XW_event.xbutton.y;
      switch (XW_event.xbutton.button)
	{
	case Button1:
	  mbutton = 1;
	  break;

	case Button2:
	  mbutton = 2;
	  break;

	case Button3:
	  mbutton = 3;
	  break;

	case Button4:
	  mbutton = 4;
	  break;

	case Button5:
	  mbutton = 5;
	  break;

	default:
	  mbutton = 0;
	}

      user_mouse(mbutton, xmdown, ymdown, xmup, ymup);
      break;

    case KeyPress:		/* process keyboard input */
      i = XLookupString((XKeyEvent *)&XW_event,text,10,&XW_key,0);
      if(i==1&&text[0]=='q'){
	done = 1;
      } else {
	user_key(i,text);
      }
      break;

    } /* switch(XW_event.type) */

  } /* while (done==0) */

/* termination */
  XWfinish();
  exit(0);
}

#else  /*% % % % % % % % % % % % %*/

        /* for Xt version */  /* Glish needs the damn widgets */
        /* What can I say?  X is rather stupid in not having a simple */
        /* stdin XEvent type, ay what? */
/*============================*/
/* Define some prototypes.  The actual functions are in XWglobal.c */

XtActionProc Mouse1_Proc(Widget w, XButtonEvent *event,
		String *params, Cardinal num_params);
XtActionProc Mouse2_Proc(Widget w, XButtonEvent *event,
		String *params, Cardinal num_params);
XtActionProc Mouse3_Proc(Widget w, XButtonEvent *event,
		 String *params, Cardinal num_params);
XtActionProc Key_Proc(Widget w, XKeyEvent *event, String *params,
		      Cardinal num_params);
XtActionProc Expose_Proc(Widget w, XExposeEvent *event,
		 String *params, Cardinal num_params);
void Stdin_Proc(XtPointer client_data, int *fid, XtInputId *id);
void XWflush(void);
/*============================*/
XtActionProc GQuit(Widget w, XKeyEvent *event,
	      String *params, Cardinal num_params)
{
  printf("quit\n");
  exit(0);
}
/*============================*/
void getargfont(int argc, char **argv);
/*============================*/
void XWaldoEventLoop(int argc, char **argv)
{
  XtAppContext app_context;
  Widget topLevel, plot_window;
  static XtActionsRec stufftodo[] =
    {
      {"zoomleft",  (XtActionProc)Mouse1_Proc},
      {"zoomright", (XtActionProc)Mouse2_Proc},
      {"unzoom",    (XtActionProc)Mouse3_Proc},
      {"expose",    (XtActionProc)Expose_Proc},
      {"quit",      (XtActionProc)GQuit},
      {"key",       (XtActionProc)Key_Proc}
    };

#ifdef QUACK
  String trans = "#augment\n\
     <Expose>:            expose()\n\
     <Configure>:         expose()\n\
     <Key>q:              quit()\n\
     <Key>:               key()";  /*\n\
     <Btn1Down>,<Btn1Up>: zoomleft()\n\
     <Btn2Down>,<Btn2Up>: zoomright()\n\
     <Btn3Down>,<Btn3Up>: unzoom()"; */
#else
  String trans = "#augment\n\
     <Expose>:            expose()\n\
     <Configure>:         expose()\n\
     <Key>q:              quit()\n\
     <Key>:               key()\n\
     <Btn1Down>,<Btn1Up>: zoomleft()\n\
     <Btn2Down>,<Btn2Up>: zoomright()\n\
     <Btn3Down>,<Btn3Up>: unzoom()";
#endif

  XWglobal();
  getargfont(argc, argv);

  topLevel = XtVaAppInitialize
    (&app_context,  /* Application Context */
     "XWaldo",      /* Application Class   */
     NULL, 0,       /* command line option list */
     &argc, argv,   /* command line args */
     NULL,          /* for missing app-defaults file */
     NULL);         /* terminate varargs list */

  plot_window = XtVaCreateManagedWidget
    ("plotwindow",  /* widget name */
     boxWidgetClass,/* widget class from */
     topLevel,      /* parent widget */
/*     XtNtranslations, XtParseTranslationTable(trans), */
     NULL);         /* terminate varargs list */

  XtAugmentTranslations(plot_window,XtParseTranslationTable(trans));

  XtAppAddActions(app_context, stufftodo, XtNumber(stufftodo));

  XtRealizeWidget(topLevel);

  XW_display    = XtDisplay(plot_window);
  XW_window     = XtWindow(plot_window);
  XW_screen_ptr = XtScreen(plot_window);
  XW_screen     = XScreenNumberOfScreen(XW_screen_ptr);

  XW_gc      = XCreateGC(XW_display, XW_window, 0,0); /* mask, &values); */
  XW_bgnd = BlackPixel(XW_display,XW_screen);
  XW_fgnd = WhitePixel(XW_display,XW_screen);

  XSetWindowBackground(XW_display,XW_window,XW_bgnd);

  XSetBackground(XW_display,XW_gc,XW_bgnd);
  XSetForeground(XW_display,XW_gc,XW_fgnd);

  /* set up the font */
  if((XW_font_info = XLoadQueryFont(XW_display,fontname)) == NULL)
    {
      fprintf(stderr,"main: Could not find font '%s'.\n",fontname);
      exit(0);
    }
  XSetFont(XW_display,XW_gc,XW_font_info->fid);

  XWsetscale();          /* set the scaling of the window */
  XWgetsomecolors();     /* set up some colors */

  user_start(argc,argv);

#ifdef GLISH
  MainEventLoop(app_context, XW_display, gflag);
#else
  XtAppAddInput(app_context, fileno(stdin), (XtPointer)XtInputReadMask,
		Stdin_Proc, NULL);
  XtAppMainLoop(app_context);
#endif
}
#endif
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

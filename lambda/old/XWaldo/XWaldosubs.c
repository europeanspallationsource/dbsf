/* $Header: /rap/lambda/XWaldo/RCS/XWaldosubs.c,v 1.1 1995/06/20 13:57:47 mackay Exp mackay $ */
char string[10];  /* For debugging purposes only. */
/* #define WHOBEI 1 */

/* Do we want to use the Xtoolkit version ?  If not zap the following line */
/* #define USE_XT 1 */   /* can use the -DUSE_XT switch to gcc */
/* USE_XT is defined in include/XWaldo.h except for the Amiga */

/*-------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <XWaldo.h>

int read(int fd, char *buf, int n);

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA     /*==========================*/

#define     INTUI_V36_NAMES_ONLY 1        /* don't load obsolete names */
#include    <exec/types.h>
#include    <exec/memory.h>
#include    <intuition/intuitionbase.h>
#include    <graphics/gfxmacros.h>
#include    <graphics/displayinfo.h>
#include    <intuition/screens.h>

#include    <clib/dos_protos.h>
#include    <clib/exec_protos.h>
#include    <clib/graphics_protos.h>
#include    <clib/intuition_protos.h>
#include    <clib/layers_protos.h>

extern struct   Screen    *XW_display;
extern struct   RastPort  *XW_rp;
extern struct   Window    *XW_window;
extern struct   Library   *IntuitionBase;
extern struct   GfxBase   *GfxBase;
extern struct   Library   *LayersBase;

struct   Region    *XW_clipregion = NULL;

#else   /*%%%%%%%%%%%%%%%%%%%%*/


#ifdef USE_XT
#include    <X11/Intrinsic.h>    /* Intrinsics Defs */
#include    <X11/StringDefs.h>  /* Standard String Defs */
#include    <X11/Xaw/Box.h> /* Athena Command Widget ??? */

#endif


#include    <X11/Xlib.h>
#include    <X11/Xutil.h>

extern Display     *XW_display;
extern Window       XW_window;
extern Visual      *XW_visual;
extern GC           XW_gc;
extern XSizeHints   XW_hint;
extern XWMHints     XW_wmhint;
extern XFontStruct *XW_font_info;

extern char        *fontname;

extern XEvent              XW_event;
extern KeySym              XW_key;

extern Screen  *XW_screen_ptr;
extern long     XW_screen;    /* This refers to an X screen number not an Amiga
			  Screen (see XW_display above). */

#define NCOLORS 8
extern char xcolorname[][NCOLORS];
extern XColor  XW_color[NCOLORS];
extern XColor  XW_exact[NCOLORS];
extern Colormap XW_cmap;


#endif            /*==========================*/
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

extern int  XW_clipped;

extern unsigned long       XW_fgnd, XW_bgnd;

extern unsigned int        XW_width;
extern unsigned int        XW_height;
extern unsigned int        XW_border;
extern unsigned int        XW_depth;

extern int  XW_number_of_colors;


/* Define some stuff global to these routines, but not the user routines. */
#define DEF_WIN_LEFT   0
#define DEF_WIN_TOP    0
#define DEF_WIN_WIDTH  400
#define DEF_WIN_HEIGHT 250

extern double txmin;    /* virtual limits of plot region */
extern double txmax;
extern double tymin;
extern double tymax;
extern long kwidth;     /* actual width of the whole window */
extern long kheight;    /* actual height of the whole window */
extern long kxmin;      /* window limits of plot region */
extern long kxmax;
extern long kymin;
extern long kymax;
extern double dixbydx;    /* scaling factors between window & virtual coords */
extern double diybydy;

extern long kxbeam;       /* current ix coordinate of beam in window coords */
extern long kybeam;       /* current iy coordinate of beam in window coords */
extern double txbeam;     /* current x coordinate of beam in virtual coords */
extern double tybeam;     /* current y coordinate of beam in virtual coords */
extern double tximag;     /* unclipped virtual x coordinate of last motion */
extern double tyimag;     /* unclipped virtual y coordinate of last motion */


/* for debuggery */
#define CLIPBUG 1

#ifdef CLIPBUG
static long lowx  = 0;
static long highx = 0;
static long lowy  = 0;
static long highy = 0;
#endif

/*==========================*/
/*==========================*/
void XWsetscale(void)
{
  dixbydx = (double)(kxmax - kxmin) / (txmax - txmin);
  diybydy = (double)(kymax - kymin) / (tymax - tymin);
}
/*==========================*/
void XWsetregion(long x0, long x1, long y0, long y1)
{
  kxmin = x0;
  kxmax = x1;
  kymax = y0;
  kymin = y1;
  XWsetscale();
}
/*==========================*/
void XWsetvirtual(double x0, double x1, double y0, double y1)
{
  txmin = x0;
  txmax = x1;
  tymin = y0;
  tymax = y1;
  XWsetscale();
}
/*==========================*/
void XWscale(double x, double y, long *ix, long *iy)
{
  *ix = kxmin + (x-txmin) * dixbydx;
  *iy = kymin + (y-tymin) * diybydy;
} /* XWscale */
/*==========================*/
void XWunscale(long ix, long iy, double *x, double *y)
{
  *x = txmin + (ix-kxmin) / dixbydx;
  *y = tymin + (iy-kymin) / diybydy;
}
/*=========================*/
void XWmovabs(long ix, long iy)
{
#ifdef _AMIGA
  Move(XW_rp,ix,iy);
#endif
  kxbeam = ix;
  kybeam = iy;
}
/*=========================*/
void XWdrwabs(long ix, long iy)
{
#ifdef _AMIGA
  Draw(XW_rp,ix,iy);
#else
  XDrawLine(XW_display,XW_window,XW_gc,kxbeam,kybeam,ix,iy);
#endif
  kxbeam = ix;
  kybeam = iy;


#ifdef CLIPBUG
  if(kxbeam < lowx) lowx = kxbeam;
  if(kybeam < lowy) lowy = kybeam;
  if(kxbeam > highx) highx = kxbeam;
  if(kybeam > highy) highy = kybeam;
  if(ix < lowx) lowx = ix;
  if(iy < lowy) lowy = iy;
  if(ix > highx) highx = ix;
  if(iy > highy) highy = iy;
#endif
  



}
/*==========================*/
void XWmovrel(long ix, long iy)
{
  kxbeam = kxbeam + ix;
  kybeam = kybeam + iy;
}
/*=========================*/
void XWdrwrel(long ix, long iy)
{
  XWdrwabs(ix+kxbeam,iy+kybeam);
}
/*=========================*/
void XWmovea(double x, double y)
{
  long ix, iy;

  XWscale(x,y,&ix,&iy);
  tximag = x;
  tyimag = y;
  XWmovabs(ix,iy);
}
/*=========================*/
void XWdrawa(double x, double y)
{
  long ix, iy;

  XWscale(x,y,&ix,&iy);
  XWdrwabs(ix,iy);

  tximag = x;
  tyimag = y;
}
/*=========================*/
void XWmover(double x, double y)
{
  XWmovea(tximag+x, tyimag+y);
}
/*==========================*/
void XWdrawr(double x,double y)
{
  XWdrawa(tximag+x, tyimag+y);
}
/*==========================*/
void XWlblabs(long ix, long iy, char *text, int ipos)
{
  int xl,xr,yb,yu;
  int dx, dy;

#ifdef _AMIGA
  struct TextExtent te;

  TextExtent(XW_rp,text,strlen(text),&te);
  xl = te.te_Extent.MinX;
  xr = te.te_Extent.MaxX;
  yu = te.te_Extent.MinY;
  yb = te.te_Extent.MaxY;
  
#else

  int direction, ascent, descent;
  XCharStruct overall;

  XTextExtents(XW_font_info,text,strlen(text),&direction,&ascent,&descent
	       ,&overall);
  xl = overall.lbearing;
  xr = overall.rbearing;
  yu = -overall.ascent;
  yb = overall.descent;

#endif

  switch (ipos)
    {
    case (1):
      dx = -xr;
      dy = -yu;
      break;

    case (2):
      dx = -(xl + xr)/2;
      dy = -yu;
      break;

    case (3):
      dx = xl;
      dy = -yu;
      break;

    case (4):
      dx = -xr;
      dy = -(yu + yb)/2;
      break;

    case (5):
      dx = -(xl + xr)/2;
      dy = -(yu + yb)/2;
      break;

    case (6):
      dx = -xl;
      dy = -(yu + yb)/2;
      break;

    case (7):
      dx = -xr;
      dy = -yb;
      break;

    case (8):
      dx = -(xl + xr)/2;
      dy = -yb;
      break;

    case (9):
      dx = -xl;
      dy = -yb;
      break;
    }

#ifdef _AMIGA
  XWmovabs(ix+dx,iy+dy);
  Text(XW_rp,text,strlen(text));
#else
  XDrawString(XW_display,XW_window,XW_gc,ix+dx,iy+dy,text,strlen(text));
#endif
  XWmovabs(ix,iy);
}
/*==========================*/
void XWlblrel(long ix, long iy, char *text, int ipos)
{
  XWlblabs(ix+kxbeam,iy+kxbeam,text,ipos);
}
/*==========================*/
void XWlabela(double x, double y, char *text, int ipos)
{
  long ix, iy;

  XWscale(x,y,&ix,&iy);
  XWlblabs(ix,iy,text,ipos);
}
/*==========================*/
void XWlabelr(double x, double y, char *text, int ipos)
{
  XWlabela(tximag+x,tyimag+y,text,ipos);
}
/*==========================*/

void XWlinea(double x0, double y0, double x1, double y1)
{
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
  XWmovea(x0,y0);
  XWdrawa(x1,y1);
#else    /*%%%%%%%%%%%%*/
  long ix0 = 0;
  long iy0 = 0;
  long ix1 = 0;
  long iy1 = 0;

  XWscale(x0,y0,&ix0,&iy0);
  XWscale(x1,y1,&ix1,&iy1);
  XDrawLine(XW_display,XW_window,XW_gc,ix0,iy0,ix1,iy1);
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
  }
/*================================*/
void XWfinish(void)
{
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
  if(XW_clipregion != NULL)
    {
      XW_clipregion = InstallClipRegion(XW_window->WLayer,NULL);
      if(XW_clipregion != NULL) DisposeRegion(XW_clipregion);
    }
  if(XW_window     != NULL) CloseWindow(XW_window);
  if(LayersBase    != NULL) CloseLibrary(LayersBase);
  if(GfxBase       != NULL) CloseLibrary((struct Library *)GfxBase);
  if(IntuitionBase != NULL) CloseLibrary((struct Library *)IntuitionBase);
#else       /*%%%%%%%%%%%%%%%*/
#ifdef USE_XT
#else
  XFreeGC(XW_display,XW_gc);
  XDestroyWindow(XW_display,XW_window);
  XCloseDisplay(XW_display);
#endif
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
}
/*=========================*/
void XW_bomb(char *errmess)
{
  fprintf(stderr,"%s\n",errmess);
  XWfinish();
  exit(0);
}
/*=========================*/
void XWerase(void)
{
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
  SetRast(XW_rp,0);
  RefreshWindowFrame(XW_window);
#else
  XClearWindow(XW_display,XW_window);
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
}
/*=========================*/
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
void XWunclipWindow(struct Window *win)
{
  struct Region     *old_region;

/* Remove any old region by installing a NULL region,
** then dispose of the old region if one was installed.
*/
  if (NULL != (old_region = InstallClipRegion(win->WLayer, NULL)))
    DisposeRegion(old_region);
}
#else
void XWunclipWindow(Window win)
{
}
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
/*=========================*/
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
void XWclipWindow(struct Window *win,
	     long minX, long minY, long maxX, long maxY)
{
/* see pp 723-724 of Amiga RKRM Libraries */
  struct Rectangle rect;

  rect.MinX = minX;   /* set up the limits for the clip */
  rect.MaxX = maxX;
  rect.MinY = minY;
  rect.MaxY = maxY;

  if(XW_clipregion!=NULL)
    {
      XWunclipWindow(XW_window); /* trash any old clip region */
    }

  XW_clipregion = NewRegion(); /* get new region and OR in the limits */

  if(XW_clipregion != NULL)
    {
      if(NULL == OrRectRegion(XW_clipregion,&rect))
	{
	  DisposeRegion(XW_clipregion);
	  XW_clipregion = NULL;
	}
    }
  InstallClipRegion(XW_window->WLayer, XW_clipregion);
}
#else   /*%%%%%%%%%%%%%%%%*/
void XWclipWindow(Window win,
			  long minX, long minY, long maxX, long maxY)
{
  XRectangle  rect[1];

  rect[0].x = minX;
  rect[0].y = minY;
  rect[0].width = maxX - minX;
  rect[0].height = maxY - minY;
  XSetClipRectangles(XW_display,XW_gc,(int) 0,(int) 0,rect,(int) 1,YSorted);
}
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
/*=========================*/
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA
void XWclipWindowToBorders(struct Window *win)
{
  XWclipWindow(win,
	       win->BorderLeft,
	       win->BorderTop,
	       win->Width  - win->BorderRight  - 1,
	       win->Height - win->BorderBottom - 1);
}
#else   /*%%%%%%%%%%%%%%%%*/
void XWclipWindowToBorders(Window win)
{
  XSetClipMask(XW_display,XW_gc,None);
}
#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
/*=========================*/
void XWsetclip(int on)
{
  if(on)
    {
      XWclipWindow(XW_window, kxmin, kymax, kxmax, kymin);
      XW_clipped = 1;
    } else {
      XWclipWindowToBorders(XW_window);
      XW_clipped = 0;
    }
}  
/*=========================*/
void XWboxregion(void)
{
  XWmovabs(kxmin,kymin);
  XWdrwabs(kxmin,kymax);
  XWdrwabs(kxmax,kymax);
  XWdrwabs(kxmax,kymin);
  XWdrwabs(kxmin,kymin);
}
/*=========================*/
void XWgetsomecolors(void)
{
#ifdef _AMIGA   /*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
/* Color #0 is the background color that to which we erase. */
/* All of the colors will be whatever has been defined for the WorkBench */
/* screen. */

  struct DrawInfo *screeninfo;

  screeninfo = GetScreenDrawInfo(XW_display);
  if(screeninfo==0)
    {
      fprintf(stderr,"XWgetsomecolors could not GetScreenDrawInfo\n");
      XW_number_of_colors = 1;
    } else {
      XW_number_of_colors = pow(2.0,(double)screeninfo->dri_Depth);
    }
  SetAPen(XW_rp,1);   /* start with color #1 */


#else           /*%%%%%%%%%%%%%*/

  int depth;
  int class;
  int i;

  depth = DefaultDepth(XW_display,XW_screen);

  XW_visual = DefaultVisual(XW_display,XW_screen);
  class = XW_visual->class;
  if(depth == 1)
    {
      
      return;  /* a monochrome screen */
    }
#ifdef NONONO
  if(class != PseudoColor)
    {
      return;  /* for now assume a monochrome screen */
    }
#endif
  XW_cmap = DefaultColormap(XW_display,XW_screen);

  for(i=0; i<NCOLORS; i++)
    {
      if(XAllocNamedColor(XW_display,XW_cmap,
			  xcolorname[i],&XW_color[i],&XW_exact[i])==0)
	 {
	   fprintf(stderr,"XWgetsomecolors: Could not allocate '%s'",
		   xcolorname[i]);
	 }
      XW_number_of_colors = i+1;
    }

#endif          /*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
}
/*========================*/
void XWsetpen(int pen)
{
  int i;

  if(pen>=0 && pen<XW_number_of_colors)
    {
      i = pen;
    } else {
      i = 1;
    }
#ifdef _AMIGA       /*%%%%%%%%%%%%%%%%%%%%%%%*/
  SetAPen(XW_rp,i);
#else               /*%%%%%%%%%*/
  XSetForeground(XW_display,XW_gc,XW_color[i].pixel);
#endif              /*%%%%%%%%%%%%%%%%%%%%%%%*/
}
/*=========================*/
void XWinit(int argc, char **argv)
{
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#ifdef _AMIGA  /* for Intuition version */
  static char windowtitle[] = "XWaldo";

  struct TagItem win_tags[]=
    {
      {WA_Left,  DEF_WIN_LEFT},
      {WA_Top,   DEF_WIN_TOP},
      {WA_Width, DEF_WIN_WIDTH},
      {WA_Height,DEF_WIN_HEIGHT},
      {WA_MinWidth, 20},
      {WA_MinHeight,20},
      {WA_MaxWidth, 640},        /* Perhaps this should not be defined */
      {WA_MaxHeight,400},        /* Perhaps this should not be defined */
      {WA_CloseGadget,TRUE},
      {WA_SizeGadget,TRUE},
      {WA_DragBar,TRUE},
      {WA_DepthGadget,TRUE},
      {WA_RMBTrap,TRUE},         /* Trap the right mouse (menu) button */
      {WA_Title,(long) windowtitle},
      {WA_IDCMP,IDCMP_CLOSEWINDOW | IDCMP_MOUSEBUTTONS |
	        IDCMP_VANILLAKEY | IDCMP_RAWKEY |
		IDCMP_REFRESHWINDOW },
      {TAG_DONE}
    };


  IntuitionBase = OpenLibrary("intuition.library",37);
  if(IntuitionBase==NULL)
    {
      XW_bomb("Could not open 'intuition.library' version>=37.\n");
    }

  GfxBase = (struct GfxBase *)OpenLibrary("graphics.library",37);
  if(GfxBase==NULL)
    {
      XW_bomb("Could not open 'graphics.library' version>=37.\n");
    }

  LayersBase = OpenLibrary("layers.library",37);
  if(LayersBase==NULL)
    {
      XW_bomb("Could not open 'layers.library' version>=37.\n");
    }

  if(XW_display = LockPubScreen("Workbench"))
    {
      XW_window = OpenWindowTagList(NULL,win_tags);
      UnlockPubScreen(NULL, XW_display);
    } else {
      XW_bomb("XWinit: Could not lock public screen.\n");
    }

  if(XW_window == 0)
    {
      XW_bomb("XWinit: Could not open window.\n");
    } else {
      XW_rp = XW_window->RPort;
    }
  XWclipWindowToBorders(XW_window);
  XW_clipped = 0;

  SetDrMd(XW_rp,JAM1); /* Fix text to only add to plot,i.e., ignore BgPen */

#else       /*%%%%%%%%%%%%%%%*/

/* initialization */    /*  straight Xlib verion:  We don't need no stinking */
                        /*  widgets! */
  XW_display = XOpenDisplay("");
  XW_screen  = DefaultScreen(XW_display);

/* default pixel values */
  XW_bgnd = BlackPixel(XW_display,XW_screen);
  XW_fgnd = WhitePixel(XW_display,XW_screen);

/* default program-specified window position and size */
  XW_hint.x = 200;
  XW_hint.y = 300;
  XW_hint.width = DEF_WIN_WIDTH;
  XW_hint.height = DEF_WIN_HEIGHT;
  XW_hint.flags = PPosition | PSize;
  XW_wmhint.flags = InputHint;
  XW_wmhint.input = True;

/* window creation */
  XW_window = XCreateSimpleWindow(XW_display,
				  DefaultRootWindow(XW_display),
				  XW_hint.x,XW_hint.y,XW_hint.width,
				  XW_hint.height,5,XW_fgnd,
				  XW_bgnd);

#ifdef WHOBEI
  fprintf(stderr,"XW_display=0x%x\n",XW_display);
  fprintf(stderr,"XW_window =0x%x\n",XW_window);
#endif

  XSetStandardProperties(XW_display,XW_window,argv[0],argv[0]
			 ,None,argv,argc,&XW_hint);
  XSetWMHints(XW_display,XW_window,&XW_wmhint);

/* Get the font information */
  if((XW_font_info = XLoadQueryFont(XW_display,"fixed")) == NULL)
    {
      fprintf(stderr,"XWinit: could not find font '%s'.\n",fontname);
      exit(0);
    }

/* GC creation and initialization */
  XW_gc = XCreateGC(XW_display,XW_window,0,0);
  XSetBackground(XW_display,XW_gc,XW_bgnd);
  XSetForeground(XW_display,XW_gc,XW_fgnd);

/* set up the font */
  XSetFont(XW_display,XW_gc,XW_font_info->fid);

/* input event selection */
  XSelectInput(XW_display,XW_window
	       ,ButtonPressMask|KeyPressMask|ExposureMask);

/* window mapping */
  XMapRaised(XW_display,XW_window);

/* solicit both ButtonPress and ButtonRelease events */
/**  XSelectInput(XW_display,XW_window,
	       ButtonPressMask | ButtonReleaseMask);   ?? doesn't work **/

#endif
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

/* set the scaling of the window */
  XWsetscale();

/* set up some colors */
  XWgetsomecolors();
}
/*=============================*/
int eatswitch(int *argc, char** argv, char* swtch)
{ /* This function scans the argument list for 'swtch' and removes it from
  ** the list.  It returns a 1 if 'swtch' was found, or a 0 if not.
  */

  int i, j;

  if(*argc>1)
    {
      for(i=1;i<*argc;i++)
	{
	  if(strcmp(argv[i],swtch)==0)
	    {
	      for(j=i;j<*argc-1;j++)
		{
		  argv[j] = argv[j+1];
		}
	      (*argc)--;
	      return 1;
	    }
	}
    }
  return 0;
}

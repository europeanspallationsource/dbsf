;GIL-2
(
(
	:type                   :menu
	:name                   latticesMenu
	:help                   ""
	:columns                1
	:label                  ""
	:label-type             :string
	:menu-type              :command
	:menu-handler           nil
	:menu-title             "Sites"
	:menu-item-labels       ("FNAL" )
	:menu-item-label-types  (:string )
	:menu-item-defaults     (t )
	:menu-item-handlers     (nil )
	:menu-item-menus        (FNALsubMenu )
	:menu-item-colors       ("" )
	:pinnable               nil
	:user-data              ()
)
(
	:type                   :menu
	:name                   FNALsubMenu
	:help                   ""
	:columns                1
	:label                  ""
	:label-type             :string
	:menu-type              :command
	:menu-handler           nil
	:menu-title             ""
	:menu-item-labels       ("Tevatron" "Main Injector" "Main Ring" "Accumulator" "Booster" "8 GeV beamline" "MI to Tev" )
	:menu-item-label-types  (:string :string :string :string :string :string :string )
	:menu-item-defaults     (t nil nil nil nil nil nil )
	:menu-item-handlers     (nil nil mainRingHandler accumulatorHandler boosterHandler eightGevHandler mi2tevHandler )
	:menu-item-menus        (TEVsubMenu MIsubMenu nil nil nil nil nil )
	:menu-item-colors       ("" "" "" "" "" "" "" )
	:pinnable               nil
	:user-data              ()
)
(
	:type                   :menu
	:name                   functionMenu
	:help                   ""
	:columns                1
	:label                  ""
	:label-type             :string
	:menu-type              :command
	:menu-handler           nil
	:menu-title             "LAMBDA functions"
	:menu-item-labels       ("dbsf" "twiss" )
	:menu-item-label-types  (:string :string )
	:menu-item-defaults     (nil nil )
	:menu-item-handlers     (dbsfHandler twissHandler )
	:menu-item-menus        (nil nil )
	:menu-item-colors       ("" "" )
	:pinnable               nil
	:user-data              ()
)
(
	:type                   :menu
	:name                   TEVsubMenu
	:help                   ""
	:columns                1
	:label                  ""
	:label-type             :string
	:menu-type              :command
	:menu-handler           nil
	:menu-title             ""
	:menu-item-labels       ("Injection" "Low Beta" )
	:menu-item-label-types  (:string :string )
	:menu-item-defaults     (nil t )
	:menu-item-handlers     (TevInjectionHandler TevlowBetaHandler )
	:menu-item-menus        (nil nil )
	:menu-item-colors       ("" "" )
	:pinnable               nil
	:user-data              ()
)
(
	:type                   :menu
	:name                   formatMenu
	:help                   ""
	:columns                1
	:label                  ""
	:label-type             :string
	:menu-type              :command
	:menu-handler           nil
	:menu-title             "Formats"
	:menu-item-labels       ("SDS" "MAD" "SYNCH" "FLAT" )
	:menu-item-label-types  (:string :string :string :string )
	:menu-item-defaults     (t nil nil nil )
	:menu-item-handlers     (formatSDSHandler formatMADHandler formatSYNCHHandler formatFLATHandler )
	:menu-item-menus        (nil nil nil nil )
	:menu-item-colors       ("" "" "" "" )
	:pinnable               nil
	:user-data              ()
)
(
	:type                   :menu
	:name                   MIsubMenu
	:help                   ""
	:columns                1
	:label                  ""
	:label-type             :string
	:menu-type              :command
	:menu-handler           nil
	:menu-title             ""
	:menu-item-labels       ("17" "16" "15" )
	:menu-item-label-types  (:string :string :string )
	:menu-item-defaults     (t nil nil )
	:menu-item-handlers     (mi17Handler mi16Handler mi15Handler )
	:menu-item-menus        (nil nil nil )
	:menu-item-colors       ("" "" "" )
	:pinnable               nil
	:user-data              ()
)
(
	:type                   :menu
	:name                   helpMenu
	:help                   ""
	:columns                1
	:label                  ""
	:label-type             :string
	:menu-type              :command
	:menu-handler           nil
	:menu-title             ""
	:menu-item-labels       ("Lattices" "LAMBDA functions" "Files" )
	:menu-item-label-types  (:string :string :string )
	:menu-item-defaults     (nil nil nil )
	:menu-item-handlers     (helpLatticesHandler helpFunctionsHandler helpFilesHandler )
	:menu-item-menus        (nil nil nil )
	:menu-item-colors       ("" "" "" )
	:pinnable               nil
	:user-data              ()
)
(
	:type                   :base-window
	:name                   firstWindow
	:owner                  nil
	:width                  632
	:height                 186
	:background-color       ""
	:foreground-color       ""
	:label                  "LAMBDA"
	:label-type             :string
	:mapped                 nil
	:show-footer            t
	:resizable              t
	:icon-file              "lambda.icon"
	:icon-mask-file         ""
	:event-handler          nil
	:events                 ()
	:user-data              ()
)
(
	:type                   :control-area
	:name                   controls1
	:owner                  firstWindow
	:help                   ""
	:x                      0
	:y                      0
	:width                  632
	:height                 186
	:background-color       ""
	:foreground-color       ""
	:show-border            nil
	:menu                   nil
	:event-handler          nil
	:events                 ()
	:user-data              ()
)
(
	:type                   :button
	:name                   helpButton
	:owner                  controls1
	:help                   ""
	:x                      24
	:y                      24
	:constant-width         nil
	:button-type            :normal
	:width                  105
	:height                 19
	:foreground-color       ""
	:label                  "   Help . . .   "
	:label-type             :string
	:menu                   helpMenu
	:notify-handler         nil
	:event-handler          nil
	:events                 ()
	:user-data              ()
)
(
	:type                   :message
	:name                   message1
	:owner                  controls1
	:help                   ""
	:x                      208
	:y                      24
	:width                  117
	:height                 13
	:foreground-color       ""
	:label                  "Current settings "
	:label-type             :string
	:label-bold             t
	:event-handler          nil
	:events                 ()
	:user-data              ()
)
(
	:type                   :text-field
	:name                   latticeTxt
	:owner                  controls1
	:help                   ""
	:x                      236
	:y                      48
	:width                  243
	:height                 15
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Lattice                 "
	:label-type             :string
	:value-x                359
	:value-y                48
	:layout-type            :horizontal
	:value-length           15
	:stored-length          80
	:read-only              t
	:notify-handler         nil
	:event-handler          nil
	:events                 ()
	:user-data              ()
)
(
	:type                   :button
	:name                   latticeButton
	:owner                  controls1
	:help                   ""
	:x                      24
	:y                      72
	:constant-width         nil
	:button-type            :normal
	:width                  96
	:height                 19
	:foreground-color       ""
	:label                  "   Lattice   "
	:label-type             :string
	:menu                   latticesMenu
	:notify-handler         nil
	:event-handler          nil
	:events                 ()
	:user-data              ()
)
(
	:type                   :text-field
	:name                   filenameTxt
	:owner                  controls1
	:help                   ""
	:x                      236
	:y                      73
	:width                  361
	:height                 15
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Lattice filename"
	:label-type             :string
	:value-x                357
	:value-y                73
	:layout-type            :horizontal
	:value-length           30
	:stored-length          80
	:read-only              nil
	:notify-handler         filenameHandler
	:event-handler          nil
	:events                 ()
	:user-data              ()
)
(
	:type                   :text-field
	:name                   formatTxt
	:owner                  controls1
	:help                   ""
	:x                      236
	:y                      98
	:width                  203
	:height                 15
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "File format:         "
	:label-type             :string
	:value-x                359
	:value-y                98
	:layout-type            :horizontal
	:value-length           10
	:stored-length          80
	:read-only              t
	:notify-handler         nil
	:event-handler          nil
	:events                 ()
	:user-data              ()
)
(
	:type                   :button
	:name                   fileFormatButton
	:owner                  controls1
	:help                   ""
	:x                      24
	:y                      104
	:constant-width         nil
	:button-type            :normal
	:width                  123
	:height                 19
	:foreground-color       ""
	:label                  "   File format   "
	:label-type             :string
	:menu                   formatMenu
	:notify-handler         nil
	:event-handler          nil
	:events                 ()
	:user-data              ()
)
(
	:type                   :button
	:name                   functionButton
	:owner                  controls1
	:help                   ""
	:x                      24
	:y                      136
	:constant-width         nil
	:button-type            :normal
	:width                  166
	:height                 19
	:foreground-color       ""
	:label                  "   LAMBDA functions   "
	:label-type             :string
	:menu                   functionMenu
	:notify-handler         nil
	:event-handler          nil
	:events                 ()
	:user-data              ()
)
(
	:type                   :message
	:name                   message2
	:owner                  controls1
	:help                   ""
	:x                      452
	:y                      144
	:width                  143
	:height                 13
	:foreground-color       ""
	:label                  "About LAMBDA  .  .  .  "
	:label-type             :string
	:label-bold             t
	:event-handler          aboutHandler
	:events                 ()
	:user-data              ()
)
)

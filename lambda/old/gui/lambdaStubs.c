#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/panel.h>
#include <xview/textsw.h>
#include <xview/xv_xrect.h>
#include <gdd.h>
#include "lambdaGUI.h"

/*
 * Instance XV_KEY_DATA key.  An instance is a set of related
 * user interface objects.  A pointer to an object's instance
 * is stored under this key in every object.  This must be a
 * global variable.
 */
Attr_attribute	INSTANCE;

struct {					      /* LambdaGlobals definition */
  char commandLine[160];			      /* LambdaGlobals definition */
  char latticeFileName[160];			      /* LambdaGlobals definition */
  char latticeIdent[160];			      /* LambdaGlobals definition */
  char dbCall[10];                                    /* LambdaGlobals definition */
  enum { none, sds, mad, synch, flat } fileFormat;    /* LambdaGlobals definition */
} LambdaGlobals;                                      /* LambdaGlobals definition */


void main(argc, argv)
	int		argc;
	char		**argv;
{
lambda_firstWindow_objects	*lambda_firstWindow;

/*
 * Initialize Globals
 */
LambdaGlobals.commandLine[0]       = '\0';
LambdaGlobals.latticeFileName[0]   = '\0';
LambdaGlobals.latticeIdent[0]      = '\0';
LambdaGlobals.dbCall[0]            = '\0';
LambdaGlobals.fileFormat           = none;

/*
 * Initialize XView.
 */
xv_init(XV_INIT_ARGC_PTR_ARGV, &argc, argv, 0);
INSTANCE = xv_unique_key();

/*
 * Initialize user interface components.
 */
lambda_firstWindow = lambda_firstWindow_objects_initialize(NULL, NULL);

/*
 * Turn control over to XView.
 */
xv_main_loop(lambda_firstWindow->firstWindow);
exit(0);
}

/*
 * Menu handler for `FNALsubMenu (Main Ring)'.
 */
Menu_item  mainRingHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_NOTIFY:
    strcpy( LambdaGlobals.latticeIdent, "mr" );
    xv_set( ip->latticeTxt,
      PANEL_VALUE, "mr", 
      NULL );
    strcpy( LambdaGlobals.dbCall, "dbsf" );
    break;

  default:
    break;
  }
  return item;
}

/*
 * Menu handler for `FNALsubMenu (Accumulator)'.
 */
Menu_item  accumulatorHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_NOTIFY:
    strcpy( LambdaGlobals.latticeIdent, "accumulator" );
    xv_set( ip->latticeTxt,
      PANEL_VALUE, "accumulator", 
      NULL );
    strcpy( LambdaGlobals.dbCall, "dbsf" );
    break;

  default:
    break;
  }
  return item;
}

/*
 * Menu handler for `FNALsubMenu (Booster)'.
 */
Menu_item  boosterHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_NOTIFY:
    strcpy( LambdaGlobals.latticeIdent, "booster" );
    xv_set( ip->latticeTxt,
      PANEL_VALUE, "booster", 
      NULL );
    strcpy( LambdaGlobals.dbCall, "dbsf" );
    break;

  default:
    break;
  }
  return item;
}

/*
 * Menu handler for `FNALsubMenu (8 GeV beamline)'.
 */
Menu_item  eightGevHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_NOTIFY:
    strcpy( LambdaGlobals.latticeIdent, "p8_v2" );
    xv_set( ip->latticeTxt,
      PANEL_VALUE, "p8_v2",
      NULL );
    strcpy( LambdaGlobals.dbCall, "dbsf" );
    break;

  default:
    break;
  }
  return item;
}

/*
 * Menu handler for `FNALsubMenu (MI to Tev)'.
 */
Menu_item  mi2tevHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_NOTIFY:
    strcpy( LambdaGlobals.latticeIdent, "mi2tev" );
    xv_set( ip->latticeTxt,
      PANEL_VALUE, "mi2tev", 
      NULL );
    strcpy( LambdaGlobals.dbCall, "dbsf" );
    break;

  default:
    break;
  }
  return item;
}

/*
 * Menu handler for `functionMenu (dbsf)'.
 */
Menu_item  dbsfHandler(item, op)
	Menu_item	item;
	Menu_generate	op;
{
char   formatType[4];
char   hostName[30];
FILE   *hostfp;

lambda_firstWindow_objects
  * ip = (lambda_firstWindow_objects *)
    xv_get(item, XV_KEY_DATA, INSTANCE);

switch (op) {

  case MENU_NOTIFY:
    /* Construct the dbsf call
       ... First check to see if a filename has been specified. */
    if( LambdaGlobals.latticeFileName[0] == '\0' ) {
      fputs( "\n*** ERROR *** \n", stderr );
      fputs("You must enter a target lattice file.\n", stderr);
      return item;
    }

    /* ... Then check that a lattice has been specified. */

    if( LambdaGlobals.latticeIdent[0] == '\0' ) {
      fputs( "\n*** ERROR *** \n", stderr );
      fputs("You have not yet chosen a lattice.\n", stderr);
      return item;
    }

    /* ... Load the format type. */
    switch ( LambdaGlobals.fileFormat ){
      case sds:
        strcpy( formatType, "-C " );
        break;

      case mad:
        strcpy( formatType, "-s " );
        break;

      case synch:
        strcpy( formatType, "-S " );
        break;

      case flat:
        strcpy( formatType, "-F " );
        break;

      default:
        fputs( "\n*** ERROR *** \n", stderr );
        fputs("Please choose the file format first.\n", stderr);
        return item;
    }

    /* ... Write the command line. */
    strcpy( LambdaGlobals.commandLine,   "/usr/local/lattice_tools/bin/" );
    strcat( LambdaGlobals.commandLine,   LambdaGlobals.dbCall );
    strcat( LambdaGlobals.commandLine,   "  " );
    strcat( LambdaGlobals.commandLine,   formatType );
    strcat( LambdaGlobals.commandLine,   "  -e -g   " );
    strcat( LambdaGlobals.commandLine,   LambdaGlobals.latticeIdent );
    strcat( LambdaGlobals.commandLine,   "  " );
    strcat( LambdaGlobals.commandLine,   LambdaGlobals.latticeIdent );
    strcat( LambdaGlobals.commandLine,   " > " );
    strcat( LambdaGlobals.commandLine,   LambdaGlobals.latticeFileName );
    strcat( LambdaGlobals.commandLine,   " &" );

    /* ... Test to see if we are on hobbes. */
    system( "hostname > /scratch/tempLambdaFile.host" );
    hostfp = fopen( "/scratch/tempLambdaFile.host", "r" );
    fgets( hostName, 25, hostfp );
    fclose( hostfp );
    system( "rm /scratch/tempLambdaFile.host" );
    
    if( !strEquals( hostName, "hobbes\n" ) ) {
      fprintf( stderr, "\n*** ERROR *** \n" );
      fprintf( stderr, "You must be logged on to hobbes to use dbsf.\n" );
      return item;
    }

    /* ... And invoke the command line if all is well. */
    system( LambdaGlobals.commandLine );
    /* DGN
       DGN printf( "\nDGN> SysComm: %s\n", LambdaGlobals.commandLine );
       DGN if( !system( LambdaGlobals.commandLine ) ) {
       DGN   fprintf( stderr, "\n*** ERROR *** \n" );
       DGN   fprintf( stderr, "Command %s failed.\n\n", LambdaGlobals.commandLine );
       DGN   return item;
       DGN }
       DGN */

    /* All done. */
    break;

  default:
    break;
}

return item;
}

/*
 * Menu handler for `functionMenu (twiss)'.
 */
Menu_item
twissHandler(item, op)
	Menu_item	item;
	Menu_generate	op;
{
lambda_firstWindow_objects
  * ip = (lambda_firstWindow_objects *)
    xv_get(item, XV_KEY_DATA, INSTANCE);

switch (op) {
  case MENU_NOTIFY:
    /*  First check to see if a filename has been specified. */
    if( LambdaGlobals.latticeFileName[0] == '\0' ) {
      fputs( "\n*** ERROR *** \n", stderr );
      fputs( "You must enter the name of a flat format lattice file.\n", 
              stderr );
      return item;
    }

    /* ... Write the command line. */
    strcpy( LambdaGlobals.commandLine,   "/home/calvin/bogacz/twiss/twiss  " );
    strcat( LambdaGlobals.commandLine,   LambdaGlobals.latticeFileName );

    /* Invoke twiss */
    system( LambdaGlobals.commandLine );
    break;

  default:
    break;
}
return item;
}

/*
 * Menu handler for `TEVsubMenu (Injection)'.
 */
Menu_item  TevInjectionHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_NOTIFY:
    strcpy( LambdaGlobals.latticeIdent, "tev" );
    xv_set( ip->latticeTxt,
      PANEL_VALUE, "tev", 
      NULL );
    strcpy( LambdaGlobals.dbCall, "dbsf" );
    break;

  default:
    break;
  }
  return item;
}

/*
 * Menu handler for `TEVsubMenu (Low Beta)'.
 */
Menu_item  TevlowBetaHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_NOTIFY:
    strcpy( LambdaGlobals.latticeIdent, "tev" );
    xv_set( ip->latticeTxt,
      PANEL_VALUE, "tev", 
      NULL );
    strcpy( LambdaGlobals.dbCall, "dbsf" );
    break;

  default:
    break;
  }
  return item;
}

/*
 * Menu handler for `formatMenu (SDS)'.
 */
Menu_item  formatSDSHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_DISPLAY:
    break;

  case MENU_DISPLAY_DONE:
    break;

  case MENU_NOTIFY:
    LambdaGlobals.fileFormat = sds;
    xv_set( ip->formatTxt,
      PANEL_VALUE, "SDS", 
      NULL );
    break;

  case MENU_NOTIFY_DONE:
    break;
  }
  return item;
}

/*
 * Menu handler for `formatMenu (MAD)'.
 */
Menu_item   formatMADHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_DISPLAY:
    break;

  case MENU_DISPLAY_DONE:
    break;

  case MENU_NOTIFY:
    LambdaGlobals.fileFormat = mad;
    xv_set( ip->formatTxt,
      PANEL_VALUE, "MAD", 
      NULL );
    break;

  case MENU_NOTIFY_DONE:
    break;
}
return item;
}

/*
 * Menu handler for `formatMenu (SYNCH)'.
 */
Menu_item  formatSYNCHHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects * ip = (lambda_firstWindow_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_DISPLAY:
    break;

  case MENU_DISPLAY_DONE:
    break;

  case MENU_NOTIFY:
    LambdaGlobals.fileFormat = synch;
    xv_set( ip->formatTxt,
      PANEL_VALUE, "SYNCH", 
      NULL );
    break;

  case MENU_NOTIFY_DONE:
    break;
  }
  return item;
}

/*
 * Menu handler for `formatMenu (FLAT)'.
 */
Menu_item  formatFLATHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects * ip = (lambda_firstWindow_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_DISPLAY:
    break;

  case MENU_DISPLAY_DONE:
    break;

  case MENU_NOTIFY:
    LambdaGlobals.fileFormat = flat;
    xv_set( ip->formatTxt,
      PANEL_VALUE, "FLAT", 
      NULL );
    break;

  case MENU_NOTIFY_DONE:
    break;
  }
  return item;
}

/*
 * Menu handler for `MIsubMenu (17)'.
 */
Menu_item  mi17Handler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_NOTIFY:
    strcpy( LambdaGlobals.latticeIdent, "mi_17" );  
    xv_set( ip->latticeTxt,
      PANEL_VALUE, "mi_17", 
      NULL );
    strcpy( LambdaGlobals.dbCall, "dbsf" );
    break;

  default:
    break;
  }
  return item;
}

/*
 * Menu handler for `MIsubMenu (16)'.
 */
Menu_item  mi16Handler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_NOTIFY:
    strcpy( LambdaGlobals.latticeIdent, "mi_16" );
    xv_set( ip->latticeTxt,
      PANEL_VALUE, "mi_16", 
      NULL );
    strcpy( LambdaGlobals.dbCall, "dbsf" );
    break;

  default:
    break;
  }
  return item;
}

/*
 * Menu handler for `MIsubMenu (15)'.
 */
Menu_item  mi15Handler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_NOTIFY:
    strcpy( LambdaGlobals.latticeIdent, "mi_15" );
    xv_set( ip->latticeTxt,
      PANEL_VALUE, "mi_15", 
      NULL );
    strcpy( LambdaGlobals.dbCall, "dbsf" );
    break;

  default:
    break;
  }
  return item;
}

/*
 * Menu handler for `helpMenu (Lattices)'.
 */
Menu_item  helpLatticesHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_NOTIFY:
    fputs( "\n*** SORRY *** \n", stderr );
    fputs( "SORRY. All our lines are busy.\n", stderr );
    break;

  default:
    break;
  }
  return item;
}

/*
 * Menu handler for `helpMenu (LAMBDA functions)'.
 */
Menu_item  helpFunctionsHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_NOTIFY:
    fputs( "\n*** SORRY *** \n", stderr );
    fputs( "SORRY. All our lines are busy.\n", stderr );
    break;

  default:
    break;
  }
  return item;
}

/*
 * Menu handler for `helpMenu (Files)'.
 */
Menu_item  helpFilesHandler(item, op)
  Menu_item  item;
  Menu_generate  op;
{
  lambda_firstWindow_objects
    * ip = (lambda_firstWindow_objects *)
      xv_get(item, XV_KEY_DATA, INSTANCE);

  switch (op) {
  case MENU_NOTIFY:
    fputs( "\n*** SORRY *** \n", stderr );
    fputs( "SORRY. All our lines are busy.\n", stderr );
    break;

  default:
    break;
  }
  return item;
}

/*
 * Notify callback function for `filenameTxt'.
 */
Panel_setting
filenameHandler(item, event)
	Panel_item	item;
	Event		*event;
{
lambda_firstWindow_objects
  *ip = (lambda_firstWindow_objects *)
    xv_get(item, XV_KEY_DATA, INSTANCE);

/* char *	value = (char *) xv_get(item, PANEL_VALUE); */

strcpy( LambdaGlobals.latticeFileName, (char *) xv_get(item, PANEL_VALUE) );
/* DGN 
   DGN fprintf( stderr, "And the name is: %s\n", LambdaGlobals.latticeFileName );
   DGN */

return panel_text_notify(item, event);
}

/*
 * Event callback function for `message2'.
 */
void aboutHandler(item, event)
Panel_item	item;
Event		*event;
{
static char toggle;

lambda_firstWindow_objects
  *ip = (lambda_firstWindow_objects *)
    xv_get(item, XV_KEY_DATA, INSTANCE);

if( toggle ) {
  printf("\n");
  printf("LAMBDA:          Loosely Associated Modules for               \n");
  printf("                 Beamline Design and Analysis.                \n");
  printf("                                                              \n");
  printf("Authors:         dbsf...       Gary Trahern,                  \n");
  printf("                               Ellen Syphers                  \n");
  printf("                                                              \n");
  printf("                 twiss ...     Alex Bogacz                    \n");
  printf("                               bogacz@calvin.fnal.gov         \n");
  printf("                               (708) 840 3873                 \n");
  printf("                                                              \n");
  printf("Documentation:   XXXXXXX                                      \n");
  printf("                                                              \n");
  printf("    ... blah .. blah ..... etc. etc.   ...                    \n");
  printf("\n");
}
toggle = !toggle;
}

// $Header: /rap/lambda/Geom3d/include/RCS/Geom3d.hxx,v 1.1 1996/02/23 01:58:24 mackay Exp $

// Geom3d.h
// Try to start a 3 dimensional geometry package for points, lines, and planes.
#ifndef GEOM3D_HXX
#define GEOM3D_HXX 1

#include <iostream.h>
#include <stdlib.h>
#include <math.h>

enum { FALSE=0, TRUE=1, AVERAGE=2 };
const double Pi = acos(-1.0);
const double Piover2 = Pi/2.0;

/* Here we define three classes: Point, Line, and Plane.
**
** Assume 
**     Point p1, p2, p3
**     Line  l1, l2, l3
**     Plane m1, m2, m3
**     <Point, Line, Plane> t1, t2, t3    (i.e., any of the three)
**     double s
**     int iflag
**     
** The following operations are are valid:
**  Translation: (by vector p1)
**    t3 = p1 + t2
**    t3 = t2 + p1
**    t3 += p1
**    t3 = p1 - t2
**    t3 = t2 - p1
**    t3 -= p1
**
**  Inversion or negation:
**    t2 = -t1
**
**  Scalar product
**    s = p1 * p2              // i.e.,  s  = p1 . p2
**  Cross product
**    p3 = cross(p1,p2)        // i.e.,  p3 = p1 x p2
**
**  Magnitude of point (vector)
**    s = magnitude(p1)
**  Distance
**    s = distance(p1,p2)      // between two points
**    s = distance(p1,l1)      // between point and line
**    s = distance(l1,p1)      // between point and line
**    s = distance(p1,m1)      // between point and plane
**    s = distance(m1,p1)      // between point and plane
**
**  Scaling of points
**    p2 = s  * p1
**    p2 = p1 / s
**    p1 *= s
**    p1 /= f
**
**  Rotation of coordinate system
**    t2 = rotate_x(t1, angle)  // rotate object about x-axis
**    t2 = rotate_y(t1, angle)  // rotate object about y-axis
**    t2 = rotate_z(t1, angle)  // rotate object about z-axis
**
**  Test for equivalence  (points only)
**    if(p1==p2){...}
**    if(p1!=p2){...}
**
**  Test for parallelism [note (l1 || l1) and (m1 || m1) are always TRUE]
**    if(l1 || l2){...}
**    if(m1 || m2){...}
**    if(l1 || m1){...}
**    if(m1 || l1){...}
**
**  Test for perpendicularity
**    if(perp(l1,l2))
**    if(perp(m1,m2))
**    if(perp(l1,m1))
**    if(perp(m1,l1))
**
**  Find point p2 on line l1 closest to given point p1.
**    p2 = l1.project(p1)
**  Find point p2 on plane m1 closest to given point p1.
**    p2 = m1.project(p1)
**  Find line l2 in plane which is the projection of l1 on m1.
**    l2 = m1.project(l1)
**
**  Find angle between
**    s  = angle(l1,l2)  // two lines
**    s  = angle(m1,m2)  // two planes
**    s  = angle(l1,m1)  // a line and a plane
**    s  = angle(m1,l1)  // a line and a plane
**
**  Return intersection
**    p1 = intersect(l1,l2,distance)
**    p1 = intersect(l1,m1,distance)
**    l1 = intersect(m1,m2,distance)
**    p1 = intersect(m1,m2,m3)
**
*/

//========================================================
//========================================================

class Point    //  This is really a 3D vector class
{

public:
// Coordinates.
  double x;
  double y;
  double z;
//-- -- -- -- -- -- --

// The class constructors:
  Point(double xx=0.0, double yy=0.0, double zz=0.0)
    :x(xx), y(yy), z(zz) {};

// Addition and subtraction operators:  +, -, +=, -=
  Point operator+(const Point &p2) const
    {                       // Adds two vectors = p1 + p2
      Point t;
      t.x = x +  p2.x;
      t.y = y +  p2.y;
      t.z = z +  p2.z;
      return t;
    };
  Point operator-(const Point &p2) const
    {                       // Subtracts two vectors = p1 - p2
      Point t;
      t.x = x -  p2.x;
      t.y = y -  p2.y;
      t.z = z -  p2.z;
      return t;
    };
  Point &operator+=(const Point &p2)
    {
      this->x += p2.x;
      this->y += p2.y;
      this->z += p2.z;
      return *this;
    };
  Point &operator-=(const Point &p2)
    {
      this->x -= p2.x;
      this->y -= p2.y;
      this->z -= p2.z;
      return *this;
    };
  Point &operator/=(double f)
    {
      this->x /= f;
      this->y /= f;
      this->z /= f;
      return *this;
    };
  Point &operator*=(double f)
    {
      this->x *= f;
      this->y *= f;
      this->z *= f;
      return *this;
    };

// Negation operator (sign flip).
  friend Point operator-(const Point &p1)
    {                       // Returns the negative of a vector = -p1
      Point t;
      t.x = -p1.x;
      t.y = -p1.y;
      t.z = -p1.z;
      return t;
    };

// Multiplication ops: "scalar prod", "dbl * vec", "vec/dbl", cross prod
  double operator*(const Point &p2) const
    {                       // Returns the scalar product = p1 "." p2
      return x*p2.x + y*p2.y + z*p2.z;
    };

  Point operator*(double s) const
    {                       // Scale the vector
      Point t;
      t.x = s*x;
      t.y = s*y;
      t.z = s*z;
      return t;
    };
  friend Point operator*(double s, const Point &p)
    {                       // Scale the vector
      Point t;
      t.x = s*p.x;
      t.y = s*p.y;
      t.z = s*p.z;
      return t;
    };
  Point operator/(double s) const
    {                       // Divide vector by a scalar.
      return *this * (1.0/s);
    };
  friend Point cross(const Point &p1, const Point &p2)
    {                       // Return the cross product = p1 x p2
      Point t;
      t.x = p1.y*p2.z - p1.z*p2.y;
      t.y = p1.z*p2.x - p1.x*p2.z;
      t.z = p1.x*p2.y - p1.y*p2.x;
      return t;
    };

// Equivalence operator
  int operator==(const Point &p) const
    {                       // Check two points for equivalence.
      if((x == p.x) && (y == p.y) && (z == p.z))
	{
	  return 1;
	} else {
	  return 0;
	}
    };
  int operator!=(const Point &p) const
    {                       // Check two points for nonequivalence.
      if((x != p.x) || (y != p.y) || (z != p.z))
	{
	  return 1;
	} else {
	  return 0;
	}
    };

// Magnitude, normalization, distance
  friend double magnitude(const Point &p)
    {                       // returns the magnitude of a vector
      return sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
    };
  friend Point normalize(const Point &p)
    {                                // normalize a vector to 1 [= v/(|v|)]
      double length = magnitude(p);
      if(length <= 0.0)
	{
	  cerr << "? error: can't normalize a zero vector.\n";
	  return Point(1.0,0,0);     // default to the x-axis.
	}
      return p/length;
    };
  friend double distance(const Point &p1, const Point &p2)
    {                       // Return the distance between p1 and p2
      return magnitude(p1-p2);
    };

// Rotation about origin
  friend Point rotate_x(const Point &p, double angle)
    {                        // Rotate point about x-axis
      double c = cos(angle); // angle>0 rotates y-coord toward +z
      double s = sin(angle);
      Point t;
      t.x = p.x;
      t.y =  c*p.y - s*p.z;
      t.z =  s*p.y + c*p.z;
      return t;
    };
  friend Point rotate_y(const Point &p, double angle)
    {                        // Rotate point about y-axis
      double c = cos(angle); // angle>0 rotates z-coord toward +x
      double s = sin(angle);
      Point t;
      t.y = p.y;
      t.z =  c*p.z - s*p.x;
      t.x =  s*p.z + c*p.x;
      return t;
    };
  friend Point rotate_z(const Point &p, double angle)
    {                        // Rotate point about z-axis
      double c = cos(angle); // angle>0 rotates x-coord toward +y
      double s = sin(angle);
      Point t;
      t.z = p.z;
      t.x =  c*p.x - s*p.y;
      t.y =  s*p.x + c*p.y;
      return t;
    };

// Output via ostream
  friend ostream &operator<<(ostream & os, const Point &p)
    {
      os << "[ " << p.x << ", " << p.y << ", " << p.z << " ]";
      return os;
    };
};
//=============================================
//=============================================

class Line                // A class of lines
{
private: // ********** Data: ******************
  Point _anchor;           // A point on the line (anchor point)
  Point _cosines;          // the direction cosines (a unit vector)

// -- -- -- -- --

public:
// Constructors
  Line() { _cosines.x=1.0; };
  Line(const Point &p1, const Point &p2)
    {                  // Initialize by two points
      _anchor = p1;
      double length = distance(p1,p2);
      if(length <= 0.0)
	{
	  cerr << "? error: can't construct a line with "
	       << "zero length between two points.\n";
	  _cosines.x = 1.0;     // default to the x-axis.
	} else {
	  _cosines.x = (p2.x-p1.x)/length;
	  _cosines.y = (p2.y-p1.y)/length;
	  _cosines.z = (p2.z-p1.z)/length;
	}
    };
  Line(const Point &p1, double cosx, double cosy, double cosz)
    {                   // Initialize with a point and direction cosines
      _anchor = p1;
      _cosines = normalize(Point(cosx,cosy,cosz));
    };

// Component access functions:
  Point anchor() const { return _anchor; };
  double x() const { return _anchor.x; };
  double y() const { return _anchor.y; };
  double z() const { return _anchor.z; };
  void Setanchor(const Point &p) { _anchor = p; };
  void Setx(double val) { _anchor.x = val; };
  void Sety(double val) { _anchor.y = val; };
  void Setz(double val) { _anchor.z = val; };
  Point cosines() const { return _cosines; };
  void Setcosines(double sx, double sy, double sz)
    {
      _cosines = normalize(Point(sx,sy,sz));
    };
  void Setcosines(Point p)
    {
      _cosines = normalize(p);
    };

// Translation operators +, -, line += vec, line -= vec
  Line operator+(const Point & p) const
    {                  // Translate a line with a point (vector).
      Line t;
      t._anchor = p + _anchor;
      t._cosines = _cosines;
      return t;
    };
  friend Line operator+(const Point &p, const Line &l)
    {                  // Translate a line with a point (vector).
      return l+p;
    };
  Line operator-(const Point &p) const
    {                  // Translate a line with a point (vector).
      Line t;
      t._anchor =  _anchor - p;
      t._cosines = _cosines;
      return t;
    };
  friend Line operator-(const Point &p, const Line &l)
    {                  // Translate a line with a point (vector).
      return (-l)+p;
    };
  Line &operator+=(const Point &p)
    {
      this->_anchor += p;
      return *this;
    };
  Line &operator-=(const Point &p)
    {
      this->_anchor -= p;
      return *this;
    };

// Negation operator (sign flip).
  friend Line operator-(const Line &l)
    {  // Only need to flip sign of anchor point.
      Line t;
      t._anchor  = -l._anchor;
      t._cosines = l._cosines;
      return t;
    };

// Test parallelness, and perpendicularity
  int operator||(const Line &l) const
    {                  // Check to see if they are parallel.
      if(_cosines == l._cosines)
	{
	  return TRUE;
	} else {
	  return FALSE;
	}
    };
  friend int perp(const Line &l1, const Line &l2)
    {                  // Are l1 and l2 perpendicular?
      return !(l1._cosines * l2._cosines);
    };

// Projection of a point onto the line.
  Point project(const Point &p) const
    {                  // Find point on line closest to Point p.
      return _anchor + _cosines * ((p-_anchor)*_cosines);
    };

// Angle between two lines.
  friend double angle(const Line &l1, const Line &l2)
    {                  // Return the angle between two lines.
		       // Note that the lines do not have to intersect.
      double ang = acos(l1._cosines*l2._cosines);
      if(ang>Piover2)ang = Pi-ang;
      return ang;
    };

// Intersection of two lines.
  friend Point intersect(const Line &l1, const Line &l2, double *dist)
    {                // Find the intersection of two lines.
		     // If the lines are skew it returns the average
		     // of the points of closest approach and sets
		     //    dist = distance between the two lines.
		     // If the lines are parallel, it returns garbage with
		     //    dist = -1.0
      Point pave;
      if(l1 || l2)
	{
	  *dist = -1.0;
	  return pave;
	}
      double cang   = l1._cosines*l2._cosines;
      double denom  = 1.0 - cang*cang;
      Point  delta  = l1._anchor-l2._anchor;
      double t1     = (cang*l2._cosines - l1._cosines)*delta/denom;
      double t2     = (l2._cosines - cang*l1._cosines)*delta/denom;
      Point  p1     = l1._anchor + l1._cosines*t1;
      Point  p2     = l2._anchor + l2._cosines*t2;

#ifdef DEBUG
// Debugging output
      cerr<<"Line::intersection--- cang = "<<cang<<"\tdenom = "<<denom
	  <<endl;
      cerr << "Line::intersection--- delta = " << delta << endl;
      cerr << "Line::intersection--- t1 = " << t1 << "\tt2 = " << t2
	   << endl;
      cerr << "Line::intersection--- p1 = " << p1 << endl;
      cerr << "Line::intersection--- p2 = " << p2 << endl;
#endif

      if(p1==p2)
	{
	  *dist = 0.0;
	  return p1;
        } else {              // average the two closest points.
	  *dist = distance(p1,p2);			
	  return 0.5*(p1+p2);
	}
    };

// Distance of a point from a line.
  friend double distance(const Line &l, const Point &p)
    {                 /* Calculate the distance of a point to a line. */
      return distance(p,l.project(p));
    };      

  friend double distance(const Point &p, const Line &l)
    {                 /* Calculate the distance of a point to a line. */
      return distance(p,l.project(p));
    };

// Rotation about the origin.
  friend Line rotate_x(const Line &l, double ang)
    {
      Line t;
      t._cosines = rotate_x(l._cosines,ang);
      t._anchor  = rotate_x(l._anchor,ang);
      return t;
    };
  friend Line rotate_y(const Line &l, double ang)
    {
      Line t;
      t._cosines = rotate_y(l._cosines,ang);
      t._anchor  = rotate_y(l._anchor,ang);
      return t;

    };
  friend Line rotate_z(const Line &l, double ang)
    {
      Line t;
      t._cosines = rotate_z(l._cosines,ang);
      t._anchor  = rotate_z(l._anchor,ang);
      return t;
    };

// Output via ostream.
  friend ostream &operator<<(ostream & os, const Line &l)
    {                  // Print out line l.
      os << "Line\n\t     anchor point: " << l._anchor;
      os << "\n\tdirection cosines: " << l._cosines << endl;
      return os;
    };
};

//=============================================
//=============================================

class Plane
{
private: // ************ Data: ************
  Point  _cosines;    // direction cosines of normal to plane from origin
  double _r;          // distance of plane from origin

// -- -- -- -- --
public:
// Constructors
  Plane(const Point &c, double r): _r(r) // define plane with direction cosines
    {                                    // and distance from origin to plane
      _cosines = normalize(c);           // Don't assume it's normalized.
    };
  Plane(double cx=1.0, double cy=0, double cz=0, double r=0)
    : _r(r)                       // similar to previous
    {
      _cosines = normalize(Point(cx,cy,cz));
    };
  Plane(Point p1, Point p2, Point p3)
    {                             // Define a plane by three points.
      _cosines = normalize(cross((p2-p1),(p3-p1)));
      _r = _cosines*p1;
      if(_r<0)
	{
	  _r = -_r;
	  _cosines = -_cosines;
	}
    };
  Plane(const Line &l, const Point &p)
    {
      Plane m(p, l.anchor(), l.anchor()+l.cosines());
      _cosines = m._cosines;
      _r = m._r;
    };
  Plane(const Point &p, const Line &l)
    {
      Plane m(p, l.anchor(), l.anchor()+l.cosines());
      _cosines = m._cosines;
      _r = m._r;
    };

// Component access functions
  Point cosines() const { return this->_cosines; };
  double r() const { return this->_r; };
  void Setcosines(const Point &c){ _cosines = c; };
  void Setr(const double r){ _r = r; };

// Translation operators +, -, +=, -=  (by a vector).
  Plane operator+(const Point &p) const
    {
      Plane t;
      t._r = _r+p*_cosines;
      t._cosines = _cosines;
      return t;
    };
  friend Plane operator+(const Point &p, const Plane &m)
    {
      Plane t;
      t = m+p;
      return t;
    };
  Plane operator-(const Point &p) const
    {
      Plane t;
      t._r = _r-p*_cosines;
      t._cosines = _cosines;
      return t;
    };
  friend Plane operator-(const Point &p, const Plane &m)
    {
      Plane t;
      t = (-m)+p;
      return t;
    };
  Plane &operator+=(const Point &p)
    {
      this->_r += p*_cosines;
      return *this;
    };
  Plane &operator-=(const Point &p)
    {
      this->_r -= p*_cosines;
      return *this;
    };


// Negation operator (sign flip).
  friend Plane operator-(const Plane &m)
    {  // We don't need to flip the sign of the distance from the origin.
      Plane t;
      t._cosines = -m._cosines;
      return t;
    };

// Angle between two planes, or a plane and a line.
  friend double angle(const Plane &m1, const Plane &m2)
    {                  // Angle between two planes
      double ang = acos(m1._cosines*m2._cosines);
      if(ang>Piover2)ang = Pi-ang;
      return ang;
    };
  friend double angle(const Plane &m1, const Line &l1)
    {                  // Returns angle between line and normal to plane.
      return acos(m1._cosines*l1.cosines());
    };
  friend double angle(const Line &l1, const Plane &m1)
    {                  // Returns angle between line and normal to plane.
      return acos(m1._cosines*l1.cosines());
    };

// Tests of parallelness, and perpendicularity.
  int operator||(const Plane &m) const { return (_cosines==m._cosines); };
  int operator||(const Line  &l) const { return (_cosines*l.cosines()==0);};
  friend int operator||(const Line &l, const Plane &m){ return (m || l) ;};
  friend int perp(const Plane &m1, const Plane &m2)
    {
      if(m1._cosines*m2._cosines==0.0)
	{
	  return TRUE;
	} else {
	  return FALSE;
	}
    };

// Rotation about the origin.
  friend Plane rotate_x(const Plane &m, double ang)
    {
      return Plane(rotate_x(m._cosines,ang),m._r);
    };
  friend Plane rotate_y(const Plane &m, double ang)
    {
      return Plane(rotate_y(m._cosines,ang),m._r);
    };
  friend Plane rotate_z(const Plane &m, double ang)
    {
      return Plane(rotate_z(m._cosines,ang),m._r);
    };

// Distance of point to plane, plane to plane.
  friend double distance(const Point &p, const Plane &m)
    {                  // Calculate the distance of a point from a plane.
      return m._r + m._cosines * p;
    };
  friend double distance( const Plane &m, const Point &p)
    {                  // Calculate the distance of a point from a plane.
      return m._r + m._cosines * p;
    };
  friend double distance(const Plane &m1, const Plane &m2)
    {                  // Calculate the distance between two planes.
      if(m1 || m2)
	{
	  double temp = m1._r - m2._r*(m1._cosines*m2._cosines);
	  if(temp<0.0)temp = -temp;
	  return temp;
	} else {
	  return 0.0;
	}
    };

// Projection of point and line on plane.
  Point project(const Point &p) const
    {                  // Project a point onto a plane.
      return (_r - _cosines*p)*_cosines + p;
    };
  Line project(const Line &l) const
    {                  // Project a line onto a plane.
                       // Note that if the line is perpendicular to the
		       // plane then the projection should really be a
		       // point.  More work needs to be done with errors.
      Line t;
      t.Setanchor(this->project(l.anchor()));
      if(l.cosines()==_cosines)
	{
	  cerr <<
	    "?error: Plane.project(Line) when Line is perp to Plane.\n";
	  t.Setcosines(1.0,0.0,0.0);
	} else {
	  Point s  = l.cosines();
	  Point ss = s - _cosines*(_cosines*s);
	  t.Setcosines(ss/magnitude(ss));
	}
      return t;
    };

// Intersection of plane-plane, line-plane, plane-line, and three planes.
  friend Line intersect(const Plane &m1, const Plane &m2)
    {
      Line t;

      if((m1 || m2))
        {
          cerr << "? intersect(Plane,Plane): planes are parallel.\n";
          return t;
        }

      t.Setcosines(cross(m1._cosines,m2._cosines));
      double c1c2 = m1._cosines * m2._cosines;
      Point s1 = normalize(m2._cosines - c1c2 * m1._cosines);
      double tstar = (m2._r - m2._r*c1c2);
      t.Setanchor(m1._r*m1._cosines + tstar*s1);
      return t;
    };
  friend Point intersect(const Plane &m, const Line &l)
    {
      Point p;

      double dotprod;
      dotprod = l.cosines() * m.cosines();
      if(dotprod == 0)
	{
          cerr << "? intersect(Plane,Line): line and plane are parallel.\n";
          return p;
        }

      p = l.anchor() + l.cosines()*(m._r-m._cosines*l.anchor())/dotprod;
      return p;
    };
  friend Point intersect(const Line &l, const Plane &m)
    {
      return intersect(m,l);
    };
  friend Point intersect(const Plane &m1, const Plane &m2, const Plane &m3)
    {
      Point p = cross(m2._cosines,m3._cosines);

      double det = m1._cosines * p;
      if(det == 0.0)
	{
          cerr
	   << "? intersect(Plane,Plane,Plane) at least to are parallel.\n";
          return p;
        }
      p = (m1._r * p + m2._r * cross(m3._cosines,m1._cosines) +
	   m3._r * cross(m1._cosines,m2._cosines))/det;
      return p;
    }


// Output via ostream.
  friend ostream &operator<<(ostream &os, const Plane &m)
    {
      os << "\nPlane:\tcosines = " << m._cosines
         << "\tr = " << m._r;
      return os;
    };
};

//====================================
//====================================
extern Line   fitline(const Point *const pts, int npts);
extern Plane fitplane(const Point *const pts, int npts);

#endif // GEOM3D_HXX

// $Header: /rap/lambda/Geom3d/RCS/Line_test.cxx,v 1.1 1996/02/23 02:02:08 mackay Exp $
// This program tests some of the Line class functions defined in Geom3d.h,

#include <iostream.h>
#include "Geom3d.hxx"

//==============================
int main(int argc, char **argv)
{
  Point p1(0.,1.,0.), p2(0,0,1.);
  Point orig(0,0,0);


  cout.precision(8);
  cout.setf(ios::fixed);


  cout << "p1: " << p1 << endl;
  cout << "p2: " << p2 << endl;

  cout << "Test Line constructors\n";
  Line  l1;
  cout << "Default line: " << l1 << endl;
  Line  l2(p1,p2);
  cout << "line through p1 & p2   l2 =" << l2 << endl;
  Line  l3(Point(0,0,0),1,1,0);  // anchor and 3 unnormalized dir cosines.
  cout << "l3: " << l3 << endl << endl;

  cout << "l2.anchor():" << l2.anchor() << endl;
  cout << "l2.anchor().[x, y, z]:"
       << l2.anchor().x
       << l2.anchor().y
       << l2.anchor().z << endl;

  l1.Setanchor(p1);
  cout << "l1.Setanchor(p1): " << l1 << endl;
  l1.Setx(2.0);  l1.Sety(2.0);  l1.Setz(2.0);
  cout << "l1.Setx(2); l1.sety(2); l1.setz(2):" << l1 << endl;
  cout << "l1.cosines() = " << l1.cosines() << endl;
  l1.Setcosines(p1);
  cout << "l1.Setcosines(p1): " << l1 << endl;
  l1.Setcosines(1,1,1);
  cout << "l1.Setcosines(1,1,1): " << l1 << endl;

//  translations etc
  cout << endl << "p1: " << p1 << endl;
  cout << "l2: " << l2 << endl;

  cout << "l2+p1:" << l2+p1 << endl;
  cout << "p1+l2:" << p1+l2 << endl;
  cout << "l2-p1:" << l2-p1 << endl;
  cout << "p1-l2:" << p1-l2 << endl;

  l2 += p1;
  cout << "l2+=p1: " << l2 << endl;
  l2 -= p1;
  cout << "l2-=p1: " << l2 << endl;
  cout << "-l2: " << -l2 << endl;

  cout <<   "(l1 || l1):" << (l1 || l1)
       << "\t(l1 || l2):" << (l1 || l2) << endl;
  Line lx(orig,1,0,0), ly(orig,0,1,0);

  cout << endl << "lx:" << lx << endl;
  cout <<         "ly:" << ly << endl;
  cout <<   "perp(lx,lx):" << perp(lx,lx)
       << "\tperp(lx,ly):" << perp(lx,ly) << endl;

// projection, intersection distance operations:
  cout << "\n projection, intersection, & distance functions:\n";

  cout << "l2.project(p1): " << l2.project(p1) << endl;
  cout << "angle(lx,ly): " << angle(lx,ly) << endl;
  cout << "angle(l1,l2): " << angle(l1,l2) << endl;

  double d;
  cout << "intersect(l1,l2):" << intersect(l1,l2,&d)
       << " dist=" << d << endl;

  cout << "distance(p1,l1): " << distance(p1,l1) << endl;
  cout << "distance(l1,p1): " << distance(l1,p1) << endl;

// rotations
  double Piover4 = Piover2/2;

  l1 = Line(Point(1,1,1),1,0,0);
  cout << "l1: " << l1 << endl;

  l1 = rotate_x(l1,Piover4);
  cout << "l1=rotate_x(l1,Pi/4): "  << l1 << endl;

  l1 = rotate_z(l1,-Piover2);
  cout << "l1=rotate_z(l1,-Pi/2): " << l1 << endl;

  l1 = rotate_y(l1,Piover4);
  cout << "l1=rotate_y(l1,Pi/4): "  << l1 << endl;

  l1 = rotate_z(l1,Piover2);
  cout << "l1=rotate_z(l1,Pi/2): "  << l1 << endl;

// Try the fitline routine.
  const int npts = 10;
  Point q[npts];
  int i;
  for(i=0;i<npts;i++)
    {
      q[i].x = i;
      q[i].y = 3*i+0.01*sin(i);
      q[i].z = 2*i+0.01*cos(i);
    }
  Line lfit = fitline(q,npts);
  cout << "\nFit a line through the following points:\n";
  for(i=0;i<npts;i++)
    {
      cout << i << ":\t" << q[i] << "\tresid:"
	   << q[i]-lfit.project(q[i]) << endl;
    }
  cout << "The fitted line is:" << lfit << endl;

  return 0;
}

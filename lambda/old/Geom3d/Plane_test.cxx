// $Header: /rap/lambda/Geom3d/RCS/Plane_test.cxx,v 1.1 1996/02/23 02:02:08 mackay Exp $
// This program tests some of the Plane class functions defined in Geom3d.h,

#include <iostream.h>
#include <Geom3d.hxx>

//==============================
int main(int argc, char **argv)
{
  cout.precision(8);
  cout.setf(ios::fixed);

  cout << "Test Plane constructors\n";
  Point O(0,0,0);
  Point c1(1,1,1);
  Point c2(1,1,0);
  cout << " O:\t" << O  << endl;
  cout << "c1:\t" << c1 << endl;
  cout << "c2:\t" << c2 << endl;

// Plane constructors
  Plane m0;
  cout << "default plane: " << m0 << endl;

  Plane m1(c1,0);
  cout << "m1(c1,0) = " << m1 << endl;

  Plane m2(c1.x,c1.y,c1.z,1);
  cout << "m2(c1.x,c1.y,c1.z,1) = " << m2 << endl;

  Plane m3(O,c1,c2);
  cout << "m3(O,c1,c2) = " << m3 << endl;

  Line l1(O,c2);
  cout << "l1(origin --> c1): " << l1 << endl;
  Plane m4(l1,c1);
  cout << "m4(l1,c2):" << m4 << endl;
  Plane m5(c1,l1);
  cout << "m5(c1,l1):" << m5 << endl;

// component access functions

  cout << "m5.cosines() = " << m5.cosines() << endl;
  cout << "m5.r(): " << m5.r() << endl;

  m5.Setcosines(c1);
  cout << "m5.Setcosines(c1):" << m5 << endl;
  m5.Setr(10);
  cout << "m5.Setr(10):" << m5 << endl;


// translation operations
  cout << "m5+c1: " << m5+c1 << endl;
  cout << "c1+m5: " << c1+m5 << endl;
  cout << "m5-c1: " << m5-c1 << endl;
  cout << "c1-m5: " << c1-m5 << endl;

  m5 += c1;
  cout << "m5+=c1: " << m5 << endl;
  m5 -= c1;
  cout << "m5-=c1: " << m5 << endl;

  cout << "-m5: " << -m5 << endl;

// || and perp
  cout << "(m1 || m1):" << (m1 || m1)
       << "\t(m1 || m2):" << (m1 || m2) << endl;
  cout << "perp(m1,m1):" << perp(m1,m1)
       << "\tperp(m1,m2):" << perp(m1,m2) << endl;


//  let\'s see if the compiler complains
  int j = (l1 || m1) + (m1 || l1);

// ?? missing ??  j = perp(l1,m1) + perp(m1,l1);

// distances
  m1 = Plane(Point(1,1,0),0.5);
  cout << "c1: " << c1 << endl;
  cout << "m1: " << m1 << endl;
  cout << "distance(m1,c1): " << distance(m1,c1) << endl;
  cout << "distance(c1,m1): " << distance(c1,m1) << endl;
  m2 = m1 + c1;
  cout << "m2: " << m2 <<endl;
  cout << "distance(m1,m2): " << distance(m1,m2) << endl;

  m1 = Plane(1,0,0,0);
  cout << "m1: " << m1 << endl;
  m2 = m1 + c1;
  cout << "m2: " << m2 <<endl;
  cout << "distance(m1,m2): " << distance(m1,m2) << endl;

// project a line onto a plane  (points done later with fitplane)
  cout << "m1.project(l1): " << m1.project(l1) << endl;

// angles and rotations
  cout << "angle(m1,m2): " << angle(m1,m2) << endl;
  cout << "angle(l1,m1): " << angle(l1,m1) << endl;
  cout << "angle(m1,l1): " << angle(m1,l1) << endl;

  double Piover4 = Piover2/2;

  m1 = Plane(Point(1,1,1),1);
  cout << "m1: " << m1 << endl;

  m1 = rotate_x(m1,Piover4);
  cout << "m1=rotate_x(m1,Pi/4): "  << m1 << endl;

  m1 = rotate_z(m1,-Piover2);
  cout << "m1=rotate_z(m1,-Pi/2): " << m1 << endl;

  m1 = rotate_y(m1,Piover4);
  cout << "m1=rotate_y(m1,Pi/4): "  << m1 << endl;

  m1 = rotate_z(m1,Piover2);
  cout << "m1=rotate_z(m1,Pi/2): "  << m1 << endl;

// fit a plane to a bunch of points
  long i;
  const long npts = 10;
  Point q[npts];
  double ang = 0;
  double dang = 2*Pi/npts;
  double tilt = Pi/6;
  for(i=0;i<npts;i++)
    {
      Point temp(5,sin(ang),cos(ang));
      q[i] = rotate_y(temp,tilt);
      ang += dang;
    }
  Plane mfit = fitplane(q,npts);
  cout << "\nFit a plane through the following points:\n";
  for(i=0;i<npts;i++)
    {
      cout << i << ":\t" << q[i] << "\tresid:"
	   << q[i]-mfit.project(q[i]) << endl;
    }
  cout << "The fitted plane is:" << mfit << endl;

// Intersections


  return 0;
}

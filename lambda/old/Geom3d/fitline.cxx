// $Header: /rap/lambda/Geom3d/RCS/fitline.cxx,v 1.1 1996/02/23 02:02:08 mackay Exp mackay $
// fitline.cc:  for inclusion in libGeom3d.lib

#include <iostream>
#include <Geom3d.hxx>

//========================================
Line fitline(const Point *const p, int n)
{    // This does a least squares fit of a line to a set of points.
  double sumx =0, sumy =0, sumz =0,
         sumxx=0, sumyy=0, sumzz=0,
         sumxy=0, sumxz=0, sumyz=0;

  if(n<2)
    {
      std::cout << "? fitline: not enough point to fit a line.\n";
      Line pdflt;
      return pdflt;
    }
  if(n==2) return Line(p[0],p[2]);

  for(int i=0; i<n; i++)    // Calculate the moments of the distribution.
    {
      sumx += p[i].x;
      sumy += p[i].y;
      sumz += p[i].z;
      sumxx+= p[i].x*p[i].x;
      sumyy+= p[i].y*p[i].y;
      sumzz+= p[i].z*p[i].z;
      sumxy+= p[i].x*p[i].y;
      sumxz+= p[i].x*p[i].z;
      sumyz+= p[i].y*p[i].z;
    }

  double num = (double) n;
  std::cout << "sumx,sumy,sumz: " << sumx << sumy << sumz << std::endl;
  sumx /= num;
  sumy /= num;
  sumz /= num;
  std::cout << "sumx,sumy,sumz: " << sumx << sumy << sumz << std::endl;

  Point anchor(sumx,sumy,sumz);  // The anchor point of the line
  std::cout << "anchor:" << anchor;

  sumxx /= num;
  sumyy /= num;
  sumzz /= num;
  sumxy /= num;
  sumxz /= num;
  sumyz /= num;

  double sigxx = sumxx-sumx*sumx;
  double sigyy = sumyy-sumy*sumy;
  double sigzz = sumzz-sumz*sumz;
  double sigxy = sumxy-sumx*sumy;
  double sigxz = sumxz-sumx*sumz;
  double sigyz = sumyz-sumy*sumz;

// Calculate the possible direction cosine vectors.  It is possible that
// one or two of them are unnormalizable (i.e., = (0,0,0)).

  Point s1 = normalize(Point(sigxx,sigxy,sigxz));
  Point s2 = normalize(Point(sigxy,sigyy,sigyz));
  Point s3 = normalize(Point(sigxz,sigyz,sigzz));

// Calculate xi/n for each of the three direction cosine vectors.

  double xiovern1 = (1.0-s1.x*s1.x)*sigxx + (1.0-s1.y*s1.y)*sigyy +
                    (1.0-s1.z*s1.z)*sigzz
		    - 2.0*(s1.x*s1.y*sigxy + s1.x*s1.z*sigxz +
			   s1.y*s1.z*sigyz);
  double xiovern2 = (1.0-s2.x*s2.x)*sigxx + (1.0-s2.y*s2.y)*sigyy +
                    (1.0-s2.z*s2.z)*sigzz
		    - 2.0*(s2.x*s2.y*sigxy + s2.x*s2.z*sigxz +
			   s2.y*s2.z*sigyz);
  double xiovern3 = (1.0-s3.x*s3.x)*sigxx + (1.0-s3.y*s3.y)*sigyy +
                    (1.0-s3.z*s3.z)*sigzz
		    - 2.0*(s3.x*s3.y*sigxy + s3.x*s3.z*sigxz +
			   s3.y*s3.z*sigyz);

// The best of the three possibilities is that with the minimum of xi/n.

  if((xiovern1 <= xiovern2) && (xiovern1 <= xiovern3))
    {
      return Line(anchor, s1.x, s1.y, s1.z);
    } else {
      if (xiovern2 <= xiovern3)
	{
	  return Line(anchor, s2.x, s2.y, s2.z);
	} else {
	  return Line(anchor, s3.x, s3.y, s3.z);
	}
    }
}

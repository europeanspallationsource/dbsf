// $Header: /rap/lambda/Geom3d/RCS/fitplane.cxx,v 1.1 1996/02/23 02:02:08 mackay Exp $
// fitplane.cc: for inclusion in libGeom3d.lib

#include <iostream.h>
#include <Geom3d.hxx>

//================================================
Plane fitplane(const Point *const p, int n)
{ // Fit the best plane through a bunch of points.  A la "Graphic Gems III",
  // ("Newell's Method for Computing the Plane Equation of a Polygon")
  // p. 231, Ed. David Kirk, Acad. Press(1992)

  Point pave(0,0,0);  // average of all points
  Point norm(0,0,0);  // a vector normal to the best plane.
  int i;

  if(n<3)
    {
      cerr << "? fitplane: Not enough points to fit a plane." << endl;
      Plane temp;
      return temp;
    }

  for(i=0;i<n-1;i++)
    {
      pave += p[i];
      norm += cross(p[i]-p[i+1],p[i]+p[i+1]);
    }
  pave += p[n-1];
  pave /= n;
  norm += cross(p[n-1]-p[0],p[n-1]+p[0]);
  Plane temp(norm,0);
  temp += pave;
  return temp;
}

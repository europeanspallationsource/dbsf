// $Header: /rap/lambda/Geom3d/RCS/test2.cxx,v 1.1 1996/02/23 02:02:08 mackay Exp $
// This program tests some of the class functions defined in Geom3d.h,
// i.e., the "Point", "Line", and "Plane" classes.

#include <iostream.h>

#include "Geom3d.hxx"


int main()
{
//  Line linarr[10];

  cout << "sizeof(Line) = " << sizeof(Line) << endl;

  return 0;
}

// $Header: /rap/lambda/Geom3d/RCS/test3.cxx,v 1.1 1996/02/23 02:02:08 mackay Exp $
// This program tests some of the class functions defined in Geom3d.h,
// i.e., the "Point", "Line", and "Plane" classes.

#include <iostream.h>

#include "Geom3d.hxx"


int main()
{
  cout << "Test Point constructors\n";
  Point p1;
  Point p2(0.0, 0.0, 1.0);
  Point p3(1.0, 0.0, 0.0);
  cout << "\tp1 = " << p1 << endl;
  cout << "\tp2 = " << p2 << endl;
  cout << "\tp3 = " << p3 << endl;

  cout << "Test Point component access functions:\n";
  p3.x = 1.0;
  p3.y = 1.5;
  p3.z = 1.0;
  cout << "\t\"p1.x=1.0, p1.y=1.5, p1.z=1.0\"\n";
  cout << "\tp1 = " << p1 << endl;

  cout << "\tp1.x="<<p1.x;
  cout << "\tp1.y="<<p1.y;
  cout << "\tp1.z="<<p1.z << endl;

  cout << "p1+p2=" << p1+p2 << endl;
  cout << "p1-p2=" << p1-p2 << endl;
  cout << "p1*3=" << p1*3 << endl;
  cout << "p1/3=" << p1/3 << endl;

  p1 /=3; cout << "p1/=3:" << p1 << endl;
  p1 *=3; cout << "p1*=3:" << p1 << endl;

  p1 +=p2; cout << "p1+=p2:" << p1 << endl;
  p1 -=p2; cout << "p1-=p2:" << p1 << endl;

  Point p4 = -p1;
  cout << "\tp4=-p1: p4 = " << p4 << endl;

  cout << "p1 * p3 = " << p1*p3 << endl;
  cout << "p1 x p3 = " << cross(p1,p3) << endl;
  cout << "distance(p1,p2) = " << distance(p1,p2) << endl;

  cout << "p1*3:" << p1*3 << endl;
  cout << "3*p1:" << 3*p1 << endl;
  cout << "p1/3:" << p1/3 << endl;

  cout << "p1==p1:" << (p1==p1);
  cout << "\tp1!=p1:" << (p1!=p1);
  cout << "\tp1==p2:" << (p1==p2);
  cout << "\tp1!=p2:" << (p1!=p2) << endl;


  cout << "\tp1 = " << p1 << endl;
  cout << "rotate_x(p1,acos(0.0)) = " << rotate_x(p1,acos(0.0)) << endl;
  cout << "rotate_y(p1,acos(0.0)) = " << rotate_y(p1,acos(0.0)) << endl;
  cout << "rotate_z(p1,acos(0.0)) = " << rotate_z(p1,acos(0.0)) << endl;


/* Now let's try some of the Line functions */
  cout << "\nNow try some of the Line functions\n";

  Line l1(p1,p2);
  Line l2;
  cout << "l1:"<<l1<<endl;
  cout << "l2:"<<l2<<endl;
  double dist;
  cout << "intersection: " << intersect(l1,l2,&dist) << endl;
  cout << "\t\t dist = " << dist << endl;

  l1 = Line(Point(0,0,0),1,1,1);
  l2 = Line(Point(1,0,0),Point(0,1,0));
  cout << "l1:"<<l1<<endl;
  cout << "l2:"<<l2<<endl;
  cout << "intersection: " << intersect(l1,l2,&dist) << endl;
  cout << "\t\t dist = " << dist << endl;
  
  l2 = l1+p1;
  cout << "l1:"<<l1<<endl;
  cout << "l2:"<<l2<<endl;
  cout << "intersection: " << intersect(l1,l2,&dist) << endl;
  cout << "\t\t dist = " << dist << endl;
  
/* Plane */

  Plane m1(0.0,1.0,1.0,1.0);
  cout << "p1 =" << p1 << endl;
  cout << "Plane m1: " << m1 << endl;
  cout << "dist(p1,m1) = " << distance(p1,m1) << endl;

  return 0;
}

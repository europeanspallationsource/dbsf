#include <iostream.h>
#include <iomanip.h>

#ifndef _AMIGA
#include "String.h"
#else
#include "String.hpp"
#endif

#include <stdlib.h>
#include <math.h>

#include "Geom3d.hxx"

const int nfidmax = 20;

int main(int argc, char **argv)
{
  double N, W, E;
  Point fid[nfidmax];
  String fidname[nfidmax];
  Point beam[3];
  Point leg[5], sp[7]; // Ignore leg[0] and sp[0]
  int nfid = 0;

  cout.precision(6);
  cout.setf(ios::fixed, ios::floatfield);

  while(cin>>fidname[nfid])
    {
      cin >> N >> W >> E;
      fid[nfid] = Point(N,W,E);
      cout << "Point: " << fidname[nfid] << "\t" <<  fid[nfid]  << endl;
//      cout << N << "\t" << W << "\t" << E << endl;
      if(nfid >= nfidmax)
	{
	  cout  <<  "I quit!  There are too many fiducials.\n";
	  exit(1);
	}

      if(fidname[nfid] == "IP:")beam[0]  = fid[nfid];
      if(fidname[nfid] == "IN:")beam[1]  = fid[nfid];
      if(fidname[nfid] == "OUT:")beam[2] = fid[nfid];
      if(fidname[nfid] == "LEG1:")leg[1] = fid[nfid];
      if(fidname[nfid] == "LEG2:")leg[2] = fid[nfid];
      if(fidname[nfid] == "LEG3:")leg[3] = fid[nfid];
      if(fidname[nfid] == "LEG4:")leg[4] = fid[nfid];
      if(fidname[nfid] == "SP1:")sp[1] = fid[nfid];
      if(fidname[nfid] == "SP2:")sp[2] = fid[nfid];
      if(fidname[nfid] == "SP3:")sp[3] = fid[nfid];
      if(fidname[nfid] == "SP4:")sp[4] = fid[nfid];
      if(fidname[nfid] == "SP5:")sp[5] = fid[nfid];
      if(fidname[nfid] == "SP6:")sp[6] = fid[nfid];
      nfid++;
    }
  cout << endl;

  cout << "SP3:" << "\t" << sp[3] <<endl;
  cout << "SP6:" << "\t" << sp[6] <<endl;

  Line centerline(sp[3],sp[6]);  // centerline above midplane
  cout << "centerline: " << centerline << endl;

/*
  Plane legplane(leg[1],leg[2],leg[3]);
  cout << "legplane: " << legplane << endl;
  cout << "Distance from IP to plane of leg-fids:" << legplane << endl;
*/




  return 0;
}

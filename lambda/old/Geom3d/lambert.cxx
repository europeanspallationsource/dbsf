#include <iostream.h>
#include <iomanip.h>

#ifdef _AMIGA
#include "String.h"
#elseif
#include <String.h>
#endif

#include <stdlib.h>
#include <math.h>

#include "Geom3d.hxx"

const int nfidmax = 20;
const double convft = 1./0.3048;

int main(int argc, char **argv)
{
  cout.precision(6);
  cout.setf(ios::fixed, ios::floatfield);

  double bang = 0.038;
  double l5 = 2.41 + 0.00035;
  double l4 = l5 + (157.48-137.795)*0.0254/cos(bang/2.0);
  double l1 = l5 + (157.48-2.0)*0.0254/cos(bang/2.0);

  cout << "Y-line/Yellow  ring lamberson\n";

/*
5100     3706.326802  qfa6          quadrupole      0    1      1.110000
            31690.829416      0.000000  30336.518105
5114     3721.137849  q7o6          quadrupole      0    0      0.930000
            31690.690376      0.000000  30321.797714
*/

  Point yellow_q8ctr( 31690.829416, 0, 30336.518105 );
  Point yellow_q7ctr( 31690.690376, 0, 30321.797714 );
  double lenq7 = 0.93;
  double lenq8 = 1.11;

  Point normq8toq7 = normalize(yellow_q7ctr-yellow_q8ctr);

  Point yellow_q8end = yellow_q8ctr + (normq8toq7 * (lenq8/2.0));
  Point yellow_q7end = yellow_q7ctr - (normq8toq7 * (lenq7/2.0));

//  Point yellow_q8end( 31690.824174, 0, 30335.963129 );

  Point yp4 = yellow_q8end + l4 * normq8toq7;
  Point yp1 = yellow_q8end + l1 * normq8toq7;
  Point yp4_feet = yp4 * convft;
  Point yp1_feet = yp1 * convft;

  cout << "\nYellow Ring Lambertson:\n";
  cout << "yp1=\t" << yp1
    << "   dist to q8 inj pt = " << distance(yp1,yellow_q8end)
    << endl;
  cout << "yp4=\t" << yp4
    << "   dist to q8 inj pt = " << distance(yp4,yellow_q8end)
    << endl;
  cout << "center Q7 to Q8 = " << distance(yellow_q8ctr,yellow_q7ctr) << endl;

  cout << "yp1(feet)=\t" << yp1_feet
    << "   dist to q8 inj pt = " << distance(yp1,yellow_q8end)*convft
    << endl;
  cout << "yp4(feet)=\t" << yp4_feet
    << "   dist to q8 inj pt = " << distance(yp4,yellow_q8end)*convft
    << endl;
  cout << "center Q7 to Q8(feet) = "
    << distance(yellow_q8ctr,yellow_q7ctr)*convft << endl;



  Point ylamb_in( 31690.687648, 0.044223, 30329.554521 );
  Point ylamb_out(31690.801414, 0.032230, 30333.552885 );
  Point ylamb_ip( 31690.782520, 0.038229, 30331.552623 );
  double ylamb_theta = 0.028445458;  // at IP  azimuth
  double ylamb_phi   =-0.002998254;  // at IP  pitch
  double ylamb_psi   = 0.000057513;  // at IP  roll
  double lchord_ylamb = distance(ylamb_in,ylamb_out);
  cout << "\nY-line Injected beam coordinates:" << endl;
  cout << "  IN =\t" << ylamb_in << endl;
  cout << "  IP =\t" << ylamb_ip << endl;
  cout << " OUT =\t" << ylamb_out << endl;
  cout << " ylamb injection chord length = " << lchord_ylamb << endl;

  Line y_inray(ylamb_in,ylamb_ip);
  Line y_outray(ylamb_ip,ylamb_out);
  double ybang = angle(y_inray,y_outray);
  cout << "ylamb_bang = " << ybang << endl;

// Estimate heights of injected trajectory nearest yp1 and yp4.
  Line line14(yp1,yp4);
  Plane twoinches = rotate_y(Plane(line14.cosines(),0.0),bang/2.0)+yp1;
  Point yt1 = intersect(twoinches,Line(ylamb_in,ylamb_ip));
  Plane fiftycm = twoinches + (yp4-yp1);
  Point yt4 = intersect(fiftycm,Line(ylamb_ip,ylamb_out));

//  cout << "line14:" << line14 << endl;
//  cout << "twoinches:" << twoinches << endl;
//  cout << "yp4-yp1" << yp4-yp1 << endl;
//  cout << "fiftycm:" << fiftycm << endl;

  cout << "yt1=\t" << yt1 << endl;
  cout << "yt4=\t" << yt4 << endl;

  Point yp5 = yellow_q8end + l5 * normalize(yellow_q7ctr-yellow_q8end);
  cout << "yp5=\t" << yp5
    << "   dist to q8 inj pt = " << distance(yp5,yellow_q8end)
    << endl;
  cout << " deviation  yp5-y_out: " << yp5-ylamb_out << endl;

  Point ipshift = ylamb_ip-(ylamb_in+ylamb_out)/2.0;
  cout << "IP-(IN+OUT)/2 = " << ipshift;
  double shift = magnitude(ipshift);
  cout << "  distance = " << shift << endl;

//-- -- -- -- -- -- --
  cout << endl;

  cout << "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --\n";
  cout << "X-line/Blue ring  lambertson\n";

/*
 142      111.777151  q7o6          quadrupole      0    0      0.930000
            31698.523350      0.000000  30097.479263
 156      126.408198  qfa6          quadrupole      0    0      1.110000
            31699.688841      0.000000  30082.804425
*/

  Point   blue_q8ctr( 31699.688841, 0, 30082.804425 );
  Point   blue_q7ctr( 31698.523350, 0, 30097.479263 );
  normq8toq7 = normalize(blue_q7ctr-blue_q8ctr);
  Point blue_q8end = blue_q8ctr + (normq8toq7 * (lenq8/2.0));

  Point bp4 = blue_q8end + l4 * normalize(blue_q7ctr-blue_q8end);
  Point bp1 = blue_q8end + l1 * normalize(blue_q7ctr-blue_q8end);
  Point bp1_feet = bp1*convft;
  Point bp4_feet = bp4*convft;

  cout << "\nBlue Ring Lambertson:\n";
  cout << "bp1=\t" << bp1
    << "   dist to q8 inj pt = " << distance(bp1,blue_q8end)
    << endl;
  cout << "bp4=\t" << bp4
    << "   dist to q8 inj pt = " << distance(bp4,blue_q8end)
    << endl;
  cout << "center Q7 to Q8 = " << distance(blue_q8ctr,blue_q7ctr) << endl;


  cout << "bp1(feet)=\t" << bp1_feet
    << "   dist to q8 inj pt = " << distance(bp1,blue_q8end)*convft
    << endl;
  cout << "bp4(feet)=\t" << bp4_feet
    << "   dist to q8 inj pt = " << distance(bp4,blue_q8end)*convft
    << endl;
  cout << "center Q7 to Q8(feet) = "
    << distance(blue_q8ctr,blue_q7ctr)*convft << endl;


  Point xlamb_in( 31699.061648, 0.044223, 30089.741155 );
  Point xlamb_out(31699.454071, 0.032229, 30085.760466 );
  Point xlamb_ip( 31699.295700, 0.038228, 30087.754520 );
  double xlamb_theta = 3.043337612;  // at IP  azimuth
  double xlamb_phi   =-0.002998505;  // at IP  pitch
  double xlamb_psi   = 0.000056079;  // at IP  roll
  double lchord_xlamb = distance(xlamb_in,xlamb_out);
  cout << "\nX-line Injected beam coordinates:" << endl;
  cout << "  IN =\t" << xlamb_in << endl;
  cout << "  IP =\t" << xlamb_ip << endl;
  cout << " OUT =\t" << xlamb_out << endl;
  cout << " xlamb injection chord length = " << lchord_xlamb << endl;

  cout << endl;
  
  Line x_inray(xlamb_in,xlamb_ip);
  Line x_outray(xlamb_ip,xlamb_out);
  double xbang = angle(x_inray,x_outray);
  cout << "xlamb_bang = " << xbang << endl;
// Estimate heights of injected trajectory nearest bp1 and bp4.
  line14 =Line(bp1,bp4);
  twoinches = rotate_y(Plane(line14.cosines(),0.0),-bang/2.0)+bp1;
  Point xt1 = intersect(twoinches,Line(xlamb_in,xlamb_ip));
  fiftycm = twoinches + (bp4-bp1);
  Point xt4 = intersect(fiftycm,Line(xlamb_ip,xlamb_out));

//  cout << "line14:" << line14 << endl;
//  cout << "twoinches:" << twoinches << endl;
//  cout << "xp4-xp1" << xp4-xp1 << endl;
//  cout << "fiftycm:" << fiftycm << endl;

  cout << "xt1=\t" << xt1 << endl;
  cout << "xt4=\t" << xt4 << endl;

  Point bp5 = blue_q8end + l5 * normalize(blue_q7ctr-blue_q8end);
  cout << "bp5=\t" << bp5
    << "   dist to q8 inj pt = " << distance(bp5,blue_q8end)
    << endl;
  cout << " deviation  bp5-x_out: " << bp5-xlamb_out << endl;

  ipshift = xlamb_ip-(xlamb_in+xlamb_out)/2.0;
  cout << "IP-(IN+OUT)/2 = " << ipshift;
  shift = magnitude(ipshift);
  cout << "  distance = " << shift << endl;

// Summarize:

  cout << "" << endl;
  
  cout << "\nYellow Ring Lambertson in meters:\n";
  cout << "              N[m]         W[m]       E[m]\n";
  cout << "  p1 =\t" << yp1
    << "   dist to q8 inj pt = " << distance(yp1,yellow_q8end)
    << endl;
  cout << "  p4 =\t" << yp4
    << "   dist to q8 inj pt = " << distance(yp4,yellow_q8end)
    << endl;
  cout << "  IN =\t" << ylamb_in << endl;
  cout << "  IP =\t" << ylamb_ip << endl;
  cout << " OUT =\t" << ylamb_out << endl;
  cout << "  t1 =\t" << yt1 << endl;
  cout << "  t4 =\t" << yt4 << endl;

  cout << "\nBlue Ring Lambertson in meters:\n";
  cout << "              N[m]         W[m]       E[m]\n";
  cout << "  p1 =\t" << bp1
    << "   dist to q8 inj pt = " << distance(bp1,blue_q8end)
    << endl;
  cout << "  p4 =\t" << bp4
    << "   dist to q8 inj pt = " << distance(bp4,blue_q8end)
    << endl;
  cout << "  IN =\t" << xlamb_in << endl;
  cout << "  IP =\t" << xlamb_ip << endl;
  cout << " OUT =\t" << xlamb_out << endl;
  cout << "  t1 =\t" << xt1 << endl;
  cout << "  t4 =\t" << xt4 << endl;

  cout << "\nYellow Ring Lambertson in feet:\n";
  cout << "              N[feet]      W[feet]    E[feet]\n";
  cout << "  p1 =\t" << yp1*convft
    << "   dist to q8 inj pt = " << distance(yp1,yellow_q8end)*convft
    << endl;
  cout << "  p4 =\t" << yp4*convft
    << "   dist to q8 inj pt = " << distance(yp4,yellow_q8end)*convft
    << endl;
  cout << "  IN =\t" << ylamb_in*convft << endl;
  cout << "  IP =\t" << ylamb_ip*convft << endl;
  cout << " OUT =\t" << ylamb_out*convft << endl;
  cout << "  t1 =\t" << yt1*convft << endl;
  cout << "  t4 =\t" << yt4*convft << endl;

  cout << "\nBlue Ring Lambertson in feet:\n";
  cout << "              N[feet]      W[feet]    E[feet]\n";
  cout << "  p1 =\t" << bp1*convft
    << "   dist to q8 inj pt = " << distance(bp1,blue_q8end)*convft
    << endl;
  cout << "  p4 =\t" << bp4*convft
    << "   dist to q8 inj pt = " << distance(bp4,blue_q8end)*convft
    << endl;
  cout << "  IN =\t" << xlamb_in*convft << endl;
  cout << "  IP =\t" << xlamb_ip*convft << endl;
  cout << " OUT =\t" << xlamb_out*convft << endl;
  cout << "  t1 =\t" << xt1*convft << endl;
  cout << "  t4 =\t" << xt4*convft << endl;

  return 0;
}

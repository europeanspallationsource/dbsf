/* $Header: /usr2/local/lambda/survey/include/RCS/Survey_data.h,v 1.2 1994/02/18 21:43:08 mackay Exp $ */

#ifndef SURVEY_DATA_H
#define SURVEY_DATA_H 1

#ifndef ISTKsdsgen_h
#include <Sds/sdsgen.h>
#endif
#ifndef survout_h
#include <survout.h>
#endif

typedef struct
{
  struct survey_out *survey_ptr;
  long number_of_survey;
  struct delta *delta_ptr;
  long number_of_delta;
} Survey_data;

#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN extern
#endif

EXTERN int survey_in_sds(sds_handle sds, Survey_data *sdp);
#undef EXTERN

#endif /* SURVEY_DATA_H */

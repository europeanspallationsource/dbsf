#ifndef FLATIN_H
#define FLATIN_H

/* $Header: /rap/lambda/flatin/include/RCS/flatin.h,v 1.5 2004/06/14 18:02:32 satogata Exp $ */

#ifndef ISTKsdsgen_h         /* We're gonna need it anyway */
#include <Sds/sdsgen.h>
#endif

#define LATTICE_BASE_LEVEL 0
#define LATTICE_SLOT_LEVEL 1
#define LATTICE_SUPERSLOT_LEVEL 2

/* These define the row_struct array without using cah pointers:
   somewhat wasteful of space but will allow fairly easy
   transport between programs and languages even without SDS
   */
#define NAME_FLATIN_MAX 64
#define COLUMN_FLATIN_MAX 22

/* These two are not needed if SDS is used. If SDS isn't used, they should
   be derived so we presume lattice_inits.h has been loaded..... */

#ifndef SDS_MAGIC_FLATIN
#define SDS_MAGIC_FLATIN
#define NUMBER_OF_COLUMN_ENTRIES sizeof(row)/sizeof(row_struct) -1
#define LEGAL_TYPE_COUNT  sizeof(legal_type)/sizeof(legal)
#endif

typedef struct {
  char name[NAME_FLATIN_MAX];
  double value;
} parameter;

typedef struct {
  char  name[NAME_FLATIN_MAX];
  char  type[NAME_FLATIN_MAX];
  char  sub_type[NAME_FLATIN_MAX];
  double  length;
  double  strength;
  double  tilt;
  int    type_index;
  int    npars;
  int    length_index;
  int    strength_index;
  int    tilt_index;
  int    more_index;
} element;

typedef struct {
  double s;
  int  element_index;
  int  type_index;
  int occurence;
  int level;
  int hook_index;
  int sense;
} atom;

struct legal_type {
  char name[NAME_FLATIN_MAX];
  int  has_length;
  int  has_strength;
  int  has_tilt;
  int  has_more;
}; 

typedef struct legal_type legal;

struct row {
  char    name[NAME_FLATIN_MAX];
  int     cols;
  char    heading[COLUMN_FLATIN_MAX][NAME_FLATIN_MAX];
};

typedef struct row row_struct;

typedef struct {
  int     morend;
  double  attribute[COLUMN_FLATIN_MAX];
} more;

typedef struct {
  parameter *parameter_ptr;
  int number_of_parameters;
  element *element_ptr;
  int number_of_elements;
  atom *atom_ptr;
  int number_of_atoms;
  legal *legal_type_ptr;
  int number_of_legal_types;
  row_struct *row_ptr;
  int number_of_rows;
} flatin_data;

#ifdef __cplusplus  /* This makes it work with C++ */
#define EXTERN extern "C"
#else
#define EXTERN extern
#endif

EXTERN int flatin(char *lattice, flatin_data *fdp);
EXTERN int flatin_sds(sds_handle sds, flatin_data *fdp);
EXTERN int raw_flatin_sds(sds_handle sds, flatin_data *fdp);

#undef EXTERN


#endif /* FLATIN_H */

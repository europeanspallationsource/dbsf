
/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_synch.h,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

/* header file for synch output routines
 - prints full expressions
 - prints parameters then magnet definitions then beam definitions
 - if grad_brho is true, try to parse strength definitions for grad and
   brho values to output in element declarations, otherwise use K 1.0 form
*/

extern void print_synch( /* symbol root; int grad_brho */ );

/* end of file */

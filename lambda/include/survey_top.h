#ifndef SURVEY_TOP_H
#define SURVEY_TOP_H

/* $Header: /usr2/local/lambda/survey/include/RCS/survey_top.h,v 1.2 1993/07/07 18:02:59 peggs Exp $ */

#define TOP_BUFF_MAX       1024

typedef struct {
  FILE *three_d;
  FILE *topx;
  FILE *topy;
  FILE *topz;
  double *temp;
  coord        *c_ptr;
  element      *e_ptr;
  local_matrix *l_ptr;
} top;

#endif /* SURVEY_TOP_H */

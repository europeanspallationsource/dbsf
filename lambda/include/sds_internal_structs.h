
/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/sds_internal_structs.h,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

/* SDS INTERNAL STRUCTS *************************************** */

struct parameter
{
    char name[NAME_MAX];
    double value;
};

struct element
{
    char	name[NAME_MAX];
    char	type[NAME_MAX];
    char	sub_type[NAME_MAX];
    double	length;
    double	strength;
    double	tilt;
    int		type_index;
    int		npars;
    int		length_index;
    int		strength_index;
    int		tilt_index;
    int		more_index;
};

struct lattice
{
    double	s;
    int		element_index;
    int		type_index;
    int		occurence;
    int		level;
    int		hook_index;
    int		sense;
};

struct legal_type {
    char	name[NAME_MAX];
    int		has_length;
    int		has_strength;
    int		has_tilt;
    int		has_more;
}; 

struct row {
    char	name[NAME_MAX];
    int		cols;
    char	heading[COLUMN_MAX][NAME_MAX];
};

struct dimensions {
    char	ashape;
    double	axsize;
    double	aysize;
    double	acenterx;
    double	acentery;
    char	mshape;
    double	mxsize;
    double	mysize;
    double	mcenterx;
    double	mcentery;
};

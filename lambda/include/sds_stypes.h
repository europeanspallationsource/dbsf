
/* $Header: /rap/lambda/dbsf/lattice_tree/RCS/sds_stypes.h,v 1.1 1994/06/07 19:31:42 trahern Exp satogata $ */

/* SDS STRUCTS *************************************** */
#define NAME_MAX 64
#define COLUMN_MAX 22

struct type_list p_tlist[] =
{
    { (long)NAME_MAX, SDS_FSTRING },
    { (long)1, SDS_DOUBLE },
    { (long)0, SDS_RETLIST }, 
    { (long)0, SDS_ENDLIST }
};

char p_names[] = "name,value";

struct type_list e_tlist[] =
{
    { (long)NAME_MAX, SDS_FSTRING },
    { (long)NAME_MAX, SDS_FSTRING },
    { (long)NAME_MAX, SDS_FSTRING },
    { (long)1, SDS_DOUBLE },
    { (long)1, SDS_DOUBLE },
    { (long)1, SDS_DOUBLE },
    { (long)1, SDS_INT },
    { (long)1, SDS_INT },
    { (long)1, SDS_INT },
    { (long)1, SDS_INT },
    { (long)1, SDS_INT },
    { (long)1, SDS_INT },
    { (long)0, SDS_RETLIST },
    { (long)0, SDS_ENDLIST }
};

char e_names[] = "name,type,sub_type,length,strength,tilt,type_index,npars,length_index,strength_index,tilt_index,more_index";

struct type_list l_tlist[] =
{
    { (long)1, SDS_DOUBLE },
    { (long)1, SDS_INT },
    { (long)1, SDS_INT },
    { (long)1, SDS_INT },
    { (long)1, SDS_INT },
    { (long)1, SDS_INT },
    { (long)1, SDS_INT },
    { (long)0, SDS_RETLIST },
    { (long)0, SDS_ENDLIST }
};

char l_names[] = "s,element_index,type_index,occurence,level,hook_index,sense";

struct type_list legal_tlist[] =
{
    { (long)NAME_MAX, SDS_FSTRING },
    { (long)1, SDS_INT },
    { (long)1, SDS_INT },
    { (long)1, SDS_INT },
    { (long)1, SDS_INT },
    { (long)0, SDS_RETLIST },
    { (long)0, SDS_ENDLIST }
};

char legal_names[] = "name,has_length,has_strength,has_tilt,has_more";

#define COLUMN_TYPE_CODE_PLACE 2

struct type_list row_tlist[] =
{
    { (long)NAME_MAX, SDS_FSTRING },
    { (long)1, SDS_INT },
    { (long)COLUMN_MAX, 0 },
    { (long)0, SDS_RETLIST }, 
    { (long)0, SDS_ENDLIST }
};

char row_names[] = "name,cols,column";

struct type_list head_tlist[] =
{
    { (long)NAME_MAX, SDS_FSTRING },
    { (long)0, SDS_RETLIST },
    { (long)0, SDS_ENDLIST }
};

char head_names[] = "heading";

struct type_list dimensions_tlist[] =
{
    { (long)1, SDS_FSTRING },
    { (long)1, SDS_DOUBLE },
    { (long)1, SDS_DOUBLE },
    { (long)1, SDS_DOUBLE },
    { (long)1, SDS_DOUBLE },
    { (long)1, SDS_FSTRING },
    { (long)1, SDS_DOUBLE },
    { (long)1, SDS_DOUBLE },
    { (long)1, SDS_DOUBLE },
    { (long)1, SDS_DOUBLE },
    { (long)0, SDS_RETLIST }, 
    { (long)0, SDS_ENDLIST }
};

char dimensions_names[] = "ashape,axsize,aysize,acenterx,acentery,mshape,mxsize,mysize,mcenterx,mcentery";

/* end of file */

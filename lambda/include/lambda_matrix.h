/* $Header: /usr/local/lambda/matrix/include/RCS/lambda_matrix.h,v 1.2 1994/05/27 13:56:41 mackay Exp $ */
/* Matrix functions for twiss and survey. */

#ifndef lambda_matrix_h
#define lambda_matrix_h 1

#ifdef __cplusplus
extern "C" {
#endif

extern void cp_matrix(double **a,double **b,int n);
extern void cp_vector(double *x, double * y, int n);
extern double *dim1(int grid);
extern double **dim2(int grid, int row);
extern double ***dim3(int grid, int row, int col);
extern void free2(double **pa);
extern void free3(double ***pa);
extern void matrix_matrix(double **c, double **a, double **b, int n);
extern void matrix_vector(double *y, double **a, double *x, int n);
extern void print_matrix(FILE *file_ptr, double **m, int n);
extern void print_vect(FILE *file_ptr, double *x, int n);
extern void unit_matrix(double **m, int n);

#ifdef __cplusplus
}
#endif

#endif

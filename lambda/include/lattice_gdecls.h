
/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_gdecls.h,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

/* LATTICE GLOBAL DECLARATIONS */

/* various string, character and form buffering macros used in lots of places */

#define BUF_CHAR(x)	{ *buffer++ = x; }
#define BUF_FORM(x,y)	{ sprintf( buffer, x, y ); buffer += strlen( buffer ); }
#define BUF_FORM2(x,y,z) \
    { sprintf( buffer, x, y, z ); buffer += strlen( buffer ); }
#define BUF_STRING(x)	BUF_FORM("%s",x)

#define MAX_BUFFER	4096

#define OUTBUFF8(form,s,t,u,v,w,x,y,z) {sprintf(out_buff,form,s,t,u,v,w,x,y,z);}
#define OUTBUFF7(form,t,u,v,w,x,y,z) { sprintf( out_buff, form, t,u,v,w,x,y,z);}
#define OUTBUFF6(form,u,v,w,x,y,z) { sprintf( out_buff, form, u, v, w, x, y,z);}
#define OUTBUFF5(form,v,w,x,y,z) { sprintf( out_buff, form, v, w, x, y, z ); }
#define OUTBUFF4(form,w,x,y,z)	{ sprintf( out_buff, form, w, x, y, z ); }
#define OUTBUFF3(form,x,y,z)	{ sprintf( out_buff, form, x, y, z ); }
#define OUTBUFF2(form,y,z)	{ sprintf( out_buff, form, y, z ); }
#define OUTBUFF1(form,z)	{ sprintf( out_buff, form, z ); }
#define OUTBUFF0(form)		{ sprintf( out_buff, form ); }

/* this is for various magnet tilt comparisons */
#define PI		3.141592653589793238
#define HALFPI		( PI / 2 )
#define TOLERANCE	.0000001

/* this is the format string for ALL floating point output */
#define FLOAT_FORMAT	"%.14g" 

/* the array of element attribute names */
extern char *elem_attr_names[];

/* the array of element attribute default values in character format */
/* CHECK THESE BEFORE USE, MANY ARE STILL UNSET */
extern char *elem_attr_defaults[];

/* the array of multipole attribute names */
extern char *multi_attr_names[];

/* the array of element names */
extern char *elem_names[];

/* the array of templates for ordering arguments consistently */
/* currently used by flat and sds style outputs */
extern int *elem_arg_forms[];

/* END OF FILE */

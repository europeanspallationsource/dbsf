/* symbol_table_lib.h - definitions file for symbol table library */

/* needed for caddr_t definition */
#include <sys/types.h>

typedef caddr_t symbol_data_type;

typedef struct hash_entry
    {
    struct hash_entry *prev, *next;
    char *name;
    symbol_data_type symbol_data;
    } *symbol;

typedef struct hash_entry **hash_table;

typedef struct symbol_table
    {
    int num_entries;
    symbol_data_type no_symbol_val;
    hash_table h_table;
    } *symbol_table;

#define NULL_SYMBOL ((symbol) 0)
#define NULL_SYMBOL_TABLE ((symbol_table) 0)

symbol_table sym_table_create( /* num_entries, no_sym_val */ );
symbol sym_install( /* sym_tbl, sym_name, sym_data */ );
symbol sym_find( /* sym_tbl, sym_name */ );
symbol_data_type sym_lookup( /* sym_tbl, sym_name */ );

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct thing {
  struct thing *next;
  char *this;
  int  number_of_elements;
};

static int number_of_heaps = 0;


struct heap {
  struct thing *head;
  struct thing *tail;
  int size;
  int number_of_things;
  int current_index;
};



static struct thing *heaps;
static struct thing *get_heap();
static void listless();


int
NewHeap(size)
int size;
{
  struct thing *newheap;
  struct thing *lastheap;
  struct heap *heap;

  newheap = (struct thing *)calloc(1, sizeof(struct thing));

  if (newheap == 0)
    listless("heap pointer allocation");

  newheap->next = 0;

  newheap->this = malloc(sizeof(struct heap));
  heap = (struct heap *)newheap->this;

  heap->number_of_things = 0;
  heap->head = 0;
  heap->size = size;
  heap->current_index = 0;

  if (number_of_heaps == 0)
    heaps = newheap;

  number_of_heaps++;
  lastheap = get_heap(number_of_heaps - 2);

  lastheap->next = newheap;
	return number_of_heaps - 1;
}



static
struct thing *
get_heap(i)
int i;
{
  int j = 0;
  struct thing *current = heaps;

	if (i < 0) i = 0;

  if (i >= number_of_heaps)
    return 0;
  while (j != i)
	{
    current = current->next;
		j++;
	}
  return current;
}


int 
AddToHeap(heapnumber,data,number_of_elements)
int heapnumber;
char *data;
int number_of_elements;
{
  struct thing *heapthing;
  struct thing *tempthing;
  struct thing *newthing;
  struct heap *heap;
  int old_index;

  if ((heapthing = get_heap(heapnumber)) == 0)
    listless("No such heap");

  heap = (struct heap *)heapthing->this;
  tempthing = heap->tail;
  newthing = 
  heap->tail = (struct thing *)malloc(sizeof(struct thing));

  newthing->this = data;
  newthing->number_of_elements = number_of_elements;

  if (heap->number_of_things == 0)
  {
    heap->head = heap->tail;
  }
  else
  {
    tempthing->next = heap->tail;
  }
  newthing->next = 0;
  heap->number_of_things++;
  old_index = heap->current_index;
  heap->current_index += number_of_elements;
  return old_index;
}



char *
ConstructHeap(heapnumber)
int heapnumber;
{
  struct thing *heapthing = get_heap(heapnumber);
  struct heap *heap = (struct heap *)heapthing->this;
  char *dataheap;
  char *ptr;
  struct thing *thing;

  if (heap == 0)
    listless("Cannot construct heap - it doesn't exist");

  dataheap = malloc(heap->size * heap->current_index);
  if (dataheap == 0)
    listless("Can't malloc for heap construction");

  thing = heap->head;
  ptr = dataheap;
  do 
  {
    memcpy(ptr, thing->this, heap->size * thing->number_of_elements);
    ptr += heap->size * thing->number_of_elements;
  } 
  while ((thing = thing->next) != 0); 
  return dataheap;
} 

int
CurrentSize(heapnumber)
{
  struct heap *heap;
	struct thing *heapthing;

  if ((heapthing = get_heap(heapnumber)) == 0)
    listless("No such heap");

  heap = (struct heap *)heapthing->this;
	return heap->current_index;
}

int
CurrentNumber(heapnumber)
{
  struct heap *heap;
	struct thing *heapthing;

  if ((heapthing = get_heap(heapnumber)) == 0)
    listless("No such heap");

  heap = (struct heap *)heapthing->this;
	return heap->number_of_things;
}



static
void
listless(msg)
char *msg;
{
  fprintf(stderr,"From list code:\n");
  fprintf(stderr,"%s\n",msg);
  exit(1);
}




/* array.c - utility routines to deal with array */

/* finds the minimum and maximum value of double-precision array "array".
 * Each element in the array is assumed to be "step_size" *bytes* from
 * the next one.  The total size of the array is taken as "num_elems"
 * elements.  The maximum and minimum are returned in min_addr and
 * max_addr respectively.
 *
 * An example use to find the minimum of the "x" field of an array
 * of structs:
 *
 *	double min_val, max_val;
 *
 *	find_array_extremum( &array[0].x,
 *		sizeof( array[0] ),
 *		&array[last] - array + 1,
 *		&min_val, &max_val );
 */

void find_array_extremum( register double *array,
			 register int step_size,
			 int num_elems, double *min_addr,
			 double *max_addr )
{
  register double min_val, max_val;
  double *last_elem = (double *) (((char *) array) + num_elems * step_size);

  max_val = min_val = *array;

  while ( array < last_elem )
    {
      if ( *array < min_val )
	min_val = *array;
      else if ( *array > max_val )
	max_val = *array;

      array = (double *) (((char *) array) + step_size);
    }

  *min_addr = min_val;
  *max_addr = max_val;
}

/* utility_lib.c - library of (non-window) utility routines */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/types.h> /* needed by stat.h */
#include <sys/stat.h>

#include "utility.h"

int *int_array_alloc( int size );
char *struct_array_alloc( int size );
int utility_alloc_gripe( char msg[], int size );
int utility_gripe( char msg[] );

/* returns a copy of string "str" */
char *copy_string(char str[])
{
  register char *copy = malloc( strlen( str ) + 1 );
  return ( strcpy( copy, str ) );
}


/* converts a string to upper case */
char *str_to_upper( char str[] )
    {
    register char *s, c;

    for ( s = str; (c = *s); ++s )
	if ( islower(c) )
	    *s = toupper(c);
    
    return ( str );
    }


/* returns a factor of "n" suitable for splitting "n" items into rows and
 * columns
 */

int nice_factor( int n )
    {
    register int sqrt_n, delta;

    /* we do this the crude way because we don't want to force the user
     * to link with the math library
     */

    for ( sqrt_n = 1; sqrt_n * sqrt_n < n; ++sqrt_n )
	;

    for ( delta = 0; delta < n; ++delta )
	{
	if ( n % (sqrt_n + delta) == 0 )
	    return ( sqrt_n + delta );
	else if ( n % (sqrt_n - delta) == 0 )
	    return ( sqrt_n - delta );
	}

    /* this really can't happen */
    return ( 1 );
    }


/* returns true if a file exists, false otherwise */

int file_exists( char file[] )
    {
    struct stat buf;

    return ( stat( file, &buf ) == 0 );
    }


/* returns a pointer to the last simple name of a file name */

char *last_simple_name( char file[] )
    {
    register char *lsn = strrchr( file, '/' );

    return ( lsn ? lsn + 1 : file );
    }


char **string_array_alloc( int size )
    {
    register char **new = (char **) malloc( size * sizeof( char * ) );

    if ( new == (char **) NULL )
	utility_alloc_gripe( "could not allocate string array", size );
    
    return ( new );
    }


double *double_array_alloc( int size )
    {
    register double *new = (double *) malloc( size * sizeof( double  ) );

    if ( new == (double *) NULL )
	utility_alloc_gripe( "could not allocate double array", size );
    
    return ( new );
    }


float *float_array_alloc( int size )
    {
    register float *new = (float *) malloc( size * sizeof( float  ) );

    if ( new == (float *) NULL )
	utility_alloc_gripe( "could not allocate float array", size );
    
    return ( new );
    }


int *int_array_alloc( int size )
    {
    register int *new = (int *) malloc( size * sizeof( int  ) );

    if ( new == (int *) NULL )
	utility_alloc_gripe( "could not allocate int array", size );
    
    return ( new );
    }


char *struct_array_alloc( int size )
    {
    register char *new = malloc( size * sizeof( char  ) );

    if ( new == NULL )
	utility_alloc_gripe( "could not allocate struct array", size );
    
    return ( new );
    }


int utility_alloc_gripe( char msg[], int size )
{
  char fmt_msg[200];
  
  sprintf( fmt_msg, "%s (%d elements)", msg, size );
  return utility_gripe( fmt_msg );
}


int utility_gripe( char msg[] )
{
  fprintf( stderr, "utility library error - %s\n", msg );
  exit( 1 );
}

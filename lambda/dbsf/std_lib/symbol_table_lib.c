/* symbol_table_lib - library of symbol table routines */

#include <stdlib.h>
#include <string.h>
#include "symbol_table_lib.h"

/* sym_table_create - create a symbol table */

int hashfunct( char *str, int hash_size );
symbol findsym( char *sym, hash_table table, int table_size );

symbol_table sym_table_create( int n_entries, symbol_data_type no_sym_val)
{
  symbol_table tbl;
  int i;
  hash_table ht;

  if ( ! (tbl = (symbol_table) malloc( sizeof( struct symbol_table ) )) )
    return ( NULL_SYMBOL_TABLE );
    
  if ( ! (tbl->h_table = ht =
	  (hash_table) malloc( sizeof( struct hash_entry ) * n_entries )) ) {
    free( tbl );
    return ( NULL_SYMBOL_TABLE );
  }
    
  tbl->num_entries = n_entries;
  tbl->no_symbol_val = no_sym_val;

  for ( i = 0; i < n_entries; ++i )
    ht[i] = NULL_SYMBOL;

  return ( tbl );
}


symbol sym_install( symbol_table sym_tbl, char *sym_name, symbol_data_type sym_data )
{
  symbol newsym, addsym();

  if ( ! (newsym =
	  addsym( sym_name, sym_tbl->h_table, sym_tbl->num_entries )) )
    return ( NULL_SYMBOL );

  newsym->symbol_data = sym_data;

  return ( newsym );
}


/* sym_find - return symbol table entry associated with a name */

symbol sym_find( symbol_table sym_tbl, char sym_name[] )
{
  return ( findsym( sym_name, sym_tbl->h_table, sym_tbl->num_entries ) );
}


/* sym_lookup - look up the symbol data associated with a name */

symbol_data_type sym_lookup( symbol_table sym_tbl, char *sym_name )
{
  symbol sym;

  sym = findsym( sym_name, sym_tbl->h_table, sym_tbl->num_entries );

  return ( sym ? sym->symbol_data : sym_tbl->no_symbol_val );
}



/* Private Routines */

/* addsym - add symbol to a hash table, if not already present.  Return entry.
 *
 * synopsis
 *    symbol newsym, addsym();
 *    char *sym;
 *    hash_table table;
 *    int table_size;
 *    newsym = addsym( sym, table, table_size );
 */

symbol addsym( char *sym, hash_table table, int table_size )
{
  int hash_val = hashfunct( sym, table_size );
  symbol entry = table[hash_val];
  symbol new_entry;
  symbol successor;

  while ( entry ) {
    if ( ! strcmp( sym, entry->name ) ) { /* entry already exists */
      return ( entry );
    }
	
    entry = entry->next;
  }
    
  /* create new entry */
  if ( ! (new_entry = (symbol) malloc( sizeof( struct hash_entry ) )) )
    return ( NULL_SYMBOL );

  if ( (successor = table[hash_val]) ) { /* add to beginning of bucket list */
    new_entry->next = successor;
    successor->prev = new_entry;
  }
  else
    new_entry->next = NULL_SYMBOL;

  new_entry->prev = NULL_SYMBOL;
  new_entry->name = sym;

  table[hash_val] = new_entry;

  return ( new_entry );
}


/* findsym - find symbol in symbol table
 *
 * synopsis
 *    char sym[];
 *    hash_table table;
 *    int table_size;
 *    symbol entry, findsym();
 *    entry = findsym( sym, table, table_size );
 */

symbol findsym( char sym[], hash_table table, int table_size )
{
  symbol entry = table[hashfunct( sym, table_size )];

  while ( entry ) {
    if ( ! strcmp( sym, entry->name ) )  return ( entry );
    entry = entry->next;
  }

  return ( NULL_SYMBOL );
}

    
/* hashfunct - compute the hash value for "str" and hash size "hash_size"
 *
 * synopsis
 *    char str[];
 *    int hash_size, hash_val;
 *    hash_val = hashfunct( str, hash_size );
 */
int hashfunct( char str[], int hash_size )
{
  int hashval;
  int locstr;

  hashval = 0;
  locstr = 0;

  while ( str[locstr] )
    hashval = ((hashval << 1) + str[locstr++]) % hash_size;

  return ( hashval );
}

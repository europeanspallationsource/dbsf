/* @(#) $Header: /scratch/trahern/dbsf/std_lib/RCS/utility.h,v 1.1 1994/06/07 19:32:45 trahern Exp $ (LBL) */
/* utility.h - standard header file for control programs */

/* this file defines a bunch of generally useful things */

#define DEFINE_ICON(name,load_cmd) \
	static short CAT(name,_icon_image)[] = \
		{ \
		load_cmd \
		}; \
	DEFINE_ICON_FROM_IMAGE(CAT(name,_icon),CAT(name,_icon_image))

#define ICON_IMAGE(name) (&CAT(name,_icon).ic_mpr)

#define DEFINE_CURSOR_IMAGE(name,load_cmd) \
	static short CAT(name,_cursor_data)[] = \
		{ \
		load_cmd \
		}; \
	mpr_static(CAT(name,_cursor_image), 16, 16, 1,CAT(name,_cursor_data));


#define YES 1
#define NO 0

#define MAX_LINE 512

/* returns a pointer to an object of a given type.  The object is
 * uninitialized
 */
#define ALLOC(type) ((struct type *) malloc( sizeof(struct type) ))

/* returns a pointer to an object of a given type.  The object is zeroed */
#define CALLOC(type) ((struct type *) calloc( 1, sizeof(struct type) ))

extern char *copy_string(char *), *str_to_upper(char *);

int file_exists();
char *last_simple_name();

char **string_array_alloc();
double *double_array_alloc();
float *float_array_alloc();
int *int_array_alloc();
char *struct_array_alloc();

#define allocate_structure_array( type, size ) \
	((type *) struct_array_alloc( sizeof( type ) * size ))

#define free_array( array ) \
	free( (char *) array )



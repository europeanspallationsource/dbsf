/* convert an f77 string to a C string.  This routine trashes the f77 string */

#include "utility.h"

char *f77_str_to_C( char f77_str[], int len )
    {
    register int i;

    for ( i = len - 1; i > 0; --i )
	if ( f77_str[i] != ' ' )
	    break;

    if ( i == len - 1 )
	/* truncate last character */
	f77_str[i] = '\0';
    else
	f77_str[i + 1] = '\0';

    return ( copy_string( f77_str ) );
    }

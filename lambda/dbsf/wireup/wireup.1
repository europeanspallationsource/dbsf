.TH WIREUP N "15 March 1993" "EPB Version 3.4" "PROGRAMS FOR HIGH ENERGY PHYSICS"
.SH NAME
wireup - sds lattice magnet ganging
.SH SYNOPSIS
.B wireup
.I sds_file
[
.B source_db
]
.SH DESCRIPTION
.I wireup
attempts to determine which bus each element in the
.I sds_file
is on, and its sequence within that bus by using the bus definitions from the
.I source_db.
A wiring sds file is produced which contains an object with a record for each
element in the lattice and and object with a record for each bus in the lattice.
The structure of the element and bus records is described below.
The file is named `<root beam>_wiring.sds'.
.SH OPTIONS
.TP
.B sds_file
is the file that the flat lattice is stored in.  The root name of the beamline
to resolve is the name of the first element in the element array of this lattice
object.
.TP
.B source_db
is the database where the definition of the bus structure is located.  This name
is looked for first on the command line, then in the environment variable
DBSF_DB, and finally as a component of a lattice sds standard file name.
The standard name convention for lattice sds' is `<source_db>.<root beam>'.
If the dot is found AND the root beam name in the lattice sds matches the last
half of the file name, the first half is treated as the source_db.  Otherwise
an error occurs.
.SH DATABASE FORMAT
.LP
This section is included as a tutorial to adding ganging information to the
lattice database.  Ganging information includes both the description of a
certain type of bus ( referred to as a `family' ) and the demarcation of where
instances of that family ( buses ) occur within the element lattice.
.LP
Family descriptions are located in the
.I bus
table of the database.  The format
of the descriptions looks similar to that of beam lines, however the meaning
is different.  The sequential order of elements in a family begins with
elements in the `clockwise' or `right' direction from the master power supply
for that family.  At some point the family ends and elements are wired into the
family in the `counter-clockwise' or `left' direction.  This continues back
past the master power supply to a second turnaround.  From there the wiring
direction reverses back to the master_ps.
So a stretch of beam line that looks like:
.sp
d d d d d qf d d d d d qd d d d d d qf d d d d d qd
.sp
looks to a bus designer like so:
.sp
.nf
turnaround             master_ps              turnaround
.br
|  -  -  -         -  -  -   - - -            - - -    |
.br
| 19 20 21        22 23 24 V 1 2 3            4 5 6    |
.br
|->d d d    ---->   d d qd   d d d   ---->    d d qd ->|
.br
|   <-  d d qf d d d       <--    d d qf d d d    <-   |
.br
|    18 17 16 15 14 13          12 11 10 9 8 7         |
.br
|     +  +  +  +  +  +           +  +  + + + +         |
.fi
.sp
The elements on the `top' of the above diagram are considered to
be `-' magnets while the ones on the bottom are considered to be `+'
magnets.  This refers to the polarity that the magnets are wired in.
In the database we describe the family from its left turnaround point.
The `level' field in the database holds the information indicating which
direction a beam should be matched in.  So a description of the  above family
would look like:
.sp
.sp
	name	level	elements
.br
	----	-----	--------
.br
	3dr	  r   	d d d
.br
	cl	  l  	d d qf d d d
.br
	hcr	  r  	d d qd
.br
	circ	       	3dr cl hcr 3dr cl hcr
.sp
`Level' can be left blank to indicate a generic structure.  So alternately:
.sp
	name	level	elements
.br
	----	-----	--------
.br
	3d	       	d d d
.br
	cl	  l  	d d qf 3d
.br
	hcd	       	d d qd
.br
	circ	  r  	2*(3d cl hcd)
.sp
( the 2* repetition factor could have been used in the previous example also ).
.LP
A couple of considerations about levels:
.HP
- under certain circumstances it might be desirable to define a bus in which
the matching of elements begins in the `left' or `counter-clockwise'
direction.  In this case the top level definition must contain an `x' in the
.I level
field of the bus definition.  i.e. making `circ' (above) counter-clockwise
would involve adding the additional definition:
.sp
	xcirc	  x  	circ
.sp
The `c' directive may also be used to indicate `clockwise', but this is the
default anyway.
.HP
- in the case of a `clockwise' (default) bus, the elements matched in the
`left' direction are considered to be `-' magnets while `right' magnets are `+'.
The opposite is true for `counter-clockwise' circuits.
.HP
- sometimes an element at the end of a bus isn't considered to be matched in
the left or right direction.  A `0' (zero) can be placed in the level field
for this element (or elements) to cause the output sds direction indicator to
be `0' instead of `+' or `-'.  The elements will still be assigned sequence
numbers as if they were matched in the `left' direction.
.HP
- there must be some level active at all times ( in other words, there must
be no element such that there is no level directive in any of the bus beams
which represent its parents )
.HP
- the level directive does not override the level directive of its children, but
the other way around; the `level' of an element is the first level that it
finds as it looks up at its parents ( its closest parent with a level )
.LP
And some tips:
.HP
- the direction of bus components can be changed locally to `counter-clockwise'
or `clockwise' using the `x' and `c' level directives respectively.  This can
be used to get elements which are assigned sequence numbers based on their
direction but end up having the opposite polarity (`+',`-').  ( this may never
be desirable so be careful! )
.HP
- beams from the beam line table may be inserted in the elements field as well
as the name of elements ( this can make for some very terse definitions if
wiring is sequential and all inclusive )
.HP
- the master power supply can be placed right after the first or right before
the last turnaround for sequential `clockwise' or `counter-clockwise' families.
.HP
- the master power supply can be placed in the bus definition in addition to
( but not instead of in ) the lattice.  In this way it is used as a conceptual
place holder so one does not forget where this entry point is in the bus
definition.
.LP
Instances of families or `buses' are inserted in the lattice by placing special
markers in the beam definitions and the
.I support_points
table.
The two reference points required by the wireup algorithm are the locations of
the left turnaround and the master power supply.  The placement of the
turnaround need not be exact if there is only one bus of its family type in the
lattice.  In this case it can just be placed at the beginning of the lattice and
matching will begin when the first element in the bus family type is
encountered, however if that first element exists as part of another beam in the
lattice somewhere before the element that marks the true beginning of the
family, then placement is more crucial.  The best way to place both markers is
within the smallest specific beamline that contains all of the elements of that
family.  So, if a beam line is defined:
.sp
	name  	elements
.br
	----  	--------
.br
	corner	d qf d qd d qf d qd
.sp
with arbitrary junk between each of the `interesting' elements, then
add names to the beginning and anywhere in
between the elements that the master power supply feed enters the family.
For example:
.sp
	name  	elements
.br
	----  	--------
.br
	corner	ct d qf d qd cps d qf d qd
.sp
Next, an entry for each name must be added to the
.I support_points
table.  The fields in the support_points table should be filled in as follows:
.sp
	field         	value
.br
	-----         	-----
.br
	name        	the same name as in the beam
.br
	type        	`turnaround' OR `master_ps'
.br
	system_name	the name of the family this is for
.br
	displacement	NULL or 0
.br
	comment		up to you
.sp
Now every time this beam is used, it will automatically insert the correct
markers into the lattice for wireup to use.
.LP
Some tips:
.HP
- there may be situations when multiple support points are at the same
place in the lattice.  In this case you can just add more rows to the
.I support_points
table with the same marker name.  Every time the marker is found in
the lattice ALL of the support points with the same name will be activated.
.HP
- masterps support points can be placed in the bus definition as long as
the support point name is NOT also the name of a marker in the lattice.
Just make up a name for the power supply, put it in the bus definition and add
a row to the
.I support_points
table with the same name.  This usually makes placing the masterps easier.
.HP
- the diagnostic which reports extra power supplies in the lattice can be
accidentally activated by having a support point name which appears in both
the bus and lattice definitions.  The program will still function correctly
in most circumstances, but it is advisable to have unique names for support
points that appear as part of bus definitions.
.sp
.SH SDS FORMAT
.LP
The sds file produced by
.I wireup
contains two objects.  Each object is an array of records.  The records in the
first array correspond to the elements in the lattice array of the sds
provided as input to
.I wireup .
Each record has the following fields:
.sp
	field name	type		description
.br
	------------	----		-------------
.br
	bus_sequence	int		indicates the wiring order
.br
		       			for this element in it's bus
.br
	bus_index     	int		indicates this element's bus
.br
		      			(index to record in bus array)
.br
	bus_dir		byte		acsii code for '+', '0' or '-'
.br
		       			indicating wiring direction
.sp
The second array has one record for each bus family declared in the lattice.
The records have the following format:
.sp
	field name	type		description
.br
	------------	----		-------------
.br
	family_name	char		the name of the family as
.br
		       			identified in the database
.SH FILES
.TP
.I support.h
C language header file for structures used internally to wireup
.TP
.I sds_wireup_structs.h
C language header which describes the format of the sds structures which
are in the sds file produced by wireup
.TP
.I main_wireup.c
all of the code for the wireup program
.SH SEE ALSO
.LP
dbsf, sid, latview, teslis, glish - SDS and lattice manipulation programs
.LP
To get sid to make the buses array follow the ganging_lattice array add
these two lines to your .sidopt:
.br
	buses ganging_lattice.bus_index 1
.br
	ganging_lattice junk 0
.SH AUTHORS
.B wireup
and db query routines by Eric Barr ( SSCL )
.br
Conceptual design help by Ellen Syphers ( SSCL ) and Garry Trahern ( SSCL )
.br
SDS libraries by Chris Saltmarsh ( LBL and SSCL )
.br
Database design by Eric and Garry
.sp
SSCL - Superconducting Super Collider Lab
.br
LBL - Lawrence Berkeley Labs
.sp
Send comments, bugs, complaints, suggestions, gripes, wishes, money, praise, cookies, beer to:
.br
	barr@fremont.ssc.gov
.br
.SH DIAGNOSTICS
.LP
.I malloc error in ... -
malloc failed to retrieve a block of free memory for the named routine
( need to make more virtual memory available )
.LP
.I nddq_remove: directive queue underflow -
there was some error creating the structure describing the family resulting
in an error during matching ( internal bug please report )
.LP
.I bad beam op ... -
an unknown beam operation was found in the lattice indicating that this
version of wireup is outdated ( get another release from above address )
.LP
.I family ... doesn't exist -
the system_name field of a support point indicates a family which is not in
the bus table ( put it there, nix the support point or ignore )
.LP
.I direction stack empty for ... -
there is no level visible for some element in the family definition
( fix the levels in the bus table, see above for help )
.LP
.I no support_points table
.I info for ... -
a support point was found in the sds but not in the database ( figure out
which is busted and fix it, see above for help )
.LP
.I warning: power supply ... for
.I ... has no turnaround -
( put one in or ignore it )
.LP
.I unknown power support point type: ... -
( take it out of the support_points table )
.LP
.I families active at end of
.I matching: ... -
these families had too many elements specified for them ( the definition of the
bus family should be changed )
.LP
.I incorrect sds file type: must
.I be a lattice sds -
the sds file specified doesn't have a lattice in it
.LP
.I corrupt lattice sds file -
( try regenerating the file )
.LP
.I sds file name non-standard
.I ( not <db_name>.<root beam> ) -
this only happens if there is no database name on the command line, the 
environment variable DBSF_DB is not set AND wireup could not get the
database name from the sds file name because it was malformed ( rename the
file correctly or supply a db name )
.LP
.I no valid database name could
.I be found -
( same as previous )
.SH BUGS
.LP
Bugs?  Ha.  What?  Oh.  :-(  Send a description to me at barr@fremont.ssc.gov
.sp
Now for some possible enhancements:
.LP
- actually add the new wireup sds to the lattice sds
.br
- make '0' directive match in both directions
.br
- make a specific diagnostic to indicate a masterps marker is in both the bus
and lattice definitions
.br
- add a version command line option (-v).
.SH COPYRIGHT
.LP
Copyright (c) 1991 Universities Research Association, Inc. All Rights
Reserved. DISCLAIMER The software is licensed on an `as is' basis only.
Universities Research Association, Inc. (URA) makes no representations or
warranties, express or implied.  By way of example but not of limitation, URA
makes no representations or WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY
PARTICULAR PURPOSE, that the use of the licensed software will not infringe any
patent, copyright, or trademark, or as to the use (or the results of the use) of
the licensed software or written material in terms of correctness, accuracy,
reliability, currentness or otherwise.  The entire risk as to the results and
performance of the  licensed software is assumed by the user.  Universities
Research Association, Inc. and any of its trustees, overseers, directors,
officers, employees, agents or contractors shall not be liable under any claim,
charge, or demand, whether in contract, tort, criminal law, or otherwise, for
any and all loss, cost, charge, claim, demand, fee, expense, or damage of every
nature and kind arising out of, connected with, resulting from or sustained as
a result of using this software.  In no event shall URA be liable for special,
direct, indirect or consequential damages, losses, costs, charges, claims,
demands, fees or expenses of any  nature or kind.  This material resulted from
work developed under a Government Contract and is subject to the following
license:  The Government retains a paid-up, nonexclusive, irrevocable worldwide
license to reproduce, prepare derivative works, perform publicly and display
publicly by or for the Government, including the right to distribute to
other Government contractors.  Neither the United States nor the United States
Department of Energy, nor any of their employees, makes any warranty, express
or implied, or assumes any legal liability or responsibility for the accuracy,
completeness, or usefulness of any information, apparatus, product, or process
disclosed, or represents that its use would not infringe privately owned rights.

/* lattice wiring utility - takes a database and an sds and creates a */
/* circuit ordered sds with bus names on each element */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Sds/sdsgen.h"

#include "sds_defines.h"
#include "sds_internal_structs.h"

#include "../lattice_tree/lattice_defines.h"
#include "../lattice_tree/lattice_tree.h"
#include "../lattice_tree/lattice_gdecls.h"
#include "../dbsf/dbsf_stack.h"
#include "../dbsf/dbsf_gdecls.h"

#include "sds_wireup_structs.h"
#include "support.h"
#include "../dbsf/dbquery.h"

#define ESIZE (sizeof(struct element))

int mad_flag = 0;
int sds_file = 0;
stacknode *stack;

/* push a letter onto a direction stack */
void pushd( s, c )
direction_stack_node **s;
char c;
{
    direction_stack_node *temp;

    if ( ( temp = (direction_stack_node *)malloc(
	sizeof( direction_stack_node ) )) == (direction_stack_node *)NULL )
    {
	fprintf( stderr, "**wireup: malloc error in pushd\n" );
	exit( 1 );
    }

    temp->direction = c;
    temp->next = *s;
    *s = temp;
}

/* pop a letter from the direction_stack */
void popd( s )
direction_stack_node **s;
{
    direction_stack_node *temp;

    temp = (*s)->next;
    free( (char *)*s );
    *s = temp;
}

/* this routine empties a families queue and assigns sequence numbers to the */
/* elements that it contained */
void empty_queue( fam, bind_array )
family_context_block *fam;
struct ganging_lattice *bind_array;
{
    family_element_queue_node *temp;

    /* this makes the queue a simple list */
    if ( fam->elemq )
    {
	temp = fam->elemq->next;
	fam->elemq->next = NULL;
	fam->elemq = temp;
    }

    /* assign sequence numbers to elems in elem queue and free queue nodes */
    while ( fam->elemq )
    {
	bind_array[fam->elemq->elem_index].bus_sequence = fam->sequence_counter;
	bind_array[fam->elemq->elem_index].bus_dir = fam->elemq->dir_char;
	fam->sequence_counter += 1;
	bind_array[fam->elemq->elem_index].bus_index = fam->family_number;
	temp = fam->elemq->next;
	free( (char *)fam->elemq );
	fam->elemq = temp;
    }
}

/* close a family by sequencing its element queue and taking it out of the */
/* active list */
void close_family( fam, bind_array )
family_context_block *fam;
struct ganging_lattice *bind_array;
{
    empty_queue( fam, bind_array ); /* sequences elements in the queue */

    /* check if the family is being closed before a power supply was reached */
    if ( fam->matching_state == 0 )
	fprintf( stderr,
	  "**wireup: warning: family '%s' finished before power supply found\n",
	  fam->name );

    /* unlink from active list */
    fam->active_forw->active_back = fam->active_back;
    fam->active_back->active_forw = fam->active_forw;
    fam->active_back = NULL;
    fam->active_forw = NULL;
}

/* this adds a name or directive to the name nddq */
void nddq_add( q, n, c )
nddq_node **q;
char *n;
char c;
{
    nddq_node *temp;

    if ( (temp = (nddq_node *)malloc( sizeof( nddq_node ) )) == NULL )
    {
	fprintf( stderr, "**wireup: malloc error in nddq_add\n" );
	exit( 1 );
    }

    temp->directive = c;
    temp->name = n;

    if ( *q == NULL )
    {
	temp->next = temp;
    } else {
	temp->next = (*q)->next;
	(*q)->next = temp;
    }

    *q = temp;
}

/* take an entry off of the nddq */
void nddq_remove( q )
nddq_node **q;
{
    nddq_node *temp;

    if ( *q == NULL )
    {
	fprintf( stderr, "**wireup: nddq_remove: directive queue underflow\n" );
	exit( 1 );
    } else {
	if ( *q == (*q)->next )
	{
	    free( (char *)*q );
	    *q = (nddq_node *)NULL;
	} else {
	    temp = (*q)->next->next;
	    free( (char *)(*q)->next );
	    (*q)->next = temp;
	}
    }
}

/* this routine pops directives off of the family context block nddq until */
/* a name is reached or the nddq empties out */
void pop_directives( al, ba )
family_context_block *al;
struct ganging_lattice *ba;
{
    while ( al->nddq && al->nddq->next->directive != '.' )
    {
	switch ( al->nddq->next->directive )
	{
	case 'r':
	case 'l':
	case '0':
	    pushd( &(al->direction_stack), al->nddq->next->directive );
	    nddq_remove( &(al->nddq) );
	    break;

	case 'c':
	case 'x':
	    pushd( &(al->clock_stack), al->nddq->next->directive );
	    nddq_remove( &(al->nddq) );
	    break;

	case 'm':
	    if ( al->matching_state == 1 )
	    {
		fprintf( stderr, "*** warning: built in masterps %s ignored\n",
		  al->nddq->next->name );
		fprintf( stderr, "    for system %s, extra masterps exists\n",
		  al->name );
	    } else {
		al->matching_state = 1;

		/* for counter clockwise circuits, empty out element queue */
		if ( al->master_dir == 0 )
		    empty_queue( al, ba );
	    }

	    nddq_remove( &(al->nddq) );
	    break;
	    
	case 'p':
	    popd( &(al->direction_stack) );
	    nddq_remove( &(al->nddq) );
	    break;
	
	case 'o':
	    popd( &(al->clock_stack) );
	    nddq_remove( &(al->nddq) );
	    break;
	
	case '.': /* this means we have reached a name */
	default:
	    break;
	}
    }
}

/* this determines which family if any is interested in the given element */
/* and advances its directive pointer accordingly */
/* returns null pointer if noone is interested */
/* enhancement - if we can get more than one family instantiation to use */
/* the nddq we should do no freeing, just pointer advancement */
family_context_block *which_family( n, al, bind_array )
char *n;
family_context_block *al;
struct ganging_lattice *bind_array;
{
    family_context_block *temp;

    al = al->active_forw;

    while ( *al->name != '\0' )
    {
	pop_directives( al, bind_array );

	if ( al->nddq )
	{

	    if ( strcmp( al->nddq->next->name, n ) == 0 )
	    {
		nddq_remove( &(al->nddq) );
		return( al );
	    } else {
		al = al->active_forw;
	    }

	} else {
	    temp = al->active_forw;
	    close_family( al, bind_array );
	    al = temp;
	}
    }

    return( NULL );
}

/* put an elem on the elem queue head or tail depending on the flag */
void queue_elem( num, q, tail, dir )
int num;
family_element_queue_node **q;
int tail; /* true if the item is to go on the tail of the queue */
char dir;
{
    family_element_queue_node *temp;

    if ( ( temp = (family_element_queue_node *)malloc( sizeof(
	family_element_queue_node ) )) == (family_element_queue_node *)NULL )
    {
	fprintf( stderr, "**wireup: malloc error in queue_elem\n" );
	exit( 1 );
    }

    temp->elem_index = num;
    temp->dir_char = dir;

    if ( *q == NULL )
    {
	temp->next = temp;
	*q = temp;
    } else {
	temp->next = (*q)->next;
	(*q)->next = temp;

	if ( tail )
	    *q = temp;
    }
}

static void unroll_circuit();

/* decides what to put on the queues and which unroller to call */
void circuit_pusher( name, q, beam, forwards )
char *name;
nddq_node **q;
beamnode *beam;
int forwards;
{
    symbol_node *sym_data;
    support_node *sp;
    int first = 0;
    char *ind;

    if ( forwards == -1 )
    {
	first = 1;
	forwards = 1;
    }

    if ( beam->beam_operand )
    {
	unroll_circuit( name, beam->beam_operand, q, forwards );
    } else {
	sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

	if ( sym_data->label_type == BEAMLINE || sym_data->label_type == SLOT
	    /* SSS || sym_data->label_type == SUPERSLOT */ )
	{
	    ind = sym_data->sub_type;
	    
	    if ( *ind != 'c' && *ind != 'x' && first )
	    {
		nddq_add( q, (char *)NULL, 'c' );
	    }

	    if( (*ind=='0' || *ind=='r' || *ind=='l' || *ind=='c' || *ind=='x')
	      &&ind[1]=='\0' )
	    {	
		nddq_add( q, (char *)NULL, *ind );
		unroll_circuit( name, sym_data->beam_root, q, forwards );
		nddq_add( q, (char *)NULL, ((*ind=='c'||*ind=='x')?'o':'p') );
	    } else
		unroll_circuit( name, sym_data->beam_root, q, forwards );

	    if ( *ind != 'c' && *ind != 'x' && first )
	    {
		nddq_add( q, (char *)NULL, 'o' );
	    }

	} else
	    if ( *(beam->elem_operand->name) != '\0' )
		/* this is where we check markers to see if they are also */
		/* support points so we can add the correct directives */
		if ( sym_data->label_type == ATTR_MARKER &&
		  (sp = get_support_points( beam->elem_operand->name, name,
		  "masterps" )) != (support_node *)NULL )
		    nddq_add( q, beam->elem_operand->name, 'm' );
		else
		    nddq_add( q, beam->elem_operand->name, '.' );
    }
}

/* this is the algorithm which creates nddl lists for given families */
static void unroll_circuit( name, beam, q, forwards )
char *name;
beamnode *beam;
nddq_node **q;
int forwards;
{
    register double repeat;

    if ( beam )
    {
	if ( !forwards )
	    unroll_circuit( name, beam->next, q, 0 );

        switch (beam->operation)
	{
	case BEAM_NO_OP:
	    circuit_pusher( name, q, beam, forwards );
	    break;

	case BEAM_REPEAT:
	    for ( repeat=eval_expr( beam->iteration ); repeat > 0; repeat -= 1 )
		circuit_pusher( name, q, beam, forwards );
	    break;

	case BEAM_TRANSPOSE:
	    circuit_pusher( name, q, beam, ( forwards == 1 ? 0 : 1 ) );
	    break;

	default:
	    fprintf( stderr, "**wireup: bad beam op %d\n", beam->operation );
	    exit( 1 );
	}

	if ( forwards )
	    unroll_circuit( name, beam->next, q, 1 );
    }
}

symbol circ_tree( name )
char *name;
    {
    symbol root;
    symbol new_node;

    root = get_symbol_node( name, dbsf_st );

    push( root );

    /* resolve symbol nodes from the stack as long as the supply from */
    /* db_resolve lasts */
    while ( (new_node = pop()) )
	db_resolve( new_node ); /* this will build the circuit tree */

    return ( root );
    }

/* starts a new family with name supplied and adds it to the family list */
/* and active list */
void new_family( name, fl, al )
char *name;
family_context_block **fl;
family_context_block *al;
{
    family_context_block *temp;
    beamnode fbeam;

    if ( (temp = (family_context_block *)malloc( sizeof(family_context_block)))
	== (family_context_block *)NULL )
    {
	fprintf( stderr, "**wireup: malloc error in new_family\n" );
	exit( 1 );
    }

    if ( is_new_name( name, dbsf_st ) )
    {
	fbeam.elem_operand = circ_tree( name );

	if ( fbeam.elem_operand == (symbol)NULL )
	{
	    fprintf( stderr, "**wireup: family '%s' doesn't exist\n", name );
	    exit( 1 );
	}
    } else {
	fbeam.elem_operand = get_symbol_node( name, dbsf_st );
    }

    /* initialize the structure fields */
    temp->active_forw = al ;
    temp->active_back = al->active_back;
    temp->list_next = (*fl)->list_next;
    temp->name = name;
    temp->family_number = (*fl)->family_number + 1;
    temp->elemq = NULL;
    temp->sequence_counter = 0;
    temp->direction_stack = NULL;
    temp->clock_stack = NULL;
    temp->matching_state = 0;

    fbeam.operation = BEAM_NO_OP;
    fbeam.next = (beamnode *)NULL;
    fbeam.beam_operand = (beamnode *)NULL;
    fbeam.beam_params = (beam_param_node *)NULL;
    temp->nddq = NULL;
    unroll_circuit( name, &fbeam, &(temp->nddq), -1 );
    temp->master_dir = ('c'==temp->nddq->next->directive);

    /* if we made it this far, link it into the lists */
    al->active_back->active_forw = temp;
    al->active_back = temp;
    (*fl)->list_next = temp;
    *fl = temp;
}

/* this just feeds the names in the name string to new_family one at a time */
void new_families( name, fl, al )
char *name;
family_context_block **fl;
family_context_block *al;
{
    char *cur_end;

    while( *name != '\0' )
    {
	cur_end = strchr( name, ' ' );

	if ( cur_end != NULL )
	    *cur_end = '\0';

	new_family( name, fl, al );

	if ( cur_end != NULL )
	{
	    name = cur_end + 1;

	    while ( *name == ' ' )
		name++;
	} else 
	    return;
    }
}

/* init family list with 'unknown' family header */
void init_fl( fl )
family_context_block **fl;
{
    if ( ( *fl = (family_context_block *)malloc( sizeof(family_context_block) ))
	== (family_context_block *)NULL )
    {
	fprintf( stderr, "**wireup: malloc error in init_fl\n" );
	exit( 1 );
    }
    
    if ( ( (*fl)->name = (char *)malloc( 8 * sizeof( char ) )) == (char *)NULL )
    {
	fprintf( stderr, "**wireup: malloc error in init_fl\n" );
	exit( 1 );
    }
   
    strcpy( (*fl)->name, "unknown" );
    (*fl)->family_number = 0;
    (*fl)->sequence_counter = 0;
    (*fl)->list_next = *fl;
}

/* changes the matching state for the first added family of type name */
int change_state( name, al, bind_array )
char *name;
family_context_block *al;
struct ganging_lattice *bind_array;
{
    family_context_block *temp = al->active_forw;

    while( temp != al )
    {
	if ( strcmp( temp->name, name ) == 0 && temp->matching_state == 0 )
	{
	    temp->matching_state = 1;

	    /* for counter clockwise circuits, empty out the element queue */
	    if ( temp->master_dir == 0 )
		empty_queue( temp, bind_array );

	    return( 1 );
	}

	temp = temp->active_forw;
    }

    return( 0 );
}

/* fills in bind array and family list and returns family list */
family_context_block *sds_scanner( elem_count, elem_head, lat_head, bind_array )
int elem_count;
struct element *elem_head;
struct lattice *lat_head;
struct ganging_lattice *bind_array;
{
    int elem_index;
    int i;
    struct element *ep;
    support_node *sp;
    family_context_block *fl = NULL; /* master family list */
    family_context_block al; /* list of active families */
    family_context_block *interested;

    /* initialize active list */
    al.active_forw = &al;
    al.active_back = &al;
    al.name = (char *)malloc(sizeof(char));
    *(al.name) = '\0';

    init_fl( &fl );

    for( i=0; i < elem_count; i++ )
    {
	elem_index = lat_head->element_index;
	ep = ( (struct element *)( (char *)elem_head + ESIZE * elem_index ) );

	interested = which_family( ep->name, &al, bind_array );

	if ( interested == NULL )
	{
	    bind_array[i].bus_sequence = fl->list_next->sequence_counter;
	    bind_array[i].bus_dir = '0';
	    fl->list_next->sequence_counter += 1;
	    bind_array[i].bus_index = fl->list_next->family_number;
	} else {
	    /* look at matching state and direction indicator to decide */
	    /* what to do with the element */
	    if ( interested->direction_stack == NULL )
	    {
		fprintf( stderr, "**wireup: direction stack empty for %s\n",
		    interested->name );
		exit( 1 );
	    }

	    if ( interested->clock_stack == NULL )
	    {
		fprintf( stderr, "**wireup: clock stack empty for %s\n",
		    interested->name );
		exit( 1 );
	    }

	    if ( interested->matching_state == 0 ) /* we are left of the feed */
	    {
		if ( interested->direction_stack->direction == 'r' )
		{
		    /* put the elem on the tail of the elem queue */
		    queue_elem( i, &(interested->elemq), 1, 
		      (interested->clock_stack->direction == 'c'?'-':'+') );
		} else {
		    /* put the elem on the head of the elem queue */
		    queue_elem( i, &(interested->elemq), 0,
		      (interested->direction_stack->direction == 'l'?
		      (interested->clock_stack->direction=='c'?'+':'-'):'0') );
		}

	    } else { /* we are right of the feed */
		if ( interested->direction_stack->direction == 'r' )
		{
		    bind_array[i].bus_sequence = interested->sequence_counter;
		    bind_array[i].bus_dir =
		      (interested->clock_stack->direction == 'c'?'-':'+');
		    interested->sequence_counter += 1;
		    bind_array[i].bus_index = interested->family_number;
		} else {
		    /* put the elem on the head of the elem queue */
		    queue_elem( i, &(interested->elemq), 0,
		      (interested->direction_stack->direction == 'l'?
		      (interested->clock_stack->direction=='c'?'+':'-'):'0') );
		}
		
		pop_directives( interested, bind_array );

		if ( interested->nddq == NULL )
		    close_family( interested, bind_array );
	    }
	}

	if ( ep->type_index == ATTR_MARKER && ( sp = get_support_points(
	  ep->name, (char *)NULL, (char *)NULL )) != (support_node *)NULL )
	{
	    /* now make a list of all of the rows */
	    support_node *tmp;

	    tmp = sp;
	    sp->next = (support_node *)NULL;

	    while ( (sp = get_next_sp( ep->name )) != (support_node *)NULL )
	    {
		sp->next = tmp;
		tmp = sp;
	    }

	    sp = tmp;

	    while ( sp != (support_node *)NULL )
	    {
		if ( strcmp( "turnaround", sp->type ) == 0 )
		{
		    new_families( sp->system_name, &fl, &al );
		} else {
		    if ( strcmp( "masterps", sp->type ) == 0 )
		    {
			if ( change_state(sp->system_name, &al, bind_array)==0 )
			  fprintf( stderr,
			  "**warning: masterps %s for %s has no turnaround\n",
			  ep->name, sp->system_name );
		    } else {
			fprintf( stderr,
			  "**wireup: unknown type: '%s' %s '%s'\n",
			  sp->type, "for support point", sp->name);
		    }
		}
		
		sp = sp->next;
	    }
	}

	/* advance element pointer in source sds */
	lat_head=(struct lattice *)( (char *)lat_head+sizeof(struct lattice) );
    }

    /* if there are any active families at this point there s an error */
    if ( al.active_forw != &al )
    {
	family_context_block *temp = al.active_forw;
	
	fprintf( stderr, "**wireup: families active at end of matching:\n" );

	while( temp != &al )
	{
	    fprintf( stderr, "    %s\n", temp->name );
	    temp = temp->active_forw;
	}
    }

    return( fl );
}

/* this fills in the family array from the family list */
void fill_fa( fl, fam_array )
family_context_block *fl;
struct buses *fam_array;
{
    int i = fl->family_number;
    fl = fl->list_next;

    for( ; i >= 0; i-- )
    {
	strcpy( fam_array[fl->family_number].family_name, fl->name );
	fl = fl->list_next;
    }
}

void write_wireup_sds( binds, bus_names, elem_count, bus_count, main_name )
struct ganging_lattice *binds;
struct buses *bus_names;
int elem_count;
int bus_count;
char *main_name;
{
    int sds;
    int bind_typecode, bus_typecode;
    char out_sds_name[100];

    sprintf( out_sds_name, "%s_wiring.sds", main_name );

    sds_init();
    sds = sds_new( out_sds_name );

    bind_typecode = sds_define_structure( sds, ganging_lattice_tl,
	"bus_sequence,bus_index,bus_dir" );
    bus_typecode = sds_define_structure( sds, buses_tl, "family_name" );

    sds_declare_structure( sds,(char *)binds,"ganging_lattice",elem_count,bind_typecode );
    sds_declare_structure( sds, (char *)bus_names, "buses", bus_count, bus_typecode );

    sds_ass( sds, out_sds_name, SDS_FILE );
}

void do_wireup( elem_count, elem_head, lat_head, main_name )
int elem_count;
struct element *elem_head;
struct lattice *lat_head;
char *main_name;
{
    int family_count = 0;
    struct ganging_lattice *bind_array;
    struct buses *fam_array;
    family_context_block *fam_list;

    /* allocate element binding array */
    if ( (bind_array = (struct ganging_lattice *)malloc( (unsigned)elem_count *
	sizeof(struct ganging_lattice) )) == (struct ganging_lattice *)NULL )
    {
	fprintf( stderr, "**wireup: malloc error getting bind array\n" );
	exit( 1 );
    }

    /* code to fill in bind array and determine # of families */
    fam_list = sds_scanner( elem_count, elem_head, lat_head, bind_array );
    family_count = fam_list->family_number + 1; /* cuz #0 is 'unknown' family */

    if ( (fam_array = (struct buses *)malloc( (unsigned)family_count *
	sizeof(struct buses) )) == (struct buses *)NULL )
    {
	fprintf( stderr, "**wireup: malloc error getting fam_array\n" );
	exit( 1 );
    }

    /* code to fill in family array from family list */
    fill_fa( fam_list, fam_array );

    write_wireup_sds( bind_array,fam_array,elem_count,family_count,main_name );
}

void p_usage()
{
    fprintf( stderr, "usage: wireup sds_file [source_db]\n" );
    fprintf( stderr, "sds_file	name of source lattice sds file\n" );
    fprintf( stderr,
    "source_db	db the sds was derived from ( DBSF_DB env variable is\n" );
    fprintf( stderr, "		examined then sds file name component )\n" );
    exit( 2 );
}

int main( argc, argv, envp )
int argc;
char *argv[];
char *envp[];
{
    char *getenv();
    char *db_name = getenv( "DBSF_DB" ); /* this only works if DBSF_DB is set */

    struct element *elem_head;
    struct lattice *lat_head;
    int ind, sds;
    int lat_elems;
    char *sds_source_file;

    symbol parse_tree;
    int errflag = 0;
    char key_name[MAXSTRING];	/* the name to be defined */

    /* creates empty symbol table */
    dbsf_st = sym_table_create( HASH_TABLE_SIZE, NO_SYM_VAL );

    /* set default for strength table  */
    strcpy( str_tbl_used , "strength" );

    /* get command line args */
    /* must be at least sds_file name and key, db name is optional */
    if ( argc > 1 )
	{
	sds_source_file = argv[1];

	if ( argc == 3 )
	    {
	    db_name = argv[2]; /* next check the command line for db */
	    
	    if ( argc > 3 )
	       fprintf( stderr,
	         "**wireup: options after database name ignored\n" );
	    }
	}
    else
	{
	fprintf( stderr, "**wireup: mandatory options missing\n" );
	++errflag;
	}

    if ( errflag )
	p_usage();

    sds_init();
    sds = sds_access( sds_source_file, SDS_FILE, SDS_READ );

    elem_head = (struct element *)sds_obname2ptr( sds, "element" );
    lat_head = (struct lattice *)sds_obname2ptr( sds, "lattice" );

    if ( !elem_head || !lat_head )
    {
	fprintf( stderr,
	  "**wireup: incorrect sds file type: must be a lattice sds\n" );
	p_usage();
    }

    ind = sds_name2ind( sds, "element" );
    printf( "elements: %ld\n", sds_array_size( sds, ind ) );

    if ( ind < 0 )
    {
	fprintf( stderr, "**wireup: corrupt lattice sds file\n" );
	exit( 1 );
    }

    ind = sds_name2ind( sds, "lattice" );
    printf( "instances: %d\n", (lat_elems = sds_array_size( sds, ind )) );

    if ( ind < 0 )
    {
	fprintf( stderr, "**wireup: corrupt lattice sds file\n" );
	exit( 1 );
    }

    strcpy( key_name, elem_head->name ); /* this should be the root beamline */
    printf( "root beam: '%s'\n", key_name );

    if ( !db_name ) /* get db name from sds file name */
    {
	/* the db name shuld be everything up to the '.' in the file name */

	if ( (db_name = strchr( sds_source_file, '.' )) != (char *)NULL )
	{
	    *db_name = '\0'; /* end the file name at the '.' */
	    db_name++;

	    /* make sure the last half is the name of the root */
	    if ( strcmp( db_name, key_name ) == 0 )
		db_name = sds_source_file; /* set the db name to first half */
	    else
		db_name = (char *)NULL; /* they didn't match */
	}
	/* no dot means the filename is not standard so quit */
    }

    if ( !db_name )
    {
	fprintf( stderr,
	"**wireup: sds file name non-standard ( not <db_name>.<root beam> )\n");
	fprintf( stderr, "**wireup: no valid database name could be found\n" );
	p_usage();
    }

    set_db_name( db_name ); /* give the db routines whatever we have now */
    printf( "database: '%s'\n", db_name );
    open_db(); /* initialize database query routines */
    parse_tree = circ_tree( key_name ); /* get parse tree */
    do_wireup( lat_elems, elem_head, lat_head, parse_tree->name );
    close_db(); /* close database query routines */

    return( 0 );
}

/* end-o-file */

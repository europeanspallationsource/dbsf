/* this describes the wired sds struct  */

/* C forms */

struct ganging_lattice
{
    int bus_sequence;
    int bus_index;
    char bus_dir;
};

struct buses
{
    char family_name[20];
};

/* SDS forms */

struct type_list ganging_lattice_tl[] =
{
    { (long)1,SDS_INT },
    { (long)1,SDS_INT },
    { (long)1,SDS_FSTRING },
    { (long)0,SDS_RETLIST },
    { (long)0,SDS_ENDLIST }
};

struct type_list buses_tl[] =
{
    { (long)20,SDS_FSTRING },
    { (long)0,SDS_RETLIST },
    { (long)0,SDS_ENDLIST }
};

/* end-o-header */

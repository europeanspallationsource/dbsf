/* header file for wireup structures */

/* nodes for support points */
typedef struct snx
    {
    struct snx *next;
    symbol elem;
    char name[20];
    char type[20];
    char system_name[20];
    double displacement;
} support_node;

/* element queue */
typedef struct eq
    {
    struct eq *next;
    long elem_index;
    char dir_char;
} family_element_queue_node;

/* direction stack */
typedef struct ds
    {
    struct ds *next;
    char direction;
} direction_stack_node;

/* name and direction directive queue */
typedef struct nddqn
    {
    struct nddqn *next;
    char *name;
    char directive;
} nddq_node;

/* family context block */
typedef struct fcb
    {
    struct fcb *active_forw;
    struct fcb *active_back;
    struct fcb *list_next;
    char *name;
    long family_number;
    family_element_queue_node *elemq;
    long sequence_counter;
    direction_stack_node *direction_stack;
    direction_stack_node *clock_stack;
    int master_dir;
    long matching_state;
    nddq_node *nddq;
} family_context_block;

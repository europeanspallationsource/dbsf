%{

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "../lattice_tree/lattice_tree.h"
#include "dbsf_gdecls.h"
#include "y.tab.h"

#define RET_TOK(tok) {/*fprintf(stderr,"tok %s\n",yytext);*/ return ( tok ); }
#define RET_SYM(ste) { yylval.st_entry = ste; RET_TOK( TOK_ID ) ; }
#define RET_NUM(number) { yylval.value = number; RET_TOK( TOK_NUM ); }

#undef YY_INPUT
#define YY_INPUT(buf,result,max_size) result = load_input( buf );

int load_input();

%}

ID		[A-Za-z][A-Za-z0-9_]*

D		[0-9]
EXP		e[-+]?{D}+
NUM1		{D}+(\.{D}*)?
NUM2		{D}*\.{D}+
NUM		({NUM1}|{NUM2})({EXP}?)

WS		[ \t]*

OPERATORS	[-+/*(),]

%%
^beam		RET_TOK( TOK_BEAM_START )

^expr		RET_TOK( TOK_EXPR_START )

{ID}		RET_SYM( get_symbol_node( yytext, dbsf_st ) )

{NUM}		RET_NUM( atof( yytext ) )

{OPERATORS}	RET_TOK( yytext[0] )

\\		RET_TOK( yytext[0] )

{WS}

\n		RET_TOK( TOK_EOL )

.		{
		fprintf( stderr,  "***DBSFlex: Warning: Bad char '%c', ascii value %d, ignored\n", yytext[0], yytext[0] );
		inc_errors();
		}

%%

int load_input( buffer )
char *buffer;
    {
    strcpy( buffer, in_buffer );
    return( strlen( in_buffer ) );
    }

/* END OF FILE */

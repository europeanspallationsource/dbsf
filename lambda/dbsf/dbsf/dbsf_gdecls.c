
/* $Header: /scratch/trahern/dbsf/dbsf/RCS/dbsf_gdecls.c,v 1.1 1994/06/07 19:30:02 trahern Exp $ */

#include <stdio.h>
#include <stdlib.h>
#include "../lattice_tree/lattice_defines.h"
#include "../lattice_tree/lattice_tree.h"
#include "dbsf_gdecls.h"

int num_errors = 0;		/* total number of scanner and parser errors */
char in_buffer[MAXSTRING];	/* YYINPUT source */
symbol_table dbsf_st;		/* the dbsf symbol table */

exprnode *expr_hook;		/* hooks for yyparse results */
beamnode *beam_hook;

/* prints <str> to stderr with a line number, and calls inc_errors */
void report_error( msg, name )
char *msg, *name;
    {
    fprintf( stderr, "***%s error caused by %s\n", msg, name );
    inc_errors();
    }

/* increments num_errors and aborts if there have been too many errors */
void inc_errors()
    {
    if ( ++num_errors > 15 )
	{
	fprintf( stderr, "\nParsing aborted due to excessive errors\n" );
	exit( 1 );
	}
    }

/* END OF FILE */

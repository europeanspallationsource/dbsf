
/* $Header: /scratch/trahern/dbsf/dbsf/RCS/dbsf_do_views.c,v 1.1 1994/06/07 19:30:02 trahern Exp $ */

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "../lattice_tree/lattice_gdecls.h"
#include "../lattice_tree/lattice_defines.h"
#include "../lattice_tree/lattice_tree.h"
#include "dbsf_gdecls.h"
#include "../wireup/support.h"
#include "dbquery.h"

void do_rules();
static void traverse_call_func();

static void traverse_beam( root, view_string, factor )
beamnode *root;
char *view_string;
int factor;
{
    int new_factor;

    if ( root )
    {
	if ( root->operation != BEAM_ARGUMENT )
	{
	    if ( root->operation == BEAM_CALL_FUNC )
	    {
		traverse_call_func( root->beam_params, view_string, factor );
		traverse_beam( ((symbol_node *)(root->elem_operand->symbol_data))->beam_root, view_string, factor );
	    } else {
		if ( root->operation == BEAM_REPEAT )
		{
		    new_factor = factor * eval_expr( root->iteration );
		    do_rules( root->elem_operand, view_string, new_factor );
		    traverse_beam( root->beam_operand, view_string, new_factor);
		} else {
		    do_rules( root->elem_operand, view_string, factor );
		    traverse_beam( root->beam_operand, view_string, factor );
		}
	    }
	}

	traverse_beam( root->next, view_string, factor );
    }
}

static void traverse_call_func( root, view_string, factor )
beam_param_node *root;
char *view_string;
int factor;
{
    register beam_param_node *next = root;

    while ( next )
    {
	traverse_beam( next->beam_root, view_string, factor );
	next = next->next;
    }
}

static int count_attrs( attrs )
attrnode *attrs;
{
    int count = 0;

    while ( attrs )
    {
	count++;
	attrs = attrs->next;
    }

    return( count );
}

/* return 1 if the values of all of the attributes are the same */
/* 0 otherwise.  assumes that the element types are the same */
static int compare_elems( e1, e2 )
symbol_node *e1;
symbol_node *e2;
{
    register attrnode *n1 = e1->expr_list;
    register attrnode *n2 = e2->expr_list;
    int attrtype;
    double value;

    if ( e1 == e2 )
	return( 1 );  /* they are exactly the same */

    if ( count_attrs( n1 ) != count_attrs( n2 ) )
	return( 0 );  /* not the same number of attributes */

    while ( n1 )
    {
	attrtype = n1->attr;
	value = eval_expr( n1->expr );

	while ( n2 )
	{
	    if ( n2->attr == attrtype ) 
	    {
		if ( fabs( eval_expr( n2->expr ) - value ) < TOLERANCE )
		    break;
		else
		    return( 0 ); /* attribute values don't match */
	    } else {
		if ( n2->next == (attrnode *)NULL )
		    return( 0 ); /* not the same attributes */
		else
		    n2 = n2->next;
	    }
	}

	n2 = e2->expr_list;
	n1 = n1->next;
    }

    return( 1 ); /* same #of attributes and same attribute values */
}

/* This copies all of the attributes from the second elem to the first elem. */
/* IMPORTANT: this only duplicates the attrnodes, the expr trees are RECYCLED */
static void copy_attrs( e1, e2 )
attrnode **e1;
attrnode *e2;
{
    while ( e2 )
    {
	*e1 = link_attr_node( *e1, e2->expr, e2->attr );
	e2 = e2->next;
    }
}

/* finds the length attribute and doubles it by adding some exprnodes */
/* if htere is no length it does nothing */
static void double_length( el )
attrnode *el;
{
    exprnode *two = get_expr_node( EXPR_NUM, NULL, NULL, 2.0 );

    if ( (el = find_attr( ATTR_L, el )) )
	el->expr = get_expr_node( EXPR_MUL, two, el->expr, 0.0 );
}

/* supported rule types */
#define DRIFT_RULE	1
#define QUAD_RULE	2
#define MANY_QUADS	3
#define NOMATCH_QUADS	4
#define ILLEGAL_ELEMS	5
#define VANISH_RULE	6

/* error strings that go along with above errors */
#define NOMATCH_ERR_STR	"quads must match to be combined"
#define MANY_ERR_STR	"too many quads"
#define ILLEGAL_ERR_STR	"only markers and quads allowed"

void do_rules( root, view_string, factor )
symbol root;
char *view_string;
int factor;
{
    static double length = 0.0;  /* this is where we accumulate the length */
    static int matching = 0; /* 0 => not matching,  >1 => matching rule # */
    static char *new_name;
    static symbol_node *first_quad = (symbol_node *)NULL;
    static int found_quads = 0;

    char **rule;
    symbol_node *sym_data;
    register attrnode *next;

    if ( root )
    {
        sym_data = (symbol_node *)root->symbol_data;

	switch ( sym_data->label_type )
	{
	case ATTR_MARKER:
	case ATTR_QUADRUPOLE:
	    /* ignore all of these things unless we are in the rule state */
	    if ( matching == DRIFT_RULE )
	    {
		next = find_attr( ATTR_L, sym_data->expr_list );

		if ( next )
		    length += eval_expr( next->expr ) * factor;
            } else {
		if ( matching == QUAD_RULE &&
		  sym_data->label_type == ATTR_QUADRUPOLE )
		{
		    if ( first_quad == (symbol_node *)NULL )
			first_quad = sym_data;
		    else
			if ( found_quads )
			    matching = MANY_QUADS;
			else if ( compare_elems( first_quad, sym_data ) == 0 )
			    matching = NOMATCH_QUADS;
			else
			    found_quads = 1;
		}
	    }
	    break;

	case ATTR_DRIFT:
	case ATTR_HMONITOR:
	case ATTR_VMONITOR:
	case ATTR_MONITOR:
	case ATTR_INSTRUMENT:
	case ATTR_WIGGLER:
	case ATTR_RBEND:
	case ATTR_SBEND:
	case ATTR_SEXTUPOLE:
	case ATTR_OCTUPOLE:
	case ATTR_MULTIPOLE:
	case ATTR_SOLENOID:
	case ATTR_RFCAVITY:
	case ATTR_EL_SEPARATE:
	case ATTR_SROT:
	case ATTR_YROT:
	case ATTR_HKICK:
	case ATTR_VKICK:
	case ATTR_KICKER:
	case ATTR_ECOLLIMATOR:
	case ATTR_RCOLLIMATOR:
	case ATTR_TBEND:
	case ATTR_THINLENS:
	case ATTR_TANK:
	case ATTR_EDGE:
	case ATTR_PMQ:
	case ATTR_RFQCELL:
	case ATTR_DOUBLET:
	case ATTR_TRIPLET:
	case ATTR_RFGAP:
	case ATTR_SPECIAL:
	case ATTR_ROTATION:
	    /* ignore all of these things unless we are in the rule state */
	    if ( matching == DRIFT_RULE ) /* OR switch ( matching ) { */
	    {
		next = find_attr( ATTR_L, sym_data->expr_list );

		if ( next )
		    length += eval_expr( next->expr ) * factor;
            } else
		if ( matching == QUAD_RULE )
		    matching = ILLEGAL_ELEMS;
	    break;

	case GEOMETRY:
	case STRENGTH:
	    break;

	case BEAMLINE:
	case PARAM_LINE:
	    traverse_beam( sym_data->beam_root, view_string, factor );
	    break;

	case SLOT:
	    /* this is where all of the special rule finding and */
	    /* substitution stuff goes */
	    if ( matching )
	    {
		fprintf( stderr,
		  "*** WARNING: Slot `%s' within another slot\n", root->name );
		traverse_beam( sym_data->beam_root, view_string, factor );
	    } else {
		rule = get_rule( root->name, view_string );

		if ( rule )
		{
		    if ( strcmp( rule[0], "drift" ) == 0 )
		    {
			exprnode *texpr =get_expr_node(EXPR_NUM,NULL,NULL,0.0);
			char *tmp;

			new_name = rule[1];

			if ( ( tmp = strchr( new_name, ' ' ) ) != (char *)NULL )
			    *tmp = '\0';

			length = 0.0;
			matching = DRIFT_RULE;
			traverse_beam( sym_data->beam_root, view_string, 1 );

			/* make new element */
			tmp = (char *)malloc( strlen( new_name ) );
			free( root->name );
			root->name = tmp;
			strcpy( root->name, new_name );

			sym_data->label_type = ATTR_DRIFT;
			texpr->value = length;
			texpr->evaluated = 1;
			sym_data->expr_list = link_attr_node(
			  (attrnode *)NULL, texpr, ATTR_L );

			matching = 0;
			free( (char *)rule );
		    } else {
			if ( strcmp( rule[0], "quad" ) == 0 )
			{
			    /*** EXPERIMENTAL XXX not completely tested ***/
			    char *tmp;

			    new_name = rule[1];

			    if ( (tmp = strchr( new_name, ' ' )) != (char *)NULL)
				*tmp = '\0';

			    length = 0.0;
			    first_quad = (symbol_node *)NULL;
			    found_quads = 0;
			    matching = QUAD_RULE;
			    traverse_beam( sym_data->beam_root, view_string, 1);

			    if ( matching == NOMATCH_QUADS )
			    {
				fprintf( stderr,
				  "*** Error in slot `%s': %s - rule ignored\n",
				  root->name, NOMATCH_ERR_STR );
				break;
			    }

			    if ( matching == MANY_QUADS )
			    {
				fprintf( stderr,
				  "*** Error in slot `%s': %s - rule ignored\n",
				   root->name, MANY_ERR_STR );
				break;
			    }

			    if ( matching == ILLEGAL_ELEMS )
			    {
				fprintf( stderr,
				  "*** Error in slot `%s': %s - rule ignored\n",
				  root->name, ILLEGAL_ERR_STR );
				break;
			    }

			    /* make new element */
			    tmp = (char *)malloc( strlen( new_name ) );
			    free( root->name );
			    root->name = tmp;
			    strcpy( root->name, new_name );

			    sym_data->label_type = ATTR_QUADRUPOLE;
			    copy_attrs(&(sym_data->expr_list),first_quad->expr_list);
			    double_length( sym_data->expr_list );
			    matching = 0;
			    free( (char *)rule );
			} else {
			    fprintf ( stderr,
			      "*** WARNING: unknown rule type `%s' for slot `%s' ignored\n",
			      rule[0], root->name );
			    traverse_beam(sym_data->beam_root,view_string,factor);
			}
		    }
                } else
		    traverse_beam( sym_data->beam_root, view_string, factor );
	    }
	    break;
	}
    }
}

/* END OF FILE */

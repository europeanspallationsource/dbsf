/* $Header: /scratch/trahern/dbsf/dbsf/RCS/dbsf_stack.h,v 1.1 1994/06/07 19:30:02 trahern Exp $ */

/* STACK DATA STRUCTURE HEADER */

typedef struct sn
    {
    struct sn *next;
    symbol unresolved;
    } stacknode;

/* the stack of symbol table entries with unresolved fields */
extern stacknode *stack;

/* pushes an id on the stack to be resolved if it isn't already there */
extern void dbsf_push( symbol id  );

/* pops the top entry from the stack or returns NULL if the stack is empty */
extern symbol dbsf_pop();

/* END OF FILE */

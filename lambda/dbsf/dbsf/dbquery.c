/* $Header: /rap/lambda/dbsf/dbsf/RCS/dbquery.c,v 1.3 2001/02/01 16:17:23 tepikian Exp $ */

#include <stdio.h>
#define USE_MYSQL 1
#define DEBUG 0
#ifdef USE_MYSQL
#include <mysql.h>
#else
#include <sybfront.h>
#include <sybdb.h>
#include <syberror.h>
#endif
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "../lattice_tree/lattice_defines.h"
#include "../lattice_tree/lattice_tree.h"
#include "dbsf_gdecls.h"
#include "../wireup/support.h"
#include "dbquery.h"

static int support_point( char *, char *, char * );
static int beam(char *);
static int magnet(char *);
static int ideal(char *);
static int strength(char *);
static int geometry(char *);
static int bus(char *);
static int rfcavity(char *);
extern int sds_file;

/*
static int dtl_cell(char *);
static int ncells(char *);
static int rfq_cell(char *);
static int solenoid(char *);
static int quadrupole(char *);
static int drift(char *);

All the 'new' tables get their data via the 'get_more' function
Therefore only a single template function returning '1' is needed
It replaces all of the above functions
This makes redundant a lot of the new buffers ... 
*/

static int new_table(char *);

#define OBJLEN		25
#define DEFLEN          160
#define ATTRLEN		140
#define ATTRCOUNT	60
#define COMLENGTH	160

/* array indices for the strings of interest in the database return struct */
#define BEAM_DEF		2	/* beamline, ideal magnet and bus */
#define TYPE_DEF		2	/* for magnet_piece table */
#define LEN_DEF			3	/* for magnet_piece table */
#define STR_DEF			4	/* for magnet_piece table */
#define TILT_DEF		5	/* for magnet_piece table */
#define TYPE_ATTR_DEF		6	/* for magnet_piece table */
#define APERTURE_DEF		7	/* for magnet_piece table */
#define DEF_STRENGTH		2	/* for strength table */
#define DEF_GEOMETRY		2	/* for geometry table */
#define SOURCE_TABLE		1	/* common to all return structures */
#define NAME_DEF		0	/* common to all return structures */
#define LEVEL_DEF		3	/* for bus table */
#define SYS_DEF			3	/* for support_points */
#define DISPLACEMENT_DEF	4	/* for support_points */

#define FOLD(most,least)	( 256 * most + least )

/* table names (identified by first 2 characters so far) */
#define MAGNET_PIECE_TABLE	FOLD( 'm', 'a' )
#define BEAM_LINE_TABLE		FOLD( 'b', 'e' )
#define SLOT_TABLE		FOLD( 's', 'l' )
#define STRENGTH_TABLE		FOLD( 's', 't' )
#define GEOMETRY_TABLE		FOLD( 'g', 'e' )
#define BUS_TABLE		FOLD( 'b', 'u' )
#define SUPPORT_POINT_TABLE	FOLD( 's', 'u' )

#define RFCAVITY_TABLE	        FOLD( 'r', 'f' )
#define RFQ_CELL_TABLE		FOLD( 'r', 'f' ) + 1 // This takes care of the rfcavity duplicate

#define DTL_CELL_TABLE		FOLD( 'd', 't' )
#define NCELLS_TABLE		FOLD( 'n', 'c' )

#define QUADRUPOLE_TABLE	FOLD( 'q', 'u' )
#define DRIFT_TABLE		FOLD( 'd', 'r' )
#define SOLENOID_TABLE		FOLD( 's', 'o' )

#define BEND_TABLE		FOLD( 'b', 'e' ) + 1 // Duplicate BEAM_LINE_TABLE

#define COC_TABLE		FOLD( 'c', 'l' )
#define ELSEPARATOR_TABLE	FOLD( 'e', 'l' )

char str_tbl_used[64];  /* name of strength table to use  */
static char sqlCmd[512];

char       database_name[256];
char       table_name[DEFLEN+1]; 
char       buff1[DEFLEN+1];
char       buff2[DEFLEN+1];
char       buff3[DEFLEN+1];
char       buff4[DEFLEN+1];
char       buff5[DEFLEN+1];
char       buff6[DEFLEN+1];
char       buff7[DEFLEN+1];
char       buff8[DEFLEN+1];
char       buff9[DEFLEN+1];
char       buff10[DEFLEN+1];
char       buff11[DEFLEN+1];
char       buff12[DEFLEN+1];
char       buff13[DEFLEN+1];
char       buff14[DEFLEN+1];
char       buff15[DEFLEN+1];
char       buff16[DEFLEN+1];
char       buff17[DEFLEN+1];
char       buff18[DEFLEN+1];
char       buff19[DEFLEN+1];
char       buff20[DEFLEN+1];
char       buff21[DEFLEN+1];
char       buff22[DEFLEN+1];
char       *object[OBJLEN];

char       comment[COMLENGTH];

char       attr_buff1[ATTRLEN];
char       attr_buff2[ATTRLEN];
char       attr_buff3[ATTRLEN];
char       attr_buff4[ATTRLEN];
char       attr_buff5[ATTRLEN];
char       attr_buff6[ATTRLEN];
char       attr_buff7[ATTRLEN];
char       attr_buff8[ATTRLEN];
char       attr_buff9[ATTRLEN];
char       attr_buff10[ATTRLEN];
char       attr_buff11[ATTRLEN];
char       attr_buff12[ATTRLEN];
char       attr_buff13[ATTRLEN];
char       attr_buff14[ATTRLEN];
char       attr_buff15[ATTRLEN];
char       attr_buff16[ATTRLEN];
char       attr_buff17[ATTRLEN];
char       attr_buff18[ATTRLEN];
char       attr_buff19[ATTRLEN];
char       attr_buff20[ATTRLEN];
char       attr_buff21[ATTRLEN];
char       attr_buff22[ATTRLEN];
char       attr_buff23[ATTRLEN];
char       attr_buff24[ATTRLEN];
char       attr_buff25[ATTRLEN];
char       attr_buff26[ATTRLEN];
char       attr_buff27[ATTRLEN];
char       attr_buff28[ATTRLEN];

char       *attr_array[ATTRCOUNT];

/******************************************************************************/

/* this tells us if there should be an error reported if there is no extra */
/* data in the extended tables.  this is TEMPORARY while magnet_piece still */
/* has length, strength and tilt info in it for backwards compatibility */
/* eventually, if there is an extended info table, an entry will have to be */
/* there */
int no_more_error[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
    0, 0, 1, 0, 0, 0, 0, 1, 1 };

/* these are the up-to-date arrays which provide the correct input parameters */
/* for get_more */

char *table_location[] =
    {
    "", /* "" => no more data */
    "drift",
    "monitor", /* hmon */
    "monitor", /* vmon */
    "monitor",  /* monitor */
    "monitor", /* instr */
    "", /* wiggler */
    "bend", /* rbend */
    "bend", /* sbend */
    "quadrupole",
    "sextupole",
    "octupole",
    "multipole",
    "solenoid",
    "rfcavity",
    "elseparator",
    "", /* srot */
    "", /* yrot */
    "closed_orbit_corrector", /* hkick */
    "closed_orbit_corrector", /* vkick */
    "closed_orbit_corrector", /* kicker */
    "", /* marker */
    "collimator", /* ecoll */
    "collimator", /* rcoll */
    "", /* tbend */
    "thinlens",
    "tank",
    "edge",
    "pmq",
    "rfqcell",
    "doublet",
    "triplet",
    "rfgap",
    "special",
    "rotation",
    "",
    "",
    "",
    "",
    "dtl_cell",
    "ncells",
    "rfq_cell",
    "bend",
    "closed_orbit_corrector",
    "command"
    };

char *attr_string[] =
    {
    "", /* "" => no attrs */
    "length, aperture, ysize",
    "length",
    "length",
    "length",
    "length",
    "",
    "length, angle, tilt, quad_strength, sxtp_strength, octp_strength, entrance_angle, exit_angle, field_integral, half_gap, entrance_curv, exit_curv",
    "length, angle, tilt, quad_strength, sxtp_strength, octp_strength, entrance_angle, exit_angle, field_integral, half_gap, entrance_curv, exit_curv",
    "length, strength, tilt, zero, aperture, comment",
    "length, strength, tilt",
    "length, strength, tilt",
    "K0L, K1L, K2L, K3L, K4L, K5L, K6L, K7L, K8L, K9L, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9",
    "length, strength, aperture",
    "length, voltage, phase_lag, harmonic_number, rf_coupling, rf_power, shunt_imped, fill_time, one, aperture",
    "length, efield_strength, tilt, zero, aperture",
    "",
    "",
    "length, tilt, horz_angle, vert_angle",
    "length, tilt, horz_angle, vert_angle",
    "length, tilt, horz_angle, vert_angle",
    "",
    "length, xsize, ysize",
    "length, xsize, ysize",
    "",
    "xfocal, yfocal, zfocal",
    "accel_grad, length, phase, iden_cav",
    "angle, radius, gap, fringe_factor1, fringe_factor2",
    "bfld_grad, length, inner_radius, outer_radius",
    "V_div_rsquared, AV, length, phase, type_cell",
    "bfld_grad, length, dist_between",
    "bfld_grad_outer, length_outer, dist_between, bfld_grad_inner, length_inner",
    "eff_gap_voltage, phase, eg_flag, dW_flag, harmonic",
    "length",
    "phase",
    "",
    "",
    "",
    "",
    "length, Lq1, Lq2, cellCenter, B1p, B2p, E0TL, rfphase, aperture, absolutePhase, betas, transitTime, kTsp, k2Tsp",
    "Mode, cellNumber, betag, E0T, rfphase, aperture, absolutePhase, kE0Ti, kE0To, dzi, dzo, betas, transitTime, kTsp, k2Tspp, transitTimeIn, kTip, k2Tipp, transitTimeOut, kTop, k2Topp, length",
    "meanV, vaneRadius, accelParam, modulation, length, rfPhase, cellType, transCurv, transFocus",
    "length, angle, tilt, quad_strength, sxtp_strength, octp_strength, entrance_angle, exit_angle, field_integral, half_gap, entrance_curv, exit_curv, aperture, zero",
    "zero, zero, aperture",
    "comment"
    };



/* this is the magnet size and aperture column name list for the query */
char *ap_ms_columns = "shape, xsize, ysize, centerx, centery";

int attr_count[] =
  {
     0, 3, 1, 1, 1,
     1, 0, 12, 12, 6,
     3, 3, 20, 3, 10,
     5, 0, 0, 4, 4,
     4, 0, 3, 3, 0,
     3, 4, 5, 4, 5,
     3, 5, 5, 1, 1, 
     0, 0, 0, 0, 14, 
     22, 9, 14, 3, 1
//     0, 0
};

/* these arrays provide a map for the attr_array structure */
/* returned by dbquery:more_data depending on the type of the element */

int length_only[] = { ATTR_L, ATTR_NULL };

int drift_attr[] = {ATTR_L, ATTR_APERTURE, ATTR_YSIZE, ATTR_NULL};

int rbend_attr[] = { ATTR_L, ATTR_ANGLE, ATTR_TILT, ATTR_K1, ATTR_K2, ATTR_K3,
    ATTR_E1, ATTR_E2, ATTR_FINT, ATTR_HGAP, ATTR_H1, ATTR_H2, ATTR_NULL };

int sbend_attr[] = { ATTR_L, ATTR_ANGLE, ATTR_TILT, ATTR_K1, ATTR_K2, ATTR_K3,
    ATTR_E1, ATTR_E2, ATTR_FINT, ATTR_HGAP, ATTR_H1, ATTR_H2, ATTR_NULL };

int quad_attr[] = { ATTR_L, ATTR_K1, ATTR_TILT, ATTR_ZERO, ATTR_APERTURE, ATTR_MAGNET_PIECE_TYPE, ATTR_NULL };
int sext_attr[] = { ATTR_L, ATTR_K2, ATTR_TILT, ATTR_NULL };
int octu_attr[] = { ATTR_L, ATTR_K3, ATTR_TILT, ATTR_NULL };

int multipole_attr[] = { ATTR_K0L, ATTR_K1L, ATTR_K2L, ATTR_K3L, ATTR_K4L,
    ATTR_K5L, ATTR_K6L, ATTR_K7L, ATTR_K8L, ATTR_K9L, ATTR_T0, ATTR_T1, ATTR_T2,
    ATTR_T3, ATTR_T4, ATTR_T5, ATTR_T6, ATTR_T7, ATTR_T8, ATTR_T9, ATTR_NULL };

int sole_attr[] = { ATTR_L, ATTR_KS, ATTR_APERTURE, ATTR_NULL };

int rf_attr[] = { ATTR_L, ATTR_VOLT, ATTR_LAG, ATTR_HARMON, ATTR_BETRF, ATTR_PG, ATTR_SHUNT, ATTR_TFILL, ATTR_ONE, ATTR_APERTURE, ATTR_NULL };

int elsep_attr[] = { ATTR_L, ATTR_E, ATTR_TILT, ATTR_ZERO, ATTR_APERTURE, ATTR_NULL };

int coc_attr[] = { ATTR_L, ATTR_TILT, ATTR_KICKH, ATTR_KICKV, ATTR_NULL };

int collimator_attr[] = { ATTR_L, ATTR_XSIZE, ATTR_YSIZE, ATTR_NULL };

int thinlens_attr[] = { ATTR_XFOCAL, ATTR_YFOCAL, ATTR_ZFOCAL, ATTR_NULL };

int tank_attr[] = { ATTR_ACCEL, ATTR_L, ATTR_PHASE, ATTR_IDEN_CAV, ATTR_NULL };

int edge_attr[] = { ATTR_ANGLE, ATTR_RADIUS, ATTR_GAP, ATTR_FF1, ATTR_FF2, ATTR_NULL };

int pmq_attr[] = { ATTR_K1, ATTR_L, ATTR_IN_RAD, ATTR_OUT_RAD, ATTR_NULL };

int rfqcell_attr[] = { ATTR_VR2, ATTR_AV, ATTR_L, ATTR_PHASE, ATTR_CELL, ATTR_NULL };

int doublet_attr[] = { ATTR_K1, ATTR_L, ATTR_DIST_BETW, ATTR_NULL };

int triplet_attr[] = { ATTR_BFLD_OUT, ATTR_L_OUT, ATTR_DIST_BETW, ATTR_BFLD_IN, ATTR_L_IN, ATTR_NULL };

int rfgap_attr[] = { ATTR_EGAPVOLT, ATTR_PHASE, ATTR_EGFLAG, ATTR_DWFLAG, ATTR_HARMON, ATTR_NULL };

int special_attr[] = { ATTR_L, ATTR_NULL };

int rotation_attr[] = { ATTR_ANGLE, ATTR_NULL };

int dtl_cell_attr[] = { ATTR_LENGTH, ATTR_LQ1, ATTR_LQ2, ATTR_CELLCENTER, ATTR_B1P, ATTR_B2P, ATTR_E0TL, ATTR_RFPHASE, ATTR_APERTURE, ATTR_ABSPHASE, ATTR_BETAS, ATTR_TTIME, ATTR_KTSP, ATTR_K2TSP, ATTR_NULL };

int ncells_attr[] = { ATTR_MODE, ATTR_CELLNUMBER, ATTR_BETAG, ATTR_E0T, ATTR_RFPHASE, ATTR_APERTURE, ATTR_ABSPHASE, ATTR_KE0TI, ATTR_KE0TO, ATTR_DZI, ATTR_DZO, ATTR_BETAS, ATTR_TTIME, ATTR_KTSP, ATTR_K2TSPP, ATTR_TTIMEIN, ATTR_KTIP, ATTR_K2TIPP, ATTR_TTIMEOUT, ATTR_KTOP, ATTR_K2TOPP, ATTR_LENGTH, ATTR_NULL };

int rfq_cell_attr[] = { ATTR_MEANV, ATTR_VANER, ATTR_ACCPARAM, ATTR_MODULATION, ATTR_LENGTH, ATTR_RFPHASE, ATTR_CELLTYPE, ATTR_TCURV, ATTR_TFOCUS, ATTR_NULL};

// Added aperture and dummy 0 element
int bend_attr[] = { ATTR_L, ATTR_ANGLE, ATTR_DTILT, ATTR_K1, ATTR_K2, ATTR_K3, ATTR_E1, ATTR_E2, ATTR_FINT, ATTR_HGAP, ATTR_H1, ATTR_H2, ATTR_APERTURE, ATTR_ZERO, ATTR_NULL };

int close_orbit_corrector_attr[] = { ATTR_ZERO, ATTR_ZERO, ATTR_APERTURE, ATTR_NULL };

int command_attr[] = {ATTR_COMMAND, ATTR_NULL};

/* this is a master list of the attr maps which is indexed by the ATTR_elem */
int *attr_maps[] = { (int *)NULL, drift_attr, length_only, length_only,
    length_only, length_only, (int *)NULL, rbend_attr, sbend_attr, quad_attr,
    sext_attr, octu_attr, multipole_attr, sole_attr, rf_attr, elsep_attr,
    (int *)NULL, (int *)NULL, coc_attr, coc_attr, coc_attr, (int *)NULL,
    collimator_attr, collimator_attr, (int *)NULL, thinlens_attr, tank_attr,
    edge_attr, pmq_attr, rfqcell_attr, doublet_attr, triplet_attr, rfgap_attr, 
    special_attr, rotation_attr, (int *)NULL, (int *)NULL, (int *)NULL, (int *)NULL,
    dtl_cell_attr, ncells_attr, rfq_cell_attr, bend_attr, close_orbit_corrector_attr, command_attr};

/* this is an attribute map for aperture info */
int ap_attrs[] = { ATTR_AP_SHAPE, ATTR_AP_XSIZE, ATTR_AP_YSIZE, ATTR_AP_CENTERX, ATTR_AP_CENTERY, ATTR_NULL };

/* this is an attribute map for magnet size info */
int ms_attrs[] = { ATTR_MS_SHAPE, ATTR_MS_XSIZE, ATTR_MS_YSIZE, ATTR_MS_CENTERX, ATTR_MS_CENTERY, ATTR_NULL };

/* takes a string representation of a type (i.e. quad, drift, ...) and */
/* returns the appropriate ATTR_ for that type.  Currently the first two */
/* characters are sufficient to distinguish the elements, so only the */
/* first two characters are examined (no longer true since trace3d added) */

int get_element_type( char *text )
{
    int fold = FOLD( text[0], text[1] );

    if(DEBUG) printf("getting type for: %s\n", text);

    switch ( fold ) {
	case FOLD('t','c'):  
			if (sds_file) return ( ATTR_TCELEMENT );
			else return ( ATTR_DRIFT );
	case FOLD('d','r'):  return ( ATTR_DRIFT );
	case FOLD('h','m'):  return ( ATTR_HMONITOR );
	case FOLD('v','m'):  return ( ATTR_VMONITOR );
	case FOLD('m','o'):  return ( ATTR_MONITOR );
	case FOLD('i','n'):  return ( ATTR_INSTRUMENT );
	case FOLD('w','i'):  return ( ATTR_WIGGLER );
	case FOLD('r','b'):  return ( ATTR_RBEND );
	case FOLD('s','b'):  return ( ATTR_SBEND );
	case FOLD('q','u'):  return ( ATTR_QUADRUPOLE );
	case FOLD('s','e'):  return ( ATTR_SEXTUPOLE );
	case FOLD('o','c'):  return ( ATTR_OCTUPOLE );
	case FOLD('m','u'):  return ( ATTR_MULTIPOLE );
	case FOLD('s','o'):  return ( ATTR_SOLENOID );
	case FOLD('r','f'): 
          if ( text[2] == 'c' )
	     return ( ATTR_RFCAVITY );
          if ( text[2] == 'q' )
	     return ( ATTR_RFQ_CELL );
          if ( text[2] == 'g' )
             return ( ATTR_RFGAP );
	case FOLD('e','l'):  return ( ATTR_EL_SEPARATE );
	case FOLD('s','r'):  return ( ATTR_SROT );
	case FOLD('y','r'):  return ( ATTR_YROT );
	case FOLD('h','k'):  return ( ATTR_HKICK );
	case FOLD('v','k'):  return ( ATTR_VKICK );
	case FOLD('k','i'):  return ( ATTR_KICKER );
	case FOLD('m','a'):  return ( ATTR_MARKER );
	case FOLD('e','c'):  return ( ATTR_ECOLLIMATOR );
	case FOLD('r','c'):  return ( ATTR_RCOLLIMATOR );
	case FOLD('t','h'):  return ( ATTR_THINLENS );
	case FOLD('t','a'):  return ( ATTR_TANK );
	case FOLD('e','d'):  return ( ATTR_EDGE );
	case FOLD('p','m'):  return ( ATTR_PMQ );
	case FOLD('d','o'):  return ( ATTR_DOUBLET );
	case FOLD('t','r'):  return ( ATTR_TRIPLET );
	case FOLD('s','p'):  return ( ATTR_SPECIAL );
	case FOLD('r','o'):  return ( ATTR_ROTATION );

	case FOLD('d','t'):  return ( ATTR_DTL_CELL );
	case FOLD('n','c'):  return ( ATTR_NCELLS );
	case FOLD('b','e'):  return ( ATTR_BEND );
	case FOLD('c','o'):  return ( ATTR_COMMAND );
	default:
	    fprintf( stderr, "get_element_type: found unknown type '%s', drift substituted\n", text );
	    return ( ATTR_DRIFT );
	}
    }

/* 
Takes care of better name handling since
there are duplicates in the table names
as far as the first two characters go 
*/
int get_table_type( char *text )
{
    int fold = FOLD( text[0], text[1] );

    if(DEBUG) printf("getting table type for: %s\n", text);

    switch ( fold ) {
	case RFCAVITY_TABLE:
	  if ( text[2] == 'c' )
	     return ( RFCAVITY_TABLE );
          if ( text[2] == 'q' )
	     return ( RFQ_CELL_TABLE );

	case BEAM_LINE_TABLE:
          if ( text[2] == 'a' )
	     return ( BEAM_LINE_TABLE );
          if ( text[2] == 'n' )
	     return ( BEND_TABLE );
         
        default: return fold;
       
    }
}


/* this array is indexed by the ATTR_element attrs. i.e. ATTR_SROT */
/* to determine which attribute to assign the STR_DEF to in elements */
 int str_attr[] =                         
    {                                     
    0,        
    0,        	/* DRIFT */
    0,       	/* HMONITOR */
    0,	        /* VMONITOR */
    0,	        /* MONITOR */
    0,	        /* INSTRUMENT */
    0,	        /* WIGGLER */
    ATTR_ANGLE,      /* RBEND */
    ATTR_ANGLE,      /* SBEND */
    ATTR_K1,	     /* QUADRUPOLE */
    ATTR_K2,         /* SEXTUPOLE */
    ATTR_K3,	     /* OCTUPOLE */
    0,               /* MULTIPOLE */
    ATTR_KS,	     /* SOLENOID */
    ATTR_FREQ,       /* RFCAVITY */
    ATTR_E,	     /* ELSEPARATE */
    ATTR_ANGLE,      /* SROT */
    ATTR_ANGLE,      /* YROT */
    ATTR_KICK, 	/* HKICK */
    ATTR_KICK, 	/* VKICK */
    0,	 	/* KICKER */
    0,	 	/* MARKER */
    0,	 	/* ECOLLIMATOR */
    0,	        /* RCOLLIMATOR */
    0,	        /* TBEND */
    0,	        /* THIN LENS */
    0,	        /* TANK */
    0,	        /* EDGE */
    ATTR_K1,    /* PMQ */
    0,	 	/* RFQCELL */
    ATTR_K1,    /* DOUBLET */
    0,	 	/* TRIPLET */
    0,	 	/* RFGAP */
    0,	 	/* SPECIAL */
    ATTR_ANGLE 	/* ROTATION */
    };           

/************************************************************************/
void set_db_name( char *name ) {
  strcpy( database_name, name );
}

/************************************************************************/
static MYSQL *conn;
static MYSQL_RES *result;
static MYSQL_ROW row;

/************************************************************************/
void open_db() {
  /*
  const char* server="10.5.2.193";

  const char* user = "ess";
  const char* password = "ess12";
  const char* database = "ess";
  */
  const char* server="auxserxx.esss.lu.se";  

  const char* user="lattice_reader";
  const char* password="not_beans_again";
  const char* database="ess2011";
      
  if (DEBUG) printf("dbquery::open_db()\n");
  if( database_name ) {
    database = database_name; }

  conn = mysql_init(NULL);
  if (!mysql_real_connect(conn,server,user,password,database,
			  0,NULL,0)) {
    fprintf(stderr,"%s\n",mysql_error(conn));
    exit(0);
  }

  /* *** TJS do we still need error handlers? */
  // dberrhandle( err_handler );
  // dbmsghandle( msg_handler );

  object[1] = table_name;
  object[2] = buff1;
  object[3] = buff2;
  object[4] = buff3;
  object[5] = buff4;
  object[6] = buff5;
  object[7] = buff6;
  object[8] = buff7;
  object[9] = buff8;
  object[10] = buff9;
  object[11] = buff10;
  object[12] = buff11;
  object[13] = buff12;
  object[14] = buff13;
  object[15] = buff14;
  object[16] = buff15;
  object[17] = buff16;
  object[18] = buff17;
  object[19] = buff18;
  object[20] = buff19;
  object[21] = buff20;
  object[22] = buff21;
  object[23] = comment;

  attr_array[0] = attr_buff1;
  attr_array[1] = attr_buff2;
  attr_array[2] = attr_buff3;
  attr_array[3] = attr_buff4;
  attr_array[4] = attr_buff5;
  attr_array[5] = attr_buff6;
  attr_array[6] = attr_buff7;
  attr_array[7] = attr_buff8;
  attr_array[8] = attr_buff9;
  attr_array[9] = attr_buff10;
  attr_array[10] = attr_buff11;
  attr_array[11] = attr_buff12;
  attr_array[12] = attr_buff13;
  attr_array[13] = attr_buff14;
  attr_array[14] = attr_buff15;
  attr_array[15] = attr_buff16;
  attr_array[16] = attr_buff17;
  attr_array[17] = attr_buff18;
  attr_array[18] = attr_buff19;
  attr_array[19] = attr_buff20;
  attr_array[20] = attr_buff21;
  attr_array[21] = attr_buff22;
  attr_array[22] = attr_buff23;
  attr_array[23] = attr_buff24;
  attr_array[24] = attr_buff25;
  attr_array[25] = attr_buff26;
  attr_array[26] = attr_buff27;
  attr_array[27] = attr_buff28;

}

/************************************************************************/

void close_db() {
  if (DEBUG) printf("dbquery::close_db()\n");
  mysql_close(conn);
}

/************************************************************************/
/* make a support node out of the support point data from the support_point */
/* routine.  returns a null pointer if there is no data */
support_node *get_support_points( char* in_name, char* fam_name,
				  char* sp_type )
{
  support_node *temp;

  if (DEBUG) printf("dbquery::get_support_points(%s,%s,%s)\n",in_name,fam_name,sp_type);

  if ( support_point( in_name, fam_name, sp_type ) ) {
    if ( (temp = (support_node *)malloc( sizeof(support_node) )) == NULL )
      {
	fprintf( stderr, "*** dbquery: mem allocation error\n" );
	exit( 1 );
	/* NOTREACHED */
      }

    strcpy( temp->name, in_name );
    strcpy( temp->type, buff1 );
    strcpy( temp->system_name, buff2 );
    temp->displacement = atof( buff3 );

    return( temp );
  } else return( NULL );
}

/************************************************************************/
/* get the rest of the rows of the last result */
support_node *get_next_sp( char *in_name )
{
  support_node *temp;

  if (DEBUG) printf("dbquery::get_next_sp(%s)\n",in_name);

  row = mysql_fetch_row(result);

  if ( row==NULL ) { return (support_node*)NULL; }

  temp = (support_node*)malloc(sizeof(support_node));
  if ( temp == NULL ){
    fprintf( stderr, "*** dbquery: support pointer mem allocation error\n" );
    exit( 1 );
    /* NOTREACHED */
  }

  strcpy( temp->name, in_name );
  strcpy( temp->type, buff1 );
  strcpy( temp->system_name, buff2 );
  temp->displacement = atof( buff3 );
  return( temp );
}

/************************************************************************/
char *synch_convert( char* in_name ) {
  if (DEBUG) printf("dbquery::synch_convert(%s)\n",in_name);
  sprintf(sqlCmd,"select standard_name from name_alias where synch_name = '%s'",
	  in_name );
  if (mysql_query(conn,sqlCmd)) {
    fprintf(stderr,"mysql error, line %d: %s\n",__LINE__,mysql_error(conn));
    exit(0);
  }
  result = mysql_use_result(conn);
  if ((row=mysql_fetch_row(result))!=NULL) {
    strcpy(buff7,row[0]);
    while (row!=NULL) { row=mysql_fetch_row(result); }
    mysql_free_result(result);
    return(buff7);
  }
  fprintf(stderr,"dbquery: error reading name_alias table\n");
  mysql_free_result(result);
  return((char*)NULL);
}

/************************************************************************/
char *synch_alias( char *in_name ) {
  if (DEBUG) printf("dbquery::synch_alias(%s)\n",in_name);
  sprintf(sqlCmd,"select synch_name from name_alias where standard_name = '%s'",
	  in_name );
  if (mysql_query(conn,sqlCmd)) {
    fprintf(stderr,"mysql error, line %d: %s\n",__LINE__,mysql_error(conn));
    exit(0);
  }
  result = mysql_use_result(conn);
  if ((row=mysql_fetch_row(result))!=NULL) {
    strcpy(buff1,row[0]);
    while (row!=NULL) { row=mysql_fetch_row(result); }
    mysql_free_result(result);
    return(buff1);
  }
  fprintf(stderr,"dbquery: error reading synch_alias table\n");
  mysql_free_result(result);
  return((char*)NULL);
}

/***********************************************************************/

char **get_data(char *in_name) {
  int iobj;
  int notfound = 0;

  if (DEBUG) printf("dbquery::get_data(%s)\n",in_name);
  object[0] = in_name;
    
  sprintf(sqlCmd,"select table_name from name_location where name = '%s'",
	  in_name );
  if (mysql_query(conn,sqlCmd)) {
    fprintf(stderr,"mysql error, line %d: %s\n",__LINE__,mysql_error(conn));
    exit(0);
  }
  result = mysql_use_result(conn);
  row = mysql_fetch_row(result);
  if (row) {
    strcpy(table_name,row[0]);
  } else {
    /* temporarily fool following code into thinking item was found in */
    /* support points table so we actually check there even though */
    /* the name_location table said it wasn't there */
    notfound = 1;
    table_name[0] = 's';
    table_name[1] = 'u';
    printf("get_data:: Table %s not found\n", in_name);
  }
  while (row!=NULL) { row=mysql_fetch_row(result); }
  mysql_free_result(result);

  // Better table name recognition
  switch (get_table_type(table_name)) {
  case BEAM_LINE_TABLE:    iobj = beam( in_name );     break;
  case MAGNET_PIECE_TABLE: iobj = magnet( in_name );   break;
  case SLOT_TABLE:         iobj = ideal( in_name );    break;
  case STRENGTH_TABLE:     iobj = strength( in_name ); break;
  case GEOMETRY_TABLE:     iobj = geometry( in_name ); break;
  case BUS_TABLE:          iobj = bus( in_name );      break;
  case RFCAVITY_TABLE:     iobj = rfcavity( in_name ); break;

  case DTL_CELL_TABLE:
  case NCELLS_TABLE:
  case QUADRUPOLE_TABLE:
  case DRIFT_TABLE:
  case RFQ_CELL_TABLE:
  case SOLENOID_TABLE:
  case BEND_TABLE:
  case COC_TABLE:
  case ELSEPARATOR_TABLE:  iobj = new_table( in_name);  break;	

  case SUPPORT_POINT_TABLE: iobj = support_point( in_name,
						  (char *)NULL,
						  (char *)NULL );


    /* *** TJS think this isn't needed with MySQL */
    // dbcanquery( dbproc );

    /* NOW if it wasn't in the name location table OR the */
    /* support points table, say so */
    if ( iobj == 0 && notfound == 1 ) {
      fprintf( stderr,
	       "***dbquery, line %d: %s not in name_location or support_points table\n",
	       __LINE__, in_name );

      exit( 1 );
      /*NOTREACHED*/
    }

    break;
  default:
    fprintf( stderr, "dbquery, line %d: table name '%s' unknown\n", __LINE__, table_name );
    exit( 1 );
    /*NOTREACHED*/
    break;
  }

  if ( iobj == 0 ) {
    fprintf(stderr,"dbquery, line %d: something went wrong searching %s for %s\n",
	    __LINE__, table_name,in_name);
    exit( 1 );
    /*NOTREACHED*/
  }
  return ( object );
}

/******************************************************************************/
/* This routine retrieves the element specific data from the element tables. */
/* The element name, table name, a list of the columns with desired */
/* attributes, and the number of desired attributes is supplied as input. */
/* The return value is a string array with the desired attribute values or */
/* a (char **)NULL if the name was no found in the table */

char **get_more( char *name, char *table_name, char *attr_str,
		 int more_attr_count) {
  int i;

  if (DEBUG) printf("dbquery::get_more(%s,%s,%s,%d)\n",name,table_name,attr_str,more_attr_count);

  /* *** TJS do we still need error handlers? */
  // dberrhandle( no_table_errs ); /* temp err handler */
  // dbmsghandle( no_table_handler ); /* temporary handler */

  // Check for various special tables that need additional data fetching, e.g. aperture
  if(strcmp(table_name, "solenoid") == 0)
    sprintf(sqlCmd,"select s.length, s.strength, a.xsize from solenoid s left join aperture a on a.name = '%s' where s.name = '%s'", name, name);
  else if( strcmp(table_name, "bend") == 0){
    	// Please note that the tilt is compared to string value of 'pi/2', not to an expression
	// This has been commented out because it is not OK to do it like this [2012-01-17, mrescic]
	//    sprintf(sqlCmd, "select b.length, b.angle, (select if(b.tilt like 'pi/2', 1, 0)), b.quad_strength, b.sxtp_strength, b.octp_strength, b.entrance_angle, b.exit_angle, b.field_integral, b.half_gap*2, b.entrance_curv, b.exit_curv, a.xsize, 0 from bend b left join aperture a on a.name='%s' where b.name = '%s'", name, name);
	sprintf(sqlCmd, "select b.length, b.angle, b.tilt, b.quad_strength, b.sxtp_strength, b.octp_strength, b.entrance_angle, b.exit_angle, b.field_integral, b.half_gap*2, b.entrance_curv, b.exit_curv, a.xsize, 0 from bend b left join aperture a on a.name='%s' where b.name = '%s'", name, name);
  }
  else if( strcmp(table_name, "closed_orbit_corrector") == 0)
    sprintf(sqlCmd,"select 0, 0, a.xsize from closed_orbit_corrector c left join aperture a on a.name = '%s' where c.name = '%s'", name, name);
  else if( strcmp(table_name, "elseparator") == 0)
    sprintf(sqlCmd,"select e.length, e.efield_strength, e.tilt, 0, a.xsize from elseparator e left join aperture a on a.name = '%s' where e.name = '%s'", name, name);
  else if( strcmp(table_name, "rfcavity") == 0)
    sprintf(sqlCmd,"select r.length, r.voltage, r.phase_lag, r.harmonic_number, r.rf_coupling, r.rf_power, r.shunt_imped, r.fill_time, 1, a.xsize from rfcavity r left join aperture a on a.name = '%s' where r.name = '%s'", name, name);
  else if( strcmp(table_name, "quadrupole") == 0)
    sprintf(sqlCmd, "select q.length, q.strength, q.tilt, 0, a.xsize, mp.comment from quadrupole q left join aperture a on a.name = '%s' left join magnet_piece mp on mp.name = '%s' where q.name = '%s'", name, name, name);
  else if( strcmp(table_name, "drift") == 0)
    sprintf(sqlCmd, "select d.length, a.xsize, a.ysize from drift d left join aperture a on a.name = '%s' where d.name = '%s'", name, name);
  else if( strcmp(table_name, "command") == 0)
    sprintf(sqlCmd, "select comment from magnet_piece where name = '%s'", name);
  else
    sprintf(sqlCmd,"select %s from %s where name='%s'", attr_str, table_name, name);

  if (mysql_query(conn,sqlCmd)) {
    fprintf(stderr,"mysql error, line %d: %s\n",__LINE__,mysql_error(conn));
    fprintf(stderr,"  %s\n",sqlCmd);

    return((char**)NULL);
  }
  result = mysql_use_result(conn);
  row = mysql_fetch_row(result);
  if (row==NULL) {
    if(DEBUG) fprintf( stderr,
	     "dbquery line %d: error reading %s table\n  query %s\n",
	     __LINE__, table_name, sqlCmd );
    mysql_free_result(result);
    return ((char**)NULL);
  }

  // Insert '0' if the there is a 'null' element retrieved in a row
  // In the case 'aperture' is missing for a solenoid ...
  for( i = 1; i <= more_attr_count; i++ ){
    if(row[i-1] != NULL)
      strcpy(attr_array[i-1],row[i-1]);
    else 
      strcpy(attr_array[i-1],"0");
  }

  while (row!=NULL) { row=mysql_fetch_row(result); }
  mysql_free_result(result);
  return( attr_array );
}

/******************************************************************************/
/* this reads the integer list of attr numbers and a list of expressions */
/* describing the values and adds the resulting attribute nodes to the */
/* attribute list specified */
void add_map_attrs( int *map, char **attr_exprs, attrnode **attr_list )
{
  int count = 0;

  if (DEBUG) printf("dbquery::add_map_attrs(%d,...)\n",map[0]);
  while ( map[count] != 0 ) {
    if ( map[count] == ATTR_AP_SHAPE || map[count] == ATTR_MS_SHAPE ) {
      expr_hook = get_expr_node( EXPR_SHAPE, (exprnode *)NULL,
				 (exprnode *)NULL, 0.0 );

      if ( expr_hook )  /* stash the shape character in the funcname */
	expr_hook->funcname = *attr_exprs[count];

    }
    // magnet_piece type is stored in the comment and should not be followed
    // here we define values for printout
    // 0 = quad, 1 = thinlensx, 2 = thinlensy
    else if(map[count] == ATTR_MAGNET_PIECE_TYPE){
      expr_hook = get_expr_node( EXPR_MPT, (exprnode *)NULL,
				 (exprnode *)NULL, 0.0 );

      if(strcmp(attr_exprs[count], "thinlensx") == 0)
	expr_hook->value = 1;
      else if(strcmp(attr_exprs[count], "thinlensy") == 0)
	expr_hook->value = 2;
      else
	expr_hook->value = 0;

    }
    else if(map[count] == ATTR_COMMAND){
	expr_hook = get_expr_node( EXPR_COMMAND, (exprnode *)NULL,
				 (exprnode *)NULL, 0.0 );
	// After too much trouble this solves it!
	strcpy(expr_hook->command, attr_exprs[count]);
    }
    else {
      sprintf( in_buffer, "expr %s\n\n", attr_exprs[count] );
      yyparse();
    }

    if ( expr_hook )
      *attr_list = link_attr_node( *attr_list, expr_hook, map[count] );

    count++;
  }
}

/******************************************************************************/
/* this fills in the symbol table data for the node supplied */
/* the symbol->name must be set */
/* support is a boolean value which indicates whether support point data */
/* structures should be constructed or if the support points should be */
/* converted to ATTR_MARKERS  with type "power" */
void db_resolve( symbol node )
{
  symbol_node *sym_data = ( (symbol_node *)(node->symbol_data) );
  char **new_data = get_data( node->name );
  char **more_data = (char **)NULL;

  if (DEBUG) printf("dbquery::db_resolve(node->name=%s)\n",node->name);
  int fold = get_table_type(new_data[SOURCE_TABLE]);

  switch ( fold ) {

  case MAGNET_PIECE_TABLE:

    set_label_type( node, get_element_type( new_data[TYPE_DEF] ) );
    if ( !(check_set_context( node, BEAM_ARG )) )
      report_error( "Inconsistent usage", node->name );

    if ( attr_count[sym_data->label_type] ) {
      more_data = get_more( node->name,
			    table_location[sym_data->label_type],
			    attr_string[sym_data->label_type],
			    attr_count[sym_data->label_type] );
		
      if ( !more_data && no_more_error[sym_data->label_type] ) {
	fprintf( stderr,
		 "***dbquery, line %d: no data in %s table for element %s\n",
		 __LINE__, table_location[sym_data->label_type], node->name );
	exit( 1 );
	/* NOTREACHED */
      }
    }

    if ( more_data )
      add_map_attrs( attr_maps[sym_data->label_type], more_data,
		     &(sym_data->expr_list) );
    else {
      sprintf( in_buffer, "expr %s\n\n", new_data[LEN_DEF] );
      yyparse();

      if ( expr_hook )
	sym_data->expr_list = link_attr_node( sym_data->expr_list,
					      expr_hook, ATTR_L );

      sprintf( in_buffer, "expr %s\n\n", new_data[STR_DEF] );
      yyparse();

      if ( expr_hook && str_attr[sym_data->label_type] )
	sym_data->expr_list = link_attr_node( sym_data->expr_list,
					      expr_hook, str_attr[sym_data->label_type] );

      sprintf( in_buffer, "expr %s\n\n", new_data[TILT_DEF] );
      yyparse();

      if ( expr_hook )
	sym_data->expr_list = link_attr_node( sym_data->expr_list,
					      expr_hook, ATTR_TILT );
    }

    if ( new_data[TYPE_ATTR_DEF][0] != ' '
	 && new_data[TYPE_ATTR_DEF][0] != '\0' )
      if ( (sym_data->sub_type = (char *)malloc(
						( strlen(new_data[TYPE_ATTR_DEF])+1 )* sizeof(char))) ) {
	strcpy( sym_data->sub_type, new_data[TYPE_ATTR_DEF] );
      } else {
	fprintf( stderr, "*** dbquery: mem allocation error\n" );
	exit( 1 );
	/* NOTREACHED */
      } else sym_data->sub_type = (char *)NULL;

    /* go for the aperture info */
    more_data = get_more( node->name, "aperture", ap_ms_columns, 5 );

    if ( more_data )
      add_map_attrs( ap_attrs, more_data, &(sym_data->expr_list) );

    /* go for the magnet size info */
    more_data = get_more( node->name, "magnet_size", ap_ms_columns, 5 );

    if ( more_data )
      add_map_attrs( ms_attrs, more_data, &(sym_data->expr_list) );

    break;

  case SUPPORT_POINT_TABLE:             

    set_label_type( node, ATTR_MARKER );

    if ( !(check_set_context( node, BEAM_ARG )) )
      report_error( "Inconsistent usage", node->name );

    /* set sub_type */
    if ( (sym_data->sub_type = (char *)malloc( 6 * sizeof(char))) ) {
      strcpy( sym_data->sub_type, "power" );
    } else {
      fprintf( stderr, "*** dbsf: mem allocation error\n" );
      exit( 1 );
    }

    break;                                              

  case BEAM_LINE_TABLE:
  case SLOT_TABLE:

    set_label_type( node, (
			   FOLD( new_data[SOURCE_TABLE][0], new_data[SOURCE_TABLE][1] )
			   == SLOT_TABLE ? SLOT : BEAMLINE ) );

    if ( !(check_set_context( node, BEAM_ARG )) )
      report_error( "Inconsistent usage", node->name );

    sprintf( in_buffer, "beam %s\n\n", new_data[BEAM_DEF] );
    yyparse();
    sym_data->beam_root = beam_hook;

    break;

  case BUS_TABLE:

    set_label_type( node, BEAMLINE );

    if ( !(check_set_context( node, BEAM_ARG )) )
      report_error( "Inconsistent usage", node->name );

    /* bind beam */
    sprintf( in_buffer, "beam %s\n\n", new_data[BEAM_DEF] );
    yyparse();
    sym_data->beam_root = beam_hook;

    /* bind level */
    if ( (sym_data->sub_type = malloc( 2*sizeof(char) )) != (char *)NULL ) {
      sym_data->sub_type[0] = *new_data[LEVEL_DEF];
      sym_data->sub_type[1] = '\0';
    } else {
      fprintf( stderr, "*** dbsf: mem allocation error\n" );
      exit( 1 );
      /* NOTREACHED */
    }

    break;

  case STRENGTH_TABLE:

    set_label_type( node, STRENGTH );
    if ( !(check_set_context( node, EXPR_ARG )) )
      report_error( "Inconsistent usage", node->name );

    sprintf( in_buffer, "expr %s\n\n", new_data[DEF_STRENGTH] );
    yyparse();

    if ( expr_hook )
      sym_data->expr_list = link_attr_node( sym_data->expr_list,
					    expr_hook, ATTR_L );

    break;

  case GEOMETRY_TABLE:

    set_label_type( node, GEOMETRY );
    if ( !(check_set_context( node, EXPR_ARG )) )
      report_error( "Inconsistent usage", node->name );

    sprintf( in_buffer, "expr %s\n\n", new_data[DEF_GEOMETRY] );
    yyparse();

    if ( expr_hook )
      sym_data->expr_list = link_attr_node( sym_data->expr_list,
					    expr_hook, ATTR_L );

    break;

  case NCELLS_TABLE:
  case DTL_CELL_TABLE:
  case RFQ_CELL_TABLE:
  case SOLENOID_TABLE:
  case QUADRUPOLE_TABLE:
  case DRIFT_TABLE:
  case BEND_TABLE:
  case COC_TABLE:
  case ELSEPARATOR_TABLE:
    
    if(fold == NCELLS_TABLE) 		set_label_type( node, ATTR_NCELLS);
    else if(fold == DTL_CELL_TABLE) 	set_label_type( node, ATTR_DTL_CELL);
    else if(fold == RFQ_CELL_TABLE)	set_label_type( node, ATTR_RFQ_CELL);
    else if(fold == SOLENOID_TABLE)     set_label_type( node, ATTR_SOLENOID);
    else if(fold == QUADRUPOLE_TABLE)   set_label_type( node, ATTR_QUADRUPOLE);
    else if(fold == DRIFT_TABLE) 	set_label_type( node, ATTR_DRIFT);
    else if(fold == BEND_TABLE) 	set_label_type( node, ATTR_BEND);
    else if(fold == COC_TABLE) 		set_label_type( node, ATTR_COC);
    else if(fold == ELSEPARATOR_TABLE) 	set_label_type( node, ATTR_EL_SEPARATE);

    if ( attr_count[sym_data->label_type] ) {
      more_data = get_more( node->name,
			    table_location[sym_data->label_type],
			    attr_string[sym_data->label_type],
			    attr_count[sym_data->label_type] );
		
      if ( !more_data && no_more_error[sym_data->label_type] ) {
	fprintf( stderr,
		 "***dbquery, line %d: no data in %s table for element %s\n",
		 __LINE__, table_location[sym_data->label_type], node->name );
	exit( 1 );
      }
    }

    if ( more_data )
      add_map_attrs( attr_maps[sym_data->label_type], more_data,
		     &(sym_data->expr_list) );
    /*
    else {
      printf("No new data, using old data!\n");
      add_map_attrs( attr_maps[sym_data->label_type], new_data, &(sym_data->expr_list) );

    }
    */

    if ( new_data[TYPE_ATTR_DEF][0] != ' ' && new_data[TYPE_ATTR_DEF][0] != '\0' ){
      if ( (sym_data->sub_type = (char *)malloc(( strlen(new_data[TYPE_ATTR_DEF])+1 )* sizeof(char))) ) {
	strcpy( sym_data->sub_type, new_data[TYPE_ATTR_DEF] );
      } 
      else {
	fprintf( stderr, "*** dbquery: mem allocation error\n" );
	exit( 1 );
      }
    }
    else sym_data->sub_type = (char *)NULL;

    break;

  default:
    fprintf( stderr, "*** dbquery : unknown table source found\n" );
    /* exit( 1 ); */
    /* NOTREACHED */
    break;

  }
}

/******************************************************************************/
static int beam( char *in_name )
{
  if (DEBUG) printf("dbquery::beam(%s)\n",in_name);
  sprintf(sqlCmd,"select elements, comment from beam_line where name = '%s'",
	  in_name);
  
  if (mysql_query(conn,sqlCmd)) {
    fprintf(stderr,"mysql error, line %d: %s\n",__LINE__,mysql_error(conn));
    return(0);
  }
  result = mysql_use_result(conn);
  if ((row=mysql_fetch_row(result))==NULL) {
    fprintf( stderr, "*** dbquery : unknown beam %s\n",in_name);
    mysql_free_result(result);
    return(0);
  }
  strcpy(buff1,row[0]);
  strcpy(comment,row[1]);
  while (row!=NULL) { row=mysql_fetch_row(result); }
  mysql_free_result(result);
  return(3);
}


/**************************************************************************/
/* return the type of the rule and the new name for a given slot if there */
/* is one else NULL */

char **get_rule( char *in_name, char *view_string )
{
  char **temp = (char **)malloc( 2*sizeof( char *) );

  if (DEBUG) printf("dbquery::get_rule(%s,%s)\n",in_name,view_string);
  sprintf(sqlCmd, "select type_rule, name, comment from view_rules where pieces = '%s' and lattice_view like '[%s]'", in_name, view_string);

  if (mysql_query(conn,sqlCmd)) {
    fprintf(stderr,"mysql error, line %d: %s\n",__LINE__,mysql_error(conn));
    return ((char**)NULL);
  }
  result = mysql_use_result(conn);
  if ((row=mysql_fetch_row(result))==NULL) {
    fprintf( stderr, "*** dbquery : unknown get_rule %s, %s\n",in_name,view_string);
    mysql_free_result(result);
    return ((char**)NULL);
  }
  strcpy(buff1,row[0]);
  strcpy(buff2,row[1]);
  strcpy(comment,row[2]);
  temp[0] = buff1;
  temp[1] = buff2;
  while (row!=NULL) { row=mysql_fetch_row(result); }
  mysql_free_result(result);
  return( temp );
}

/******************************************************************************/
static int bus( char *in_name )
{
  if (DEBUG) printf("dbquery::bus(%s)\n",in_name);
  sprintf(sqlCmd, "select elements, level, comment from bus where name = '%s'", in_name);
  
  if (mysql_query(conn,sqlCmd)) {
    fprintf(stderr,"mysql error, line %d: %s\n",__LINE__,mysql_error(conn));
    return(0);
  }
  result = mysql_use_result(conn);
  if ((row=mysql_fetch_row(result))==NULL) {
    fprintf( stderr, "*** dbquery : unknown bus %s\n",in_name);
    mysql_free_result(result);
    return(0);
  }
  strcpy(buff1,row[0]);
  strcpy(buff2,row[1]);
  strcpy(comment,row[2]);
  while (row!=NULL) { row=mysql_fetch_row(result); }
  mysql_free_result(result);
  return(4);
}

/******************************************************************************/
static int rfcavity( char *in_name )
{
  if (DEBUG) printf("dbquery::rfcavity(%s)\n",in_name);
  sprintf(sqlCmd, "select length,voltage,phase_lag,harmonic_number,rf_coupling,rf_power,shunt_imped,fill_time from rfcavity where name = '%s'", in_name);
  
  if (mysql_query(conn,sqlCmd)) {
    fprintf(stderr,"mysql error, line %d: %s\n",__LINE__,mysql_error(conn));
    return(0);
  }
  result = mysql_use_result(conn);
  if ((row=mysql_fetch_row(result))==NULL) {
    fprintf( stderr, "*** dbquery : unknown rfcavity %s\n",in_name);
    mysql_free_result(result);
    return(0);
  }
  strcpy(buff1,row[0]);
  strcpy(buff2,row[1]);
  strcpy(comment,row[2]);
  while (row!=NULL) { row=mysql_fetch_row(result); }
  mysql_free_result(result);
  return(6);
}

/******************************************************************************/
static int support_point( char *in_name, char *fam_name, char *sp_type )
{
  if (DEBUG) printf("dbquery::support_point(%s,%s,%s)\n",in_name,fam_name,sp_type);
  if ( fam_name == (char *)NULL )
    sprintf(sqlCmd,"select type, system_name, displacement, comment from support_points where name='%s'", in_name);
  else
    sprintf(sqlCmd,"select type, system_name, displacement, comment from support_points where name='%s' and system_name='%s' and type='%s'", in_name, fam_name, sp_type);
  
  if (mysql_query(conn,sqlCmd)) {
    fprintf(stderr,"mysql error, line %d: %s\n",__LINE__,mysql_error(conn));
    return(0);
  }
  result = mysql_use_result(conn);
  if ((row = mysql_fetch_row(result))==NULL) {
    fprintf( stderr, "*** dbquery : bad support_point %s\n",in_name);
    mysql_free_result(result);
    return(0);
  }
  strcpy(buff1,row[0]);
  strcpy(buff2,row[1]);
  strcpy(buff3,row[2]);
  strcpy(comment,row[3]);
  while (row!=NULL) { row=mysql_fetch_row(result); }
  mysql_free_result(result);
  return(5);
}

/******************************************************************************/

static int ideal( char *in_name )
{
  if (DEBUG) printf("dbquery::ideal(%s)\n",in_name);
  sprintf(sqlCmd,"select pieces, comment from slot where name='%s'", in_name);
  
  if (mysql_query(conn,sqlCmd)) {
    fprintf(stderr,"mysql error, line %d: %s\n",__LINE__,mysql_error(conn));
    return(0);
  }
  result = mysql_use_result(conn);
  if ((row = mysql_fetch_row(result))==NULL) {
    fprintf( stderr, "*** dbquery : bad ideal %s\n",in_name);
    mysql_free_result(result);
    return(0);
  }
  strcpy(buff1,row[0]);
  strcpy(comment,row[1]);
  while (row!=NULL) { row=mysql_fetch_row(result); }
  mysql_free_result(result);
  return(3);
}

/******************************************************************************/

static int magnet( char *in_name )
{
  if (DEBUG) printf("dbquery::magnet(%s)\n",in_name);
  sprintf(sqlCmd,"select type, length_defn, strength_defn, tilt, engineering_type, comment  from magnet_piece where name = '%s'", in_name);
  
  if (mysql_query(conn,sqlCmd)) {
    fprintf(stderr,"mysql error, line %d: %s\n",__LINE__,mysql_error(conn));
    return(0);
  }
  result = mysql_use_result(conn);
  if ((row = mysql_fetch_row(result))==NULL) {
    fprintf( stderr, "*** dbquery : bad magnet %s\n",in_name);
    mysql_free_result(result);
    return(0);
  }
  strcpy(buff1,row[0]);
  if(row[1])strcpy(buff2,row[1]);
  if(row[2])strcpy(buff3,row[2]);
  if(row[3])strcpy(buff4,row[3]);
  if(row[4])strcpy(buff5,row[4]);
  if(row[5])strcpy(comment,row[5]);
  while (row!=NULL) { row=mysql_fetch_row(result); }
  mysql_free_result(result);
  return(7);
}

/******************************************************************************/

/*
static int aperture( index )
char *index;
{
  dbcmd(dbproc, "select geometry, xsize, ysize");
  dbcmd(dbproc, " from aperture ");
  dbfcmd(dbproc, "where aperture_type = '%s'", index);

  if (mysql_query(dbproc) != FAIL) {
    if (dbresults(dbproc) != FAIL) {
      dbbind(dbproc, 1, STRINGBIND, 0, attr_buff21);
      dbbind(dbproc, 2, STRINGBIND, 0, attr_buff22);
      dbbind(dbproc, 3, STRINGBIND, 0, attr_buff23);
      if (dbnextrow(dbproc) == REG_ROW)
	return(4);
    }
  }
  return(0);
}
*/

/******************************************************************************/

/*
static int magnet_size( index )
char *index;
{
  dbcmd(dbproc, "select type, xsize, ysize");
  dbcmd(dbproc, " from magnet_size ");
  dbfcmd(dbproc, "where name = '%s'", index);

  if (mysql_query(dbproc) != FAIL) {
    if (dbresults(dbproc) != FAIL) {
      dbbind(dbproc, 1, STRINGBIND, 0, attr_buff24);
      dbbind(dbproc, 2, STRINGBIND, 0, attr_buff25);
      dbbind(dbproc, 3, STRINGBIND, 0, attr_buff26);
      if (dbnextrow(dbproc) == REG_ROW)
	return(4);
    }
  }
  return(0);
}
*/

/*******************************************************************/

static int strength( char *in_name )
{
  sprintf(sqlCmd,"select definition, comment  from %s where name = '%s'",
	  str_tbl_used, in_name);
  
  if (mysql_query(conn,sqlCmd)) {
    fprintf(stderr,"mysql error, line %d: %s\n",__LINE__,mysql_error(conn));
    return(0);
  }
  result = mysql_use_result(conn);
  if ((row = mysql_fetch_row(result))==NULL) {
    fprintf( stderr, "*** dbquery : bad strength %s\n",in_name);
    mysql_free_result(result);
    return(0);
  }
  strcpy(buff1,row[0]);
  strcpy(comment,row[1]);
  while (row!=NULL) { row=mysql_fetch_row(result); }
  mysql_free_result(result);
  return(3);
}

/*******************************************************************/
static int geometry( char *in_name )
{
  sprintf(sqlCmd,"select definition, comment from geometry where name = '%s'",
	  in_name);
  
  if (mysql_query(conn,sqlCmd)) {
    fprintf(stderr,"mysql error, line %d: %s\n",__LINE__,mysql_error(conn));
    return(0);
  }
  result = mysql_use_result(conn);
  if ((row = mysql_fetch_row(result))==NULL) {
    fprintf( stderr, "*** dbquery : bad geometry %s\n",in_name);
    mysql_free_result(result);
    return(0);
  }
  strcpy(buff1,row[0]);
  strcpy(comment,row[1]);
  while (row!=NULL) { row=mysql_fetch_row(result); }
  mysql_free_result(result);
  return(3);
}

/******************************************************************************/

static int new_table( char *in_name )
{
  return 1;  
}



/* END OF FILE */

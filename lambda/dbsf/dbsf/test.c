#include <mysql.h>
#include <stdio.h>

int main() {
  MYSQL *conn;
  MYSQL_RES *res;
  MYSQL_RES *res2;
  MYSQL_ROW row;
  MYSQL_ROW row2;
  MYSQL_FIELD* field;

  char *server = "auxserxx.esss.lu.se"; /* TJS *** */
  char *user = "garry";
  char *password = "HeaveInca";
  char *database = "linac";
   
  conn = mysql_init(NULL);
   
  /* Connect to database */
  if (!mysql_real_connect(conn, server,
			  user, password, database, 0, NULL, 0)) {
    fprintf(stderr, "error: %s\n", mysql_error(conn));
    return(0);
  }

  /* list tables */
  res=mysql_list_tables(conn,NULL);
  while ((row = mysql_fetch_row(res)) != NULL) {
    printf("list_tables output: table=%s\n", row[0]);
    /* list fields */
    res2=mysql_list_fields(conn,row[0],NULL);
    while ((field = mysql_fetch_field(res2)) != NULL)
      printf("  list_fields output: field=%s, type=%d, length=%ld\n", field->name, field->type,
	     field->length);
    mysql_free_result(res2);
  }
  mysql_free_result(res);
      
      
  /* send SQL query for beam_line table */
  if (mysql_query(conn, "SELECT * FROM rfcavity")) {
    fprintf(stderr, "error: %s\n", mysql_error(conn));
    return(0);
  }

  res = mysql_use_result(conn);
   
  /* output fields 0,1,2 (name/elements/comment) of each row of beam_line */
  while ((row = mysql_fetch_row(res)) != NULL)
    printf("output: %s, %s, %s\n", row[0], row[1], row[2]);

  /* Release memory used to store results and close connection */
  mysql_free_result(res);
  mysql_close(conn);
}

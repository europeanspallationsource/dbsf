
/* $Header: /scratch/trahern/dbsf/dbsf/RCS/dbsf_gdecls.h,v 1.1 1994/06/07 19:30:02 trahern Exp $ */

/* DBSF GDECLS HEADER */

/* prints <str> to stderr with the name and name_def the error ocurred during */
/* also calls inc_errors */
extern void report_error( /* msg, name */ );

/* incrtements the total number of errors counter and */
/* aborts if there have been too many errors */
extern void inc_errors();

/* the buffer for YYINPUT to draw from */
extern char in_buffer[];

/* the dbsf symbol table */
extern symbol_table dbsf_st;

/* hooks for yyparse results */
extern exprnode *expr_hook;
extern beamnode *beam_hook;

struct beam_head_info_struct
    {
    beam_optype beam_head_op;
    exprnode *iteration;
    };

/* END OF FILE */

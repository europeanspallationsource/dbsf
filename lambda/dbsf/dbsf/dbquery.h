
/* $Header: /scratch/trahern/dbsf/dbsf/RCS/dbquery.h,v 1.1 1994/06/07 19:30:02 trahern Exp $ */

/* database query routine header */

/* OPTIONAL : set dbname and the strength table name */
extern void set_db_name( /* char *name */ ); /* sets db name,default = 'fodo' */
extern char str_tbl_used[]; /* strength table to use, default = 'strength' */

/* USAGE : first call open_db(), THEN do ALL queries, THEN call close_db() */
#include "../lattice_tree/lattice_defines.h"

extern void open_db();
extern char *synch_alias( /* char *in_name */ ); /* resolve synch name alias */
extern char *synch_convert( /* char *in_name */ ); /* convert synch => std */

/* looks in support point table for names of support points.  A pointer to */
/* the name of the support point is returned or NULL if there is no such sp */
extern support_node *get_support_points( /* char *name */ );
/* call only after results have been initialized with get_support_point */
/* returns NULL when results are all gone or an error occurs */
extern support_node *get_next_sp( /* char *name */ );

/* looks for a rule for element <in_name> with lattice view character in */
/* the view string.  returns (char **)NULL if none */
extern char **get_rule( /* char *in_name, *view_string */ );

/* support 0 => support points turned into markers */
/* support <> 0 => support point data structures filled in */
extern void db_resolve( /* symbol node, int support */ );
extern void close_db();

/* END OF FILE */

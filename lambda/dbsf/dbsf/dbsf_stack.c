
/* $Header: /scratch/trahern/dbsf/dbsf/RCS/dbsf_stack.c,v 1.1 1994/06/07 19:30:02 trahern Exp $ */

/* STACK ROUTINES */

#include <stdio.h>
#include <stdlib.h>
#include "../lattice_tree/lattice_tree.h"
#include "dbsf_stack.h"

static stacknode *add_stack_node( symbol id, stacknode* next )
    {
    stacknode *new_node;

    if ( (new_node = (stacknode *)malloc( sizeof( stacknode ) )) )
	{
	new_node->next = next;
	new_node->unresolved = id;
	return ( new_node );
	}
    else
	{
	fprintf( stderr, "Stack allocation error, parsing aborted\n" );
	exit( 1 );
	/*NOTREACHED*/
	}
    }

void dbsf_push( symbol id )
    {
    stacknode *new_node;
    symbol_node *sym_data;

    sym_data = (symbol_node *)(id->symbol_data);

    if (sym_data==(symbol_node*)NULL) {
      printf("NULL sym_data in dbsf_push() for id->name=%s\n",id->name);
      return;
    }

    if ( !(sym_data->in_stack) )
        {
        sym_data->in_stack = 1; /* mark as added to stack */
        new_node = add_stack_node( id, stack ); /* add to stack */
        stack = new_node;
        }
    }

symbol dbsf_pop()
    {
    symbol data;
    stacknode *newstack;

    if ( stack )
	{
        data = stack->unresolved;
        newstack = stack->next;
        free( (char *)stack );
        stack = newstack;
        return ( data );
        }
    else
	{
	return ( (symbol)NULL );
	}
    }

/* END OF FILE */

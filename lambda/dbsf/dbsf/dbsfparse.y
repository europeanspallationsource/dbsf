%{

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../lattice_tree/lattice_tree.h"
#include "dbsf_gdecls.h"
#include "dbsf_stack.h"

#define yyerror( str ) report_error( str, in_buffer )

extern int yylex();

exprnode *nullexpr = (exprnode *)NULL;

%}

%union {
	double value;
	int op;
	symbol st_entry;
	beamnode *b_node;
	beam_optype beam_op;
	exprnode *ex;
	struct beam_head_info_struct beam_head_info;
       }

%start prog

%token <st_entry> TOK_ID
%token <value> TOK_NUM
%token TOK_EOL TOK_ATAN TOK_BEAM_START TOK_EXPR_START

 /* precedences */
%nonassoc <op> '(' ')' ',' '\\'
%left <op> '+' '-'
%left <op> '/' '*'

%type <ex> expr
%type <b_node> beam beam_element
%type <beam_head_info> beam_head

%%

prog:
	  TOK_EXPR_START expr TOK_EOL
	    {
	    expr_hook = $2;
	    return ( 0 );
	    }

	| TOK_BEAM_START beam TOK_EOL
	    {
	    beam_hook = $2;
	    return ( 0 );
	    }

	| TOK_EXPR_START '\\' TOK_EOL
	    {
	    expr_hook = get_expr_node( EXPR_TILT, nullexpr, nullexpr );
	    return ( 0 );
	    }

	| TOK_EXPR_START TOK_EOL
	    {
	    expr_hook = (exprnode *)NULL;
	    return ( 0 );
	    }

	| TOK_EOL TOK_EXPR_START expr TOK_EOL
	    {
	    expr_hook = $3;
	    return ( 0 );
	    }

	| TOK_EOL TOK_EXPR_START '\\' TOK_EOL
	    {
	    expr_hook = get_expr_node( EXPR_TILT, nullexpr, nullexpr );
	    return ( 0 );
	    }

	| TOK_EOL TOK_BEAM_START beam TOK_EOL
	    {
	    beam_hook = $3;
	    return ( 0 );
	    }

	| TOK_EOL TOK_EXPR_START TOK_EOL
	    {
	    expr_hook = (exprnode *)NULL;
	    return ( 0 );
	    }
	;

beam:
	  beam_element
	| beam beam_element
	    {
	    (beam_tail( $1 ))->next = $2;
	    $$ = $1;
	    }
	;

beam_element:

	  beam_head '(' beam ')'
	    {
	    $$ = get_beam_node( $1.beam_head_op, $1.iteration,
			(symbol)NULL, $3 );
	    }

	| '(' beam ')'
	    { $$ = get_beam_node( BEAM_NO_OP, nullexpr, (symbol)NULL, $2 ); }

	| beam_head TOK_ID
	    {
	    $$ = get_beam_node( $1.beam_head_op, $1.iteration,
			$2, (beamnode *)NULL );
	    if ( !(check_set_context( $2, BEAM_ARG )) )
		report_error( "Inconsistent usage", $2->name );
	    dbsf_push( $2);
	    }

	| TOK_ID
	    {
	    $$ = get_beam_node( BEAM_NO_OP, nullexpr, $1, (beamnode *)NULL );
	    if ( !(check_set_context( $1, BEAM_ARG )) )
		report_error( "Inconsistent usage", $1->name );
	    dbsf_push( $1 );
	    }
	;

beam_head:
	  '-'
	    {
	    $$.iteration = nullexpr;
	    $$.beam_head_op = BEAM_TRANSPOSE;
	    }
	| TOK_NUM '*'
	    {
	    $$.iteration = get_expr_node( EXPR_NUM, nullexpr, nullexpr );
	    $$.iteration->value = $1;
	    $$.beam_head_op = BEAM_REPEAT;
	    }
	| TOK_ID '*'
	    {
	    $$.iteration = get_expr_node( EXPR_STE, nullexpr, nullexpr );
	    /* make sure the value doesn't get mistaken for negative,
	     * meaning transpose
	     */
	    $$.iteration->value = 0.0;
	    $$.iteration->id = $1;
	    if ( !(check_set_context( $1, EXPR_ARG )) )
		report_error( "Inconsistent usage", $1->name );
	    dbsf_push( $1 );
	    $$.beam_head_op = BEAM_REPEAT;
	    }
	;

expr:
	  expr '+' expr
	    { $$ = get_expr_node( EXPR_ADD, $1, $3 ); }

	| expr '-' expr
	    { $$ = get_expr_node( EXPR_SUB, $1, $3 ); }

	| expr '*' expr
	    { $$ = get_expr_node( EXPR_MUL, $1, $3 ); }

	| expr '/' expr
	    { $$ = get_expr_node( EXPR_DIV, $1, $3 ); }

	| '(' expr ')'
	    { $$ = $2; }

	| '-' expr 	%prec '*'
	    { $$ = get_expr_node( EXPR_UNEG, $2, nullexpr ); }

	| '+' expr 	%prec '*'
	    { $$ = $2; }

	| TOK_ID
	    {
	    $$ = get_expr_node( EXPR_STE, nullexpr, nullexpr );
	    $$->id = $1;
	    if ( !(check_set_context( $1, EXPR_ARG )) )
		report_error( "Inconsistent usage", $1->name );
	    dbsf_push( $1 );
	    }

	| TOK_ID '(' expr ',' expr ')'
	    {
	    $$ = get_expr_node( EXPR_FUNC, $3, $5 );
	    $$->id = $1;
	    if ( !($$->funcname = get_funcname( $1->name )) )
		{
		$$->funcname = FUNC_ABS;
		report_error( "Function not implemented", $1->name );
		}
	    }

	| TOK_ID '(' expr ')'
	    {
	    $$ = get_expr_node( EXPR_FUNC, $3, nullexpr );
	    $$->id = $1;
	    if ( !($$->funcname = get_funcname( $1->name )) )
		{
		$$->funcname = FUNC_ABS;
		report_error( "Function not implemented", $1->name );
		}
	    }

	| TOK_ID '(' ')'
	    {
	    $$ = get_expr_node( EXPR_FUNC, nullexpr, nullexpr );
	    $$->id = $1;
	    if ( !($$->funcname = get_funcname( $1->name )) )
		{
		$$->funcname = FUNC_ABS;
		report_error( "Function not implemented", $1->name );
		}
	    }

	| TOK_NUM
	    {
	    $$ = get_expr_node( EXPR_NUM, nullexpr, nullexpr );
	    $$->value = $1;
	    }
	;
%%
/* END OF FILE */

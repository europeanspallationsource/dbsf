/* $Header: /scratch/trahern/dbsf/dbsf/RCS/dbsf_main.c,v 1.1 1994/06/07 19:30:02 trahern Exp $ */
/* the main driver routines for parse tree construction */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "../lattice_tree/lattice_defines.h"
#include "../lattice_tree/lattice_tree.h"
#include "../lattice_tree/lattice_flat.h"
#include "../lattice_tree/lattice_hilly.h"
#include "../lattice_tree/lattice_magic.h"
#include "../lattice_tree/lattice_trace3d.h"
#include "../lattice_tree/lattice_tracewin.h"
#include "../lattice_tree/lattice_transport.h"
#include "../lattice_tree/lattice_synch.h"
#include "../lattice_tree/lattice_sds.h"
#include "../lattice_tree/lattice_gdecls.h"
#include "../wireup/support.h"
#include "dbsf_do_views.h"
#include "dbsf_stack.h"
#include "dbsf_gdecls.h"
#include "dbquery.h"

extern void print_heading();

/* This global goes here so that TCELEMENT can be recognised
   properly only if SDS output is requested
*/
int sds_file = 0;		/* output an sds file format */
stacknode *stack;

/*======================================================================*/
symbol resolve( char *name ) {
  symbol root;
  symbol new_node;

  root = get_symbol_node( name, dbsf_st );
  dbsf_push( root );

  /* resolve symbol nodes from the stack as long as the supply from */
  /* db_resolve lasts */
  while ( (new_node = dbsf_pop()) ) {
    db_resolve( new_node );
  }

  return ( root );
}

/*======================================================================*/
int main( int argc, char *argv[], char *envp[] )
{
  extern char *optarg;	/* getopt name buffer */
  extern int optind;
  char *getenv();
  char *db_name = getenv( "DBSF_DB" ); /* this only works if DBSF_DB is set */

  symbol parse_tree;
  int errflag = 0;
  int c;			/* option letter buffer */
  char argname[MAXSTRING];	/* the name to be defined */
  int eval_exprs = 0;		/* evaluate expressions in output file */
  int group_lines = 0;		/* group param, elem and beam defs separately */
  int standard_403 = 0;		/* conform output to MAD 4.03 input standards */
  int standard_81 = 0;		/* conform output to MAD 8.1 input standards */
  int flat_file = 0;		/* output a flat file format */
  int old_flat = 0;		/* output original flat file format */
  int synch_file = 0;		/* output synch file format */
  int magic_file = 0;		/* output magic file format */
  int trace3d_file = 0;		/* output trace3d file format */
  int tracewin_file = 0;	/* output tracewin file format */
  int transport_file = 0;	/* output transport file format */
  int teapot_file = 0;		/* output teapot file ( MAD 4.03 ) */
  int grad_brho = 0;		/* make SYNCH routines parse the strength */
  int dim_info = 0;		/* output dimension info in sds file and TPOT */
  int has_views = 0;		/* indicates that we have lattice views */
  char *view_string;		/* this holds character flags indicating views*/
  /* definition and print out gradient and Brho for MAG and SXTP decls */

  /* creates empty symbol table */
  dbsf_st = sym_table_create( HASH_TABLE_SIZE, NO_SYM_VAL );
 
  /* set default for strength table  */
  strcpy( str_tbl_used , "strength" );

  optind = 1; /* start processing with the first command line argument */

  /* get command line control flags */
  /* to add a flag, add a variable, add the letter to the string below and */
  /* add three lines of the form below.  */
  while ( (c = getopt( argc, argv, "CdMSgesfFb8TtwpPV:" )) != EOF )
    switch ( c )
      {
      case 'C': /* output sds file */
	sds_file = 1;
	break;

      case 'd': /* output dimension info (for sds or teapot, mad too! */
	dim_info = 1;
	break;

      case 'e':	/* set expression evaluation flag */
	eval_exprs = 1;
	break;

      case 'f':	/* output old flat file */
	old_flat = 1;

	if ( flat_file )
	  {
	    fprintf( stderr, "dbsf: only option 'F' OR 'f' allowed\n" );
	    ++errflag;
	  }
	break;

      case 'F':	/* output new flat file */
	flat_file = 1;

	if ( old_flat )
	  {
	    fprintf( stderr, "dbsf: only option 'F' OR 'f' allowed\n" );
	    ++errflag;
	  }
	break;

      case 'g':	/* group param, elem and line declaration lines */
	group_lines = 1;
	break;

      case '8':	/* conform output to MAD 8.1 input standards */
	standard_81 = 1;

	if ( standard_403 )
	  {
	    fprintf( stderr,
		     "dbsf: -8 not allowed with -s or -p\n" );
	    ++errflag;
	  }
	break;

      case 's':	/* conform output to MAD 4.03 input standards */
	standard_403 = 1;

	if ( standard_81 )
	  {
	    fprintf( stderr,
		     "dbsf: -8 not allowed with -s\n" );
	    ++errflag;
	  }

	break;

      case 'b':	/* parse strngth def for gradient and Brho in SYNCH */
	grad_brho = 1;
	break;

      case 'S':	/* print out a SYNCH file */
	synch_file = 1;
	break;

      case 'M':	/* print out a MAGIC file */
	magic_file = 1;
	break;

      case 't':	/* print out a TRACE3D file */
	trace3d_file = 1;
	break;

      case 'w': /* print out a TraceWin file */
	tracewin_file = 1;
	break;

      case 'P':	/* print out a TRANSPORT file */
	transport_file = 1;
	break;

      case 'p':	/* print out a TEAPOT file */
	dim_info = 1;
	teapot_file = 1;
	standard_403 = 1;

	if (standard_81)
	  {
	    fprintf( stderr,
		     "dbsf: -8 not allowed with -p\n" );
	    ++errflag;
	  }

	break;

      case 'T':  /* use strength table specified */
	strcpy(str_tbl_used, argv[optind] );
	++optind;
	break;

      case 'V':	/* save view string */
	has_views = 1;
	view_string = optarg;
	break;

      case '?':
      default:
	++errflag;
	break;
      }

  /* just a little bit of flag and argument checking.  This could be */
  /* extended to make sure that incompatible flags were not being set */
  if ( optind < argc ) {
    strcpy( argname, argv[optind] );

    if ( argc - optind - 1 ) {
      db_name = argv[optind+1]; /* next check the command line for db */
	    
      if ( argc - optind - 2 )
	fprintf( stderr, "dbsf: options after database name ignored\n" );
    }
  } else {
    fprintf( stderr, "dbsf: mandatory key option missing\n" );
    ++errflag;
  }

  if ( errflag ) {
    fprintf( stderr, "Usage: dbsf [-CSMs8egfFbtpPdw] [-V view_flags] [-T str_tbl_name] key [source_db]\n" );

    fprintf( stderr, "-C		output an SDS file\n" );
    fprintf( stderr, "-d		add dimension info to SDS, MAD (make non-standard MAD)\n" );
    fprintf( stderr, "-S		output a SYNCH format file\n" );
    fprintf( stderr, "-M		output a MAGIC format file\n" );
    fprintf( stderr, "-s		conform to MAD 4.03 standards\n" );
    fprintf( stderr, "-8		conform to MAD 8.1 standards\n" );
    fprintf( stderr, "-e		evaluate expressions\n" );
    fprintf( stderr, "-g		group parameter, element and beam definitions separately\n" );
    fprintf( stderr, "-f		produce old flat file format\n" );
    fprintf( stderr, "-F		produce new flat file format\n" );
    fprintf( stderr, "-b		try to produce gradient & brho vals for elements in SYNCH file\n" );
    fprintf( stderr, "-t		output a TRACE3D format file\n" );
    fprintf( stderr, "-p		output a TEAPOT format file (like -sd sans DRIFT aperture info)\n" );
    fprintf( stderr, "-P		output a TRANSPORT format file\n" );
    fprintf( stderr, "-w		output a TraceWIN format file\n" );
    fprintf( stderr, "-V view_flags	character flags indicating which view rules to apply\n" );
    fprintf( stderr, "-T str_tbl_name	run with the specified strength table\n" );
    fprintf( stderr, "key		the name of the item to be defined\n" );
    fprintf( stderr, "source_db	db to search ( default is environment variable DBSF_DB ) \n" );

    exit( 2 );
    /*NOTREACHED*/
  }

  if ( db_name )
    set_db_name( db_name ); /* give the db routines whatever we have now */

  open_db(); /* initialize database query routines */

  parse_tree = resolve( argname );

  if( has_views )
    do_rules( parse_tree, view_string, 1 );

  close_db(); /* close database query routines */

  if ( flat_file || old_flat )
    print_flat( parse_tree, old_flat );
  else if ( synch_file )
    print_synch( parse_tree, grad_brho );
  else if (trace3d_file)
    print_trace3d( parse_tree );
  else if (tracewin_file)
    print_tracewin( parse_tree );
  else if ( magic_file )
    print_magic( parse_tree );
  else if ( sds_file )
    print_sds( parse_tree, db_name, argname, dim_info );
  else if ( transport_file ) {
    print_transport_title( db_name, argname );
    print_transport( parse_tree );
  } else {
    print_heading( db_name, argname );
    print_hilly( parse_tree, eval_exprs, group_lines, standard_403,
		 standard_81, dim_info, teapot_file );
  }

  return 0;
}
/* END OF FILE */


/* $Header: /scratch/trahern/dbsf/dbsf/RCS/whatis_main.c,v 1.1 1994/06/07 19:30:02 trahern Exp $ */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../lattice_tree/lattice_defines.h"
#include "../lattice_tree/lattice_tree.h"
#include "../lattice_tree/lattice_flat.h"
#include "../lattice_tree/lattice_hilly.h"
#include "../lattice_tree/lattice_magic.h"
#include "../lattice_tree/lattice_synch.h"
#include "../wireup/support.h"
#include "dbsf_stack.h"
#include "dbsf_gdecls.h"
#include "dbquery.h"


int sds_file = 0;
int mad_flag = 0;
stacknode *stack;

extern char **get_data(char *);

static void resolve( name )
     char *name;
{
  char **stuff_to_print;
  int name_lgth;
  int m;

  stuff_to_print = get_data( name ); /* get_data function is in dbquery.c */

  for (m=0; name[m] != ' ' && m < 20; m++)
    ; 
     
  name_lgth = m;
  switch ( name_lgth ) {
  case 1:
    printf("%.1s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 2:
    printf("%.2s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 3:
    printf("%.3s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 4:
    printf("%.4s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 5:
    printf("%.5s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 6:
    printf("%.6s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 7:
    printf("%.7s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 8:
    printf("%.8s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 9:
    printf("%.9s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 10:
    printf("%.10s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 11:
    printf("%.11s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 12:
    printf("%.12s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 13:
    printf("%.13s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 14:
    printf("%.14s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 15:
    printf("%.15s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 16:
    printf("%.16s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 17:
    printf("%.17s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 18:
    printf("%.18s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 19:
    printf("%.19s is in table %s  \n", name, stuff_to_print[1]);
    break;
  case 20:
    printf("%.20s is in table %s  \n", name, stuff_to_print[1]);
    break;
  default:
    printf("too many characters\n");
    break;
  }

  switch ( stuff_to_print[1][0] ) /*switch on first character of table_name */  {
  case 'b': /* beam_line */
    printf("  with elements = %s, and comment =  %s\n",
	   stuff_to_print[2], stuff_to_print[8] );
    break;
  case 'm': /* magnet_piece */
    printf("  with type = %s, length = %s, strength = %s, \n",
	   stuff_to_print[2],stuff_to_print[3],stuff_to_print[4] );
    printf("  tilt = %s, engineering_type = %s, and comment = %s \n",
	   stuff_to_print[5],stuff_to_print[6],stuff_to_print[8] );
    break;
  case 'i': /* ideal_magnet */
    printf("  with elements = %s, and comment =  %s\n",
	   stuff_to_print[2], stuff_to_print[8] );
    break;
  case 's': /* strength */
    if ( stuff_to_print[1][1] == 'l' ) {
      /* this disambiguates the strength table from the */
      /* ideal magnet table throughout dbsf (hack) */
      printf("  with elements = %s, and comment =  %s\n",
	     stuff_to_print[2], stuff_to_print[8] );
    }
    else
      printf("  with definition = %s, and comment =  %s\n",
	     stuff_to_print[2], stuff_to_print[8] );
    break;
  case 'g': /* geometry */
    printf("  with definition = %s, and comment =  %s\n",
	   stuff_to_print[2], stuff_to_print[8] );
    break;
  default:
    fprintf( stderr,"dbquery: table name '%s' unknown\n",stuff_to_print[1]);
    exit( 1 );
    /*NOTREACHED*/
    break;
  }

  /* close database query routines */
  close_db();
}


int main( int argc, char* argv[], char* envp[] )
{
  extern char *synch_convert();
  extern int optind;
  char *getenv();
  char *db_name = getenv( "DBSF_DB" ); /* this only works if DBSF_DB is set */

  int errflag = 0;
  int c;			/* option letter buffer */
  char argname[MAXSTRING];	/* the name to be defined */
  int synch_file = 0;		/* output synch file format */
  char *std_name;

  /* creates empty symbol table */
  dbsf_st = sym_table_create( HASH_TABLE_SIZE, NO_SYM_VAL );

  optind = 1; /* start processing with the first command line argument */

  /* set default for strength table  */
  strcpy( str_tbl_used , "strength" );

  /* get command line control flags */
  /* to add a flag, add a variable, add the letter to the string below and */
  /* add three lines of the form below.  */
  while ( (c = getopt( argc, argv, "S" )) != EOF )       
    switch ( c ) {                                                 
    case 'S':	/* print out a SYNCH file  */               
      synch_file = 1;                               
      break;                                        
                                                            
    case '?':                                         
    default:                                          
      ++errflag;                                    
      break;                                        
    }                                                 
                                                            
  /* just a little bit of flag and argument checking.  This could be */
  /* extended to make sure that incompatible flags were not being set */
  if ( optind < argc ) {                                                        
    strcpy( argname, argv[optind] );                         
                                                              
    if ( argc - optind - 1 ) {                                                      
      db_name = argv[optind+1];  /*  next check the command line for db  */
                                                                 	    
      if ( argc - optind - 2 )                               
	fprintf( stderr, "dbsf: options after database name ignored\n" );
    }  
  } else {                                                            
    fprintf( stderr, "dbsf: mandatory key option missing\n" );   
    ++errflag;                                                    
  }                                                              
                                                                    
  if ( errflag ) {                                                                    
    fprintf( stderr, "usage: dbwhatis [-S] key [db_name]\n" );                      
                                                                                           
    fprintf( stderr, "  -S         print out definition of a SYNCH name\n" );            
    fprintf( stderr, "  key	       the name of the item to be defined\n" );              
    fprintf( stderr, "  db_name    db to search (defaults to DBSF_DB environment variable)\n" );  
    exit( 2 );                     
    /* NOTREACHED */
  }                              
                                   
  if ( db_name )
    set_db_name( db_name ); /* give the db routines whatever we have now */

  /* initialize database query routines */
  open_db();

  if ( synch_file ) { 
    std_name = synch_convert( argname );
    printf("The standard name for %s is %s\n",argname,std_name);
    resolve( std_name ); 
  }
  else resolve( argname );

  return 0;
}
/* END OF FILE */

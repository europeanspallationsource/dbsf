
/* $Header: /rap/lambda/dbsf/dbsf/RCS/dbquery.c,v 1.3 2001/02/01 16:17:23 tepikian Exp $ */

#include <stdio.h>
#include <sybfront.h>
#include <sybdb.h>
#include <syberror.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "../lattice_tree/lattice_defines.h"
#include "../lattice_tree/lattice_tree.h"
#include "dbsf_gdecls.h"
#include "../wireup/support.h"
#include "dbquery.h"

static int support_point( char *, char *, char * );
static int beam(char *);
static int magnet(char *);
static int ideal(char *);
static int strength(char *);
static int geometry(char *);
static int bus(char *);
extern int sds_file;

#define OBJLEN		10
#define DEFLEN          160
#define ATTRLEN		140
#define ATTRCOUNT	60
#define COMLENGTH	160

/* array indices for the strings of interest in the database return struct */
#define BEAM_DEF		2	/* beamline, ideal magnet and bus */
#define TYPE_DEF		2	/* for magnet_piece table */
#define LEN_DEF			3	/* for magnet_piece table */
#define STR_DEF			4	/* for magnet_piece table */
#define TILT_DEF		5	/* for magnet_piece table */
#define TYPE_ATTR_DEF		6	/* for magnet_piece table */
#define APERTURE_DEF		7	/* for magnet_piece table */
#define DEF_STRENGTH		2	/* for strength table */
#define DEF_GEOMETRY		2	/* for geometry table */
#define SOURCE_TABLE		1	/* common to all return structures */
#define NAME_DEF		0	/* common to all return structures */
#define LEVEL_DEF		3	/* for bus table */
#define SYS_DEF			3	/* for support_points */
#define DISPLACEMENT_DEF	4	/* for support_points */

#define FOLD(most,least)	( 256 * most + least )

/* table names (identified by first 2 characters so far) */
#define MAGNET_PIECE_TABLE	FOLD( 'm', 'a' )
#define BEAM_LINE_TABLE		FOLD( 'b', 'e' )
#define SLOT_TABLE		FOLD( 's', 'l' )
#define STRENGTH_TABLE		FOLD( 's', 't' )
#define GEOMETRY_TABLE		FOLD( 'g', 'e' )
#define BUS_TABLE		FOLD( 'b', 'u' )
#define SUPPORT_POINT_TABLE	FOLD( 's', 'u' )

int        err_handler();
int        msg_handler();

char str_tbl_used[64];  /* name of strength table to use  */

char       database_name[256];
char       table_name[DEFLEN+1]; 
char       buff1[DEFLEN+1];
char       buff2[DEFLEN+1];
char       buff3[DEFLEN+1];
char       buff4[DEFLEN+1];
char       buff5[DEFLEN+1];
char       buff6[DEFLEN+1];
char       buff7[DEFLEN+1];
char       *object[OBJLEN];
DBPROCESS  *dbproc;
RETCODE    result;

char       comment[COMLENGTH];

char       attr_buff1[ATTRLEN];
char       attr_buff2[ATTRLEN];
char       attr_buff3[ATTRLEN];
char       attr_buff4[ATTRLEN];
char       attr_buff5[ATTRLEN];
char       attr_buff6[ATTRLEN];
char       attr_buff7[ATTRLEN];
char       attr_buff8[ATTRLEN];
char       attr_buff9[ATTRLEN];
char       attr_buff10[ATTRLEN];
char       attr_buff11[ATTRLEN];
char       attr_buff12[ATTRLEN];
char       attr_buff13[ATTRLEN];
char       attr_buff14[ATTRLEN];
char       attr_buff15[ATTRLEN];
char       attr_buff16[ATTRLEN];
char       attr_buff17[ATTRLEN];
char       attr_buff18[ATTRLEN];
char       attr_buff19[ATTRLEN];
char       attr_buff20[ATTRLEN];
char       attr_buff21[ATTRLEN];
char       attr_buff22[ATTRLEN];
char       attr_buff23[ATTRLEN];
char       attr_buff24[ATTRLEN];
char       attr_buff25[ATTRLEN];
char       attr_buff26[ATTRLEN];
char       attr_buff27[ATTRLEN];
char       attr_buff28[ATTRLEN];

char       *attr_array[ATTRCOUNT];

/******************************************************************************/

/* this tells us if there should be an error reported if there is no extra */
/* data in the extended tables.  this is TEMPORARY while magnet_piece still */
/* has length, strength and tilt info in it for backwards compatibility */
/* eventually, if there is an extended info table, an entry will have to be */
/* there */
int no_more_error[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
    0, 0, 1, 0, 0, 0, 0, 1, 1 };

/* these are the up-to-date arrays which provide the correct input parameters */
/* for get_more */

char *table_location[] =
    {
    "", /* "" => no more data */
    "drift",
    "monitor", /* hmon */
    "monitor", /* vmon */
    "monitor",  /* monitor */
    "monitor", /* instr */
    "", /* wiggler */
    "bend", /* rbend */
    "bend", /* sbend */
    "quadrupole",
    "sextupole",
    "octupole",
    "multipole",
    "solenoid",
    "rfcavity",
    "elseparator",
    "", /* srot */
    "", /* yrot */
    "closed_orbit_corrector", /* hkick */
    "closed_orbit_corrector", /* vkick */
    "closed_orbit_corrector", /* kicker */
    "", /* marker */
    "collimator", /* ecoll */
    "collimator", /* rcoll */
    "", /* tbend */
    "thinlens",
    "tank",
    "edge",
    "pmq",
    "rfqcell",
    "doublet",
    "triplet",
    "rfgap",
    "special",
    "rotation"
    };

char *attr_string[] =
    {
    "", /* "" => no attrs */
    "length",
    "length",
    "length",
    "length",
    "length",
    "",
    "length, angle, tilt, quad_strength, sxtp_strength, octp_strength, entrance_angle, exit_angle, field_integral, half_gap, entrance_curv, exit_curv",
    "length, angle, tilt, quad_strength, sxtp_strength, octp_strength, entrance_angle, exit_angle, field_integral, half_gap, entrance_curv, exit_curv",
    "length, strength, tilt",
    "length, strength, tilt",
    "length, strength, tilt",
    "K0L, K1L, K2L, K3L, K4L, K5L, K6L, K7L, K8L, K9L, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9",
    "length, strength",
    "length, voltage, phase_lag, harmonic_number, rf_coupling, rf_power, shunt_imped, fill_time",
    "length, efield_strength, tilt",
    "",
    "",
    "length, tilt, horz_angle, vert_angle",
    "length, tilt, horz_angle, vert_angle",
    "length, tilt, horz_angle, vert_angle",
    "",
    "length, xsize, ysize",
    "length, xsize, ysize",
    "",
    "xfocal, yfocal, zfocal",
    "accel_grad, length, phase, iden_cav",
    "angle, radius, gap, fringe_factor1, fringe_factor2",
    "bfld_grad, length, inner_radius, outer_radius",
    "V_div_rsquared, AV, length, phase, type_cell",
    "bfld_grad, length, dist_between",
    "bfld_grad_outer, length_outer, dist_between, bfld_grad_inner, length_inner",
    "eff_gap_voltage, phase, eg_flag, dW_flag, harmonic",
    "length",
    "phase"
    };

/* this is the magnet size and aperture column name list for the query */
char *ap_ms_columns = "shape, xsize, ysize, centerx, centery";

int attr_count[] =
  {
     0, 1, 1, 1, 1,
     1, 0, 12, 12, 3,
     3, 3, 20, 2, 8,
     3, 0, 0, 4, 4,
     4, 0, 3, 3, 0,
     3, 4, 5, 4, 5,
     3, 5, 5, 1, 1,
					0, 0, 0, 0, 0
};

/* these arrays provide a map for the attr_array structure */
/* returned by dbquery:more_data depending on the type of the element */

int length_only[] = { ATTR_L, ATTR_NULL };

int bend_attr[] = { ATTR_L, ATTR_ANGLE, ATTR_TILT, ATTR_K1, ATTR_K2, ATTR_K3,
    ATTR_E1, ATTR_E2, ATTR_FINT, ATTR_HGAP, ATTR_H1, ATTR_H2, ATTR_NULL };

int quad_attr[] = { ATTR_L, ATTR_K1, ATTR_TILT, ATTR_NULL };
int sext_attr[] = { ATTR_L, ATTR_K2, ATTR_TILT, ATTR_NULL };
int octu_attr[] = { ATTR_L, ATTR_K3, ATTR_TILT, ATTR_NULL };

int multipole_attr[] = { ATTR_K0L, ATTR_K1L, ATTR_K2L, ATTR_K3L, ATTR_K4L,
    ATTR_K5L, ATTR_K6L, ATTR_K7L, ATTR_K8L, ATTR_K9L, ATTR_T0, ATTR_T1, ATTR_T2,
    ATTR_T3, ATTR_T4, ATTR_T5, ATTR_T6, ATTR_T7, ATTR_T8, ATTR_T9, ATTR_NULL };

int sole_attr[] = { ATTR_L, ATTR_KS, ATTR_NULL };

int rf_attr[] = { ATTR_L, ATTR_VOLT, ATTR_LAG, ATTR_HARMON, ATTR_BETRF, ATTR_PG, ATTR_SHUNT, ATTR_TFILL, ATTR_NULL };

int elsep_attr[] = { ATTR_L, ATTR_E, ATTR_TILT, ATTR_NULL };

int coc_attr[] = { ATTR_L, ATTR_TILT, ATTR_KICKH, ATTR_KICKV, ATTR_NULL };

int collimator_attr[] = { ATTR_L, ATTR_XSIZE, ATTR_YSIZE, ATTR_NULL };

int thinlens_attr[] = { ATTR_XFOCAL, ATTR_YFOCAL, ATTR_ZFOCAL, ATTR_NULL };

int tank_attr[] = { ATTR_ACCEL, ATTR_L, ATTR_PHASE, ATTR_IDEN_CAV, ATTR_NULL };

int edge_attr[] = { ATTR_ANGLE, ATTR_RADIUS, ATTR_GAP, ATTR_FF1, ATTR_FF2, ATTR_NULL };

int pmq_attr[] = { ATTR_K1, ATTR_L, ATTR_IN_RAD, ATTR_OUT_RAD, ATTR_NULL };

int rfqcell_attr[] = { ATTR_VR2, ATTR_AV, ATTR_L, ATTR_PHASE, ATTR_CELL, ATTR_NULL };

int doublet_attr[] = { ATTR_K1, ATTR_L, ATTR_DIST_BETW, ATTR_NULL };

int triplet_attr[] = { ATTR_BFLD_OUT, ATTR_L_OUT, ATTR_DIST_BETW, ATTR_BFLD_IN, ATTR_L_IN, ATTR_NULL };

int rfgap_attr[] = { ATTR_EGAPVOLT, ATTR_PHASE, ATTR_EGFLAG, ATTR_DWFLAG, ATTR_HARMON, ATTR_NULL };

int special_attr[] = { ATTR_L, ATTR_NULL };

int rotation_attr[] = { ATTR_ANGLE, ATTR_NULL };

/* this is a master list of the attr maps which is indexed by the ATTR_elem */
int *attr_maps[] = { (int *)NULL, length_only, length_only, length_only,
    length_only, length_only, (int *)NULL, bend_attr, bend_attr, quad_attr,
    sext_attr, octu_attr, multipole_attr, sole_attr, rf_attr, elsep_attr,
    (int *)NULL, (int *)NULL, coc_attr, coc_attr, coc_attr, (int *)NULL,
    collimator_attr, collimator_attr, (int *)NULL, thinlens_attr, tank_attr,
    edge_attr, pmq_attr, rfqcell_attr, doublet_attr, triplet_attr, rfgap_attr, 
    special_attr, rotation_attr };

/* this is an attribute map for aperture info */
int ap_attrs[] = { ATTR_AP_SHAPE, ATTR_AP_XSIZE, ATTR_AP_YSIZE, ATTR_AP_CENTERX, ATTR_AP_CENTERY, ATTR_NULL };

/* this is an attribute map for magnet size info */
int ms_attrs[] = { ATTR_MS_SHAPE, ATTR_MS_XSIZE, ATTR_MS_YSIZE, ATTR_MS_CENTERX, ATTR_MS_CENTERY, ATTR_NULL };

/* takes a string representation of a type (i.e. quad, drift, ...) and */
/* returns the appropriate ATTR_ for that type.  Currently the first two */
/* characters are sufficient to distinguish the elements, so only the */
/* first two characters are examined (no longer true since trace3d added) */
int get_type( text )
char text[];
    {
    int fold = FOLD( text[0], text[1] );

    switch ( fold )
	{
	case FOLD('t','c'):  
			if (sds_file) return ( ATTR_TCELEMENT );
			else return ( ATTR_DRIFT );
	case FOLD('d','r'):  return ( ATTR_DRIFT );
	case FOLD('h','m'):  return ( ATTR_HMONITOR );
	case FOLD('v','m'):  return ( ATTR_VMONITOR );
	case FOLD('m','o'):  return ( ATTR_MONITOR );
	case FOLD('i','n'):  return ( ATTR_INSTRUMENT );
	case FOLD('w','i'):  return ( ATTR_WIGGLER );
	case FOLD('r','b'):  return ( ATTR_RBEND );
	case FOLD('s','b'):  return ( ATTR_SBEND );
	case FOLD('q','u'):  return ( ATTR_QUADRUPOLE );
	case FOLD('s','e'):  return ( ATTR_SEXTUPOLE );
	case FOLD('o','c'):  return ( ATTR_OCTUPOLE );
	case FOLD('m','u'):  return ( ATTR_MULTIPOLE );
	case FOLD('s','o'):  return ( ATTR_SOLENOID );
	case FOLD('r','f'): 
          if ( text[2] == 'c' )
	     return ( ATTR_RFCAVITY );
          if ( text[2] == 'q' )
	     return ( ATTR_RFQCELL );
          if ( text[2] == 'g' )
             return ( ATTR_RFGAP );
	case FOLD('e','l'):  return ( ATTR_EL_SEPARATE );
	case FOLD('s','r'):  return ( ATTR_SROT );
	case FOLD('y','r'):  return ( ATTR_YROT );
	case FOLD('h','k'):  return ( ATTR_HKICK );
	case FOLD('v','k'):  return ( ATTR_VKICK );
	case FOLD('k','i'):  return ( ATTR_KICKER );
	case FOLD('m','a'):  return ( ATTR_MARKER );
	case FOLD('e','c'):  return ( ATTR_ECOLLIMATOR );
	case FOLD('r','c'):  return ( ATTR_RCOLLIMATOR );
	case FOLD('t','h'):  return ( ATTR_THINLENS );
	case FOLD('t','a'):  return ( ATTR_TANK );
	case FOLD('e','d'):  return ( ATTR_EDGE );
	case FOLD('p','m'):  return ( ATTR_PMQ );
	case FOLD('d','o'):  return ( ATTR_DOUBLET );
	case FOLD('t','r'):  return ( ATTR_TRIPLET );
	case FOLD('s','p'):  return ( ATTR_SPECIAL );
	case FOLD('r','o'):  return ( ATTR_ROTATION );
	default:
	    fprintf( stderr, "Get_Type: found unknown type '%s', drift substituted\n", text );
	    return ( ATTR_DRIFT );
	}
    }

/* this array is indexed by the ATTR_element attrs. i.e. ATTR_SROT */
/* to determine which attribute to assign the STR_DEF to in elements */
 int str_attr[] =                         
    {                                     
    0,        
    0,        	/* DRIFT */
    0,       	/* HMONITOR */
    0,	        /* VMONITOR */
    0,	        /* MONITOR */
    0,	        /* INSTRUMENT */
    0,	        /* WIGGLER */
    ATTR_ANGLE,      /* RBEND */
    ATTR_ANGLE,      /* SBEND */
    ATTR_K1,	     /* QUADRUPOLE */
    ATTR_K2,         /* SEXTUPOLE */
    ATTR_K3,	     /* OCTUPOLE */
    0,               /* MULTIPOLE */
    ATTR_KS,	     /* SOLENOID */
    ATTR_FREQ,       /* RFCAVITY */
    ATTR_E,	     /* ELSEPARATE */
    ATTR_ANGLE,      /* SROT */
    ATTR_ANGLE,      /* YROT */
    ATTR_KICK, 	/* HKICK */
    ATTR_KICK, 	/* VKICK */
    0,	 	/* KICKER */
    0,	 	/* MARKER */
    0,	 	/* ECOLLIMATOR */
    0,	        /* RCOLLIMATOR */
    0,	        /* TBEND */
    0,	        /* THIN LENS */
    0,	        /* TANK */
    0,	        /* EDGE */
    ATTR_K1,    /* PMQ */
    0,	 	/* RFQCELL */
    ATTR_K1,    /* DOUBLET */
    0,	 	/* TRIPLET */
    0,	 	/* RFGAP */
    0,	 	/* SPECIAL */
    ATTR_ANGLE 	/* ROTATION */
    };           

/************************************************************************/
void set_db_name( name )
char *name;
    {
    strcpy( database_name, name );
    }

/************************************************************************/
void open_db()
    {
    LOGINREC   *login;
    int tries = 0;

    if ( (login = dblogin( )) == NULL )
	{
	dbperror( dberrno() );
	exit(0);
	}

    DBSETLUSER( login, "lattice_reader" );
    DBSETLPWD( login, "not_beans_again" );
    DBSETLAPP( login, "dbquery" );

    while ( (dbproc = dbopen( login, (char *)NULL )) == NULL )
        {
	dbperror( dberrno() );

	if ( tries++ > 0 )
	    exit(0);
	else
	    sleep(1);
        }

    dberrhandle( err_handler );
    dbmsghandle( msg_handler );

    if ( dbuse( dbproc, database_name ) == FAIL )
	{
	fprintf( stderr, "***dbquery: dbuse unsuccessful\n" );
	exit( 1 );
	/*NOTREACHED*/
	}

    object[1] = table_name;
    object[2] = buff1;
    object[3] = buff2;
    object[4] = buff3;
    object[5] = buff4;
    object[6] = buff5;
    object[7] = buff6;
    object[8] = comment;

    attr_array[0] = attr_buff1;
    attr_array[1] = attr_buff2;
    attr_array[2] = attr_buff3;
    attr_array[3] = attr_buff4;
    attr_array[4] = attr_buff5;
    attr_array[5] = attr_buff6;
    attr_array[6] = attr_buff7;
    attr_array[7] = attr_buff8;
    attr_array[8] = attr_buff9;
    attr_array[9] = attr_buff10;
    attr_array[10] = attr_buff11;
    attr_array[11] = attr_buff12;
    attr_array[12] = attr_buff13;
    attr_array[13] = attr_buff14;
    attr_array[14] = attr_buff15;
    attr_array[15] = attr_buff16;
    attr_array[16] = attr_buff17;
    attr_array[17] = attr_buff18;
    attr_array[18] = attr_buff19;
    attr_array[19] = attr_buff20;
    attr_array[20] = attr_buff21;
    attr_array[21] = attr_buff22;
    attr_array[22] = attr_buff23;
    attr_array[23] = attr_buff24;
    attr_array[24] = attr_buff25;
    attr_array[25] = attr_buff26;
    attr_array[26] = attr_buff27;
    attr_array[27] = attr_buff28;

    }

/************************************************************************/

void close_db()
    {
    dbexit();
    }

/************************************************************************/
/* make a support node out of the support point data from the support_point */
/* routine.  returns a null pointer if there is no data */
support_node *get_support_points( in_name, fam_name, sp_type )
char *in_name;
char *fam_name;
char *sp_type;
{
    support_node *temp;

    if ( support_point( in_name, fam_name, sp_type ) )
    {
	if ( (temp = (support_node *)malloc( sizeof(support_node) )) == NULL )
	{
	    fprintf( stderr, "*** dbquery: mem allocation error\n" );
	    exit( 1 );
	    /* NOTREACHED */
	}

	strcpy( temp->name, in_name );
	strcpy( temp->type, buff1 );
	strcpy( temp->system_name, buff2 );
	temp->displacement = atof( buff3 );

	return( temp );
    } else {
	return( NULL );
    }
}

/************************************************************************/
/* get the rest of the rows of the last result */
support_node *get_next_sp( in_name )
char *in_name;
{
    support_node *temp;

    if ( dbnextrow( dbproc ) == REG_ROW )
    {
	if ( (temp = (support_node *)malloc( sizeof(support_node) )) == NULL )
	{
	    fprintf( stderr, "*** dbquery: mem allocation error\n" );
	    exit( 1 );
	    /* NOTREACHED */
	}

	strcpy( temp->name, in_name );
	strcpy( temp->type, buff1 );
	strcpy( temp->system_name, buff2 );
	temp->displacement = atof( buff3 );
	return( temp );
    } else {
	return( (support_node *)NULL );
    }
}

/************************************************************************/
char *synch_convert( in_name )
char *in_name;
    {
    dbcmd( dbproc, "select standard_name from name_alias" );
    dbfcmd( dbproc, " where synch_name = '%s'", in_name );    

    if ( dbsqlexec(dbproc) != FAIL)
	{
	if ( dbresults(dbproc) != FAIL)
	    {
	    dbbind(dbproc, 1, STRINGBIND, 0, buff7);

		if ( dbnextrow(dbproc) == REG_ROW )
		    return( buff7 );
		else
		    return( (char *)NULL );
	    }
	}

    fprintf(stderr,"dbquery: error reading name_alias table\n");
    exit( 1 );
    /*NOTREACHED*/
    }
/************************************************************************/
char *synch_alias( in_name )
char *in_name;
    {
    dbcmd( dbproc, "select synch_name from name_alias" );
    dbfcmd( dbproc, " where standard_name = '%s'", in_name );

    if ( dbsqlexec(dbproc) != FAIL)
	{
	if ( dbresults(dbproc) != FAIL)
	    {
	    dbbind(dbproc, 1, STRINGBIND, 0, buff1);

		if ( dbnextrow(dbproc) == REG_ROW )
		    return( buff1 );
		else
		    return( (char *)NULL );
	    }
	}

    fprintf(stderr,"dbquery: error reading name_alias table\n");
    exit( 1 );
    /*NOTREACHED*/
    }

/***********************************************************************/

char **get_data( in_name )
char in_name[];
    {
    int iobj;
    int notfound = 0;

    object[0] = in_name;

    dbcmd( dbproc, "select table_name from name_location" );
    dbfcmd( dbproc, " where name = '%s'", in_name );

    if ( dbsqlexec( dbproc ) == FAIL )
	{
	fprintf( stderr, "***dbquery: dbsqlexec FAILed\n" );
	exit( 1 );
	/*NOTREACHED*/
	}

    if ( dbresults( dbproc ) == FAIL )
	{
	fprintf( stderr, "***dbquery: dbresults FAILed\n" );
	exit( 1 );
	/*NOTREACHED*/
	}

    dbbind( dbproc, 1, STRINGBIND, 0, table_name );

    if ( (result = dbnextrow( dbproc )) == FAIL )
	{
	fprintf( stderr, "***dbquery: dbnextrow FAILed\n" );
	exit( 1 );
	/*NOTREACHED*/
	}

    if ( result == NO_MORE_ROWS )
    {
	/* temporarily fool following code into thinking item was found in */
	/* support points table so we actually check there even though */
	/* the name_location table said it wasn't there */
	notfound = 1;
	table_name[0] = 's';
	table_name[1] = 'u';
    }

    switch ( FOLD( table_name[0], table_name[1] )  )
	{
	case BEAM_LINE_TABLE:
	    iobj = beam( in_name );
	    break;
	case MAGNET_PIECE_TABLE:
	    iobj = magnet( in_name );
	    break;
	case SLOT_TABLE:
	    iobj = ideal( in_name );
	    break;
	case STRENGTH_TABLE:
	    iobj = strength( in_name );
	    break;
	case GEOMETRY_TABLE:
	    iobj = geometry( in_name );
	    break;
	case BUS_TABLE:
	    iobj = bus( in_name );
	    break;
	case SUPPORT_POINT_TABLE:
	    iobj = support_point( in_name, (char *)NULL, (char *)NULL );
	    dbcanquery( dbproc );

	    /* NOW if it wasn't in the name location table OR the */
	    /* support points table, say so */
	    if ( iobj == 0 && notfound == 1 )
	    {
		fprintf( stderr,
		"***dbquery: %s not in name_location or support_points table\n",
		in_name );

		exit( 1 );
		/*NOTREACHED*/
	    }

	    break;
	default:
	    fprintf( stderr, "dbquery: table name '%s' unknown\n", table_name );
	    exit( 1 );
	    /*NOTREACHED*/
	    break;
	}

    if ( iobj == 0 ) 
	{
	fprintf(stderr,"dbquery: something went wrong searching %s for %s\n",table_name,in_name);
	exit( 1 );
	/*NOTREACHED*/
	}
    else
	return ( object );

    }

/******************************************************************************/

/* this is the message handler which takes care of invalid table lookups */
int no_table_handler( dbproc, msgno, msgstate, severity, msgtext, srvname, 
    procname, line )
DBPROCESS *dbproc;
DBINT msgno;
int msgstate;
int severity;
char *msgtext;
char *srvname;
char *procname;
DBUSMALLINT line;
{
    if ( msgno == 208 && msgstate == 1 && severity == 16 ) /* invalid table */
    {
	/* do nothing */
    } else {
	fprintf( stderr, "*** DB error message: %ld, Level %d, State %d\n",
	    msgno, severity, msgstate );
	
	if ( strlen(srvname) > 0 )
	    fprintf( stderr, "Server '%s', ", srvname );

	if ( strlen(procname) > 0 )
	    fprintf( stderr, "Procedure '%s', ", procname );

	if ( line > 0 )
	    fprintf( stderr, "Line %d, ", line );

	fprintf( stderr, "\n\t%s\n", msgtext );
    }

    return( 0 );
}

/******************************************************************************/

/* this is the error handler which takes care of invalid table lookups */
int no_table_errs( dbproc, severity, dberr, oserr, dberrstr, oserrstr )
DBPROCESS *dbproc;
int severity;
int dberr;
int oserr;
char *dberrstr;
char *oserrstr;
    {
    if ( (dbproc == NULL) || (DBDEAD(dbproc)) )
	return( INT_EXIT );
    else
        {
	if ( *dberrstr == 'G' )
	   return( INT_CANCEL ); /* do nothing */ 
	else
	   {
	   fprintf( stderr, "DB-LIBRARY error: %s\n", dberrstr );

	   if ( oserr != DBNOERR )
	       fprintf( stderr, "OS error: %s\n", oserrstr );

	   return( INT_CANCEL );
	   }
	}
    }

/******************************************************************************/

/* This routine retrieves the element specific data from the element tables. */
/* The element name, table name, a list of the columns with desired */
/* attributes, and the number of desired attributes is supplied as input. */
/* The return value is a string array with the desired attribute values or */
/* a (char **)NULL if the name was no found in the table */

char **get_more( name, table_name, attr_str, more_attr_count)
char *name, *table_name, *attr_str;
int more_attr_count;
    {
    int i;

    dberrhandle( no_table_errs ); /* temp err handler */
    dbmsghandle( no_table_handler ); /* temporary handler */

    dbfcmd( dbproc, "select %s", attr_str );
    dbfcmd( dbproc, " from %s ", table_name );
    dbfcmd( dbproc, "where name = '%s'", name );

    if ( dbsqlexec(dbproc) != FAIL )
	if ( dbresults(dbproc) != FAIL )
	    {
	    for( i = 1; i <= more_attr_count; i++ )
		dbbind(dbproc, i, STRINGBIND, 0, attr_array[i-1]);

	    if ((result = dbnextrow(dbproc)) == REG_ROW)
		{
		dberrhandle( (char *)NULL );
		dbmsghandle( (char *)NULL );
		return( attr_array );
		}
	    else
		if ( result == FAIL )
		    {
		    fprintf( stderr,
			"***dbquery: error reading %s table\n", table_name );
		    exit( 1 );
		    /*NOTREACHED*/
		    }
	    }

    dberrhandle( (char *)NULL );
    dbmsghandle( (char *)NULL );
    return( (char **)NULL );
}

/******************************************************************************/
/* this reads the integer list of attr numbers and a list of expressions */
/* describing the values and adds the resulting attribute nodes to the */
/* attribute list specified */
void add_map_attrs( map, attr_exprs, attr_list )
int *map;
char **attr_exprs;
attrnode **attr_list;
{
    int count = 0;

    while ( map[count] != 0 )
    {
	if ( map[count] == ATTR_AP_SHAPE || map[count] == ATTR_MS_SHAPE )
	{
	    expr_hook = get_expr_node( EXPR_SHAPE, (exprnode *)NULL,
	      (exprnode *)NULL, 0.0 );

            if ( expr_hook )  /* stash the shape character in the funcname */
		expr_hook->funcname = *attr_exprs[count];

	} else {
	    sprintf( in_buffer, "expr %s\n\n", attr_exprs[count] );
	    yyparse();
	}

	if ( expr_hook )
	    *attr_list = link_attr_node( *attr_list, expr_hook, map[count] );

	count++;
    }
}

/******************************************************************************/

/* this fills in the symbol table data for the node supplied */
/* the symbol->name must be set */
/* support is a boolean value which indicates whether support point data */
/* structures should be constructed or if the support points should be */
/* converted to ATTR_MARKERS  with type "power" */
void db_resolve( node )
symbol node;
    {
    symbol_node *sym_data = ( (symbol_node *)(node->symbol_data) );
    char **new_data = get_data( node->name );
    char **more_data = (char **)NULL;

    switch ( FOLD( new_data[SOURCE_TABLE][0], new_data[SOURCE_TABLE][1] ) )
	{

	case MAGNET_PIECE_TABLE:

	    set_label_type( node, get_type( new_data[TYPE_DEF] ) );
	    if ( !(check_set_context( node, BEAM_ARG )) )
		report_error( "Inconsistent usage", node->name );

	    if ( attr_count[sym_data->label_type] )
		{
		more_data = get_more( node->name,
		    table_location[sym_data->label_type],
		    attr_string[sym_data->label_type],
		    attr_count[sym_data->label_type] );
		
		if ( !more_data && no_more_error[sym_data->label_type] )
		    {
		    fprintf( stderr,
			"***dbquery : no data in %s table for element %s\n",
			table_location[sym_data->label_type], node->name );
		    exit( 1 );
		    /* NOTREACHED */
		    }
		}

	    if ( more_data )
		add_map_attrs( attr_maps[sym_data->label_type], more_data,
		    &(sym_data->expr_list) );
	    else
		{
		sprintf( in_buffer, "expr %s\n\n", new_data[LEN_DEF] );
		yyparse();

		if ( expr_hook )
		    sym_data->expr_list = link_attr_node( sym_data->expr_list,
			expr_hook, ATTR_L );

		sprintf( in_buffer, "expr %s\n\n", new_data[STR_DEF] );
		yyparse();

		if ( expr_hook && str_attr[sym_data->label_type] )
		    sym_data->expr_list = link_attr_node( sym_data->expr_list,
			expr_hook, str_attr[sym_data->label_type] );

		sprintf( in_buffer, "expr %s\n\n", new_data[TILT_DEF] );
		yyparse();

		if ( expr_hook )
		    sym_data->expr_list = link_attr_node( sym_data->expr_list,
			expr_hook, ATTR_TILT );
		}

	    if ( new_data[TYPE_ATTR_DEF][0] != ' '
		 && new_data[TYPE_ATTR_DEF][0] != '\0' )
		if ( (sym_data->sub_type = (char *)malloc(
		  ( strlen(new_data[TYPE_ATTR_DEF])+1 )* sizeof(char))) )
		    {
		    strcpy( sym_data->sub_type, new_data[TYPE_ATTR_DEF] );
		    }
		else
		    {
		    fprintf( stderr, "*** dbquery: mem allocation error\n" );
		    exit( 1 );
		    /* NOTREACHED */
		    }
	    else
		sym_data->sub_type = (char *)NULL;

            /* go for the aperture info */
	    more_data = get_more( node->name, "aperture", ap_ms_columns, 5 );

	    if ( more_data )
		add_map_attrs( ap_attrs, more_data, &(sym_data->expr_list) );

	    /* go for the magnet size info */
	    more_data = get_more( node->name, "magnet_size", ap_ms_columns, 5 );

	    if ( more_data )
		add_map_attrs( ms_attrs, more_data, &(sym_data->expr_list) );

	    break;

	case SUPPORT_POINT_TABLE:             

	    set_label_type( node, ATTR_MARKER );

	    if ( !(check_set_context( node, BEAM_ARG )) )
		report_error( "Inconsistent usage", node->name );

	    /* set sub_type */
	    if ( (sym_data->sub_type = (char *)malloc( 6 * sizeof(char))) )
	    {
		strcpy( sym_data->sub_type, "power" );
	    } else {
		fprintf( stderr, "*** dbsf: mem allocation error\n" );
		exit( 1 );
	    }

	    break;                                              

	case BEAM_LINE_TABLE:
	case SLOT_TABLE:

	    set_label_type( node, (
		FOLD( new_data[SOURCE_TABLE][0], new_data[SOURCE_TABLE][1] )
		== SLOT_TABLE ? SLOT : BEAMLINE ) );

	    if ( !(check_set_context( node, BEAM_ARG )) )
		report_error( "Inconsistent usage", node->name );

	    sprintf( in_buffer, "beam %s\n\n", new_data[BEAM_DEF] );
	    yyparse();
	    sym_data->beam_root = beam_hook;

	    break;

        case BUS_TABLE:

	    set_label_type( node, BEAMLINE );

	    if ( !(check_set_context( node, BEAM_ARG )) )
		report_error( "Inconsistent usage", node->name );

	    /* bind beam */
	    sprintf( in_buffer, "beam %s\n\n", new_data[BEAM_DEF] );
	    yyparse();
	    sym_data->beam_root = beam_hook;

	    /* bind level */
	    if ( (sym_data->sub_type = malloc( 2*sizeof(char) )) != (char *)NULL )
		{
		sym_data->sub_type[0] = *new_data[LEVEL_DEF];
		sym_data->sub_type[1] = '\0';
		}
	    else
		{
		fprintf( stderr, "*** dbsf: mem allocation error\n" );
		exit( 1 );
		/* NOTREACHED */
		}

	    break;

	case STRENGTH_TABLE:

	    set_label_type( node, STRENGTH );
	    if ( !(check_set_context( node, EXPR_ARG )) )
		report_error( "Inconsistent usage", node->name );

	    sprintf( in_buffer, "expr %s\n\n", new_data[DEF_STRENGTH] );
	    yyparse();

	    if ( expr_hook )
		sym_data->expr_list = link_attr_node( sym_data->expr_list,
		    expr_hook, ATTR_L );

	    break;

	case GEOMETRY_TABLE:

	    set_label_type( node, GEOMETRY );
	    if ( !(check_set_context( node, EXPR_ARG )) )
		report_error( "Inconsistent usage", node->name );

	    sprintf( in_buffer, "expr %s\n\n", new_data[DEF_GEOMETRY] );
	    yyparse();

	    if ( expr_hook )
		sym_data->expr_list = link_attr_node( sym_data->expr_list,
		    expr_hook, ATTR_L );

	    break;

	default:
	    fprintf( stderr, "*** dbquery : unknown table source found\n" );
	    exit( 1 );
	    /* NOTREACHED */
	    break;

	}
    }

/******************************************************************************/

static int beam( in_name )
char *in_name;
{
  dbcmd(dbproc, "select elements, comment from beam_line");
  dbfcmd(dbproc, " where name = '%s'", in_name);
  
  if (dbsqlexec(dbproc) != FAIL) {
    if (dbresults(dbproc) != FAIL) {
      dbbind(dbproc, 1, STRINGBIND, 0, buff1);
      dbbind(dbproc, 2, STRINGBIND, 0, comment);
      if (dbnextrow(dbproc) == REG_ROW)
	return(3);
    }
  }
  return(0);
}


/**************************************************************************/
/* return the type of the rule and the new name for a given slot if there */
/* is one else NULL */

char **get_rule( in_name, view_string )
char *in_name;
char *view_string;
{
  char **temp = (char **)malloc( 2*sizeof( char *) );

  dbcmd(dbproc, "select type_rule, name, comment from view_rules");
  dbfcmd(dbproc, " where pieces = '%s' and ", in_name);
  dbfcmd(dbproc, " lattice_view like '[%s]'", view_string);

  if (dbsqlexec(dbproc) != FAIL) {
    if (dbresults(dbproc) != FAIL) {
      dbbind(dbproc, 1, STRINGBIND, 0, buff1);
      dbbind(dbproc, 2, STRINGBIND, 0, buff2);
      dbbind(dbproc, 3, STRINGBIND, 0, comment);
      if (dbnextrow(dbproc) == REG_ROW)
      {
	temp[0] = buff1;
	temp[1] = buff2;
        return( temp );
      }
    }
  }
  return( (char **)NULL );
}

/******************************************************************************/

static int bus( in_name )
char *in_name;
{
  dbcmd(dbproc, "select elements, level, comment from bus");
  dbfcmd(dbproc, " where name = '%s'", in_name);
  
  if (dbsqlexec(dbproc) != FAIL) {
    if (dbresults(dbproc) != FAIL) {
      dbbind(dbproc, 1, STRINGBIND, 0, buff1);
      dbbind(dbproc, 2, STRINGBIND, 0, buff2);
      dbbind(dbproc, 3, STRINGBIND, 0, comment);
      if (dbnextrow(dbproc) == REG_ROW)
	return(4);
    }
  }
  return(0);
}

/******************************************************************************/

static int support_point( in_name, fam_name, sp_type )
char *in_name, *fam_name, *sp_type;
{
  dbcmd(dbproc, "select type, system_name, displacement, comment");
  dbcmd(dbproc, " from support_points");

  if ( fam_name == (char *)NULL )
      dbfcmd(dbproc, " where name = '%s'", in_name);
  else
      dbfcmd( dbproc,
	" where name = '%s' and system_name = '%s' and type = '%s'",
	in_name, fam_name, sp_type );
  
  if (dbsqlexec(dbproc) != FAIL) {
    if (dbresults(dbproc) != FAIL) {
      dbbind(dbproc, 1, STRINGBIND, 0, buff1);
      dbbind(dbproc, 2, STRINGBIND, 0, buff2);
      dbbind(dbproc, 3, STRINGBIND, 0, buff3);
      dbbind(dbproc, 4, STRINGBIND, 0, comment);
      if (dbnextrow(dbproc) == REG_ROW)
	return(5);
    }
  }
  return(0);
}

/******************************************************************************/

static int ideal( in_name )
char *in_name;
{
  dbcmd(dbproc, "select pieces, comment from slot");
  dbfcmd(dbproc, " where name = '%s'", in_name);
  
  if (dbsqlexec(dbproc) != FAIL) {
    if (dbresults(dbproc) != FAIL) {
      dbbind(dbproc, 1, STRINGBIND, 0, buff1);
      dbbind(dbproc, 2, STRINGBIND, 0, comment);
      if (dbnextrow(dbproc) == REG_ROW)
	return(3);
    }
  }
  return(0);
}

/******************************************************************************/

static int magnet( in_name )
char *in_name;
{
  dbcmd(dbproc,
   "select type, length_defn, strength_defn, tilt, engineering_type, comment");
  dbcmd(dbproc, " from magnet_piece ");
  dbfcmd(dbproc, "where name = '%s'", in_name);
  
  if (dbsqlexec(dbproc) != FAIL) {
    if (dbresults(dbproc) != FAIL) {
      dbbind(dbproc, 1, STRINGBIND, 0, buff1);
      dbbind(dbproc, 2, STRINGBIND, 0, buff2);
      dbbind(dbproc, 3, STRINGBIND, 0, buff3);
      dbbind(dbproc, 4, STRINGBIND, 0, buff4);
      dbbind(dbproc, 5, STRINGBIND, 0, buff5);
      dbbind(dbproc, 6, STRINGBIND, 0, comment);
      if (dbnextrow(dbproc) == REG_ROW)
	return(7);
    }
  }
  return(0);
}

/******************************************************************************/

/*
static int aperture( index )
char *index;
{
  dbcmd(dbproc, "select geometry, xsize, ysize");
  dbcmd(dbproc, " from aperture ");
  dbfcmd(dbproc, "where aperture_type = '%s'", index);

  if (dbsqlexec(dbproc) != FAIL) {
    if (dbresults(dbproc) != FAIL) {
      dbbind(dbproc, 1, STRINGBIND, 0, attr_buff21);
      dbbind(dbproc, 2, STRINGBIND, 0, attr_buff22);
      dbbind(dbproc, 3, STRINGBIND, 0, attr_buff23);
      if (dbnextrow(dbproc) == REG_ROW)
	return(4);
    }
  }
  return(0);
}
*/

/******************************************************************************/

/*
static int magnet_size( index )
char *index;
{
  dbcmd(dbproc, "select type, xsize, ysize");
  dbcmd(dbproc, " from magnet_size ");
  dbfcmd(dbproc, "where name = '%s'", index);

  if (dbsqlexec(dbproc) != FAIL) {
    if (dbresults(dbproc) != FAIL) {
      dbbind(dbproc, 1, STRINGBIND, 0, attr_buff24);
      dbbind(dbproc, 2, STRINGBIND, 0, attr_buff25);
      dbbind(dbproc, 3, STRINGBIND, 0, attr_buff26);
      if (dbnextrow(dbproc) == REG_ROW)
	return(4);
    }
  }
  return(0);
}
*/

/*******************************************************************/

static int strength( in_name )
char *in_name;
{
  dbfcmd(dbproc, "select definition, comment from %s",str_tbl_used);
  dbfcmd(dbproc, " where name = '%s'", in_name);
  
  if (dbsqlexec(dbproc) != FAIL) {
    if (dbresults(dbproc) != FAIL) {
      dbbind(dbproc, 1, STRINGBIND, 0, buff1);
      dbbind(dbproc, 2, STRINGBIND, 0, comment);
      if (dbnextrow(dbproc) == REG_ROW)
	return(3);
    }
  }
  return(0);
}

/*******************************************************************/
static int geometry( in_name )
char *in_name;
{
  dbcmd(dbproc, "select definition, comment from geometry");
  dbfcmd(dbproc, " where name = '%s'", in_name);

  if (dbsqlexec(dbproc) != FAIL) {
    if (dbresults(dbproc) != FAIL) {
      dbbind(dbproc, 1, STRINGBIND, 0, buff1);
      dbbind(dbproc, 2, STRINGBIND, 0, comment);
      if (dbnextrow(dbproc) == REG_ROW)
	return(3);
    }
  }
  return(0);
}
/************************************************************************/

int err_handler(dbproc, severity, errno, oserr)
     DBPROCESS        *dbproc;
     int              severity;
     int              errno;
     int              oserr;
{
  printf("DB-LIBRARY error:\n\t%s\n", dberrstr(errno));
  
  if (oserr != DBNOERR)
    printf("Operating-system error:\n\t%s\n", dboserrstr(errno));
  
  if ((dbproc == NULL) || (DBDEAD(dbproc)))
    return(INT_EXIT);
  else
    return(INT_CANCEL);
}

/******************************************************************************/

int msg_handler(dbproc, msgno, msgstate, severity, msgtext)
     DBPROCESS        *dbproc;
     int              msgno;
     int              msgstate;
     int              severity;
     char             *msgtext;
{
  if ( severity > 0 )
    printf
      ("DataServer message %d, state %d, severity %d:\n\t%s\n",
       msgno, msgstate, severity, msgtext);
  return(DBNOSAVE);
}

/******************************************************************************/

/* END OF FILE */

.TH DBSF N "16 December 1993" "EPB Version 3.4" "PROGRAMS FOR HIGH ENERGY PHYSICS"
.SH NAME
dbsf - data base lattice to various lattice format conversion
.SH SYNOPSIS
.B dbsf
[
.B -bCdefFgMpPsSt8
] [
.B "-V view_flags"
] [
.B "-T strength_table"
]
.I key
[
.B source_data_base
]
.SH DESCRIPTION
.I dbsf
extracts the definition of
.I key
from the source data base and prints out a Standard Input Format (SIF)
lattice file, a special flat file format, a SYNCH format file, an SDS format
file, a MAGIC format file, a Trace3d format file, or a TRANSPORT format file.
.SH OPTIONS
.TP
.B -b
(SYNCH) directs dbsf to try to parse the strength definition for magnet
gradients and B-rho values to be put in element declaring statements.
For the B-rho value to be extracted the strength definition must be of the
form (expr) / B-rho.  All other forms will cause the default K form to be used.
.TP
.B -C
creates a standard data set (SDS) format file in the directory that dbsf is
executed in.  The file name is constructed from the name of the database
and the key resolved:  <database name>.<key>
.TP
.B -d
(SDS & MAD) is used to add aperture and magnet size information to
the output.
It adds the new attributes TYPEAPER, XAPSIZE, YAPSIZE, XOFFSET and YOFFSET
to the MAD output.  NO magnet size attributes are added to the MAD output.
This information is read from the
.I aperture
and
.I magnet_size
tables of the database being used.  Five values are included to describe the
aperture dimensions.  They are
.I "shape, xsize, ysize, centerx"
and
.I centery.
The
same values are used to describe the magnet dimensions.  All values which are
not found in the database are set to 0.0 with the exception of shape which
gets set to the ASCII representation for space ( 32 decimal ).  The valid
shapes are
.I e
for elliptical,
.I r
for rectangular, and
.I d
for diamond.  The
.I xsize
and
.I ysize
attributes are used to specify the axes of the shape, while the
.I centerx
and
.I centery
attributes are used to specify the offset of the center of the shape
.B "relative to the center of the BEAM."
.TP
.B -e
(SIF) calls for evaluation of all expressions present in the input.  The
default is to print an output file which reflects the full scope of control
variables defined in the input.  (i.e. foo: quad, l = a * b as opposed to
foo: quad, l = 3.234 )
.TP
.B -f
causes a flat file format to be generated.  This format currently consists of
an easy-to-parse arrangement of parameters, elements, and a ring layout,
with 'END' markers terminating each section.
.TP
.B -F
causes a new flat file format to be generated.  This format is similar to the -f
format but also includes extended attribute information and MAD engineering
type values appended to the basic magnet type with a colon separator. i.e.
"drf: drift, l=1, type = dd"
.br
turns into:
.br
drf
.br
drift:dd
.br
1.0
.br
.TP
.B -g
(SIF) makes the output file have three separate sections: variable declarations,
element declarations and line declarations.  This makes the output file much
easier to read, but is unnecessary for files which will just be fed to 
another filter program or MAD.
.TP
.B -M
causes a MAGIC format file to be produced.  This is a flat file format with
one line per element and all expressions evaluated.
.TP
.B -p
causes a TEAPOT format file to be produced.  This file follows MAD 4.03
standards with the addition of aperture size info as described in the -d
option description.  EXCEPTION: aperture info for DRIFTs is omitted even if
there is aperture information for it in the
.I aperture
table of the database.  This is the way of TEAPOT.
.TP
.B -P
causes a file suitable for use with TRANSPORT to be created.  A flat list
of MAD4.03 standard element definitions in beam order is created with all
parameter expressions evaluated.  Therefore, no parameter or beamline
definitions appear in this format.  Also, a title line with database and
beamline names is printed with a date and time stamp.
.TP
.B -s
(SIF) causes the output file to conform to MAD 4.03 standards.  This includes
making all output characters upper-case and removing underscores from names.
Care should be exercised when using this option because tokens like 'BeamPart'
and 'BEAM_PART' will both end up as 'BEAMPART' in the output file.
.TP
.B -S
causes a SYNCH format file to be created which has parameters, elements and
beam lines in groups with expressions evaluated.
.TP
.B -t
causes a trace3d format file to be produced.  This is a flat file format with
one line per element and all expressions evaluated.
.TP
.B "-T strength_table"
allows the designation of an alternate table as the source of strength values.
The default table is `strength'.
.TP
.B "-V view_chars"
is used to specify a list of character keys which activate
.I "view rules"
from the
.I "view_rules"
table of the database being used.  So far only two rules exist.  The first
turns a slot into a drift space with the same length.  The second turns a slot
containing two identical quadrupoles into a single quadrupole with the correct
length and strength.  The format of the
.I view_rules
table includes a new name for the slot ( which may be the same as the old name
), the name of the slot to be modified, a single character key in the
.I lattice_view
field used to activate the rule from the command line, a
.I type_rule
which can currently be either
.I drift
or
.I quad
as described above, and a comment.  The
.I lattice_view
characters should be the same for groups of rules which will be applied
at the same time.
.TP
.B -8
(SIF) causes the output file to conform to MAD 8.1 standards.  Output is made
all caps and underscores removed.  Also, the harmonic number attribute for
rfcavity elements is printed instead of the frequency.  "param_name := value"
is used in place of "PARAM, param_name = value".
.TP
.B key
is the root name to resolve.  It can be a parameter name, beam name or element
name.  In any case, all parameters, element and beams which are included in the
definition of the root name are also resolved and printed.
.TP
.B source_data_base
specifies the Sybase data base where the definition of
.I key
can be found.  By default the value of the environment variable
.B DBSF_DB
is used.
.SH MAPPINGS
This section is included to illustrate and explain some of the less intuitive
data base to output format mappings.
.TP
.B multipoles
are included in the data base with their KnL and Tn multipole moments.
Blank or NULL fields are assumed to be zero.  A `\\' in one of the Tn locations
is meant to designate a skew moment with angle PI/(2n+2).  A `\\' in any field
other than a Tn field is reported as an error by the various output routines.
Standard input format can specify multipoles easily with skew moments
represented by Tn values without numerical assignments. i.e. "mmm: multipole,
k1l = 1, k2l = 2, t1, t2 = .1" where t1 is a skew moment.  SYNCH however can
only designate multipoles with single multipole moments and skew or 0 tilt
values.  This is overcome by declaring an NPOL for each moment with the skew
bit set if it is a skew, or alternately a ROT for arbitrary moment rotations.
The flat format prints the KnL and Tn values as a list of evaluated values.
If the moment is skewed, the value of PI/(2n+2) is evaluated and printed.
MAGIC and Trace3d ignore multipoles.
.TP
.B kicker
elements are kicks in both planes.  This is trivial in SIF as it is an element
type.  However, SYNCH kicks are zero length, single plane elements.  To solve
this, a drift space of length orig_length/2 is declared, followed by a kick
for each plane and another drift space.  These are combined into a beam line
which is named after the original element.
.TP
.B "extended data tables"
have been added to accommodate elements with more attributes than will fit in
the magnet_piece table of the database.  The second function of these tables
is to prevent the overloading of the strength column of the magnet_piece table.
Currently, it is used to hold angles, multipole moments, kicks, frequencies and
electrostatic field attributes.  In any case, if data is found in the extended
tables for a given element, it is used in place of the info in the magnet_piece
table.  In the case of multipoles and kickers, additional info must be included
or an error message is printed.
.SH FILES
.TP
.I dbsflex.l
.I lex
format lexical analyzer for tokenizing data base query results
.TP
.I dbsfparse.y
.I yacc
format parser for building the 'lattice tree' intermediate data format
.TP
.I dbsf_stack.*
definition and support routines for the stack structure which holds the names
of unresolved elements, parameters and lines.
.LP
.I dbquery.c
database query routines
.LP
.I dbsf_do_views.*
traverses the parse tree and does view rule substitutions.
.LP
.I dbsf_main.c
main control and processing routines
.TP
.I lattice_tree.*
definition and support routines for 'lattice tree' intermediate data
representation
.LP
.I lattice_*
various file format output routines
.SH SEE ALSO
.LP
sid, latview, teslis, glish, wireup - SDS and lattice manipulation programs
.LP
Lattice Tree Intermediate Data Representation - Eric Barr @ Superconducting
Supercollider Lab (SSCL)
.LP
Sybase Lattice Representations - Steve Peggs @ FNAL, Gary Trahern @ SSCL, Eric
Barr @ SSCL
.LP
Flat File Lattice Format - Steve Peggs
.LP
MAGIC Program - David Johnson @ SSCL
.LP
SYNCH - A. A. Garren, A. S. Kenney, E. D. Courant, M. J. Syphers, A. D. Russell
.SH AUTHORS
This program has gone through many revisions.
The original data base format specification, flat file format specification,
and data base query routines were written by Steve Peggs.  The parsing
routines, 'lattice tree' intermediate data representation specification,
and output routines were written by
Eric Barr with conceptual and technical guidance from Vern Paxson.
Revisions to the data base and flat output format by Garry Trahern.
Strength table option, trace3d, teapot, transport output formats, and 
endless debugging by Ellen Syphers.
SYNCH, MAGIC, new flat, MAD 8.1 output formats by Eric Barr.
SDS file output routines and copyright notice were added by Chris Saltmarsh.
Extended attribute table support and 'wireup' support added by Eric Barr.
View rule, aperture and magnet size data handling code by Eric Barr with
design help from Ellen Syphers and Garry Trahern.

Comments, complaints, suggestions, gripes, wishes, money, praise, cookies, beer to:
.nf
    barr@fremont.ssc.gov
.fi
.SH DIAGNOSTICS
.LP
.I function not implemented -
there is a function call present in the input data base which is not
currently implemented (it can not be evaluated).
.LP
.I unknown type -
an element has been declared with a type which is not known to the parser.  The
type of 'drift' is substituted for the unknown type.
.LP
.I too many options -
there were extra command line options after the data base name.  The extra 
options are ignored.
.LP
.I mandatory key option missing -
the
.I key
option was missing from the command line so conversion can not take place.
.LP
.I memory allocation error -
this can occur during the allocation of any of the different data structures
for reasons usually beyond the control of the user.  There is not enough
memory to hold the intermediate data representations of a large input file
or the system is experiencing memory management difficulties.
.LP
.I bad char in input ignored -
this is a lexical scanner diagnostic indicating the presence of an invalid
character in the input.
.LP
.I dbuse unsuccessful -
this is normally caused by trying to use a source data base which doesn't
exist.
.LP
.I id not in name_location table -
the
.I key
does not exist in the source data base.
.LP
.I beam transpose operator on element -
there is an unnecessary transpose operator on an element (the transpose of an
element is not different from an element without a transpose).
.LP
.I unresolved argument found -
the print routines found an element, parameter or line which was never defined
by the source data base.
.LP 
.I id used inconsistently -
caused by the use of a parameter in a beam line or by the use of an element or
beam line in an expression.
.SH BUGS
.LP
TEAPOT: I have no idea what an XSEPTUM is.  It is not implemented.
.LP
The user, Gaussian, and random functions are not implemented.
.LP
There are too many errors which generate the message 'syntax error'.
.LP
The code structure could be drastically reworked to have a single parse tree
traversal routine which can be in states corresponding to the type of output
file desired.
.LP
Allowable combinations of command line flags should be documented and have
proper use diagnostics.
.SH COPYRIGHT
.LP
Copyright (c) 1991, 1992, 1993 Universities Research Association, Inc. All
Rights Reserved. DISCLAIMER The software is licensed on an `as is' basis only.
Universities Research Association, Inc. (URA) makes no representations or
warranties, express or implied.  By way of example but not of limitation, URA
makes no representations or WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY
PARTICULAR PURPOSE, that the use of the licensed software will not infringe any
patent, copyright, or trademark, or as to the use (or the results of the use) of
the licensed software or written material in terms of correctness, accuracy,
reliability, currentness or otherwise.  The entire risk as to the results and
performance of the  licensed software is assumed by the user.  Universities 
Research Association, Inc. and any of its trustees, overseers, directors,
officers, employees, agents or contractors shall not be liable under any claim,
charge, or demand, whether in contract, tort, criminal law, or otherwise, for
any and all loss, cost, charge, claim, demand, fee, expense, or damage of every
nature and kind arising out of, connected with, resulting from or sustained as
a result of using this software.  In no event shall URA be liable for special,
direct, indirect or consequential damages, losses, costs, charges, claims,
demands, fees or expenses of any  nature or kind.  This material resulted from 
work developed under a Government Contract and is subject to the following
license:  The Government retains a paid-up, nonexclusive, irrevocable worldwide
license to reproduce, prepare derivative works, perform publicly and display
publicly by or for the Government, including the right to distribute to 
other Government contractors.  Neither the United States nor the United States
Department of Energy, nor any of their employees, makes any warranty, express
or implied, or assumes any legal liability or responsibility for the accuracy,
completeness, or usefulness of any information, apparatus, product, or process
disclosed, or represents that its use would not infringe privately owned rights.
This copyright notice is reprinted with permission from the authors with the
above disclaimers.

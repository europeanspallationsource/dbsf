These are notes for the make of dbsf on SUN (Solaris) and SGI (IRIX)
platforms.


 There are environment variables in the Makefile(s) in the dbsf 
distribution many of which are assumed to be defined in the standard 
environment at login. In particular CC and SYSLIBS are needed. 

 At RHIC/BNL the following definitions apply:

 On SUN:
 CC is gnu gcc
 SYSLIBS is "-lsocket -lnsl -lfl"

 On SGI:
 CC is IRIX cc
 SYSLIBS is undefined in standard setup
  (However, in order to compile dbsf on SGI, one will have to define
   SYSLIBS to be -lnsl -lfl to resolve references.)



/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_transport.c,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lattice_gdecls.h"
#include "lattice_transport.h"
#include "lattice_tree.h"
#include "lattice_defines.h"

#define INDENT		10
#define FORWARDS	1
#define BACKWARDS	0

extern int standard_403;   /* MAD 4.03 standard file */
extern int standard_81;   /* MAD 8.1 standard file */

static void expand_expr( expr, buffer )
exprnode *expr;
char *buffer;
{
    BUF_FORM(FLOAT_FORMAT,eval_expr( expr ))
}

static void print_buffer( buf )
char buf[];
{
    no_under_all_upper( buf ); /* take out underscores and capitalize */
    printf( "%s", buf );
}

/* removed for now eh ? */
#if 0

/* this handles all of the output destined for the transport file */
/* also hamdles chopping buffer into little pieces and putting on '&' */
/* XXX we might not need ( be able to use ) any of this */
static void print_buffer( buf )
char buf[];
{

    static int chop_buf_flag = 0;

    if ( strlen( buf ) > 80 )
    {
        chop_buf_flag = 1;
	chop_buf( buf );
    } else {
	no_under_all_upper( buf );

        if (chop_buf_flag)
	{
            printf( "%s\n", buf );
            chop_buf_flag = 0;
	} else
           printf( "%s ;\n", buf );
    }
}

/* chops some chars off of a buffer, tacks on a &, prints them and the rest */
static void chop_buf( buf )
char *buf;
{
    char work_buf[MAXSTRING];
    register char *tmp = buf + 72;
    int newlen;

    while ( *tmp != ',' && tmp > buf )
	--tmp;

    if ( tmp <= buf )
	{ /* oops, no ',' in the string - settle for any whitespace */
	tmp = buf + 72;
	while ( *tmp != ' ' && tmp > buf )
	    --tmp;
	
	if ( tmp <= buf )
	    {
	    fprintf( stderr, "can't split line \"%s\" in chop_buf()\n", buf );
	    exit( 1 );
	    }
	}

    newlen = tmp - buf + 2;
    sprintf( work_buf, "%.*s &", newlen, buf );
    print_buffer( work_buf );

    tmp += 2;
    while ( *tmp++ == ' ' )
	;

 
    /* add indent to additional lines */
    tmp--;

    for( newlen = INDENT; newlen >= 0; newlen-- )
	*--tmp = ' ';

    /* indent stuff ends here */
    print_buffer( tmp );
}

#endif

static void buff_reg_elem( ste, buffer )
symbol ste;
char *buffer;
    {
    char tmp_buf[MAXSTRING];
    int multipolify = 0;
    char *type_text;

    symbol_node *sym_data = (symbol_node *)(ste->symbol_data);
    attrnode *next = sym_data->expr_list;

    BUF_FORM( "%s:", ste->name );
    BUF_FORM2( "%*c", (int)( INDENT - strlen( ste->name ) ), ' ');

    /* this is where the nasty change from zero length sextupole to multipole */
    /* should go.  I don't like it.  FIX ME */
    if ( sym_data->label_type == ATTR_SEXTUPOLE || sym_data->label_type ==
	ATTR_OCTUPOLE )
	if ( eval_expr(find_attr(ATTR_L,sym_data->expr_list)->expr) == 0.0 )
	    multipolify = 1;

    if ( sym_data->label_type == ATTR_INSTRUMENT )
	type_text = elem_names[ATTR_DRIFT];
    else if ( multipolify )
	type_text = elem_names[ATTR_MULTIPOLE];
    else
	type_text = elem_names[sym_data->label_type];

    BUF_FORM( "%s", type_text );
    
    while ( next )
	{
	if ( next->expr ) if ( next->expr->operation == EXPR_TILT )
	     BUF_FORM( ", %s ", "tilt" ); 

	expand_expr( next->expr, tmp_buf );

	if ( multipolify )
	    {
	    switch ( next->attr ) {

	    case ATTR_L:
		break;

	    case ATTR_K2:
		BUF_FORM( ", %s = ", "k2l" );
		BUF_FORM( "%s", tmp_buf );
		break;

	    case ATTR_K3:
		BUF_FORM( ", %s = ", "k3l" );
		BUF_FORM( "%s", tmp_buf );
		break;

	    case ATTR_TILT:
		BUF_FORM( ", %s = ",
		    (sym_data->label_type == ATTR_SEXTUPOLE ? "t2":"t3") );
		BUF_FORM( "%s", tmp_buf );
		break;

	    default:
		fprintf( stderr, "\n***ERROR: illegal element attribute\n" );
		exit( 1 );
	    }
	} else {
	    if ( (standard_403 && next->attr == ATTR_HARMON) ||
		 (standard_81 && next->attr == ATTR_FREQ) )
	        /* don't print */
		/* both harmon AND freq get printed if they are both present */
		/* and no standards are in effect */
		;
	    else {
		/*
		**
		**  don't print anything on these conditions:
		**    - element is vkick and attribute is kickh
		**    - element is hkick and attribute is kickv
		**    - attribute is tilt
		**    - attribute is aperture or magnet size info
		**  print kick attr if:
		**    - attr is kick and elem is vkick or hkick
		**    - attr is kickv and elem is vkick
		**    - attr is kickh and elem is hkick
		**  otherwise print what you get
		**
		*/

		if ( next->expr->operation != EXPR_TILT
		  && !( next->attr & AP_MS_ATTR_BIT ) )
		{
		    if ( ( sym_data->label_type == ATTR_VKICK &&
			    next->attr == ATTR_KICKV ) ||
			 ( sym_data->label_type == ATTR_HKICK &&
			    next->attr == ATTR_KICKH ) )
		    {
			BUF_FORM( ", %s = ", elem_attr_names[ATTR_KICK] )
			BUF_FORM( "%s", tmp_buf );
		    } else
			if ( ( sym_data->label_type == ATTR_VKICK &&
				    next->attr == ATTR_KICKH ) ||
			     ( sym_data->label_type == ATTR_HKICK &&
				    next->attr == ATTR_KICKV ) )
			{
			    /* NOTHING */
			} else {
			    BUF_FORM( ", %s = ", elem_attr_names[next->attr] );
			    BUF_FORM( "%s", tmp_buf );
			}
		}
	    }
	}

	next = next->next;
    }
	
    if ( sym_data->sub_type )
	if ( sym_data->sub_type != '\0' )
	    BUF_FORM( ", type = %s", sym_data->sub_type );

    BUF_FORM( "%s\n", " ;" );
}

static void buff_multipole( ste, buffer )
symbol ste;
char *buffer;
{
    char tmp_buf[MAXSTRING];

    symbol_node *sym_data = (symbol_node *)(ste->symbol_data);
    attrnode *next = sym_data->expr_list;

    BUF_FORM( "%s:", ste->name );
    BUF_FORM2( "%*c", (int)( INDENT - strlen( ste->name ) ), ' ');
    BUF_FORM( "%s", elem_names[sym_data->label_type] );

    while ( next )
    {
      if ( next->expr ) {
	    if ( next->expr->operation == EXPR_TILT )
	    {
		if ( next->attr <= ATTR_T9 && next->attr >= ATTR_T0 )
		{
		    BUF_FORM( ", %s", multi_attr_names[next->attr] );
		} else {
		    fprintf( stderr, "\n*** dbsf: illegal '\\' in %s %s %s\n",
		      multi_attr_names[next->attr], "attribute of element",
		      ste->name );
		    exit( 1 );
		    /* NOTREACHED */
		}
	    } else {
		if ( !( next->attr & AP_MS_ATTR_BIT ) )
		{
		    expand_expr( next->expr, tmp_buf );
		    BUF_FORM( ", %s = ", multi_attr_names[next->attr] );
		    BUF_FORM( "%s", tmp_buf );
		}
	    }
      }
      
      next = next->next;
    }
	
    if ( sym_data->sub_type )
	if ( sym_data->sub_type != '\0' )
	    BUF_FORM( ", type = %s", sym_data->sub_type );

    BUF_FORM( "%s\n", " ;" );
}

static void flatten_beam( beam, direction )
beamnode *beam;
int direction;
{
    register double repeat = 1.0;
    int newdir = direction;

    if ( beam )
    {
	if ( direction == BACKWARDS )
	    flatten_beam( beam->next, BACKWARDS );

        switch ( beam->operation )
	{
	case BEAM_REPEAT:
	    repeat = eval_expr(beam->iteration);
	    break;

	case BEAM_TRANSPOSE:
	    newdir = ( direction == FORWARDS ? BACKWARDS : FORWARDS );
	    break;

	case BEAM_NO_OP:
	    break;

	default:
	    fprintf( stderr, "\nflatten_beam: bad beam operation\n" );
	    exit( 1 );
	    /*NOTREACHED*/
	}

	for ( ; repeat > 0.0; repeat -= 1.0 )
	    if ( beam->beam_operand )
		flatten_beam( beam->beam_operand, newdir );
	    else
		print_transport( beam->elem_operand );

	if ( direction == FORWARDS )
	    flatten_beam( beam->next, FORWARDS );
    }
}

void print_transport( root )
symbol root;
{
    symbol_node *sym_data;
    char out_buff[4096];

    if ( root )
    {
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /* makes sure it is only printed once */
	    switch( sym_data->label_type )
	    {
	    case ATTR_DRIFT:
	    case ATTR_HMONITOR:
	    case ATTR_VMONITOR:
	    case ATTR_MONITOR:
	    case ATTR_INSTRUMENT:
	    case ATTR_WIGGLER:
	    case ATTR_RBEND:
	    case ATTR_SBEND:
	    case ATTR_QUADRUPOLE:
	    case ATTR_SEXTUPOLE:
	    case ATTR_OCTUPOLE:
	    case ATTR_SOLENOID:
	    case ATTR_EL_SEPARATE:
	    case ATTR_HKICK:
	    case ATTR_VKICK:
	    case ATTR_KICKER:
	    case ATTR_RFCAVITY:
	    case ATTR_ECOLLIMATOR:
	    case ATTR_RCOLLIMATOR:
	    case ATTR_MARKER:
	    case ATTR_SROT:
	    case ATTR_YROT:
		buff_reg_elem( root, out_buff );
		print_buffer( out_buff );
		break;

	    case ATTR_THINLENS:
	    case ATTR_TANK:
	    case ATTR_EDGE:
	    case ATTR_PMQ:
	    case ATTR_RFQCELL:
	    case ATTR_DOUBLET:
	    case ATTR_TRIPLET:
	    case ATTR_RFGAP:
	    case ATTR_SPECIAL:
	    case ATTR_ROTATION:
		print_buffer(
		  "This database contains a trace3d-specific element which\n" );
		print_buffer(
		  "is not valid in MAD.  This run will now terminate.\n" );
		print_buffer(
		  "Use the flat or trace3d option to view this lattice.\n" );
		fprintf( stderr,
		  "This database contains a trace3d-specific element which\n" );
		fprintf( stderr,
		  "is not valid in MAD.  This run will now terminate.\n");
		fprintf( stderr,
		  "Use the flat or trace3d option to view this lattice.\n" );
		exit( 1 );
		break;

	    case ATTR_MULTIPOLE:
		buff_multipole( root, out_buff );
		print_buffer( out_buff );
		break;

	    case GEOMETRY:
	    case STRENGTH:
		break;

	    case BEAMLINE:
	    case SLOT:
		flatten_beam( sym_data->beam_root, FORWARDS );
		break;

	    default:
		fprintf( stderr, "print_transport: bad element type\n" );
		break;
	    }
    }
}

#include <time.h>

extern time_t time();

void print_transport_title(db, key)  
char *db;                
char *key;              
{
    struct tm *systime;
    time_t t;

    t = time(NULL);
    systime = localtime(&t);
			
    printf( "\"database: %s  beamline: %s   %.2d/%.2d/%.2d  %.2d:%.2d\"\n0\n",
	db,key,systime->tm_mon+1,systime->tm_mday,systime->tm_year,
	systime->tm_hour,systime->tm_min);
}

/* END OF FILE */


/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_sds.c,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

/* This is really fun */
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Sds/sdsgen.h"
#include "lattice_defines.h"
#include "lattice_gdecls.h"
#include "lattice_tree.h"
#include "lattice_flat.h"


/* XXX these should be in a header file in the sds code somewhere */
extern int AddToHeap();
extern int NewHeap();
extern void *ConstructHeap();
extern int CurrentNumber();

#include "sds_defines.h"
#include "sds_internal_structs.h"
#include "sds_stypes.h"
#include "lattice_inits.h"

/* These two are not needed if SDS is used. If SDS isn't used, they should
   be derived from legal_type_array */

#ifndef SDS_MAGIC
#define NUMBER_OF_COLUMN_ENTRIES sizeof(row)/sizeof(struct row) -1
#define LEGAL_TYPE_COUNT  sizeof(legal_type)/sizeof(struct legal_type)
#endif

/* Thing to cope with the `level` of each object */

#define N_LEVELS 64
static int  level = N_LEVELS;
static int  min_level = N_LEVELS;
static int *temlevel;

/* the output arrays that will be constructed */

static int  param_num = 0;
static int  param_index = 0;
static struct parameter *pars;

static int  element_num =  0;
static int  element_index =  0;
static struct element *elems;

static int dimopt;
static struct dimensions *dims;

static int  lattice_num =  0;
static int  lattice_index =  0;
static struct lattice *lat;

/* and to hold the lengths of complex objects */

static double linelength[N_LEVELS];
static int preposn[N_LEVELS];

/* and the total beamline length */

/* static double ess = (double)0.0; */

int  Count = true;

int ValHeap;

/* function forward decls */
static void find_beam_param_params();
static void scan_params();
static void find_beam_param_elems();
static void print_elems();
static void buffer_beam();
static void buffer_beam_backwards();
static void load_beam();
static int find_el();

static void count_param( ste )
symbol ste;
{
  symbol_node *sym_data = (symbol_node *)(ste->symbol_data);

  if ( sym_data->declared == DECLARED )
  {
    ++param_num;
  }
  sym_data->declared = COUNTED;
}


static void load_param( ste )
symbol ste;
{
  symbol_node *sym_data = (symbol_node *)(ste->symbol_data);

  if ( sym_data->declared == COUNTED )
  {
    strcpy(pars[param_index].name, ste->name);
    pars[param_index].value = eval_expr(sym_data->expr_list->expr);
	AddToHeap(ValHeap,(char *)&(pars[param_index]),1);

    sym_data->declared = CLEARED;
    param_index++;
  }
}

static void find_beam_params( root )
beamnode *root;
    {
    if ( root )
  {
  if ( root->operation != BEAM_ARGUMENT )
      {
      if ( root->operation == BEAM_CALL_FUNC )
    {
    find_beam_param_params( root->beam_params );
    find_beam_params( ((symbol_node *)(root->elem_operand->symbol_data))->beam_root );
    }
      else
    {
    scan_params( root->elem_operand);
    find_beam_params( root->beam_operand );
    }
      }

  find_beam_params( root->next );
  }
    }

static void find_beam_param_params( root )
beam_param_node *root;
    {
    register beam_param_node *next = root;

    while ( next )
  {
  find_beam_params( next->beam_root );
  next = next->next;
  }
    }

static void scan_params( root)
symbol root;
    {
    symbol_node *sym_data;
    register attrnode *next;

    if ( root )
  {
        sym_data = (symbol_node *)root->symbol_data;

  switch ( sym_data->label_type )
      {
      case ATTR_TCELEMENT:
      case ATTR_DRIFT:
      case ATTR_HMONITOR:
      case ATTR_VMONITOR:
      case ATTR_MONITOR:
      case ATTR_INSTRUMENT:
      case ATTR_WIGGLER:
      case ATTR_RBEND:
      case ATTR_SBEND:
      case ATTR_QUADRUPOLE:
      case ATTR_SEXTUPOLE:
      case ATTR_OCTUPOLE:
      case ATTR_MULTIPOLE:
      case ATTR_SOLENOID:
      case ATTR_RFCAVITY:
      case ATTR_EL_SEPARATE:
      case ATTR_SROT:
      case ATTR_YROT:
      case ATTR_HKICK:
      case ATTR_VKICK:
      case ATTR_KICKER:
      case ATTR_MARKER:
      case ATTR_ECOLLIMATOR:
      case ATTR_RCOLLIMATOR:
      case ATTR_TBEND:
      case ATTR_THINLENS:
      case ATTR_TANK:
      case ATTR_EDGE:
      case ATTR_PMQ:
      case ATTR_RFQCELL:
      case ATTR_DOUBLET:
      case ATTR_TRIPLET:
      case ATTR_RFGAP:
      case ATTR_SPECIAL:
      case ATTR_ROTATION:
    next = sym_data->expr_list;

    while ( next ) {
      if ( next->expr->operation == EXPR_STE ) {
	if (Count) {
          count_param( next->expr->id );
	} else {
          load_param( next->expr->id );
	}
      }

      next = next->next;
    }

    break;

      case GEOMETRY:
      case STRENGTH:
    break;

      case BEAMLINE:
      case SLOT:
      case PARAM_LINE:
    find_beam_params( sym_data->beam_root );
    break;
      }
  }
    }


/* ELEMENT BUFFERING ROUTINES */

static int search_pars(name)
char *name;
{
  struct parameter *p = pars;
  int count;
  for (count = 0;count < param_num; count++, p++)
  if (!strcmp(name,p->name))
    return count;
  return -1;
}

static int find_arg( ste, attrnum, multi, retval, parno )
symbol ste;
int attrnum;
int multi;
double *retval;
int *parno;
{
  symbol_node *sym_data;
  register attrnode *exprlist =((symbol_node *)(ste->symbol_data))->expr_list;
  *parno = -1;

  sym_data = (symbol_node *)ste->symbol_data;

  while ( exprlist )
  {
    if ( exprlist->attr == attrnum )
    {
      if ( exprlist->expr->operation == EXPR_STE )
      {
        *parno = search_pars(exprlist->expr->id->name);

	if (*parno != -1)
          *retval = pars[*parno].value;

	return 1;
      } else {
	if ( exprlist->expr->operation == EXPR_TILT )
	{
	  if ( multi && attrnum <= ATTR_T9 && attrnum >= ATTR_T0 )
	  {
	    *retval = ( PI/(2*( attrnum - ATTR_T0)+2) );
	    return 1;
	  } else {
	    if ( sym_data->label_type == ATTR_RBEND || sym_data->label_type ==
	      ATTR_SBEND)
	      *retval = (PI/2) ;   

	    if (sym_data->label_type == ATTR_QUADRUPOLE)
	      *retval = (PI/4) ;   

	    if (sym_data->label_type == ATTR_SEXTUPOLE)
	      *retval = (PI/6) ;   

	    if (sym_data->label_type == ATTR_OCTUPOLE)
	      *retval = (PI/8) ;

	    return 1;   
	  }
	} else {
	  *retval =  eval_expr( exprlist->expr );
	  return 1;
	}
      }
    } else {
      exprlist = exprlist->next;
    }
  }

  return 0;
}


static void print_beamline( ste )
symbol ste;
{
  symbol_node *sym_data = (symbol_node *)(ste->symbol_data);
  int index;

  if (Count)
  {
    if (sym_data->declared == DECLARED)
    {
      element_num++;
    }
    sym_data->declared = COUNTED;
  }
  else
  {
    if (sym_data->declared == COUNTED)
    {
      index = sym_data->label_type;

      if (index == BEAMLINE)
	  {
        index = 
        elems[element_index].type_index = ATTR_BEAMLINE;
	  }
      else
	  {
        index = 
        elems[element_index].type_index = ATTR_SLOT;
	  }
      strcpy(elems[element_index].type, elem_names[index]);
      strcpy(elems[element_index].name, ste->name);
      if (sym_data->sub_type)
          if (*(sym_data->sub_type) != '\0' )
          strcpy(elems[element_index].sub_type, sym_data->sub_type);
      element_index++;
    }
    sym_data->declared = DECLARED;
  }
}

/* this find all of the available aperture and magnet_size info for a */
/* given element and fills in the dimensions data for it if there is any */
/* assumes global variables element_index and dims */
static void do_dims( attrs )
attrnode *attrs;
{
    attrnode *attr_info;

    /* search for each of the attributes */

    if ( (attr_info = find_attr( ATTR_AP_SHAPE, attrs )) != (attrnode *)NULL )
	dims[element_index].ashape = (char)(attr_info->expr->funcname);
    else
	dims[element_index].ashape = SHAPE_UNKNOWN;
	
    if ( (attr_info = find_attr( ATTR_AP_XSIZE, attrs )) != (attrnode *)NULL )
	dims[element_index].axsize = eval_expr( attr_info->expr );

    if ( (attr_info = find_attr( ATTR_AP_YSIZE, attrs )) != (attrnode *)NULL )
	dims[element_index].aysize = eval_expr( attr_info->expr );

    if ( (attr_info = find_attr( ATTR_AP_CENTERX, attrs )) != (attrnode *)NULL )
	dims[element_index].acenterx = eval_expr( attr_info->expr );

    if ( (attr_info = find_attr( ATTR_AP_CENTERY, attrs )) != (attrnode *)NULL )
	dims[element_index].acentery = eval_expr( attr_info->expr );

    if ( (attr_info = find_attr( ATTR_MS_SHAPE, attrs )) != (attrnode *)NULL )
	dims[element_index].mshape = (char)(attr_info->expr->funcname);
    else
	dims[element_index].mshape = SHAPE_UNKNOWN;

    if ( (attr_info = find_attr( ATTR_MS_XSIZE, attrs )) != (attrnode *)NULL )
	dims[element_index].mxsize = eval_expr( attr_info->expr );

    if ( (attr_info = find_attr( ATTR_MS_YSIZE, attrs )) != (attrnode *)NULL )
	dims[element_index].mysize = eval_expr( attr_info->expr );

    if ( (attr_info = find_attr( ATTR_MS_CENTERX, attrs )) != (attrnode *)NULL )
	dims[element_index].mcenterx = eval_expr( attr_info->expr );

    if ( (attr_info = find_attr( ATTR_MS_CENTERY, attrs )) != (attrnode *)NULL )
	dims[element_index].mcentery = eval_expr( attr_info->expr );
}

static void print_reg_elem( ste , multi )
symbol ste;
int multi;
{
  register int count, index;
  int attrnum, pass;
  int more = 1;
  struct parameter *par;
  int parcount, parno, morecount, idnex;
  int gotmore = 0;

  symbol_node *sym_data = (symbol_node *)(ste->symbol_data);

  if ( Count )
  {
    if (sym_data->declared == DECLARED)
      element_num++;

    sym_data->declared = COUNTED;
  } else {
    if ( sym_data->declared == COUNTED && dimopt )
    {
	sym_data->declared = DECLARED;
        do_dims( sym_data->expr_list );
    }

    index = elems[element_index].type_index = sym_data->label_type;
    strcpy(elems[element_index].name, ste->name);
    strcpy(elems[element_index].type, elem_names[index]);

    if (sym_data->sub_type)
      if (*(sym_data->sub_type) != '\0' )
        strcpy(elems[element_index].sub_type, sym_data->sub_type);

    count = -1;
    parcount = 0;
    pass = 1;
    morecount = 0;

    while ( (attrnum = elem_arg_forms[index][++count]) && more )
    {
      if (attrnum == A_DELIM )
      {
	if ( !multi && small_bend( ste ) )
	  more = 0;
	else {
	  gotmore = 1;
	  morecount = elem_arg_forms[index][++count];
	}
      } else {
	double tempval;

	if ( !find_arg( ste, attrnum, multi, &tempval, &parno ) )
	  tempval = ( multi ? 0.0 : atof(elem_attr_defaults[attrnum]) );

	if (parno == -1 || morecount)
	{
	  par = (struct parameter *)malloc(sizeof(struct parameter));

	  /* if multi use multi_attr_names header  */
	  if (multi)
	    strcpy(par->name,multi_attr_names[attrnum]);
	  else
	    strcpy(par->name,elem_attr_names[attrnum]);

	  par->value = tempval;
	  idnex = AddToHeap(ValHeap,(char *)par, 1);
	} else {
	  /* extra 1 because I add junkl param at the start of the list....*/
	  idnex = parno + 1;
	  par = &(pars[parno]);
	}

	if ( gotmore )
	{
	  elems[element_index].more_index = idnex;
	  gotmore = 0;
	}

	switch (attrnum) {
	case ATTR_L:
	  if ( !(multi) ) {
	     elems[element_index].length = par->value;
	     elems[element_index].length_index = idnex;
	  }
	  break;

	case ATTR_TILT:
	  elems[element_index].tilt = par->value;
	  elems[element_index].tilt_index = idnex;
	  break;

	case ATTR_ANGLE:
	  if ( index == ATTR_RBEND || index == ATTR_SBEND || index == ATTR_SROT
	    || index == ATTR_YROT || index == ATTR_ROTATION )
	  {
	    elems[element_index].strength = par->value;
	    elems[element_index].strength_index = idnex;
	  }
	  break;

	case ATTR_K1:
	  if ( index==ATTR_QUADRUPOLE || index==ATTR_PMQ || index==ATTR_DOUBLET ) 
	  {
	    elems[element_index].strength = par->value;
	    elems[element_index].strength_index = idnex;
	  }
	  break;

	case ATTR_K2:
	  if ( index == ATTR_SEXTUPOLE ) 
	       {
		elems[element_index].strength = par->value;
		elems[element_index].strength_index = idnex;
	       }
	  break;

	case ATTR_K3:
	  if ( index == ATTR_OCTUPOLE ) 
	       {
		elems[element_index].strength = par->value;
		elems[element_index].strength_index = idnex;
	       }
	  break;

	case ATTR_KS:
	  if ( index == ATTR_SOLENOID ) 
	       {
		elems[element_index].strength = par->value;
		elems[element_index].strength_index = idnex;
	       }
	  break;

	case ATTR_FREQ:
	  if ( index == ATTR_RFCAVITY ) 
	       {
		elems[element_index].strength = par->value;
		elems[element_index].strength_index = idnex;
	       }
	  break;

	case ATTR_E:
	  if ( index == ATTR_EL_SEPARATE ) 
	       {
		elems[element_index].strength = par->value;
		elems[element_index].strength_index = idnex;
	       }
	  break;

	case ATTR_KICK:
	  if ( index == ATTR_HKICK || index == ATTR_VKICK ) 
	       {
		elems[element_index].strength = par->value;
		elems[element_index].strength_index = idnex;
	       }
	  break;

	default:
	  break;
	}

	parcount++;
      }
    }

    elems[element_index].npars = morecount;
    element_index++;
    sym_data->declared = CLEARED;
  }
}

static void find_beam_elems( root )
beamnode *root;
    {
    if ( root )
  {
  if ( root->operation != BEAM_ARGUMENT )
      {
      if ( root->operation == BEAM_CALL_FUNC )
    {
    find_beam_param_elems( root->beam_params );
    find_beam_elems( ((symbol_node *)(root->elem_operand->symbol_data))->beam_root );
    }
      else
    {
    print_elems( root->elem_operand );
    find_beam_elems( root->beam_operand );
    }
      }

  find_beam_elems( root->next );
  }
    }

static void find_beam_param_elems( root )
beam_param_node *root;
    {
    register beam_param_node *next = root;

    while ( next )
  {
  find_beam_elems( next->beam_root );
  next = next->next;
  }
    }

static void print_elems( root )
symbol root;
{
    symbol_node *sym_data;

    if ( root )
    {
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) 
	{
	    switch ( sym_data->label_type ) {
     case ATTR_TCELEMENT:
	    case ATTR_DRIFT:
	    case ATTR_HMONITOR:
	    case ATTR_VMONITOR:
	    case ATTR_MONITOR:
	    case ATTR_INSTRUMENT:
	    case ATTR_WIGGLER:
	    case ATTR_RBEND:
	    case ATTR_SBEND:
	    case ATTR_QUADRUPOLE:
	    case ATTR_SEXTUPOLE:
	    case ATTR_OCTUPOLE:
	    case ATTR_SOLENOID:
	    case ATTR_RFCAVITY:
	    case ATTR_EL_SEPARATE:
	    case ATTR_SROT:
	    case ATTR_YROT:
	    case ATTR_HKICK:
	    case ATTR_VKICK:
	    case ATTR_KICKER:
	    case ATTR_MARKER:
	    case ATTR_ECOLLIMATOR:
	    case ATTR_RCOLLIMATOR:
	     case ATTR_TBEND:
	     case ATTR_THINLENS:
	     case ATTR_TANK:
	     case ATTR_EDGE:
	     case ATTR_PMQ:
	     case ATTR_RFQCELL:
	     case ATTR_DOUBLET:
	     case ATTR_TRIPLET:
	     case ATTR_RFGAP:
	     case ATTR_SPECIAL:
	     case ATTR_ROTATION:   
	      print_reg_elem( root, 0 );
	      break;
		
	    case ATTR_MULTIPOLE:
	      print_reg_elem( root, 1 );
	      break;

	    case GEOMETRY:
	    case STRENGTH:
	      break;

	    case BEAMLINE:
	    case SLOT:
	      print_beamline( root ); 
	    case PARAM_LINE:
	      find_beam_elems( sym_data->beam_root );
	      break;
	    }
	}
    }
}


/* BEAM BUFFERING ROUTINES */

static beamnode *get_arg( num, args )
double num;
beam_param_node *args;
    {
    while ( --num > 0 )
  if ( args )
      args = args->next;
  else
      {
      fprintf( stderr, "Flat Print: not enough arguments to sub_line\n");
      num = 0.0;
      }

    if ( args )
        return ( args->beam_root );
    else
  {
  fprintf( stderr, "Flat Print: not enough arguments to sub_line\n");
  return (beamnode *)NULL;
  }
    }

static void insert_args( map, args )
beamnode *map;
beam_param_node *args;
    {
    register beam_param_node *next;

    while ( map )
  {
  if ( map->operation == BEAM_ARGUMENT )
      map->beam_operand = get_arg( eval_expr( map->iteration ), args );
  else
      {
      if ( map->operation == BEAM_CALL_FUNC )
    {
    next = map->beam_params;

    while ( next )
        {
        insert_args( next->beam_root, args );
        next = next->next;
        }
    }
      else
    {
    insert_args( map->beam_operand, args );
    if ( map->elem_operand )
        insert_args( ((symbol_node *)(map->elem_operand->symbol_data))->beam_root, args );
    }
      }
  map = map->next;
  }
    }

static void remove_args( map )
beamnode *map;
    {
    register beam_param_node *next;

    while ( map )
  {
  if ( map->operation == BEAM_ARGUMENT )
      map->beam_operand = (beamnode *)NULL;
  else
      {
      if ( map->operation == BEAM_CALL_FUNC )
    {
    next = map->beam_params;

    while ( next )
        {
        remove_args( next->beam_root );
        next = next->next;
        }
    }
      else
    {
    remove_args( map->beam_operand );
    if ( map->elem_operand )
        remove_args( ((symbol_node *)(map->elem_operand->symbol_data))->beam_root );
    }
      }
  map = map->next;
  }
    }


static void do_beam_call( map, args, buffer, transpose )
beamnode *map;
beam_param_node *args;
char *buffer;
int transpose;
    {
    insert_args( map, args );

    if ( transpose )
  buffer_beam_backwards( map, buffer );
    else
  buffer_beam( map, buffer );

    remove_args( map );
    }

static void buffer_beam( beam, buffer )
beamnode *beam;
char *buffer;
{
  register double repeat;
  symbol_node *sym_data;

  while ( beam )
  {
    switch (beam->operation)
    {
      case BEAM_NO_OP:
        load_beam(beam, buffer, forward);
        break;

      case BEAM_REPEAT:
        for ( repeat = eval_expr( beam->iteration ); repeat > 0; repeat -= 1 )
        load_beam(beam, buffer, forward);
        break;

      case BEAM_TRANSPOSE:
        load_beam(beam, buffer, backward);
        break;

      case BEAM_CALL_FUNC:
        sym_data = (symbol_node *)(beam->elem_operand->symbol_data);
        do_beam_call( sym_data->beam_root, beam->beam_params, buffer,0);
        break;

      case BEAM_ARGUMENT:
        load_beam(beam, buffer, forward);
        break;

      default:
        fprintf( stderr, "\nbuffer_beam: bad beam operation %d\n", beam->operation );
        exit( 1 );
        /*NOTREACHED*/
    }
    beam = beam->next;
  }
}

static void buffer_beam_backwards( beam, buffer )
beamnode *beam;
char *buffer;
{
  register double repeat;
  symbol_node *sym_data;

  if ( beam )
  {
    buffer_beam_backwards( beam->next, buffer );

    switch (beam->operation)
    {
      case BEAM_REPEAT:
        for ( repeat = eval_expr( beam->iteration ); repeat > 0; repeat -= 1 )
          load_beam(beam, buffer, backward);
        break;

      case BEAM_TRANSPOSE:
        load_beam(beam, buffer, forward);
        break;

      case BEAM_NO_OP:
        load_beam(beam, buffer, backward);
        break;

      case BEAM_CALL_FUNC:
        sym_data = (symbol_node *)(beam->elem_operand->symbol_data);
        do_beam_call( sym_data->beam_root, beam->beam_params, buffer,1);
        break;

      case BEAM_ARGUMENT:
        load_beam(beam, buffer, backward);
        break;

      default:
        fprintf( stderr, "\nbuffer_beam: bad beam operation\n" );
        exit( 1 );
        /*NOTREACHED*/
    }
  }
}

static void print_beam( root )
symbol root;
{
  symbol_node *sym_data;
  char buffer1[MAXSTRING];
  char *buffer = buffer1;
  int index;

  if ( root )
  {
    if (Count)
      lattice_num++;
    else
    {
      preposn[level-1] = lattice_index;
      linelength[level-1] = (double)0.0;

      lat[lattice_index].level = level;
      index = lat[lattice_index].element_index = 
               find_el(root->name);
      lat[lattice_index].type_index = elems[index].type_index;

      if (temlevel[lattice_index] < level)
        temlevel[lattice_index] = level;

      lattice_index++;
    }
    sym_data = (symbol_node *)root->symbol_data;

    if ( sym_data->declared ) /*this makes sure it is only printed once*/
    {
      switch( sym_data->label_type )
      {
        case ATTR_TCELEMENT:
        case ATTR_DRIFT:
        case ATTR_HMONITOR:
        case ATTR_VMONITOR:
        case ATTR_MONITOR:
        case ATTR_INSTRUMENT:
        case ATTR_WIGGLER:

        case ATTR_RBEND:
        case ATTR_SBEND:
        case ATTR_QUADRUPOLE:
        case ATTR_SEXTUPOLE:
        case ATTR_OCTUPOLE:
        case ATTR_SOLENOID:
        case ATTR_EL_SEPARATE:
        case ATTR_HKICK:
        case ATTR_VKICK:
        case ATTR_KICKER:
       
        case ATTR_RFCAVITY:
        case ATTR_ECOLLIMATOR:
        case ATTR_RCOLLIMATOR:

        case ATTR_MARKER:

        case ATTR_SROT:
        case ATTR_YROT:

            case ATTR_TBEND:
            case ATTR_THINLENS:
            case ATTR_TANK:
            case ATTR_EDGE:
            case ATTR_PMQ:
            case ATTR_RFQCELL:
            case ATTR_DOUBLET:
            case ATTR_TRIPLET:
            case ATTR_RFGAP:
            case ATTR_SPECIAL:
            case ATTR_ROTATION:

        case ATTR_MULTIPOLE:

        case GEOMETRY:
        case STRENGTH:
        case PARAM_LINE:
          break;

        case BEAMLINE:
        case SLOT:
          buffer_beam( sym_data->beam_root, buffer );
          if (!Count)
          {
            elems[lat[preposn[level-1]].element_index].length = 
                linelength[level-1];
            lat[preposn[level-1]].sense = (short)forward; 
          }
          break;

        default:
          fprintf( stderr, "something is amiss in print_beam\n" );

      }
      sym_data->declared = Count;
    }
  }
}

static void load_beam(beam, buffer,  sense)
beamnode *beam;
char  *buffer;
int  sense;
{

  symbol_node *sym_data;
  int index;
  int i;

  if ( beam->beam_operand )
  {
    if (sense == forward)
      buffer_beam( beam->beam_operand, buffer );
    else
      buffer_beam_backwards( beam->beam_operand, buffer );
  }
  else if ((beam->operation != BEAM_ARGUMENT) || ( beam->elem_operand) )
  {
    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);
    if ( sym_data->label_type == BEAMLINE ||
         sym_data->label_type == SLOT )
    {
      level--;
      if (level < min_level)
        min_level = level;
      if (Count)
      {
        lattice_num++;
      }
      else
      {
        preposn[level-1] = lattice_index;
        linelength[level-1] = (double)0.0;

        lat[lattice_index].level = level;
        index = lat[lattice_index].element_index = 
                 find_el(beam->elem_operand->name);
        lat[lattice_index].s = linelength[N_LEVELS - 1];

        if (temlevel[lattice_index] < level)
          temlevel[lattice_index] = level; 

        lat[lattice_index].type_index = elems[index].type_index;
        lattice_index++;
      }
      if (sense == forward)
        buffer_beam( sym_data->beam_root, buffer );
      else
        buffer_beam_backwards( sym_data->beam_root, buffer );
      if (!Count)
      {
        elems[lat[preposn[level-1]].element_index].length = 
            linelength[level-1];
        lat[preposn[level-1]].sense = (short)sense; 
      }
      level++;
    }
    else
    {
      if (Count)
        lattice_num++;
      else
      {
    index = lat[lattice_index].element_index = 
               find_el(beam->elem_operand->name);
    lat[lattice_index].s = linelength[N_LEVELS - 1];
    for (i=min_level-1; i < N_LEVELS; i++)
      linelength[i] += elems[index].length;
    lat[lattice_index].type_index = elems[index].type_index;
        lattice_index++;
      }
    }
  }
  else
  {
    fprintf( stderr, "buffer_beam: unresolved argument found\n" );
    exit( 1 );
  }
}


static int find_el(elname)
char  *elname;
{
  int elcount;
  for (elcount = 0; elcount < element_num; elcount++)
    if (!strcmp(elname,elems[elcount].name))
    {
      return elcount;
    }
  fprintf(stderr,"Failed to find element %s\n",elname);
  return -1;
}

static void scan_beam()
{
  int posn;
  int *occur = (int *)calloc(element_num,sizeof(int));

  for( posn = 0; posn < element_num; posn++ )
      occur[posn] = 0;

  min_level -= 3;/*  So SLOT_LEVEL and SUPERSLOT_LEVEL are free */

  for (posn = 0; posn < lattice_num; posn++)
  {
    if (temlevel[posn] != 0)
      temlevel[posn] -= min_level;

    if (elems[lat[posn].element_index].type_index == ATTR_SLOT)
      temlevel[posn] = LATTICE_SLOT_LEVEL;

    if (elems[lat[posn].element_index].type_index == ATTR_SUPERSLOT)
      temlevel[posn] = LATTICE_SUPERSLOT_LEVEL;

    lat[posn].level = temlevel[posn];
    lat[posn].occurence = occur[lat[posn].element_index];
    occur[lat[posn].element_index]++;
  }
}


/* MAIN CONTROL ROUTINE */

void print_sds( root, dbase, beamline, dim_info )
symbol root;
char  *dbase;
char  *beamline;
int dim_info;
{
  int  sds;
  int  par_code;
  int  elem_code;
  int  dim_code;
  int  lat_code;
  int  leg_code;
  int  head_code;
  int  row_code;
  char filename[128];

  struct parameter dummy_par;

  strcpy(filename,dbase);
  strcat(filename,".");
  strcat(filename,beamline);

  dimopt = dim_info;

  sds_init();
  sds = sds_new(filename);

  par_code = sds_define_structure(sds,p_tlist,p_names);
  elem_code = sds_define_structure(sds,e_tlist,e_names);
  lat_code = sds_define_structure(sds,l_tlist,l_names);
  leg_code = sds_define_structure(sds,legal_tlist,legal_names);
  head_code = sds_define_structure(sds,head_tlist,head_names);
  row_tlist[ COLUMN_TYPE_CODE_PLACE].elemcod = head_code;
  row_code = sds_define_structure(sds,row_tlist,row_names);

  if ( dimopt )
      dim_code = sds_define_structure(sds,dimensions_tlist,dimensions_names);

  ValHeap = NewHeap(sizeof(struct parameter));
  dummy_par.value = 0.0;
  strcpy(dummy_par.name,"Nothing");
  AddToHeap(ValHeap,(char *)&dummy_par,1);

  Count = true;
  scan_params( root );
  pars = (struct parameter *)calloc(param_num, sizeof(struct parameter));
  Count = false;
  scan_params( root );

  Count = true;
  print_elems( root );
  elems = (struct element *)calloc(element_num, sizeof(struct element));
  dims = (struct dimensions *)calloc(element_num, sizeof(struct dimensions));
  Count = false;
  print_elems( root );

  Count = true;
  print_beam( root );
  lat = (struct lattice *)calloc(lattice_num,sizeof(struct lattice));
  temlevel = (int *)calloc(lattice_num, sizeof(int));
  Count = false;
  print_beam( root );
  scan_beam();


  sds_declare_structure(sds,(char *) lat,"lattice",(long)lattice_num,lat_code);
  sds_declare_structure(sds,(char *) elems,"element",(long)element_num,elem_code);

  if ( dimopt )
      sds_declare_structure( sds, (char *)dims, "dimensions", (long)element_num,
	dim_code );

  sds_declare_structure(sds,(char *) ConstructHeap(ValHeap),"parameter",
    (long)CurrentNumber(ValHeap),par_code);

#define MLT sizeof(legal_type)/sizeof(struct legal_type)
  sds_declare_structure(sds,(char *) legal_type, "legal_type",(long)MLT,leg_code);

#define NCE sizeof(row)/sizeof(struct row)
  sds_declare_structure(sds,(char *) row, "row",(long)NCE,row_code);

  sds_ass(sds, filename, SDS_FILE);
}


/* END OF FILE */


/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_flat.c,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lattice_flat.h"
#include "lattice_gdecls.h"
#include "lattice_tree.h"
#include "lattice_defines.h"

#define BUF_FORM1(x,y)	{ sprintf( buffer, x, y ); }
#define F_FLOAT_FORMAT	"%.14f"

int old_flat;  /* designates that the old flat file format should be used */

/* function forward decls */
static void find_beam_param_params();
static void print_paramsf();
static void find_beam_param_elems();
static void print_elems();
static void buffer_beam();
static void buffer_beam_backwards();

/* OUTPUT ROUTINE */

static void print_buffer( buf )
char buf[];
    {
    /* this statement does all of the printing for the output file */
    printf( "%s\n", buf );
    }


/* PARAMETER BUFFERING ROUTINES */

/* evaluates expression and returns ascii answer in buffer */
static char *buffer_exprf( expr, buffer )
exprnode *expr;
char *buffer;
    {
    BUF_FORM1(F_FLOAT_FORMAT,eval_expr( expr ))
    return ( buffer );
    }

static void print_param( symbol ste )
    {
    static int param_num = 0;
    char buffer1[MAXSTRING];
    char *buffer = buffer1;
    symbol_node *sym_data = (symbol_node *)(ste->symbol_data);

    if ( sym_data->declared )
	{
	BUF_FORM1("%s", ste->name );
	print_buffer( buffer );

	buffer_exprf( sym_data->expr_list->expr, buffer );
	print_buffer( buffer );

	++param_num;
	sym_data->declared = 0;
	}
    }

static void find_beam_params( root )
beamnode *root;
{
    if ( root )
    {
	if ( root->operation != BEAM_ARGUMENT )
	{
	    if ( root->operation == BEAM_CALL_FUNC )
	    {
		find_beam_param_params( root->beam_params );
		find_beam_params( ((symbol_node *)(root->elem_operand->symbol_data))->beam_root );
	    } else {
		print_paramsf( root->elem_operand );
		find_beam_params( root->beam_operand );
	    }
	}

    find_beam_params( root->next );
    }
}

static void find_beam_param_params( root )
beam_param_node *root;
    {
    register beam_param_node *next = root;

    while ( next )
	{
	find_beam_params( next->beam_root );
	next = next->next;
	}
    }

static void print_paramsf( root )
symbol root;
    {
    symbol_node *sym_data;
    register attrnode *next;

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	switch ( sym_data->label_type )
	    {
	    case ATTR_DRIFT:
	    case ATTR_HMONITOR:
	    case ATTR_VMONITOR:
	    case ATTR_MONITOR:
	    case ATTR_INSTRUMENT:
	    case ATTR_WIGGLER:
	    case ATTR_RBEND:
	    case ATTR_SBEND:
	    case ATTR_QUADRUPOLE:
	    case ATTR_SEXTUPOLE:
	    case ATTR_OCTUPOLE:
	    case ATTR_MULTIPOLE:
	    case ATTR_SOLENOID:
	    case ATTR_RFCAVITY:
	    case ATTR_EL_SEPARATE:
	    case ATTR_SROT:
	    case ATTR_YROT:
	    case ATTR_HKICK:
	    case ATTR_VKICK:
	    case ATTR_KICKER:
	    case ATTR_MARKER:
	    case ATTR_ECOLLIMATOR:
	    case ATTR_RCOLLIMATOR:
            case ATTR_TBEND:
            case ATTR_THINLENS:
            case ATTR_TANK:
            case ATTR_EDGE:
            case ATTR_PMQ:
            case ATTR_RFQCELL:
            case ATTR_DOUBLET:
            case ATTR_TRIPLET:
            case ATTR_RFGAP:
            case ATTR_SPECIAL:
            case ATTR_ROTATION:
            case ATTR_NCELLS:
            case ATTR_DTL_CELL:
		next = sym_data->expr_list;

		while ( next )
		    {
                    if ( next->expr != NULL )
		      if ( next->expr->operation == EXPR_STE )
			if ( !(next->attr == ATTR_TILT && old_flat) )
			  print_param( next->expr->id );

		    next = next->next;
		    }

		break;

	    case GEOMETRY:
	    case STRENGTH:
	    case ATTR_COMMAND:
		break;

	    case BEAMLINE:
	    case SLOT:
	    case PARAM_LINE:
		find_beam_params( sym_data->beam_root );
		break;
	    }
	}
    }


/* ELEMENT BUFFERING ROUTINES */

int find_output_attr( ste, attrnum, multi )
symbol ste;
int attrnum;
int multi;
    {
    register attrnode *exprlist =((symbol_node *)(ste->symbol_data))->expr_list;
    char buffer1[MAXSTRING];
    char *buffer = buffer1;
    symbol_node *sym_data;

    sym_data = (symbol_node *)ste->symbol_data;

    while ( exprlist )
	if ( exprlist->attr == attrnum )
	    {
	    if ( exprlist->expr->operation == EXPR_STE )
		if ( attrnum == ATTR_TILT && old_flat )
		{
		    BUF_FORM1(F_FLOAT_FORMAT,eval_expr( ((symbol_node *)(exprlist->expr->id->symbol_data))->expr_list->expr ));
		} else {
		    BUF_FORM1("%s",exprlist->expr->id->name)
		}
	    else
		if ( exprlist->expr->operation == EXPR_TILT )
		    if ( multi && attrnum <= ATTR_T9 && attrnum >= ATTR_T0 )
			{
			BUF_FORM1(F_FLOAT_FORMAT, ( PI/(2*( attrnum - ATTR_T0 )+2) ) )
			}
		    else
			{
                        if (sym_data->label_type == ATTR_RBEND 
                              || sym_data->label_type == ATTR_SBEND)
	                   BUF_FORM1(F_FLOAT_FORMAT, (PI/2)  );   
                        if (sym_data->label_type == ATTR_QUADRUPOLE)
	                   BUF_FORM1(F_FLOAT_FORMAT, (PI/4)  );   
                        if (sym_data->label_type == ATTR_SEXTUPOLE)
	                   BUF_FORM1(F_FLOAT_FORMAT, (PI/6)  );   
                        if (sym_data->label_type == ATTR_OCTUPOLE)
	                   BUF_FORM1(F_FLOAT_FORMAT, (PI/8)  );   
	/*	fprintf( stderr, "\n*** dbsf: illegal '\\' in attribute value field\n" );  */
	/*	exit( 1 );                                                                 */
			/* NOTREACHED */
			}
		else
		    BUF_FORM1(F_FLOAT_FORMAT,eval_expr( exprlist->expr ))

	    print_buffer( buffer );
	    return ( 1 );
	    }
	else
	    exprlist = exprlist->next;

    return( 0 );
    }

/* this determines if the element is a bend without extended attributes */
int small_bend( ste )
symbol ste;
    {
    symbol_node *sym_data = (symbol_node *)(ste->symbol_data);
    register attrnode *exprlist =((symbol_node *)(ste->symbol_data))->expr_list;

    if ( sym_data->label_type==ATTR_SBEND || sym_data->label_type==ATTR_RBEND )
	{
	while ( exprlist )
	    if ( exprlist->attr == ATTR_K1 || exprlist->attr == ATTR_K2 ||
		 exprlist->attr == ATTR_K3 || exprlist->attr == ATTR_E1 ||
		 exprlist->attr == ATTR_E2 || exprlist->attr == ATTR_FINT || 
		 exprlist->attr == ATTR_HGAP || exprlist->attr == ATTR_H1 ||
		 exprlist->attr == ATTR_H2 )
		return( 0 );
	    else
		exprlist = exprlist->next;

        return( 1 ); /* no extended attrs found */
	}
        
    return( 0 );
    }

static void print_reg_elem( ste )
symbol ste;
    {
    char buffer1[MAXSTRING];
    char *buffer = buffer1;
    register int count;
    register int index;
    int attrnum;
    int more = 1;

    symbol_node *sym_data = (symbol_node *)(ste->symbol_data);

    BUF_FORM1( "%s", ste->name );
    print_buffer( buffer );

    index = sym_data->label_type;

    BUF_FORM( "%s", elem_names[index] );

    if ( !old_flat )
	if ( sym_data->sub_type )
	    if ( *(sym_data->sub_type) != '\0' )
		BUF_FORM( ":%s", sym_data->sub_type );

    buffer = buffer1;
    print_buffer( buffer );

    count = -1;

    while ( (attrnum = elem_arg_forms[index][++count]) && more )
      if ( attrnum == A_DELIM )
	{
	if ( /* small_bend( ste ) || */ old_flat )
	    more = 0;
	else
	    {
	    BUF_FORM1( "%d", elem_arg_forms[index][++count] );
	    print_buffer( buffer );
	    }
	}
      else
	if ( !find_output_attr( ste, attrnum, 0 ) )
	    {
	    BUF_FORM1( "%s", elem_attr_defaults[attrnum] );
	    print_buffer( buffer );
	    }

    BUF_FORM1("%s","");
    print_buffer( buffer );
    }

static void print_as_drift( ste )
symbol ste;
    {
    char buffer1[MAXSTRING];
    char *buffer = buffer1;
    register int count;
    register int index;
    int attrnum;
    int more = 1;

    symbol_node *sym_data = (symbol_node *)(ste->symbol_data);

    BUF_FORM1( "%s", ste->name );
    print_buffer( buffer );

    index = sym_data->label_type;

    BUF_FORM( "%s", elem_names[index] );

    if ( !old_flat )
	if ( sym_data->sub_type )
	    if ( *(sym_data->sub_type) != '\0' )
		BUF_FORM( ":%s", sym_data->sub_type );

    buffer = buffer1;
    print_buffer( buffer );

    count = -1;

    while ( (attrnum = elem_arg_forms[index][++count]) && more )
      if ( attrnum == ATTR_L || attrnum == ATTR_LENGTH )
	if ( !find_output_attr( ste, attrnum, 0 ) )
	    {
	    BUF_FORM1( "%s", elem_arg_forms[attrnum] );
	    print_buffer( buffer );
	    }

    BUF_FORM1("%s","");
    print_buffer( buffer );
}

static void print_multipole( ste )
symbol ste;
    {
    char buffer1[MAXSTRING];
    char *buffer = buffer1;
    register int count;
    register int index;
    int attrnum;

    symbol_node *sym_data = (symbol_node *)(ste->symbol_data);

    BUF_FORM1( "%s", ste->name );
    print_buffer( buffer );

    index = sym_data->label_type;

    BUF_FORM( "%s", elem_names[index] );

    if ( !old_flat )
	if ( sym_data->sub_type )
	    if ( *(sym_data->sub_type) != '\0' )
		BUF_FORM( ":%s", sym_data->sub_type );

    buffer = buffer1;
    print_buffer( buffer );
    
    count = -1;

    while ( (attrnum = elem_arg_forms[index][++count]) )
      if ( attrnum == A_DELIM )
	{
	if ( old_flat )
	{
	    BUF_FORM1( "%s", "0.0" ); /* default value */
	    print_buffer( buffer );
	    break; /* get out of while */
	}

	BUF_FORM1( "%d", elem_arg_forms[index][++count] );
	print_buffer( buffer );
	}
      else
	if ( !find_output_attr( ste, attrnum, 1 ) )
	    {
	    BUF_FORM1( "%s", "0.0" ); /* default value */
	    print_buffer( buffer );
	    }

    BUF_FORM1("%s","");
    print_buffer( buffer );
    }

static void find_beam_elems( root )
beamnode *root;
    {
    if ( root )
	{
	if ( root->operation != BEAM_ARGUMENT )
	    {
	    if ( root->operation == BEAM_CALL_FUNC )
		{
		find_beam_param_elems( root->beam_params );
		find_beam_elems( ((symbol_node *)(root->elem_operand->symbol_data))->beam_root );
		}
	    else
		{
		print_elems( root->elem_operand );
		find_beam_elems( root->beam_operand );
		}
	    }

	find_beam_elems( root->next );
	}
    }

static void find_beam_param_elems( root )
beam_param_node *root;
    {
    register beam_param_node *next = root;

    while ( next )
	{
	find_beam_elems( next->beam_root );
	next = next->next;
	}
    }

static void print_elems( root )
symbol root;
    {
    symbol_node *sym_data;
    int markered = 0;

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /* this makes sure it is only printed once */
	    {
	    switch ( sym_data->label_type )
		{
                case ATTR_RFGAP:
                case ATTR_EDGE:
                case ATTR_ROTATION:
                case ATTR_THINLENS:
   		   if ( (old_flat) )
		   {
                     sym_data->label_type = ATTR_MARKER;
		     markered = 1;
		   }
                   /*  do not put a break here  */

                case ATTR_TANK:
                case ATTR_PMQ:
                case ATTR_RFQCELL:
                case ATTR_DOUBLET:
                case ATTR_TRIPLET:
                case ATTR_SPECIAL:
		   if ( (old_flat) && markered == 0 )
                     sym_data->label_type = ATTR_DRIFT;

                   /*  do not put a break here  */
                       
		case ATTR_DRIFT:
		case ATTR_HMONITOR:
		case ATTR_VMONITOR:
		case ATTR_MONITOR:
		case ATTR_INSTRUMENT:
		case ATTR_WIGGLER:
		case ATTR_RBEND:
		case ATTR_SBEND:
		case ATTR_QUADRUPOLE:
		case ATTR_SEXTUPOLE:
		case ATTR_OCTUPOLE:
		case ATTR_SOLENOID:
		case ATTR_RFCAVITY:
		case ATTR_EL_SEPARATE:
		case ATTR_SROT:
		case ATTR_YROT:
		case ATTR_HKICK:
		case ATTR_VKICK:
		case ATTR_KICKER:
		case ATTR_ECOLLIMATOR:
		case ATTR_RCOLLIMATOR:
                    print_reg_elem( root );
		    sym_data->declared = 0;
		    break;

		case ATTR_TBEND:
		case ATTR_NCELLS:
		case ATTR_DTL_CELL:
		case ATTR_RFQ_CELL:
		    print_as_drift( root );
  		    sym_data->declared = 0;
		    break;

		case ATTR_MARKER:
		    if ( old_flat )
			if ( sym_data->sub_type )
			    if ( !strcmp( sym_data->sub_type, "power" ) )
				{
				sym_data->declared = 0;
				*(root->name) = '\0';
				break;
				}

		    print_reg_elem( root );
		    sym_data->declared = 0;
		    break;

		case ATTR_MULTIPOLE:
		    print_multipole( root );
		    sym_data->declared = 0;
		    break;

		case GEOMETRY:
		case STRENGTH:
		case ATTR_COMMAND:
		    break;

		case BEAMLINE:
		case SLOT:
		case PARAM_LINE:
	    	    find_beam_elems( sym_data->beam_root );
		    break;
		}
	    }
	}
    }


/* BEAM BUFFERING ROUTINES */

static beamnode *get_arg( num, args )
double num;
beam_param_node *args;
    {
    while ( --num > 0 )
	if ( args )
	    args = args->next;
	else
	    {
	    fprintf( stderr, "Flat Print: not enough arguments to sub_line\n");
	    num = 0.0;
	    }

    if ( args )
        return ( args->beam_root );
    else
	{
	fprintf( stderr, "Flat Print: not enough arguments to sub_line\n");
	return (beamnode *)NULL;
	}
    }

static void insert_args( map, args )
beamnode *map;
beam_param_node *args;
    {
    register beam_param_node *next;

    while ( map )
	{
	if ( map->operation == BEAM_ARGUMENT )
	    map->beam_operand = get_arg( eval_expr( map->iteration ), args );
	else
	    {
	    if ( map->operation == BEAM_CALL_FUNC )
		{
		next = map->beam_params;

		while ( next )
		    {
		    insert_args( next->beam_root, args );
		    next = next->next;
		    }
		}
	    else
		{
		insert_args( map->beam_operand, args );
		if ( map->elem_operand )
		    insert_args( ((symbol_node *)(map->elem_operand->symbol_data))->beam_root, args );
		}
	    }
	map = map->next;
	}
    }

static void remove_args( map )
beamnode *map;
    {
    register beam_param_node *next;

    while ( map )
	{
	if ( map->operation == BEAM_ARGUMENT )
	    map->beam_operand = (beamnode *)NULL;
	else
	    {
	    if ( map->operation == BEAM_CALL_FUNC )
		{
		next = map->beam_params;

		while ( next )
		    {
		    remove_args( next->beam_root );
		    next = next->next;
		    }
		}
	    else
		{
		remove_args( map->beam_operand );
		if ( map->elem_operand )
		    remove_args( ((symbol_node *)(map->elem_operand->symbol_data))->beam_root );
		}
	    }
	map = map->next;
	}
    }


static void do_beam_call( map, args, buffer, transpose )
beamnode *map;
beam_param_node *args;
char *buffer;
int transpose;
    {
    insert_args( map, args );

    if ( transpose )
	buffer_beam_backwards( map, buffer );
    else
	buffer_beam( map, buffer );

    remove_args( map );
    }

static void buffer_beam( beam, buffer )
beamnode *beam;
char *buffer;
    {
    register double repeat;
    symbol_node *sym_data;

    while ( beam )
	{
        switch (beam->operation)
            {
            case BEAM_NO_OP:

		if ( beam->beam_operand )
		    buffer_beam( beam->beam_operand, buffer );
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE 
		         || sym_data->label_type == SLOT )
			buffer_beam( sym_data->beam_root, buffer );
		    else
			if ( *(beam->elem_operand->name) != '\0' && beam->)
			    {
			    BUF_FORM1( "%s", beam->elem_operand->name );
			    print_buffer( buffer );
			    }
		    }

	        break;

            case BEAM_REPEAT:
		for ( repeat = eval_expr( beam->iteration ); repeat > 0; repeat -= 1 )
		    if ( beam->beam_operand )
			buffer_beam( beam->beam_operand, buffer );
		    else
			{
			sym_data = (symbol_node *)(beam->elem_operand->symbol_data);
			if ( sym_data->label_type == BEAMLINE
		          || sym_data->label_type == SLOT )
			    buffer_beam( sym_data->beam_root, buffer );
			else
			    if ( *(beam->elem_operand->name) != '\0' )
				{
				BUF_FORM1( "%s", beam->elem_operand->name );
				print_buffer( buffer );
				}
			}

	        break;

            case BEAM_TRANSPOSE:
		if ( beam->beam_operand )
		    buffer_beam_backwards( beam->beam_operand, buffer );
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE
		      || sym_data->label_type == SLOT )
			buffer_beam_backwards( sym_data->beam_root, buffer );
		    else
			{
		        fprintf( stderr, "buffer_beam: beam transpose operator on element" );
			if ( *(beam->elem_operand->name) != '\0' )
			    {
			    BUF_FORM1( "%s", beam->elem_operand->name );
			    print_buffer( buffer );
			    }
			}
		    }

	        break;

	    case BEAM_CALL_FUNC:
		sym_data = (symbol_node *)(beam->elem_operand->symbol_data);
		do_beam_call( sym_data->beam_root, beam->beam_params, buffer,0);
		break;

	    case BEAM_ARGUMENT:
		if ( beam->beam_operand )
		    buffer_beam( beam->beam_operand, buffer );
		else
		    if ( beam->elem_operand )
			{
			sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE
			  || sym_data->label_type == SLOT )
			    buffer_beam( sym_data->beam_root, buffer );
			else
			    if ( *(beam->elem_operand->name) != '\0' )
				{
				BUF_FORM1( "%s", beam->elem_operand->name );
				print_buffer( buffer );
				}
			}
		    else
			{
			fprintf( stderr, "buffer_beam: unresolved argument found\n" );
			exit( 1 );
			/*NOTREACHED*/
			}
			
		break;

            default:
	        fprintf( stderr, "\nbuffer_beam: bad beam operation %d\n", beam->operation );
	        exit( 1 );
	        /*NOTREACHED*/
            }

	beam = beam->next;
	}
    }

static void buffer_beam_backwards( beam, buffer )
beamnode *beam;
char *buffer;
    {
    register double repeat;
    symbol_node *sym_data;

    if ( beam )
	{
	buffer_beam_backwards( beam->next, buffer );

        switch (beam->operation)
            {
            case BEAM_REPEAT:
		for ( repeat = eval_expr( beam->iteration ); repeat > 0; repeat -= 1 )
		    if ( beam->beam_operand )
			buffer_beam_backwards( beam->beam_operand, buffer );
		    else
			{
			sym_data = (symbol_node *)(beam->elem_operand->symbol_data);
			if ( sym_data->label_type == BEAMLINE
			  || sym_data->label_type == SLOT )
			    buffer_beam_backwards( sym_data->beam_root, buffer );
			else
			    if ( *(beam->elem_operand->name) != '\0' )
				{
				BUF_FORM1( "%s", beam->elem_operand->name );
				print_buffer( buffer );
				}
			}
	        break;

            case BEAM_TRANSPOSE:
		if ( beam->beam_operand )
		    buffer_beam( beam->beam_operand, buffer );
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE
		      || sym_data->label_type == SLOT )
			buffer_beam( sym_data->beam_root, buffer );
		    else
			{
		        fprintf( stderr, "buffer_beam: beam transpose operator on element" );

			if ( *(beam->elem_operand->name) != '\0' )
			    {
			    BUF_FORM1( "%s", beam->elem_operand->name );
			    print_buffer( buffer );
			    }
			}
		    }
	        break;

            case BEAM_NO_OP:
		if ( beam->beam_operand )
		    buffer_beam_backwards( beam->beam_operand, buffer );
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE
		      || sym_data->label_type == SLOT )
			buffer_beam_backwards( sym_data->beam_root, buffer );
		    else
			if ( *(beam->elem_operand->name) != '\0' )
			    {
			    BUF_FORM1( "%s", beam->elem_operand->name );
			    print_buffer( buffer );
			    }
		    }
	        break;

	    case BEAM_CALL_FUNC:
		sym_data = (symbol_node *)(beam->elem_operand->symbol_data);
		do_beam_call( sym_data->beam_root, beam->beam_params, buffer,1);
		break;

	    case BEAM_ARGUMENT:
		if ( beam->beam_operand )
		    buffer_beam_backwards( beam->beam_operand, buffer );
		else
		    if ( beam->elem_operand )
			{
			sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE
			  || sym_data->label_type == SLOT )
			    buffer_beam_backwards( sym_data->beam_root, buffer);
			else
			    {
			    fprintf( stderr, "buffer_beam: beam transpose operator on element" );

			    if ( *(beam->elem_operand->name) != '\0' )
				{
				BUF_FORM1( "%s", beam->elem_operand->name );
				print_buffer( buffer );
				}
			    }
			}
		    else
			{
			fprintf( stderr, "buffer_beam: unresolved argument found\n" );
			exit( 1 );
			/*NOTREACHED*/
			}
			
		break;



            default:
	        fprintf( stderr, "\nbuffer_beam: bad beam operation\n" );
	        exit( 1 );
	        /*NOTREACHED*/
            }
	}
    }

static void print_beam( root )
symbol root;
    {
    symbol_node *sym_data;
    char buffer1[MAXSTRING];
    char *buffer = buffer1;

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /*this makes sure it is only printed once*/
	    {
	    switch( sym_data->label_type )
	        {
	        case ATTR_DRIFT:
	        case ATTR_HMONITOR:
	        case ATTR_VMONITOR:
	        case ATTR_MONITOR:
	        case ATTR_INSTRUMENT:
	        case ATTR_WIGGLER:

	        case ATTR_RBEND:
	        case ATTR_SBEND:
	        case ATTR_QUADRUPOLE:
	        case ATTR_SEXTUPOLE:
	        case ATTR_OCTUPOLE:
	        case ATTR_SOLENOID:
	        case ATTR_EL_SEPARATE:
	        case ATTR_HKICK:
	        case ATTR_VKICK:
	        case ATTR_KICKER:
	        
	        case ATTR_RFCAVITY:
		case ATTR_ECOLLIMATOR:
	        case ATTR_RCOLLIMATOR:

                case ATTR_TBEND:
                case ATTR_THINLENS:
                case ATTR_TANK:
                case ATTR_EDGE:
                case ATTR_PMQ:
                case ATTR_RFQCELL:
                case ATTR_DOUBLET:
                case ATTR_TRIPLET:
                case ATTR_RFGAP:
                case ATTR_SPECIAL:
                case ATTR_ROTATION:

	        case ATTR_MARKER:
		case ATTR_COMMAND:

	        case ATTR_SROT:
	        case ATTR_YROT:

	        case ATTR_MULTIPOLE:

	        case GEOMETRY:
	        case STRENGTH:
		case PARAM_LINE:

		    break;

	        case BEAMLINE:
		case SLOT:
		    buffer_beam( sym_data->beam_root, buffer );
		    break;

	        default:
		    fprintf( stderr, "something is amiss in print_beam\n" );

	        }
	    print_buffer( "" );
	    sym_data->declared = 0; /*this makes sure it is only printed once*/
	    }
	}
    }


/* MAIN CONTROL ROUTINE */

void print_flat( root, oflat )
symbol root;
int oflat;
    {
    old_flat = oflat;

    print_paramsf( root );
    printf( "\nEND\n\n" );

    print_elems( root );
    printf( "END\n\n" );

    print_beam( root );
    printf( "END\n" );
    }


/* END OF FILE */


/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/print_trnsprt_title.c,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

#include <stdio.h>
#include <string.h>
#include <time.h>

void print_trns_title(db, key)  
char *db;                
char *key;              
{struct tm *systime;
  time_t t;

  t = time(NULL);
  systime = localtime(&t);
                        
  printf("/  %s  %s   %.2d/%.2d/%.2d  %.2d:%.2d/\n",
    db,key,systime->tm_mon+1,systime->tm_mday,systime->tm_year,
    systime->tm_hour,systime->tm_min);
 }

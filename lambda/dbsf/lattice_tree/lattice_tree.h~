
/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_tree.h,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

#include "../std_lib/symbol_table_lib.h"

/* EXPRNODE FORWARD DECLARATION */

struct en;


/* BEAM PARAM LIST FORWARD DECLARATION */

struct bpn;


/* BEAMLINE HEADER */

/* type of the operation to perform on the operand in beamlines */
typedef enum beam_optype
    {
    BEAM_NO_OP,
    BEAM_REPEAT,
    BEAM_TRANSPOSE,
    BEAM_CALL_FUNC, /* elem_operand points to the symbol table entry of the */
    /* param line to be expanded,  the beam_params points to the args */
    BEAM_ARGUMENT /* the iteration specifies the argument number of this node */
    } beam_optype;

/* the nodes of a beamline */
typedef struct bn
    {
    struct bn *next;
    struct bn *beam_operand;	/* the beam that the operation acts on */
    struct bpn *beam_params;	/* the parameters to a BEAM_CALL_FUNC */
    beam_optype operation;	/* operation from above list */
    struct en *iteration;	/* iteration expression or argument number */
    symbol elem_operand;
    /* the element that the op acts on or the param line to be expanded */
    } beamnode;

/* allocates a new beam node and initializes the operator and repeat factor */
extern beamnode *get_beam_node( /* op, repeat, name, beam */ );

/* returns pointer to last node in a beamline */
extern beamnode *beam_tail( /* beamnode *beam */ );

/* BEAM PARAM LIST HEADER */

/* beam param lists are singly linked lists of beamnode pointers which are */
/* used to disambiguate beam lines and a group of beam lines.  A group of */
/* beam lines is what we want when we substitute beam lines in for arguments */
/* of param lines.  */

typedef struct bpn
    {
    struct bpn *next;
    beamnode *beam_root;
    } beam_param_node;

extern beam_param_node *get_bpn( /* beam_root */ );

extern beam_param_node *split_beam( /* beam_root */ );

/* ATTRNODE HEADER */

/* attribute lists hold pointers to expressions and a flag to designate which */
/* attribute the expression is initializing.  The advantage to this form of  */
/* attribute storage is that it is fairly compact (you only have storage */
/* allocated for attributes which are being initialized) and the structure is */
/* easy to build.  The disadvantage is slow random access, but the lists */
/* don't get very long, so this shouldn't pose too much of a problem. */

/* this is the type of the node used to hold the expression tree roots for */
/* element attribute expressions */
typedef struct an
    {
    struct an *next;
    int attr;
    struct en *expr;
    } attrnode;

/* links a new attrnode to the end of the attrnode list pointed to by <old> */
extern attrnode *link_attr_node( /* attrnode *old, exprnode *expr, int attr*/ );

/* this function finds an attribute in an attr_list and returns a pointer to */
/* the attrnode which describes it.  Returns NULL if it isn't there */
extern attrnode *find_attr( /* int attr, attrnode *attr_list */ );

/* SYMBOL TABLE HEADER */

/* this is for type checking */
typedef enum c_t
    {
    NO_TYPE,		/* initialization value */
    BEAM_ARG,		/* beams and elements can be in beams */
    EXPR_ARG		/* STRs and GEOMs can be in exprs */
    } context_type;

typedef struct			/* the structure that symbol_data points to */
    {
    int label_type;		/* TOK_command word or ATTR_element type */
    char *sub_type;		/* set by type attribute initialization */
    context_type context;	/* the way this id is used in context */
    int declared;		/* declared flag */
    int in_stack;		/* has it been queued for resolution */
    int arg_num;		/* the argument number that this id is in the */
    beamnode *beam_root;	/* the beamline if it's a line or bpmerror */
    attrnode *expr_list;	/* list of attribute expression tree roots */
    int num_args;		/* for line functions. i.e. foo( x,f ): ... */
    } symbol_node;

extern void get_sym_data( /* ste */ );

extern symbol get_symbol_node( /* name, symbol_table */ );
extern int is_new_name( /* char *name, symbol_table */ );

extern void set_label_type( /* ste, type */ );
extern void set_context( /* ste, context */ );

#define HASH_TABLE_SIZE         1013
#define NO_SYM_VAL              0

extern int check_type( /* ste, int type */ );
extern int check_set_context( /* ste, context */ );

#define SYMBOL_DATA(ste)	((symbol_node *)(ste->symbol_data))


/* EXPRTREE DATA STRUCTURES HEADER */

/* operation types for exprnodes */
#define EXPR_ADD	1
#define EXPR_SUB	2
#define EXPR_MUL	3
#define EXPR_DIV	4
#define EXPR_NUM	5
#define EXPR_FUNC	6
#define EXPR_STE	7
#define EXPR_UNEG	8
#define EXPR_TILT	9
#define EXPR_SHAPE	10
#define EXPR_MPT	11 /* magnet_piece type stored as comment*/
#define EXPR_COMMAND	12 /*command expression type */

/* the valid shapes so far */
#define SHAPE_NULL	'n'
#define SHAPE_RECTANGLE	'r'
#define SHAPE_ELIPSE	'e'
#define SHAPE_DIAMOND	'd'
#define SHAPE_UNKNOWN	' '

/* the function specifiers */
#define INVALID_FUNC	0
#define FUNC_SQRT	1
#define FUNC_LOG	2
#define FUNC_EXP	3
#define FUNC_SIN	4
#define FUNC_COS	5
#define FUNC_TAN	6
#define FUNC_ASIN	7
#define FUNC_ATAN	8
#define FUNC_ABS	9
#define FUNC_MAX	10
#define FUNC_MIN	11
#define FUNC_RANF	12
#define FUNC_GAUSS	13
#define FUNC_TGAUSS	14
#define FUNC_USER0	15
#define FUNC_USER1	16
#define FUNC_USER2	17


/* the nodes of an expression tree */
typedef struct en
    {
    struct en *op1, *op2;	/* up to two operands for ops and funcs */
    double value;		/* space for evaluation results */
    int operation;		/* EXPR_[ ADD, SUB, MUL, DIV, NUM, ... ] */
    int funcname;		/* the tag to specify the function */
    int evaluated;		/* has this sub-expr been evaluated ? */
    char command[255];		/* 255 chars for command shoud suffice - couldn't find other way of doing it*/
    symbol id;			/* the symbol table entry of an id or func */
    } exprnode;

/* allocates new exprnode and initializes operation and operands */
extern exprnode *get_expr_node( /* operation, operand1, operand2, value */ );

/* this evaluates expressions for flat file printout */
extern double eval_expr( /* expr_root */ );

/* this returns the flag for the func name specified */
extern int get_funcname( /* char *func */ );

/* evaluates functions */
extern double eval_func( /* func, op1, op2 */ );

/* puts a character representation of the expression pointed to by <expr> */
/* in <buffer>.  <parens> should be 0 unless you know what you are doing */
/* a pointer to the last character of the buffer is returned */
/* if decls is true then the MAD format of the declarations of scalars in the */
/* expression are also printed */
extern char *buffer_expr( /* exprnode *expr, char *buffer,
    int parens, int decls */ );

extern char *buffer_exprr( /* exprnode *expr, char *buffer,
    int parens, int decls */ );

/* END OF FILE */

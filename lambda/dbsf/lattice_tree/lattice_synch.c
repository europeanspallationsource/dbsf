
/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_synch.c,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

/* These are the routines for getting DBSF to output SYNCH format */
/* They are not pretty! */
/* This work funded by SSC Labs, Dallas, Texas */
/* Written by your favorite high-energy programmer and mine, Eric Barr */

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lattice_gdecls.h"
#include "lattice_synch.h"
#include "lattice_tree.h"
#include "lattice_defines.h"
#include "../dbsf/dbsf_gdecls.h"
#include "../wireup/support.h"
#include "../dbsf/dbquery.h"

static void beam_search();
static void param_search();
static void elem_search();
extern void print_buffer(char *);

/* this is the gradient-brho flag to avoid default K-1.0 format in elements */
int grad_brho;

/* this function is used to generate unique new names of four characters */
/* new_name has not been tried */
static void generate_name( buffer )
char *buffer;
    {
    static char new_name[] = { 'A', 'A', 'A', 'A', '\0' };
    char init_char = 'A';
    char last_char = 'Z';
    int i;

    while ( !is_new_name( new_name, dbsf_st ) )
        {
	for( i=3; (new_name[i] == last_char && i >= 0);
	    new_name[i--] = init_char );

	if ( i == -1 )
	    {
	    fprintf( stderr,
	     "\n*** ERROR : All valid names used. Unable to generate new names. ***\n");
	    exit(1);
	    }

	new_name[i]++;
	}

    strcpy( buffer, new_name );
    }

/* this function makes the buffer provided all upper case and reports if */
/* any changes were made by returning 1 */
static int lower_to_upper( buffer )
char *buffer;
    {
    int i = -1;
    int change = 0;

    while( buffer[++i] )
	if ( islower( buffer[i] ) )
	    {
	    buffer[i] = toupper( buffer[i] );
	    change = 1;
	    }

    return( change );
    }

/* this routine does the name mapping to SYNCH name space from DB name space */
/* first it tries all of the permutations of flipping the case of characters */
/* in the truncated name, then it gives up and generates a name */
static void synchify_name( ste )
symbol ste;
    {
    int i = 0;
    char *new_name = (char *)NULL;
    int no_alias = 1;
    int name_len;
    int new_try = 1;

    new_name = synch_alias( ste->name );

    if ( new_name != NULL )
	if ( *new_name != '\0' )
	    no_alias = 0;

    /* if there isn't an alias then we need to the original name to be */
    /* 4 chars or less and all upper case.  If it already is, then it is */
    /* already in the symbol table in its final form and we can quit */
    if ( no_alias )
	{
	int change = 0;

	/* printf( "\n.no alias for `%s'\n", ste->name ); */
	
	if ( strlen( ste->name ) > 5 )
	    {
	    ste->name[5] = '\0';
	    change = 1;
	    }

	if ( lower_to_upper( ste->name ) )
	    change = 1;

        if ( change == 0 )
	    return;
	}
    else
	{
	/* printf( "\n.we got alias `%s' for `%s'\n", new_name, ste->name );*/

	lower_to_upper( new_name );
	strcpy( ste->name, new_name );
	}

    /* if the new name is not unique we twiddle it for a while to get */
    /* a new name lexographically close to the original */
    name_len = strlen( ste->name );

    /* this could be faster !! */
    /* the meaning of new_try is confusing because it actually means that the */
    /* twiddler was able to make a new name LAST time the loop was passed */
    /* through.  If the loop is ever not able to generate a new try then */
    /* new try is false and the loop ends */
    while ( !is_new_name( ste->name, dbsf_st ) && new_try )
        {
	new_try = 0;
	i = name_len-1;

	while ( !new_try && i >= 0 )
	    {
	    if ( ste->name[i] == 'Z' )
	    {
		ste->name[i--] = 'A';
	    } else
		if ( isupper( ste->name[i] ) )
		    {
		    ste->name[i]++;
		    new_try = 1;
		    }
		else
		    i--;
	    }
	}

    /* the twiddler ran out of possible tries so generate a name */
    if ( !new_try )
	generate_name( ste->name );

    /* we do this only to avoid future collisions, the node is not used */
    get_symbol_node( ste->name, dbsf_st );
    }

/* this routine is used to declare new parameter variables to accommodate */
/* exponentiation and extended precision in parameters of elememnt */
/* declarations */
/* a pointer to the symbol table entry of the new parameter is returned */
/* after the parameter has been printed out */
static symbol new_param( expr )
exprnode *expr;  /* the expr that is the value of the param */
    {
    char name[5];
    symbol ste;
    symbol_node *sym_data;

    generate_name( name );
    ste = get_symbol_node( name, dbsf_st );
    sym_data = (symbol_node *)ste->symbol_data;
    sym_data->expr_list = link_attr_node( (attrnode *)NULL, expr, ATTR_L );
    sym_data->declared = 1;
    sym_data->label_type = GEOMETRY;

    return( ste );
    }

/* this function is used to split floating point values into mantissa and */
/* exponent for funky SYNCH float output format of ten digit mantissa */
/* including decimal and opt minus sign and second field containing the */
/* optional exponent */
/* returns 1 if the buffered float is in exponent notation */
/* returns 0 if the float has an extended mantissa */
static int buffer_float( buffer, expr_root )
char *buffer;
exprnode *expr_root;
    {
    double eval = eval_expr( expr_root );
    char *loc;
    int exp = 10; /* this is used for lots of things, first it is where the e */
    /* in the buffered float will be */

    sprintf( buffer, "%#.8e", eval );

    if ( *buffer == '-' )
	*(buffer+10) = ' ';

    
    /* this separates the exponent from the mantissa */
    if ((loc = strchr( buffer, 'e' )) != NULL )
	{
	*loc++ = ' ';
	exp = atoi( loc );

	if ( loc < (buffer+10) )
	    sprintf( loc, "%.*s", buffer+10-loc, "          " );

	if ( exp )
	    sprintf( buffer+10, "%5i", exp );
	else
	    *(buffer+10) = '\0';

	exp = 1;
	}
    else
	exp = 0;

    /* this puts a decimal point in numbers that don't already have one */
    /*** as long as we use # in the format above, we don't need this
    if ( strchr( buffer, '.' ) == NULL )
	if ((loc = strchr( buffer, ' ' )) == NULL )
	    {
	    int i = strlen( buffer );

	    buffer[i] = '.';
	    buffer[i+1] = '\0';
	    }
	else
	    *loc = '.';
    ***/

    return( exp );
    }


/* This routine buffers a beamline declaration */
/* in the process it prints out the declarations of all constituent beamlines */
/* and declares new beamlines when reflection and repetition operators are */
/* not in proper SYNCH format. i.e. a reflection is operating on some */
/* portion of a beamline or a repetition is operating on more than one beam */

static char *buffer_beam( beam, buffer, root )
beamnode *beam;
char *buffer;
int root;  /* this indicates whether we are calling this with the root of */
/* a BML declaration of if this is a recursive call. 1 for root, 0 otherwise */
    {
    float reps;
    symbol new_ste;
    symbol_node *sym_data;

    while ( beam )
	{
        switch (beam->operation)
            {
            case BEAM_REPEAT:
		if ( root )
		    {
		    BUF_STRING(" 0     ");
		    root = 0;
		    }

		reps = eval_expr( beam->iteration );

		/* this will only work for 0.5 < reps < 999.5 */
		/* they should evaluate to integers between 2 and 999 incl. */
		if ( reps > 9.5 )
		    if ( reps > 99.5 )
			BUF_FORM("%.0f( ",reps)
		    else
			BUF_FORM(" %.0f( ",reps)
		else
		    BUF_FORM("  %.0f( ",reps)

		if ( beam->beam_operand )
		    buffer = buffer_beam( beam->beam_operand, buffer, 0 );
		else
		    {
		    beam_search( beam->elem_operand );
		    BUF_FORM("%-5.5s",beam->elem_operand->name);
		    }

		BUF_STRING(" )   ");
	        break;

            case BEAM_TRANSPOSE:
		if ( root && beam->next == NULL )
		    {
		    root = 0;
		    BUF_STRING("-1     ");

		    if ( beam->beam_operand )
			buffer = buffer_beam( beam->beam_operand, buffer, 0 );
		    else
			{
			beam_search( beam->elem_operand );
			BUF_FORM("%-5.5s",beam->elem_operand->name);
			}
		    }
		else
		    {
		    char new_name[5];

		    if ( root )
			{
			BUF_STRING(" 0     ");
			root = 0;
			}

		    generate_name( new_name );
		    new_ste = get_symbol_node( new_name, dbsf_st );
		    sym_data = (symbol_node *)new_ste->symbol_data;
		    sym_data->declared = 1;
		    sym_data->label_type = BEAMLINE;

		    sym_data->beam_root = get_beam_node( BEAM_TRANSPOSE,
		     (exprnode *)NULL, beam->elem_operand, beam->beam_operand );

		    beam->elem_operand = new_ste;
		    beam->beam_operand = NULL;
		    beam->operation = BEAM_NO_OP;

		    beam_search( beam->elem_operand );
		    BUF_FORM("%-5.5s",beam->elem_operand->name);
		    }

	        break;

            case BEAM_NO_OP:
		if ( root )
		    {
		    BUF_STRING(" 0     ");
		    root = 0;
		    }

		if ( beam->beam_operand )
		    {
		    buffer = buffer_beam( beam->beam_operand, buffer, 0 );
		    }
		else
		    {
		    beam_search( beam->elem_operand );
		    BUF_FORM("%-5.5s",beam->elem_operand->name);
		    }
	        break;

            default:
	        fprintf( stderr, "\nbuffer_beam: bad beam operation\n" );
	        exit( 1 );
	        /*NOTREACHED*/
            }
	beam = beam->next;
	}
    return ( buffer );
    }

/* this handles all of the output destined for the standard format file */
/* everything is changed to upper-case */
static void buffer_print( buf )
char buf[];
    {
    /* this statement does all of the printing for the synch file */
    printf( "%s\n", buf );
    }

static void output_beam( buffer )
char *buffer;
    {
    int end = 80;
    char hold;

    while ( strlen( buffer ) > end )
	{
	hold = buffer[end];
	buffer[end] = '\0';
	buffer_print( buffer );
	buffer[end] = hold;
	buffer += end;
	end = 60;
	printf( "                    " );
	}

    buffer_print( buffer );
    }

static void beam_search( root )
symbol root;
    {
    symbol_node *sym_data;
    char buffer1[MAX_BUFFER];
    char *buffer = buffer1;
    char out_buff[MAX_BUFFER];

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /*this makes sure it is only printed once*/
	    {
	    switch( sym_data->label_type )
	        {
	        case ATTR_DRIFT:
	        case ATTR_HMONITOR:
	        case ATTR_VMONITOR:
	        case ATTR_MONITOR:
		case ATTR_INSTRUMENT:
	        case ATTR_WIGGLER:

	        case ATTR_RBEND:
	        case ATTR_SBEND:
	        case ATTR_QUADRUPOLE:
	        case ATTR_SEXTUPOLE:
	        case ATTR_OCTUPOLE:
	        case ATTR_MULTIPOLE:
	        case ATTR_SOLENOID:
	        case ATTR_EL_SEPARATE:
	        case ATTR_HKICK:
	        case ATTR_VKICK:
	        case ATTR_KICKER:
	        
	        case ATTR_RFCAVITY:
		case ATTR_ECOLLIMATOR:
	        case ATTR_RCOLLIMATOR:

	        case ATTR_MARKER:

	        case ATTR_SROT:
	        case ATTR_YROT:

	        case GEOMETRY:
	        case STRENGTH:
		    break;

	        case ATTR_THINLENS:
	        case ATTR_TANK:
	        case ATTR_EDGE:
	        case ATTR_PMQ:
	        case ATTR_RFQCELL:
	        case ATTR_DOUBLET:
	        case ATTR_TRIPLET:
	        case ATTR_RFGAP:
	        case ATTR_SPECIAL:
	        case ATTR_ROTATION:
                    print_buffer( "This database contains a trace3d-specific element which");
                    print_buffer( "is not valid in SYNCH.  This run will now terminate.");
                    print_buffer( "You may look at this lattice with the flat or trace3d option.");
		    fprintf( stderr, "This database contains a trace3d-specific element which \n" );
                    fprintf( stderr, "is not valid in SYNCH.  This run will now terminate. \n");
                    fprintf( stderr, "You may look at this lattice with the flat or trace3d option.\n");
		    exit(1);
                    break;

	        case BEAMLINE:
		case SLOT:
		    buffer_beam( sym_data->beam_root, buffer, 1 );
		    synchify_name( root );
		    OUTBUFF2( " %-5.5s BML   %s", root->name, buffer );
		    output_beam( out_buff );
		    sym_data->declared = 0; /* only print it once */
		    break;

	        default:
		    fprintf( stderr,
			"*** ERROR: something is amiss in print_lines\n" );
		    exit( 1 );
	        }
	    }
	}
    }

/* PRINT PARAMETERS SECTION ------------------------------------------------- */

/* this function is used to print out a parameter declaration */
static void output_param( root )
symbol root;
    {
    symbol_node *sym_data = (symbol_node *)root->symbol_data;
    char out_buff[MAX_BUFFER];
    char buffer[30]; /* this is used to hold the funky float format of SYNCH */

    if ( sym_data->declared )
	{
	synchify_name( root );

	if ( buffer_float( buffer, sym_data->expr_list->expr ) )
	    OUTBUFF2(" %-5.5s PARA         %s", root->name, buffer )
	else
	    OUTBUFF2(" %-5.5s =            %s", root->name, buffer )

	buffer_print( out_buff );
	sym_data->declared = 0; /* only print it once */
	}
    }

/* this function checks the K value expression to see if it is in */
/* gradient/brho form and declares the parameters in the numerator and */
/* denominator if possible. returns 1 if parseable, 0 otherwise */
static int parse_expr( expr )
exprnode *expr;
    {
    if ( expr->operation == EXPR_DIV )
	if ( expr->op2->operation == EXPR_STE )
	    {
	    output_param( expr->op2->id );

	    if (expr->op1->operation == EXPR_STE )
		output_param( expr->op1->id );
	    else
		{
		if ( expr->op1->operation == EXPR_UNEG )
		    if ( expr->op1->op1->operation == EXPR_STE )
			if ( strlen( expr->op1->op1->id->name ) < 5 )
			    {
			    output_param( expr->op1->op1->id );
			    return( 1 );
			    }

		{
		char buffer[MAX_BUFFER];
		exprnode *tmp_expr = get_expr_node( EXPR_STE,
		    (exprnode *)NULL, (exprnode *)NULL, 0.0 );

		tmp_expr->id = new_param( expr->op1 );
		buffer_expr( expr->op1, buffer, 0, 0 );
		expr->op1 = tmp_expr; /* set op1 to the new param */
		printf(". %s = %s\n", tmp_expr->id->name, buffer );
		output_param( tmp_expr->id ); /* print param declaration */
		}
		}
	    return( 1 );
	    }
    return( 0 );
    }

/* if we don't have a declared parameter for a magnet attribute, we need to */
/* declare one and print a comment about it */
static void fix_expr_attr( attr )
attrnode *attr;
    {
    char buffer[MAX_BUFFER];

    if ( attr->expr->operation == EXPR_SHAPE ) return; /* SYNCH ignores */

    if ( attr->expr->operation != EXPR_STE )
	{
	exprnode *tmp_expr = get_expr_node( EXPR_STE, /* do the declaration */
	    (exprnode *)NULL, (exprnode *)NULL, 0.0 );

	tmp_expr->id = new_param( attr->expr ); /* get a name for it */

	/* make a character string representation of the expr for the comment */
	buffer_expr( attr->expr, buffer, 0, 0 );

	attr->expr = tmp_expr; /* set the attribute value to the new param */

	if ( attr->attr == ATTR_TILT )
	    {
	    symbol_node *sd = (symbol_node *)(tmp_expr->id->symbol_data);

	    exprnode *t2 = sd->expr_list->expr;

	    /* new tilt parameter and RAD->ANGLE conversion performed here */
	    t2->value = eval_expr( t2 ) * 90 / HALFPI;
	    printf(". %s = (%s) * 180 / PI\n", tmp_expr->id->name, buffer );
	    }
	else
	    /* print a comment about the derivation of it */
	    printf(". %s = %s\n", tmp_expr->id->name, buffer );

	output_param( tmp_expr->id ); /* print out the param declaration line */
	}
    else
	if ( attr->attr == ATTR_TILT )
	    {
	    symbol_node *sym_dat = (symbol_node *)(attr->expr->id->symbol_data);

	    if ( sym_dat->declared )
		{
		exprnode *tmp_expr = get_expr_node( EXPR_STE,
		    (exprnode *)NULL, (exprnode *)NULL, 0.0 );
		symbol_node *sd;
		exprnode *t2;

		tmp_expr->id = new_param( attr->expr ); /* get a name for it */
		buffer_expr( attr->expr, buffer, 0, 0 );
		attr->expr = tmp_expr;

		sd = (symbol_node *)(tmp_expr->id->symbol_data);
		t2 = sd->expr_list->expr;

		/* new tilt parameter and RAD->ANGLE conversion made here */
		t2->value = eval_expr( t2 ) * 90 / HALFPI;
		printf(". %s = (%s) * 180 / PI\n", tmp_expr->id->name, buffer );
		output_param( tmp_expr->id );
		}
	    }
	else
	    output_param( attr->expr->id );

    }

static void beam_param_search( root )
beamnode *root;
    {
    if ( root )
	{
	param_search( root->elem_operand );
	beam_param_search( root->beam_operand );
	beam_param_search( root->next );
	}
    }

static void param_search( root )
symbol root;
    {
    symbol_node *sym_data;
    attrnode *next;

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /* this makes sure it is only printed once */
	    {
	    switch ( sym_data->label_type )
		{
		case ATTR_HMONITOR:
		case ATTR_VMONITOR:
		case ATTR_MONITOR:
		case ATTR_INSTRUMENT:
		case ATTR_WIGGLER:
		case ATTR_SOLENOID: 
		case ATTR_RFCAVITY:
		case ATTR_EL_SEPARATE: 
		case ATTR_SROT: 
		case ATTR_YROT:
		case ATTR_ECOLLIMATOR: 
		case ATTR_RCOLLIMATOR: 
		case ATTR_DRIFT:
		case ATTR_MARKER:
		    /* do nothing because these are all turned into DRF */
		    /* with the length evaluated */
		    break;

		case ATTR_MULTIPOLE:
		    /* do nothing because all of these will be evaluated */
		    break;

		case ATTR_QUADRUPOLE:
		case ATTR_SEXTUPOLE:
		case ATTR_OCTUPOLE:
		    next = sym_data->expr_list;

		    while ( next )
			{
			/* this declares a symbol for the attributes if they */
			/* are rooted in expressions */
			if ( grad_brho && ( next->attr == ATTR_K1 ||
			    next->attr == ATTR_K2 || next->attr == ATTR_K3 ) )
			    {
			    if( !parse_expr( next->expr ) )
				fix_expr_attr( next );
			    }
			else
                           {
                            if ( next->expr->operation == EXPR_TILT )
                               {
                               if (sym_data->label_type == ATTR_QUADRUPOLE)
                                  next->expr->value = 0.785398163;
                               if (sym_data->label_type == ATTR_SEXTUPOLE)
                                  next->expr->value = 0.523598776;
                               if (sym_data->label_type == ATTR_OCTUPOLE)
                                  next->expr->value = 0.392699082;
                               }
			     fix_expr_attr( next );
                            }
			next = next->next;
			}
		    break;

		case ATTR_RBEND: 
		case ATTR_SBEND:
		case ATTR_HKICK:
		case ATTR_VKICK:
		case ATTR_KICKER:
		    next = sym_data->expr_list;

		    while ( next )
			{
			/* this declares a symbol for the attributes if they */
			/* are rooted in expressions */
                        if ( next->expr->operation == EXPR_TILT )
                             next->expr->value = 1.570796327;
			fix_expr_attr( next );
			next = next->next;
			}
		    break;

	        case ATTR_THINLENS:
	        case ATTR_TANK:
	        case ATTR_EDGE:
	        case ATTR_PMQ:
	        case ATTR_RFQCELL:
	        case ATTR_DOUBLET:
	        case ATTR_TRIPLET:
	        case ATTR_RFGAP:
	        case ATTR_SPECIAL:
	        case ATTR_ROTATION:
                    print_buffer( "This database contains a trace3d-specific element which");
                    print_buffer( "is not valid in SYNCH.  This run will now terminate.");
                    print_buffer( "You may look at this lattice with the flat or trace3d option.");
		    fprintf( stderr, "This database contains a trace3d-specific element which \n" );
                    fprintf( stderr, "is not valid in SYNCH.  This run will now terminate. \n");
                    fprintf( stderr, "You may look at this lattice with the flat or trace3d option.\n");
		    exit(1);
                    break;

		case GEOMETRY:
		case STRENGTH:
		    output_param( root );
		    break;

		case BEAMLINE:
		case SLOT:
		/* searches for params in beam lines */
	    	    beam_param_search( sym_data->beam_root );
		    break;

		case PARAM_LINE:
		    break;
		}
	    }
	}
    }

/* PRINT ELEMENTS SECTION --------------------------------------------------- */

#define ABS(x) ( (x) > 0 ? (x) : -(x) )
#define MAGV(ang) ( ABS(ang-90)<TOLERANCE ? 1 : 0 )
#define ZERO_TILT(ang) ( ABS(ang)<TOLERANCE )
#define FUN_TILT(ang) ( !ZERO_TILT(ang) && !MAGV(ang) )

/* this returns the tilt specified by the elements tilt parameter or 0 if */
/* there is none */
static double tilt( exprs )
attrnode *exprs;
    {
    exprs = find_attr( ATTR_TILT, exprs );

    if ( exprs )
	return( eval_expr( exprs->expr ) );
    else
	return( 0 ); /* no tilt found so assume 0 radians */
    }

/* saves the root name in old_name and generates a new name for the root */
static void change_name( root, old_name )
symbol root;
char *old_name;
    {
    strcpy( old_name, root->name );

    generate_name( root->name );
    get_symbol_node( root->name, dbsf_st );
    }

/* this function adds a rot of angle = ATTR_TILT for the element */
/* it uses the old_name for the rot */
static void emit_rot( root, old_name )
symbol root;
char *old_name;
    {
    symbol_node *sd = (symbol_node *)(root->symbol_data );
    
    printf( " %-5.5s ROT          %-5.5s     %-5.5s\n", old_name, root->name,
	find_attr( ATTR_TILT, sd->expr_list )->expr->id->name );

    strcpy( root->name, old_name );
    }

/* this handles the rotation of sextupole elements by declaring two rotation */
/* matrices and a beamline with the rotation matrices and the sxtp to */
/* replace the element */
static void rot_sxtp( root, bml_name )
symbol root;
char *bml_name;
    {
    symbol_node *sd = (symbol_node *)(root->symbol_data );
    char pos_rot[5];
    char sxtp_name[5];
    exprnode *angle = find_attr( ATTR_TILT, sd->expr_list )->expr;
   
    /* get name for the sxtp out of root and make a name for the pos_rot */
    change_name( root, sxtp_name );

    /* get name for the pos_rot out of root and make a name for the neg_rot */
    change_name( root, pos_rot );

    /* print the positive rotation matrix declaration */
    printf( " %-5.5s ROTZ         %-5.5s\n", pos_rot, angle->id->name );

    /* print out the rotation matrix for the reverse rotation */
    if ( strlen( angle->id->name ) > 4 )
	{
	symbol neg_ang = new_param( get_expr_node( EXPR_UNEG, angle,
	    (exprnode *)NULL, 0.0 ) );

	printf( ". new param %s declared of angle -%s\n", neg_ang->name,
	    angle->id->name );

	output_param( neg_ang );

	printf( " %-5.5s ROTZ         %-5.5s\n", root->name, angle->id->name );
	}
    else
	printf( " %-5.5s ROTZ         -%-4.4s\n", root->name, angle->id->name );

    /* print the beamline that sandwiches the sxtp between the rotz matrices */
    printf( " %-5.5s BML          %-5.5s%-5.5s%-5.5s\n", bml_name, pos_rot,
	sxtp_name, root->name );

    strcpy( root->name, bml_name ); /* make name of the element the bml name */
    }

/* this function finds an attribute name if it is present and returns a */
/* pointer to the name of the id which describes the attribute */
/* if the attribute is not present, a pointer to a default value is returned */
static char *find_attr_name( attr, attr_list )
int attr;
attrnode *attr_list;
    {
    /* This line is right. Don't mess. I know it is = and not == */
    if ( (attr_list=find_attr( attr, attr_list )) )
	return( attr_list->expr->id->name );
    else
	return( elem_attr_defaults[attr] );
    }

/* this function returns the k value in K-1.0 form or gradient-brho form */
/* depending on whether or not the brho format has been selected and the */
/* correct expression form is present */
static char *k_form( k_attr, attr_list )
int k_attr;
attrnode *attr_list;
    {
    static char buffer[21];

    if ( (attr_list=find_attr( k_attr, attr_list )) )

	if ( grad_brho && attr_list->expr->operation == EXPR_DIV )

	    if ( attr_list->expr->op1->operation == EXPR_UNEG )

		sprintf( buffer, "-%-4.4s     %-5.5s",
		    attr_list->expr->op1->op1->id->name,
		    attr_list->expr->op2->id->name );

	    else

		sprintf( buffer, "%-5.5s     %-5.5s",
		    attr_list->expr->op1->id->name,
		    attr_list->expr->op2->id->name );

	else

	    sprintf( buffer, "%-5.5s     1.0", attr_list->expr->id->name );
    else

	sprintf( buffer, "%-5.5s     1.0", elem_attr_defaults[k_attr] );

    return( buffer );
    }

static void beam_elem_search( root )
beamnode *root;
    {
    if ( root )
	{
	elem_search( root->elem_operand );
	beam_elem_search( root->beam_operand );
	beam_elem_search( root->next );
	}
    }

static void emit_drf( name, len, out_buff )
char *name;
double len;
char *out_buff;
    {
    int loc = 9;
    char buffer[20];

    sprintf( buffer, "%#.9f", len );
    buffer[10] = '\0';

    while ( buffer[loc] == '0' )
	buffer[loc--] = ' ';

    OUTBUFF2(" %-5.5s DRF          %s",
	name, buffer )
    }

static void elem_search( root )
symbol root;
    {
    symbol_node *sym_data;
    char out_buff[MAX_BUFFER];
    attrnode *next;
    double mag_tilt;
    char old_name[5];

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /* this makes sure it is only printed once */
	    {
	    mag_tilt = tilt( sym_data->expr_list );

	    switch ( sym_data->label_type )
		{
		/* DRF - these are all modeled as drifts */
		case ATTR_HMONITOR:
		case ATTR_VMONITOR:
		case ATTR_MONITOR:
		case ATTR_WIGGLER:
		case ATTR_SOLENOID:
		case ATTR_RFCAVITY:
		case ATTR_EL_SEPARATE:
		case ATTR_SROT:
		case ATTR_YROT:
		case ATTR_ECOLLIMATOR:
		case ATTR_RCOLLIMATOR:
		    printf( ".*** WARNING : `%s' element type not supported.  A drift\n", elem_names[sym_data->label_type] );
		    printf( ".               space of the same length has been created to replace it\n" );
		    /* don't mess with this fall through to the DRF emitter */

		case ATTR_DRIFT:
		case ATTR_MARKER:
		case ATTR_INSTRUMENT:
		    next = sym_data->expr_list;
 
		    while ( next )
			{
			if ( next->attr == ATTR_L )
			    break;
			next = next->next;
			}

		    synchify_name( root );

		    if ( next )
			{

			/* FIX ME */
			/* if there is a STE print a name */
			/* the STE declaration wasn't printed so we must eval */
			/* if there is a func, print its value */

			if ( 0 /*** next->expr->operation == EXPR_STE ***/ )
			    OUTBUFF2(" %-5.5s DRF          %-5.5s",
				root->name, next->expr->id->name )
			else
			    emit_drf( root->name, eval_expr( next->expr ),
				out_buff );
			}
		    else
			OUTBUFF1(" %-5.5s DRF          0.0", root->name )

		    buffer_print( out_buff );
		    sym_data->declared = 0; /* only print it once */
		    break;

		case ATTR_RBEND:
		case ATTR_SBEND:
		    {
		    attrnode *e1an = find_attr( ATTR_E1, sym_data->expr_list );
		    attrnode *e2an = find_attr( ATTR_E2, sym_data->expr_list );
		    int e1 = e1an != (attrnode *)NULL && e1an->expr !=
			    (exprnode *)NULL && eval_expr(e1an->expr) != 0.0;
		    int e2 = e2an != (attrnode *)NULL && e2an->expr !=
			    (exprnode *)NULL && eval_expr(e2an->expr) != 0.0;

		    synchify_name( root );

		    if ( FUN_TILT(mag_tilt) )
			change_name( root, old_name );

		    if ( (e1 || e2) && sym_data->label_type == ATTR_SBEND )
			OUTBUFF7(" %-5.5s %-4.4s         %-5.5s     0.0       %-5.5s     %-5.5s     %-5.5s     %-5.5s",
			    root->name,
			    ( MAGV(mag_tilt) ? "MAGV" : "MAG " ),
			    find_attr_name( ATTR_L, sym_data->expr_list ),
			    find_attr_name( ATTR_L, sym_data->expr_list ),
			    find_attr_name( ATTR_ANGLE, sym_data->expr_list ),
			    find_attr_name( ATTR_E1, sym_data->expr_list ),
			    find_attr_name( ATTR_E2, sym_data->expr_list ))
		    else
			OUTBUFF6(" %-5.5s %-4.4s         %-5.5s     0.0       %-5.5s     %-5.5s     %c",
			    root->name,
			    ( MAGV(mag_tilt) ? "MAGV" : "MAG " ),
			    find_attr_name( ATTR_L, sym_data->expr_list ),
			    find_attr_name( ATTR_L, sym_data->expr_list ),
			    find_attr_name( ATTR_ANGLE, sym_data->expr_list ),
			    ( sym_data->label_type == ATTR_RBEND ? '$' : ' ' ) );
		    buffer_print( out_buff );

		    if ( FUN_TILT(mag_tilt) )
			emit_rot( root, old_name );

		    sym_data->declared = 0; /* only print it once */
		    }
		    break;

		case ATTR_QUADRUPOLE:
		    synchify_name( root );

		    if ( FUN_TILT(mag_tilt) )
			change_name( root, old_name );

		    OUTBUFF4(" %-5.5s %-4.4s         %-5.5s     %s",
			root->name,
			( MAGV(mag_tilt) ? "MAGV" : "MAG " ),
			find_attr_name( ATTR_L, sym_data->expr_list ),
		        k_form( ATTR_K1, sym_data->expr_list ) );

		    buffer_print( out_buff );

		    if ( FUN_TILT(mag_tilt) )
			emit_rot( root, old_name );

		    sym_data->declared = 0; /* only print it once */
		    break;

		case ATTR_SEXTUPOLE:
		    synchify_name( root );

		    if ( !ZERO_TILT(mag_tilt) )
			change_name( root, old_name );

		    OUTBUFF3(" %-5.5s SXTP         %-5.5s     %s",
			root->name,
			find_attr_name( ATTR_L, sym_data->expr_list ),
		        k_form( ATTR_K2, sym_data->expr_list ) );

		    buffer_print( out_buff );

		    if ( !ZERO_TILT(mag_tilt) )
			rot_sxtp( root, old_name );

		    sym_data->declared = 0; /* only print it once */
		    break;

		case ATTR_OCTUPOLE:
		    /* FIX ME this assumes 2n+2 in SXTP order param */
		    synchify_name( root );

		    if ( !ZERO_TILT(mag_tilt) )
			change_name( root, old_name );

		    OUTBUFF3(" %-5.5s NPOL   4   0 %-5.5s     %s",
			root->name,
			find_attr_name( ATTR_L, sym_data->expr_list ),
		        k_form( ATTR_K3, sym_data->expr_list ) );

		    buffer_print( out_buff );

		    if ( !ZERO_TILT(mag_tilt) )
			rot_sxtp( root, old_name );

		    sym_data->declared = 0; /* only print it once */
		    break;

		case ATTR_HKICK:
		case ATTR_VKICK:
		case ATTR_KICKER:
		    {
		    exprnode *hkick = (exprnode*)NULL;
		    exprnode *vkick = (exprnode*)NULL;
		    exprnode *length = (exprnode*)NULL;
		    int ish, isv, isfat;
		    char new_name[10];

		    next = sym_data->expr_list;
		    synchify_name( root );
 
		    while ( next )
			{
			switch( next->attr )
			    {
			    case ATTR_L:
				length = next->expr;
				break;
			    case ATTR_KICK:
				if ( sym_data->label_type == ATTR_VKICK )
				    vkick = next->expr;
				else
				    hkick = next->expr;
				break;
			    case ATTR_KICKH:
				hkick = next->expr;
				break;
			    case ATTR_KICKV:
				vkick = next->expr;
				break;
			    default:
				break;
			    };
			next = next->next;
			}
		/* determines if we have a non-zero length kick or if there */
		/* are both horiz and vert components to the kick */
		/* if so, we need a drift space to apply the kicks to */
		    isv = vkick != (exprnode *)NULL && eval_expr(vkick) != 0.0;
		    ish = hkick != (exprnode *)NULL && eval_expr(hkick) != 0.0;
		    isfat = length!=(exprnode *)NULL && eval_expr(length)!=0.0;

		    if ( !isv && !ish )
		    {
			isv = ( sym_data->label_type == ATTR_VKICK );
			ish = ( !isv );
		    }

		    if ( ish && isv )
			{
			char bml_buf[80];
			char *bml_next = bml_buf + 20;

			printf(". beginning of beamline to represent '%s' kicker element\n", root->name );
			sprintf( bml_buf, " %-5.5s BML    0     ", root->name );

			if ( isfat ) /* need a full drift */
			    {
			    generate_name(new_name);
			    get_symbol_node( new_name, dbsf_st );
			    sprintf( bml_next, "%-5.5s", new_name );
			    bml_next += 5;
			    emit_drf( new_name, eval_expr(length), out_buff);
			    buffer_print( out_buff );
			    }

			change_name( root, new_name ); /* r->nn, k->rn */
			OUTBUFF2(" %-5.5s KICK   2               0.0       1.0       %-5.5s",
			root->name, (hkick?hkick->id->name:"0.0") );
			buffer_print( out_buff );

			if ( !ZERO_TILT(mag_tilt) )
			    {
			    generate_name( old_name ); /* t->on */
			    get_symbol_node( old_name, dbsf_st );
			    emit_rot( root, old_name ); /* t->rn */
			    }

			sprintf( bml_next, "%-5.5s", root->name );
			bml_next += 5;
			strcpy( root->name, new_name ); /* r->rn */

			change_name( root, new_name ); /* r->nn, k->rn */
			OUTBUFF2(" %-5.5s KICK   1               0.0       1.0       %-5.5s",
			root->name, (vkick?vkick->id->name:"0.0") );
			buffer_print( out_buff );

			if ( !ZERO_TILT(mag_tilt) )
			    {
			    generate_name( old_name ); /* t->on */
			    get_symbol_node( old_name, dbsf_st );
			    emit_rot( root, old_name ); /* t->rn */
			    }

			sprintf( bml_next, "%-5.5s", root->name );
			bml_next += 5;
			strcpy( root->name, new_name ); /* r->rn */

			if ( isfat ) /* need a full drift */
			    {
			    generate_name(new_name);
			    get_symbol_node( new_name, dbsf_st );
			    sprintf( bml_next, "%-5.5s", new_name );
			    bml_next += 5;
			    emit_drf( new_name, eval_expr(length),
				out_buff );
			    buffer_print( out_buff );
			    }

			printf(". new beamline which represents kicker element\n");
			buffer_print( bml_buf );
			}
		    else
			{
			if ( isfat )
			    {
			    generate_name(new_name);
			    get_symbol_node( new_name, dbsf_st );
			    emit_drf( new_name, eval_expr(length),
				out_buff );
			    buffer_print( out_buff );
			    }

			if ( !ZERO_TILT(mag_tilt) )
			    change_name( root, old_name );

			OUTBUFF4(" %-5.5s KICK   %i     %-5.5s               1.0       %-5.5s",
			root->name, (isv?1:2), (isfat?new_name:"     "),
			(isv?(vkick?vkick->id->name:"0.0"):
			(ish?(hkick?hkick->id->name:"0.0"):"0.0  ")) );
			buffer_print( out_buff );

			if ( !ZERO_TILT(mag_tilt) )
			    emit_rot( root, old_name );

			}
			
		    sym_data->declared = 0; /* only print it once */

		    };

		    break;
		
		case ATTR_MULTIPOLE:
		    {
		    char bml_buf[80]; /* holds the names of the npols */
		    char *bml_next = bml_buf + 20;
		    int n; /* for KnL, Tn */
		    char root_hold[10];
		    char coeff[20];
		    attrnode *knl, *tn;
		    int loc;
		    int t = 0; /* for skew multipole moments */

		    synchify_name( root );
		    printf(". beginning of beamline to represent '%s' multipole element\n", root->name );
		    sprintf( bml_buf, " %-5.5s BML    0     ", root->name );
		    strcpy( root_hold, root->name );

		    for( n=0; n<10; n++ )
			if ( (knl=find_attr( ATTR_K0L+n, sym_data->expr_list)) )
			    {
			    change_name( root, old_name );
			    sprintf( coeff, "%#.9f", eval_expr(knl->expr) );
			    coeff[10] = '\0';
			    loc = 9;

			    while ( coeff[loc] == '0' )
				coeff[loc--] = ' ';

			    tn = find_attr( ATTR_T0+n, sym_data->expr_list );
			    t = 0;

			    if ( tn )
				if ( tn->expr)
				    if ( tn->expr->operation == EXPR_TILT )
					t = 1;

			    OUTBUFF4(" %-5.5s NPOL   %i   %i 0.0       %s1.0",
				root->name, n+1, t, coeff );
			    buffer_print( out_buff );

			    if ( tn && !t )
				{
				change_name( root, old_name );
				sprintf( coeff, "%#.9f",
				    eval_expr(tn->expr) * 180 / PI );
				coeff[10] = '\0';
				loc = 9;

				while ( coeff[loc] == '0' )
				    coeff[loc--] = ' ';

				OUTBUFF3(" %-5.5s ROT          %-5.5s     %-5.5s",
				    root->name, old_name, coeff );
				buffer_print( out_buff );
				}

			    sprintf( bml_next, "%-5.5s", root->name );
			    bml_next += 5;
			    }

		    strcpy( root->name, root_hold );

		    if ( bml_next > bml_buf )
			{
			printf(". beamline representing multipole element\n");
			buffer_print( bml_buf );
			}
		    else
			{
			printf( ". no non-zero strength coefficients in '%s' multipole : drift substituted\n", root->name );
			printf(" %-5.5s DRF          0.0\n", root->name );
			}

		    sym_data->declared = 0; /* only print it once */
		    }
		    break;

	        case ATTR_THINLENS:
	        case ATTR_TANK:
	        case ATTR_EDGE:
	        case ATTR_PMQ:
	        case ATTR_RFQCELL:
	        case ATTR_DOUBLET:
	        case ATTR_TRIPLET:
	        case ATTR_RFGAP:
	        case ATTR_SPECIAL:
	        case ATTR_ROTATION:
                    print_buffer( "This database contains a trace3d-specific element which");
                    print_buffer( "is not valid in SYNCH.  This run will now terminate.");
                    print_buffer( "You may look at this lattice with the flat or trace3d option.");
		    fprintf( stderr, "This database contains a trace3d-specific element which \n" );
                    fprintf( stderr, "is not valid in SYNCH.  This run will now terminate. \n");
                    fprintf( stderr, "You may look at this lattice with the flat or trace3d option.\n");
		    exit(1);
                    break;

		case GEOMETRY:
		case STRENGTH:
		    break;

		case BEAMLINE:
		case SLOT:
	    	    beam_elem_search( sym_data->beam_root );
		    break;

		case PARAM_LINE:
		    break;

		default:
		    fprintf( stderr, ".***ERROR: lattice_synch.c: unknown element type found\n" );
		}
	    }
	}
    }

/* MAIN ROUTINE ------------------------------------------------------------- */

void print_synch( root, gbr )
symbol root;
int gbr;
    {
    grad_brho = gbr;
    open_db();
    param_search( root );
    printf( "\n" );
    elem_search( root );
    printf( "\n" );
    beam_search( root );
    close_db();
    }

/* END OF FILE -------------------------------------------------------------- */

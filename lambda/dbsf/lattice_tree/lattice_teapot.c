
/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_teapot.c,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "lattice_gdecls.h"
#include "lattice_tree.h"
#include "lattice_defines.h"
#include "../wireup/support.h"
#include "../dbsf/dbquery.h"

static void print_linest();
static void no_under_all_uppert();
static void chop_buft();
static void buff_reg_elemt();
static void print_buffert();
static void buff_multipolet();
static void print_paramst();
static void print_elemst();


#define INDENT		10

int flat_expr;      /* don't flatten expressions by default */
int standard_403;   /* MAD 4.03 standard file */
int standard_81;   /* MAD 8.1 standard file */
static int aper_flag;  /* signals whether or not to print aperture info */

struct
 { char* old_name;
   char* new_name;
   char* tpot_type;
   char* split;
   char* conv_to_drift;
   char* conv_to_sbend;
 } hold_tpot_info;

int teapotify_quads = 0;
int teapotify_vbend = 0;
int teapotify_rbend = 0;

char **hold_tpot;

char buf_hold_l[40];
char buf_hold_k1[40];
char buf_hold_tilt[20];
char buf_hold_angle[40];
 
/* this loads an expression into <buffer> and resolves the declaration of */
/* scalar variables contained in the expression before returning */
static void expand_exprt( expr, buffer, parens )
exprnode *expr;
char *buffer;
int parens;
    {
    if ( flat_expr )
    {
        if ( expr )
            if ( expr->operation == EXPR_SHAPE )
		BUF_FORM("%c",(char)eval_expr( expr ))
	    else
		BUF_FORM(FLOAT_FORMAT,eval_expr( expr ))
    } else
	buffer_expr( expr, buffer, parens, 1 );
    }

static char *buffer_beam( beam, buffer )
beamnode *beam;
char *buffer;
    {
    BUF_STRING("( ");

    while ( beam )
	{
        switch (beam->operation)
            {
            case BEAM_REPEAT:
		BUF_FORM("%.0f",eval_expr( beam->iteration ))
		BUF_CHAR('*');
	        break;

            case BEAM_TRANSPOSE:
		BUF_CHAR('-');
	        break;

            case BEAM_NO_OP:
	        break;

            default:
	        fprintf( stderr, "\nbuffer_beam: bad beam operation\n" );
	        exit( 1 );
	        /*NOTREACHED*/
            }

        if ( beam->beam_operand )
	    {
	    buffer = buffer_beam( beam->beam_operand, buffer );
	    }
        else
	    {
	    print_linest( beam->elem_operand );
	    BUF_FORM("%s",beam->elem_operand->name);
	    }

	if ( (beam = beam->next) )
	    BUF_STRING(", ");
	}

    BUF_STRING(" )\0");
    return ( buffer );
    }

/* this handles all of the output destined for the standard format file */
static void print_buffert( buf )
char buf[];
    {
    if ( strlen( buf ) > 80 )
	{
	chop_buft( buf );
	}
    else
	{
	if ( standard_403 || standard_81 )  /* if MAD 4.03 or MAD 8.1 file */
	    no_under_all_uppert( buf ); /* take out underscores and capitalize*/

	/* this statement does all of the printing for the S.F. file */
        printf( "%s\n", buf );
        }
    }

static void no_under_all_uppert( buf )
char *buf;
    {
    register int source = 0;
    register int dest = 0;

    for ( ; buf[source]; source++ )
	if ( islower( buf[source] ) )
	    buf[dest++] = toupper( buf[source] );
	else
	    if ( buf[source] != '_' )
	        buf[dest++] = buf[source];

    buf[dest] = '\0';
    }

/* chops some chars off of a buffer, tacks on a &, prints them and the rest */
static void chop_buft( buf )
char *buf;
    {
    char work_buf[MAXSTRING];
    register char *tmp = buf + 72;
    int newlen;

    while ( *tmp != ',' && tmp > buf )
	--tmp;

    if ( tmp <= buf )
	{ /* oops, no ',' in the string - settle for any whitespace */
	tmp = buf + 72;
	while ( *tmp != ' ' && tmp > buf )
	    --tmp;
	
	if ( tmp <= buf )
	    {
	    fprintf( stderr, "can't split line \"%s\" in chop_buft()\n", buf );
	    exit( 1 );
	    }
	}

    newlen = tmp - buf + 2;
    sprintf( work_buf, "%.*s &", newlen, buf );
    print_buffert( work_buf );

    tmp += 2;
    while ( *tmp++ == ' ' )
	;

 
    /* add indent to additional lines */
    tmp--;

    for( newlen = INDENT; newlen >= 0; newlen-- )
	*--tmp = ' ';

    /* indent stuff ends here */

    print_buffert( tmp );
    }

static void print_linest( root )
symbol root;
    {
    symbol_node *sym_data;
    char buffer1[MAXSTRING];
    char *buffer = buffer1;
    char out_buff[4096];

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /*this makes sure it is only printed once*/
	    {
	    switch( sym_data->label_type )
	        {
	        case ATTR_DRIFT:
	        case ATTR_HMONITOR:
	        case ATTR_VMONITOR:
	        case ATTR_MONITOR:
	        case ATTR_INSTRUMENT:
	        case ATTR_WIGGLER:

	        case ATTR_SEXTUPOLE:
	        case ATTR_OCTUPOLE:
	        case ATTR_SOLENOID:
	        case ATTR_EL_SEPARATE:
	        case ATTR_HKICK:
	        case ATTR_VKICK:
	        case ATTR_KICKER:
	        
	        case ATTR_RFCAVITY:
		case ATTR_ECOLLIMATOR:
	        case ATTR_RCOLLIMATOR:

	        case ATTR_MARKER:

	        case ATTR_SROT:
	        case ATTR_YROT:
		    buff_reg_elemt( root, out_buff );
		    break;

	        case ATTR_QUADRUPOLE:
                    hold_tpot = read_tpot_table(root->name);
                    if (hold_tpot)
                       {
                       teapotify_quads= 1;
                       hold_tpot_info.old_name = hold_tpot[2];
                       hold_tpot_info.new_name = hold_tpot[3];
                       hold_tpot_info.tpot_type = hold_tpot[4];
                       hold_tpot_info.split = hold_tpot[5];
                       }
		    buff_reg_elemt( root, out_buff );
		    break;

	        case ATTR_RBEND:
	        case ATTR_SBEND:
                    hold_tpot = read_tpot_table(root->name);
                    if (hold_tpot)
                      {
                       hold_tpot_info.old_name = hold_tpot[2];
                       hold_tpot_info.conv_to_drift = hold_tpot[6];
                       if (*hold_tpot_info.conv_to_drift == '1')
                         teapotify_vbend= 1;
                       hold_tpot_info.conv_to_sbend = hold_tpot[7];
                       if (*hold_tpot_info.conv_to_sbend == '1')
                         {
                         teapotify_rbend= 1;
                         hold_tpot_info.new_name = hold_tpot[3];
                         }
                      }
		    buff_reg_elemt( root, out_buff );
		    break;

	        case ATTR_THINLENS:
	        case ATTR_TANK:
	        case ATTR_EDGE:
	        case ATTR_PMQ:
	        case ATTR_RFQCELL:
	        case ATTR_DOUBLET:
	        case ATTR_TRIPLET:
	        case ATTR_RFGAP:
	        case ATTR_SPECIAL:
	        case ATTR_ROTATION:
                    print_buffert( "This database contains a trace3d-specific element which");
                    print_buffert( "is not valid in TEAPOT.  This run will now terminate.");
                    print_buffert( "You may look at this lattice with the flat or trace3d option.");
		    fprintf( stderr, "This database contains a trace3d-specific element which \n" );
                    fprintf( stderr, "is not valid in TEAPOT.  This run will now terminate. \n");
                    fprintf( stderr, "You may look at this lattice with the flat or trace3d option.\n");
		    exit(1);
                    break;

	        case ATTR_MULTIPOLE:
		    buff_multipolet( root, out_buff );
		    break;

	        case GEOMETRY:
	        case STRENGTH:
		    expand_exprt( sym_data->expr_list->expr, buffer, 0 );

		    if ( !standard_403 )
			OUTBUFF2("%s := %s",root->name,buffer)
		    else
			OUTBUFF2("parameter, %s = %s",root->name,buffer)
		    break;

	        case BEAMLINE:
		case SLOT:
		    buffer_beam( sym_data->beam_root, buffer );
		    OUTBUFF4( "%s:%*cline = %s", root->name,
			( INDENT - strlen( root->name ) ), ' ', buffer );
		    break;

	        default:
		    fprintf( stderr, "something is amiss in print_linest\n" );

	        }
	    print_buffert( out_buff );

           if (teapotify_quads  && hold_tpot_info.split[0] != '\0')
             {
             BUF_FORM("%s:",hold_tpot_info.new_name);
             BUF_FORM2( "%*c", ( INDENT - strlen( hold_tpot_info.new_name ) ), ' ');
             BUF_FORM("%s, ","quadrupole");
             if (buf_hold_l[0] != '\0')
              {
               BUF_FORM("%s = ","L");
               BUF_FORM("%s / ", buf_hold_l);
               BUF_FORM("%s, ", hold_tpot_info.split);
              }
             if (buf_hold_tilt[0] != '\0')
              {
               BUF_FORM("%s = ","tilt");
               BUF_FORM("%s, ", buf_hold_tilt);
              }
             if (hold_tpot_info.tpot_type != '\0')
              {
               BUF_FORM("%s = ","type");
               BUF_FORM("%s, ", hold_tpot_info.tpot_type);
              }
             if (buf_hold_k1[0] != '\0')
              {
               BUF_FORM("%s = ","k1");
               BUF_FORM("%s", buf_hold_k1);
              }
             print_buffert( buffer1 );
             buffer = buffer1;

             BUF_FORM("%s:",hold_tpot_info.old_name);
             BUF_FORM2( "%*c", ( INDENT - strlen( hold_tpot_info.old_name ) ), ' ');
             BUF_FORM("%s = ( ","line");
             BUF_FORM("%s * ",hold_tpot_info.split);
             BUF_FORM("%s )",hold_tpot_info.new_name);
             teapotify_quads = 0;
             print_buffert( buffer1 );
            }

           if (teapotify_vbend)
             {
             BUF_FORM("%s:",hold_tpot_info.old_name);
             BUF_FORM2( "%*c", ( INDENT - strlen( hold_tpot_info.old_name ) ), ' ');
             BUF_FORM("%s, ","drift");
             if (buf_hold_l[0] != '\0')
              {
               BUF_FORM("%s = ","L");
               BUF_FORM("%s ", buf_hold_l);
              }
             teapotify_vbend = 0;
             print_buffert( buffer1 );
            }

           if (teapotify_rbend)
             {
             BUF_FORM("%s:",hold_tpot_info.new_name);
             BUF_FORM2( "%*c", ( INDENT - strlen( hold_tpot_info.new_name ) ), ' ');
             BUF_FORM("%s, ","sbend");
             if (buf_hold_l[0] != '\0')
              {
               BUF_FORM("%s = ","L");
               BUF_FORM("%s ", buf_hold_l);
              }
             if (buf_hold_angle[0] != '\0')
              {
               BUF_FORM("%s = ","ANGLE");
               BUF_FORM("%s, ", buf_hold_angle);
              }
             if (buf_hold_k1[0] != '\0')
              {
               BUF_FORM("%s = ","k1");
               BUF_FORM("%s", buf_hold_k1);
              }
             print_buffert( buffer1 );
             buffer = buffer1;

             BUF_FORM("PARAMETER, RAD%s = ", hold_tpot_info.old_name); 
             BUF_FORM("( %s ) / ", buf_hold_l); 
             BUF_FORM("( %s )", buf_hold_angle);
             print_buffert( buffer1 );
             buffer = buffer1;
 
             BUF_FORM("PARAMETER, K%s = ", hold_tpot_info.old_name); 
             BUF_FORM("sin( %s ) / ", buf_hold_l); 
             BUF_FORM("sin( %s ) / ", buf_hold_angle); 
             BUF_FORM("rad%s", hold_tpot_info.old_name); 
             print_buffert( buffer1 );
             buffer = buffer1;

             BUF_FORM("M%s:", hold_tpot_info.old_name); 
             BUF_FORM2("%*c", ( INDENT - strlen( hold_tpot_info.old_name ) - 1 ), ' ');
             BUF_FORM("%s, ","multipole");
             BUF_FORM("%s = -", "k1l"); 
             BUF_FORM("k%s", hold_tpot_info.old_name); 
             print_buffert( buffer1 );
             buffer = buffer1;

             BUF_FORM("%s: ", hold_tpot_info.old_name); 
             BUF_FORM("LINE = ( m%s, ", hold_tpot_info.old_name); 
             BUF_FORM("%s, ", hold_tpot_info.new_name); 
             BUF_FORM("m%s)", hold_tpot_info.old_name); 

             teapotify_rbend = 0;
             print_buffert( buffer1 );
            }

	   sym_data->declared = 0; /*this makes sure it is only printed once*/
	   }
	}
    }

static void param_buffert( expr, buffer )
exprnode *expr;
char *buffer;
    {
    expand_exprt( expr, buffer, 0 );
    }

static void buff_reg_elemt( ste, buffer )
symbol ste;
char *buffer;
    {
    char tmp_buf[MAXSTRING];
    int multipolify = 0;
    int j;

    symbol_node *sym_data = (symbol_node *)(ste->symbol_data);
    attrnode *next = sym_data->expr_list;

    if ( (sym_data->label_type == ATTR_QUADRUPOLE && teapotify_quads && hold_tpot_info.split[0] != '\0')
        || (sym_data->label_type == ATTR_RBEND && teapotify_rbend)
        || ((sym_data->label_type == ATTR_RBEND || 
            sym_data->label_type == ATTR_SBEND)  && teapotify_vbend) )
      {
       BUF_FORM( "!%s:", ste->name );
       BUF_FORM2( "%*c", ( INDENT - strlen( ste->name ) ), ' ');
      }
    else
      {
       BUF_FORM( "%s:", ste->name );
       BUF_FORM2( "%*c", ( INDENT - strlen( ste->name ) ), ' ');
      }

    /* this is where the nasty change from zero length sextupole to multipole */
    /* should go.  I don't like it.  FIX ME */
    if ( sym_data->label_type == ATTR_SEXTUPOLE || sym_data->label_type ==
	ATTR_OCTUPOLE )
	if ( eval_expr(find_attr(ATTR_L,sym_data->expr_list)->expr) == 0.0 )
	    multipolify = 1;

    BUF_FORM( "%s",(multipolify?"multipole":elem_names[sym_data->label_type]));
    
    while ( next )
    {
	if ( next->expr ) if ( next->expr->operation == EXPR_TILT )
	    {
	     BUF_FORM( ", %s ", "tilt" ); 
	    /* NOTREACHED */
	    }

	param_buffert( next->expr, tmp_buf );

	if ( multipolify )
	    {
	    switch ( next->attr ) {

	    case ATTR_L:
		break;

	    case ATTR_K2:
		BUF_FORM( ", %s = ", "k2l" );
		BUF_FORM( "%s", tmp_buf );
		break;

	    case ATTR_K3:
		BUF_FORM( ", %s = ", "k3l" );
		BUF_FORM( "%s", tmp_buf );
		break;

	    case ATTR_TILT:
		BUF_FORM( ", %s = ",
		    (sym_data->label_type == ATTR_SEXTUPOLE ? "t2":"t3") );
		BUF_FORM( "%s", tmp_buf );
		break;

	    default:
		fprintf( stderr, "\n***ERROR: illegal element attribute\n" );
		exit( 1 );
	    }
	} else {
	    if ( (standard_403 && next->attr == ATTR_HARMON) ||
		 (standard_81 && next->attr == ATTR_FREQ) )
	        /* don't print */
		/* both harmon AND freq get printed if they are both present */
		/* and no standards are in effect */
		;
	    else {
		/*
		**
		**  don't print anything on these conditions:
		**    - element is vkick and attribute is kickh
		**    - element is hkick and attribute is kickv
		**    - attribute is tilt
		**    - attribute is aperture or magnet size info
		**  print kick attr if:
		**    - attr is kick and elem is vkick or hkick
		**    - attr is kickv and elem is vkick
		**    - attr is kickh and elem is hkick
		**  otherwise print what you get
		**
		*/

		if ( next->expr->operation != EXPR_TILT
		  && next->attr < ATTR_MS_SHAPE 
		  && ( (next->attr & AP_MS_ATTR_BIT) ? aper_flag : 1) )
		{
		    if ( ( sym_data->label_type == ATTR_VKICK &&
			    next->attr == ATTR_KICKV ) ||
			 ( sym_data->label_type == ATTR_HKICK &&
			    next->attr == ATTR_KICKH ) )
		    {
			BUF_FORM( ", %s = ", elem_attr_names[ATTR_KICK] )
			BUF_FORM( "%s", tmp_buf );
		    } else
			if ( ( sym_data->label_type == ATTR_VKICK &&
				    next->attr == ATTR_KICKH ) ||
			     ( sym_data->label_type == ATTR_HKICK &&
				    next->attr == ATTR_KICKV ) )
			{
			    /* NOTHING */
			} else {
			    BUF_FORM( ", %s = ", elem_attr_names[next->attr] );
			    BUF_FORM( "%s", tmp_buf );
			}
		}

		if (teapotify_quads && hold_tpot_info.split[0] != '\0')
		   switch ( elem_attr_names[next->attr][0] )
		    {
		      case 'l':
			for ( j=0; j < strlen( tmp_buf ); j++ )
			 buf_hold_l[j] = tmp_buf[j];
			for ( ; j < 40; j++ )
			 buf_hold_l[j] = tmp_buf[j];
			break;
		      case 'k':
			for ( j=0; j < strlen( tmp_buf ); j++ )
			 buf_hold_k1[j] = tmp_buf[j];
			for ( ; j < 40; j++ )
			 buf_hold_k1[j] = tmp_buf[j];
			break;
		      case 't':
			for ( j=0; j < strlen( tmp_buf ); j++ )
			 buf_hold_tilt[j] = tmp_buf[j];
			for ( ; j < 40; j++ )
			 buf_hold_tilt[j] = tmp_buf[j];
			break;
		      default:
			printf( "problem in lattice_teapot.c - quad attr unknown\n");
			break;
		     }
		if (teapotify_vbend) 
		   switch ( elem_attr_names[next->attr][0] )
		    {
		      case 'l':
			for ( j=0; j < strlen( tmp_buf ); j++ )
			 buf_hold_l[j] = tmp_buf[j];
			for ( ; j < 40; j++ )
			 buf_hold_l[j] = tmp_buf[j];
			break;
		      default:
			break;                             
		     }
		if (teapotify_rbend)
		   switch ( elem_attr_names[next->attr][0] )
		    {
		      case 'l':
			for ( j=0; j < strlen( tmp_buf ); j++ )
			 buf_hold_l[j] = tmp_buf[j];
			for ( ; j < 40; j++ )
			 buf_hold_l[j] = tmp_buf[j];
			break;
		      case 'k':
			for ( j=0; j < strlen( tmp_buf ); j++ )
			 buf_hold_k1[j] = tmp_buf[j];
			for ( ; j < 40; j++ )
			 buf_hold_k1[j] = tmp_buf[j];
			break;
		      case 'a':
			for ( j=0; j < strlen( tmp_buf ); j++ )
			 buf_hold_angle[j] = tmp_buf[j];
			for ( ; j < 40; j++ )
			 buf_hold_angle[j] = tmp_buf[j];
			break;
		      default:
			break;
		     }
	    }
	}

	next = next->next;
    }
 
    if ( sym_data->sub_type )
	if ( sym_data->sub_type != '\0' )
	    BUF_FORM( ", type = %s", sym_data->sub_type );
    
    if (teapotify_quads && hold_tpot_info.split[0] == '\0')
       {
       BUF_FORM( ", type = %s", "ir" );
       teapotify_quads = 0;
       }
	
    }

static void buff_multipolet( ste, buffer )
symbol ste;
char *buffer;
{
    char tmp_buf[MAXSTRING];

    symbol_node *sym_data = (symbol_node *)(ste->symbol_data);
    attrnode *next = sym_data->expr_list;

    BUF_FORM( "%s:", ste->name );
    BUF_FORM2( "%*c", ( INDENT - strlen( ste->name ) ), ' ');
    BUF_FORM( "%s", elem_names[sym_data->label_type] );
    
    while ( next )
    {
	if ( next->expr )
	    if ( next->expr->operation == EXPR_TILT )
	    {
		if ( next->attr <= ATTR_T9 && next->attr >= ATTR_T0 )
		{
		    BUF_FORM( ", %s", multi_attr_names[next->attr] );
		} else {
		    fprintf( stderr, "\n*** dbsf: illegal '\\' in %s %s %s\n",
		      multi_attr_names[next->attr], "attribute of element",
		      ste->name );
		    exit( 1 );
		    /* NOTREACHED */
		}
	    } else {
		if ( next->attr < ATTR_MS_SHAPE
		  && ( (next->attr & AP_MS_ATTR_BIT) ? aper_flag : 1) )
		{
		    param_buffert( next->expr, tmp_buf );
		    BUF_FORM( ", %s = ", multi_attr_names[next->attr] );
		    BUF_FORM( "%s", tmp_buf );
		}
	    }

	next = next->next;
    }
	
    if ( sym_data->sub_type )
	if ( sym_data->sub_type != '\0' )
	    BUF_FORM( ", type = %s", sym_data->sub_type );
}

static void find_beam_paramst( root )
beamnode *root;
    {
    char buffer[MAXSTRING];

    if ( root )
	{
	expand_exprt( root->iteration, buffer, 0 );
	print_paramst( root->elem_operand );
	find_beam_paramst( root->beam_operand );

	find_beam_paramst( root->next );
	}
    }

static void print_paramst( root )
symbol root;
    {
    symbol_node *sym_data;
    char buffer[MAXSTRING];
    register attrnode *next;

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /* this makes sure it is only printed once */
	    {
	    switch ( sym_data->label_type )
		{
		case ATTR_DRIFT:
		case ATTR_HMONITOR:
		case ATTR_VMONITOR:
		case ATTR_MONITOR:
		case ATTR_INSTRUMENT:
		case ATTR_WIGGLER:
		case ATTR_RBEND:
		case ATTR_SBEND:
		case ATTR_QUADRUPOLE:
		case ATTR_SEXTUPOLE:
		case ATTR_OCTUPOLE:
		case ATTR_MULTIPOLE:
		case ATTR_SOLENOID:
		case ATTR_RFCAVITY:
		case ATTR_EL_SEPARATE:
		case ATTR_SROT:
		case ATTR_YROT:
		case ATTR_HKICK:
		case ATTR_VKICK:
		case ATTR_KICKER:
		case ATTR_MARKER:
		case ATTR_ECOLLIMATOR:
		case ATTR_RCOLLIMATOR:
		    next = sym_data->expr_list;

		    while ( next )
		    {
		      if ( next->expr)
		      {
			if (next->expr->operation != EXPR_TILT)
			{
			  expand_exprt( next->expr, buffer, 0 );
			} else {
/* XXX
			  if ( sym_data->label_type != ATTR_MULTIPOLE
			    || next->attr > ATTR_T9 || next->attr < ATTR_T0 )
			  {
			    fprintf( stderr,
			      "\n*** dbsf: illegal '\\' %s '%s'\n",
			      "found in attribute of element", root->name );
			    exit( 1 );
			  }
XXX */
			}
		      }

		      next = next->next;
		    }

		    break;
		    
	        case ATTR_THINLENS:
	        case ATTR_TANK:
	        case ATTR_EDGE:
	        case ATTR_PMQ:
	        case ATTR_RFQCELL:
	        case ATTR_DOUBLET:
	        case ATTR_TRIPLET:
	        case ATTR_RFGAP:
	        case ATTR_SPECIAL:
	        case ATTR_ROTATION:
                    print_buffert( "This database contains a trace3d-specific element which");
                    print_buffert( "is not valid in TEAPOT.  This run will now terminate.");
                    print_buffert( "You may look at this lattice with the flat or trace3d option.");
		    fprintf( stderr, "This database contains a trace3d-specific element which \n" );
                    fprintf( stderr, "is not valid in TEAPOT.  This run will now terminate. \n");
                    fprintf( stderr, "You may look at this lattice with the flat or trace3d option.\n");
		    exit(1);
                    break;

		case GEOMETRY:
		case STRENGTH:
		    print_linest( root );
		    break;

		case BEAMLINE:
		case SLOT:
	    	    find_beam_paramst( sym_data->beam_root );
		    break;

		case PARAM_LINE:
		    break;
		}
	    }
	}
    }

static void find_beam_elemst( root )
beamnode *root;
    {
    if ( root )
	{
	print_elemst( root->elem_operand );
	find_beam_elemst( root->beam_operand );
	find_beam_elemst( root->next );
	}
    }

static void print_elemst( root )
symbol root;
    {
    symbol_node *sym_data;

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /* this makes sure it is only printed once */
	    {
	    switch ( sym_data->label_type )
		{
		case ATTR_DRIFT:
		case ATTR_HMONITOR:
		case ATTR_VMONITOR:
		case ATTR_MONITOR:
		case ATTR_INSTRUMENT:
		case ATTR_WIGGLER:
		case ATTR_RBEND:
		case ATTR_SBEND:
		case ATTR_QUADRUPOLE:
		case ATTR_SEXTUPOLE:
		case ATTR_OCTUPOLE:
		case ATTR_MULTIPOLE:
		case ATTR_SOLENOID:
		case ATTR_RFCAVITY:
		case ATTR_EL_SEPARATE:
		case ATTR_SROT:
		case ATTR_YROT:
		case ATTR_HKICK:
		case ATTR_VKICK:
		case ATTR_KICKER:
		case ATTR_MARKER:
		case ATTR_ECOLLIMATOR:
		case ATTR_RCOLLIMATOR:
		    print_linest( root );
		    break;
		    
	        case ATTR_THINLENS:
	        case ATTR_TANK:
	        case ATTR_EDGE:
	        case ATTR_PMQ:
	        case ATTR_RFQCELL:
	        case ATTR_DOUBLET:
	        case ATTR_TRIPLET:
	        case ATTR_RFGAP:
	        case ATTR_SPECIAL:
	        case ATTR_ROTATION:
                    print_buffert( "This database contains a trace3d-specific element which");
                    print_buffert( "is not valid in TEAPOT.  This run will now terminate.");
                    print_buffert( "You may look at this lattice with the flat or trace3d option.");
		    fprintf( stderr, "This database contains a trace3d-specific element which \n" );
                    fprintf( stderr, "is not valid in TEAPOT.  This run will now terminate. \n");
                    fprintf( stderr, "You may look at this lattice with the flat or trace3d option.\n");
		    exit(1);
                    break;

		case GEOMETRY:
		case STRENGTH:
		    break;

		case BEAMLINE:
		case SLOT:
	    	    find_beam_elemst( sym_data->beam_root );
		    break;

		case PARAM_LINE:
		    break;
		}
	    }
	}
    }

void print_teapot( root, eval_exprs, group_lines, std403, std81, print_aper )
symbol root;
int eval_exprs, group_lines, std403, std81, print_aper;
    {
    extern int flat_expr;
    extern int standard_403;
    extern int standard_81;

    flat_expr = eval_exprs;
    standard_403 = std403;
    standard_81 = std81;
    aper_flag = print_aper;

    open_db();

    if ( group_lines )
	{
	print_paramst( root );

        printf( "\n" );

        print_elemst( root );

        printf( "\n" );
	}

    print_linest( root );

    close_db();

    }

/* END OF FILE */

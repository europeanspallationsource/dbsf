
/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/sds_defines.h,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

/* SDS DEFINES *************************************** */

#define false 0
#define true 1
#define forward 1
#define backward -1

#define LATTICE_BASE_LEVEL 0
#define LATTICE_SLOT_LEVEL 1
#define LATTICE_SUPERSLOT_LEVEL 2

/* These define the row_struct array without using cah pointers:
   somewhat wasteful of space but will allow fairly easy
   transport between programs and languages even without SDS
 */
#define NAME_MAX 64
#define COLUMN_MAX 22

/* Not sure about these ones: rather no use 'em if possible */
#define PARAMETER_MAX   100

#define ELEMENT_MAX   200

#define LATTICE_MAX   30000

#define ATTRIBUTE_MAX   10
#define MORE_MAX    10


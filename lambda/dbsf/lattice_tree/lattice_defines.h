
/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_defines.h,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

/* defines for lattice tree representation */
/* used by mad and dbsf */

/* if ANY CHANGES to these defines are made, ALL mad and dbsf source files */
/* must be recompiled */

/* element attributes */

#define ATTR_NULL	0

/* ATTR_K.Ls must be contiguous */

#define ATTR_K0L	1	/* multipole */
#define ATTR_K1L	2	
#define ATTR_K2L	3
#define ATTR_K3L	4
#define ATTR_K4L	5
#define ATTR_K5L	6
#define ATTR_K6L	7
#define ATTR_K7L	8
#define ATTR_K8L	9
#define ATTR_K9L	10

/* ATTR_Ts must be contiguous */

#define ATTR_T0		11	/* multipole */
#define ATTR_T1		12
#define ATTR_T2		13
#define ATTR_T3		14
#define ATTR_T4		15
#define ATTR_T5		16
#define ATTR_T6		17
#define ATTR_T7		18
#define ATTR_T8		19
#define ATTR_T9		20

/* attributes for other elements */

#define ATTR_ANGLE	1	/* sbend srot yrot */
#define ATTR_BETRF	2	/* rfcavity */
#define ATTR_E		3	/* elseparate */
#define ATTR_E1		4	/* sbend */
#define ATTR_E2		5	/* sbend */
#define ATTR_FINT	6	/* sbend */
#define ATTR_FREQ	7	/* rfcavity */
#define ATTR_H1		8	/* sbend */
#define ATTR_H2		9	/* sbend */
#define ATTR_HARMON	10	/* rfcavity, rfgap */
#define ATTR_HGAP	11	/* sbend */
#define ATTR_KICKH	12	/* kicker */
#define ATTR_K1		13	/* sbend quadrupole pmq doublet*/
#define ATTR_K2		14	/* sbend sextupole */
#define ATTR_K3		15	/* sbend octupole */
#define ATTR_KICK	16	/* hkick vkick */
#define ATTR_KS		17	/* solenoid */
#define ATTR_L		18	/* all except: multipole srot yrot marker */
#define ATTR_LAG	19	/* rfcavity */
#define ATTR_PG		20	/* rfcavity */
#define ATTR_SHUNT	21	/* rfcavity */
#define ATTR_TFILL	22	/* rfcavity */
#define ATTR_TILT	23      /* sbend quadrupole sextupole octupole elseparate hkick vkick */
#define ATTR_KICKV	24	/* kicker */
#define ATTR_VOLT	25	/* rfcavity */
#define ATTR_XSIZE	26	/* ecollimator rcollimator */
#define ATTR_YSIZE	27	/* ecollimator rcollimator */
#define ATTR_XFOCAL	28	/* thin lens */
#define ATTR_YFOCAL	29	/* thin lens */
#define ATTR_ZFOCAL	30	/* thin lens */
#define ATTR_IN_RAD	31	/* pmq */
#define ATTR_OUT_RAD	32	/* pmq */
#define ATTR_DIST_BETW	33	/* doublet, triplet */
#define ATTR_BFLD_IN	34	/* triplet */
#define ATTR_BFLD_OUT	35	/* triplet */
#define ATTR_L_IN	36	/* triplet */
#define ATTR_L_OUT	37	/* triplet */
#define ATTR_RADIUS	38	/* edge */
#define ATTR_GAP	39	/* edge */
#define ATTR_FF1	40	/* edge */
#define ATTR_FF2	41	/* edge */
#define ATTR_EGAPVOLT	42	/* rfgap */
#define ATTR_PHASE	43	/* rfgap */
#define ATTR_EGFLAG	44	/* rfgap */
#define ATTR_DWFLAG	45	/* rfgap */
#define ATTR_VR2	46	/* rfqcell */
#define ATTR_AV 	47	/* rfqcell */
#define ATTR_CELL	48	/* rfqcell */
#define ATTR_ACCEL	49	/* tank */
#define ATTR_IDEN_CAV	50	/* tank */

/* new for aperture and magnet size info */
/* must be contiguous */
#define ATTR_AP_SHAPE	64	/* all */
#define ATTR_AP_XSIZE	65	/* all */
#define ATTR_AP_YSIZE	66	/* all */
#define ATTR_AP_CENTERX	67	/* all */
#define ATTR_AP_CENTERY	68	/* all */
#define ATTR_MS_SHAPE	69	/* all */
#define ATTR_MS_XSIZE	70	/* all */
#define ATTR_MS_YSIZE	71	/* all */
#define ATTR_MS_CENTERX	72	/* all */
#define ATTR_MS_CENTERY	73	/* all */

/* new for TraceWIN support */
/* Add all the newly added attributes */
/* All of these are subject to change .... */
#define ATTR_LENGTH	74	/* dtl_cell */
#define ATTR_LQ1	75
#define ATTR_LQ2	76
#define ATTR_CELLCENTER	77
#define ATTR_B1P	78
#define ATTR_B2P	79
#define ATTR_E0TL	80
#define ATTR_RFPHASE	81
#define ATTR_APERTURE	82
#define ATTR_ABSPHASE	83
#define ATTR_BETAS	84
#define ATTR_TTIME	85
#define ATTR_KTSP	86
#define ATTR_K2TSP	87

#define ATTR_MODE	88	/* ncells */
#define ATTR_CELLNUMBER	89
#define ATTR_BETAG	90
#define ATTR_E0T	91
#define ATTR_KE0TI	95
#define ATTR_KE0TO	96
#define ATTR_DZI	97
#define ATTR_DZO	98
#define ATTR_K2TSPP	99
#define ATTR_TTIMEIN	100
#define ATTR_KTIP	101
#define ATTR_K2TIPP	102
#define ATTR_TTIMEOUT	103
#define ATTR_KTOP	104
#define ATTR_K2TOPP	105


#define ATTR_MEANV	106	/* rfq_cell */
#define ATTR_VANER	107
#define ATTR_ACCPARAM	108
#define ATTR_MODULATION	109
#define ATTR_CELLTYPE	110
#define ATTR_TCURV	111
#define ATTR_TFOCUS	112

#define ATTR_DTILT	113	/* discrete tilt for bend */

#define ATTR_HANGLE	114	/* closed_orbit_corrector */
#define ATTR_VANGLE	115	

/* if this bit is set in an ATTR_ value it is an aperture or magnet size attr */
#define AP_MS_ATTR_BIT	64

/* can be any unused ATTR_attr value */
#define A_DELIM			128

/* ATTR_elements must be contiguous to index valid attribute mask array */
/* changes here must be reflected in: */
/* lattice_gdecls: elem_names[] */
/* mad_gdecls: elem_masks[] */
/* dbsf_gdecls: str_attr[] */

#define ATTR_NULL		0
#define ATTR_DRIFT		1
#define ATTR_HMONITOR		2
#define ATTR_VMONITOR		3
#define ATTR_MONITOR		4
#define ATTR_INSTRUMENT		5
#define ATTR_WIGGLER		6
#define ATTR_RBEND		7
#define ATTR_SBEND		8
#define ATTR_QUADRUPOLE		9
#define ATTR_SEXTUPOLE		10
#define ATTR_OCTUPOLE		11
#define ATTR_MULTIPOLE		12
#define ATTR_SOLENOID		13
#define ATTR_RFCAVITY		14
#define ATTR_EL_SEPARATE	15
#define ATTR_SROT		16
#define ATTR_YROT		17
#define ATTR_HKICK		18
#define ATTR_VKICK		19
#define ATTR_KICKER		20
#define ATTR_MARKER		21
#define ATTR_ECOLLIMATOR	22
#define ATTR_RCOLLIMATOR	23
#define ATTR_TBEND		24
#define ATTR_THINLENS		25
#define ATTR_TANK		26
#define ATTR_EDGE		27
#define ATTR_PMQ		28
#define ATTR_RFQCELL		29
#define ATTR_DOUBLET		30
#define ATTR_TRIPLET		31
#define ATTR_RFGAP		32
#define ATTR_SPECIAL		33
#define ATTR_ROTATION		34

/* these are included for the purposes of flat and SDS outputs */
#define ATTR_BEAMLINE		35
#define ATTR_SLOT		36
#define ATTR_SUPERSLOT		37
#define ATTR_TCELEMENT		38

#define ATTR_DTL_CELL		39
#define ATTR_NCELLS		40
#define ATTR_RFQ_CELL		41

#define ATTR_BEND		42
#define ATTR_COC		43
#define ATTR_COMMAND		44
#define ATTR_ZERO		45
#define ATTR_ONE		46
#define ATTR_MAGNET_PIECE_TYPE	47



/* other types for symbol table entries (can't overlap ATTR_ values) */

#define GEOMETRY                61
#define STRENGTH                62
#define BEAMLINE                63
#define PARAM_LINE              64
#define SLOT                    65

/* the maximum size of strings */
#define MAXSTRING		255

/* to set declaration/count flags */
#define DECLARED 1
#define COUNTED 2
#define CLEARED 0

/* END OF FILE */

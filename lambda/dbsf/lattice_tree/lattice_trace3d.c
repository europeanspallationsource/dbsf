
/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_trace3d.c,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

/*       Prints a "flat file" format for the trace3d program     */
/* written by Ellen Syphers (utilizing code written by Eric Barr) */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "lattice_flat.h"
#include "lattice_gdecls.h"
#include "lattice_tree.h"
#include "lattice_defines.h"

int seq_num = 0;
int rownum = 1;

#define BUF_FORMXYZ(x,y,z)  { sprintf( buffer, x, y, z ); }
#define BUF_FORMXY(x,y)  { sprintf( buffer, x, y ); } 
#define PRINT_COMMA     { fputc(',', stdout); } 
#define PRINT_EOL	{ fputc('\n', stdout); }

/* 1 is the trace3d element number for drift */
int drift_trace3d[] = { ATTR_L, ATTR_NULL };

/* 2 is the trace3d element number for thinlens */
int thinlens_trace3d[] = { ATTR_XFOCAL, ATTR_YFOCAL, ATTR_ZFOCAL, ATTR_NULL };

/* 3 for quadrupole */
int quadrupole_trace3d[] = { ATTR_K1, ATTR_L, ATTR_NULL };

/* 4  for pmq */
int pmq_trace3d[] = { ATTR_K1, ATTR_L, ATTR_IN_RAD, ATTR_OUT_RAD, ATTR_NULL };

/*  5 for solenoid  */
int solenoid_trace3d[] = { ATTR_KS, ATTR_L, ATTR_NULL };

/*  6 for doublet */
int doublet_trace3d[] = { ATTR_K1, ATTR_L, ATTR_DIST_BETW, ATTR_NULL };

/*  7 for triplet */
int triplet_trace3d[]={ATTR_BFLD_OUT, ATTR_L_OUT, ATTR_DIST_BETW, ATTR_BFLD_IN, ATTR_L_IN, ATTR_NULL};

/* 8 for bend  */
int sbend_trace3d[] = { ATTR_ANGLE, ATTR_L, ATTR_K1, ATTR_NULL };
int rbend_trace3d[] = { ATTR_ANGLE, ATTR_L, ATTR_K1, ATTR_NULL };

/* 9 for edge  */
int edge_trace3d[] = { ATTR_ANGLE, ATTR_RADIUS, ATTR_GAP, ATTR_FF1, ATTR_FF2, ATTR_NULL };

/* 10 for rfgap  */
int rfgap_trace3d[]={ATTR_EGAPVOLT, ATTR_PHASE, ATTR_EGFLAG, ATTR_DWFLAG, ATTR_HARMON, ATTR_NULL};

/* 11 for rfq cell  */
int rfqcell_trace3d[]={ATTR_VR2, ATTR_AV, ATTR_L, ATTR_PHASE, ATTR_CELL, ATTR_NULL};

/*  12 for rf_cavity */
int rfcavity_trace3d[] = { ATTR_VOLT, ATTR_L, ATTR_LAG, ATTR_NULL };

/*  13 for tank */
int tank_trace3d[] = { ATTR_ACCEL, ATTR_L, ATTR_PHASE, ATTR_IDEN_CAV, ATTR_NULL };

/*  14 for special, user defined */
int special_trace3d[] = { ATTR_L, ATTR_NULL };

/*  15 for rotation */
int rotation_trace3d[] = { ATTR_ANGLE, ATTR_NULL };

/* this is commented out */
/*int multipole_trace3d[] = { ATTR_K1, ATTR_L, ATTR_NULL };
int octupole_trace3d[] = { ATTR_K3, ATTR_L, ATTR_NULL };
int sextupole_trace3d[] = { ATTR_K3, ATTR_L, ATTR_NULL };
int hmonitor_trace3d[] = { ATTR_L, ATTR_NULL };
int vmonitor_trace3d[] = { ATTR_L, ATTR_NULL };
int monitor_trace3d[] = { ATTR_L, ATTR_NULL };
int instrument_trace3d[] = { ATTR_L, ATTR_NULL };
int wiggler_trace3d[] = { ATTR_L, ATTR_NULL };
int elseparate_trace3d[] = { ATTR_L, ATTR_E, ATTR_TILT, ATTR_NULL };
int srot_trace3d[] = { ATTR_ANGLE, ATTR_NULL };
int yrot_trace3d[] = { ATTR_ANGLE, ATTR_NULL };
int hkick_trace3d[] = { ATTR_L, ATTR_TILT, ATTR_KICK, ATTR_NULL };
int vkick_trace3d[] = { ATTR_L, ATTR_TILT, ATTR_KICK, ATTR_NULL };
int kicker_trace3d[] = { ATTR_L, ATTR_TILT, ATTR_KICKH, ATTR_KICKV, ATTR_NULL };
int marker_trace3d[] = { ATTR_NULL };
int ecollimator_trace3d[] = { ATTR_L, ATTR_XSIZE, ATTR_NULL };
int rcollimator_trace3d[] = { ATTR_L, ATTR_XSIZE, ATTR_NULL };
int tbend_trace3d[] = { ATTR_NULL };   */

/* 17 for all others */
int multipole_trace3d[] = { ATTR_NULL };
int octupole_trace3d[] = { ATTR_NULL };
int sextupole_trace3d[] = { ATTR_NULL };
int hmonitor_trace3d[] = { ATTR_NULL };
int vmonitor_trace3d[] = { ATTR_NULL };
int monitor_trace3d[] = { ATTR_NULL };
int instrument_trace3d[] = { ATTR_NULL };
int wiggler_trace3d[] = { ATTR_NULL };
int elseparate_trace3d[] = { ATTR_NULL };
int srot_trace3d[] = { ATTR_NULL };
int yrot_trace3d[] = { ATTR_NULL };
int hkick_trace3d[] = { ATTR_NULL };
int vkick_trace3d[] = { ATTR_NULL };
int kicker_trace3d[] = { ATTR_NULL };
int marker_trace3d[] = { ATTR_NULL };
int ecollimator_trace3d[] = { ATTR_NULL };
int rcollimator_trace3d[] = { ATTR_NULL };
int tbend_trace3d[] = { ATTR_NULL }; 

static int *elem_trace3d_forms[] =
    {
    (int *) 0,
    drift_trace3d,
    hmonitor_trace3d,
    vmonitor_trace3d,
    monitor_trace3d,
    instrument_trace3d,
    wiggler_trace3d,
    rbend_trace3d,
    sbend_trace3d,
    quadrupole_trace3d,
    sextupole_trace3d,
    octupole_trace3d,
    multipole_trace3d,
    solenoid_trace3d,
    rfcavity_trace3d,
    elseparate_trace3d,
    srot_trace3d,
    yrot_trace3d,
    hkick_trace3d,
    vkick_trace3d,
    kicker_trace3d,
    marker_trace3d,
    ecollimator_trace3d,
    rcollimator_trace3d,
    tbend_trace3d,
    thinlens_trace3d,
    tank_trace3d,
    edge_trace3d,
    pmq_trace3d,
    rfqcell_trace3d,
    doublet_trace3d,
    triplet_trace3d,
    rfgap_trace3d,
    special_trace3d,
    rotation_trace3d
    };

static int trace3d_elems[] =
    {
    17,
    1,	/* drift */
    17,	/* hmonitor */
    17,	/* vmonitor */
    17,	/* monitor */
    17,	/* instrument */
    17,	/* wiggler */
    8,	/* rbend */
    8,	/* sbend */
    3,	/* quadrupole */
    17,	/* sextupole */
    17,	/* octupole */
    17,	/* multipole */
    5,	/* solenoid */
    12,	/* rfcavity */
    17,	/* elseparate */
    17,	/* srot   */
    17,	/* yrot   */
    17,	/* hkick  */
    17,	/* vkick  */
    17,	/* kicker */
    17,	/* marker */
    17,	/* ecollimator */
    17,	/* rcollimator */
    17,	/* tbend  */
    2,	/* thin lens  */
    13,	/* tank  */
    9,  /* edge  */
    4,  /* pmq  */
    11, /* rfqcell  */
    6,  /* doublet */
    7,  /* triplet */
    10, /* rfgap  */
    14, /* special  */
    15  /* rotation  */
    };

  double pi;

/* function forward decls */
static void buffer_beam();
static void buffer_beam_backwards();

/* OUTPUT ROUTINE */

static void printit( buf )
char buf[];
    {
    /* this statement does all of the printing for the output file */
    /* not including error messages */
    printf( "%s", buf );
    }

/* ELEMENT BUFFERING ROUTINES */

int bend_dir;
int one_time_flag = 0;
double rho, n;

static void find_print_t3d_arg( ste, attrnum )
symbol ste;
int attrnum;
    {
    symbol_node *sym_data = (symbol_node *)(ste->symbol_data);
    attrnode *exprlist = sym_data->expr_list;
    char buffer1[MAXSTRING];
    char *buffer = buffer1;
   
    register int count;
    register int index;

    double save_length, save_k1, save_angle, save_sol_l;
    attrnode *len_attr;
    attrnode *k1_attr;
    attrnode *angle_attr;
    attrnode *sol_l_attr;

    pi = 2 * asin(1.0);
    index = sym_data->label_type;
    count = trace3d_elems[index];
   while ( exprlist )
    {
     if ( exprlist->attr == attrnum )
      {
      if ( ( count==8 ) && (one_time_flag == 0) )
        {
         if (attrnum == ATTR_ANGLE)
           {
            save_angle = eval_expr( exprlist->expr );
            if ( (len_attr = find_attr( ATTR_L, sym_data->expr_list )) )
              if ( len_attr->expr )
                save_length = eval_expr(len_attr->expr);
            if ( (k1_attr = find_attr( ATTR_K1, sym_data->expr_list )) )
              if ( k1_attr->expr )
                save_k1 = eval_expr( k1_attr->expr );
            one_time_flag = 1;
           }
         if (attrnum == ATTR_L)
           {
            save_length = eval_expr( exprlist->expr );
            if ( (angle_attr = find_attr( ATTR_ANGLE, sym_data->expr_list )) )
              if ( angle_attr->expr )
                save_angle = eval_expr(angle_attr->expr);
            if ( (k1_attr = find_attr( ATTR_K1, sym_data->expr_list )) )
              if ( k1_attr->expr )
                save_k1 = eval_expr( k1_attr->expr );
            one_time_flag = 1;
           }
         if (attrnum == ATTR_K1)
           {
            save_k1 = eval_expr( exprlist->expr );
            if ( (angle_attr = find_attr( ATTR_ANGLE, sym_data->expr_list )) )
              if ( angle_attr->expr )
                save_angle = eval_expr(angle_attr->expr);
            if ( (len_attr = find_attr( ATTR_L, sym_data->expr_list )) )
              if ( len_attr->expr )
                save_length = eval_expr( len_attr->expr );
            one_time_flag = 1;
           }
        rho = (save_length/save_angle);
        n = -(rho*rho)*save_k1;
        }
      if ( count == 12 )  /*  rfcavity  */
        if (attrnum == ATTR_VOLT)
          {
           if ( (sol_l_attr = find_attr( ATTR_L, sym_data->expr_list )) )
            if ( sol_l_attr->expr )
              save_sol_l = eval_expr(sol_l_attr->expr);
           }

      switch ( attrnum )
       {
        case ( ATTR_ANGLE ):
          BUF_FORMXYZ( ", a(1,%i) = %-11.9g", seq_num, ceil(bend_dir*(180/pi)*eval_expr(exprlist->expr)) )
          break;

        case ( ATTR_L ):
         switch( index )
          {
          case ( 1 ):
           BUF_FORMXYZ(", a(1,%i) = %-11.9g", seq_num, 1000*eval_expr( exprlist->expr ))
           break;
          case ( 9 ):
          case ( 28 ):
          case ( 14 ):
          case ( 26 ):
          case ( 13 ):
          case ( 30 ):
           BUF_FORMXYZ(", a(2,%i) = %-11.9g", seq_num, 1000*eval_expr( exprlist->expr ) )
           break;
          case ( 29 ):
           BUF_FORMXYZ(", a(3,%i) = %-11.9g",seq_num,1000*eval_expr( exprlist->expr ) )
           break;
          case( 8 ):
           BUF_FORMXYZ(", a(2,%i) = %-11.9g", seq_num, rho )
           break;
          case( 7 ):
           BUF_FORMXYZ(", a(2,%i) = %-11.9g", seq_num, 500*eval_expr(exprlist->expr)/( 2*sin(save_angle/2) ) )
           break;
          }
         break; 

        case ( ATTR_K1 ):
          if ( index == 7 || index == 8 )
            BUF_FORMXYZ(", a(3,%i) = %-11.9g", seq_num, n )
          else BUF_FORMXYZ(", a(1,%i) = %-11.9g", seq_num, eval_expr(exprlist->expr) )
          break;
   
        case ( ATTR_XFOCAL ):
          BUF_FORMXYZ(", a(1,%i) = %-11.9g", seq_num, 1000*eval_expr( exprlist->expr ))
          break;

        case ( ATTR_BFLD_OUT ):
        case ( ATTR_EGAPVOLT ):
        case ( ATTR_ACCEL ):
          BUF_FORMXYZ(", a(1,%i) = %-11.9g", seq_num, eval_expr( exprlist->expr ))
          break;

        case ( ATTR_KS ):
          BUF_FORMXYZ(", a(1,%i) = %-11.9g", seq_num, 10000*eval_expr( exprlist->expr ))  /* 10**4 gauss = 1 tesla */ 
          break;

        case ( ATTR_VR2 ):
          BUF_FORMXYZ(", a(1,%i) = %-11.9g", seq_num, 1000*1000*eval_expr( exprlist->expr ))
          break;

        case ( ATTR_YFOCAL ):
        case ( ATTR_L_OUT ):
        case ( ATTR_RADIUS ):
          BUF_FORMXYZ(", a(2,%i) = %-11.9g", seq_num, 1000*eval_expr( exprlist->expr ))
          break;

        case ( ATTR_AV ):
          BUF_FORMXYZ(", a(2,%i) = %-11.9g", seq_num, eval_expr( exprlist->expr ))
          break;

        case ( ATTR_ZFOCAL ):
        case ( ATTR_IN_RAD ):
        case ( ATTR_DIST_BETW ):
        case ( ATTR_GAP ):
          BUF_FORMXYZ(", a(3,%i) = %-11.9g", seq_num, 1000*eval_expr( exprlist->expr ))
          break;

        case ( ATTR_EGFLAG ):
          BUF_FORMXYZ(", a(3,%i) = %-11.9g", seq_num, eval_expr( exprlist->expr ))
          break;

        case ( ATTR_OUT_RAD ):
          BUF_FORMXYZ(", a(4,%i) = %-11.9g", seq_num, 1000*eval_expr( exprlist->expr ))
          break;

        case ( ATTR_BFLD_IN ):
        case ( ATTR_FF1 ):
        case ( ATTR_DWFLAG ):
        case ( ATTR_IDEN_CAV ):
          BUF_FORMXYZ(", a(4,%i) = %-11.9g", seq_num, eval_expr( exprlist->expr ))
          break;

        case ( ATTR_L_IN ):
          BUF_FORMXYZ(", a(5,%i) = %-11.9g", seq_num, 1000*eval_expr( exprlist->expr ))
          break;

        case ( ATTR_HARMON ):
        case ( ATTR_FF2 ):
        case ( ATTR_CELL ):
          BUF_FORMXYZ(", a(5,%i) = %-11.9g", seq_num, eval_expr( exprlist->expr ))
          break;

        case ( ATTR_VOLT ):
          BUF_FORMXYZ(", a(1,%i) = %-11.9g", seq_num, eval_expr( exprlist->expr ) / save_sol_l )
          break;

        case ( ATTR_LAG ):
           BUF_FORMXYZ(", a(3,%i) = %-11.9g", seq_num, eval_expr(exprlist->expr) )

        case ( ATTR_PHASE ):
          if (count == 10 )
           BUF_FORMXYZ(", a(2,%i) = %-11.9g", seq_num, eval_expr(exprlist->expr) )
          if (count == 11 )
           BUF_FORMXYZ(", a(4,%i) = %-11.9g", seq_num, eval_expr( exprlist->expr ))
          if (count == 13 )
           BUF_FORMXYZ(", a(3,%i) = %-11.9g", seq_num, eval_expr( exprlist->expr ))
          break;

        default:    
	  BUF_FORMXYZ(", a(1,%i) = %-11.9g", seq_num, eval_expr( exprlist->expr ))
          break;
       }

       if (attrnum != ATTR_MARKER) /* don't print out for markers */
         printit( buffer );
       return;
      }
     else
       exprlist = exprlist->next;
    }
   return;
   }


static void print_elem_trace3d( ste )
symbol ste;
    {
    symbol_node *sym_data = (symbol_node *)ste->symbol_data;

    char buffer1[MAXSTRING];
    char *buffer = buffer1;
    register int index;
    register int count;
/*    buffer1[MAXSTRING] = NULL;   */

    bend_dir = 1;
    index = sym_data->label_type;
    count = trace3d_elems[index];

    if( count == 8 )
	{
	register attrnode *exprlist = sym_data->expr_list;

	while ( exprlist )
	    if ( exprlist->attr == ATTR_TILT )
		{
		if( fabs(eval_expr( exprlist->expr ) - HALFPI) < TOLERANCE )
                   {}
		else if( fabs(eval_expr( exprlist->expr ) + HALFPI) < TOLERANCE )
                   bend_dir = -1;
		 else
	           if ( fabs(eval_expr( exprlist->expr )) > TOLERANCE )
	             fprintf( stderr, "*** ERROR: unsupported magnet tilt != 90,-90,0\n" );
		break;
		}
	    else
		exprlist = exprlist->next;
	}
    if (index != 21)  /* if not a marker  */
      {seq_num++;
       BUF_FORMXYZ( " nt(%i) = %i", seq_num, count );
       printit( buffer );}

    count = -1;

    while ( elem_trace3d_forms[index][++count] )
	find_print_t3d_arg( ste, elem_trace3d_forms[index][count] );

    if (index != 21)
        PRINT_COMMA;
    PRINT_EOL
    }

/* BEAM BUFFERING ROUTINES */

static beamnode *get_arg( num, args )
double num;
beam_param_node *args;
    {
    while ( --num > 0 )
	if ( args )
	    args = args->next;
	else
	    {
	    fprintf( stderr, "Flat Print: not enough arguments to sub_line\n");
	    num = 0.0;
	    }

    if ( args )
        return ( args->beam_root );
    else
	{
	fprintf( stderr, "Flat Print: not enough arguments to sub_line\n");
	return (beamnode *)NULL;
	}
    }

static void insert_args( map, args )
beamnode *map;
beam_param_node *args;
    {
    register beam_param_node *next;

    while ( map )
	{
	if ( map->operation == BEAM_ARGUMENT )
	    map->beam_operand = get_arg( eval_expr( map->iteration ), args );
	else
	    {
	    if ( map->operation == BEAM_CALL_FUNC )
		{
		next = map->beam_params;

		while ( next )
		    {
		    insert_args( next->beam_root, args );
		    next = next->next;
		    }
		}
	    else
		{
		insert_args( map->beam_operand, args );
		if ( map->elem_operand )
		    insert_args( ((symbol_node *)(map->elem_operand->symbol_data))->beam_root, args );
		}
	    }
	map = map->next;
	}
    }

static void remove_args( map )
beamnode *map;
    {
    register beam_param_node *next;

    while ( map )
	{
	if ( map->operation == BEAM_ARGUMENT )
	    map->beam_operand = (beamnode *)NULL;
	else
	    {
	    if ( map->operation == BEAM_CALL_FUNC )
		{
		next = map->beam_params;

		while ( next )
		    {
		    remove_args( next->beam_root );
		    next = next->next;
		    }
		}
	    else
		{
		remove_args( map->beam_operand );
		if ( map->elem_operand )
		    remove_args( ((symbol_node *)(map->elem_operand->symbol_data))->beam_root );
		}
	    }
	map = map->next;
	}
    }

static void do_beam_call( map, args, buffer, transpose )
beamnode *map;
beam_param_node *args;
char *buffer;
int transpose;
    {
    insert_args( map, args );

    if ( transpose )
	buffer_beam_backwards( map, buffer );
    else
	buffer_beam( map, buffer );

    remove_args( map );
    }

static void buffer_beam( beam, buffer )
beamnode *beam;
char *buffer;
    {
    register double repeat;
    symbol_node *sym_data;

    while ( beam )
	{
        switch (beam->operation)
            {
            case BEAM_NO_OP:
		if ( beam->beam_operand )
		    buffer_beam( beam->beam_operand, buffer );
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE 
                         || sym_data->label_type == SLOT )
			buffer_beam( sym_data->beam_root, buffer );
		    else
			print_elem_trace3d( beam->elem_operand );
		    }
	        break;

            case BEAM_REPEAT:
		for ( repeat=eval_expr( beam->iteration ); repeat>0; repeat-=1 )
		    if ( beam->beam_operand )
			buffer_beam( beam->beam_operand, buffer );
		    else
			{
			sym_data =
			    (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE 
                              || sym_data->label_type == SLOT )
			    buffer_beam( sym_data->beam_root, buffer );
			else
			    print_elem_trace3d( beam->elem_operand );
			}
	        break;

            case BEAM_TRANSPOSE:
		if ( beam->beam_operand )
		    buffer_beam_backwards( beam->beam_operand, buffer );
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE 
                          || sym_data->label_type == SLOT )
			buffer_beam_backwards( sym_data->beam_root, buffer );
		    else
			print_elem_trace3d( beam->elem_operand );
		    }

	        break;

	    case BEAM_CALL_FUNC:
		sym_data = (symbol_node *)(beam->elem_operand->symbol_data);
		do_beam_call( sym_data->beam_root, beam->beam_params, buffer,0);
		break;

	    case BEAM_ARGUMENT:
		if ( beam->beam_operand )
		    buffer_beam( beam->beam_operand, buffer );
		else
		    if ( beam->elem_operand )
			{
			sym_data =
			    (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE 
                              || sym_data->label_type == SLOT )
			    buffer_beam( sym_data->beam_root, buffer );
			else
			    print_elem_trace3d( beam->elem_operand );
			}
		    else
			{
			fprintf( stderr,
			    "buffer_beam: unresolved argument found\n" );
			exit( 1 );
			/*NOTREACHED*/
			}
			
		break;

            default:
	        fprintf( stderr,
		    "\nbuffer_beam: bad beam operation %d\n", beam->operation );
	        exit( 1 );
	        /*NOTREACHED*/
            }

	beam = beam->next;
	}
    }

static void buffer_beam_backwards( beam, buffer )
beamnode *beam;
char *buffer;
    {
    register double repeat;
    symbol_node *sym_data;

    if ( beam )
	{
	buffer_beam_backwards( beam->next, buffer );

        switch (beam->operation)
            {
            case BEAM_REPEAT:
		for ( repeat=eval_expr(beam->iteration); repeat>0; repeat-=1 )
		    if ( beam->beam_operand )
			buffer_beam_backwards( beam->beam_operand, buffer );
		    else
			{
			sym_data =
			    (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE 
                              || sym_data->label_type == SLOT )
			    buffer_beam_backwards( sym_data->beam_root,buffer );
			else
			    print_elem_trace3d( beam->elem_operand );
			}
	        break;

            case BEAM_TRANSPOSE:
		if ( beam->beam_operand )
		    buffer_beam( beam->beam_operand, buffer );
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE 
                          || sym_data->label_type == SLOT )
			buffer_beam( sym_data->beam_root, buffer );
		    else
			print_elem_trace3d( beam->elem_operand );
		    }
	        break;

            case BEAM_NO_OP:
		if ( beam->beam_operand )
		    buffer_beam_backwards( beam->beam_operand, buffer );
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE 
                          || sym_data->label_type == SLOT )
			buffer_beam_backwards( sym_data->beam_root, buffer );
		    else
			print_elem_trace3d( beam->elem_operand );
		    }
	        break;

	    case BEAM_CALL_FUNC:
		sym_data = (symbol_node *)(beam->elem_operand->symbol_data);
		do_beam_call( sym_data->beam_root, beam->beam_params, buffer,1);
		break;

	    case BEAM_ARGUMENT:
		if ( beam->beam_operand )
		    buffer_beam_backwards( beam->beam_operand, buffer );
		else
		    if ( beam->elem_operand )
			{
			sym_data =
			    (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE 
                              || sym_data->label_type == SLOT )
			    buffer_beam_backwards( sym_data->beam_root, buffer);
			else
			    print_elem_trace3d( beam->elem_operand );
			}
		    else
			{
			fprintf( stderr,
			    "buffer_beam: unresolved argument found\n" );
			exit( 1 );
			/*NOTREACHED*/
			}
			
		break;

            default:
	        fprintf( stderr, "\nbuffer_beam: bad beam operation\n" );
	        exit( 1 );
	        /*NOTREACHED*/
            }
	}
    }

/* MAIN ROUTINE */

void print_trace3d( root )
symbol root;
    {
    symbol_node *sym_data;
    char buffer1[MAXSTRING];
    char *buffer = buffer1;

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /*this makes sure it is only printed once*/
	    {
	    switch( sym_data->label_type )
	        {
	        case ATTR_DRIFT:
	        case ATTR_HMONITOR:
	        case ATTR_VMONITOR:
	        case ATTR_MONITOR:
	        case ATTR_WIGGLER:

	        case ATTR_RBEND:
	        case ATTR_SBEND:
	        case ATTR_QUADRUPOLE:
	        case ATTR_SEXTUPOLE:
	        case ATTR_OCTUPOLE:
	        case ATTR_SOLENOID:
	        case ATTR_EL_SEPARATE:
	        case ATTR_HKICK:
	        case ATTR_VKICK:
                case ATTR_KICKER:
	        
	        case ATTR_RFCAVITY:
		case ATTR_ECOLLIMATOR:
	        case ATTR_RCOLLIMATOR:

	        case ATTR_SROT:
	        case ATTR_YROT:

                case ATTR_THINLENS:
                case ATTR_TANK:
 /*  how do doublet, triplet, etc work????  */

	        case ATTR_MULTIPOLE:
		    print_elem_trace3d( root );
		    sym_data->declared = 0;
		    break;

	        case GEOMETRY:	
	        case STRENGTH:
		case PARAM_LINE:
                case ATTR_MARKER:
		    break;

	        case BEAMLINE:
                case SLOT:
		    buffer_beam( sym_data->beam_root, buffer );
		    break;

	        default:
		    fprintf( stderr, "something is amiss in print_lines\n" );

	        }
	    sym_data->declared = 0; /*this makes sure it is only printed once*/
	    }
	}
    }

/* END OF FILE */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "lattice_flat.h"
#include "lattice_gdecls.h"
#include "lattice_tree.h"
#include "lattice_defines.h"

int twin_seq_num = 0;
int twin_rownum = 1;

#define BUF_FORMXYZ(x,y,z)  { sprintf( buffer, x, y, z ); }
#define BUF_FORMXY(x,y)  { sprintf( buffer, x, y ); } 
#define PRINT_COMMA     { fputc(',', stdout); } 
#define PRINT_EOL	{ fputc('\t\n', stdout); }

int drift_tracewin[] = { ATTR_L, ATTR_APERTURE, ATTR_YSIZE, ATTR_NULL };

int thinlens_tracewin[] = { ATTR_XFOCAL, ATTR_YFOCAL, ATTR_ZFOCAL, ATTR_NULL };

int thinlensx_tracewin[] = { ATTR_K1, ATTR_ZERO, ATTR_APERTURE, ATTR_NULL };
int thinlensy_tracewin[] = { ATTR_ZERO, ATTR_K1, ATTR_APERTURE, ATTR_NULL };

int quadrupole_tracewin[] = { ATTR_L, ATTR_K1, ATTR_APERTURE, ATTR_TILT, ATTR_ZERO, ATTR_ZERO, ATTR_ZERO, ATTR_ZERO, ATTR_NULL };

int pmq_tracewin[] = { ATTR_K1, ATTR_L, ATTR_IN_RAD, ATTR_OUT_RAD, ATTR_NULL };

int solenoid_tracewin[] = { ATTR_L, ATTR_KS, ATTR_APERTURE, ATTR_NULL };

int doublet_tracewin[] = { ATTR_K1, ATTR_L, ATTR_DIST_BETW, ATTR_NULL };

int triplet_tracewin[]={ATTR_BFLD_OUT, ATTR_L_OUT, ATTR_DIST_BETW, ATTR_BFLD_IN, ATTR_L_IN, ATTR_NULL};

int sbend_tracewin[] = { ATTR_ANGLE, ATTR_L, ATTR_K1, ATTR_NULL };
int rbend_tracewin[] = { ATTR_ANGLE, ATTR_L, ATTR_K1, ATTR_NULL };

// These three are needed for bend element support
int entry_edge_tracewin[] = {ATTR_E1, ATTR_H1, ATTR_HGAP, ATTR_ZERO, ATTR_ZERO, ATTR_APERTURE, ATTR_DTILT, ATTR_NULL};
int bend_tracewin[] = {ATTR_ANGLE, ATTR_ZERO, ATTR_ZERO, ATTR_APERTURE, ATTR_DTILT, ATTR_NULL};
int exit_edge_tracewin[] = {ATTR_E2, ATTR_H2, ATTR_HGAP, ATTR_ZERO, ATTR_ZERO, ATTR_APERTURE, ATTR_DTILT, ATTR_NULL};

int edge_tracewin[] = { ATTR_ANGLE, ATTR_RADIUS, ATTR_GAP, ATTR_FF1, ATTR_FF2, ATTR_NULL };

int rfgap_tracewin[]={ATTR_EGAPVOLT, ATTR_PHASE, ATTR_EGFLAG, ATTR_DWFLAG, ATTR_HARMON, ATTR_NULL};

int rfqcell_tracewin[]={ATTR_VR2, ATTR_AV, ATTR_L, ATTR_PHASE, ATTR_CELL, ATTR_NULL};

int rfcavity_tracewin[] = { ATTR_L, ATTR_VOLT, ATTR_LAG, ATTR_APERTURE, ATTR_ONE, ATTR_NULL };

int tank_tracewin[] = { ATTR_ACCEL, ATTR_L, ATTR_PHASE, ATTR_IDEN_CAV, ATTR_NULL };

int special_tracewin[] = { ATTR_L, ATTR_NULL };

int rotation_tracewin[] = { ATTR_ANGLE, ATTR_NULL };

int dtl_cell_tracewin[] = { ATTR_LENGTH, ATTR_LQ1, ATTR_LQ2, ATTR_CELLCENTER, ATTR_B1P, ATTR_B2P, ATTR_E0TL, ATTR_RFPHASE, ATTR_APERTURE, ATTR_ABSPHASE, ATTR_BETAS, ATTR_TTIME, ATTR_KTSP, ATTR_K2TSP, ATTR_NULL };
int ncells_tracewin[] = { ATTR_MODE, ATTR_CELLNUMBER, ATTR_BETAG, ATTR_E0T, ATTR_RFPHASE, ATTR_APERTURE, ATTR_ABSPHASE, ATTR_KE0TI, ATTR_KE0TO, ATTR_DZI, ATTR_DZO, ATTR_BETAS, ATTR_TTIME, ATTR_KTSP, ATTR_K2TSPP, ATTR_TTIMEIN, ATTR_KTIP, ATTR_K2TIPP, ATTR_TTIMEOUT, ATTR_KTOP, ATTR_K2TOPP, ATTR_LENGTH, ATTR_NULL };
int rfq_cell_tracewin[] = { ATTR_MEANV, ATTR_VANER, ATTR_ACCPARAM, ATTR_MODULATION, ATTR_LENGTH, ATTR_RFPHASE, ATTR_CELLTYPE, ATTR_TCURV, ATTR_TFOCUS, ATTR_NULL};

int closed_orbit_corrector_tracewin[] = {ATTR_LENGTH, ATTR_ZERO, ATTR_ZERO, ATTR_APERTURE, ATTR_NULL};

int multipole_tracewin[] = { ATTR_NULL };
int octupole_tracewin[] = { ATTR_NULL };
int sextupole_tracewin[] = { ATTR_NULL };
int hmonitor_tracewin[] = { ATTR_NULL };
int vmonitor_tracewin[] = { ATTR_NULL };
int monitor_tracewin[] = { ATTR_NULL };
int instrument_tracewin[] = { ATTR_NULL };
int wiggler_tracewin[] = { ATTR_NULL };
int elseparate_tracewin[] = { ATTR_E, ATTR_L, ATTR_ZERO, ATTR_APERTURE, ATTR_NULL };
int srot_tracewin[] = { ATTR_NULL };
int yrot_tracewin[] = { ATTR_NULL };
int hkick_tracewin[] = { ATTR_NULL };
int vkick_tracewin[] = { ATTR_NULL };
int kicker_tracewin[] = { ATTR_NULL };
int marker_tracewin[] = { ATTR_NULL };
int ecollimator_tracewin[] = { ATTR_NULL };
int rcollimator_tracewin[] = { ATTR_NULL };
int tbend_tracewin[] = { ATTR_NULL }; 

static int *elem_tracewin_forms[] =
    {
    (int *) 0,
    drift_tracewin,
    hmonitor_tracewin,
    vmonitor_tracewin,
    monitor_tracewin,
    instrument_tracewin,
    wiggler_tracewin,
    rbend_tracewin,
    sbend_tracewin,
    quadrupole_tracewin,
    sextupole_tracewin,
    octupole_tracewin,
    multipole_tracewin,
    solenoid_tracewin,
    rfcavity_tracewin,
    elseparate_tracewin,
    srot_tracewin,
    yrot_tracewin,
    hkick_tracewin,
    vkick_tracewin,
    kicker_tracewin,
    marker_tracewin,
    ecollimator_tracewin,
    rcollimator_tracewin,
    tbend_tracewin,
    thinlens_tracewin,
    tank_tracewin,
    edge_tracewin,
    pmq_tracewin,
    rfqcell_tracewin,
    doublet_tracewin,
    triplet_tracewin,
    rfgap_tracewin,
    special_tracewin,
    rotation_tracewin,
    0,
    0,
    0,
    0,
    dtl_cell_tracewin,
    ncells_tracewin,
    rfq_cell_tracewin,
    0, // bend, not used
    closed_orbit_corrector_tracewin
    };

static int tracewin_elems[] =
    {
    17,
    3,	// drift 
    17,	// hmonitor 
    17,	// vmonitor 
    17,	// monitor 
    17,	// instrument 
    17,	// wiggler 
    8,	// rbend 
    8,	// sbend 
    8,	// quadrupole 
    17,	// sextupole 
    17,	// octupole 
    17,	// multipole 
    5,	// solenoid 
    12,	// rfcavity 
    17,	// elseparate 
    17,	// srot   
    17,	// yrot   
    17,	// hkick  
    17,	// vkick  
    17,	// kicker 
    17,	// marker 
    17,	// ecollimator 
    17,	// rcollimator 
    17,	// tbend  
    2,	// thin lens  
    13,	// tank  
    9,  // edge  
    4,  // pmq  
    11, // rfqcell  
    6,  // doublet 
    7,  // triplet 
    10, // rfgap  
    14, // special  
    15, // rotation  
    0,
    0,
    0,
    0,
    14, // dtl_cell
    21, // ncells
    9,  // rfq_cell
    0,  // bend, not used
    3
    };

// These are the names that are printed for objects, e.g. DRIFT
// Indexes should match their ATTR_... define values
// e.g. ATTR_DRIFT is 1, therefore "DRIFT" should be indexed 1
// Updated with data from Garry, date: 2010-11-10
char *tracewin_element_names[] =
    {
    "NULL?", /* "" => no attrs */
    "DRIFT",
    "NA", //
    "NA", //
    "NA", //
    "NA", // Garry, doublecheck
    "NA",
    "BEND + EDGE",
    "BEND + EDGE", 
    "QUAD", 
    "MULTIPOLE", // Order = 3 - see how to do this (print with name, not parameters)...
    "MULTIPOLE", // Order = 4
    "MULTIPOLE",  
    "SOLENOID", 
    "GAP",
    "ELECTROSTA_ACC", // Doublecheck /*The problem of implementation is that TraceWin only appears to do transverse (vertical?) acceleration in this context. TraceWin is full of elements that due longitudinal acceleration of course, so we might have to use the above element for 'vertical' motion and a thin_gap (?) for longitudinal acceleration. */
    "BEAM_ROT", 
    "BEAM_ROT", 
    "THIN_STEERING", 
    "THIN_STEERING", 
    "THIN_STEERING", 
    "DIAG_POSITION", 
    "DIAG_SIZE",  // ?
    "DIAG_SIZE",  // ?
    "THIN_STEERING",  // ?
    "THIN_LENS",
    "DTL_CELL",  // or NCELL maybe ?
    "EDGE", 
    "QUAD_ELE",   // pmq - not sure about this
    "RFQ_CELL", 
    "NA",  // doublet - ?
    "NA", // triplet - ? 
    "GAP", 
    "TBD",  // special - ?
    "BEAM_ROT", 
    "",
    "",
    "",
    "",
    "DTL_CEL", //
    "NCELLS",
    "RFQ_CELL",
    "", // BEND, not used
    "THIN_STEERING" // closed orbit corrector
    };

  double pi;

// function forward decls 
static char* buffer_beam();
static char* buffer_beam_backwards();

static int print_quad_tracewin();
static void print_bend_tracewin();


// OUTPUT ROUTINE 

static void printit( buf )
char buf[];
    {
    // this statement does all of the printing for the output file 
    // not including error messages 
    printf( "%s", buf );
    }

// ELEMENT BUFFERING ROUTINES 

int twin_bend_dir;
int twin_one_time_flag = 0;
double twin_rho, twin_n;

// This function prints out the object arguments
// For tracewin it should only print the value
static void find_print_twin_arg( ste, attrnum )
symbol ste;
int attrnum;
    {
    symbol_node *sym_data = (symbol_node *)(ste->symbol_data);
    attrnode *exprlist = sym_data->expr_list;
    char buffer1[MAXSTRING];
    char *buffer = buffer1;

    register int count;
    register int index;

    double save_length, save_k1, save_angle, save_sol_l;
    attrnode *len_attr;
    attrnode *k1_attr;
    attrnode *angle_attr;
    attrnode *sol_l_attr;

    pi = 2.0 * asin(1.0);
    index = sym_data->label_type;
    count = tracewin_elems[index];

   while ( exprlist )
    {
     if ( exprlist->attr == attrnum )
      {
	if(attrnum == ATTR_ANGLE || attrnum == ATTR_TILT || attrnum == ATTR_PHASE || attrnum == ATTR_RFPHASE)
	  sprintf( buffer, "%-11.9g ", eval_expr(exprlist->expr) * 180.0 / pi);
	else if(attrnum == ATTR_LENGTH || attrnum == ATTR_L || attrnum == ATTR_APERTURE || attrnum == ATTR_YSIZE || attrnum == ATTR_DZI || 
		attrnum == ATTR_DZO || attrnum == ATTR_LQ1 || attrnum == ATTR_LQ2 || attrnum == ATTR_CELLCENTER)
	  sprintf( buffer, "%-11.9g ", eval_expr(exprlist->expr) * 1000.0);
	else if(attrnum == ATTR_ZERO)
	  sprintf( buffer, "%d ", 0);
	else if(attrnum == ATTR_DTILT)
	  sprintf( buffer, "%d ", (int)eval_expr(exprlist->expr));
	else
	  sprintf( buffer, "%-11.9g ", eval_expr(exprlist->expr));
	
	printit( buffer );

        return;
      }
     else
       exprlist = exprlist->next;
    }
   return;
   }

// A modified print function for bend elements
static void print_command_tracewin ( ste )
symbol ste;
    {
    
    symbol_node *sym_data = (symbol_node *)ste->symbol_data;

    char buffer1[MAXSTRING];
    char *buffer = buffer1;
    register int index;
    register int count;

    int twin_seq_num = 0;
    int twin_rownum = 1;

    index = sym_data->label_type;
    count = tracewin_elems[index];

    sprintf(buffer, "%s: ", ste->name);
    printit(buffer);

    attrnode *exprlist = sym_data->expr_list;

    while(exprlist){
	if(exprlist->attr == ATTR_COMMAND){
		sprintf(buffer, "%s", exprlist->expr->command);
		printit(buffer);		
		break;
        }
	exprlist = exprlist->next;
    }
    
    PRINT_EOL;
}

static void print_elem_tracewin( ste )
symbol ste;
    {
    
    symbol_node *sym_data = (symbol_node *)ste->symbol_data;

    char buffer1[MAXSTRING];
    char *buffer = buffer1;
    register int index;
    register int count;

    int twin_seq_num = 0;
    int twin_rownum = 1;

    index = sym_data->label_type;
    count = tracewin_elems[index];

    // This is handled here since the type was not picked by the main function
    if(index == ATTR_MARKER){
      sprintf(buffer, ";marker %s", ste->name);
      printit(buffer);
      PRINT_EOL;
    }
    else if(index == ATTR_QUADRUPOLE && print_quad_tracewin(ste))
	return;
    else if(index == ATTR_COMMAND){
	print_command_tracewin(ste);	
	return;
    }
    else if(index == ATTR_BEND){
	print_bend_tracewin(ste);
	return;
    }
    else{    
      // Print the name of the element as well !
      sprintf(buffer, "%s: ", ste->name);
      printit(buffer);

      if(index < 44)sprintf(buffer, "%s ", tracewin_element_names[index]);
      else{ 
	sprintf(buffer, "JOHN_LONG ");
	PRINT_EOL;
	return;
      }


      // Here additional things will be printed that are not actual parameters
      // Like the 'MULTIPOLE' Order for sextupoles or octopoles

      printit(buffer);

      count = -1;

      while ( elem_tracewin_forms[index][++count] )
        find_print_twin_arg( ste, elem_tracewin_forms[index][count] );

      PRINT_EOL;
    }
  }

// A modified print function for bend elements
static void print_bend_tracewin ( ste )
symbol ste;
    {
    
    symbol_node *sym_data = (symbol_node *)ste->symbol_data;

    char buffer1[MAXSTRING];
    char *buffer = buffer1;
    register int index;
    register int count;

    int twin_seq_num = 0;
    int twin_rownum = 1;

    index = sym_data->label_type;
    count = tracewin_elems[index];

    sprintf(buffer, "%s_In: EDGE ", ste->name);
    printit(buffer);

    count = -1;

    while ( entry_edge_tracewin[++count] )
    	find_print_twin_arg( ste, entry_edge_tracewin[count] );

    PRINT_EOL;

    sprintf(buffer, "%s: BEND ", ste->name);
    printit(buffer);

    count = -1;

    while ( bend_tracewin[++count] )
    	find_print_twin_arg( ste, bend_tracewin[count] );

    PRINT_EOL;

    sprintf(buffer, "%s_Out: EDGE ", ste->name);
    printit(buffer);

    count = -1;

    while ( exit_edge_tracewin[++count] )
    	find_print_twin_arg( ste, exit_edge_tracewin[count] );

    PRINT_EOL;
    }

static int print_quad_tracewin( ste )
symbol ste;
    {
    
    symbol_node *sym_data = (symbol_node *)ste->symbol_data;

    char buffer1[MAXSTRING];
    char *buffer = buffer1;
    register int index;
    register int count;

    int twin_seq_num = 0;
    int twin_rownum = 1;

    index = sym_data->label_type;
    count = tracewin_elems[index];

    // find the contents of the comment field
    attrnode *exprlist = sym_data->expr_list;

    while(exprlist){
	if(exprlist->attr == ATTR_MAGNET_PIECE_TYPE){
		// 'thinlensx'
		if((int)eval_expr(exprlist->expr) == 1){
			sprintf(buffer, "%s: THIN_LENS ", ste->name);
			printit(buffer);

			count = -1;

			while ( thinlensx_tracewin[++count] )
			    	find_print_twin_arg( ste, thinlensx_tracewin[count] );

			PRINT_EOL;
			return 1;
		}
		// 'thinlensy'
		else if((int)eval_expr(exprlist->expr) == 2){
			sprintf(buffer, "%s: THIN_LENS ", ste->name);
			printit(buffer);

			count = -1;

			while ( thinlensy_tracewin[++count] )
			    	find_print_twin_arg( ste, thinlensy_tracewin[count] );

			PRINT_EOL;
			return 1;
		}
	  
 	  break;
        }
	exprlist = exprlist->next;
    }
    return 0;
}

static void remove_args( map )
beamnode *map;
    {
    register beam_param_node *next;

    while ( map )
	{
	if ( map->operation == BEAM_ARGUMENT )
	    map->beam_operand = (beamnode *)NULL;
	else
	    {
	    if ( map->operation == BEAM_CALL_FUNC )
		{
		next = map->beam_params;

		while ( next )
		    {
		    remove_args( next->beam_root );
		    next = next->next;
		    }
		}
	    else
		{
		remove_args( map->beam_operand );
		if ( map->elem_operand )
		    remove_args( ((symbol_node *)(map->elem_operand->symbol_data))->beam_root );
		}
	    }
	map = map->next;
	}
    }

static beamnode *get_arg( num, args )
double num;
beam_param_node *args;
    {
    while ( --num > 0 )
	if ( args )
	    args = args->next;
	else
	    {
	    fprintf( stderr, "Flat Print: not enough arguments to sub_line\n");
	    num = 0.0;
	    }

    if ( args )
        return ( args->beam_root );
    else
	{
	fprintf( stderr, "Flat Print: not enough arguments to sub_line\n");
	return (beamnode *)NULL;
	}
    }

static void insert_args( map, args )
beamnode *map;
beam_param_node *args;
    {
    register beam_param_node *next;

    while ( map )
	{
	if ( map->operation == BEAM_ARGUMENT )
	    map->beam_operand = get_arg( eval_expr( map->iteration ), args );
	else
	    {
	    if ( map->operation == BEAM_CALL_FUNC )
		{
		next = map->beam_params;

		while ( next )
		    {
		    insert_args( next->beam_root, args );
		    next = next->next;
		    }
		}
	    else
		{
		insert_args( map->beam_operand, args );
		if ( map->elem_operand )
		    insert_args( ((symbol_node *)(map->elem_operand->symbol_data))->beam_root, args );
		}
	    }
	map = map->next;
	}
    }

static void do_beam_call( map, args, buffer, transpose )
beamnode *map;
beam_param_node *args;
char *buffer;
int transpose;
    {
    insert_args( map, args );

    if ( transpose )
	buffer_beam_backwards( map, buffer );
    else
	buffer_beam( map, buffer );

    remove_args( map );
    }

static void append(char* what, char* where){
	sprintf(where, "%s%s", where, what);

}
static void append_s(char* what, char* where){
	sprintf(where, "%s %s", where, what);

}
static void append_nl(char* what, char* where){
	sprintf(where, "%s\n%s", where, what);

}

// This is the max size for the ;beamlines and ;slots string in the end
// If segmentation faults occur, increase ;)
// Dynamic reallocation? Nahh....
static int buffer_size = 1024*100;

static char* buffer_beam( root, beam, buffer)
symbol root;
beamnode *beam;
char *buffer;
    {
    register double repeat;
    symbol_node *sym_data;
 
    char return_buffer[buffer_size];
    memset(return_buffer, 0x00, buffer_size);
    char *return_string = return_buffer;
   
    char recursion_buffer[buffer_size];
    memset(recursion_buffer, 0x00, buffer_size);
    char *recursion_string = recursion_buffer;

    //prepend the beamline / slot identifier
    append(((symbol_node *)(root->symbol_data))->label_type == BEAMLINE ? ";beamline" : ";slot", return_string);
    // prepend the name
    append_s(root->name, return_string);

    while ( beam )
	{
	switch (beam->operation)
            {
            case BEAM_NO_OP:
		if ( beam->beam_operand ){
		    append_s(beam->elem_operand->name, return_string);
		    append_nl(buffer_beam( beam->elem_operand, beam->beam_operand, buffer), recursion_string);
		}
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE || sym_data->label_type == SLOT ){
			append_s(beam->elem_operand->name, return_string);
			append_nl(buffer_beam( beam->elem_operand, sym_data->beam_root, buffer ), recursion_string);
		    }
		    else{
			print_elem_tracewin( beam->elem_operand );
			append_s(beam->elem_operand->name, return_string);
		    }
		    }
	        break;

            case BEAM_REPEAT:
		for ( repeat=eval_expr( beam->iteration ); repeat>0; repeat-=1 )
		    if ( beam->beam_operand ){
			append_s(beam->elem_operand->name, return_string);
       		        append_nl(buffer_beam( beam->elem_operand, beam->beam_operand, buffer), recursion_string);
		    }
		    else
			{
			sym_data =
			    (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE || sym_data->label_type == SLOT ){
			    append_s(beam->elem_operand->name, return_string);
			    append_nl(buffer_beam( beam->elem_operand, sym_data->beam_root, buffer ), recursion_string);
			}
			else{
			    print_elem_tracewin( beam->elem_operand );
 			    append_s(beam->elem_operand->name, return_string);
			}
			}
	        break;

            case BEAM_TRANSPOSE:
		if ( beam->beam_operand ){
		    append_s(beam->elem_operand->name, return_string);
 		    append_nl(buffer_beam_backwards( beam->elem_operand, sym_data->beam_root, buffer ), recursion_string);
		}
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE || sym_data->label_type == SLOT ){
			append_s(beam->elem_operand->name, return_string);
			append_nl(buffer_beam_backwards( beam->elem_operand, sym_data->beam_root, buffer ), recursion_string);
		    }
		    else{
			print_elem_tracewin( beam->elem_operand );
			append_s(beam->elem_operand->name, return_string);
		    }
		    }

	        break;

	    case BEAM_CALL_FUNC:
		sym_data = (symbol_node *)(beam->elem_operand->symbol_data);
		do_beam_call( sym_data->beam_root, beam->beam_params, buffer,0);
		break;

	    case BEAM_ARGUMENT:
		if ( beam->beam_operand ){
		    append_s(beam->elem_operand->name, return_string);
       		    append_nl(buffer_beam( beam->elem_operand, beam->beam_operand, buffer), recursion_string);
		}
		else
		    if ( beam->elem_operand )
			{
			sym_data =
			    (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE || sym_data->label_type == SLOT ){
			    append_s(beam->elem_operand->name, return_string);
			    append_nl(buffer_beam( beam->elem_operand, sym_data->beam_root, buffer ), recursion_string);
			}
			else{
			    print_elem_tracewin( beam->elem_operand );
    			    append_s(beam->elem_operand->name, return_string);
			}
			}
		    else
			{
			fprintf( stderr,
			    "buffer_beam: unresolved argument found\n" );
			exit( 1 );
			/*NOTREACHED*/
			}
			
		break;

            default:
	        fprintf( stderr,
		    "\nbuffer_beam: bad beam operation %d\n", beam->operation );
	        exit( 1 );
	        /*NOTREACHED*/
            }

	beam = beam->next;
	}
    	append(recursion_string, return_string);
	return return_string;
    }



static char* buffer_beam_backwards( root, beam, buffer )
symbol root;
beamnode *beam;
char *buffer;
    {
    register double repeat;
    symbol_node *sym_data;

    char return_buffer[buffer_size];
    memset(return_buffer, 0x00, buffer_size);
    char *return_string = return_buffer;
   
    char recursion_buffer[buffer_size];
    memset(recursion_buffer, 0x00, buffer_size);
    char *recursion_string = recursion_buffer;

    //prepend the beamline / slot identifier
    append(((symbol_node *)(root->symbol_data))->label_type == BEAMLINE ? ";beamline" : ";slot", return_string);
    // prepend the name
    append_s(root->name, return_string);

    if ( beam )
	{
	buffer_beam_backwards( beam->next, buffer );

        switch (beam->operation)
            {
            case BEAM_REPEAT:
		for ( repeat=eval_expr(beam->iteration); repeat>0; repeat-=1 )
		    if ( beam->beam_operand ){
			append_s(beam->elem_operand->name, return_string);
		        append_nl(buffer_beam_backwards( beam->elem_operand, beam->beam_operand, buffer), recursion_string);
			
		    }
		    else
			{
			sym_data =
			    (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE || sym_data->label_type == SLOT ){
			append_s(beam->elem_operand->name, return_string);
			append_nl(buffer_beam_backwards( beam->elem_operand, sym_data->beam_root, buffer ), recursion_string);
	  	        }
		        else{
			    print_elem_tracewin( beam->elem_operand );
			    append_s(beam->elem_operand->name, return_string);
		        }
			}
	        break;

            case BEAM_TRANSPOSE:
		if ( beam->beam_operand ){
		    append_s(beam->elem_operand->name, return_string);
 		    append_nl(buffer_beam( beam->elem_operand, sym_data->beam_root, buffer ), recursion_string);
		}
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE || sym_data->label_type == SLOT ){
			append_s(beam->elem_operand->name, return_string);
			append_nl(buffer_beam( beam->elem_operand, sym_data->beam_root, buffer ), recursion_string);
		    }
		    else{
			print_elem_tracewin( beam->elem_operand );
			append_s(beam->elem_operand->name, return_string);
		    }
		    }
	        break;

            case BEAM_NO_OP:
		if ( beam->beam_operand ){
		    append_s(beam->elem_operand->name, return_string);
		    append_nl(buffer_beam_backwards( beam->elem_operand, beam->beam_operand, buffer), recursion_string);
		}
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE || sym_data->label_type == SLOT ){
			append_s(beam->elem_operand->name, return_string);
			append_nl(buffer_beam_backwards( beam->elem_operand, sym_data->beam_root, buffer ), recursion_string);
		    }
		    else{
			print_elem_tracewin( beam->elem_operand );
			append_s(beam->elem_operand->name, return_string);
		    }
		    }
	        break;

	    case BEAM_CALL_FUNC:
		sym_data = (symbol_node *)(beam->elem_operand->symbol_data);
		do_beam_call( sym_data->beam_root, beam->beam_params, buffer,1);
		break;

	    case BEAM_ARGUMENT:
		if ( beam->beam_operand ){
		    append_s(beam->elem_operand->name, return_string);
		    append_nl(buffer_beam_backwards( beam->elem_operand, beam->beam_operand, buffer), recursion_string);
		}
		else
		    if ( beam->elem_operand )
			{
			sym_data =
			    (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE || sym_data->label_type == SLOT ){
			    append_s(beam->elem_operand->name, return_string);
			    append_nl(buffer_beam_backwards( beam->elem_operand, sym_data->beam_root, buffer ), recursion_string);
			}
			else{
			    print_elem_tracewin( beam->elem_operand );
			    append_s(beam->elem_operand->name, return_string);
			}
			}
		    else
			{
			fprintf( stderr,
			    "buffer_beam: unresolved argument found\n" );
			exit( 1 );
			/*NOTREACHED*/
			}
			
		break;

            default:
	        fprintf( stderr, "\nbuffer_beam: bad beam operation\n" );
	        exit( 1 );
	        /*NOTREACHED*/
            }
	}
    return return_string;
    }

// MAIN ROUTINE 

void print_tracewin( root )
symbol root;
    {
    symbol_node *sym_data;
    char buffer1[MAXSTRING];
    char *buffer = buffer1;

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared) //this makes sure it is only printed once
	    {
            switch( sym_data->label_type )
	        {
		case ATTR_DRIFT:
	        case ATTR_HMONITOR:
	        case ATTR_VMONITOR:
	        case ATTR_MONITOR:
	        case ATTR_WIGGLER:

	        case ATTR_RBEND:
	        case ATTR_SBEND:

	        case ATTR_SEXTUPOLE:
	        case ATTR_OCTUPOLE:
	        case ATTR_SOLENOID:
	        case ATTR_EL_SEPARATE:
	        case ATTR_HKICK:
	        case ATTR_VKICK:
                case ATTR_KICKER:
	        
	        case ATTR_RFCAVITY:
		case ATTR_ECOLLIMATOR:
	        case ATTR_RCOLLIMATOR:

	        case ATTR_SROT:
	        case ATTR_YROT:

                case ATTR_THINLENS:
                case ATTR_TANK:

	        case ATTR_MULTIPOLE:
		case ATTR_NCELLS:
		case ATTR_DTL_CELL:
		case ATTR_RFQ_CELL:
		case ATTR_COC:
		case ATTR_COMMAND:
		case ATTR_QUADRUPOLE:
                case ATTR_BEND:
		    print_elem_tracewin( root );
		    sym_data->declared = 0;
		    break;

	        case GEOMETRY:	
	        case STRENGTH:
		case PARAM_LINE:
		case ATTR_MARKER:
		    break;

                case BEAMLINE:
                case SLOT:
		    printf("%s\n", buffer_beam( root, sym_data->beam_root, buffer));
		    break;

	        default:
		    fprintf( stderr, "something is amiss in print_lines\twin_n" );

	        }
	    sym_data->declared = 0; //this makes sure it is only printed once
	    }
	}
    }

// END OF FILE 

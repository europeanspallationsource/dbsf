
/* $Header: /rap/lambda/dbsf_sys5/lattice_tree/RCS/lattice_hilly.c,v 1.2 1996/05/28 20:13:18 trahern Exp $ */

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lattice_gdecls.h"
#include "lattice_hilly.h"
#include "lattice_tree.h"
#include "lattice_defines.h"

void print_lines();
void no_under_all_upper();

static void chop_buf();
static void buff_reg_elem();
static void buff_multipole();
static void print_params();
static void find_beam_elems();
static void print_elems();

#define INDENT		10

/* this array tells us whether or not it is legal to add aperture info to */
/* the TEAPOT element */
/* make sure these stay aligned with the ATTR_element values */

/* well it turns out that drift is the only difference for now, but just you */
/* wait and see if this doesn't make it easier to change the logic in the */
/* future */

int tpot_aper_valid[] = {
    0, /* void */
    0, /* drift */
    1, /* hmonitor */
    1, /* vmonitor */
    1, /* monitor */
    1, /* instrument */
    1, /* wiggler */
    1, /* rbend */
    1, /* sbend */
    1, /* quadrupole */
    1, /* sextupole */
    1, /* octupole */
    1, /* multipole */
    1, /* solenoid */
    1, /* rfcavity */
    1, /* elseparator */
    1, /* srot */
    1, /* yrot */
    1, /* hkick */
    1, /* vkick */
    1, /* kicker */
    1, /* marker */
    1, /* ecollimator */
    1, /* rcollimator */
    1, /* tbend */
    1, /* thinlens */
    1, /* tank */
    1, /* edge */
    1, /* pmq */
    1, /* rfqcell */
    1, /* doublet */
    1, /* triplet */
    1, /* rfgap */
    1, /* special */
    1, /* rotation */
    1, /* beamline */
    1, /* slot */
    1, /* superslot */
    1  /* tcelement */
};

int flat_expr;      /* don't flatten expressions by default */
int standard_403;   /* MAD 4.03 standard file */
int standard_81;   /* MAD 8.1 standard file */
static int print_aper;	/* flag whether or not to print aperture info */
static int is_tpot;	/* tpot file flag */

/* this loads an expression into <buffer> and resolves the declaration of */
/* scalar variables contained in the expression before returning */
static void expand_expr( exprnode *expr, char *buffer, int parens )
{
  if ( flat_expr ) {
    if ( expr ) {
      if ( expr->operation == EXPR_SHAPE )
	BUF_FORM("%c",(char)eval_expr( expr ))
	else
	  BUF_FORM(FLOAT_FORMAT,eval_expr( expr ))
	    }
  } else buffer_expr( expr, buffer, parens, 1 );
}

static char *buffer_beam( beam, buffer )
beamnode *beam;
char *buffer;
    {
    BUF_STRING("( ");

    while ( beam )
	{
        switch (beam->operation)
            {
            case BEAM_REPEAT:
		BUF_FORM("%.0f",eval_expr( beam->iteration ))
		BUF_CHAR('*');
	        break;

            case BEAM_TRANSPOSE:
		BUF_CHAR('-');
	        break;

            case BEAM_NO_OP:
	        break;

            default:
	        fprintf( stderr, "\nbuffer_beam: bad beam operation\n" );
	        exit( 1 );
	        /*NOTREACHED*/
            }

        if ( beam->beam_operand )
	    {
	    buffer = buffer_beam( beam->beam_operand, buffer );
	    }
        else
	    {
	    print_lines( beam->elem_operand );
	    BUF_FORM("%s",beam->elem_operand->name);
	    }

	if ( (beam = beam->next) )
	    BUF_STRING(", ");
	}

    BUF_STRING(" )\0");
    return ( buffer );
    }

/* this handles all of the output destined for the standard format file */
void print_buffer( buf )
char buf[];
    {
    if ( strlen( buf ) > 80 )
	{
	chop_buf( buf );
	}
    else
	{
	if ( standard_403 || standard_81 )  /* if MAD 4.03 or MAD 8.1 file */
	    no_under_all_upper( buf ); /* take out underscores and capitalize */

	/* this statement does all of the printing for the S.F. file */
        printf( "%s\n", buf );
        }
    }

void no_under_all_upper( buf )
char *buf;
    {
    register int source = 0;
    register int dest = 0;

    for ( ; buf[source]; source++ )
	if ( islower( buf[source] ) )
	    buf[dest++] = toupper( buf[source] );
	else
	    if ( buf[source] != '_' )
	        buf[dest++] = buf[source];

    buf[dest] = '\0';
    }

/* chops some chars off of a buffer, tacks on a &, prints them and the rest */
static void chop_buf( buf )
char *buf;
    {
    char work_buf[MAXSTRING];
    register char *tmp = buf + 72;
    int newlen;

    while ( *tmp != ',' && tmp > buf )
	--tmp;

    if ( tmp <= buf )
	{ /* oops, no ',' in the string - settle for any - or + */
	tmp = buf + 72;
	while ( ( *tmp != '-' && *tmp != '+') && tmp > buf )
	    --tmp;
	
	if ( tmp <= buf )
	  { /* oops, no ', or - or +' in the string - settle for any whitespace */
	  tmp = buf + 72;
	  while ( *tmp != ' ' && tmp > buf )
	    --tmp;
	
	  if ( tmp <= buf )
	    {
	    fprintf( stderr, "can't split line \"%s\" in chop_buf()\n", buf );
	    exit( 1 );
	    }
	  }
	}

    newlen = tmp - buf + 1;
    sprintf( work_buf, "%.*s &", newlen, buf );
    print_buffer( work_buf );

    tmp += 1;
    while ( *tmp++ == ' ' )
	;

 
    /* add indent to additional lines */
    tmp--;

    for( newlen = INDENT; newlen >= 0; newlen-- )
	*--tmp = ' ';

    /* indent stuff ends here */

    print_buffer( tmp );
    }

void print_lines( root )
symbol root;
    {
    symbol_node *sym_data;
    char buffer1[MAXSTRING];
    char *buffer = buffer1;
    char out_buff[4096];

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /*this makes sure it is only printed once*/
	    {
	    switch( sym_data->label_type )
	        {
	        case ATTR_DRIFT:
	        case ATTR_HMONITOR:
	        case ATTR_VMONITOR:
	        case ATTR_MONITOR:
	        case ATTR_INSTRUMENT:
	        case ATTR_WIGGLER:

	        case ATTR_RBEND:
	        case ATTR_SBEND:
	        case ATTR_QUADRUPOLE:
	        case ATTR_SEXTUPOLE:
	        case ATTR_OCTUPOLE:
	        case ATTR_SOLENOID:
	        case ATTR_EL_SEPARATE:
	        case ATTR_HKICK:
	        case ATTR_VKICK:
	        case ATTR_KICKER:
	        
	        case ATTR_RFCAVITY:
		case ATTR_ECOLLIMATOR:
	        case ATTR_RCOLLIMATOR:

	        case ATTR_MARKER:

	        case ATTR_SROT:
	        case ATTR_YROT:
		    buff_reg_elem( root, out_buff );
		    break;

	        case ATTR_THINLENS:
	        case ATTR_TANK:
	        case ATTR_EDGE:
	        case ATTR_PMQ:
	        case ATTR_RFQCELL:
	        case ATTR_DOUBLET:
	        case ATTR_TRIPLET:
	        case ATTR_RFGAP:
	        case ATTR_SPECIAL:
	        case ATTR_ROTATION:
                    print_buffer( "This database contains a trace3d-specific element which");
                    print_buffer( "is not valid in MAD.  This run will now terminate.");
                    print_buffer( "You may look at this lattice with the flat or trace3d option.");
		    fprintf( stderr, "This database contains a trace3d-specific element which \n" );
                    fprintf( stderr, "is not valid in MAD.  This run will now terminate. \n");
                    fprintf( stderr, "You may look at this lattice with the flat or trace3d option.\n");
		    exit(1);
                    break;

	        case ATTR_MULTIPOLE:
		    buff_multipole( root, out_buff );
		    break;

	        case GEOMETRY:
	        case STRENGTH:
		    expand_expr( sym_data->expr_list->expr, buffer, 0 );

		    if ( !standard_403 )
			OUTBUFF2("%s := %s",root->name,buffer)
		    else
			OUTBUFF2("parameter, %s = %s",root->name,buffer)
		    break;

	        case BEAMLINE:
		case SLOT:
		    buffer_beam( sym_data->beam_root, buffer );
		    OUTBUFF4( "%s:%*cline = %s", root->name,
			(int)( INDENT - strlen( root->name ) ), ' ', buffer );
		    break;

	        default:
		    fprintf( stderr, "something is amiss in print_lines\n" );

	        }
	    print_buffer( out_buff );
	    sym_data->declared = 0; /*this makes sure it is only printed once*/
	    }
	}
    }

static void param_buffer( expr, buffer )
exprnode *expr;
char *buffer;
    {
    expand_expr( expr, buffer, 0 );
    }

static void buff_reg_elem( ste, buffer )
symbol ste;
char *buffer;
    {
    char tmp_buf[MAXSTRING];
    int multipolify = 0;

    symbol_node *sym_data = (symbol_node *)(ste->symbol_data);
    attrnode *next = sym_data->expr_list;

    BUF_FORM( "%s:", ste->name );
    BUF_FORM2( "%*c", (int)( INDENT - strlen( ste->name ) ), ' ');

    if ( sym_data->label_type == ATTR_SEXTUPOLE || sym_data->label_type ==
      ATTR_OCTUPOLE )
	if ( eval_expr( (find_attr(ATTR_L, sym_data->expr_list))->expr )==0.0 )
	    multipolify = 1;

    BUF_FORM( "%s",(multipolify?"multipole":elem_names[sym_data->label_type]));
    
    while ( next )
    {
	if ( next->expr )
	    if ( next->expr->operation == EXPR_TILT && !multipolify )
		BUF_FORM( ", %s", "tilt" ); 

	param_buffer( next->expr, tmp_buf );

	if ( multipolify )
	{
	    switch ( next->attr ) {

	    case ATTR_L:
		break;

	    case ATTR_K2:
		BUF_FORM( ", %s = ", "k2l" );
		BUF_FORM( "%s", tmp_buf );
		break;

	    case ATTR_K3:
		BUF_FORM( ", %s = ", "k3l" );
		BUF_FORM( "%s", tmp_buf );
		break;

	    case ATTR_TILT:
		BUF_FORM( ", %s",
		  ( sym_data->label_type == ATTR_SEXTUPOLE ? "t2" : "t3" ) );

		if ( tmp_buf[0] != '\\' )
		    BUF_FORM( " = %s", tmp_buf );

		break;

	    default:
		fprintf( stderr, "\n***ERROR: illegal element attribute\n" );
		exit( 1 );
	    }
	} else {
	    if ( (standard_403 && next->attr == ATTR_HARMON && !is_tpot) ||
		 (standard_81 && next->attr == ATTR_FREQ) )
	        /* don't print */
		/* both harmon AND freq get printed if they are both present */
		/* and no standards are in effect */
		;
	    else {
		/*
		**
		**  don't print anything on these conditions:
		**    - element is vkick and attribute is kickh
		**    - element is hkick and attribute is kickv
		**    - attribute is tilt
		**    - attribute is aperture info invalid on TEAPOT type
		**    - attribute is aperture or magnet size and printing of
		**      that info is turned off
		**  print kick attr if:
		**    - attr is kick and elem is vkick or hkick
		**    - attr is kickv and elem is vkick
		**    - attr is kickh and elem is hkick
		**  otherwise print what you get
		**
		*/

		if ( next->expr->operation != EXPR_TILT
		  && next->attr < ATTR_MS_SHAPE
		  && ( !(next->attr & AP_MS_ATTR_BIT)
		  || ( print_aper && ( !is_tpot
		  || ( is_tpot && tpot_aper_valid[sym_data->label_type] )))) )
		{
		    if ( ( sym_data->label_type == ATTR_VKICK &&
			    next->attr == ATTR_KICKV ) ||
			 ( sym_data->label_type == ATTR_HKICK &&
			    next->attr == ATTR_KICKH ) )
		    {
			BUF_FORM( ", %s = ", elem_attr_names[ATTR_KICK] )
			BUF_FORM( "%s", tmp_buf );
		    } else
			if ( ( sym_data->label_type == ATTR_VKICK &&
				    next->attr == ATTR_KICKH ) ||
			     ( sym_data->label_type == ATTR_HKICK &&
				    next->attr == ATTR_KICKV ) )
			{
			    /* NOTHING */
			} else {
			  if(is_tpot && next->attr == ATTR_HARMON)
			    {
			      BUF_FORM( ", %s = ", elem_attr_names[ATTR_FREQ] );
			    } else
			      {
				BUF_FORM( ", %s = ", elem_attr_names[next->attr] );
			      }
			  BUF_FORM( "%s", tmp_buf );
			}
		}
	    }
	}

	next = next->next;
    }
	
    if ( sym_data->sub_type )
	if ( sym_data->sub_type != '\0' )
	    BUF_FORM( ", type = %s", sym_data->sub_type );
    }

static void buff_multipole( symbol ste, char *buffer )
{
  char tmp_buf[MAXSTRING];

  symbol_node *sym_data = (symbol_node *)(ste->symbol_data);
  attrnode *next = sym_data->expr_list;

  BUF_FORM( "%s:", ste->name );
  BUF_FORM2( "%*c",(int)( INDENT - strlen( ste->name ) ), ' ');
  BUF_FORM( "%s", elem_names[sym_data->label_type] );
    
  while ( next ) {
    if ( next->expr ) {
      if ( next->expr->operation == EXPR_TILT ) {
	if ( next->attr <= ATTR_T9 && next->attr >= ATTR_T0 ) {
	  BUF_FORM( ", %s", multi_attr_names[next->attr] );
	} else {
	  fprintf( stderr,
		   "\n*** dbsf: illegal '\\' in %s attribute of element %s\n",
		   multi_attr_names[next->attr], ste->name );
	  exit( 1 );
	  /* NOTREACHED */
	}
      } else {
	if ( next->expr->operation != EXPR_TILT
	     && next->attr < ATTR_MS_SHAPE
	     && ( !(next->attr & AP_MS_ATTR_BIT)
		  || ( print_aper && ( !is_tpot
				       || ( is_tpot && tpot_aper_valid[sym_data->label_type] )))) ) {
	  param_buffer( next->expr, tmp_buf );
	  BUF_FORM( ", %s = ", multi_attr_names[next->attr] );
	  BUF_FORM( "%s", tmp_buf );
	}
      }
    }
    next = next->next;
  }
	
  if ( sym_data->sub_type )
    if ( sym_data->sub_type != '\0' )
      BUF_FORM( ", type = %s", sym_data->sub_type );
}

static void find_beam_params( root )
beamnode *root;
    {
    char buffer[MAXSTRING];

    if ( root )
	{
	expand_expr( root->iteration, buffer, 0 );
	print_params( root->elem_operand );
	find_beam_params( root->beam_operand );

	find_beam_params( root->next );
	}
    }

static void print_params( root )
symbol root;
    {
    symbol_node *sym_data;
    char buffer[MAXSTRING];
    register attrnode *next;

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /* this makes sure it is only printed once */
	    {
	    switch ( sym_data->label_type )
		{
		case ATTR_DRIFT:
		case ATTR_HMONITOR:
		case ATTR_VMONITOR:
		case ATTR_MONITOR:
		case ATTR_INSTRUMENT:
		case ATTR_WIGGLER:
		case ATTR_RBEND:
		case ATTR_SBEND:
		case ATTR_QUADRUPOLE:
		case ATTR_SEXTUPOLE:
		case ATTR_OCTUPOLE:
		case ATTR_MULTIPOLE:
		case ATTR_SOLENOID:
		case ATTR_RFCAVITY:
		case ATTR_EL_SEPARATE:
		case ATTR_SROT:
		case ATTR_YROT:
		case ATTR_HKICK:
		case ATTR_VKICK:
		case ATTR_KICKER:
		case ATTR_MARKER:
		case ATTR_ECOLLIMATOR:
		case ATTR_RCOLLIMATOR:
		    next = sym_data->expr_list;

		    while ( next )
		    {
			if ( next->expr)
			    if (next->expr->operation != EXPR_TILT)
				expand_expr( next->expr, buffer, 0 );

			next = next->next;
		    }

		    break;
		    
	        case ATTR_THINLENS:
	        case ATTR_TANK:
	        case ATTR_EDGE:
	        case ATTR_PMQ:
	        case ATTR_RFQCELL:
	        case ATTR_DOUBLET:
	        case ATTR_TRIPLET:
	        case ATTR_RFGAP:
	        case ATTR_SPECIAL:
	        case ATTR_ROTATION:
                    print_buffer( "This database contains a trace3d-specific element which");
                    print_buffer( "is not valid in MAD.  This run will now terminate.");
                    print_buffer( "You may look at this lattice with the flat or trace3d option.");
		    fprintf( stderr, "This database contains a trace3d-specific element which \n" );
                    fprintf( stderr, "is not valid in MAD.  This run will now terminate. \n");
                    fprintf( stderr, "You may look at this lattice with the flat or trace3d option.\n");
		    exit(1);
                    break;

		case GEOMETRY:
		case STRENGTH:
		    print_lines( root );
		    break;

		case BEAMLINE:
		case SLOT:
	    	    find_beam_params( sym_data->beam_root );
		    break;

		case PARAM_LINE:
		    break;
		}
	    }
	}
    }

static void find_beam_elems( root )
beamnode *root;
    {
    if ( root )
	{
	print_elems( root->elem_operand );
	find_beam_elems( root->beam_operand );
	find_beam_elems( root->next );
	}
    }

static void print_elems( root )
symbol root;
{
    symbol_node *sym_data;

    if ( root )
    {
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /* this makes sure it is only printed once */
	{
	    switch ( sym_data->label_type )
	    {
	    case ATTR_DRIFT:
	    case ATTR_HMONITOR:
	    case ATTR_VMONITOR:
	    case ATTR_MONITOR:
	    case ATTR_INSTRUMENT:
	    case ATTR_WIGGLER:
	    case ATTR_RBEND:
	    case ATTR_SBEND:
	    case ATTR_QUADRUPOLE:
	    case ATTR_SEXTUPOLE:
	    case ATTR_OCTUPOLE:
	    case ATTR_MULTIPOLE:
	    case ATTR_SOLENOID:
	    case ATTR_RFCAVITY:
	    case ATTR_EL_SEPARATE:
	    case ATTR_SROT:
	    case ATTR_YROT:
	    case ATTR_HKICK:
	    case ATTR_VKICK:
	    case ATTR_KICKER:
	    case ATTR_MARKER:
	    case ATTR_ECOLLIMATOR:
	    case ATTR_RCOLLIMATOR:
		print_lines( root );
		break;
		
	    case ATTR_THINLENS:
	    case ATTR_TANK:
	    case ATTR_EDGE:
	    case ATTR_PMQ:
	    case ATTR_RFQCELL:
	    case ATTR_DOUBLET:
	    case ATTR_TRIPLET:
	    case ATTR_RFGAP:
	    case ATTR_SPECIAL:
	    case ATTR_ROTATION:
		print_buffer( "This database contains a trace3d-specific element which");
		print_buffer( "is not valid in MAD.  This run will now terminate.");
		print_buffer( "You may look at this lattice with the flat or trace3d option.");
		fprintf( stderr, "This database contains a trace3d-specific element which \n" );
		fprintf( stderr, "is not valid in MAD.  This run will now terminate. \n");
		fprintf( stderr, "You may look at this lattice with the flat or trace3d option.\n");
		exit(1);
		break;

	    case GEOMETRY:
	    case STRENGTH:
		break;

	    case BEAMLINE:
	    case SLOT:
		find_beam_elems( sym_data->beam_root );
		break;

	    case PARAM_LINE:
		break;
	    }
	}
    }
}

void print_hilly( root, eval_exprs, group_lines, std403, std81, dim_info, tpot )
symbol root;
int eval_exprs, group_lines, std403, std81, dim_info, tpot;
{
    extern int flat_expr;
    extern int standard_403;
    extern int standard_81;

    flat_expr = eval_exprs;
    standard_403 = std403;
    standard_81 = std81;
    print_aper = dim_info;
    is_tpot = tpot;

    if ( group_lines )
    {
	print_params( root );
        printf( "\n" );
        print_elems( root );
        printf( "\n" );
    }

    print_lines( root );
}


/* END OF FILE */

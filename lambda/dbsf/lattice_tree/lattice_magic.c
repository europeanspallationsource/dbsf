
/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_magic.c,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

/* prints a "flat file" format for the MAGIC?? program */
/* this was done at the request of David Johnson */
/* routines written by high-energy programmer Eric Barr */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "lattice_magic.h"
#include "lattice_gdecls.h"
#include "lattice_tree.h"
#include "lattice_defines.h"

#define BUF_FORM1(x,y)	{ sprintf( buffer, x, y ); }
#define PRINT_EOL	{ fputc('\n',stdout); }
#define WEAK_FIELD	0.0000000001

extern void print_buffer(char *);

/* 1 is the magic element number for drift, and instrument */
int drift_magic[] =       { ATTR_L, ATTR_L, ATTR_NULL };
int instrument_magic[] =  { ATTR_L, ATTR_L, ATTR_NULL };

/* 2 for horizontal, 3 for vertical */
int sbend_magic[] =       { ATTR_ANGLE, ATTR_L, ATTR_NULL };

/* 4 for horizontal, 5 for vertical */
int rbend_magic[] =       { ATTR_ANGLE, ATTR_L, ATTR_NULL };

/* 6 */
int quadrupole_magic[] =  { ATTR_K1, ATTR_L, ATTR_NULL };

/* 9 for all others */
int sextupole_magic[] =   { ATTR_L, ATTR_L, ATTR_NULL };
int multipole_magic[] =   { ATTR_L, ATTR_L, ATTR_NULL };
int octupole_magic[] =    { ATTR_L, ATTR_L, ATTR_NULL };
int hmonitor_magic[] =    { ATTR_L, ATTR_L, ATTR_NULL };
int vmonitor_magic[] =    { ATTR_L, ATTR_L, ATTR_NULL };
int monitor_magic[] =     { ATTR_L, ATTR_L, ATTR_NULL };
int wiggler_magic[] =     { ATTR_L, ATTR_L, ATTR_NULL };
int solenoid_magic[] =    { ATTR_L, ATTR_L, ATTR_NULL };
int rfcavity_magic[] =    { ATTR_L, ATTR_L, ATTR_NULL };
int elseparate_magic[] =  { ATTR_L, ATTR_L, ATTR_NULL };
int srot_magic[] =        { ATTR_L, ATTR_L, ATTR_NULL };
int yrot_magic[] =        { ATTR_L, ATTR_L, ATTR_NULL };
int hkick_magic[] =       { ATTR_L, ATTR_L, ATTR_NULL };
int vkick_magic[] =       { ATTR_L, ATTR_L, ATTR_NULL };
int kicker_magic[] =      { ATTR_L, ATTR_L, ATTR_NULL };
int marker_magic[] =      { ATTR_L, ATTR_L, ATTR_NULL };
int ecollimator_magic[] = { ATTR_L, ATTR_L, ATTR_NULL };
int rcollimator_magic[] = { ATTR_L, ATTR_L, ATTR_NULL };

static int *elem_magic_forms[] =
    {
    (int *) 0,
    drift_magic,
    hmonitor_magic,
    vmonitor_magic,
    monitor_magic,
    instrument_magic,
    wiggler_magic,
    rbend_magic,
    sbend_magic,
    quadrupole_magic,
    sextupole_magic,
    octupole_magic,
    multipole_magic,
    solenoid_magic,
    rfcavity_magic,
    elseparate_magic,
    srot_magic,
    yrot_magic,
    hkick_magic,
    vkick_magic,
    kicker_magic,
    marker_magic,
    ecollimator_magic,
    rcollimator_magic
    };

static int magic_elems[] =
    {
    9,
    1,	/* drift */
    9,	/* hmonitor */
    9,	/* vmonitor */
    9,	/* monitor */
    1,  /* instrument */
    9,	/* wiggler */
    4,	/* rbend */
    2,	/* sbend */
    6,	/* quadrupole */
    9,	/* sextupole */
    9,	/* octupole */
    9,	/* multipole */
    9,	/* solenoid */
    9,	/* rfcavity */
    9,	/* elseparate */
    9,	/* srot */
    9,	/* yrot */
    9,	/* hkick */
    9,	/* vkick */
    9,	/* kicker */
    9,	/* marker */
    8,	/* ecollimator */
    9,	/* rcollimator */
    };

/* function forward decls */
static void buffer_beam();
static void buffer_beam_backwards();

/* OUTPUT ROUTINE */

static void printb( buf )
char buf[];
    {
    /* this statement does all of the printing for the output file */
    /* not including error messages */
    printf( "%s", buf );
    }

/* ELEMENT BUFFERING ROUTINES */

extern int bend_dir;

static void find_print_arg( ste, attrnum )
symbol ste;
int attrnum;
    {
    symbol_node *sym_data = (symbol_node *)(ste->symbol_data);
    attrnode *exprlist = sym_data->expr_list;
    char buffer1[MAXSTRING];
    char *buffer = buffer1;

    while ( exprlist )
	if ( exprlist->attr == attrnum )
	    if ( attrnum == ATTR_ANGLE )
		{
		double length;
		attrnode *len_attr;

		if ( (len_attr=find_attr( ATTR_L, sym_data->expr_list )) )
		    if ( len_attr->expr )
			{
			if ( (length=eval_expr(len_attr->expr)) == 0.0 )
			    length = 1.0;
			}
		    else
			length = 1.0;
		else
		    length = 1.0;
		    
		BUF_FORM1("%23.16g",bend_dir*eval_expr(exprlist->expr)/length)
		printb( buffer );
		return;
		}
	    else
		if ( attrnum == ATTR_K1 )
		    {
		    double val = eval_expr( exprlist->expr );

		    if ( val == 0.0 ) val = WEAK_FIELD;

		    BUF_FORM1("%23.16g", val)
		    printb( buffer );
		    return;
		    }
		else
		    {
		    BUF_FORM1("%23.16g",eval_expr( exprlist->expr ))
		    printb( buffer );
		    return;
		    }
	else
	    exprlist = exprlist->next;

    BUF_FORM1( "%25s", "0" ); /* default value ? */
    printb( buffer );
    return;
    }

static void print_elem_magic( ste )
symbol ste;
    {
    symbol_node *sym_data = (symbol_node *)ste->symbol_data;

    char buffer1[MAXSTRING];
    char *buffer = buffer1;
    register int index;
    int count;

    bend_dir = 1;
    index = sym_data->label_type;
    count = magic_elems[index];
 
    if ( sym_data->declared ) /*this makes sure it is only printed once*/
       {
        switch( index )
	 {
	  case ATTR_THINLENS:
	  case ATTR_TANK:
	  case ATTR_EDGE:
	  case ATTR_PMQ:
	  case ATTR_RFQCELL:
	  case ATTR_DOUBLET:
	  case ATTR_TRIPLET:
	  case ATTR_RFGAP:
	  case ATTR_SPECIAL:
	  case ATTR_ROTATION:
              print_buffer( "This database contains a trace3d-specific element which");
              print_buffer( "is not valid in MAGIC.  This run will now terminate.");
              print_buffer( "You may look at this lattice with the flat or trace3d option.");
 	      fprintf( stderr, "This database contains a trace3d-specific element which \n" );
              fprintf( stderr, "is not valid in MAGIC.  This run will now terminate. \n");
              fprintf( stderr, "You may look at this lattice with the flat or trace3d option.\n");
              exit(1);
              break;
          default:
              break;
         }
       }

    if ( index == ATTR_MARKER )
	{
	BUF_FORM1( "%i   0                           0                      0", count );
	printb( buffer );
	PRINT_EOL
	return;
	}

    if( count == 2 || count == 4 )
	{
	register attrnode *exprlist = sym_data->expr_list;

	while ( exprlist )
	    if ( exprlist->attr == ATTR_TILT )
		{
		if( fabs(eval_expr( exprlist->expr ) - HALFPI) < TOLERANCE )
		    count++;
		else
		    if( fabs(eval_expr( exprlist->expr ) + HALFPI) < TOLERANCE )
			{
			count++;
			bend_dir = -1;
			}
		    else
			if ( fabs(eval_expr( exprlist->expr )) > TOLERANCE )
			    fprintf( stderr, "*** ERROR: unsupported magnet tilt != 90,-90,0\n" );
		break;
		}
	    else
		exprlist = exprlist->next;
	}

    BUF_FORM1( "%i   0     ", count );
    printb( buffer );

    count = -1;

    while ( elem_magic_forms[index][++count] )
	find_print_arg( ste, elem_magic_forms[index][count] );

    PRINT_EOL
    }

/* BEAM BUFFERING ROUTINES */

static beamnode *get_arg( num, args )
double num;
beam_param_node *args;
    {
    while ( --num > 0 )
	if ( args )
	    args = args->next;
	else
	    {
	    fprintf( stderr, "Flat Print: not enough arguments to sub_line\n");
	    num = 0.0;
	    }

    if ( args )
        return ( args->beam_root );
    else
	{
	fprintf( stderr, "Flat Print: not enough arguments to sub_line\n");
	return (beamnode *)NULL;
	}
    }

static void insert_args( map, args )
beamnode *map;
beam_param_node *args;
    {
    register beam_param_node *next;

    while ( map )
	{
	if ( map->operation == BEAM_ARGUMENT )
	    map->beam_operand = get_arg( eval_expr( map->iteration ), args );
	else
	    {
	    if ( map->operation == BEAM_CALL_FUNC )
		{
		next = map->beam_params;

		while ( next )
		    {
		    insert_args( next->beam_root, args );
		    next = next->next;
		    }
		}
	    else
		{
		insert_args( map->beam_operand, args );
		if ( map->elem_operand )
		    insert_args( ((symbol_node *)(map->elem_operand->symbol_data))->beam_root, args );
		}
	    }
	map = map->next;
	}
    }

static void remove_args( map )
beamnode *map;
    {
    register beam_param_node *next;

    while ( map )
	{
	if ( map->operation == BEAM_ARGUMENT )
	    map->beam_operand = (beamnode *)NULL;
	else
	    {
	    if ( map->operation == BEAM_CALL_FUNC )
		{
		next = map->beam_params;

		while ( next )
		    {
		    remove_args( next->beam_root );
		    next = next->next;
		    }
		}
	    else
		{
		remove_args( map->beam_operand );
		if ( map->elem_operand )
		    remove_args( ((symbol_node *)(map->elem_operand->symbol_data))->beam_root );
		}
	    }
	map = map->next;
	}
    }

static void do_beam_call( map, args, buffer, transpose )
beamnode *map;
beam_param_node *args;
char *buffer;
int transpose;
    {
    insert_args( map, args );

    if ( transpose )
	buffer_beam_backwards( map, buffer );
    else
	buffer_beam( map, buffer );

    remove_args( map );
    }

static void buffer_beam( beam, buffer )
beamnode *beam;
char *buffer;
    {
    register double repeat;
    symbol_node *sym_data;

    while ( beam )
	{
        switch (beam->operation)
            {
            case BEAM_NO_OP:
		if ( beam->beam_operand )
		    buffer_beam( beam->beam_operand, buffer );
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE
		      || sym_data->label_type == SLOT )
			buffer_beam( sym_data->beam_root, buffer );
		    else
			print_elem_magic( beam->elem_operand );
		    }
	        break;

            case BEAM_REPEAT:
		for ( repeat=eval_expr( beam->iteration ); repeat>0; repeat-=1 )
		    if ( beam->beam_operand )
			buffer_beam( beam->beam_operand, buffer );
		    else
			{
			sym_data =
			    (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE
			  || sym_data->label_type == SLOT )
			    buffer_beam( sym_data->beam_root, buffer );
			else
			    print_elem_magic( beam->elem_operand );
			}
	        break;

            case BEAM_TRANSPOSE:
		if ( beam->beam_operand )
		    buffer_beam_backwards( beam->beam_operand, buffer );
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE
		      || sym_data->label_type == SLOT )
			buffer_beam_backwards( sym_data->beam_root, buffer );
		    else
			print_elem_magic( beam->elem_operand );
		    }

	        break;

	    case BEAM_CALL_FUNC:
		sym_data = (symbol_node *)(beam->elem_operand->symbol_data);
		do_beam_call( sym_data->beam_root, beam->beam_params, buffer,0);
		break;

	    case BEAM_ARGUMENT:
		if ( beam->beam_operand )
		    buffer_beam( beam->beam_operand, buffer );
		else
		    if ( beam->elem_operand )
			{
			sym_data =
			    (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE
			  || sym_data->label_type == SLOT )
			    buffer_beam( sym_data->beam_root, buffer );
			else
			    print_elem_magic( beam->elem_operand );
			}
		    else
			{
			fprintf( stderr,
			    "buffer_beam: unresolved argument found\n" );
			exit( 1 );
			/*NOTREACHED*/
			}
			
		break;

            default:
	        fprintf( stderr,
		    "\nbuffer_beam: bad beam operation %d\n", beam->operation );
	        exit( 1 );
	        /*NOTREACHED*/
            }

	beam = beam->next;
	}
    }

static void buffer_beam_backwards( beam, buffer )
beamnode *beam;
char *buffer;
    {
    register double repeat;
    symbol_node *sym_data;

    if ( beam )
	{
	buffer_beam_backwards( beam->next, buffer );

        switch (beam->operation)
            {
            case BEAM_REPEAT:
		for ( repeat=eval_expr(beam->iteration); repeat>0; repeat-=1 )
		    if ( beam->beam_operand )
			buffer_beam_backwards( beam->beam_operand, buffer );
		    else
			{
			sym_data =
			    (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE
			  || sym_data->label_type == SLOT )
			    buffer_beam_backwards( sym_data->beam_root,buffer );
			else
			    print_elem_magic( beam->elem_operand );
			}
	        break;

            case BEAM_TRANSPOSE:
		if ( beam->beam_operand )
		    buffer_beam( beam->beam_operand, buffer );
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE
		      || sym_data->label_type == SLOT )
			buffer_beam( sym_data->beam_root, buffer );
		    else
			print_elem_magic( beam->elem_operand );
		    }
	        break;

            case BEAM_NO_OP:
		if ( beam->beam_operand )
		    buffer_beam_backwards( beam->beam_operand, buffer );
		else
		    {
		    sym_data = (symbol_node *)(beam->elem_operand->symbol_data);

		    if ( sym_data->label_type == BEAMLINE
		      || sym_data->label_type == SLOT )
			buffer_beam_backwards( sym_data->beam_root, buffer );
		    else
			print_elem_magic( beam->elem_operand );
		    }
	        break;

	    case BEAM_CALL_FUNC:
		sym_data = (symbol_node *)(beam->elem_operand->symbol_data);
		do_beam_call( sym_data->beam_root, beam->beam_params, buffer,1);
		break;

	    case BEAM_ARGUMENT:
		if ( beam->beam_operand )
		    buffer_beam_backwards( beam->beam_operand, buffer );
		else
		    if ( beam->elem_operand )
			{
			sym_data =
			    (symbol_node *)(beam->elem_operand->symbol_data);

			if ( sym_data->label_type == BEAMLINE
			  || sym_data->label_type == SLOT )
			    buffer_beam_backwards( sym_data->beam_root, buffer);
			else
			    print_elem_magic( beam->elem_operand );
			}
		    else
			{
			fprintf( stderr,
			    "buffer_beam: unresolved argument found\n" );
			exit( 1 );
			/*NOTREACHED*/
			}
			
		break;

            default:
	        fprintf( stderr, "\nbuffer_beam: bad beam operation\n" );
	        exit( 1 );
	        /*NOTREACHED*/
            }
	}
    }

/* MAIN ROUTINE */

void print_magic( root )
symbol root;
    {
    symbol_node *sym_data;
    char buffer1[MAXSTRING];
    char *buffer = buffer1;

    if ( root )
	{
        sym_data = (symbol_node *)root->symbol_data;

	if ( sym_data->declared ) /*this makes sure it is only printed once*/
	    {
	    switch( sym_data->label_type )
	        {
	        case ATTR_DRIFT:
	        case ATTR_HMONITOR:
	        case ATTR_VMONITOR:
	        case ATTR_MONITOR:
	        case ATTR_INSTRUMENT:
	        case ATTR_WIGGLER:

	        case ATTR_RBEND:
	        case ATTR_SBEND:
	        case ATTR_QUADRUPOLE:
	        case ATTR_SEXTUPOLE:
	        case ATTR_OCTUPOLE:
	        case ATTR_SOLENOID:
	        case ATTR_EL_SEPARATE:
	        case ATTR_HKICK:
	        case ATTR_VKICK:
	        case ATTR_KICKER:
	        
	        case ATTR_RFCAVITY:
		case ATTR_ECOLLIMATOR:
	        case ATTR_RCOLLIMATOR:

	        case ATTR_MARKER:

	        case ATTR_SROT:
	        case ATTR_YROT:

	        case ATTR_MULTIPOLE:
		    print_elem_magic( root );
		    sym_data->declared = 0;
		    break;

	        case ATTR_THINLENS:
	        case ATTR_TANK:
	        case ATTR_EDGE:
	        case ATTR_PMQ:
	        case ATTR_RFQCELL:
	        case ATTR_DOUBLET:
	        case ATTR_TRIPLET:
	        case ATTR_RFGAP:
	        case ATTR_SPECIAL:
	        case ATTR_ROTATION:
                    print_buffer( "This database contains a trace3d-specific element which");
                    print_buffer( "is not valid in MAGIC.  This run will now terminate.");
                    print_buffer( "You may look at this lattice with the flat or trace3d option.");
		    fprintf( stderr, "This database contains a trace3d-specific element which \n" );
                    fprintf( stderr, "is not valid in MAGIC.  This run will now terminate. \n");
                    fprintf( stderr, "You may look at this lattice with the flat or trace3d option.\n");
		    exit(1);
                    break;

	        case GEOMETRY:
	        case STRENGTH:
		case PARAM_LINE:
		    break;

	        case BEAMLINE:
		case SLOT:
		    buffer_beam( sym_data->beam_root, buffer );
		    break;

	        default:
		    fprintf( stderr, "something is amiss in print_lines\n" );

	        }
	    printb( "" );
	    sym_data->declared = 0; /*this makes sure it is only printed once*/
	    }
	}
    }

/* END OF FILE */

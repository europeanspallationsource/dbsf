
/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_flat.h,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

/* prints a "flat file" format from a lattice description tree */

extern void print_flat( /* symbol root, int old_flat */ );

extern int small_bend( /* symbol ste */ ); /* for sds file format use */


/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_gdecls.c,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

/* LATTICE GLOBAL DECLARATIONS */

#include "lattice_gdecls.h"
#include "lattice_defines.h"

/* the names of the element attributes for error messages */
/* this array is indexed by the ATTR_ element attrs. i.e. ATTR_ANGLE */
// Add here the NCELLS and DTL_DELL attributes according to their DEFINES...
char *elem_attr_names[] =
    {
    "",
    "angle",
    "betrf",
    "e",
    "e1",
    "e2",
    "fint",
    "freq",
    "h1",
    "h2",
    "harmon",
    "hgap",
    "hkick",
    "k1",
    "k2",
    "k3",
    "kick",
    "ks",
    "l",
    "lag",
    "pg",
    "shunt",
    "tfill",
    "tilt",
    "vkick",
    "volt",
    "xsize",
    "ysize",
    "xfocal",
    "yfocal",
    "zfocal",
    "in_rad",
    "out_rad",
    "dist_betw",
    "bfld_in",
    "bfld_out",
    "l_in",
    "l_out",
    "radius",
    "gap",
    "ff1",
    "ff2",
    "egapvolt",
    "phase",
    "egflag",
    "dwflag",
    "vr2",
    "av",
    "cell",
    "accel",
    "iden_cav",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "typeaper",
    "xapsize",
    "yapsize",
    "xoffset",
    "yoffset",
    "magshape",
    "xmagsize",
    "ymagsize",
    "xmagoffset",
    "ymagoffset"
    };

/* this is the character representation of the default values for */
/* unspecified attributes */
/* THE BLANK ONES ARE WRONG */
/* this array is indexed by the ATTR_ element attrs. i.e. ATTR_ANGLE */
char *elem_attr_defaults[] =
    {
    "", /* this one is ok */
    "0.0",	/*angle*/
    "0.0",	/*betrf*/
    "0.0",	/*e*/
    "0.0",	/*e1*/
    "0.0",	/*e2*/
    "0.0",	/*fint*/
    "0.0",	/*freq*/ /* WRONG FIXXX ME */
    "0.0",	/*h1*/
    "0.0",	/*h2*/
    "1.0",	/*harmon*/
    "0.0",	/*hgap*/
    "0.0",	/*hkick*/
    "0.0",	/*k1*/
    "0.0",	/*k2*/
    "0.0",	/*k3*/
    "0.0",	/*kick*/
    "0.0",	/*ks*/
    "0.0",	/*l*/
    "0.0",	/*lag*/
    "0.0",	/*pg*/
    "0.0",	/*shunt*/
    "0.0",	/*tfill*/
    "0.0",	/*tilt*/
    "0.0",	/*vkick*/
    "0.0",	/*volt*/
    "1.0",	/*xsize*/
    "1.0",	/*ysize*/
    "0.0",      /*xfocal*/
    "0.0",      /*yfocal*/
    "0.0",      /*zfocal*/
    "0.0",      /*in_rad*/
    "0.0",      /*out_rad*/
    "0.0",      /*dist_betw*/
    "0.0",      /*bfld_in*/
    "0.0",      /*bfld_out*/
    "0.0",      /*l_in*/
    "0.0",      /*l_out*/
    "0.0",      /*radius*/
    "0.0",      /*gap*/
    "0.0",      /*ff1*/
    "0.0",      /*ff2*/
    "0.0",      /*egapvolt*/
    "0.0",      /*phase*/
    "0.0",      /*egflag*/
    "0.0",      /*dwflag*/
    "0.0",      /*vr2*/
    "0.0",      /*av*/
    "0.0",      /*cell*/
    "0.0",      /*accel*/
    "0.0"       /*iden_cav*/
    };

/* this array is indexed by the ATTR_ multipole attrs. i.e. ATTR_K1L */
char *multi_attr_names[] =
    {
    "",
    "k0l",
    "k1l",
    "k2l",
    "k3l",
    "k4l",
    "k5l",
    "k6l",
    "k7l",
    "k8l",
    "k9l",
    "t0",
    "t1",
    "t2",
    "t3",
    "t4",
    "t5",
    "t6",
    "t7",
    "t8",
    "t9",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "ashape",
    "axsize",
    "aysize",
    "acenterx",
    "acentery",
    "mshape",
    "mxsize",
    "mysize",
    "mcenterx",
    "mcentery"
    };

/* the names of the elements for error messages */
/* this array is indexed by the ATTR_ elements. i.e. ATTR_DRIFT */
char *elem_names[] =
    {
    "",
    "drift",
    "hmonitor",
    "vmonitor",
    "monitor",
    "instrument",  /* 5 */
    "wiggler",
    "rbend",
    "sbend",
    "quadrupole",
    "sextupole",    /* 10 */
    "octupole",
    "multipole",
    "solenoid",
    "rfcavity",
    "elseparato",  /* 15 */
    "srot",
    "yrot",
    "hkick",
    "vkick",
    "kicker",      /* 20 */
    "marker",
    "ecollimator",
    "rcollimator",
    "tbend",
    "thinlens",    /* 25 */
    "tank",
    "edge",
    "pmq",
    "rfqcell",
    "doublet",    /* 30 */
    "triplet",
    "rfgap",
    "special",
    "rotation",
    "beamline",   /* 35 */
    "slot",
    "superslot",
    "tcelement",
    "drift", // dtl cell
    "drift", /* 40 */ // ncells
    "drift", // bend
    "drift" // rfq_cell
    };

/* these specify the order that element attributes should be emitted in for */
/* flat and sds style outputs */

/* these specify the number of additional attributes for each of the */
/* extra attribute elements */
#define BEND_MORE       9
#define RFCAV_MORE      7
#define MULTI_MORE      20
#define KICK_MORE       2
#define COLL_MORE       2
#define DOUB_MORE       1
#define TRIP_MORE       5
#define THIN_MORE       3
#define EDGE_MORE       5
#define RFQ_MORE        4
#define RFGAP_MORE      5
#define PMQ_MORE        2
#define TANK_MORE       3

int drift_args[] = { ATTR_L, ATTR_NULL };
int tce_args[] = { ATTR_L, ATTR_NULL };

int ecollimator_args[] = { ATTR_L, A_DELIM, COLL_MORE, ATTR_XSIZE, ATTR_YSIZE,
	ATTR_NULL };
int rcollimator_args[] = { ATTR_L, A_DELIM, COLL_MORE, ATTR_XSIZE, ATTR_YSIZE,
	ATTR_NULL };

int elseparate_args[] = { ATTR_L, ATTR_E, ATTR_TILT, ATTR_NULL };
int hkick_args[] = { ATTR_L, ATTR_KICK, ATTR_TILT, ATTR_NULL };
int hmonitor_args[] = { ATTR_L, ATTR_NULL };
int instrument_args[] = { ATTR_L, ATTR_NULL };

int kicker_args[] = { ATTR_L, ATTR_TILT, A_DELIM, KICK_MORE, ATTR_KICKH, ATTR_KICKV,
	ATTR_NULL };

int marker_args[] = { ATTR_NULL };
int monitor_args[] = { ATTR_L, ATTR_NULL };

int multipole_args[] = { A_DELIM, MULTI_MORE, ATTR_K0L, ATTR_K1L,
	ATTR_K2L, ATTR_K3L, ATTR_K4L, ATTR_K5L, ATTR_K6L, ATTR_K7L, ATTR_K8L,
	ATTR_K9L, ATTR_T0, ATTR_T1, ATTR_T2, ATTR_T3, ATTR_T4, ATTR_T5,
	ATTR_T6, ATTR_T7, ATTR_T8, ATTR_T9, ATTR_NULL };

int octupole_args[] = { ATTR_L, ATTR_K3, ATTR_TILT, ATTR_NULL };
int sextupole_args[] = { ATTR_L, ATTR_K2, ATTR_TILT, ATTR_NULL };
int quadrupole_args[] = { ATTR_L, ATTR_K1, ATTR_TILT, ATTR_NULL };

int rbend_args[] = { ATTR_L, ATTR_ANGLE, ATTR_TILT, A_DELIM, BEND_MORE,
	ATTR_K1, ATTR_K2, ATTR_K3, ATTR_E1, ATTR_E2, ATTR_FINT, ATTR_HGAP,
	ATTR_H1, ATTR_H2, ATTR_NULL };
int sbend_args[] = { ATTR_L, ATTR_ANGLE, ATTR_TILT, A_DELIM, BEND_MORE,
	ATTR_K1, ATTR_K2, ATTR_K3, ATTR_E1, ATTR_E2, ATTR_FINT, ATTR_HGAP,
	ATTR_H1, ATTR_H2, ATTR_NULL };

int rfcavity_args[] = { ATTR_L, A_DELIM, RFCAV_MORE, ATTR_VOLT, ATTR_LAG,
	ATTR_HARMON, ATTR_BETRF, ATTR_PG, ATTR_SHUNT, ATTR_TFILL, ATTR_NULL };

int solenoid_args[] = { ATTR_L, ATTR_KS, ATTR_NULL };
int srot_args[] = { ATTR_ANGLE, ATTR_NULL };
int vkick_args[] = { ATTR_L, ATTR_KICK, ATTR_TILT, ATTR_NULL };
int vmonitor_args[] = { ATTR_L, ATTR_NULL };
int wiggler_args[] = { ATTR_L, ATTR_TILT, ATTR_NULL };
int yrot_args[] = { ATTR_ANGLE, ATTR_NULL };

int tbend_args[] = { ATTR_NULL };

int thinlens_args[] = { A_DELIM, THIN_MORE, ATTR_XFOCAL, ATTR_YFOCAL, ATTR_ZFOCAL, ATTR_NULL };
int tank_args[] = { ATTR_L, A_DELIM, TANK_MORE, ATTR_ACCEL, ATTR_PHASE, ATTR_IDEN_CAV, ATTR_NULL };
int edge_args[] = { A_DELIM, EDGE_MORE, ATTR_ANGLE, ATTR_RADIUS, ATTR_GAP, ATTR_FF1, ATTR_FF2, ATTR_NULL };
int pmq_args[] = { ATTR_L, ATTR_K1, A_DELIM, PMQ_MORE, ATTR_IN_RAD, ATTR_OUT_RAD, ATTR_NULL };
int rfqcell_args[] = {ATTR_L,A_DELIM, RFQ_MORE, ATTR_VR2, ATTR_AV, ATTR_PHASE, ATTR_CELL, ATTR_NULL};
int doublet_args[] = { ATTR_L, ATTR_K1, A_DELIM, DOUB_MORE, ATTR_DIST_BETW, ATTR_NULL };
int triplet_args[] = {A_DELIM, TRIP_MORE, ATTR_BFLD_OUT, ATTR_L_OUT, ATTR_DIST_BETW, ATTR_BFLD_IN, ATTR_L_IN, ATTR_NULL};
int rfgap_args[] = {A_DELIM, RFGAP_MORE, ATTR_EGAPVOLT, ATTR_PHASE, ATTR_EGFLAG, ATTR_DWFLAG, ATTR_HARMON, ATTR_NULL};
int special_args[] = { ATTR_L, ATTR_NULL };
int rotation_args[] = { ATTR_ANGLE, ATTR_NULL };

int dtl_cell_args[] = { ATTR_LENGTH, ATTR_LQ1, ATTR_LQ2, ATTR_CELLCENTER, ATTR_B1P, ATTR_B2P, ATTR_E0TL, ATTR_RFPHASE, ATTR_APERTURE, ATTR_ABSPHASE, ATTR_BETAS, ATTR_TTIME, ATTR_KTSP, ATTR_K2TSP, ATTR_NULL };
int ncells_args[] = { ATTR_MODE, ATTR_CELLNUMBER, ATTR_BETAG, ATTR_E0T, ATTR_RFPHASE, ATTR_APERTURE, ATTR_ABSPHASE, ATTR_KE0TI, ATTR_KE0TO, ATTR_DZI, ATTR_DZO, ATTR_BETAS, ATTR_TTIME, ATTR_KTSP, ATTR_K2TSPP, ATTR_TTIMEIN, ATTR_KTIP, ATTR_K2TIPP, ATTR_TTIMEOUT, ATTR_KTOP, ATTR_K2TOPP, ATTR_LENGTH, ATTR_NULL };
int rfq_cell_args[] = { ATTR_MEANV, ATTR_VANER, ATTR_ACCPARAM, ATTR_MODULATION, ATTR_LENGTH, ATTR_RFPHASE, ATTR_CELLTYPE, ATTR_TCURV, ATTR_TFOCUS, ATTR_NULL};
int bend_args[] = { ATTR_L, ATTR_ANGLE, ATTR_DTILT, ATTR_K1, ATTR_K2, ATTR_K3, ATTR_E1, ATTR_E2, ATTR_FINT, ATTR_HGAP, ATTR_H1, ATTR_H2, ATTR_APERTURE, ATTR_ZERO, ATTR_NULL };

int *elem_arg_forms[] =
    {
    (int *) 0,
    drift_args,
    hmonitor_args,
    vmonitor_args,
    monitor_args,
    instrument_args,   /* 5 */
    wiggler_args,
    rbend_args,
    sbend_args,
    quadrupole_args,
    sextupole_args,    /* 10 */
    octupole_args,
    multipole_args,
    solenoid_args,
    rfcavity_args,
    elseparate_args,   /* 15 */
    srot_args,
    yrot_args,
    hkick_args,
    vkick_args,
    kicker_args,      /* 20 */
    marker_args,
    ecollimator_args,
    rcollimator_args,
    tbend_args,
    thinlens_args,    /* 25 */
    tank_args,
    edge_args,
    pmq_args,
    rfqcell_args,
    doublet_args,      /* 30 */
    triplet_args,
    rfgap_args,
    special_args,
    rotation_args,
    (int *) 0,
    (int *) 0,
    (int *) 0,
    tce_args,
    dtl_cell_args,
    ncells_args,
    rfq_cell_args
};

/* END OF FILE */

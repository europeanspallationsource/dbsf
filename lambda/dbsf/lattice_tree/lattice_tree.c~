
/* $Header: /scratch/trahern/dbsf/lattice_tree/RCS/lattice_tree.c,v 1.1 1994/06/07 19:31:42 trahern Exp $ */

/* LATTICE TREE ROUTINES */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "lattice_tree.h"
#include "lattice_gdecls.h"
#include "../std_lib/utility.h"

extern void print_lines();

/* BEAMLINE ROUTINES */

/* returns pointer to last node in a beamline */
beamnode *beam_tail( beamnode *beam )
{
  register beamnode *next=beam;
  if (!next) {
    fprintf(stderr,"Error: beam_tail() called with NULL beam\n");
    exit(2);
  }
  while ( next->next )  next = next->next;
  return( next );
}

beamnode *get_beam_node( beam_optype op, exprnode *repeat, symbol name, beamnode *beam )
{
  register beamnode *new_node;
  double num = 0.0;

  if ( repeat )
    num = repeat->value;

  if ( (new_node = (beamnode *)malloc( sizeof( beamnode ) )) ) {
    if ( num < 0 ) {
      repeat->value = -repeat->value;

      new_node->operation = BEAM_TRANSPOSE;
      new_node->iteration = (exprnode *)NULL;
      new_node->beam_params = (beam_param_node *)NULL;
      new_node->beam_operand = get_beam_node( BEAM_REPEAT, repeat, name, beam );
    } else {
      new_node->operation = op;
      new_node->iteration = repeat;
      new_node->elem_operand = name;

      if ( op == BEAM_CALL_FUNC )	{
	new_node->beam_params = split_beam( beam );
	new_node->beam_operand = (beamnode *)NULL;
      } else {
	new_node->beam_operand = beam;
	new_node->beam_params = (beam_param_node *)NULL;
      }
    }

    new_node->next = (beamnode *)NULL;
    return ( new_node );
  } else {
    fprintf( stderr, "Symbol Table: not enough memory, parsing aborted\n" );
    exit( 1 );
    /* NOTREACHED */
  }

}


/* SYMBOL TABLE ROUTINES */

void set_label_type( symbol ste, int label_type )
{
  symbol_node *sym_data = (symbol_node*)(ste->symbol_data);

  if ( sym_data->declared ) {
    if ( sym_data->label_type != label_type )
      fprintf( stderr, "Symbol Table: '%s' already declared\n", ste->name );
  } else {
    sym_data->label_type = label_type;
    sym_data->declared = 1;
  }
}

void get_sym_data( symbol ste )
{
  symbol_node *new_node;

  if ( (new_node = (symbol_node *)malloc( sizeof( symbol_node ) )) ) {
    new_node->label_type = 0;
    new_node->sub_type = NULL;
    new_node->context = NO_TYPE;
    new_node->declared = 0;
    new_node->in_stack = 0;
    new_node->arg_num = 0;
    new_node->num_args = 0;
    new_node->beam_root = (beamnode *)NULL;
    new_node->expr_list = (attrnode *)NULL;
    // (symbol_node *)(ste->symbol_data) = new_node;
    ste->symbol_data = new_node;
  } else {
    fprintf( stderr, "Symbol Table: not enough memory, parsing aborted\n" );
    exit( 1 );
    /*NOTREACHED*/
  }
}

/* checks consistent usage of id (if not yet used, sets context to <context>) */
int check_set_context( symbol ste, context_type context )
{
  symbol_node *sym_data = (symbol_node *)ste->symbol_data;

  if ( sym_data->context ) {
    if ( sym_data->context != context ) return ( 0 );
    else return ( 1 );
  } else {
    sym_data->context = context;
    return ( 1 );
  }

}

void set_context( symbol ste, context_type context )
{
  symbol_node *sym_data = (symbol_node *)ste->symbol_data;

  sym_data->context = context;
}

symbol get_symbol_node( char *name, symbol_table sym_table )
{
  symbol tmp_sym=sym_find(sym_table, name);

  if ( tmp_sym == NULL_SYMBOL ) {
    tmp_sym = sym_install( sym_table, copy_string( name ), 0 );
    get_sym_data( tmp_sym );
  }

  return ( tmp_sym );
}

/* returns TRUE if the name is not in the sym_tab and FALSE if it is */
int is_new_name( char *name, symbol_table sym_tab )
{
  return ( sym_find( sym_tab, name ) == NULL_SYMBOL );
}

int check_type( symbol ste, int type )
{
  symbol_node *sym_data = (symbol_node *)(ste->symbol_data);

  if ( sym_data->declared && sym_data->label_type == type )
    return ( 1 );
  else
    return ( 0 );
}

/* BEAM PARAM LIST ROUTINES */

beam_param_node *get_bpn( beamnode *beam )
{
  beam_param_node *new_node;

  if ( (new_node = (beam_param_node *)malloc( sizeof( beam_param_node ) )) ){
    new_node->next = (beam_param_node *)NULL;
    new_node->beam_root = beam;
    return ( new_node );
  } else {
    fprintf( stderr, "Parser: memory allocation error\n" );
    exit( 1 );
    /*NOTREACHED*/
  }
}

beam_param_node *split_beam( beamnode *beam )
{
  register beamnode *next = beam;
  beam_param_node *head = get_bpn( (beamnode *)NULL );
  register beam_param_node *last = head;
  register beam_param_node *new = (beam_param_node *)NULL;

  while ( next ) {			/* if there is another node */
    new = get_bpn( next );		/* make a new argument */

    next = next->next;		/* advance and unlink */
    beam->next = (beamnode *)NULL;
    beam = next;

    last->next = new;		/* new is now last so reset last */
    last = new;
  }

  new = head->next;
  free( (char *)head );
  return ( new );
}


/* ATTRNODE ROUTINES */

/* this function finds an attribute in an attr_list and returns a pointer to */
/* the attrnode which describes it.  Returns NULL if it isn't there */
attrnode *find_attr( int attr, attrnode* attr_list )
{
  while ( attr_list )
    if( attr_list->attr == attr ) return( attr_list );
    else attr_list = attr_list->next;

  return( attr_list );
}

attrnode *last_node( attrnode *attr_list )
{
  register attrnode *next_node = attr_list;

  if ( next_node ) {
    while ( next_node->next )
      next_node = next_node->next;
    return ( next_node );
  }

  return ( (attrnode *)NULL );
}

attrnode *link_attr_node( attrnode *old_list, exprnode *expr, int attr )
{
  attrnode *new_attrnode, *new_head;

  if ( (new_attrnode = (attrnode *)malloc( sizeof( attrnode ) )) ) {
    new_attrnode->next = (attrnode *)NULL;
    new_attrnode->expr = expr;
    new_attrnode->attr = attr;

    if ( (new_head = last_node( old_list )) ) {
      new_head->next = new_attrnode;
      return ( old_list );
    } else {
      return ( new_attrnode );
    }
  } else {
    fprintf( stderr, "Parser: memory allocation error\n" );
    exit( 1 );
    /*NOTREACHED*/
  }
}



/* EXPRNODE ROUTINES */
#define OPT_PAREN(x)	{ if ( parens ) BUF_CHAR(x) }

/* this maps the expression operation defines on to character equivalents */
char expr_op_text[] = " +-*/";

/* this array tells us when we have to parenthesize sub expressions */
int noparens[] = { 0, 0, 0, 0, 0 };
int subparens[] = { 0, 1, 1, 0, 0 };
int divparens[] = { 0, 1, 1, 1, 1 };
int *leftparens[] = { noparens, noparens, noparens, subparens, subparens };
int *rightparens[] = { noparens, noparens, subparens, subparens, divparens };

/* this loads an expression into <buffer> and resolves the declaration of */
/* scalar variables contained in the expression if <decls> is TRUE */
char *buffer_expr( exprnode *expr, char *buffer, int parens, int decls )
{
  if ( expr ) {
    switch ( expr->operation ) {
    case EXPR_SUB:
    case EXPR_ADD:
    case EXPR_MUL:
    case EXPR_DIV:
      OPT_PAREN('(');
      buffer = buffer_expr( expr->op1, buffer,
			    leftparens[expr->operation][expr->op1->operation], decls );
      BUF_FORM(" %c ",expr_op_text[expr->operation]);
      buffer = buffer_expr( expr->op2, buffer,
			    rightparens[expr->operation][expr->op2->operation], decls );
      OPT_PAREN(')');
      break;

    case EXPR_UNEG:
      BUF_CHAR('-');
      buffer = buffer_expr( expr->op1, buffer, 1, decls );
      break;

    case EXPR_FUNC:
      BUF_FORM("%s( ",expr->id->name);
      buffer = buffer_expr( expr->op1, buffer, 0, decls );

      if ( expr->op2 ) {	/* if there is another argument */
	BUF_CHAR(',');
	BUF_CHAR(' ');
	buffer = buffer_expr( expr->op2, buffer, 0, decls );
      }

      BUF_CHAR(' ');
      BUF_CHAR(')');
      break;

    case EXPR_STE:
      BUF_FORM("%s",expr->id->name);

      if ( decls )
	print_lines( expr->id );

      break;

    case EXPR_NUM:
      BUF_FORM(FLOAT_FORMAT,expr->value);
      break;

    case EXPR_TILT:
      BUF_CHAR('\\');
      break;

    case EXPR_SHAPE:
      BUF_CHAR( expr->funcname );
      break;

    case EXPR_MPT:
      // This does not get called
      BUF_FORM(FLOAT_FORMAT, expr->value );
      break;

    default:
      fprintf( stderr, "buffer_expr: bad operation '%d'\n",
	       expr->operation );
      exit( 1 );
      /*NOTREACHED*/
    }
  }

  BUF_CHAR('\0');
  --buffer; /* we don't want to count the null character */
  return ( buffer );
}

exprnode *get_expr_node( int op, exprnode *left, exprnode *right, double value )
{
  register exprnode *new_node;

  if ( (new_node = (exprnode *)malloc( sizeof( exprnode ) )) ) {
    new_node->operation = op;
    new_node->op1 = left;
    new_node->op2 = right;
    new_node->evaluated = 0;
    new_node->value = value;

    return ( new_node );
  } else {
    fprintf( stderr, "Parser: not enough memory, parsing aborted" );
    exit( 1 );
    /*NOTREACHED*/
  }
}

extern double sqrt();
extern double log();
extern double exp();
extern double sin();
extern double cos();
extern double tan();
extern double asin();
extern double atan();

double eval_func( int func, double op1, double op2 )
{
  switch ( func ) {
  case FUNC_SQRT:
    return ( sqrt( op1 ) );
  case FUNC_LOG:
    return ( log( op1 ) );
  case FUNC_EXP:
    return ( exp( op1 ) );
  case FUNC_SIN:
    return ( sin( op1 ) );
  case FUNC_COS:
    return ( cos( op1 ) );
  case FUNC_TAN:
    return ( tan( op1 ) );
  case FUNC_ASIN:
    return ( asin( op1 ) );
  case FUNC_ATAN:
    return ( atan( op1 ) );
  case FUNC_ABS:
    return ( (op1 > 0 ? op1 : -op1) );
  case FUNC_MAX:
    return ( (op1 > op2 ? op1 : op2) );
  case FUNC_MIN:
    return ( (op1 > op2 ? op2 : op1) );
  case FUNC_RANF:
    fprintf( stderr, "Parser: No random number functions defined\n" );
    return ( 1 );
  case FUNC_GAUSS:
  case FUNC_TGAUSS:
    fprintf( stderr, "Parser: No gaussian functions defined\n" );
    return ( 1 );
  case FUNC_USER0:
  case FUNC_USER1:
  case FUNC_USER2:
    fprintf( stderr, "Parser: No user functions defined\n" );
    return ( 1 );
  default:
    fprintf( stderr, "Parser: Bad function in eval_func\n" );
    return ( 1 );
  }
}

int get_funcname( char func[] )
{
  switch ( func[0] )	{
  case 'a':
    if ( !( strcmp( func, "asin" ) ) ) return ( FUNC_ASIN );
    if ( !( strcmp( func, "abs" ) ) ) return ( FUNC_ABS );
    if ( !( strcmp( func, "atan" ) ) ) return ( FUNC_ATAN );
    return ( 0 );
  case 'c':
    if ( !( strcmp( func, "cos" ) ) ) return ( FUNC_COS );
    return ( 0 );
  case 'e':
    if ( !( strcmp( func, "exp" ) ) ) return ( FUNC_EXP );
    return ( 0 );
  case 'g':
    if ( !( strcmp( func, "gauss" ) ) ) return ( FUNC_GAUSS );
    return ( 0 );
  case 'l':
    if ( !( strcmp( func, "log" ) ) ) return ( FUNC_LOG );
    return ( 0 );
  case 'm':
    if ( !( strcmp( func, "max" ) ) ) return ( FUNC_MAX );
    if ( !( strcmp( func, "min" ) ) ) return ( FUNC_MIN );
    return ( 0 );
  case 'r':
    if ( !( strcmp( func, "ranf" ) ) ) return ( FUNC_RANF );
    return ( 0 );
  case 's':
    if ( !( strcmp( func, "sqrt" ) ) ) return ( FUNC_SQRT );
    if ( !( strcmp( func, "sin" ) ) ) return ( FUNC_SIN );
    return ( 0 );
  case 't':
    if ( !( strcmp( func, "tan" ) ) ) return ( FUNC_TAN );
    if ( !( strcmp( func, "tgauss" ) ) ) return ( FUNC_TGAUSS );
    return ( 0 );
  case 'u':
    if ( !( strcmp( func, "user0" ) ) ) return ( FUNC_USER0 );
    if ( !( strcmp( func, "user1" ) ) ) return ( FUNC_USER1 );
    if ( !( strcmp( func, "user2" ) ) ) return ( FUNC_USER2 );
    return ( 0 );
  default:
    return ( 0 );
  }
}


double eval_expr( exprnode *root )
{
  symbol_node *sym_data;

  if ( root ) {
      if ( !(root->evaluated) ) {
	switch ( root->operation ) {
	case EXPR_ADD:
	  root->value = eval_expr( root->op1 )+eval_expr( root->op2 );
	  break;

	case EXPR_SUB:
	  root->value = eval_expr( root->op1 )-eval_expr( root->op2 );
	  break;

	case EXPR_MUL:
	  root->value = eval_expr( root->op1 )*eval_expr( root->op2 );
	  break;

	case EXPR_DIV:
	  root->value = eval_expr( root->op1 )/eval_expr( root->op2 );
	  break;

	case EXPR_NUM:
	  break;

	case EXPR_FUNC:
	  root->value = eval_func( root->funcname, eval_expr( root->op1 ), eval_expr( root->op2 ) );
	  break;

	case EXPR_STE:
	  sym_data = (symbol_node *)(root->id->symbol_data);
	  root->value = eval_expr( sym_data->expr_list->expr );
	  break;

	case EXPR_UNEG:
	  root->value = -eval_expr( root->op1 );
	  break;

	case EXPR_TILT:
	  /*	    fprintf( stderr,"\n*** eval_expr: illegal TILT specifier '\\' found in expression\n");  */
	  /*	    exit( 1 );         */
	  /*NOTREACHED*/
	  break;

	case EXPR_SHAPE:
	  root->value = (double)(*root->funcname);
	  break;
	
	case EXPR_MPT:
          root->value = (double)(root->value);
	  break;

	case EXPR_COMMAND: break;

	default:
	  fprintf( stderr,"something is amiss in eval_expr switch\n");
	  exit( 1 );
	  /*NOTREACHED*/
	  break;
	}
	root->evaluated = 1; /* mark as evaluated */
      }
      return ( root->value );
    }
    else
      return ( 0.0 );
  }

  /* END OF FILE */

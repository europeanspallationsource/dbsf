!print bpm
!print all
origin x 2.43 y 0.97 z 3.88 si 0.3 theta -0.2
survey 
topdrawer
stop

-----
notes
-----
1.	anything written after 'stop' is unread
2.	anything after ! is a comment
3.	the following symbols are legal breaks between command tokens  
	{ ' ' \t , & = % }
4.	blanks lines are ignored
5.	dictionaries of keywords are defined in $LAMBDA/include/survey_dict.h

-------------------------------------
examples of legal command line syntax
-------------------------------------
print location_1 location_2
print all
print #s/e

origin y 1.2
origin y = 1.2
origin, y, 1.2
origin x 1.2 y 3.4 z 5.6 theta 7.8 fi 9.0 si 0.1 s 2.3

stop

survey

topdrawer

#if !defined(twiss_sds_h)
#define twiss_sds_h 1

/* $Header: /usr2/local/lambda/twiss/include/RCS/twiss_sds.h,v 1.3 1994/02/22 20:47:29 mackay Exp $ */

#include "Sds/sdsgen.h"

struct type_list twiss_input_tl[] =
{
  { (long)40, SDS_FSTRING },
  { (long)0, SDS_RETLIST },
  { (long)0, SDS_ENDLIST }
};

char twiss_input_nm[] = "input_file";

struct type_list tw_tl[] = 
{
  { (long)1, SDS_INT },
  { (long)1, SDS_INT },
  { (long)1, SDS_INT },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)1, SDS_DOUBLE },
  { (long)0, SDS_RETLIST },
  { (long)0, SDS_ENDLIST }
};

char tw_nm[] = 
  "lattice_index,element_index,type_index,pathlen,beta_x,alfa_x,gamma_x,beta_y,alfa_y,gamma_y,eta_x,eta_xp,eta_y,eta_yp,eta_z,eta_1,mu_x,mu_y,bm_xx,bm_xxp,bm_xy,bm_xyp,bm_xz,bm_xu,bm_xpxp,bm_xpy,bm_xpyp,bm_xpz,bm_xpu,bm_yy,bm_yyp,bm_yz,bm_yu,bm_ypyp,bm_ypz,bm_ypu,bm_zz,bm_zu,bm_uu";
 
#endif

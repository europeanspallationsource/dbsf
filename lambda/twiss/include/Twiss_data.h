/* $Header: /usr2/local/lambda/twiss/include/RCS/Twiss_data.h,v 1.3 1994/02/18 21:32:51 mackay Exp $ */

#ifndef TWISS_DATA_H
#define TWISS_DATA_H 1

#ifndef TWISS_H
#include "twiss.h"
#endif

/*
#ifndef ISTKsdsgen_h
#include <Sds/sdsgen.h>
#endif
*/

typedef struct
{
  twiss_ifile_name *flat_file_ptr;
  optic *optic_ptr;
  int number_of_optic;
} Twiss_data;

#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN extern
#endif

EXTERN  int twiss_in_sds(sds_handle sds, Twiss_data *odp);

#undef EXTERN

#endif /* TWISS_DATA_H */

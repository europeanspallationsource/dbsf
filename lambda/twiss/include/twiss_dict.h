#ifndef TWISS_DICT_H
#define TWISS_DICT_H

/* $Header: /rap/lambda/twiss/include/RCS/twiss_dict.h,v 1.6 1994/09/11 19:14:34 mackay Exp $ */

#define DICT_SIZE       64
#define MAX_TOKEN_SIZE  132
#define MAX_TOKEN_COUNT 100

/*----------------------------------------------------------------------*/
/*     dictionaries                                                     */
/*----------------------------------------------------------------------*/

typedef char token[MAX_TOKEN_SIZE];
typedef struct {
  token piece[MAX_TOKEN_COUNT];
} array_of_tokens;

enum command_type {no_keyword, deltap, gammat_sens, init_condx, offset, 
		     periodic, print, quitit, stop, topdrawer, twiss, football,
		     update, quiet_mode, readit, help, matrix, cmd_error};
typedef struct {
  enum command_type dict[DICT_SIZE];
  token             word[DICT_SIZE];
} command_struct;
command_struct command = {
  {no_keyword, deltap, gammat_sens, init_condx, offset, periodic, print,
     quitit, stop, topdrawer, twiss, football, update, quiet_mode, readit,
     help, matrix, cmd_error},
  {"", "delta", "gammat_sens", "initial", "initial_offset", "periodic", 
     "print", "quit",  "stop", "topdrawer", "twiss", "football", "update",
     "quiet", "read", "help", "matrix"}
};

enum mark_type {all, mark_named_element};
typedef struct {
  enum mark_type dict[DICT_SIZE];
  token          word[DICT_SIZE];
} mark_struct;
mark_struct mark = {
  {all, all, mark_named_element},
  {"all", "#s/e"}
};

enum offset_type {s_off, mu_x_off, mu_y_off, offset_error};
typedef struct {
  enum offset_type dict[DICT_SIZE];
  token            word[DICT_SIZE];
} offset_struct;
offset_struct offset_names = {
  {s_off,      mu_x_off,     mu_y_off,     offset_error},
  {"s_offset","mu_x_offset","mu_y_offset"}
};

enum twiss_type {beta_x, beta_y, alfa_x, alfa_y, eta_x, eta_y, eta_xp, eta_yp,
     bm_xx,   bm_xxp,   bm_xy,   bm_xyp,   bm_xz,   bm_xu,
              bm_xpxp,  bm_xpy,  bm_xpyp,  bm_xpz,  bm_xpu,
                        bm_yy,   bm_yyp,   bm_yz,   bm_yu,
                                 bm_ypyp,  bm_ypz,  bm_ypu,
                                           bm_zz,   bm_zu,
                                                    bm_uu,
		 twiss_error};

typedef struct {
  enum twiss_type dict[DICT_SIZE];
  token           word[DICT_SIZE];
} twiss_struct;
twiss_struct twiss_names = {
  {beta_x, beta_y, alfa_x, alfa_y, eta_x, eta_y, eta_xp, eta_yp,
     bm_xx,   bm_xxp,   bm_xy,   bm_xyp,   bm_xz,   bm_xu,
              bm_xpxp,  bm_xpy,  bm_xpyp,  bm_xpz,  bm_xpu,
                        bm_yy,   bm_yyp,   bm_yz,   bm_yu,
                                 bm_ypyp,  bm_ypz,  bm_ypu,
                                           bm_zz,   bm_zu,
                                                    bm_uu,
  twiss_error},

  {"beta_x", "beta_y", "alfa_x", "alfa_y",
   "eta_x", "eta_y", "eta_xp", "eta_yp",
   "xx",   "xxp",   "xy",   "xyp",   "xz",   "xu",
           "xpxp",  "xpy",  "xpyp",  "xpz",  "xpu",
                    "yy",   "yyp",   "yz",   "yu",
                            "ypyp",  "ypz",  "ypu",
                                     "zz",   "zu",
                                             "uu"
					          }
};

enum foot_type
   {lbl_beta_x, lbl_alfa_x, lbl_gamma_x,
    lbl_beta_y, lbl_alfa_y, lbl_gamma_y,
    lbl_eta_x, lbl_eta_xp, lbl_eta_y, lbl_eta_yp,
    lbl_eta_z, lbl_eta_1, lbl_mu_x, lbl_mu_y,
     lbl_xx,   lbl_xxp,    lbl_xy,    lbl_xyp,   lbl_xz,   lbl_xu,
               lbl_xpxp,   lbl_xpy,   lbl_xpyp,  lbl_xpz,  lbl_xpu,
                           lbl_yy,    lbl_yyp,   lbl_yz,   lbl_yu,
                                      lbl_ypyp,  lbl_ypz,  lbl_ypu,
                                                 lbl_zz,   lbl_zu,
                                                           lbl_uu,
    lbl_error};

typedef struct {
  enum foot_type dict[DICT_SIZE];
  token          word[DICT_SIZE];
} foot_struct;

foot_struct label_footprint = {
  {lbl_beta_x, lbl_alfa_x, lbl_gamma_x,
   lbl_beta_y, lbl_alfa_y, lbl_gamma_y,
   lbl_eta_x, lbl_eta_xp, lbl_eta_y, lbl_eta_yp,
   lbl_eta_z, lbl_eta_1, lbl_mu_x, lbl_mu_y,
   lbl_xx,  lbl_xxp,  lbl_xy,  lbl_xyp,  lbl_xz,  lbl_xu,
            lbl_xpxp, lbl_xpy, lbl_xpyp, lbl_xpz, lbl_xpu,
                      lbl_yy,  lbl_yyp,  lbl_yz,  lbl_yu,
                               lbl_ypyp, lbl_ypz, lbl_ypu,
                                         lbl_zz,  lbl_zu,
                                                  lbl_uu,
  lbl_error},
  {
   "beta_x", "alfa_x", "gamma_x",
   "beta_y", "alfa_y", "gamma_y",
   "eta_x",  "eta_xp",  "eta_y",   "eta_yp",
   "eta_z",  "eta_1",  "mu_x", "mu_y",
   "xx",   "xxp",   "xy",   "xyp",   "xz",   "xu",
           "xpxp",  "xpy",  "xpyp",  "xpz",  "xpu",
                    "yy",   "yyp",   "yz",   "yu",
                            "ypyp",  "ypz",  "ypu",
                                     "zz",   "zu",
                                             "uu"
  }
};


#endif /* TWISS_DICT_H */

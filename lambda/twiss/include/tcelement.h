/* $Header: /usr2/local/lambda/twiss/include/RCS/tcelement.h,v 1.1 1993/08/28 20:46:29 mackay Exp $ */
typedef struct {     /* mat_elt  for linear matrix */
  char    index[8];
  double  value;
} tcelement;

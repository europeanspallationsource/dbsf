/* $Header: /usr2/local/lambda/twiss/include/RCS/twiss_func_extern.h,v 1.1 1994/02/22 20:47:29 mackay Exp $ */
/* Define some prototypes for the functions in "twiss_func.h" */

#ifndef TWISS_FUNC_EXTERN_H
#define TWISS_FUNC_EXTERN_H 1

#ifndef TWISS_H
#include <twiss.h>
#endif

#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN extern
#endif

EXTERN void alloc_twiss(twiss_data *line_ptr);
EXTERN void cp_matrix(double **a,double **b,int n);
EXTERN void cp_vector(double *x, double * y, int n);
EXTERN void create_transfer_matrices(twiss_data *line_ptr);
EXTERN double *dim1(int grid);
EXTERN double **dim2(int grid, int row);
EXTERN double ***dim3(int grid, int row, int col);
EXTERN void free2(double **pa);
EXTERN void free3(double ***pa);
EXTERN void get_drift(double **m, double length);
EXTERN void get_quad(double **m, double length, double strength, double tilt,
                     double del);
EXTERN void get_rbend(double **m, double length, double bend_angle, double k1,
                      double e1, double e2, double tilt, double del);
EXTERN void get_sbend(double **m, double length, double bend_angle,
                      double k1, double e1, double e2, double tilt,
		      double del);
EXTERN void get_srot(double **m, double strength);
EXTERN void get_yrot(double **m, double strength);
EXTERN int get_tcelement(char *database_name, char *element_name, double **m);
EXTERN void get_sext(double **m, double length, double strength, double tilt,
                     double del, double *offset);
EXTERN void get_transfer_matrix(twiss_data *line_ptr);
EXTERN void initialize_twiss_data(twiss_data *line_ptr, flatin_data *flat_ptr);
EXTERN void matrix_matrix(double **c, double **a, double **b, int n);
EXTERN void matrix_vector(double *y, double **a, double *x, int n);
EXTERN void partition(double **m, double **a_x, double **a_y,
	              double **m_x, double **m_y);
EXTERN void periodic_twiss(twiss_data *line_ptr);
EXTERN void print_matrix(FILE *file_ptr, double **m, int n);
EXTERN void print_vect(FILE *file_ptr, double *x, int n);
EXTERN void propagate_twiss(twiss_data *line_ptr);
EXTERN void unit_matrix(double **m, int n);
EXTERN void propagate_football(twiss_data *line_ptr);

/*======  For completeness let's put in the prototypes for "twiss_top.c" ====*/

EXTERN void write_twiss_top(twiss_data *line_ptr);



#undef EXTERN

#endif /* TWISS_FUNC_EXTERN_H */



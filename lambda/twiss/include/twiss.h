#ifndef TWISS_H
#define TWISS_H         1

/* $Header: /usr2/local/lambda/twiss/include/RCS/twiss.h,v 1.3 1993/09/20 14:12:03 mackay Exp $ */

#ifndef FLATIN_H
#include <flatin.h>
#endif

#define FALSE           0
#define TRUE            1
#define BUFF_MAX        156   /* Here we assume that Courier8 is used in */
#define HEADER_PERIOD   58    /* landscape mode. */

typedef struct {
  double **matrix;
  int repout;
} magmat;

typedef struct {
  char filename[40];
} twiss_ifile_name;

typedef struct {
  int lattice_index;
  int element_index;
  int type_index;
  double pathlen;
  double beta_x;
  double alfa_x;
  double gamma_x;
  double beta_y;
  double alfa_y;
  double gamma_y;
  double eta_x;
  double eta_xp;
  double eta_y;
  double eta_yp;
  double eta_z;
  double eta_1;
  double mu_x;
  double mu_y;
  double bm_xx;
  double bm_xxp;
  double bm_xy;
  double bm_xyp;
  double bm_xz;
  double bm_xu;
  double bm_xpxp;
  double bm_xpy;
  double bm_xpyp;
  double bm_xpz;
  double bm_xpu;
  double bm_yy;
  double bm_yyp;
  double bm_yz;
  double bm_yu;
  double bm_ypyp;
  double bm_ypz;
  double bm_ypu;
  double bm_zz;
  double bm_zu;
  double bm_uu;
} optic;

typedef struct {
  double co_x;
  double co_xp;
  double co_y;
  double co_yp;
  double co_dt;
  double co_delta;
} clorb;

typedef struct {
  time_t time_stamp;
  char   flat_file_name[NAME_MAX];
  double delta;
  double s_offset;
  double mu_x_offset;
  double mu_y_offset;
  double beta_x_max;
  double beta_y_max;
  double eta_x_min;
  double eta_x_max;
  double eta_y_min;
  double eta_y_max;
  double gamma_t;
  double quad_strength_max;
  clorb  init_clorb;
  optic  init_optic;
  double        **transfer_matrix;
  clorb  *clorb_ptr;
  flatin_data   *flatin_ptr;
  magmat *magmat_ptr;
  optic  *optic_ptr;
} twiss_data;

/*----------------------------------------------------------------------*/
/*     externals                                                        */
/*----------------------------------------------------------------------*/

extern double pi, twopi, twopinv;
extern double *dim1();
extern double **dim2();
extern legal legal_type[];

#endif /* TWISS_H */

#ifndef TWISS_TOP_H
#define TWISS_TOP_H         1

#define TOP_BUFF_MAX    1000
#define TOP_SPACE       0.25
#define BEND_SPACE      0.12
#define W_Y_MIN         1.0
#define W_Y_MAX         8.5

#endif /* TWISS_H */

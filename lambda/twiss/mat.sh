#!/bin/sh

ind1=`echo 'use atr_gddb
go
select "ooga", atom_index from NameLookup where SiteWideName like "'$1'"
go
'|isql -Uharmless -Pharmless|awk '/^ ooga/{print $2}'`

ind2=`echo 'use atr_gddb
go
select "ooga", atom_index from NameLookup where SiteWideName like "'$2'"
go
'|isql -Uharmless -Pharmless|awk '/^ ooga/{print $2}'`

echo transfer matrix from $1 to $2 is
testmat Twiss.YTransfer ${ind1} ${ind2}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <Twiss_data.h>
#include <lambda_matrix.h>

extern int get_mat32(int index1, int index2, Twiss_data tdp, double **m);

Twiss_data tdp;
sds_handle sds_twiss;

char twissfile[120];
int index1, index2;

/*===========================*/
int process_cmd(int argc, char **argv)
{
  if(argc==4)
    {
      strcpy(twissfile,argv[1]);
      index1 = atoi(argv[2]);
      index2 = atoi(argv[3]);
    } else {
      fprintf(stderr,"Usage: %s <filename> index1 index2\n",argv[0]);
      fprintf(stderr,
	      "\twhere <filename> is a Twiss SDS file in shared memory.\n");
      return 1;
    }
  return 0;
}
/*===========================*/
int get_Twiss(char *filnam)
{
  char errmsg[120];
  int answer;

  sds_twiss = sds_access(filnam,SDS_SHARED_MEM,SDS_WRITE);

  if(sds_twiss < 0)
    {
      sprintf(errmsg,"get_Twiss: Could not sds_access(%s)\n",filnam);
      sds_perror(errmsg);
      return 1;
    }

  answer = twiss_in_sds(sds_twiss, &tdp);
  return answer;
}
/*===========================*/
void dump_mat32(double **m)
{
  int i, j;
  for(i=0;i<6;i++)
    {
      for(j=0;j<6;j++)
	{
	  printf("  %10.6f",m[i][j]);
	}
      printf("\n");
    }
}
/*===========================*/
int main(int argc, char **argv)
{
  double **m = dim2(6,6);

  if(process_cmd(argc,argv))exit(1);

  sds_init();

  if(get_Twiss(twissfile))exit(1);
  
  get_mat32(index1,index2,tdp,m);

  dump_mat32(m);

  free2(m);

  return 0;
}

/* $Header: /rap/lambda/twiss/RCS/twiss.c,v 1.29 1996/06/27 20:39:29 mackay Exp $ */
/* $Id: twiss.c,v 1.29 1996/06/27 20:39:29 mackay Exp $ */
/*----------------------------------------------------------------------

     APRIL 1992
     Written by: ALEX BOGACZ & steve peggs & Waldo MacKay
     for: propagating twiss functions and closed orbit through 
     a beamline read by flatin.c or flatin_sds.c .

     August  '92, sp: first stable release
     January '93, sp: added 'gamma_t' function
     May     '93, wwm: Added SROT, YROT
                       Added TILTs, edge angles, and gradients to bends.
		       Also fixed the eta functions to allow for coupling.
		       SDS i/o added by Chris Saltmarsh.
		       Now propagates the beam hyperfootball (non-periodic).
     August  '93, wwm: Write out input file name in the sds output file.
                       Fix sds ouput to output "all" elements regardless
		       of the arguments specified in the print command.
     August  '93, wwm: Add code to allow using SDS files in shared memory.
                       Fix pointers to lattice array -- Twiss now outputs
		       pointers to "lattice", "element", and "type_index".
		       Renamed labels in the SDS output to correspond to
		       the "optic" structure names.
		       Added new structure to SDS file which contains the
		       name of the input flat-SDS file.
		       Fixed problem in topdrawer output, which may be
		       due to using a non-ansi compiler.
     28 Aug, '93, wwm: Add code to read in tcelements ("get_tcelement").
     15 Sep, '93, wwm: Fix write_twiss(...) headers and put a WARNING into
                       get_yrot(...).  Update "Usage:" message.
*/
/* $Log: twiss.c,v $
 * Revision 1.29  1996/06/27  20:39:29  mackay
 * update to use libdbapps.a for the tcelement stuff.  It has
 * not been debugged totally, but I plan on changing the form
 * of the tcelement table to a better form.
 *
 * Revision 1.28  1994/09/11  19:14:00  mackay
 * add a command for printing the complete transfer matrix.
 *
 * Revision 1.27  1994/09/09  23:08:12  mackay
 * decouple initialization of Twiss parameters and beam matrix
 * values.
 *
 * Revision 1.26  1994/06/14  12:51:03  mackay
 * change to new sds functions.
 *
 * Revision 1.25  1994/05/25  20:54:38  mackay
 * fix it so the initial command should work after using the
 * twiss and football commands.
 *
 * Revision 1.24  1994/05/14  18:51:50  mackay
 * Now includes <lambda_matrix.h> for prototypes of general
 * purpose matrix functions.
 *
 * Revision 1.23  1994/05/11  19:54:06  mackay
 * Now twiss reads initialization values from optic tw_save[0] if tw_save!=0
 * and if it is a nonperiodic solution.
 *
 * Revision 1.22  1994/05/10  19:12:20  mackay
 * Test to see if twiss is in interactive mode before exit(1) in many cases.
 *
 * Revision 1.21  1994/05/09  18:30:05  mackay
 * make it type out update info if not in "quiet" mode.
 *
 * Revision 1.20  1994/03/08  15:41:53  mackay
 * change the enumeration of "read" to "readit" so that it would not clash
 * with the read in <unistd.h>
 *
 * Revision 1.19  1994/03/07  21:56:07  mackay
 * make twiss start up in quiet mode when interactive.
 *
 * Revision 1.18  1994/03/07  20:48:05  mackay
 * put in fflush after every prompt in -i mode.
 *
 * Revision 1.17  1994/03/06  20:32:22  mackay
 * add a help command
 *
 * Revision 1.16  1994/03/06  20:14:02  mackay
 * add a "read" command for the interactive mode.  this command reads in
 *  commands from the given file, e.g., twiss.cmd.
 *
 * Revision 1.15  1994/03/06  19:29:36  mackay
 * change the interactive prompt
 *
 * Revision 1.14  1994/03/06  16:04:08  mackay
 * fix a minor bug.
 *
 * Revision 1.13  1994/03/06  15:25:33  mackay
 * add commands quit, quiet, and update
 *
 * Revision 1.12  1994/02/22  20:42:26  mackay
 * Everything should be ansified and some of the memory leaks
 * have been fixed (But not quite all of them.)  Chris thinks
 * that some of them may be buried in DB_lib stuff.
 *
 * Revision 1.11  1994/01/31  14:40:19  mackay
 * fix help message to add the -e switch for sds error reporting.
 *
 * Revision 1.10  1994/01/31  14:20:21  mackay
 * Change the Twiss output file name to Twiss.____ where ____ is the name
 * of the input lattice_file.  This should allow multiple Twiss files in the
 * same directory, i.e., for storage and injection ... .
 * */
/*----------------------------------------------------------------------*/

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <flatin.h>
#include <lattice_defines.h>
#include <twiss.h>
#include <twiss_dict.h>
#include <twiss_sds.h>
#include <twiss_func_extern.h>
#include <lambda_matrix.h>

extern int *latindices;

/*----------------------------------------------------------------------*/
/*     globals                                                          */
/*----------------------------------------------------------------------*/

FILE *tlog, *cmd_file;
array_of_tokens *cmd_token_ptr;

optic *tw_save = 0;

int token_count;
int line_count        = 0;
int header            = TRUE;
int verbose           = FALSE;
int interact          = FALSE;  /* flag for interactive mode */
int stdin_open        = FALSE;  /* flag for interactive mode open */
int periodic_soln     = FALSE;
int tev_config_format = FALSE;
int quiet_flag        = FALSE;  /* stop printout for running with glish
                                 * in interactive mode */
int nfootprint, footprint[DICT_SIZE];
static char version[] = "$Id: twiss.c,v 1.29 1996/06/27 20:39:29 mackay Exp $";

double pi, twopi, twopinv;
int errout = 0;

extern int shared_memory;   /* flag for using shared memory */

#ifndef NOSYBASE
#include <sybfront.h>
#include <sybdb.h>
#include <syb_tables.h>
DBPROCESS *dbproc;
char database[31];
#endif

/*===========================================*/
/* function prototypes */
void initial_offset(twiss_data *line_ptr);
void initial_twiss(twiss_data *line_ptr);
void mark_print(twiss_data *line_ptr);
array_of_tokens *parsef(char *line, int *t_count);
void periodic_solution(twiss_data *main_ptr, twiss_data *side_ptr);
void write_gammat_header(void);
void write_gammat_sensitivity(twiss_data *line_ptr);
void write_super_header(twiss_data *line_ptr);
void write_twiss(twiss_data *line_ptr);
void write_twiss_header(void);
void write_twiss_summary(twiss_data *line_ptr);
void write_twiss_sds(twiss_data *line_ptr);
void set_print_football(void);
void write_football(twiss_data *line_ptr);
void write_football_header(void);





/*----------------------------------------------------------------------*/

void update_element(int j, twiss_data *line_ptr)
{
  flatin_data  *f_ptr = line_ptr->flatin_ptr;
  double **m;
  double length, strength, tilt, k1, e1, e2;
  int moreind;

  if(j<=0)
    {
      fprintf(stderr,"? update_element: j=%d < 0.\n",j);
      return;
    }
  if(j>=f_ptr->number_of_elements)
    {
      fprintf(stderr,"? update_element: j=%d > last element = %d.\n",j,
	      f_ptr->number_of_elements-1);
      return;
    }

  m        = line_ptr->magmat_ptr[j].matrix;
  length   = f_ptr->element_ptr[j].length;
  strength = f_ptr->element_ptr[j].strength;
  tilt     = f_ptr->element_ptr[j].tilt;

  switch(f_ptr->element_ptr[j].type_index){
  case ATTR_QUADRUPOLE:
    get_quad(m,length,strength,tilt,line_ptr->delta);      
    if (fabs(strength) > line_ptr->quad_strength_max) 
      line_ptr->quad_strength_max = fabs(strength);
    if(!quiet_flag)fprintf(stderr,"update quad\tindex=%4d  strength=%f\n",j,strength);
    break;
  case ATTR_RBEND:
    if(f_ptr->element_ptr[j].npars > 0){
      moreind = f_ptr->element_ptr[j].more_index;
      k1 = f_ptr->parameter_ptr[moreind].value;
      e1 = f_ptr->parameter_ptr[moreind+3].value;
      e2 = f_ptr->parameter_ptr[moreind+4].value;
    } else {
      k1 = 0;
      e1 = 0;
      e2 = 0;
    }
    get_rbend(m,length,strength,k1,e1,e2,tilt,line_ptr->delta);
    if(!quiet_flag)fprintf(stderr,"update rbend\tindex=%4d  strength=%f  k1=%f\n",
			  j,strength,k1);
    break;
  case ATTR_SBEND:
    if(f_ptr->element_ptr[j].npars > 0){
      moreind = f_ptr->element_ptr[j].more_index;
      k1 = f_ptr->parameter_ptr[moreind].value;
      e1 = f_ptr->parameter_ptr[moreind+3].value;
      e2 = f_ptr->parameter_ptr[moreind+4].value;
    } else {
      k1 = 0;
      e1 = 0;
      e2 = 0;
    }
    get_sbend(m,length,strength,k1,e1,e2,tilt,line_ptr->delta);
    if(!quiet_flag)fprintf(stderr,"update sbend\tindex=%4d  strength=%f  k1=%f\n",
			  j,strength,k1);
    break;
  case ATTR_SROT:
    get_srot(m,strength);
    break;
  case ATTR_YROT:
    get_yrot(m,strength);
    break;
  case ATTR_TCELEMENT:  /* Don't bother to update tcelements. */
/*    get_tcelement(getenv("DBSF_DB"),
**		  f_ptr->element_ptr[j].name, m);
*/
    break;
  default:
    /* works even if legal_type.has_length = 0 */
    get_drift(m,length);
    break;
  }
}

/*----------------------------------------------------------------------*/
void reinit_twiss(twiss_data *line_ptr)
{  /* Copy initial values of optic structure from twiss output file. */
  line_ptr->init_optic.pathlen = tw_save[0].pathlen;
  line_ptr->init_optic.beta_x = tw_save[0].beta_x;
  line_ptr->init_optic.alfa_x = tw_save[0].alfa_x;
  line_ptr->init_optic.gamma_x = tw_save[0].gamma_x;
  line_ptr->init_optic.beta_y = tw_save[0].beta_y; 
  line_ptr->init_optic.alfa_y = tw_save[0].alfa_y; 
  line_ptr->init_optic.gamma_y = tw_save[0].gamma_y;
  line_ptr->init_optic.eta_x = tw_save[0].eta_x;  
  line_ptr->init_optic.eta_xp = tw_save[0].eta_xp; 
  line_ptr->init_optic.eta_y = tw_save[0].eta_y;  
  line_ptr->init_optic.eta_yp = tw_save[0].eta_yp;
  line_ptr->init_optic.eta_z = tw_save[0].eta_z;
  line_ptr->init_optic.eta_1 = tw_save[0].eta_1;
  line_ptr->init_optic.mu_x = tw_save[0].mu_x;
  line_ptr->init_optic.mu_y = tw_save[0].mu_y;
  line_ptr->init_optic.bm_xx = tw_save[0].bm_xx;
  line_ptr->init_optic.bm_xxp = tw_save[0].bm_xxp;
  line_ptr->init_optic.bm_xy = tw_save[0].bm_xy;
  line_ptr->init_optic.bm_xyp = tw_save[0].bm_xyp;
  line_ptr->init_optic.bm_xz = tw_save[0].bm_xz;
  line_ptr->init_optic.bm_xu = tw_save[0].bm_xu;
  line_ptr->init_optic.bm_xpxp = tw_save[0].bm_xpxp;
  line_ptr->init_optic.bm_xpy = tw_save[0].bm_xpy;
  line_ptr->init_optic.bm_xpyp = tw_save[0].bm_xpyp;
  line_ptr->init_optic.bm_xpz = tw_save[0].bm_xpz;
  line_ptr->init_optic.bm_xpu = tw_save[0].bm_xpu;
  line_ptr->init_optic.bm_yy = tw_save[0].bm_yy;
  line_ptr->init_optic.bm_yyp = tw_save[0].bm_yyp;
  line_ptr->init_optic.bm_yz = tw_save[0].bm_yz;
  line_ptr->init_optic.bm_yu = tw_save[0].bm_yu;
  line_ptr->init_optic.bm_ypyp = tw_save[0].bm_ypyp;
  line_ptr->init_optic.bm_ypz = tw_save[0].bm_ypz;
  line_ptr->init_optic.bm_ypu = tw_save[0].bm_ypu;
  line_ptr->init_optic.bm_zz = tw_save[0].bm_zz;
  line_ptr->init_optic.bm_zu = tw_save[0].bm_zu;
  line_ptr->init_optic.bm_uu = tw_save[0].bm_uu;
}
/*----------------------------------------------------------------------*/

void main(int argc,char **argv)
{
  int c, cmdline_counter, i, k;
  char cmdline[BUFF_MAX];
  enum command_type command_dict;

  twiss_data  main_line, side_line;
  flatin_data main_flat, side_flat;

  char twiss_cmd_file[100];



  initialize_twiss_data(&main_line,&main_flat);
  initialize_twiss_data(&side_line,&side_flat);

  tlog = fopen("twiss.log", "w");
  if(tlog==NULL)
    {
      fprintf(stderr,"Could not open 'twiss.log'.  No log will be written.\n");
      tlog = fopen("/dev/null","w");
      if(tlog==NULL)
	{
	  fprintf(stderr,"Could not open '/dev/null' for log output!!!\n");
	  exit(1);
	}
    }

  pi      = 4.0 * atan(1.0);
  twopi   = 2.0 * pi;
  twopinv = 1.0/twopi;
  
/*-----------------------------------------------------------------*/

  while( --argc > 0  &&  (*++argv)[0] == '-') {
    while(( c = *++argv[0] )) {
      switch(c) {
      case 'h':
	header = FALSE;
	break;
      case 'g':
	tev_config_format = TRUE;
	break;
      case 's':
	shared_memory = TRUE;
	break;
      case 'v':
	verbose = TRUE;
	break;
      case 'e':
	errout = SDS_ERROR;
	break;
      case 'i':
	interact = TRUE;
	quiet_flag = TRUE;   /* start with everything quiet. */
	break;
      default:
	fprintf(stderr,"twiss: illegal command line option %c \n", c);
	if(!interact)exit(1);
      }
    }
  }
  sds_init();
  sds_output_errors(errout);
  sds_output_proginfo(errout);

  if ( argc == 1 ) {
    strcpy(main_line.flat_file_name,*argv);
  }
  else {
    printf("Usage: twiss [-ghsv] flat_file_name\n");
    printf("  flat_file_name is a beam line in 'flat' or 'SDS' format\n");
    printf("  -e   to print out SDS error messages\n");
    printf("  -g   to format standard output like 'tev_config'\n");
    printf("  -h   to NOT send headers to standard output\n");
    printf("  -i   to accept input from stdin\n");
    printf("  -s   to use shared memory rather than disk files\n");
    printf("  -v   for verbose logging output to 'twiss.log'\n");
    printf("  twiss also expects to find a 'twiss.cmd' file\n");
    printf("\nRCS Version = %s\n",version);
    exit(1);
  }

  time(&main_line.time_stamp);
  fprintf(tlog,"%s%s\n",ctime(&main_line.time_stamp),main_line.flat_file_name);

  alloc_twiss(&main_line);
  create_transfer_matrices(&main_line);

  /* --------------------------------------------------------------------*/
  /*      Read the command file TWISS.CMD                               */
  /*---------------------------------------------------------------------*/

  if(interact)
    {
      cmd_file = stdin;
      stdin_open = TRUE;
      printf("twiss<\n");
      fflush(stdout);
    } else {
      cmd_file = fopen("twiss.cmd", "r");
      if (cmd_file == NULL)
	{
	  fprintf(stderr,"twiss: couldn't find a 'twiss.cmd' file\n");
	  if(!interact)exit(1);
	}
    }

  cmdline_counter = 0;
  while( fgets(cmdline, BUFF_MAX, cmd_file) != NULL )  {
    cmd_token_ptr = parsef(cmdline, &token_count);
    
    if (token_count) {
      fprintf(tlog,"twiss.cmd %-3d: ", ++cmdline_counter);
      for(i = 0; i < token_count; i++) 
	fprintf(tlog,"%s ", cmd_token_ptr->piece[i] );
      fprintf(tlog,"\n");
      
      /*------------------------------------------------------------------*/
      /*               Interpret the command line                         */
      /*------------------------------------------------------------------*/
      
      command_dict = cmd_error ;
      for(k = 0; k < DICT_SIZE; k++)  {
	if( strcmp(cmd_token_ptr->piece[0], command.word[k]) == 0) {
	  command_dict = command.dict[k];
	  break;
	}
      }
      
      switch(command_dict)  {
      case no_keyword:
	break;
      case deltap:
	if (sscanf(cmd_token_ptr->piece[1],"%lf",
		   &main_line.delta) == EOF) {
	  fprintf(stderr,"twiss: garbled definition of deltap/p \n");
	  if(!interact)exit(1);
	}
	break;
      case gammat_sens:
	write_gammat_sensitivity(&main_line);
	break;
      case help:
	printf("A list of possible commands is:\n");
	printf("   deltap <#>\n");
	printf("   gammat_sens\n");
	printf("   initial [<name> <#>] ...\n");
	printf("   initial_offset [<name> <#>] ...\n");
	printf("   periodic\n");
	printf("   print [<name>] ...\n");
	printf("   print all\n");
	printf("   print\n");
	printf("   topdrawer\n");
	printf("   twiss\n");
	printf("   football [<name>] ...\n");
	printf("   matrix\n");
	printf("   update <#>\n");
	printf("   quiet\n");
	printf("   read <filename>\n");
	printf("   help\n");
	printf("   quit\n");
	printf("   stop\n");
        break;
      case init_condx:
	periodic_soln = FALSE;
	initial_twiss(&main_line);
	break;
      case offset:
	initial_offset(&main_line);
	break;
      case periodic:
	periodic_soln = TRUE;
	periodic_solution(&main_line,&side_line);
	break;
      case print:
	quiet_flag = FALSE;
	mark_print(&main_line);
	break; 
      case quiet_mode:
	quiet_flag = TRUE;
	break;
      case quitit:
	if(!stdin_open) fclose(cmd_file);
	fclose(tlog);
	exit(0);
      case readit:
	if(interact && stdin_open)
	  {
	    if(sscanf(cmd_token_ptr->piece[1],"%s",twiss_cmd_file)==EOF)
	      {
		fprintf(stderr,
			"? twiss: the 'read' command requires a filename.\n");
	      } else {
		stdin_open = FALSE;
		cmd_file = fopen(twiss_cmd_file,"r");
		if(cmd_file == NULL)
		  {
		    fprintf(stderr,"? twiss: could not open %s\n",
			    twiss_cmd_file);
		    cmd_file = stdin;
		    stdin_open = FALSE;
		  }
	      }
	  }
	break;
      case stop:
	if(!interact)
	  {
	    fclose(cmd_file);
	    fclose(tlog);
	    exit(0);
	  } else {
	    if(stdin_open)
	      {
		fclose(tlog);
		exit(0);
	      } else {
		fclose(cmd_file);
		cmd_file = stdin;
		stdin_open = TRUE;
	      }
	  }
	break;
      case topdrawer:
	write_twiss_top(&main_line);
	break;
      case twiss:
	nfootprint = 10;
	if(!periodic_soln && (tw_save != 0))reinit_twiss(&main_line);
	propagate_twiss(&main_line);
	if(!quiet_flag)write_twiss(&main_line);
	write_twiss_sds(&main_line);
	break;
      case football:
	set_print_football();
	if(!periodic_soln && (tw_save != 0))reinit_twiss(&main_line);
	propagate_football(&main_line);
	if(!quiet_flag)write_football(&main_line);
	write_twiss_sds(&main_line);
	break;
      case update:
	if(sscanf(cmd_token_ptr->piece[1],"%d",&i)==EOF)
	  {
	    fprintf(stderr,"? twiss: garbled update command.\n");
	    if(!interact)exit(1);
	  } else {
	    update_element(i,&main_line);
	  }
	break;
      case matrix:
	get_transfer_matrix(&main_line);
	printf("The transfer matrix for this beamline is:\n");
	print_matrix(stdout,main_line.transfer_matrix,6);
	break;
      case cmd_error:
	fprintf(tlog,"*** Unrecognized keyword `%s'\n",
		cmd_token_ptr->piece[0]);
	fprintf(stderr,"*** Unrecognized keyword `%s'\n",
		cmd_token_ptr->piece[0]);
	break;                
      }
    }
    if(stdin_open)
      {
	printf("twiss<\n");
	fflush(stdout);
      }
  }

  fclose(cmd_file);
  fclose(tlog);
  exit(0);
}
/****************************************************************************/

void initial_offset(twiss_data *line_ptr)
{
  int k, n, s;
  double temp;
  enum offset_type offset_dict;

  n = 1;
  while (n<token_count) {

    offset_dict = offset_error ;
    for (k=0; k<=DICT_SIZE; k++) {
      if (strcmp(cmd_token_ptr->piece[n], offset_names.word[k]) == 0) {
	offset_dict = offset_names.dict[k];
      }
    }

    switch(offset_dict){
    case s_off:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&line_ptr->s_offset);
      n += 2 ;
      break;
    case mu_x_off:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&temp);
      line_ptr->mu_x_offset = twopi * temp;
      n += 2 ;
      break;
    case mu_y_off:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&temp);
      line_ptr->mu_y_offset = twopi * temp;
      n += 2 ;
      break;
    case offset_error:
      fprintf(stderr,"twiss: illegal offset setting option '%s'\n",
	      cmd_token_ptr->piece[n]);
      if(!interact)exit(1);
    }
    if (s == EOF) {
      fprintf(stderr,"twiss: garbled offset initialization \n");
      if(!interact)exit(1);
    }
  }
  
}


/*----------------------------------------------------------------------*/
void invreinit_twiss(twiss_data *line_ptr)
{  /* Copy initial values of optic structure from twiss output file. */
  tw_save[0].pathlen	= line_ptr->init_optic.pathlen;
  tw_save[0].beta_x	= line_ptr->init_optic.beta_x;
  tw_save[0].alfa_x	= line_ptr->init_optic.alfa_x;
  tw_save[0].gamma_x	= line_ptr->init_optic.gamma_x;
  tw_save[0].beta_y	= line_ptr->init_optic.beta_y;
  tw_save[0].alfa_y	= line_ptr->init_optic.alfa_y;
  tw_save[0].gamma_y	= line_ptr->init_optic.gamma_y;
  tw_save[0].eta_x 	= line_ptr->init_optic.eta_x;
  tw_save[0].eta_xp	= line_ptr->init_optic.eta_xp;
  tw_save[0].eta_y 	= line_ptr->init_optic.eta_y;
  tw_save[0].eta_yp	= line_ptr->init_optic.eta_yp;
  tw_save[0].eta_z	= line_ptr->init_optic.eta_z;
  tw_save[0].eta_1	= line_ptr->init_optic.eta_1;
  tw_save[0].mu_x	= line_ptr->init_optic.mu_x;
  tw_save[0].mu_y	= line_ptr->init_optic.mu_y;
  tw_save[0].bm_xx	= line_ptr->init_optic.bm_xx;
  tw_save[0].bm_xxp	= line_ptr->init_optic.bm_xxp;
  tw_save[0].bm_xy	= line_ptr->init_optic.bm_xy;
  tw_save[0].bm_xyp	= line_ptr->init_optic.bm_xyp;
  tw_save[0].bm_xz	= line_ptr->init_optic.bm_xz;
  tw_save[0].bm_xu	= line_ptr->init_optic.bm_xu;
  tw_save[0].bm_xpxp	= line_ptr->init_optic.bm_xpxp;
  tw_save[0].bm_xpy	= line_ptr->init_optic.bm_xpy;
  tw_save[0].bm_xpyp	= line_ptr->init_optic.bm_xpyp;
  tw_save[0].bm_xpz	= line_ptr->init_optic.bm_xpz;
  tw_save[0].bm_xpu	= line_ptr->init_optic.bm_xpu;
  tw_save[0].bm_yy	= line_ptr->init_optic.bm_yy;
  tw_save[0].bm_yyp	= line_ptr->init_optic.bm_yyp;
  tw_save[0].bm_yz	= line_ptr->init_optic.bm_yz;
  tw_save[0].bm_yu	= line_ptr->init_optic.bm_yu;
  tw_save[0].bm_ypyp	= line_ptr->init_optic.bm_ypyp;
  tw_save[0].bm_ypz	= line_ptr->init_optic.bm_ypz;
  tw_save[0].bm_ypu	= line_ptr->init_optic.bm_ypu;
  tw_save[0].bm_zz	= line_ptr->init_optic.bm_zz;
  tw_save[0].bm_zu	= line_ptr->init_optic.bm_zu;
  tw_save[0].bm_uu	= line_ptr->init_optic.bm_uu;
}
/*************************************************************************/
void initial_twiss(twiss_data *line_ptr)
{
  optic *o_ptr = &line_ptr->init_optic;
  int k, n, s;
  enum twiss_type twiss_dict;
  n = 1;
  while (n<token_count) {

    twiss_dict = twiss_error ;
    for (k=0; k<=DICT_SIZE; k++) {
      if (strcmp(cmd_token_ptr->piece[n], twiss_names.word[k]) == 0) {
	twiss_dict = twiss_names.dict[k];
      }
    }
    
    switch(twiss_dict){
    case beta_x:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->beta_x);
      if (o_ptr->beta_x <= 0.0) {
	fprintf(stderr,"twiss: initial beta_x must be positive !\n");
	if(!interact)exit(1);
      }
      o_ptr->gamma_x = (1.0 + (o_ptr->alfa_x)*(o_ptr->alfa_x)) / 
	o_ptr->beta_x;
      n += 2 ;
      break;
    case beta_y:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->beta_y);
      if (o_ptr->beta_y <= 0.0) {
	fprintf(stderr,"twiss: initial beta_y must be positive !\n");
	if(!interact)exit(1);
      }
      o_ptr->gamma_y = (1.0 + (o_ptr->alfa_y)*(o_ptr->alfa_y)) / 
	o_ptr->beta_y;
      n += 2 ;
      break;
    case alfa_x:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->alfa_x);
      o_ptr->gamma_x = (1.0 + (o_ptr->alfa_x)*(o_ptr->alfa_x)) / 
	o_ptr->beta_x;
      n += 2 ;
      break;
    case alfa_y:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->alfa_y);
      o_ptr->gamma_y = (1.0 + (o_ptr->alfa_y)*(o_ptr->alfa_y)) / 
	o_ptr->beta_y;
      n += 2 ;
      break;
    case eta_x:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->eta_x);
      n += 2 ;
      break;
    case eta_y:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->eta_y);
      n += 2 ;
      break;
    case eta_xp:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->eta_xp);
      n += 2 ;
      break;
    case eta_yp:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->eta_yp);
      n += 2 ;
      break;

/* The following 21 elements are the upper triangle of the beam sigma
*  matrix.
*  These values are now independent from the initial Twiss parameters
*  as defined above.
*/
    case bm_xx:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_xx);
      if (o_ptr->bm_xx < 0.0) {
	fprintf(stderr,"twiss: initial bm_xx must be >=0.\n");
	if(!interact)exit(1);
      }
      n += 2 ;
      break;
    case bm_xxp:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_xxp);
      n += 2 ;
      break;
    case bm_xy:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_xy);
      n += 2 ;
      break;
    case bm_xyp:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_xyp);
      n += 2 ;
      break;
    case bm_xz:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_xz);
      n += 2 ;
      break;
    case bm_xu:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_xu);
      n += 2 ;
      break;
    case bm_xpxp:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_xpxp);
      if (o_ptr->bm_xpxp < 0.0) {
	fprintf(stderr,"twiss: initial bm_xpxp must be >=0.\n");
	if(!interact)exit(1);
      }
      n += 2 ;
      break;
    case bm_xpy:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_xpy);
      n += 2 ;
      break;
    case bm_xpyp:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_xpyp);
      n += 2 ;
      break;
    case bm_xpz:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_xpz);
      n += 2 ;
      break;
    case bm_xpu:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_xpu);
      n += 2 ;
      break;
    case bm_yy:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_yy);
      if (o_ptr->bm_yy < 0.0) {
	fprintf(stderr,"twiss: initial bm_yy must be >=0\n");
	if(!interact)exit(1);
      }
      n += 2 ;
      break;
    case bm_yyp:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_yyp);
      n += 2 ;
      break;
    case bm_yz:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_yz);
      n += 2 ;
      break;
    case bm_yu:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_yu);
      n += 2 ;
      break;
    case bm_ypyp:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_ypyp);
      if (o_ptr->bm_ypyp < 0.0) {
	fprintf(stderr,"twiss: initial bm_ypyp must be >=0.\n");
	if(!interact)exit(1);
      }
      n += 2 ;
      break;
    case bm_ypz:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_ypz);
      n += 2 ;
      break;
    case bm_ypu:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_ypu);
      n += 2 ;
      break;
    case bm_zz:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_zz);
      if (o_ptr->bm_zz < 0.0) {
	fprintf(stderr,"twiss: initial bm_zz must be >=0.\n");
	if(!interact)exit(1);
      }
      n += 2 ;
      break;
    case bm_zu:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_zu);
      n += 2 ;
      break;
    case bm_uu:
      s = sscanf(cmd_token_ptr->piece[n+1],"%lf",&o_ptr->bm_uu);
      if (o_ptr->bm_uu < 0.0) {
	fprintf(stderr,"twiss: initial bm_uu must be >=0.\n");
	if(!interact)exit(1);
      }
      n += 2 ;
      break;
    case twiss_error:
      fprintf(stderr,"twiss: illegal twiss setting option '%s'\n",
	      cmd_token_ptr->piece[n]);
      if(!interact)exit(1);
    }

    if (s == EOF) {
      fprintf(stderr,"twiss: garbled twiss initialization \n");
      if(!interact)exit(1);
    }
  }
  if(tw_save != 0)invreinit_twiss(line_ptr);

  line_count += 1;
}

/****************************************************************************/

void mark_print(twiss_data *line_ptr)
{
  flatin_data *f_ptr = line_ptr->flatin_ptr;
  int found, i, k, n;
  enum mark_type mark_dict;
  
  n = 1  ;
  while (n < token_count) {
    mark_dict = mark_named_element ;
    for (k=0; k<DICT_SIZE; k++)  {
      if( strcmp(cmd_token_ptr->piece[n], mark.word[k]) == 0 )  {
	mark_dict = mark.dict[k];
	break;
      }
    }
    
    switch(mark_dict)  {
    case all:
      for(i = 0; i < f_ptr->number_of_elements; i++) 
	line_ptr->magmat_ptr[i].repout = TRUE ;
      n = token_count; /* may as well exit immediately! */
      break;
    case mark_named_element:
      found = FALSE ;
      for(i = 0; i < f_ptr->number_of_elements && !found; i++) {
	if(strcmp(cmd_token_ptr->piece[n], f_ptr->element_ptr[i].name) == 0) {
	  found = TRUE ;
	  line_ptr->magmat_ptr[i].repout = TRUE ;
	}
      }
      if (!found) {
	fprintf(tlog,"*** Could not find an element named '%s' to mark\n",
		cmd_token_ptr->piece[n]);
      }
      ++n;
      break;      
    }
  }
}

/****************************************************************************/

array_of_tokens *parsef(char *line, int *t_count)
{
    /*This function parses a string, returning 't_count' valid tokens,
     * from 0 thru' (t_count-1) :  item.piece[t_count] is NOT valid
     */
  int counter, place, length;
  array_of_tokens item;
  
  place = 0;
  counter = 0;
  while (counter < MAX_TOKEN_COUNT) {
    place += strspn(&line[place],        " \t,&=%");
    if (sscanf(&line[place], "%s", item.piece[counter]) == EOF) break;    
    length = strcspn(item.piece[counter]," \t,&=%!");
    strncpy(&item.piece[counter][length],"\0",1);
    if (length == 0) break; /* rest of the line is a comment starting with '!' */
    place   += length;
    counter += 1;
  }
  *t_count = counter;
  
  return &item;
}

/****************************************************************************/

void periodic_solution(twiss_data *main_ptr, twiss_data *side_ptr)
{
  twiss_data *line_ptr;

  switch (token_count) {
  case 1:
    line_ptr = main_ptr;
    break;
  case 2:
    line_ptr = side_ptr;
    if (sscanf(cmd_token_ptr->piece[1],"%s",line_ptr->flat_file_name) == EOF) {
      fprintf(stderr,"twiss: periodic command garbled - flat_file_name?\n");
      if(!interact)exit(1);
    }
    alloc_twiss(line_ptr);
    create_transfer_matrices(line_ptr);
    break;
  default:
    fprintf(stderr,"twiss: periodic command is garbled - default\n");
    if(!interact)exit(1);
    break;
  }

  get_transfer_matrix(line_ptr);
  if (verbose) {
    fprintf(tlog,"\n \t \t total transfer matrix of the beamline \n");
    print_matrix(tlog,line_ptr->transfer_matrix,6);
  }

  periodic_twiss(line_ptr);

}

/****************************************************************************/

void write_gammat_header()
{

  printf("----------------------------------------------------------------\n");
  printf("name\t\t s     d(gt)/d(kL)   beta_x\teta_x\tmu_x\n");
  printf("----------------------------------------------------------------\n");
  line_count += 3;

}

/****************************************************************************/

void write_gammat_sensitivity(twiss_data *line_ptr)
{
  /* for an excellent reference, see T. Risselada, CERN PS/90-51 */

  optic   *o_ptr = line_ptr->optic_ptr;
  atom    *a_ptr = line_ptr->flatin_ptr->atom_ptr;
  element *e_ptr = line_ptr->flatin_ptr->element_ptr;
  int i, atom_count, count, tag;
  double common_factor, dgt_dkl;
  double s_off     = line_ptr->s_offset;
  double mu_x_off = line_ptr->mu_x_offset;

  if (line_ptr->gamma_t == 0) {
    fprintf(stderr,"twiss: gamma_t = 0 - MUST do a 'twiss' before a 'gammat_sens'\n");
    exit(0);
  }

  if (header) {
    printf("\n");
    line_count += 1;
    write_super_header(line_ptr);
  }

  count = 0;
  atom_count = line_ptr->flatin_ptr->number_of_atoms;
  common_factor =  0.5 * pow(line_ptr->gamma_t,(double)3.0) / 
    a_ptr[atom_count-1].s;

  for (i = 1; i <= atom_count; i++) {
    tag = a_ptr[i-1].element_index ;
    if (line_ptr->magmat_ptr[tag].repout) {
      if (header && (count++%HEADER_PERIOD == 0)) write_gammat_header();

      dgt_dkl = common_factor * pow(o_ptr[i].eta_x,(double)2.0); 

      printf("%-10s%11.3f%12.6f%10.4f%10.5f%10.6f\n", 
	     e_ptr[tag].name, 
	     s_off + a_ptr[i-1].s, 
	     dgt_dkl, 
	     o_ptr[i].beta_x, 
	     o_ptr[i].eta_x, 
	     (mu_x_off + o_ptr[i].mu_x) * twopinv);
      line_count += 1;
    }
  }

  if (header) {
    printf("----------------------------------------------------------------\n");
    line_count += 1;
  }

}

/****************************************************************************/

void write_super_header(twiss_data *line_ptr)
{
  
  printf("++++++++++++++++++++++++\n");
  printf("%slattice file: %s  \ndelta_p/p = %f\n",
	 ctime(&line_ptr->time_stamp),
	 line_ptr->flat_file_name,
	 line_ptr->delta);
  printf("++++++++++++++++++++++++\n");
  line_count += 6;
  }

/****************************************************************************/

void write_twiss(twiss_data *line_ptr)
{
  optic   *o_ptr = line_ptr->optic_ptr;
  optic   opt;
  atom    *a_ptr = line_ptr->flatin_ptr->atom_ptr;
  element *e_ptr = line_ptr->flatin_ptr->element_ptr;
  char buff[BUFF_MAX];
  int j, tag, atom_count;
  double s_off = line_ptr->s_offset;
  double mu_x_off = line_ptr->mu_x_offset;
  double mu_y_off = line_ptr->mu_y_offset;

  if (header) write_super_header(line_ptr);
  if (header) write_twiss_header();

  atom_count = line_ptr->flatin_ptr->number_of_atoms;
  for (j = 0; j <= atom_count; j++) {
    opt = o_ptr[j];
    if(j<=0)
      {
	tag = 0;
      } else {
	tag = a_ptr[j-1].element_index ;
      }
    if (j == 0 || j == atom_count || line_ptr->magmat_ptr[tag].repout) {
      if (header && (line_count%HEADER_PERIOD == 0)) write_twiss_header();
      if (tev_config_format) {
	sprintf(buff,"%9.3f%9.3f%9.3f%9.3f%9.3f%9.3f%9.3f%9.3f%9.4f%9.4f",
		opt.mu_x, opt.mu_y,
		opt.beta_x, opt.beta_y, opt.alfa_x, opt.alfa_y, 
		opt.eta_x,  opt.eta_y,  opt.eta_xp, opt.eta_yp);
	if (j == 0) {
	  printf("%9.3f%s\n", s_off, buff);
	  line_count += 1;
	} else {
	  printf("%9.3f%s                    %-10s\n", 
		 a_ptr[j-1].s + s_off, buff, e_ptr[tag].name);
	  line_count += 1;
	}
      } else {
	sprintf(buff,"%10.4f%9.4f%10.4f%9.4f%10.5f%9.4f%10.5f%9.4f%10.6f%10.6f",
		opt.beta_x, opt.alfa_x, opt.beta_y, opt.alfa_y, 
		opt.eta_x,  opt.eta_xp, opt.eta_y,  opt.eta_yp,
		(opt.mu_x + mu_x_off)*twopinv, 
		(opt.mu_y + mu_y_off)*twopinv);
	if (j == 0) {
	  printf("BEGIN     %11.3f%s\n", s_off, buff);
	  line_count += 1;
	} else {
	  printf("%-10s%11.3f%s\n",e_ptr[tag].name,s_off + a_ptr[j-1].s, buff);
	  line_count += 1;
	}
      }
    }

  }

  if (header) write_twiss_summary(line_ptr);

}

/****************************************************************************/

void write_twiss_header(void)
{

  printf("------------------------------------------------------------");
  printf("------------------------------------------------------------\n");
  line_count += 1;
  if (tev_config_format) {
    printf("     s        fix      fiy      betx    bety     alfx     alfy     etax     etay     etaxp   etayp     xco       yco    name\n");
    line_count += 1;
  } else {
    printf("name\t\t s\tbeta_x\t  alfa_x   beta_y    alfa_y");
    printf("    eta_x    eta_xp\t  eta_y\t   eta_yp  mu_x/2pi  mu_y/2pi\n");
    line_count += 1;
  }
  printf("------------------------------------------------------------");
  printf("------------------------------------------------------------\n");
  line_count += 1;
}

/****************************************************************************/

void write_twiss_summary(twiss_data *line_ptr)
{
  flatin_data  *f_ptr = line_ptr->flatin_ptr;
  int i, linelength; 

  linelength = 10+11+nfootprint*10;
  for (i=0;i<linelength;i++) printf("-");
  printf("\n");
  line_count += 1;

  if (periodic_soln){
    printf("this is the PERIODIC SOLUTION for this line\n");
    line_count += 1;
  }

  printf("total length = %-.14f [m]\n",
	 f_ptr->atom_ptr[f_ptr->number_of_atoms - 1].s);

  printf("beta_x_max = %-.3f [m]   beta_y_max = %-.3f [m]\n",
	 line_ptr->beta_x_max, line_ptr->beta_y_max);
  printf("eta_x_min  = %-.3f [m]   eta_x_max  = %-.3f [m]   ",
	 line_ptr->eta_x_min, line_ptr->eta_x_max);
  printf("eta_y_min  = %-.3f [m]   eta_y_max  = %-.3f [m]\n",
	 line_ptr->eta_y_min, line_ptr->eta_y_max);
  line_count += 3;

  if (periodic_soln){
    printf("gamma_t    = %-.5f\n", line_ptr->gamma_t);
    line_count += 1;
  }
  for (i=0;i<linelength;i++) printf("-");
  printf("\n");
  line_count += 1;

/*
SP May 4 '93    This was a hack done during gamma-t jump design ???
  opt = line_ptr->optic_ptr[line_ptr->flatin_ptr->number_of_atoms];
  printf("%8.4f%8.4f%8.4f%8.2f%8.2f%8.3f%8.3f\n", 
	 line_ptr->gamma_t - 22.80861,
	 opt.mu_x*twopinv - 28.0, 
	 opt.mu_y*twopinv - 29.0,
	 line_ptr->beta_x_max,
	 line_ptr->beta_y_max,
	 line_ptr->eta_x_max,
	 line_ptr->eta_x_min);
  line_count += 1;
*/
}
/***************************************************************************/
void write_twiss_sds(twiss_data *line_ptr)
{
  optic   *o_ptr = line_ptr->optic_ptr;
  optic   opt;
  atom    *a_ptr = line_ptr->flatin_ptr->atom_ptr;
  int j, count, tag, atom_count;
  sds_handle sds;
  sds_code tc;
  optic *tw;
  extern int *latindices;
  char tmpfilnam[120];

  twiss_ifile_name *tin;
  sds_code tinc;

  count = 0;
  atom_count = line_ptr->flatin_ptr->number_of_atoms;

  strcpy(tmpfilnam,"Twiss.");
  strcat(tmpfilnam,line_ptr->flat_file_name);

  /* sds_init() is now called in "main(...)" */

  if(tw_save == 0)
    {
      if(shared_memory)
	{
	  sds = sds_access(tmpfilnam,SDS_SHARED_MEM,SDS_WRITE);
	  if(sds <=0)
	    {
	      sds_handle sds_temp = sds_new(tmpfilnam);
	      tinc = sds_define_structure(sds_temp,twiss_input_tl,
					  twiss_input_nm);
	      sds_declare_structure(sds_temp,SDS_ALLOCATE,"flat_file",1,tinc);
	      tc = sds_define_structure(sds_temp,tw_tl,tw_nm);
	      sds_declare_structure(sds_temp,SDS_ALLOCATE,"optic",
				 atom_count + 1, tc);
	      sds = sds_ass(sds_temp,tmpfilnam,SDS_SHARED_MEM);
	      if(sds<=0)
		{
		  sds_perror("write_twiss_sds: ");
		  if(!interact)exit(1);
		}
	      sds_destroy(sds_temp);
	    }
	}
      else
	{
	  sds = sds_new(tmpfilnam);
	  tinc = sds_define_structure(sds,twiss_input_tl,twiss_input_nm);
	  sds_declare_structure(sds,SDS_ALLOCATE,"flat_file",1,tinc);
	  tc = sds_define_structure(sds,tw_tl,tw_nm);
	  sds_declare_structure(sds,SDS_ALLOCATE,"optic",atom_count + 1, tc);
	  sds_ass(sds,tmpfilnam,SDS_PROC_MEM);
	}
    }

  if( (tw_save == 0) )
    {
      tin = (twiss_ifile_name *)sds_obname2ptr(sds,"flat_file");
      strcpy(tin->filename,line_ptr->flat_file_name);
      tw_save = (optic *)sds_obname2ptr(sds,"optic");
    }

  tw = tw_save;
  for (j = 0; j <= atom_count; j++) {
    opt = o_ptr[j];
    if (j > 0)
      {
	tag = a_ptr[j-1].element_index ;
      }
    else
      {
	opt.lattice_index = 0; /* Start the list; not really an element */
	opt.element_index = 0;
      }
    if (latindices)
      tw->lattice_index = latindices[j];
    else
      tw->lattice_index = 0;
    tw->element_index = opt.element_index;
    tw->type_index = opt.type_index;
    tw->pathlen = opt.pathlen;
    tw->beta_x = opt.beta_x; 
    tw->alfa_x = opt.alfa_x; 
    tw->gamma_x = opt.gamma_x;
    tw->beta_y = opt.beta_y; 
    tw->alfa_y = opt.alfa_y; 
    tw->gamma_y = opt.gamma_y;
    tw->eta_x = opt.eta_x;  
    tw->eta_xp = opt.eta_xp; 
    tw->eta_y = opt.eta_y;  
    tw->eta_yp = opt.eta_yp;
    tw->eta_z = opt.eta_z;
    tw->eta_1 = opt.eta_1;
    tw->mu_x = opt.mu_x;
    tw->mu_y = opt.mu_y;
    tw->bm_xx = opt.bm_xx;
    tw->bm_xxp = opt.bm_xxp;
    tw->bm_xy = opt.bm_xy;
    tw->bm_xyp = opt.bm_xyp;
    tw->bm_xz = opt.bm_xz;
    tw->bm_xu = opt.bm_xu;
    tw->bm_xpxp = opt.bm_xpxp;
    tw->bm_xpy = opt.bm_xpy;
    tw->bm_xpyp = opt.bm_xpyp;
    tw->bm_xpz = opt.bm_xpz;
    tw->bm_xpu = opt.bm_xpu;
    tw->bm_yy = opt.bm_yy;
    tw->bm_yyp = opt.bm_yyp;
    tw->bm_yz = opt.bm_yz;
    tw->bm_yu = opt.bm_yu;
    tw->bm_ypyp = opt.bm_ypyp;
    tw->bm_ypz = opt.bm_ypz;
    tw->bm_ypu = opt.bm_ypu;
    tw->bm_zz = opt.bm_zz;
    tw->bm_zu = opt.bm_zu;
    tw->bm_uu = opt.bm_uu;
    tw++;
  }
  if(!shared_memory)
    {
      sds_ass(sds,tmpfilnam,SDS_FILE);
    }
}
/*************************************************************************/
void set_print_football(void)
/* Setup what variables to print in output stream. */
{
  int k, n;

  n = 1;
  nfootprint = 0;
  if(token_count<=1) {  /* Use default values */
    nfootprint = 10;
    footprint[1] = lbl_beta_x;
    footprint[2] = lbl_alfa_x;
    footprint[3] = lbl_beta_y;
    footprint[4] = lbl_alfa_y;
    footprint[5] = lbl_eta_x;
    footprint[6] = lbl_eta_xp;
    footprint[7] = lbl_eta_y;
    footprint[8] = lbl_eta_yp;
    footprint[9] = lbl_mu_x;
    footprint[10]= lbl_mu_y;
  } else {
    for (n=1; n<token_count;n++){
      for (k=0; k<lbl_error; k++) {
	if (strcmp(cmd_token_ptr->piece[n], label_footprint.word[k]) == 0) {
	  nfootprint += 1;
	  footprint[nfootprint] = label_footprint.dict[k];
	  break;
	} else {
	  if(k == lbl_error){
	    printf("TWISS doesn't print:%s\n",cmd_token_ptr->piece[n]);
	    line_count += 1;
	  }
	}
      }
    }
  }
}
/****************************************************************************/

void write_football(twiss_data *line_ptr)
{
  optic   *o_ptr = line_ptr->optic_ptr;
  optic   opt;
  atom    *a_ptr = line_ptr->flatin_ptr->atom_ptr;
  element *e_ptr = line_ptr->flatin_ptr->element_ptr;
  char buff[BUFF_MAX];
  int j, tag, atom_count;
  int i, k;
  double s_off = line_ptr->s_offset;
  double value;

  if (header) write_super_header(line_ptr);

  if(header){
    write_football_header();
    line_count -= 3;
  }
  atom_count = line_ptr->flatin_ptr->number_of_atoms;
  for (j = 0; j <= atom_count; j++) {
    opt = o_ptr[j];
    tag = a_ptr[j-1].element_index ;
    if (j == 0 || j == atom_count || line_ptr->magmat_ptr[tag].repout) {
      if (header && (line_count >= HEADER_PERIOD) ){
	line_count = 0;
	write_football_header();
      }

/*
      if (tev_config_format) {
	sprintf(buff,"%9.3f%9.3f%9.3f%9.3f%9.3f%9.3f%9.3f%9.3f%9.4f%9.4f",
		opt.mu_x, opt.mu_y,
		opt.beta_x, opt.beta_y, opt.alfa_x, opt.alfa_y, 
		opt.eta_x,  opt.eta_y,  opt.eta_xp, opt.eta_yp);
	if (j == 0) {
	  printf("%9.3f%s\n", s_off, buff);
	  line_count += 1;
	} else {
	  printf("%9.3f%s                    %-10s\n", 
		 a_ptr[j-1].s + s_off, buff, e_ptr[tag].name);
	  line_count += 1;
	}
      } else {
*/

      if(j == 0){
	sprintf(buff,"%-10s%11.3f","BEGIN",s_off+a_ptr[j-1].s);
      } else {
	sprintf(buff,"%-10s%11.3f",e_ptr[tag].name,s_off+a_ptr[j-1].s);
      }
      i = 21;
      for(k=1;k<=nfootprint;k++){
	switch(label_footprint.dict[footprint[k]]){
	case lbl_beta_x:
	  value = opt.beta_x;
	  break;
	case lbl_alfa_x:
	  value = opt.alfa_x;
	  break;
	case lbl_gamma_x:
	  value = opt.gamma_x;
	  break;
	case lbl_beta_y:
	  value = opt.beta_y;
	  break;
	case lbl_alfa_y:
	  value = opt.alfa_y;
	  break;
	case lbl_gamma_y:
	  value = opt.gamma_y;
	  break;
	case lbl_eta_x:
	  value = opt.eta_x;
	  break;
	case lbl_eta_xp:
	  value = opt.eta_xp;
	  break;
	case lbl_eta_y:
	  value = opt.eta_y;
	  break;
	case lbl_eta_yp:
	  value = opt.eta_yp;
	  break;
	case lbl_eta_z:
	  value = opt.eta_z;
	  break;
	case lbl_eta_1:
	  value = opt.eta_1;
	  break;
	case lbl_mu_x:
	  value = opt.mu_x*twopinv;
	  break;
	case lbl_mu_y:
	  value = opt.mu_y*twopinv;
	  break;
	case lbl_xx:
	  value = opt.bm_xx;
	  break;
	case lbl_xxp:
	  value = opt.bm_xxp;
	  break;
	case lbl_xy:
	  value = opt.bm_xy;
	  break;
	case lbl_xyp:
	  value = opt.bm_xyp;
	  break;
	case lbl_xz:
	  value = opt.bm_xz;
	  break;
	case lbl_xu:
	  value = opt.bm_xu;
	  break;
	case lbl_xpxp:
	  value = opt.bm_xpxp;
	  break;
	case lbl_xpy:
	  value = opt.bm_xpy;
	  break;
	case lbl_xpyp:
	  value = opt.bm_xpyp;
	  break;
	case lbl_xpz:
	  value = opt.bm_xpz;
	  break;
	case lbl_xpu:
	  value = opt.bm_xpu;
	  break;
	case lbl_yy:
	  value = opt.bm_yy;
	  break;
	case lbl_yyp:
	  value = opt.bm_yyp;
	  break;
	case lbl_yz:
	  value = opt.bm_yz;
	  break;
	case lbl_yu:
	  value = opt.bm_yu;
	  break;
	case lbl_ypyp:
	  value = opt.bm_ypyp;
	  break;
	case lbl_ypz:
	  value = opt.bm_ypz;
	  break;
	case lbl_ypu:
	  value = opt.bm_ypu;
	  break;
	case lbl_zz:
	  value = opt.bm_zz;
	  break;
	case lbl_zu:
	  value = opt.bm_zu;
	  break;
	case lbl_uu:
	  value = opt.bm_uu;
	  break;
	default:
	  fprintf(stderr,"??? You should not get this message !!\n");
	  fprintf(stderr,"Unknown parameter name in `write_football'.\n");
	  fprintf(stderr,"  label_footprint[k]=%d\n",label_footprint.dict[k]);
	}
	sprintf(&buff[i],"%10.4f",value);
	i += 10;
      }
      printf("%s\n",buff);
      line_count += 1;
    }
  }
  if (header) write_twiss_summary(line_ptr);
}

/****************************************************************************/

void write_football_header(void)
{
  char buff[BUFF_MAX];
  int i, j, k;
  int linelength;
  char name_of_parameter[9];

  if(10+11+nfootprint*10 > BUFF_MAX){
    printf("Warning: too many functions requested in 'football' command\n");
    printf("   Truncating printout. BUFF_MAX=%d\n",BUFF_MAX);
    line_count += 2;
    nfootprint = (BUFF_MAX+1-10-11)/10;
  }
  linelength = 10+11+nfootprint*10;

  sprintf(buff,"%-10s%8s   ","name","s");
  i=21;
  for(j=1;j<=nfootprint;j++){
    k=footprint[j];
    if(k==lbl_mu_x || k==lbl_mu_y){
      strcpy(name_of_parameter,label_footprint.word[footprint[j]]);
      sprintf(&buff[i],"%10s",strcat(name_of_parameter,"/2pi"));
    } else {
      sprintf(&buff[i],"%10s",label_footprint.word[footprint[j]]);
    }
    i += 10;
  }

  for (i=0;i<linelength;i++) printf("-");
  printf("\n%s\n",buff);
  for (i=0;i<linelength;i++) printf("-");
  printf("\n");
  line_count += 3;
}

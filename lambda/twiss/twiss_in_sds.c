/* $Header: /usr/local/lambda/twiss/RCS/twiss_in_sds.c,v 1.4 1994/06/14 12:50:41 mackay Exp $ */

#include <stdio.h>
#include <stdlib.h>
#include <Twiss_data.h>

int twiss_in_sds(sds_handle sds, Twiss_data *odp)
{
/* Reads in the Twiss.... file. */

  sds_handle ob_index;

  odp->flat_file_ptr = (twiss_ifile_name *) sds_obname2ptr(sds,"flat_file");
  if(!odp->flat_file_ptr)
    {
      fprintf(stderr,"? twiss_in: Not reading an SDS file\n");
      return 1;
    }

  odp->optic_ptr = (optic *) sds_obname2ptr(sds,"optic");
  ob_index = sds_name2ind(sds,"optic");
  odp->number_of_optic = sds_array_size(sds,ob_index);

  return 0;
}

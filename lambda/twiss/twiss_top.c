/* $Header: /usr2/local/lambda/twiss/RCS/twiss_top.c,v 1.3 1994/02/22 20:46:24 mackay Exp $ */

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include <flatin.h>
#include <twiss.h>
#include <twiss_func_extern.h>
#include <twiss_top.h>
#include <lattice_defines.h>

/****************************************************************************/
void write_twiss_top(twiss_data *line_ptr)
{
  FILE *top = fopen("twiss.top", "w");
  atom    *a_ptr = line_ptr->flatin_ptr->atom_ptr;
  element *e_ptr = line_ptr->flatin_ptr->element_ptr;
  flatin_data    *f_ptr = line_ptr->flatin_ptr;
  optic   *o_ptr = line_ptr->optic_ptr;
  char t_stamp[BUFF_MAX];
  int count, i, j, print;
  int atom_count = f_ptr->number_of_atoms;
  double down, up, left, right, s_max, s_old, y_min, y_max;
  double bend_half_height, new_y_max, new_w_y_max, quad_factor;

 
/* ------------------------------------------------------------------------- */
  
  s_max = a_ptr[atom_count-1].s;

  if (line_ptr->beta_x_max > line_ptr->beta_y_max) {
    y_max = sqrt(line_ptr->beta_x_max);
  } else {
    y_max = sqrt(line_ptr->beta_y_max);
  }
  if (line_ptr->eta_x_max > y_max) y_max = line_ptr->eta_x_max;
  if (line_ptr->eta_y_max > y_max) y_max = line_ptr->eta_y_max;
  /* this gambles that sqrt(beta_max) is approximately eta_max */
  y_max = ceil(y_max*(1.0+TOP_SPACE));
  if (fmod(y_max,2.0) != 0.0 ) y_max += 1.0;

  y_min = 0.0;
  if (line_ptr->eta_x_min < y_min) y_min = line_ptr->eta_x_min;
  if (line_ptr->eta_y_min < y_min) y_min = line_ptr->eta_y_min;
  y_min = floor(y_min);
  /*  if (fmod(y_min,2.0) != 0.0) y_min -= 1.0; */

  count = strcspn(ctime(&line_ptr->time_stamp),"\n");
  strncpy(t_stamp,ctime(&line_ptr->time_stamp),count);
  t_stamp[count] = 0;

  fprintf(top,"SET FONT DUPLEX\n");
  fprintf(top,"SET SIZE 13.2 BY 10.2\n");
  fprintf(top,"SET WINDOW X FROM 1.0 TO 12.3 \n");
  fprintf(top,"SET LIMITS X FROM 0.0 TO %f \n", s_max);
  fprintf(top,"SET WINDOW Y FROM %f TO %f\n", W_Y_MIN, W_Y_MAX);
  fprintf(top,"SET LIMITS Y FROM %f TO %f \n", y_min, y_max);
  fprintf(top,"TITLE 0.15 4.5 ANGLE 90 'B0X121/23,   B0Y121/23  (M21/23)'\n");
  fprintf(top,"CASE                    'GXLXX   X    GXLXX   X   LX   X '\n");
  fprintf(top,"TITLE 12.7 4.7 ANGLE 90 'H0X1,   H0Y1   (M) '\n");
  fprintf(top,"CASE                    'GXLX    GXLX    L  '\n");
  fprintf(top,"TITLE  5.5 0.5 'Path Length  (m)' \n");
  fprintf(top,"TITLE 12.5 %f '%3.0f' \n", W_Y_MIN, y_min);
  fprintf(top,"TITLE 12.5 %f '%3.0f' \n", W_Y_MAX, y_max );
  fprintf(top,"TITLE  5.0 9.9 SIZE 2 'BEAMLINE = %s'\n",
	  line_ptr->flat_file_name);
  fprintf(top,"TITLE 1.0 0.2 SIZE 1.5 '%s'\n", t_stamp);
  fprintf(top,"SET TICKS TOP OFF \n");

/* ------------------------------------------------------------------------- */

  s_old = 0.0;
  fprintf(top,"%.6f %.6f\n", s_old, sqrt(o_ptr[0].beta_x));
  count = 0;
  for (i = 1; i <= atom_count; i++) {
    if (a_ptr[i-1].s != s_old) {
      s_old = a_ptr[i-1].s;
      fprintf(top,"%.6f %.6f\n", s_old, sqrt(o_ptr[i].beta_x));
      if (++count > TOP_BUFF_MAX) {
	fprintf(top,"JOIN 1\n");
	fprintf(top,"%.6f %.6f\n", s_old, sqrt(o_ptr[i].beta_x));
	count = 0;
      }
    }
  }
  fprintf(top,"JOIN 1\n");

/* ------------------------------------------------------------------------- */

  s_old = 0.0;
  fprintf(top,"%.6f %.6f\n", 0.0, sqrt(o_ptr[0].beta_y));
  count = 0;
  for (i = 1; i <= atom_count; i++) {
    if (a_ptr[i-1].s != s_old) {
      fprintf(top,"%.6f %.6f\n", a_ptr[i-1].s, sqrt(o_ptr[i].beta_y));
      if (++count > TOP_BUFF_MAX) {
	fprintf(top,"JOIN 1\n");
	fprintf(top,"%.6f %.6f\n", a_ptr[i-1].s, sqrt(o_ptr[i].beta_y));
	count = 0;
      }
    }
  }
  fprintf(top,"JOIN 1\n");
  
  /* ---------------------------------------------------------------------- */
  
  s_old = 0.0;
  fprintf(top,"%.6f %.6f\n", 0.0, o_ptr[0].eta_x);
  count = 0;
  for (i = 1; i <= atom_count; i++) {
    if (a_ptr[i-1].s != s_old) {
      fprintf(top,"%.6f %.6f\n", a_ptr[i-1].s, o_ptr[i].eta_x);
      if (++count > TOP_BUFF_MAX) {
	fprintf(top,"JOIN 1\n");
	fprintf(top,"%.6f %.6f\n", a_ptr[i-1].s, o_ptr[i].eta_x);
	count = 0;
      }
    }
  }
  fprintf(top,"JOIN 1\n");
  
  /* ---------------------------------------------------------------------- */
  
  s_old = 0.0;
  fprintf(top,"%.6f %.6f\n", 0.0, o_ptr[0].eta_y);
  count = 0;
  for (i = 1; i <= atom_count; i++) {
    if (a_ptr[i-1].s != s_old) {
      fprintf(top,"%.6f %.6f\n", a_ptr[i-1].s, o_ptr[i].eta_y);
      if (++count > TOP_BUFF_MAX) {
	fprintf(top,"JOIN 1\n");
	fprintf(top,"%.6f %.6f\n", a_ptr[i-1].s, o_ptr[i].eta_y);
	count = 0;
      }
    }
  }
  fprintf(top,"JOIN 1\n");

  /* ----------------------------------------------------------------------- */

  if (line_ptr->quad_strength_max != 0.0) {
    quad_factor    = 0.5 * TOP_SPACE * (y_max - y_min) / 
      line_ptr->quad_strength_max;
  }
  bend_half_height = 0.5 * BEND_SPACE * (y_max - y_min);

  new_w_y_max = W_Y_MAX + 0.5 * TOP_SPACE * (W_Y_MAX - W_Y_MIN);
  new_y_max   = y_max   + 0.5 * TOP_SPACE * (y_max - y_min);

  fprintf(top,"SET AXES OFF\n");
  fprintf(top,"SET WINDOW Y FROM %f TO %f \n", W_Y_MIN, new_w_y_max);
  fprintf(top,"SET LIMITS Y FROM %f TO %f \n", y_min, new_y_max);

  left = 0.0;  /* if  i == 1 */
  for (i = 1; i <= atom_count; i++) {
    print = TRUE;
    j    = a_ptr[i-1].element_index;
    switch (e_ptr[j].type_index) {
    case ATTR_QUADRUPOLE:
      down = y_max;
      up   = y_max + quad_factor * e_ptr[j].strength;
      break;
    case ATTR_RBEND:
      down = y_max - bend_half_height;
      up   = y_max + bend_half_height;
      break;
    case ATTR_SBEND:
      down = y_max - bend_half_height;
      up   = y_max + bend_half_height;
      break;
    default:
      print = FALSE;
      break;
    }

    if (print) {
      if (i > 1) left = a_ptr[i-2].s;
      right = a_ptr[i-1].s;
      fprintf(top,"%.6f %.6f\n", left,  down);
      fprintf(top,"%.6f %.6f\n", left,  up);
      fprintf(top,"%.6f %.6f\n", right, up);
      fprintf(top,"%.6f %.6f\n", right, down);
      fprintf(top,"%.6f %.6f\n", left,  down);
      fprintf(top,"JOIN 1\n");
    }
  }

/* ------------------------------------------------------------------------- */

  fclose(top);
}

/****************************************************************************/

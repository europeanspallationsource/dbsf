/* $Header: /rap/lambda/twiss/RCS/dump_twiss.c,v 1.3 1994/09/09 23:26:15 mackay Exp $ */
/* A program to dump the values of a Twiss SDS file for a given element number
** into an ASCII file */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include <twiss.h>
#include <Twiss_data.h>

Twiss_data td;
sds_handle sds;
FILE *tfile;

int ielt;

/*========================*/
int process_cmd(int argc, char **argv)
{
  int i;

  if(argc<3 || argc>4)
    {
      fprintf(stderr,"usage: %s [-s] <Twiss> <#>\n",argv[0]);
      fprintf(stderr,
	      "\twhere\t<Twiss> is the name of the SDS Twiss file, and\n");
      fprintf(stderr,"\t\t<#> is the number of the 'optic' element to dump.\n");
      fprintf(stderr,
	      "\t\t-s indicates that the file is in shared memory.\n");
      return 1;
    }

  if(strcmp("-s",argv[1])==0)
    {
      i = 2;
      sds = sds_access(argv[i],SDS_SHARED_MEM,SDS_READ);
    } else {
      i = 1;
      sds = sds_access(argv[i],SDS_FILE,SDS_READ);
    }

  if(sds<=0)
    {
      fprintf(stderr,"? Could not sds_access(%s)\n",argv[1]);
      return 1;
    }
  if(twiss_in_sds(sds,&td))
    {
      fprintf(stderr,"? Problem with twiss_in_sds()\n");
      return 1;
    }
  ielt = atoi(argv[i+1]);
  printf("Dump of optic[%d] from\nSDS Twiss file:%s\n",ielt,argv[i]);
  return 0;
}
/*========================*/
void dumpelt(int i)
{
  printf(" Twiss from flat SDS file: %s\n\n",td.flat_file_ptr);

  printf("lattice_index:\t%d\n",td.optic_ptr[i].lattice_index);
  printf("element_index:\t%d\n",td.optic_ptr[i].element_index);
  printf("type_index:\t%d\n",td.optic_ptr[i].type_index);
  printf("pathlen:\t%13.6f\n",td.optic_ptr[i].pathlen);
  printf("beta_x:\t\t%12.6f\n",td.optic_ptr[i].beta_x);
  printf("alfa_x:\t\t%12.6f\n",td.optic_ptr[i].alfa_x);
  printf("alfa_x:\t\t%12.6f\n",td.optic_ptr[i].alfa_x);
  printf("gamma_x:\t%12.6f\n",td.optic_ptr[i].gamma_x);
  printf("beta_y:\t\t%12.6f\n",td.optic_ptr[i].beta_y);
  printf("alfa_y:\t\t%12.6f\n",td.optic_ptr[i].alfa_y);
  printf("gamma_y:\t%12.6f\n",td.optic_ptr[i].gamma_y);
  printf("eta_x:\t\t%12.6f\n",td.optic_ptr[i].eta_x);
  printf("eta_xp:\t\t%12.6f\n",td.optic_ptr[i].eta_xp);
  printf("eta_y:\t\t%12.6f\n",td.optic_ptr[i].eta_y);
  printf("eta_yp:\t\t%12.6f\n",td.optic_ptr[i].eta_yp);
  printf("eta_z:\t\t%12.6f\n",td.optic_ptr[i].eta_z);
  printf("eta_1:\t\t%12.6f\n",td.optic_ptr[i].eta_1);
  printf("mu_x:\t\t%12.6f\n",td.optic_ptr[i].mu_x);
  printf("mu_y:\t\t%12.6f\n",td.optic_ptr[i].mu_y);
  printf("bm_xx:\t\t%18.12f\n",td.optic_ptr[i].bm_xx);
  printf("bm_xxp:\t\t%18.12f\n",td.optic_ptr[i].bm_xxp);
  printf("bm_xy:\t\t%18.12f\n",td.optic_ptr[i].bm_xy);
  printf("bm_xyp:\t\t%18.12f\n",td.optic_ptr[i].bm_xyp);
  printf("bm_xz:\t\t%18.12f\n",td.optic_ptr[i].bm_xz);
  printf("bm_xu:\t\t%18.12f\n",td.optic_ptr[i].bm_xu);
  printf("bm_xpxp:\t%18.12f\n",td.optic_ptr[i].bm_xpxp);
  printf("bm_xpy:\t\t%18.12f\n",td.optic_ptr[i].bm_xpy);
  printf("bm_xpyp:\t%18.12f\n",td.optic_ptr[i].bm_xpyp);
  printf("bm_xpz:\t\t%18.12f\n",td.optic_ptr[i].bm_xpz);
  printf("bm_xpu:\t\t%18.12f\n",td.optic_ptr[i].bm_xpu);
  printf("bm_yy:\t\t%18.12f\n",td.optic_ptr[i].bm_yy);
  printf("bm_yyp:\t\t%18.12f\n",td.optic_ptr[i].bm_yyp);
  printf("bm_yz:\t\t%18.12f\n",td.optic_ptr[i].bm_yz);
  printf("bm_yu:\t\t%18.12f\n",td.optic_ptr[i].bm_yu);
  printf("bm_ypyp:\t%18.12f\n",td.optic_ptr[i].bm_ypyp);
  printf("bm_ypz:\t\t%18.12f\n",td.optic_ptr[i].bm_ypz);
  printf("bm_ypu:\t\t%18.12f\n",td.optic_ptr[i].bm_ypu);
  printf("bm_zz:\t\t%18.12f\n",td.optic_ptr[i].bm_zz);
  printf("bm_zu:\t\t%18.12f\n",td.optic_ptr[i].bm_zu);
  printf("bm_uu:\t\t%18.12f\n",td.optic_ptr[i].bm_uu);
}
/*========================*/
int main(int argc, char **argv)
{
  sds_init();

  if(process_cmd(argc, argv))return 1;

  dumpelt(ielt);
  return 0;
}

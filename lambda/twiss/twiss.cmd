!Twiss command file for the RHIC injection lines.

! Initial values for Au+79 ions
initial  xx     0.000007324389
initial  xxp    0.000000747739
initial  xy     0.000000000000
initial  xyp    0.000000000000
initial  xz     0.000000000000
initial  xu    -0.000001560600
initial  xpxp   0.000000080698
initial  xpy    0.000000000000
initial  xpyp   0.000000000000
initial  xpz    0.000000000000
initial  xpu   -0.000000135252
initial  yy     0.000000863805
initial  yyp   -0.000000112959
initial  yz     0.000000000000
initial  yu     0.000000000000
initial  ypyp   0.000000035217
initial  ypz    0.000000000000
initial  ypu    0.000000000000
initial  zz     1.488400000000
initial  zu     0.000000000000
initial  uu     0.000001040400
initial  beta_x 37.500  alfa_x -4.100
initial  beta_y  6.500  alfa_y  0.850
initial  eta_x  -1.500  eta_xp -0.130

print all
football
stop

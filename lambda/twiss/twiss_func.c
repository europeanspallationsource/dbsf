/* $Header: /rap/lambda/twiss/RCS/twiss_func.c,v 1.19 1996/06/27 20:39:29 mackay Exp $ */

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "flatin.h"
#include "twiss.h"
#include "lattice_defines.h"
#include "Sds/sdsgen.h"

#include "twiss_func_extern.h"
#include "lambda_matrix.h"

int shared_memory = FALSE;

#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN extern
#endif

#ifndef NOSYBASE
#include <sybfront.h>
#include <sybdb.h>
#include <syb_tables.h>
#include "tcelement.h"
#include <Dbapps/dbapp_protos.h>

int dbflag = 0;
#endif

#undef EXTERN

extern int interact;

int tilt_err_quad = 0;
int tilt_err_sext = 0;

/****************************************************************************/

void alloc_twiss(twiss_data *line_ptr)
{
  flatin_data *f_ptr;
  magmat      *m_ptr;
  sds_handle  flat_sds;
  int i;

  /* first, free memory, if any has been allocated ................*/

  if (line_ptr->magmat_ptr != NULL) {
    for (i = 0; i < line_ptr->flatin_ptr->number_of_elements; i++)
      free2(line_ptr->magmat_ptr[i].matrix);
    free(line_ptr->magmat_ptr);
  }

  if (line_ptr->optic_ptr != NULL) free(line_ptr->optic_ptr);

  if (line_ptr->clorb_ptr != NULL) free(line_ptr->clorb_ptr);

  /* ............ then read new flatin data .....................*/

  sds_init();
  if(shared_memory)
    {
      flat_sds = sds_access(line_ptr->flat_file_name,SDS_SHARED_MEM,SDS_READ);
      if(flat_sds<=0)
	{
	  printf("Could not locate shared memory for %s\n",
		 line_ptr->flat_file_name);
	  exit(0);
	}
    } else {
      flat_sds = sds_access(line_ptr->flat_file_name,SDS_FILE,SDS_READ);
    }
  if (flat_sds == SDS_NOT_SDS) {
    flatin(line_ptr->flat_file_name, line_ptr->flatin_ptr);
  } else if (flat_sds >= 0) {
    flatin_sds(flat_sds, line_ptr->flatin_ptr);
  } else {
    fprintf(stderr,"twiss: couldn't find ASCII or SDS flat file %s\n", 
	    line_ptr->flat_file_name);
    exit(1);
  }
  
  /* ................. and then allocate memory */

  f_ptr = line_ptr->flatin_ptr;
  line_ptr->magmat_ptr = 
    (magmat *) calloc(f_ptr->number_of_elements,sizeof(magmat));
  m_ptr = line_ptr->magmat_ptr;
  for (i = 0; i < f_ptr->number_of_elements; i++) m_ptr[i].matrix = dim2(6,6);

  line_ptr->optic_ptr = 
    (optic *) calloc(f_ptr->number_of_atoms + 1, sizeof(optic));

  line_ptr->clorb_ptr = 
    (clorb *) calloc(f_ptr->number_of_atoms + 1, sizeof(clorb));

}

/****************************************************************************/
void get_quadmultipole(double **m, double k1, double tilt)
{ /* Thin lens quadrupole */
  double **R = dim2(6,6);
  double **temp = dim2(6,6);

  unit_matrix(m,6);
  m[1][0] = -k1;
  m[3][2] = k1;

  /* Apply the TILT angle */
  if(tilt != 0) {
    get_srot(R,tilt);
    matrix_matrix(temp,m,R,6);
    get_srot(R,-tilt);
    matrix_matrix(m,R,temp,6);
  }

  free2(R);
  free2(temp);
}
/****************************************************************************/

void create_transfer_matrices(twiss_data *line_ptr)
{
  flatin_data  *f_ptr = line_ptr->flatin_ptr;
  int j;
  double **m;
  double length, strength, tilt, k1, e1, e2;
  double t1;
  int moreind;

  for (j = 0; j < f_ptr->number_of_elements; j++) {
    m        = line_ptr->magmat_ptr[j].matrix;
    length   = f_ptr->element_ptr[j].length;
    strength = f_ptr->element_ptr[j].strength;
    tilt     = f_ptr->element_ptr[j].tilt;

    switch(f_ptr->element_ptr[j].type_index){
    case ATTR_MULTIPOLE:
      if(f_ptr->element_ptr[j].npars > 0){
	moreind = f_ptr->element_ptr[j].more_index;
	k1 = f_ptr->parameter_ptr[moreind+1].value;
	t1 = f_ptr->parameter_ptr[moreind+11].value;
	get_quadmultipole(m,k1,t1);
      } else {
	unit_matrix(m,6);
      }
      break;
    case ATTR_QUADRUPOLE:
      get_quad(m,length,strength,tilt,line_ptr->delta);      
      if (fabs(strength) > line_ptr->quad_strength_max) 
	line_ptr->quad_strength_max = fabs(strength);
      break;
    case ATTR_RBEND:
      if(f_ptr->element_ptr[j].npars > 0){
	moreind = f_ptr->element_ptr[j].more_index;
	k1 = f_ptr->parameter_ptr[moreind].value;
	e1 = f_ptr->parameter_ptr[moreind+3].value;
	e2 = f_ptr->parameter_ptr[moreind+4].value;
      }
      else {
	k1 = 0;
	e1 = 0;
	e2 = 0;
      }
      if(strength == 0)
	{
	  get_drift(m,length);
	} else {
	  get_rbend(m,length,strength,k1,e1,e2,tilt,line_ptr->delta);
	}
      break;
    case ATTR_SBEND:
      if(f_ptr->element_ptr[j].npars > 0){
	moreind = f_ptr->element_ptr[j].more_index;
	k1 = f_ptr->parameter_ptr[moreind].value;
	e1 = f_ptr->parameter_ptr[moreind+3].value;
	e2 = f_ptr->parameter_ptr[moreind+4].value;
      }
      else {
	k1 = 0;
	e1 = 0;
	e2 = 0;
      }
      if(strength == 0)
	{
	  get_drift(m,length);
	} else {
	  get_sbend(m,length,strength,k1,e1,e2,tilt,line_ptr->delta);
	}
      break;
    case ATTR_SROT:
      get_srot(m,strength);
      break;
    case ATTR_YROT:
      get_yrot(m,strength);
      break;

#ifndef NOSYBASE
    case ATTR_TCELEMENT:
      get_tcelement(getenv("DBSF_DB"),
		    f_ptr->element_ptr[j].name, m);
      break;
#endif

    default:
      /* works even if legal_type.has_length = 0 */
      get_drift(m,length);
      break;
    }
  }
#ifndef NOSYBASE
  if(dbflag !=0)
    {
      dbexit();       /* close the sybase connection */
      dbflag = 0;
    }
#endif
}

/****************************************************************************/

void get_drift(double **m, double length)
{

  unit_matrix(m,6);
  m[0][1] = length;
  m[2][3] = length;
}

/****************************************************************************/

void get_quad(double **m, double length, double strength,
	      double tilt, double del)
{
  double q, k_q, a;
  double **R = dim2(6,6);
  double **temp = dim2(6,6);
  
/*
  if (tilt != 0.0 && tilt_err_quad==0) {
    fprintf(stderr,"WARNING: twiss handles tilts in quads improperly !\n");
    tilt_err_quad = 1;
  }
*/

  unit_matrix(m,6);
  if (strength > 0) {   /* F quad */
    q = length * strength/(1. + del);
    k_q = sqrt(q/length);
    a = k_q * length;
    m[0][0] = cos(a);
    m[0][1] = sin(a)/k_q;
    m[1][0] = - k_q * sin(a);
    m[1][1] = cos(a);
    m[2][2] = cosh(a);
    m[2][3] = sinh(a)/k_q;
    m[3][2] = k_q * sinh(a);
    m[3][3] = cosh(a);
  } else if (strength < 0) {   /* D quad */
    q = length * strength/(1. + del);
    k_q = sqrt(-q/length);
    a = k_q * length;
    m[0][0] = cosh(a);
    m[0][1] = sinh(a)/k_q;
    m[1][0] = k_q * sinh(a);
    m[1][1] = cosh(a);
    m[2][2] = cos(a);
    m[2][3] = sin(a)/k_q;
    m[3][2] = - k_q * sin(a);
    m[3][3] = cos(a);
  } else {    /* quad is off - a drift */
    m[0][0] = 1.0;
    m[0][1] = length;
    m[1][0] = 0.0;
    m[1][1] = 1.0;
    m[2][2] = 1.0;
    m[2][3] = length;
    m[3][2] = 0.0;
    m[3][3] = 1.0;
  }

  /* Apply the TILT angle */
  if(tilt != 0) {
    get_srot(R,tilt);
    matrix_matrix(temp,m,R,6);
    get_srot(R,-tilt);
    matrix_matrix(m,R,temp,6);
  }

  free2(R);
  free2(temp);
}

/****************************************************************************/

void get_rbend(double **m, double length, double bend_angle, double k1,
	       double e1, double e2, double tilt, double del)
{
  double **R    = dim2(6,6);
  double **temp = dim2(6,6);
  double pole_face_angle = 0.5 * bend_angle;

  if (del != 0.0) {
    fprintf(stderr,"twiss: cannot handle delta != 0.0 in sbends !\n");
    if(!interact)exit(1);
  }
  if (bend_angle != 0.0) {
    /* convert to arc length from from chord length */
    length = ((0.5 * bend_angle) / sin(0.5 * bend_angle)) * length;
  }

  /* An rbend is just an sbend with wedges on the ends. */
  get_sbend(m,length,bend_angle,k1
	    ,e1+pole_face_angle,e2+pole_face_angle,0,del);

  /* Apply the TILT angle */
  if(tilt != 0) {
    get_srot(R,tilt);
    matrix_matrix(temp,m,R,6);
    get_srot(R,-tilt);
    matrix_matrix(m,R,temp,6);
  }

  free2(R);
  free2(temp);
}

/****************************************************************************/

void get_sbend(double **m, double length, double bend_angle, double k1,
	       double e1, double e2, double tilt, double del)
{
  double k_x;
  double rho, fieldindex;
  double rx, ry, cx, cy, sx, sy, fudgesignx, fudgesigny;
  double p;
  int i;
  double **R    = dim2(6,6);
  double **temp = dim2(6,6);

  if (del != 0.0) {
    fprintf(stderr,"twiss: cannot handle delta != 0.0 in sbends !\n");
    if(!interact)exit(1);
  }

  rho = length/bend_angle;
  fieldindex = -k1*rho*rho;

  unit_matrix(m,6);

  if (fieldindex == 0) {
    k_x = length/bend_angle;
    m[0][0] = cos(bend_angle);
    m[0][1] = k_x * sin(bend_angle);
    m[0][5] = k_x * (1. - cos(bend_angle));
    m[1][0] = - sin(bend_angle)/k_x;
    m[1][5] = sin(bend_angle);
    m[2][3] = length;
    m[4][5] = - k_x * (bend_angle - sin(bend_angle));
  }
  else if (fieldindex == 1) {
    m[0][1] = length;
    m[1][5] = bend_angle;
    m[2][2] = cos(bend_angle);
    m[2][3] = rho*sin(bend_angle);
    m[3][2] = -sin(bend_angle)/rho;
    m[4][5] = -length*bend_angle*bend_angle;
  }
  else {
    if(fieldindex < 0) {
      ry = sqrt(-fieldindex);
      cy = cosh(ry*bend_angle);
      sy = sinh(ry*bend_angle);
      fudgesigny = 1.0;
      }
    else {
      ry = sqrt(fieldindex);
      cy = cos(ry*bend_angle);
      sy = sin(ry*bend_angle);
      fudgesigny = -1;
    }
    if(fieldindex < 1) {
      rx = sqrt(1.0-fieldindex);
      cx = cos(rx*bend_angle);
      sx = sin(rx*bend_angle);
      fudgesignx = -1.0;
      }
    else {
      rx = sqrt(fieldindex-1.0);
      cx = cosh(rx*bend_angle);
      sx = sinh(rx*bend_angle);
      fudgesignx = 1.0;
    }
    m[0][0] = cx;
    m[0][1] = rho*sx/rx;
    m[0][5] = -fudgesignx*rho*(1.0-cx)/rx/rx;
    m[1][0] = fudgesignx*rx*sx/rho;
    m[1][5] = sx/rx;
    m[2][2] = cy;
    m[2][3] = rho*sy/ry;
    m[3][2] = fudgesigny*ry*sy/rho;
/* The m[4,5] term differs from MAD in that we assume v/c=1 for the beam
*  I believe MAD  uses something more like:
*   m[4][5] = -rho*(bend_angle-sx/rx)/(1.0-fieldindex)/beta^2
*             +length/(beta*gamma)^2
*  where the default energy is 1GeV and default mass is 0.511keV/c^2
*/
    m[4][5] = -rho*(bend_angle-sx/rx)/(1.0-fieldindex);
  }
  m[1][1] = m[0][0];
  m[3][3] = m[2][2];
  m[4][0] = - m[1][5];
  m[4][1] = - m[0][5];

  if(e1 != 0) {             /* adjust for an entry angle e1 */
    p = tan(e1)/rho;
    for (i=0; i<6; i++) {
      m[i][0] = m[i][0]+p*m[i][1];
      m[i][2] = m[i][2]-p*m[i][3];
    }
  }
  if(e2 != 0) {             /* adjust for an exit angle e1 */
    p = tan(e2)/rho;
    for (i=0; i<6; i++) {
      m[1][i] = m[1][i]+p*m[0][i];
      m[3][i] = m[3][i]-p*m[2][i];
    }
  }
  /* Apply the TILT angle */
  if(tilt != 0) {
    get_srot(R,tilt);
    matrix_matrix(temp,m,R,6);
    get_srot(R,-tilt);
    matrix_matrix(m,R,temp,6);
  }
  free2(R);
  free2(temp);
}
/****************************************************************************/
void get_srot(double **m, double strength)
{    /* rotate about s-axis by angle=strength. */
  unit_matrix(m,6);
  m[0][0] = cos(strength);
  m[1][1] = m[0][0];
  m[2][2] = m[0][0];
  m[3][3] = m[0][0];
  m[0][2] = sin(strength);
  m[1][3] = m[0][2];
  m[2][0] = -m[0][2];
  m[3][1] = m[2][0];
}
/****************************************************************************/
void get_yrot(double **m, double strength)
{    /* rotate about y-axis by angle=strength. */
  fprintf(stderr,"!*** Warning 'twiss' evaluates YROT's incorrectly!\n");
  unit_matrix(m,6);
  m[0][0] = cos(strength);               /* cos(theta) */
  m[1][1] = 1/(m[0][0]*m[0][0]);         /* sec(theta)^2 */
/* The sec(theta)^2 comes from evaluating the new slope as
    tan(beam+rot) = (tan(beam)+tan(rot)) / (1-tan(beam)*tan(rot))
                  = (tan(beam)*(1+tan(rot)^2) + tan(rot) + tan(beam)^2*tan(rot)
                       + ...
                  = tan(beam)*sec(rot)^2 + tan(rot) + O(x'^2)
*/
  m[4][4] = m[0][0];
  m[0][4] = -sin(strength);
  m[4][0] = -m[0][4];
}
/****************************************************************************/
#ifndef NOSYBASE
extern DBPROCESS *dbproc;
extern char database[31];

int get_tcelement(char *database_name, char *element_name, double **m)
{     /* trajectorily challenged element */
  sds_handle sds;
  sds_code object;
  char sql[256];
  int struct_size, number_of_rows, object_size;
  int i,j,k;
  char filespec[80];
  optics_data_row *row_ptr;
  RETCODE result_code;
  sds_code code;
  
  char *cathedral;
  struct stat stbuf;

  tcelement *tce_ptr;

  unit_matrix(m,6);

  strcpy(database, database_name);

  cathedral = (char *)getenv("HOLY_LATTICE");
  if(cathedral == NULL)
    {
      fprintf(stderr,
	      "HOLY_LATTICE undefined, trying '/usr/local/Holy_Lattice'.\n");
      strcpy(filespec,"/usr/local/Holy_Lattice/");
  } else {
    strcpy(filespec,cathedral);
  }
  if(stat(filespec,&stbuf)==-1)
    {
      fprintf(stderr,"Could not find: %s\n",filespec);
      return(-1);
    }
  

  if(!strcmp(database_name,"\0"))
    {
      fprintf(stderr,
	      "get_tcelement: no database! TCELEMENT '%s' becomes a drift.\n",
	      element_name);
      return(-1);
    }
  if((sds = good_sds("OpticsData")) >=0)sds_destroy(sds);
  sds = sds_new("OpticsData");

  sprintf(sql,"select * from optics_data where name = '%s'\n",element_name);
  if(dbflag==0)
    {
      dbproc = (DBPROCESS *) db_logins("twiss", database_name,
				       "RHICSYB", "harmless", "harmless");
      dbuse(dbproc, database_name);
      sds_set_dbproc(dbproc);
      dbflag = 1;
    }
  dbcmd(dbproc, sql);
  dbsqlexec(dbproc);
  while((result_code = dbresults(dbproc)) != NO_MORE_RESULTS)
    {
      if(result_code == SUCCEED)
	{
	  code = sds_build_object(sds, "", "tcelements");
	}
    }
  
  if(sds_error<0)
    {
      sds_perror("twiss: sql query of optics_data failed in get_tcelement\n");
      return(-1);
    }
  sds_ass(sds,"OpticsData",SDS_PROC_MEM);
  if(sds_error<0)
    {
      sds_perror("twiss: assembling 'OpticsData' in get_tcelement failed\n");
      sds_error = 0;
    }
  row_ptr = sds_obname2ptr(sds,"optics_data");
  object  = sds_name2ind(sds,"optics_data");
  if(sds_error<0)
    {
      sds_perror("twiss: object 'optics_data' failed in get_tcelement\n");
      return(-1);
    }

  number_of_rows = sds_array_size(sds,object);
  if(number_of_rows == 0)
    {
      fprintf(stderr,"twiss: no row in '%s.optics_data' for TCELEMENT %s\n",
	      database_name,element_name);
      return(-1);
    }

  object_size = sds_sizeof_object(sds,object)/number_of_rows;
  struct_size = sizeof(optics_data_row);
  if(struct_size != object_size)
    {
      fprintf(stderr,"twiss: object size %d ",object_size);
      fprintf(stderr,"!= expected row size %d in optics_data table\n",
	      struct_size);
      return(-1);
    }

  strcat(filespec,row_ptr->t1);
  sds_destroy(sds);

/*  if(dbflag!=0)
    {
      dbexit();
      dbflag = 0;
    }
*/

  sds = sds_access(filespec,SDS_FILE,SDS_READ);
  if(sds == SDS_NOT_SDS)
    {
      fprintf(stderr,"get_tcelement: %s is not an SDS file.\n",filespec);
      return(-1);
    }

  tce_ptr = (tcelement *) sds_obname2ptr(sds,"mat_elt");
  object  = sds_name2ind(sds,"mat_elt");
  if(sds_error<0)
    {
      sds_perror("get_tcelement: object 'mat_elt' failed");
      return(-1);
    }
  number_of_rows = sds_array_size(sds,object);
  if(number_of_rows==0)
    {
      fprintf(stderr,"get_tcelement: no matrix elements for element %s\n",
	      element_name);
      return(-1);
    }
  object_size = sds_sizeof_object(sds,object)/number_of_rows;
  struct_size = sizeof(tcelement);
  if(struct_size != object_size)
    {
      fprintf(stderr,"get_tcelement: object size %d ",object_size);
      fprintf(stderr,"!= expected tcelement size %d ", struct_size);
      return(-1);
    }

  for (i=0; i<number_of_rows; i++)
    {
      j = tce_ptr[i].index[0];
      k = tce_ptr[i].index[1];
      m[j][k] = tce_ptr[i].value;
    }
  return 0;
}
#endif
/****************************************************************************/
void get_sext(double **m, double length, double strength, double tilt,
	      double del, double *offset)
{
  double int_strength;

  if (strength != 0.0 && tilt != 0.0 && tilt_err_sext==0) {
    fprintf(stderr,"WARNING: twiss handles tilts in sextupoles improperly !\n");
    tilt_err_sext = 1;
  }

  unit_matrix(m,6);
  
  if (length != 0.0) {
    int_strength = length * strength;
  } else {
    int_strength = strength;
  }
  /* 
    normally, use the MAD convention for 'strength', that is
    strength  = (d2By/dBx2)/Brho
    but if 'length' = 0, assume that the 'strength' is already integrated.
    
    remembering that X,Y,S is a left handed coordinate system, the sign of 
    'int_strength' is given by

    Dx' = -0.5 * int_strength * (x*x - y*y)
    Dy' =        int_strength * x*y

    expanding with  x = x0 + dx,   y = y0 + dy,  get

    Dx' = -0.5*int_strength * [x0*x0 - y0*y0]
              -int_strength*x0 * dx + int_strength*y0 * dy
	  -0.5*int_strength * [dx*dx - dy*dy]

    Dy' =  int_strength * x0*y0
          +int_strength*y0 * dx  +  int_strength*x0 * dy
	  +int_strength * dx*dy

    hence get the following matrix elements, for dx and dy
    */
  
  m[1][0] = -int_strength * offset[0] / (1.0 + del);
  m[1][2] =  int_strength * offset[2] / (1.0 + del);
  m[3][0] =  int_strength * offset[2] / (1.0 + del);
  m[3][2] =  int_strength * offset[0] / (1.0 + del);

  if (length != 0.0) {
    double **d    = dim2(6,6);
    double **temp = dim2(6,6);

    get_drift(d,0.5*length);
    matrix_matrix(temp,m,d,6);
    matrix_matrix(m,d,temp,6);

    free2(d);
    free2(temp);
  }

}

/****************************************************************************/

void get_transfer_matrix(twiss_data *line_ptr)
{
  clorb   *c_ptr = line_ptr->clorb_ptr;
  element *e_ptr;
  flatin_data    *f_ptr = line_ptr->flatin_ptr;
  magmat  *m_ptr = line_ptr->magmat_ptr;
  int eid, j;
  double **tran_ptr;
  double **mm    = dim2(6,6);
  double **m_tot = dim2(6,6);
  double **tran  = dim2(6,6);

  unit_matrix(m_tot,6);

  c_ptr[0]          = line_ptr->init_clorb;
  c_ptr[0].co_delta = line_ptr->delta;          /* fill the 6-vector */

  for (j = 1; j <= f_ptr->number_of_atoms; j++) {
    eid = f_ptr->atom_ptr[j-1].element_index;
    e_ptr = &f_ptr->element_ptr[eid];

/*  what the hell?  This screws up "pointers",  "types", and "tcelements" of
                       zero length.
    if (e_ptr->length == 0.0 && e_ptr->strength == 0.0) {
      c_ptr[j] = c_ptr[j-1];
    } else {
*/
    {
      switch(f_ptr->atom_ptr[j-1].type_index){
      case ATTR_SEXTUPOLE:
	get_sext(tran, e_ptr->length, e_ptr->strength, e_ptr->tilt,
		 line_ptr->delta, &c_ptr[j-1].co_x);
	tran_ptr = tran;
	break;       
      default:
	tran_ptr = m_ptr[eid].matrix;
	break;       
      }
      
      matrix_matrix(mm, tran_ptr, m_tot, 6);
      cp_matrix(m_tot,mm,6);    
      matrix_vector(&c_ptr[j].co_x, tran_ptr, &c_ptr[j-1].co_x, 6);
    }
  }

  cp_matrix(line_ptr->transfer_matrix,m_tot,6);    

  free2(mm);
  free2(m_tot);
  free2(tran);

}

/****************************************************************************/

void initialize_twiss_data(twiss_data *line_ptr, flatin_data *flat_ptr)
{
  line_ptr->delta        = 0.0;
  line_ptr->s_offset     = 0.0;
  line_ptr->mu_x_offset = 0.0;
  line_ptr->mu_y_offset = 0.0;
  line_ptr->beta_x_max   = 0.0;
  line_ptr->beta_y_max   = 0.0;
  line_ptr->eta_x_min    =  10000.0;
  line_ptr->eta_x_max    = -10000.0;
  line_ptr->eta_y_min    =  10000.0;
  line_ptr->eta_y_max    = -10000.0;
  line_ptr->gamma_t      = 0.0;
  line_ptr->quad_strength_max = 0.0;

  line_ptr->init_clorb.co_x     = 0.0;
  line_ptr->init_clorb.co_xp    = 0.0;
  line_ptr->init_clorb.co_y     = 0.0;
  line_ptr->init_clorb.co_yp    = 0.0;
  line_ptr->init_clorb.co_dt    = 0.0;
  line_ptr->init_clorb.co_delta = 0.0;

  line_ptr->init_optic.pathlen    = 0.0;
  line_ptr->init_optic.type_index = 0;
  line_ptr->init_optic.beta_x     = 1.0;
  line_ptr->init_optic.alfa_x     = 0.0;
  line_ptr->init_optic.gamma_x    = 1.0;
  line_ptr->init_optic.beta_y     = 1.0;
  line_ptr->init_optic.alfa_y     = 0.0;
  line_ptr->init_optic.gamma_y    = 1.0;
  line_ptr->init_optic.eta_x      = 0.0;
  line_ptr->init_optic.eta_xp     = 0.0;
  line_ptr->init_optic.eta_y      = 0.0;
  line_ptr->init_optic.eta_yp     = 0.0;
  line_ptr->init_optic.eta_z      = 0.0;
  line_ptr->init_optic.eta_1      = 1.0;
  line_ptr->init_optic.mu_x       = 0.0;
  line_ptr->init_optic.mu_y       = 0.0;

  line_ptr->init_optic.bm_xx      = 1.0; /* Assume no correlations */
  line_ptr->init_optic.bm_xpxp    = 1.0; /* and no longitudinal or */
  line_ptr->init_optic.bm_yy      = 1.0; /* momentum spread.       */
  line_ptr->init_optic.bm_ypyp    = 1.0;
  line_ptr->init_optic.bm_zz      = 0.0;
  line_ptr->init_optic.bm_uu      = 0.0;
  line_ptr->init_optic.bm_xxp     = 0.0;
  line_ptr->init_optic.bm_xy      = 0.0;
  line_ptr->init_optic.bm_xyp     = 0.0;
  line_ptr->init_optic.bm_xz      = 0.0;
  line_ptr->init_optic.bm_xu      = 0.0;
  line_ptr->init_optic.bm_xpy     = 0.0;
  line_ptr->init_optic.bm_xpyp    = 0.0;
  line_ptr->init_optic.bm_xpz     = 0.0;
  line_ptr->init_optic.bm_xpu     = 0.0;
  line_ptr->init_optic.bm_yyp     = 0.0;
  line_ptr->init_optic.bm_yz      = 0.0;
  line_ptr->init_optic.bm_yu      = 0.0;
  line_ptr->init_optic.bm_ypz     = 0.0;
  line_ptr->init_optic.bm_ypu     = 0.0;
  line_ptr->init_optic.bm_zu      = 0.0;

  line_ptr->transfer_matrix = dim2(6,6);
  line_ptr->clorb_ptr       = NULL;
  line_ptr->flatin_ptr      = flat_ptr;
  line_ptr->magmat_ptr      = NULL;
  line_ptr->optic_ptr       = NULL;

}

/****************************************************************************/

/*----- partition 6x6 matrix (m into m_x, m_y, a_x, a_y) ----*/

void partition(double **m, double **a_x, double **a_y,
	       double **m_x, double **m_y)
{
  int i, j;

  a_x[0][0] =  m[0][0]*m[0][0];
  a_x[0][1] = -2.* m[0][0]*m[0][1];
  a_x[0][2] =  m[0][1]*m[0][1];
  a_x[1][0] = -m[1][0]*m[0][0];
  a_x[1][1] = 1. + 2.*m[0][1]*m[1][0];
  a_x[1][2] = -m[0][1]*m[1][1];
  a_x[2][0] =  m[1][0]*m[1][0];
  a_x[2][1] = -2.* m[1][1]*m[1][0];
  a_x[2][2] =  m[1][1]*m[1][1];

  a_y[0][0] =  m[2][2]*m[2][2];
  a_y[0][1] = -2.* m[2][2]*m[2][3];
  a_y[0][2] =  m[2][3]*m[2][3];
  a_y[1][0] = -m[3][2]*m[2][2];
  a_y[1][1] = 1. + 2.*m[2][3]*m[3][2];
  a_y[1][2] = -m[2][3]*m[3][3];
  a_y[2][0] =  m[3][2]*m[3][2];
  a_y[2][1] = -2.* m[3][3]*m[3][2];
  a_y[2][2] =  m[3][3]*m[3][3];

  for(i = 0; i < 2; i++){
    for(j = 0; j < 2; j++){
      m_x[i][j] =  m[i][j];
      m_y[i][j] =  m[i + 2][j + 2];
    }
    m_x[2][i] =  m[5][i];
    m_x[i][2] =  m[i][5];
    m_y[2][i] =  m[5][i + 2];
    m_y[i][2] =  m[i + 2][5];
  }
  m_x[2][2] =  m[5][5];
  m_y[2][2] =  m[5][5];

}

/****************************************************************************/

void periodic_twiss(twiss_data *line_ptr)
{
  optic *o_ptr = &line_ptr->init_optic;
  double Qx, mu_x, trace_x;
  double Qy, mu_y, trace_y;
  double **a_x = dim2(3,3);
  double **a_y = dim2(3,3);
  double **m_x = dim2(3,3);
  double **m_y = dim2(3,3);

  partition(line_ptr->transfer_matrix,a_x,a_y,m_x,m_y);
  
  trace_x = m_x[0][0] + m_x[1][1];
  trace_y = m_y[0][0] + m_y[1][1];
  
  if (fabs(trace_x) > 2.0 || fabs(trace_y) > 2.0) {
    fprintf(stderr,"twiss: periodic motion unstable\n");
    if(!interact)exit(1);
  }
  mu_x = acos(0.5*trace_x);
  if (m_x[0][1] < 0.0) mu_x = twopi - mu_x;
  Qx = mu_x / twopi;

  mu_y = acos(0.5*trace_y);
  if (m_y[0][1] < 0.0) mu_y = twopi - mu_y;
  Qy = mu_y / twopi;
  
  o_ptr->beta_x = m_x[0][1]/sin(mu_x);
  o_ptr->beta_y = m_y[0][1]/sin(mu_y);
  
  o_ptr->alfa_x = 0.5 * (m_x[0][0] - m_x[1][1])/sin(mu_x);
  o_ptr->alfa_y = 0.5 * (m_y[0][0] - m_y[1][1])/sin(mu_y);
  
  o_ptr->gamma_x = (1.0 + (o_ptr->alfa_x)*(o_ptr->alfa_x) ) /
    o_ptr->beta_x;
  o_ptr->gamma_y = (1.0 + (o_ptr->alfa_y)*(o_ptr->alfa_y) ) / 
    o_ptr->beta_y;

  o_ptr->mu_x = 0.0;    /* horizontal betatron phase */
  o_ptr->mu_y = 0.0;    /* vertical betatron phase */

  o_ptr->eta_x  = (m_x[0][1]*m_x[1][2]-(m_x[1][1]-1.)*m_x[0][2])/
    ((m_x[1][1]-1.)*(m_x[0][0]-1.)-m_x[0][1]*m_x[1][0]);
  o_ptr->eta_y  = (m_y[0][1]*m_y[1][2]-(m_y[1][1]-1.)*m_y[0][2])/
    ((m_y[1][1]-1.)*(m_y[0][0]-1.)-m_y[0][1]*m_y[1][0]);

  o_ptr->eta_xp = (m_x[0][2]*m_x[1][0]-(m_x[0][0]-1.)*m_x[1][2])/
    ((m_x[1][1]-1.)*(m_x[0][0]-1.)-m_x[0][1]*m_x[1][0]);
  o_ptr->eta_yp = (m_y[0][2]*m_y[1][0]-(m_y[0][0]-1.)*m_y[1][2])/
    ((m_y[1][1]-1.)*(m_y[0][0]-1.)-m_y[0][1]*m_y[1][0]);

  if (o_ptr->beta_x <= 0.0) {
    fprintf(stderr,"twiss: initial beta_x must be positive !\n");
    if(!interact)exit(1);
  }
  if (o_ptr->beta_y <= 0.0) {
    fprintf(stderr,"twiss: initial beta_y must be positive !\n");
    if(!interact)exit(1);
  }

  free2(a_x);
  free2(a_y);
  free2(m_x);
  free2(m_y);

}

/****************************************************************************/

void propagate_twiss(twiss_data *line_ptr)
{
  atom    *a_ptr = line_ptr->flatin_ptr->atom_ptr;
  clorb   *c_ptr = line_ptr->clorb_ptr;
  element *e_ptr;
  flatin_data    *f_ptr = line_ptr->flatin_ptr;
  magmat  *m_ptr = line_ptr->magmat_ptr;
  optic   *o_ptr;
  int eid, j;
  double beta_x_max = line_ptr->beta_x_max;
  double beta_y_max = line_ptr->beta_y_max;
  double eta_x_min  = line_ptr->eta_x_min;
  double eta_x_max  = line_ptr->eta_x_max;
  double eta_y_min  = line_ptr->eta_y_min;
  double eta_y_max  = line_ptr->eta_y_max;
  double eta_ave;
  double gamma_t_sum = 0.0;
  double **tran_ptr;
  double **mm    = dim2(6,6);
  double **m_tot = dim2(6,6);
  double **tran  = dim2(6,6);
  double **a_x   = dim2(3,3);
  double **a_y   = dim2(3,3);
  double **m_x   = dim2(3,3);
  double **m_y   = dim2(3,3);
  int zkickflag=FALSE;

  unit_matrix(m_tot,6);
  
  o_ptr = line_ptr->optic_ptr;
  o_ptr[0] = line_ptr->init_optic;
  if (o_ptr[0].beta_x <= 0 || o_ptr[0].beta_y <= 0) {
    fprintf(stderr,"twiss: initial beta values must be positive !\n");
    if(!interact)exit(1);
  }

  for (j = 1; j <= f_ptr->number_of_atoms; j++) {
    eid = f_ptr->atom_ptr[j-1].element_index;
    e_ptr = &f_ptr->element_ptr[eid];
    o_ptr[j].element_index=eid;
    o_ptr[j].type_index=f_ptr->atom_ptr[j-1].type_index;
    o_ptr[j].pathlen=a_ptr[j-1].s;

/*  what the hell?  This screws up "pointers",  "types", and "tcelements" of
                       zero length.
    if (e_ptr->length == 0.0 && e_ptr->strength == 0.0) {
      c_ptr[j] = c_ptr[j-1];
      o_ptr[j] = o_ptr[j-1];
    } else {
*/
    {
      switch(f_ptr->atom_ptr[j-1].type_index){
      case ATTR_SEXTUPOLE:
	get_sext(tran, e_ptr->length, e_ptr->strength, e_ptr->tilt, 
		 line_ptr->delta, &line_ptr->clorb_ptr[j-1].co_x);
	tran_ptr = tran;
	break;       
      default:
	tran_ptr = m_ptr[eid].matrix;
	break;       
      }

      matrix_matrix(mm,tran_ptr,m_tot,6);
      cp_matrix(m_tot,mm,6);

      /* closed orbit propagation */
      matrix_vector(&c_ptr[j].co_x, tran_ptr, &c_ptr[j-1].co_x, 6);

      /* optics propagation */
      partition(tran_ptr,a_x,a_y,m_x,m_y);

      matrix_vector(&o_ptr[j].beta_x, a_x, &o_ptr[j-1].beta_x, 3);
      matrix_vector(&o_ptr[j].beta_y, a_y, &o_ptr[j-1].beta_y, 3);
      matrix_vector(&o_ptr[j].eta_x, tran_ptr, &o_ptr[j-1].eta_x, 6);
      if (o_ptr[j].eta_1 != 1.0 && !zkickflag) {
	fprintf(stderr,"Warning: %dth el't has a z-kick, so eta's are bad\n",j);
	zkickflag=TRUE;
      }

      if (o_ptr[j].beta_x > beta_x_max) beta_x_max = o_ptr[j].beta_x;
      if (o_ptr[j].beta_y > beta_y_max) beta_y_max = o_ptr[j].beta_y;
      if (o_ptr[j].eta_x < eta_x_min)   eta_x_min  = o_ptr[j].eta_x;
      if (o_ptr[j].eta_x > eta_x_max)   eta_x_max  = o_ptr[j].eta_x;
      if (o_ptr[j].eta_y < eta_y_min)   eta_y_min  = o_ptr[j].eta_y;
      if (o_ptr[j].eta_y > eta_y_max)   eta_y_max  = o_ptr[j].eta_y;

      o_ptr[j].mu_x = o_ptr[j-1].mu_x 
	+ atan2(m_x[0][1],
		m_x[0][0] * o_ptr[j-1].beta_x - m_x[0][1] * o_ptr[j-1].alfa_x);
      o_ptr[j].mu_y = o_ptr[j-1].mu_y
	+ atan2(m_y[0][1],
		m_y[0][0] * o_ptr[j-1].beta_y - m_y[0][1] * o_ptr[j-1].alfa_y);

      if(f_ptr->atom_ptr[j-1].type_index == ATTR_RBEND ||
	 f_ptr->atom_ptr[j-1].type_index == ATTR_SBEND){
	eta_ave = 0.5*(o_ptr[j-1].eta_x + o_ptr[j].eta_x) 
	  - (1.0/12.0) * e_ptr->length * e_ptr->strength;
	gamma_t_sum += eta_ave * e_ptr->strength;

      }

    } 
  }

  line_ptr->beta_x_max = beta_x_max;
  line_ptr->beta_y_max = beta_y_max;
  line_ptr->eta_x_min  = eta_x_min;
  line_ptr->eta_x_max  = eta_x_max;
  line_ptr->eta_y_min  = eta_y_min;
  line_ptr->eta_y_max  = eta_y_max;
  if (gamma_t_sum < 0.0) {
    line_ptr->gamma_t = -sqrt(f_ptr->atom_ptr[f_ptr->number_of_atoms - 1].s
				/ (-gamma_t_sum));
  } else {
    line_ptr->gamma_t =  sqrt(f_ptr->atom_ptr[f_ptr->number_of_atoms - 1].s
				/ gamma_t_sum);
  }

  free2(mm);
  free2(m_tot); 
  free2(tran);
  free2(a_x);  
  free2(a_y); 
  free2(m_x);  
  free2(m_y);    

}

/****************************************************************************/
void propagate_football(twiss_data *line_ptr)
{
  atom    *a_ptr = line_ptr->flatin_ptr->atom_ptr;
/*  clorb   *c_ptr = line_ptr->clorb_ptr; */
  element *e_ptr;
  flatin_data    *f_ptr = line_ptr->flatin_ptr;
  magmat  *m_ptr = line_ptr->magmat_ptr;
  optic   *o_ptr;

  int     eid, n, i, j, k, l;
  int     zkickflag;
  double  **tran_ptr;
  double  **sigma       = dim2(6,6);
  double  **sigma_tmp   = dim2(6,6);
  double  **tran        = dim2(6,6);

  zkickflag = FALSE;
  o_ptr = line_ptr->optic_ptr;
  o_ptr[0] = line_ptr->init_optic;

/* Initialize the beam matrix */
  sigma[0][0]=o_ptr[0].bm_xx;
  sigma[0][1]=o_ptr[0].bm_xxp;
  sigma[0][2]=o_ptr[0].bm_xy;
  sigma[0][3]=o_ptr[0].bm_xyp;
  sigma[0][4]=o_ptr[0].bm_xz;
  sigma[0][5]=o_ptr[0].bm_xu;
  sigma[1][1]=o_ptr[0].bm_xpxp;
  sigma[1][2]=o_ptr[0].bm_xpy;
  sigma[1][3]=o_ptr[0].bm_xpyp;
  sigma[1][4]=o_ptr[0].bm_xpz;
  sigma[1][5]=o_ptr[0].bm_xpu;
  sigma[2][2]=o_ptr[0].bm_yy;
  sigma[2][3]=o_ptr[0].bm_yyp;
  sigma[2][4]=o_ptr[0].bm_yz;
  sigma[2][5]=o_ptr[0].bm_yu;
  sigma[3][3]=o_ptr[0].bm_ypyp;
  sigma[3][4]=o_ptr[0].bm_ypz;
  sigma[3][5]=o_ptr[0].bm_ypu;
  sigma[4][4]=o_ptr[0].bm_zz;
  sigma[4][5]=o_ptr[0].bm_zu;
  sigma[5][5]=o_ptr[0].bm_uu;
  for(i=1;i<6;i++){
    for(j=0;j<i;j++){
      sigma[i][j]=sigma[j][i];
    }
  }

  for(n=1; n <= f_ptr->number_of_atoms; n++)
    {
      eid = f_ptr->atom_ptr[n-1].element_index;
      e_ptr = &f_ptr->element_ptr[eid];
      o_ptr[n].element_index = eid;
      o_ptr[n].type_index = f_ptr->atom_ptr[n-1].type_index;
      o_ptr[n].pathlen = a_ptr[n-1].s;

      switch(f_ptr->atom_ptr[n-1].type_index)
	{
	case ATTR_SEXTUPOLE:
	  get_sext(tran,e_ptr->length,e_ptr->strength,e_ptr->tilt,
		   line_ptr->delta, &line_ptr->clorb_ptr[n-1].co_x);
	  tran_ptr = tran;
	  break;
	default:
	  tran_ptr = m_ptr[eid].matrix;
	  break;
	}

/*   Calculate  SIGMA(n)=T(n) * SIGMA(n-1) * transpose[ T(n) ]   */

      for(i=0; i<6; i++)
	{
	  for(j=0; j<6; j++)
	    {
	      sigma_tmp[i][j]=0.0;
	      for(k=0 ;k<6 ;k++)
		{
		  for(l=0 ;l<6 ;l++)
		    {
		      sigma_tmp[i][j]=sigma_tmp[i][j]
			+tran_ptr[i][k]*tran_ptr[j][l]*sigma[k][l];
		    }
		}
	    }
	}
      cp_matrix(sigma,sigma_tmp,6);
      o_ptr[n].bm_xx      = sigma[0][0];
      o_ptr[n].bm_xxp     = sigma[0][1];
      o_ptr[n].bm_xy      = sigma[0][2];
      o_ptr[n].bm_xyp     = sigma[0][3];
      o_ptr[n].bm_xz      = sigma[0][4];
      o_ptr[n].bm_xu      = sigma[0][5];
      o_ptr[n].bm_xpxp    = sigma[1][1];
      o_ptr[n].bm_xpy     = sigma[1][2];
      o_ptr[n].bm_xpyp    = sigma[1][3];
      o_ptr[n].bm_xpz     = sigma[1][4];
      o_ptr[n].bm_xpu     = sigma[1][5];
      o_ptr[n].bm_yy      = sigma[2][2];
      o_ptr[n].bm_yyp     = sigma[2][3];
      o_ptr[n].bm_yz      = sigma[2][4];
      o_ptr[n].bm_yu      = sigma[2][5];
      o_ptr[n].bm_ypyp    = sigma[3][3];
      o_ptr[n].bm_ypz     = sigma[3][4];
      o_ptr[n].bm_ypu     = sigma[3][5];
      o_ptr[n].bm_zz      = sigma[4][4];
      o_ptr[n].bm_zu      = sigma[4][5];
      o_ptr[n].bm_uu      = sigma[5][5];
    }
  free2(sigma);
  free2(sigma_tmp);
  free2(tran);

  propagate_twiss(line_ptr);
}
/****************************************************************************/

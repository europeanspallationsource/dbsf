/* $Header: /usr/local/lambda/twiss/RCS/get_mat32.c,v 1.3 1994/05/16 12:43:24 mackay Exp $ */

#include <stdio.h>
#include <math.h>
#include <Twiss_data.h>
#include <lambda_matrix.h>

/*===========================*/
int get_mat32(int ind1, int ind2, Twiss_data tdp, double **m)
{
/* This function returns most of the transfer matrix between two elements.
** It assumes that there is no horizontal-vertical coupling.  The matrix is
** of the form:
**		( Cx  Sx  0   0   0   Dx  )
**		( Cx' Sx' 0   0   0   Dx' )
**		( 0   0   Cy  Sy  0   Dy  )
**		( 0   0   Cy' Sy' 0   Dy' )
**		( Ex  Fx  Ey  Fy  1   G   ) Here G is indeterminate.
**		( 0   0   0   0   0   1   )
*/

  optic *i;
  optic *o;
  double mu;
  double b, r, cs, sn;

  unit_matrix(m,6);

  if(ind1>=ind2)
    {
      fprintf(stderr,"get_mat32: ind1=%d must be < ind2=%d\n",ind1,ind2);
      return 1;
    }
  if(ind1<0 || ind2>(tdp.number_of_optic-1))
    {
      fprintf(stderr,"get_mat32: indices out of range\n");
      return 1;
    }

  i = &tdp.optic_ptr[ind1];
  o = &tdp.optic_ptr[ind2];

  mu = o->mu_x - i->mu_x;
  cs = cos(mu);
  sn = sin(mu);
  r = sqrt(o->beta_x/i->beta_x);
  b = sqrt(o->beta_x*i->beta_x);
  m[0][0] = r*(cs+i->alfa_x*sn);
  m[0][1] = b*sn;
  m[1][0] = -((o->alfa_x-i->alfa_x)*cs+(1+i->alfa_x*o->alfa_x)*sn)/b;
  m[1][1] = (cs-o->alfa_x*sn)/r;
  m[0][5] = o->eta_x  - m[0][0]*i->eta_x - m[0][1]*i->eta_xp;
  m[1][5] = o->eta_xp - m[1][0]*i->eta_x - m[1][1]*i->eta_xp;

  mu = o->mu_y - i->mu_y;
  cs = cos(mu);
  sn = sin(mu);
  r = sqrt(o->beta_y/i->beta_y);
  b = sqrt(o->beta_y*i->beta_y);
  m[2][2] = r*(cs+i->alfa_y*sn);
  m[2][3] = b*sn;
  m[3][2] = -((o->alfa_y-i->alfa_y)*cs+(1+i->alfa_y*o->alfa_y)*sn)/b;
  m[3][3] = (cs-o->alfa_y*sn)/r;
  m[2][5] = o->eta_y  - m[2][2]*i->eta_y - m[2][3]*i->eta_yp;
  m[3][5] = o->eta_yp - m[3][2]*i->eta_y - m[3][3]*i->eta_yp;

/* The following terms satisfy the symplectic condition transpose(M)*S*M=S. */
  m[4][0] = -m[0][0]*m[1][5] + m[1][0]*m[0][5];
  m[4][1] = -m[0][1]*m[1][5] + m[1][1]*m[0][5];

  m[4][2] = -m[2][2]*m[3][5] + m[3][2]*m[2][5];
  m[4][3] = -m[2][3]*m[3][5] + m[3][3]*m[2][5];

/* The G = m[4][5] term is not obtainable from just beta's, alpha's mu's,
** and eta's.  However! if there is information in the bm_... section, we
** may be able to construct a value for G.  */
  if(i->bm_uu==0)return 0; /* This algorithm won't work unless we have momentum
			  ** spread. */
  m[4][5] = ( (o->bm_zu - i->bm_zu)
	     - (  m[4][0]*i->bm_xu + m[4][1]*i->bm_xpu
		+ m[4][2]*i->bm_yu + m[4][3]*i->bm_ypu ) )/i->bm_uu;

  return 0;
}

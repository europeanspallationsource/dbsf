
/* $Header: /rap/lambda/flatin/RCS/flatin_sds_demo.c,v 1.2 2004/06/14 18:03:22 satogata Exp $ */

#include  <stdlib.h>
#include  <string.h>
#include  <ctype.h>

#include "flatin.h"

int
main(argc,argv)
int	argc;
char	*argv[];
{

	flatin_data my_flatin;
	char *file_name;
	char latfile[NAME_FLATIN_MAX];

	if(argc != 2) {
	  printf("Usage flatin_sds_demo sds_name\n");
	  printf("sds_name is a beamline in flat sds format\n");
	  exit(1);
	}
	file_name = *(argv+1);
	if(flatin_sds(file_name,&my_flatin)) {
	  printf("lattice has %d atoms\n",my_flatin.number_of_atoms);
	  printf("element has %d elements\n",my_flatin.number_of_elements);
	  printf("lattice has %d elements\n",my_flatin.number_of_parameters);
	  printf("legal_type has %d elements\n",my_flatin.number_of_legal_types);
	  printf("row has %d elements\n",my_flatin.number_of_rows);
	}
	exit(0);

}


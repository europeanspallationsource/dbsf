/* $Header: /rap/lambda/flatin/RCS/flat_dump.c,v 1.4 1994/08/27 17:05:22 mackay Exp $ */
/* A program to print out whats in a section of beam line from the flat
** SDS file */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <flatin.h>
#include <Survey_data.h>

int main(int argc, char **argv)
{
  sds_handle sds_flat, sds_survey;
  int errout = 0;

  flatin_data dp;
  Survey_data sp;
  long i, j;
  long first,last;
  long element_index;

  long ind_survey;
  double N, W, E;

/*
  printf("argc = %d\n",argc);
  for(i=0; i<argc; i++)
    {
      printf("%d:%s\n",i,argv[i]);
    }
*/

  if(argc<5)
    {
      printf("usage: %s sds_flat sds_survey first last\n",argv[0]);
      printf(" where sds_flat is the name of the flat sds file,\n");
      printf("       sds_survey is the name of the sds Survey file,\n");
      printf("       first is the first atom to dump, and\n");
      printf("       last is the last atom to dump.\n");
      printf("  This dumps raw entry info to the screen.\n");
      exit(1);
    }
/*
  printf("Enter first and last atoms to dump:\n");
  scanf("%d %d",&first,&last);
*/
  first = atoi(argv[3]);
  last  = atoi(argv[4]);
  if(first<0)first = 0;
  if(last<0)last = 0;

  sds_init();
  sds_output_errors(errout);
  sds_output_proginfo(errout);
  
  sds_flat = sds_access(argv[1],SDS_FILE,SDS_READ);
  if(sds_flat<=0)
    {
      printf("Could not locate lattice file:%s\n",argv[1]);
      exit(1);
    }
  raw_flatin_sds(sds_flat, &dp);

  sds_survey = sds_access(argv[2],SDS_FILE,SDS_READ);
  if(sds_survey<=0)
    {
      printf("Could not locate survey file:%s\n",argv[1]);
      exit(1);
    }
  survey_in_sds(sds_survey, &sp);
  
  printf("# in lattice:\t%d\n",dp.number_of_atoms);
  printf("# in elements:\t%d\n",dp.number_of_elements);
  printf("# in parameters:\t%d\n",dp.number_of_parameters);
  printf("# in legal_types:\t%d\n",dp.number_of_legal_types);
  printf("# in rows:\t%d\n",dp.number_of_rows);

  if(first>dp.number_of_atoms)first = dp.number_of_atoms;
  if(last>dp.number_of_atoms)last = dp.number_of_atoms;

  ind_survey = 0;
  for(i=0; i<sp.number_of_survey; i++)
    {    /* get close to the right index to start */
      if(sp.survey_ptr[i].lattice_index >= first)
	{
	  ind_survey = i;
	  break;
	}
    }

  printf("element       s[m]    latname       elt.type      level occur");
  printf("   strength ");
  printf("   atom.len   sl/bl.len     N[m]            W[m]         E[m]\n");

  for(i=first; i<=last; i++)
    {
      printf("%4ld\t",i);
      printf("%12.6f  ",dp.atom_ptr[i].s);
      element_index = dp.atom_ptr[i].element_index;
      printf("%-12s  ",dp.element_ptr[element_index].name);
      printf("%-12s  ",dp.element_ptr[element_index].type);
      printf("%3d  %3d  ",dp.atom_ptr[i].level,dp.atom_ptr[i].occurence);
      if(dp.atom_ptr[i].level != 0)
	{
	  printf("            ");
	}
      printf("%12.6f",dp.element_ptr[element_index].strength);
      printf("%12.6f",dp.element_ptr[element_index].length);

      if(i > ind_survey)
	{
	  for(j=ind_survey; j<sp.number_of_survey; j++)
	    {
	      if(sp.survey_ptr[j].lattice_index >= i)
		{
		  ind_survey = j;
		  break;
		}
	    }
	  if(sp.survey_ptr[j].lattice_index == i && j > 0)
	    {
	      N = (sp.survey_ptr[j].x+sp.survey_ptr[j-1].x)/2.0;
	      W = (sp.survey_ptr[j].y+sp.survey_ptr[j-1].y)/2.0;
	      E = (sp.survey_ptr[j].z+sp.survey_ptr[j-1].z)/2.0;
	      printf("            %13.6f %13.6f %13.6f",N,W,E);
	    }
	}

      printf("\n");
    }
  return 0;
}

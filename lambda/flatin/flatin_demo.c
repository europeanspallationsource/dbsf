
/* $Header: /usr2/local/lambda/flatin/RCS/flatin_demo.c,v 1.1 1993/07/07 13:14:06 salty Exp $ */

/* ----------------------------------------------------------------------
   This demo demonstrates and tests flatin.c functions */

#include <stdio.h>
#include <stdlib.h>
#include "flatin.h"

main(argc, argv)
     int argc; char **argv;
{
  flatin_data my_flatin_data;
  char *file_name;

  if ( argc != 2) {
    printf("Usage: flatin_demo  flat_file_name\n");
    printf("  flat_file_name is a beam line in 'flat' format\n");
    printf("  This demo demonstrates and tests flatin.c functions\n");
    exit(1);
  }
  file_name = *(argv+1);

  printf("\nLattice file name is %s\n",file_name);

  if (flatin(file_name,&my_flatin_data)) {

    printf("\nFound %d parameters, %d elements, and %d atoms\n",
	   my_flatin_data.number_of_parameters,
	   my_flatin_data.number_of_elements,
	   my_flatin_data.number_of_atoms);
    printf("in a beam line of total length %.10f meters\n\n",
          my_flatin_data.atom_ptr[my_flatin_data.number_of_atoms-1].s);

    printf("For more information, see the file 'flatin.log'\n\n");

  }

  exit(0);
  
}






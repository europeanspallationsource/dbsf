/* $Header: /rap/lambda/flatin/RCS/flatin_sds.c,v 1.3 1994/06/03 14:04:39 mackay Exp $ */

#include  <math.h>
#include  <stdlib.h>
#include  <string.h>
#include  <unistd.h>
#include  <ctype.h>

#include "Sds/sdsgen.h"
#include "flatin.h"

int *latindices;

int flatin_sds(sds,data_ptr)
sds_handle sds;
flatin_data *data_ptr;
{

  sds_handle  ob_index;
  int i, j;
  atom * atom_ptr, *ap;
  element *es;
  int number_of_atoms = 0;
  char *to, *from;
  double bend_angle, arc_length = 0;
  legal *lt;

  data_ptr->number_of_atoms = 0;

  atom_ptr = (atom *)sds_obname2ptr(sds,"lattice");
  if (!atom_ptr)
  {
    fprintf(stderr,"flatin_sds: Not lattice sds\n");
    exit(1);
  }
  es = (element *)sds_obname2ptr(sds,"element");
  ob_index = sds_name2ind(sds,"lattice");
  number_of_atoms = sds_array_size(sds,ob_index);

  data_ptr->legal_type_ptr = (legal *)sds_obname2ptr(sds,"legal_type");
  lt = data_ptr->legal_type_ptr;
  ob_index = sds_name2ind(sds,"legal_type");
  data_ptr->number_of_legal_types = sds_array_size(sds,ob_index);



  latindices = (int *)calloc(number_of_atoms, sizeof(int));
  for (i = 0;i < number_of_atoms;i++)
    if (atom_ptr[i].level == 0)
      latindices[++data_ptr->number_of_atoms] = i;

  data_ptr->atom_ptr = (atom *)calloc(number_of_atoms, sizeof(atom));
  to = (char *)data_ptr->atom_ptr;
  from = (char *)atom_ptr;

  j = 0;
  for (i = 0;i < number_of_atoms;i++,from += sizeof(atom))
    if (atom_ptr[i].level == 0)
    {
      ap = (atom *)to;
      memcpy(to, from, sizeof(atom));

      if (!strcmp(lt[es[ap->element_index].type_index].name,"rbend") &&
          ((bend_angle = es[ap->element_index].strength) != 0.0)) 
      {
        arc_length += ((0.5 * bend_angle) / sin(0.5 * bend_angle)) *
                                 es[ap->element_index].length;
      }
      else
      {
        arc_length += es[ap->element_index].length;
      }
      ap->s = arc_length;
      to += sizeof(atom);
    }

  data_ptr->element_ptr = (element *)sds_obname2ptr(sds,"element");
  ob_index = sds_name2ind(sds,"element");
  data_ptr->number_of_elements = sds_array_size(sds,ob_index);

  data_ptr->parameter_ptr = (parameter *)sds_obname2ptr(sds,"parameter");
  ob_index = sds_name2ind(sds,"parameter");
  data_ptr->number_of_parameters = sds_array_size(sds,ob_index);

  data_ptr->row_ptr = (row_struct *)sds_obname2ptr(sds,"row");
  ob_index = sds_name2ind(sds,"row");
  data_ptr->number_of_rows = sds_array_size(sds,ob_index);


  return(1);
}


/* $Header: /rap/lambda/flatin/RCS/raw_flatin_sds.c,v 1.4 1996/03/20 16:11:19 mackay Exp $ */

#include <stdio.h>
#include <stdlib.h>
#include <flatin.h>
#include <Sds/sdsgen.h>

int raw_flatin_sds(sds_handle sds, flatin_data *data_ptr)
{
/* This is a version of flatin which does not throw away slot and other stuff.
** It also does not readjust any arc lengths.
** It just reads in the flat_sds file as is.
*/

  sds_handle  ob_index;

  data_ptr->atom_ptr = (atom *)sds_obname2ptr(sds,"lattice");
  if (!data_ptr->atom_ptr)
  {
    fprintf(stderr,"raw_flatin_sds: Not lattice sds\n");
    exit(1);
  }
  ob_index = sds_name2ind(sds,"lattice");
  data_ptr->number_of_atoms = sds_array_size(sds,ob_index);

  data_ptr->legal_type_ptr = (legal *)sds_obname2ptr(sds,"legal_type");
  ob_index = sds_name2ind(sds,"legal_type");
  data_ptr->number_of_legal_types = sds_array_size(sds,ob_index);

  data_ptr->element_ptr = (element *)sds_obname2ptr(sds,"element");
  ob_index = sds_name2ind(sds,"element");
  data_ptr->number_of_elements = sds_array_size(sds,ob_index);

  data_ptr->parameter_ptr = (parameter *)sds_obname2ptr(sds,"parameter");
  ob_index = sds_name2ind(sds,"parameter");
  data_ptr->number_of_parameters = sds_array_size(sds,ob_index);

  data_ptr->row_ptr = (row_struct *)sds_obname2ptr(sds,"row");
  ob_index = sds_name2ind(sds,"row");
  data_ptr->number_of_rows = sds_array_size(sds,ob_index);

  return(1);
}

/*
  $Id: header.H,v 1.1 1996/04/09 17:25:57 tepikian Exp $

          This class includes all the basic header files.

  $Log: header.H,v $
 * Revision 1.1  1996/04/09  17:25:57  tepikian
 * Initial revision
 *
*/

#if !defined( _header_h )
#define _header_h

#include <ctype.h>
#include <math.h>
#include <float.h>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <string>
#include <strstream>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

const double EPS_MACH = DBL_EPSILON;

#if defined(__GCC_2__)
extern "C" int  strcasecmp( const char *s1, const char *s2 );
extern "C" int  strncasecmp( const char *s1, const char *s2, size_t n );
// typedef _G_time_t time_t;
#endif

enum BE_Boolean { BE_false, BE_true };

inline double abs( double a ) { return( fabs( a ) ); }
inline double norm( double a ) { return( ( a == 0.0 ) ? 0.0 : a * a ); }
template<class T> inline T min( T a, T b ) { return( ( a < b ) ? a : b ); }
template<class T> inline T max( T a, T b ) { return( ( a > b ) ? a : b ); }

inline void shft(double &a, double &b,double &c, double d)
{
   a = b; b = c; c = d;
}

inline double sign( double a, double b)
{
   return( b > 0.0 ? fabs(a) : -fabs(a) );
}

inline void swap( double &a, double&b )
{
   double temp = a; a = b; b = temp;
}

#endif  // End of header.h.

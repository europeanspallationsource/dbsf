/*
  $Id: elastic.C,v 1.4 1998/01/27 16:04:41 tepikian Exp $

  $Log: elastic.C,v $
 * Revision 1.4  1998/01/27  16:04:41  tepikian
 * Added the check for LHC's crossing point.
 *
 * Revision 1.3  1997/08/05  17:19:52  tepikian
 * Corrected the reading of the optics data from MAD.
 *
 * Revision 1.2  1997/01/09  20:50:28  tepikian
 * Added code to handle the changed format of later MAD's.
 *
 * Revision 1.1  1996/04/09  17:24:26  tepikian
 * Initial revision
 *
*/

#include "header.hxx"
#include "elastic.hxx"
#include <unistd.h>

#include <cstring>
#include <cstdlib>


Elastic::Elastic(const char *file_name)
{
   const unsigned BUFF_LEN = 256;
   unsigned length = strlen(file_name);
   char element[50], buffer[BUFF_LEN];

   // Tests to see if is the correct file type.
   if(strcmp(file_name + (length - 7), ".optics") != 0) {
      strcpy(_error_msg, "Unknown type of file: ");
      strcat(_error_msg, file_name);
      strcat(_error_msg, ".");
      _error_state = 3;
      return;
   }

   //  Checks whether the file exists.
   if(access(file_name, 00) != 0) {
      strcpy(_error_msg, "File: ");
      strcat(_error_msg, file_name);
      strcat(_error_msg, " does not exist.");
      _error_state = 1;
      return;
   } else {
      _error_state = 0;
   }
   

   // Store the original file name.
   strcpy(_file_name, file_name);

   ifstream ifs(file_name);
   if(ifs.rdstate() != 0) {
      strcpy(_error_msg, "Cannot open file: ");
      strcat(_error_msg, file_name);
      strcat(_error_msg, " for reading.");
      _error_state = 1;
      return;
   }
   stat(_file_name, &_file_stat);

   _size = 0;
   for( ; ; ) {
      // Check if the OPTICS_SIZE_MAX is exceeded.
      if(_size >= OPTICS_SIZE_MAX) {
	 strcpy(_error_msg, "Exceeded maximum array size. ");
	 strcat(_error_msg, "Increase OPTICS_SIZE_MAX in elastic.hxx.");
	 _error_state = 4;
	 return;
      }

      ifs.getline(buffer, BUFF_LEN, '\n');

      // Check for return or end of file.
      if(ifs.eof()) break;

      // Setting up an input string stream.
      istrstream istr(buffer, BUFF_LEN);

      // Check type of line
      switch(buffer[0]) {
      case '*':
	 istr >> ws >> element >> ws >> element;
	 if(strcmp(element, "NAME") != 0) {
	    strcpy(_error_msg, "Incorrect 1st column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (Elastic).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "KEYWORD") != 0) {
	    strcpy(_error_msg, "Incorrect 2nd column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (Elastic).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "S") != 0) {
	    strcpy(_error_msg, "Incorrect 3rd column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (Elastic).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "L") != 0) {
	    strcpy(_error_msg, "Incorrect 4th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (Elastic).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "K1L") != 0) {
	    strcpy(_error_msg, "Incorrect 5th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (Elastic).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "MUX") != 0) {
	    strcpy(_error_msg, "Incorrect 6th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (Elastic).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "MUY") != 0) {
	    strcpy(_error_msg, "Incorrect 7th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (Elastic).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "ALFX") != 0) {
	    strcpy(_error_msg, "Incorrect 8th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (Elastic).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "ALFY") != 0) {
	    strcpy(_error_msg, "Incorrect 9th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (Elastic).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "BETX") != 0) {
	    strcpy(_error_msg, "Incorrect 10th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (Elastic).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "BETY") != 0) {
	    strcpy(_error_msg, "Incorrect 11th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (Elastic).");
	    _error_state = 2;
	    return;
	 }
	 break;
      case '$':
	 break;
      case '@':
	 istr >> ws >> element >> ws >> element;
	 if (strcmp(element, "ALFA") == 0) {
	    istr >> ws >> element >> ws >> _alfa;
	 } else if (strcmp(element, "GAMTR") == 0) {
	    istr >> ws >> element >> ws >> _gamtr;
	 } else if (strcmp(element, "GAMMATR") == 0) {
	    istr >> ws >> element >> ws >> _gamtr;
	 } else if (strcmp(element, "XIX") == 0) {
	    istr >> ws >> element >> ws >> _xix;
	 } else if (strcmp(element, "XIY") == 0) {
	    istr >> ws >> element >> ws >> _xiy;
	 } else if (strcmp(element, "DQ1") == 0) {
	    istr >> ws >> element >> ws >> _xix;
	 } else if (strcmp(element, "DQ2") == 0) {
	    istr >> ws >> element >> ws >> _xiy;
	 } else if (strcmp(element, "QX") == 0) {
	    istr >> ws >> element >> ws >> _vx;
	 } else if (strcmp(element, "QY") == 0) {
	    istr >> ws >> element >> ws >> _vy;
	 } else if (strcmp(element, "Q1") == 0) {
	    istr >> ws >> element >> ws >> _vx;
	 } else if (strcmp(element, "Q2") == 0) {
	    istr >> ws >> element >> ws >> _vy;
	 } else if (strcmp(element, "CIRCUM") == 0) {
	    istr >> ws >> element >> ws >> _circ;
	 } else if (strcmp(element, "LENGTH") == 0) {
	    istr >> ws >> element >> ws >> _circ;
	 } else if (strcmp(element, "DELTA") == 0) {
	    istr >> ws >> element >> ws >> _delta;
	 } else if (strcmp(element, "DELTAP") == 0) {
	    istr >> ws >> element >> ws >> _delta;
	 } else if (strcmp(element, "COMMENT") == 0) {
	    istr >> ws >> element >> ws;
	    istr.getline(_title, Elastic::LINE_SIZE_MAX, '\n');
	    _title[Elastic::LINE_SIZE_MAX - 1] = '\0';
	 } else if (strcmp(element, "TITLE") == 0) {
	    istr >> ws >> element >> ws;
	    istr.getline(_title, Elastic::LINE_SIZE_MAX, '\n');
	    _title[Elastic::LINE_SIZE_MAX - 1] = '\0';
	 } else if (strcmp(element, "ORIGIN") == 0) {
	    istr >> ws >> element >> ws;
	    istr.getline(_origin, Elastic::LINE_SIZE_MAX, '\n');
	    _origin[Elastic::LINE_SIZE_MAX - 1] = '\0';
	 }
	 break;
      default:
	 istr >> ws >> element;
	 if(element[0] == '"') {
	    if (strlen(element) == 1) {
	       istr >> ws >> element;
	       _opt[_size]._name[0] = '\0';
	       istr >> ws >> element;
	       if (strlen(element) == 1) {
		  istr >> ws >> element;
		  _opt[_size]._keyword[0] = '\0';
	       } else {
		  strncpy(_opt[_size]._keyword, element + 1,
			  strlen(element) - 2);
	       }
	    } else {
	       strncpy(_opt[_size]._name, element + 1, strlen(element) - 2);
	       istr >> ws >> element;
	       if (strlen(element) == 1) {
		  istr >> ws >> element;
		  _opt[_size]._keyword[0] = '\0';
	       } else {
		  strncpy(_opt[_size]._keyword, element + 1,
			  strlen(element) - 2);
	       }
	    }
	 } else {
	    if(strcmp(element, "SPLIT") == 0) {
	       strcpy(_opt[_size]._keyword, element);
	       _opt[_size]._name[0] = '\0';
	    } else {
	       strcpy(_opt[_size]._name, element);
	       istr >> ws >> _opt[_size]._keyword;
	    }
	 }
	 istr >> ws >> _opt[_size]._s;
	 istr >> ws >> _opt[_size]._l;
	 istr >> ws >> _opt[_size]._k1l;
	 istr >> ws >> _opt[_size]._mux;
	 istr >> ws >> _opt[_size]._muy;
	 istr >> ws >> _opt[_size]._alfx;
	 istr >> ws >> _opt[_size]._alfy;
	 istr >> ws >> _opt[_size]._betx;
	 istr >> ws >> _opt[_size]._bety;

	 _size++;
      }
   }
   ifs.close();
   cout << _size << " data points read from file: " << file_name << endl;
}

void Elastic::plot(double pmom, double emmitn, double xyprime)
{
   int first;
   unsigned i;
   char plot_data[30], plot_box[30], plot_param[30], cmd_str[150];
   time_t *time_ptr;
   char time_str[30], file_mtime[30];
   double s_cross, muy_cross, beta_star, bety_star, alfy_star, bgma, amp_fct;
   double mux_cross, betx_star, alfx_star, xmin, xmax;

   const double TWOPI = 2.0 * M_PI;

   // Setting up the time string.
   time_ptr = new time_t;
   time(time_ptr);
   strcpy(time_str, ctime(time_ptr));
   strcpy(file_mtime, ctime(&_file_stat.st_mtime));
   delete time_ptr;

   // Remove the carriage return from the time string.
   for(i = 0; i < strlen(time_str); i++) {
      if(time_str[i] == '\n') {
	 time_str[i] = '\0';
	 break;
      }
   }
   for(i = 0; i < strlen(file_mtime); i++) {
      if(file_mtime[i] == '\n') {
	 file_mtime[i] = '\0';
	 break;
      }
   }

   // Find beta_star and s_cross.
   first = 1;
   xmax = -1.0e+10;
   xmin =  1.0e+10;
   beta_star = -1.0;
   s_cross = mux_cross = muy_cross = 0.0;
   betx_star = _opt[0]._betx;
   alfx_star = _opt[0]._alfx;
   bety_star = _opt[0]._bety;
   alfy_star = _opt[0]._alfy;
   for(i = 0; i < _size; i++) {
      if ((strcasecmp(_opt[i]._name, "mcr") == 0 ||
	  strcasecmp(_opt[i]._name, "star") == 0 ||
	  strcasecmp(_opt[i]._name, "phenix") == 0 ||
	  strcasecmp(_opt[i]._name, "dump") == 0 ||
	  strcasecmp(_opt[i]._name, "phobos") == 0 ||
	  strcasecmp(_opt[i]._name, "rf") == 0 ||
	  (strncasecmp(_opt[i]._name, "ip", 2) == 0 &&
	   isdigit(_opt[i]._name[2]))  ||
	  (strncasecmp(_opt[i]._name, "clock", 5) == 0 &&
	   isdigit(_opt[i]._name[5]))) &&
	  first == 1) {

	 s_cross = _opt[i]._s;
	 beta_star = 0.5 * (_opt[i]._betx + _opt[i]._bety);
	 mux_cross = _opt[i]._mux;
	 betx_star = _opt[i]._betx;
	 alfx_star = _opt[i]._alfx;
	 muy_cross = _opt[i]._muy;
	 bety_star = _opt[i]._bety;
	 alfy_star = _opt[i]._alfy;
	 first = 0;
      } else {
         xmin = ::min(xmin, _opt[i]._s);
         xmax = ::max(xmax, _opt[i]._s);
      }
   }
   bgma = pmom / 0.93827231;
   amp_fct = 6.0 * sqrt(emmitn / (6.0 * bgma));

   // Make the files for plotting.
   strcpy(plot_data, "__TMP__PLOT475.data_");
   ofstream ofplot(plot_data);

   for(i = 0; i < _size; i++) {
      double phase;
      if(xmin <= _opt[i]._s && _opt[i]._s <= xmax) {
	 ofplot << _opt[i]._s - s_cross;

	 phase = TWOPI * (_opt[i]._mux - mux_cross);
	 ofplot << "  " << xyprime * abs(sqrt(_opt[i]._betx * betx_star) * 
					 sin(phase)) / 1000.0;
	 ofplot << "  " << amp_fct * sqrt(_opt[i]._betx);
	 ofplot << "  " << abs(sqrt(_opt[i]._betx / betx_star) *
			       (cos(phase) + alfx_star * sin(phase)));

	 phase = TWOPI * (_opt[i]._muy - muy_cross);
	 ofplot << "  " << xyprime * abs(sqrt(_opt[i]._bety * bety_star) * 
					 sin(phase)) / 1000.0;
	 ofplot << "  " << amp_fct * sqrt(_opt[i]._bety);
	 ofplot << "  " << abs(sqrt(_opt[i]._bety / bety_star) *
			       (cos(phase) + alfy_star * sin(phase)));

	 ofplot << endl;
      }
   }
   ofplot.close();

   // Make the files for plotting.
   strcpy(plot_box, "__TMP__PLOT475.box_");
   ofstream ofbox(plot_box);

   for(i = 0; i < _size; i++) {
      if(xmin <= _opt[i]._s && _opt[i]._s <= xmax) {
         if(strcmp(_opt[i]._keyword, "QUADRUPOLE") == 0) {
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
            if(_opt[i]._k1l > 0.0) {
               ofbox << _opt[i]._s - s_cross - _opt[i]._l;
               ofbox << "  0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  0.0\n";
            } else if(_opt[i]._k1l < 0.0) {
               ofbox << _opt[i]._s - s_cross - _opt[i]._l;
               ofbox << "  -0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  -0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  0.0\n";
            }
         } else if(strcmp(_opt[i]._keyword, "SBEND") == 0 ||
		   strcmp(_opt[i]._keyword, "RBEND") == 0) {
	    if (_opt[i]._k1l == 0.0) {
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.25\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  0.25\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  -0.25\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  -0.25\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	    } else if (_opt[i]._k1l > 0.0) {
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.375\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  0.375\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  -0.125\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  -0.125\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	    } else {
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.125\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  0.125\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  -0.375\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  -0.375\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	    }
         } else if(strcmp(_opt[i]._keyword, "SEXTUPOLE") == 0) {
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.125\n";
            ofbox << _opt[i]._s - s_cross;
            ofbox << "  0.125\n";
            ofbox << _opt[i]._s - s_cross;
            ofbox << "  -0.125\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  -0.125\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
         } else {
            ofbox << _opt[i]._s - s_cross << "  0.0\n";
	 }
      }
   }
   ofbox.close();

   // Make the command file for XMGR.
   strcpy(plot_param, "__TMP__PLOT475.param_");
   ofstream ofcmd(plot_param);

   // Setting up the strings for the time.
   ofcmd << "with string\n";
   ofcmd << "   string on\n";
   ofcmd << "   string just 0\n";
   ofcmd << "   string loctype view\n";
   ofcmd << "   string 0.1, 0.03\n";
   ofcmd << "   string char size 0.5\n";
   ofcmd << "string def \"Time: " << time_str << "  Last file modify time: "
         << file_mtime << "\"\n";

   // Setting up the strings for the momentum.
   ofcmd << "with string\n";
   ofcmd << "   string on\n";
   ofcmd << "   string just 0\n";
   ofcmd << "   string loctype view\n";
   ofcmd << "   string 0.2, 0.8\n";
   ofcmd << "   string char size 0.8\n";
   ofcmd << "string def \"" << pmom << " GeV/c\"\n";

   // Setting up the strings for the normalized emmittance.
   ofcmd << "with string\n";
   ofcmd << "   string on\n";
   ofcmd << "   string just 0\n";
   ofcmd << "   string loctype view\n";
   ofcmd << "   string 0.2, 0.775\n";
   ofcmd << "   string char size 0.8\n";
   ofcmd << "string def \"\\8e\\0\\sN\\N = " << emmitn
         << "\\8p\\0 mm-mrad\"\n";

   // Setting up the strings for the x' or y'.
   ofcmd << "with string\n";
   ofcmd << "   string on\n";
   ofcmd << "   string just 0\n";
   ofcmd << "   string loctype view\n";
   ofcmd << "   string 0.2, 0.75\n";
   ofcmd << "   string char size 0.8\n";
   ofcmd << "string def \"\\8q\\0 = " << xyprime << "\\8m\\0rad\"\n";

   // Setting up the title and subtitle of the graph.
   ofcmd << "title \"RHIC Insertion Functions\"\n";
   ofcmd << "subtitle \"\\8n\\0\\sx\\N = " << _vx << "  \\8n\\0\\sy\\N = "
         << _vy << "  \\8b\\0\\S*\\N = " << beta_star << "  FILE = "
         << _file_name << "\"\n";
   ofcmd << "move g0.s2 to g1.s0\n";
   ofcmd << "move g0.s5 to g1.s1\n";
   ofcmd << "move g0.s3 to g0.s2\n";
   ofcmd << "move g0.s4 to g0.s3\n";
   ofcmd << "move g0.s6 to g2.s0\n";

   // The upper (beta functions) graph.
   ofcmd << "with g0\nview ymin 0.40\n";
   ofcmd << "   autoscale\n";

   // Setting up the labels
   ofcmd << "   xaxis ticklabel off\n";
   ofcmd << "   yaxis label \"[mm]\"\n";

   // Setting up the legends.
   ofcmd << "   legend on\n";
   ofcmd << "   legend x1 0.7\n";
   ofcmd << "   legend y1 0.8\n";
   ofcmd << "   legend char size 0.8\n";
   ofcmd << "   legend string 0 \"\\8q\\0L\\seff\\N [x]\"\n";
   ofcmd << "   legend string 1 \"6\\8s\\0\\sx\\N\"\n";
   ofcmd << "   legend string 2 \"\\8q\\0L\\seff\\N [y]\"\n";
   ofcmd << "   legend string 3 \"6\\8s\\0\\sy\\N\"\n";

   // Setting up the linestyles.
   ofcmd << "   s0 color 1\n";
   ofcmd << "   s0 linestyle 1\n";
   ofcmd << "   s1 color 1\n";
   ofcmd << "   s1 linestyle 3\n";
   ofcmd << "   s2 color 2\n";
   ofcmd << "   s2 linestyle 1\n";
   ofcmd << "   s3 color 2\n";
   ofcmd << "   s3 linestyle 3\n";

   // Setting up the magnet descriptions.
   ofcmd << "with g1\nview ymin 0.15\nview ymax 0.4\n";
   ofcmd << "   autoscale\n";
   ofcmd << "   legend on\n";
   ofcmd << "   legend x1 0.55\n";
   ofcmd << "   legend y1 0.35\n";
   ofcmd << "   legend char size 0.8\n";
   ofcmd << "   legend string 0 \"x\"\n";
   ofcmd << "   legend string 1 \"y\"\n";
   ofcmd << "   xaxis label \"S [m]\"\n";
   ofcmd << "   yaxis label \"a\\s11\\N\"\n";
   ofcmd << "   yaxis ticklabel op right\n";
   ofcmd << "   zeroxaxis on\n";
   ofcmd << "   zeroxaxis bar on\n";
   ofcmd << "   zeroxaxis tick major on\n";
   ofcmd << "   zeroxaxis tick minor on\n";
   ofcmd << "   s0 color 1\n";
   ofcmd << "   s1 color 2\n";

   // Setting up the magnet descriptions.
   ofcmd << "with g2\nview ymin 0.2\nview ymax 0.6\n";
   ofcmd << "   autoscale\n";
   ofcmd << "   world ymin -2\nworld ymax 2\n";
   ofcmd << "   frame off\n";
   ofcmd << "   xaxis off\n";
   ofcmd << "   yaxis off\n";
   ofcmd << "   s0 linestyle 1\n";
   ofcmd << "   s0 color 14\n";

   // Choosing the symbols.
   ofcmd.close();

   // Run XMGR.
   strcpy(cmd_str, "xmgrace -nxy ");
   strcat(cmd_str, plot_data);
   strcat(cmd_str, " -nxy ");
   strcat(cmd_str, plot_box);
   strcat(cmd_str, " -param ");
   strcat(cmd_str, plot_param);
   strcat(cmd_str, "; \\rm ");
   strcat(cmd_str, plot_data);
   strcat(cmd_str, "; \\rm ");
   strcat(cmd_str, plot_box);
   strcat(cmd_str, "; \\rm ");
   strcat(cmd_str, plot_param);
   strcat(cmd_str, " &");
   system(cmd_str);
}

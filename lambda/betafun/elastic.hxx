/*
  $Id: elastic.H,v 1.3 1997/03/24 15:58:55 tepikian Exp $

  $Log: elastic.H,v $
 * Revision 1.3  1997/03/24  15:58:55  tepikian
 * Increased array size from 5000 to 10000 to work with lhc.
 *
 * Revision 1.2  1997/03/12  16:53:13  tepikian
 * Increased array size from 4000 to 5000.
 *
 * Revision 1.1  1996/04/09  17:25:25  tepikian
 * Initial revision
 *
*/

#if !defined(__elastic_H__)
#define __elastic_H__

#include <iostream>

using namespace std;

class Elastic
{
public:
   void plot(double pmom, double emmitn, double yprime);

   int error() { return _error_state; }
   int error_msg();

   Elastic(const char *file_name);

protected:
private:
   // Maximum sizes of the arrays.
   enum SIZES_OF_ARRAYS
   {
      NAME_SIZE_MAX      = 20,
      KEYWORD_SIZE_MAX   = 20,
      FILE_NAME_SIZE_MAX = 50,
      LINE_SIZE_MAX      = 100,
      OPTICS_SIZE_MAX    = 10000
   };

   // Nested structures.
   struct Optics
   {
      char   _name[NAME_SIZE_MAX],       // Name of the element.
             _keyword[KEYWORD_SIZE_MAX]; // Device type.
      double _s,                         // Position in the ring.
             _l,                         // Length of the element.
             _k1l,                       // Integrated quadrupole strength.
             _mux,                       // Phase advance,  x - direction.
             _muy,                       // Phase advance,  y - direction.
             _alfx,                      // Alpha function, x - direction.
             _alfy,                      // Alpha function, y - direction.
             _betx,                      // Beta function,  x - direction.
             _bety;                      // Beta function,  y - direction.
   };

   // Global Data.
   char     _file_name[FILE_NAME_SIZE_MAX]; // File name.
   struct stat  _file_stat;                 // File statistics.
   char     _origin[LINE_SIZE_MAX],         // Message about the origin.
            _title[LINE_SIZE_MAX];          // Title
   double   _circ,                          // Circumference.
            _vx,                            // Tune, x - direction.
            _vy,                            // Tune, y - direction
            _xix,                           // Chromaticity, x - direction.
            _xiy,                           // Chromaticity, y - direction.
            _gamtr,                         // Gamma transition.
            _alfa,                          // Momentum compaction.
            _delta;                         // Delta(E) / c p0.
   int      _error_state;                   // Error state.
   char     _error_msg[LINE_SIZE_MAX];      // Error condition.

   // Local data.
   unsigned _size;                          // Number of data points.
   Optics   _opt[OPTICS_SIZE_MAX];          // Each data point.
          
};

inline int Elastic::error_msg()
{
   if(_error_state != 0) cerr << "Error -- " << _error_msg << endl;
   return _error_state;
}

#endif  // Ends elastic.H

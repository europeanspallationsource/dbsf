/*

*/

#if !defined(__betafunWithCO_H__)
#define __betafunWithCO_H__

using namespace std;

class BetaFunWithCO {
public:
   void plot();
   void plotCO();
   void profile();

   int error() { return _error_state; }
   int error_msg();

   BetaFunWithCO(const char *file_name);

protected:
private:
   // Maximum sizes of the arrays.
   enum SIZES_OF_ARRAYS {
      NAME_SIZE_MAX      =    20,
      KEYWORD_SIZE_MAX   =    20,
      FILE_NAME_SIZE_MAX =    50,
      LINE_SIZE_MAX      =   100,
      OPTICS_SIZE_MAX    = 20000
   };

   // Nested structures.
   struct Optics {
      char	_name[NAME_SIZE_MAX];		// Name of the element.
      char	_keyword[KEYWORD_SIZE_MAX];	// Device type.
      double	_s;			// Position in the ring.
      double	_l;			// Length of the element.
      double	_k1l;			// Integrated quadrupole strength.
      double	_betx;			// Beta function, x - direction.
      double	_dx;			// Dispersion,    x - direction.
      double	_xco;			// Dispersion,    x - direction.
      double	_bety;			// Beta function, y - direction.
      double	_dy;			// Dispersion,    y - direction.
      double	_yco;			// Dispersion,    y - direction.
   };

   // Global Data.
   char		_file_name[FILE_NAME_SIZE_MAX];	// File name.
   struct stat	_file_stat;			// File statistics.
   char		_origin[LINE_SIZE_MAX];		// Message about the origin.
   char		_title[LINE_SIZE_MAX];		// Title
   double	_circ;				// Circumference.
   double	_vx;				// Tune, x - direction.
   double	_vy;				// Tune, y - direction
   double	_xix;				// Chromaticity, x - direction.
   double	_xiy;				// Chromaticity, y - direction.
   double	_gamtr;				// Gamma transition.
   double	_alfa;				// Momentum compaction.
   double	_delta;				// Delta(E) / c p0.
   int		_error_state;			// Error state.
   char		_error_msg[LINE_SIZE_MAX];	// Error condition.

   // Local data.
   unsigned	_size;				// Number of data points.
   Optics	_opt[OPTICS_SIZE_MAX];		// Each data point.
          
};

inline int BetaFunWithCO::error_msg()
{
   if(_error_state != 0) cerr << "Error -- " << _error_msg << endl;
   return _error_state;
}

#endif  // Ends betafunWithCO.H

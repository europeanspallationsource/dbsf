/*
  $Id: beta_main.C,v 1.2 1998/08/03 18:47:20 tepikian Exp $

  This program reads a file generated using MAD's optics command and
  plots the beta and dispersion functions through the "xmgr" plotting
  program.

  $Log: beta_main.C,v $
 * Revision 1.2  1998/08/03  18:47:20  tepikian
 * Added the capability to plot the closed orbit.
 *
 * Revision 1.1  1996/04/09  14:33:05  tepikian
 * Initial revision
 *
*/

#include "header.hxx"
#include "betafun.hxx"
#include "betafunWithCO.hxx"
#include "elastic.hxx"
#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
   double energy, emit, angle;
   char in_cmd;

   BetaFun bf(argv[1]);
   if(bf.error() != 0) {
      Elastic es(argv[1]);
      if(es.error() != 0) {
	 BetaFunWithCO bfco(argv[1]);
	 if (bfco.error() != 0) {
	    bf.error_msg();
	    bfco.error_msg();
	    es.error_msg();
	    exit(1);
	 }
	 for( ; ; ) {   // infinite loop.
	    cout << '\n';
	    cout << "   b   Plot the Beta Functions and Dispersion.\n";
	    cout << "   c   Plot the Beta Functions and Closed Orbit.\n";
	    cout << "   p   Plot the beam profile.\n";
	    cout << "   q   Quit.\n\n";
	    cout << "Command> ";
	    cin >> in_cmd;
	    if(in_cmd == 'b' || in_cmd == 'B') {
	       bfco.plot();
	    } else if(in_cmd == 'p' || in_cmd == 'P') {
	       bfco.profile();
	    } else if(in_cmd == 'c' || in_cmd == 'C') {
	       bfco.plotCO();
	    } else if(in_cmd == 'q' || in_cmd == 'Q') {
	       return(0);
	    }
	 }
      }
      if (es.error_msg() != 0) return(1);
      for( ; ; ) {   // infinite loop.
	 cout << '\n';
	 cout << "   e   Plot the Elastic Functions.\n";
	 cout << "   q   Quit.\n\n";
	 cout << "Command> ";
	 cin >> in_cmd;

	 if(in_cmd == 'e' || in_cmd == 'E') {
	    cout << "\nEnergy [GeV] = ";
	    cin >> energy;
	    cout << "Normalized Emittance [pi mm-mrad] = ";
	    cin >> emit;
	    cout << "Scattering angle [micro-rad] = ";
	    cin >> angle;
	    es.plot(energy, emit, angle);
	 } else if(in_cmd == 'q' || in_cmd == 'Q') {
	    return(0);
	 }
      }
      return(0);
   }

   for( ; ; ) {   // infinite loop.
      cout << '\n';
      cout << "   b   Plot the Beta Functions.\n";
      cout << "   p   Plot the beam profile.\n";
      cout << "   q   Quit.\n\n";
      cout << "Command> ";
      cin >> in_cmd;
      if(in_cmd == 'b' || in_cmd == 'B') {
	 bf.plot();
      } else if(in_cmd == 'p' || in_cmd == 'P') {
	 bf.profile();
      } else if(in_cmd == 'q' || in_cmd == 'Q') {
	 break;
      }
   }

   return(0);
}

/*
  $Id: betafunWithCO.C,v 1.1 1998/08/03 18:48:00 tepikian Exp $


  $Log: betafunWithCO.C,v $
 * Revision 1.1  1998/08/03  18:48:00  tepikian
 * Initial revision
 *

*/

#include "header.hxx"
#include <iostream>
#include "betafunWithCO.hxx"
#include "unistd.h"

#include <cstring>
#include <cstdlib>

using namespace std;

BetaFunWithCO::BetaFunWithCO(const char *file_name) {
   const unsigned BUFF_LEN = 256;
   unsigned length = strlen(file_name);
   char element[50], buffer[BUFF_LEN];

   // Tests to see if is the correct file type.
   if(strcmp(file_name + (length - 7), ".optics") != 0) {
      strcpy(_error_msg, "Unknown type of file: ");
      strcat(_error_msg, file_name);
      strcat(_error_msg, ".");
      _error_state = 3;
      return;
   }

   //  Checks whether the file exists.
   if(access(file_name, 00) != 0) {
      strcpy(_error_msg, "File: ");
      strcat(_error_msg, file_name);
      strcat(_error_msg, " does not exist.");
      _error_state = 1;
      return;
   } else {
      _error_state = 0;
   }
   

   // Store the original file name.
   strcpy(_file_name, file_name);

   ifstream ifs(file_name);
   if(ifs.rdstate() != 0) {
      strcpy(_error_msg, "Cannot open file: ");
      strcat(_error_msg, file_name);
      strcat(_error_msg, " for reading.");
      _error_state = 1;
      return;
   }
   stat(_file_name, &_file_stat);

   _size = 0;
   for( ; ; ) {
      // Check if the OPTICS_SIZE_MAX is exceeded.
      if(_size >= OPTICS_SIZE_MAX) {
	 strcpy(_error_msg, "Exceeded maximum array size. ");
	 strcat(_error_msg, "Increase OPTICS_SIZE_MAX in betafunWithCO.hxx.");
	 _error_state = 4;
	 return;
      }

      ifs.getline(buffer, BUFF_LEN, '\n');

      // Check for return or end of file.
      if(ifs.eof()) break;

      // Setting up an input string stream.
      istrstream istr(buffer, BUFF_LEN);

      // Check type of line.
      switch(buffer[0]) {
      case '*':
	 istr >> ws >> element >> ws >> element;
	 if(strcmp(element, "NAME") != 0) {
	    strcpy(_error_msg, "Incorrect 1st column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (BetaFunWithCO).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "KEYWORD") != 0) {
	    strcpy(_error_msg, "Incorrect 2nd column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (BetaFunWithCO).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "S") != 0) {
	    strcpy(_error_msg, "Incorrect 3rd column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (BetaFunWithCO).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "L") != 0) {
	    strcpy(_error_msg, "Incorrect 4th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (BetaFunWithCO).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "K1L") != 0) {
	    strcpy(_error_msg, "Incorrect 5th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (BetaFunWithCO).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "BETX") != 0) {
	    strcpy(_error_msg, "Incorrect 6th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (BetaFunWithCO).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "DX") != 0) {
	    strcpy(_error_msg, "Incorrect 7th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (BetaFunWithCO).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "X") != 0) {
	    strcpy(_error_msg, "Incorrect 8th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (BetaFunWithCO).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "BETY") != 0) {
	    strcpy(_error_msg, "Incorrect 9th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (BetaFunWithCO).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "DY") != 0) {
	    strcpy(_error_msg, "Incorrect 10th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (BetaFunWithCO).");
	    _error_state = 2;
	    return;
	 }
	 istr >> ws >> element;
	 if(strcmp(element, "Y") != 0) {
	    strcpy(_error_msg, "Incorrect 11th column in file: ");
	    strcat(_error_msg, file_name);
	    strcat(_error_msg, " (BetaFunWithCO).");
	    _error_state = 2;
	    return;
	 }
	 break;
      case '$':
	 break;
      case '@':
	 istr >> ws >> element >> ws >> element;
	 if (strcmp(element, "ALFA") == 0) {
	    istr >> ws >> element >> ws >> _alfa;
	 } else if (strcmp(element, "GAMTR") == 0) {
	    istr >> ws >> element >> ws >> _gamtr;
	 } else if (strcmp(element, "GAMMATR") == 0) {
	    istr >> ws >> element >> ws >> _gamtr;
	 } else if (strcmp(element, "XIX") == 0) {
	    istr >> ws >> element >> ws >> _xix;
	 } else if (strcmp(element, "XIY") == 0) {
	    istr >> ws >> element >> ws >> _xiy;
	 } else if (strcmp(element, "DQ1") == 0) {
	    istr >> ws >> element >> ws >> _xix;
	 } else if (strcmp(element, "DQ2") == 0) {
	    istr >> ws >> element >> ws >> _xiy;
	 } else if (strcmp(element, "QX") == 0) {
	    istr >> ws >> element >> ws >> _vx;
	 } else if (strcmp(element, "QY") == 0) {
	    istr >> ws >> element >> ws >> _vy;
	 } else if (strcmp(element, "Q1") == 0) {
	    istr >> ws >> element >> ws >> _vx;
	 } else if (strcmp(element, "Q2") == 0) {
	    istr >> ws >> element >> ws >> _vy;
	 } else if (strcmp(element, "CIRCUM") == 0) {
	    istr >> ws >> element >> ws >> _circ;
	 } else if (strcmp(element, "LENGTH") == 0) {
	    istr >> ws >> element >> ws >> _circ;
	 } else if (strcmp(element, "DELTA") == 0) {
	    istr >> ws >> element >> ws >> _delta;
	 } else if (strcmp(element, "DELTAP") == 0) {
	    istr >> ws >> element >> ws >> _delta;
	 } else if (strcmp(element, "COMMENT") == 0) {
	    istr >> ws >> element >> ws;
	    istr.getline(_title, BetaFunWithCO::LINE_SIZE_MAX, '\n');
	    _title[BetaFunWithCO::LINE_SIZE_MAX - 1] = '\0';
	 } else if (strcmp(element, "TITLE") == 0) {
	    istr >> ws >> element >> ws;
	    istr.getline(_title, BetaFunWithCO::LINE_SIZE_MAX, '\n');
	    _title[BetaFunWithCO::LINE_SIZE_MAX - 1] = '\0';
	 } else if (strcmp(element, "ORIGIN") == 0) {
	    istr >> ws >> element >> ws;
	    istr.getline(_origin, BetaFunWithCO::LINE_SIZE_MAX, '\n');
	    _origin[BetaFunWithCO::LINE_SIZE_MAX - 1] = '\0';
	 }
	 break;
      default:
	 istr >> ws >> element;
	 if(element[0] == '"') {
	    if (strlen(element) == 1) {
	       istr >> ws >> element;
	       _opt[_size]._name[0] = '\0';
	       istr >> ws >> element;
	       if (strlen(element) == 1) {
		  istr >> ws >> element;
		  _opt[_size]._keyword[0] = '\0';
	       } else {
		  strncpy(_opt[_size]._keyword, element + 1,
			  strlen(element) - 2);
	       }
	    } else {
	       strncpy(_opt[_size]._name, element + 1, strlen(element) - 2);
	       istr >> ws >> element;
	       if (strlen(element) == 1) {
		  istr >> ws >> element;
		  _opt[_size]._keyword[0] = '\0';
	       } else {
		  strncpy(_opt[_size]._keyword, element + 1,
			  strlen(element) - 2);
	       }
	    }
	 } else {
	    if(strcmp(element, "SPLIT") == 0) {
	       strcpy(_opt[_size]._keyword, element);
	       _opt[_size]._name[0] = '\0';
	    } else {
	       strcpy(_opt[_size]._name, element);
	       istr >> ws >> _opt[_size]._keyword;
	    }
	 }
	 istr >> ws >> _opt[_size]._s;
	 istr >> ws >> _opt[_size]._l;
	 istr >> ws >> _opt[_size]._k1l;
	 istr >> ws >> _opt[_size]._betx;
	 istr >> ws >> _opt[_size]._dx;
	 istr >> ws >> _opt[_size]._xco;
	 istr >> ws >> _opt[_size]._bety;
	 istr >> ws >> _opt[_size]._dy;
	 istr >> ws >> _opt[_size]._yco;

	 _size++;
      }
   }
   ifs.close();
   cout << _size << " data points read from file: " << file_name << endl;
}

void BetaFunWithCO::plot() {
   int first;
   unsigned i;
   char plot_data[30], plot_box[30], plot_param[30], cmd_str[200];
   time_t *time_ptr;
   char time_str[30], file_mtime[30];
   double s_cross, beta_star_x, beta_star_y, xmin, xmax;

   // Setting up the time string.
   time_ptr = new time_t;
   time(time_ptr);
   strcpy(time_str, ctime(time_ptr));
   strcpy(file_mtime, ctime(&_file_stat.st_mtime));
   delete time_ptr;

   // Remove the carriage return from the time string.
   for(i = 0; i < strlen(time_str); i++) {
      if(time_str[i] == '\n') {
	 time_str[i] = '\0';
	 break;
      }
   }
   for(i = 0; i < strlen(file_mtime); i++) {
      if(file_mtime[i] == '\n') {
	 file_mtime[i] = '\0';
	 break;
      }
   }

   // Find beta_star and s_cross.
   first = 1;
   xmax = -1.0e+10;
   xmin =  1.0e+10;
   beta_star_x = beta_star_y = -1.0;
   s_cross = 0.0;
   for(i = 0; i < _size; i++) {
      if ((strncasecmp(_opt[i]._name, "ip", 2) == 0 ||
	   strcasecmp(_opt[i]._name, "mcr") == 0 ||
	  strncasecmp(_opt[i]._name, "clock", 5) == 0) &&
	  first == 1) {

	 s_cross = _opt[i]._s;
	 beta_star_x = _opt[i]._betx;
	 beta_star_y = _opt[i]._bety;
         first = 0;
      } else {
         xmin = ::min(xmin, _opt[i]._s);
         xmax = ::max(xmax, _opt[i]._s);
      }
   }

   // Make the files for plotting.
   strcpy(plot_data, "__TMP__PLOT477.data_");
   ofstream ofplot(plot_data);

   for(i = 0; i < _size; i++) {
      if(xmin <= _opt[i]._s && _opt[i]._s <= xmax) {
         ofplot << _opt[i]._s - s_cross;
         ofplot << "  " << sqrt(_opt[i]._betx);
         ofplot << "  " << sqrt(_opt[i]._bety);
         ofplot << "  " << _opt[i]._dx;
         ofplot << "  " << _opt[i]._dy;
         ofplot << endl;
      }
   }
   ofplot.close();

   // Make the files for plotting.
   strcpy(plot_box, "__TMP__PLOT477.box_");
   ofstream ofbox(plot_box);

   for(i = 0; i < _size; i++) {
      if(xmin <= _opt[i]._s && _opt[i]._s <= xmax) {
         if(strcmp(_opt[i]._keyword, "QUADRUPOLE") == 0) {
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
            if(_opt[i]._k1l > 0.0) {
               ofbox << _opt[i]._s - s_cross - _opt[i]._l;
               ofbox << "  0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  0.0\n";
            } else if(_opt[i]._k1l < 0.0) {
               ofbox << _opt[i]._s - s_cross - _opt[i]._l;
               ofbox << "  -0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  -0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  0.0\n";
            }
         } else if(strcmp(_opt[i]._keyword, "SBEND") == 0 ||
		   strcmp(_opt[i]._keyword, "RBEND") == 0) {
	    if (_opt[i]._k1l == 0.0) {
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.25\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  0.25\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  -0.25\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  -0.25\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	    } else if (_opt[i]._k1l > 0.0) {
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.375\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  0.375\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  -0.125\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  -0.125\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	    } else {
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.125\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  0.125\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  -0.375\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  -0.375\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	    }
         } else if(strcmp(_opt[i]._keyword, "SEXTUPOLE") == 0) {
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.125\n";
            ofbox << _opt[i]._s - s_cross;
            ofbox << "  0.125\n";
            ofbox << _opt[i]._s - s_cross;
            ofbox << "  -0.125\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  -0.125\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
         } else {
            ofbox << _opt[i]._s - s_cross << "  0.0\n";
	 }
      }
   }
   ofbox.close();

   // Make the command file for XMGR.
   strcpy(plot_param, "__TMP__PLOT477.param_");
   ofstream ofcmd(plot_param);

   // Setting up the strings for the time.
   ofcmd << "version 40102\n";
   ofcmd << "default linewidth 2\n";
   /*
   ofcmd << "with string\n";
   ofcmd << "   string on\n";
   ofcmd << "   string just 0\n";
   ofcmd << "   string loctype view\n";
   ofcmd << "   string 0.1, 0.03\n";
   ofcmd << "   string char size 0.5\n";
   ofcmd << "string def \"Time: " << time_str << "  Last file modify time: "
         << file_mtime << "\"\n";
*/

   // Setting up the title and subtitle of the graph.
   ofcmd << "title " << _title << "\n";
   if (beta_star_x < 0.0 || beta_star_y < 0.0) {
      ofcmd << "subtitle \"\\8n\\0\\sx\\N = " << _vx << "  \\8n\\0\\sy\\N = "
            << _vy << "\"\n";
   } else {
      ofcmd << "subtitle \"\\8n\\0\\sx\\N = " << _vx << "  \\8n\\0\\sy\\N = "
            << _vy << "  \\8b\\0\\S*\\N = (" << beta_star_x << ", "
	    << beta_star_y << ")\"\n";
   }
   ofcmd << "move g0.s2 to g1.s0\n";
   ofcmd << "move g0.s3 to g1.s1\n";
   ofcmd << "move g0.s4 to g2.s0\n";

   // The upper (beta functions) graph.
   ofcmd << "with g0\nview ymin 0.40\n";
   ofcmd << "   autoscale\n";

   // Setting up the labels
   ofcmd << "   xaxis ticklabel off\n";
   ofcmd << "   yaxis label \"\\8b\\0\\S1/2\\N [m\\S1/2\\N]\"\n";

   // Setting up the legends.
   ofcmd << "   legend on\n";
   ofcmd << "   legend x1 0.2\n";
   ofcmd << "   legend y1 0.8\n";
   ofcmd << "   legend string 0 \"X\"\n";
   ofcmd << "   legend string 1 \"Y\"\n";

   // Setting up the linestyles.
   ofcmd << "   s0 linestyle 1\n";
   ofcmd << "   s0 linewidth 2\n";
   ofcmd << "   s0 color 1\n";
   ofcmd << "   s1 linestyle 3\n";
   ofcmd << "   s1 linewidth 2\n";
   ofcmd << "   s1 color 2\n";

   // For lower (dispersion) graph.
   ofcmd << "with g1\nview ymax 0.4\nview ymin 0.15\n";
   ofcmd << "   autoscale\n";
   ofcmd << "   xaxis label \"S [m]\"\n";
   ofcmd << "   yaxis label \"\\8h\\0 [m]\"\n";
   ofcmd << "   yaxis ticklabel op right\n";

   // Setting up the linestyles.
   ofcmd << "   s0 linestyle 1\n";
   ofcmd << "   s0 linewidth 2\n";
   ofcmd << "   s0 color 1\n";
   ofcmd << "   s1 linestyle 3\n";
   ofcmd << "   s1 linewidth 2\n";
   ofcmd << "   s1 color 2\n";

   // putting a line through zero.
   ofcmd << "   zeroxaxis on\n";
   ofcmd << "   zeroxaxis bar on\n";

   // Setting up the magnet descriptions.
   ofcmd << "with g2\nview ymin 0.2\nview ymax 0.6\n";
   ofcmd << "   autoscale\n";
   ofcmd << "   world ymin -2\nworld ymax 2\n";
   ofcmd << "   frame off\n";
   ofcmd << "   xaxis off\n";
   ofcmd << "   yaxis off\n";
   ofcmd << "   s0 linestyle 1\n";
   ofcmd << "   s0 linewidth 2\n";
   ofcmd << "   s0 color 14\n";

   // Choosing the symbols.
   ofcmd.close();

   // Run XMGR.
   strcpy(cmd_str, "xmgrace -nxy ");
   strcat(cmd_str, plot_data);
   strcat(cmd_str, " -nxy ");
   strcat(cmd_str, plot_box);
   strcat(cmd_str, " -param ");
   strcat(cmd_str, plot_param);
   strcat(cmd_str, "; \\rm ");
   strcat(cmd_str, plot_data);
   strcat(cmd_str, "; \\rm ");
   strcat(cmd_str, plot_box);
   strcat(cmd_str, "; \\rm ");
   strcat(cmd_str, plot_param);
   strcat(cmd_str, " &");
   system(cmd_str);
}


void BetaFunWithCO::plotCO() {
   int first;
   unsigned i;
   char plot_data[30], plot_box[30], plot_param[30], cmd_str[200];
   time_t *time_ptr;
   char time_str[30], file_mtime[30];
   double s_cross, beta_star_x, beta_star_y, xmin, xmax;

   // Setting up the time string.
   time_ptr = new time_t;
   time(time_ptr);
   strcpy(time_str, ctime(time_ptr));
   strcpy(file_mtime, ctime(&_file_stat.st_mtime));
   delete time_ptr;

   // Remove the carriage return from the time string.
   for(i = 0; i < strlen(time_str); i++) {
      if(time_str[i] == '\n') {
	 time_str[i] = '\0';
	 break;
      }
   }
   for(i = 0; i < strlen(file_mtime); i++) {
      if(file_mtime[i] == '\n') {
	 file_mtime[i] = '\0';
	 break;
      }
   }

   // Find beta_star and s_cross.
   first = 1;
   xmax = -1.0e+10;
   xmin =  1.0e+10;
   beta_star_x = beta_star_y = -1.0;
   s_cross = 0.0;
   for(i = 0; i < _size; i++) {
      if ((strncasecmp(_opt[i]._name, "ip", 2) == 0 ||
	   strcasecmp(_opt[i]._name, "mcr") == 0 ||
	  strncasecmp(_opt[i]._name, "clock", 5) == 0) &&
	  first == 1) {

	 s_cross = _opt[i]._s;
	 beta_star_x = _opt[i]._betx;
	 beta_star_y = _opt[i]._bety;
         first = 0;
      } else {
         xmin = ::min(xmin, _opt[i]._s);
         xmax = ::max(xmax, _opt[i]._s);
      }
   }

   // Make the files for plotting.
   strcpy(plot_data, "__TMP__PLOT477.data_");
   ofstream ofplot(plot_data);

   for(i = 0; i < _size; i++) {
      if(xmin <= _opt[i]._s && _opt[i]._s <= xmax) {
         ofplot << _opt[i]._s - s_cross;
         ofplot << "  " << sqrt(_opt[i]._betx);
         ofplot << "  " << sqrt(_opt[i]._bety);
         ofplot << "  " << _opt[i]._xco;
         ofplot << "  " << _opt[i]._yco;
         ofplot << endl;
      }
   }
   ofplot.close();

   // Make the files for plotting.
   strcpy(plot_box, "__TMP__PLOT477.box_");
   ofstream ofbox(plot_box);

   for(i = 0; i < _size; i++) {
      if(xmin <= _opt[i]._s && _opt[i]._s <= xmax) {
         if(strcmp(_opt[i]._keyword, "QUADRUPOLE") == 0) {
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
            if(_opt[i]._k1l > 0.0) {
               ofbox << _opt[i]._s - s_cross - _opt[i]._l;
               ofbox << "  0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  0.0\n";
            } else if(_opt[i]._k1l < 0.0) {
               ofbox << _opt[i]._s - s_cross - _opt[i]._l;
               ofbox << "  -0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  -0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  0.0\n";
            }
         } else if(strcmp(_opt[i]._keyword, "SBEND") == 0 ||
		   strcmp(_opt[i]._keyword, "RBEND") == 0) {
	    if (_opt[i]._k1l == 0.0) {
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.25\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  0.25\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  -0.25\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  -0.25\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	    } else if (_opt[i]._k1l > 0.0) {
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.375\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  0.375\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  -0.125\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  -0.125\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	    } else {
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.125\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  0.125\n";
	       ofbox << _opt[i]._s - s_cross;
	       ofbox << "  -0.375\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  -0.375\n";
	       ofbox << _opt[i]._s - s_cross - _opt[i]._l;
	       ofbox << "  0.0\n";
	    }
         } else if(strcmp(_opt[i]._keyword, "SEXTUPOLE") == 0) {
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.125\n";
            ofbox << _opt[i]._s - s_cross;
            ofbox << "  0.125\n";
            ofbox << _opt[i]._s - s_cross;
            ofbox << "  -0.125\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  -0.125\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
         } else {
            ofbox << _opt[i]._s - s_cross << "  0.0\n";
	 }
      }
   }
   ofbox.close();

   // Make the command file for XMGR.
   strcpy(plot_param, "__TMP__PLOT477.param_");
   ofstream ofcmd(plot_param);

   // Setting up the strings for the time.
   ofcmd << "version 40102\n";
   ofcmd << "default linewidth 2\n";

   /*
   ofcmd << "with string\n";
   ofcmd << "   string on\n";
   ofcmd << "   string just 0\n";
   ofcmd << "   string loctype view\n";
   ofcmd << "   string 0.1, 0.03\n";
   ofcmd << "   string char size 0.5\n";
   ofcmd << "string def \"Time: " << time_str << "  Last file modify time: "
         << file_mtime << "\"\n";
   */

   // Setting up the title and subtitle of the graph.
   ofcmd << "title " << _title << "\n";
   if (beta_star_x < 0.0 || beta_star_y < 0.0) {
      ofcmd << "subtitle \"\\8n\\0\\sx\\N = " << _vx << "  \\8n\\0\\sy\\N = "
            << _vy << "\"\n";
   } else {
      ofcmd << "subtitle \"\\8n\\0\\sx\\N = " << _vx << "  \\8n\\0\\sy\\N = "
            << _vy << "  \\8b\\0\\S*\\N = (" << beta_star_x << ", "
	    << beta_star_y << ")\"\n";
   }
   ofcmd << "move g0.s2 to g1.s0\n";
   ofcmd << "move g0.s3 to g1.s1\n";
   ofcmd << "move g0.s4 to g2.s0\n";

   // The upper (beta functions) graph.
   ofcmd << "with g0\nview ymin 0.40\n";
   ofcmd << "   autoscale\n";

   // Setting up the labels
   ofcmd << "   xaxis ticklabel off\n";
   ofcmd << "   yaxis label \"\\8b\\0\\S1/2\\N [m\\S1/2\\N]\"\n";

   // Setting up the legends.
   ofcmd << "   legend on\n";
   ofcmd << "   legend x1 0.2\n";
   ofcmd << "   legend y1 0.8\n";
   ofcmd << "   legend string 0 \"X\"\n";
   ofcmd << "   legend string 1 \"Y\"\n";

   // Setting up the linestyles.
   ofcmd << "   s0 linestyle 1\n";
   ofcmd << "   s0 linewidth 2\n";
   ofcmd << "   s0 color 1\n";
   ofcmd << "   s1 linestyle 3\n";
   ofcmd << "   s1 linewidth 2\n";
   ofcmd << "   s1 color 2\n";

   // For lower (dispersion) graph.
   ofcmd << "with g1\nview ymax 0.4\nview ymin 0.15\n";
   ofcmd << "   autoscale\n";
   ofcmd << "   xaxis label \"S [m]\"\n";
   ofcmd << "   yaxis label \"X(Y)\\sco\\N [mm]\"\n";
   ofcmd << "   yaxis ticklabel op right\n";

   // Setting up the linestyles.
   ofcmd << "   s0 linestyle 1\n";
   ofcmd << "   s0 linewidth 2\n";
   ofcmd << "   s0 color 1\n";
   ofcmd << "   s1 linestyle 3\n";
   ofcmd << "   s1 linewidth 2\n";
   ofcmd << "   s1 color 2\n";

   // putting a line through zero.
   ofcmd << "   zeroxaxis on\n";
   ofcmd << "   zeroxaxis bar on\n";

   // Setting up the magnet descriptions.
   ofcmd << "with g2\nview ymin 0.2\nview ymax 0.6\n";
   ofcmd << "   autoscale\n";
   ofcmd << "   world ymin -2\nworld ymax 2\n";
   ofcmd << "   frame off\n";
   ofcmd << "   xaxis off\n";
   ofcmd << "   yaxis off\n";
   ofcmd << "   s0 linestyle 1\n";
   ofcmd << "   s0 linewidth 2\n";
   ofcmd << "   s0 color 14\n";

   // Choosing the symbols.
   ofcmd.close();

   // Run XMGR.
   strcpy(cmd_str, "xmgrace -nxy ");
   strcat(cmd_str, plot_data);
   strcat(cmd_str, " -nxy ");
   strcat(cmd_str, plot_box);
   strcat(cmd_str, " -param ");
   strcat(cmd_str, plot_param);
   strcat(cmd_str, "; \\rm ");
   strcat(cmd_str, plot_data);
   strcat(cmd_str, "; \\rm ");
   strcat(cmd_str, plot_box);
   strcat(cmd_str, "; \\rm ");
   strcat(cmd_str, plot_param);
   strcat(cmd_str, " &");
   system(cmd_str);
}

void BetaFunWithCO::profile() {
   int first;
   unsigned i;
   char plot_data[30], plot_box[30], plot_param[30], cmd_str[200];
   time_t *time_ptr;
   char time_str[30], file_mtime[30];
   double s_cross, beta_star_x, beta_star_y, xmin, xmax, amplitude;
   double epsn, beta_gam, sigma_p;

   // Setting up the time string.
   time_ptr = new time_t;
   time(time_ptr);
   strcpy(time_str, ctime(time_ptr));
   strcpy(file_mtime, ctime(&_file_stat.st_mtime));
   delete time_ptr;

   // Remove the carriage return from the time string.
   for(i = 0; i < strlen(time_str); i++) {
      if(time_str[i] == '\n') {
	 time_str[i] = '\0';
	 break;
      }
   }
   for(i = 0; i < strlen(file_mtime); i++) {
      if(file_mtime[i] == '\n') {
	 file_mtime[i] = '\0';
	 break;
      }
   }

   // Find beta_star and s_cross.
   first = 1;
   xmax = -1.0e+10;
   xmin =  1.0e+10;
   beta_star_x = beta_star_y = -1.0;
   s_cross = 0.0;
   for(i = 0; i < _size; i++) {
      if ((strncasecmp(_opt[i]._name, "ip", 2) == 0 ||
	   strcasecmp(_opt[i]._name, "mcr") == 0 ||
	  strncasecmp(_opt[i]._name, "clock", 5) == 0) &&
	  first == 1) {

	 s_cross = _opt[i]._s;
	 beta_star_x = _opt[i]._betx;
	 beta_star_y = _opt[i]._bety;
         first = 0;
      } else {
         xmin = ::min(xmin, _opt[i]._s);
         xmax = ::max(xmax, _opt[i]._s);
      }
   }

   cout << "Normalized Emittance = ";
   cin >> epsn;
   cout << "Beta * Gamma = ";
   cin >> beta_gam;
   cout << "SIGMA(DEL(p) / p) = ";
   cin >> sigma_p;

   // Make the files for plotting.
   strcpy(plot_data, "__TMP__PLOT479.data_");
   ofstream ofplot(plot_data);

   amplitude = sqrt(epsn / (6.0 * beta_gam));
   for(i = 0; i < _size; i++) {
      if(xmin <= _opt[i]._s && _opt[i]._s <= xmax) {
         ofplot << _opt[i]._s - s_cross;
         ofplot << "  " << amplitude * sqrt(_opt[i]._betx) + abs(sigma_p *
			   _opt[i]._dx);
         ofplot << "  " << amplitude * sqrt(_opt[i]._bety) + abs(sigma_p *
			   _opt[i]._dy);
         ofplot << endl;
      }
   }
   ofplot.close();

   // Make the files for plotting.
   strcpy(plot_box, "__TMP__PLOT479.box_");
   ofstream ofbox(plot_box);

   for(i = 0; i < _size; i++) {
      if(xmin <= _opt[i]._s && _opt[i]._s <= xmax) {
         if(strcmp(_opt[i]._keyword, "QUADRUPOLE") == 0) {
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
            if(_opt[i]._k1l > 0.0) {
               ofbox << _opt[i]._s - s_cross - _opt[i]._l;
               ofbox << "  0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  0.0\n";
            } else if(_opt[i]._k1l < 0.0) {
               ofbox << _opt[i]._s - s_cross - _opt[i]._l;
               ofbox << "  -0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  -0.5\n";
               ofbox << _opt[i]._s - s_cross;
               ofbox << "  0.0\n";
            }
         } else if(strcmp(_opt[i]._keyword, "SBEND") == 0) {
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.25\n";
            ofbox << _opt[i]._s - s_cross;
            ofbox << "  0.25\n";
            ofbox << _opt[i]._s - s_cross;
            ofbox << "  -0.25\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  -0.25\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
         } else if(strcmp(_opt[i]._keyword, "RBEND") == 0) {
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.25\n";
            ofbox << _opt[i]._s - s_cross;
            ofbox << "  0.25\n";
            ofbox << _opt[i]._s - s_cross;
            ofbox << "  -0.25\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  -0.25\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
         } else if(strcmp(_opt[i]._keyword, "SEXTUPOLE") == 0) {
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.125\n";
            ofbox << _opt[i]._s - s_cross;
            ofbox << "  0.125\n";
            ofbox << _opt[i]._s - s_cross;
            ofbox << "  -0.125\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  -0.125\n";
            ofbox << _opt[i]._s - s_cross - _opt[i]._l;
            ofbox << "  0.0\n";
         } else {
            ofbox << _opt[i]._s - s_cross << "  0.0\n";
         }
      }
   }
   ofbox.close();

   // Make the command file for XMGR.
   strcpy(plot_param, "__TMP__PLOT479.param_");
   ofstream ofcmd(plot_param);

   // Setting up the strings for the time.
   ofcmd << "version 40102\n";
   ofcmd << "default linewidth 2\n";

   /*
   ofcmd << "with string\n";
   ofcmd << "   string on\n";
   ofcmd << "   string just 0\n";
   ofcmd << "   string loctype view\n";
   ofcmd << "   string 0.1, 0.03\n";
   ofcmd << "   string char size 0.5\n";
   ofcmd << "string def \"Time: " << time_str << "  Last file modify time: "
         << file_mtime << "\"\n";
   */

   // Setting up the strings for the Beta * Gamma.
   ofcmd << "with string\n";
   ofcmd << "   string on\n";
   ofcmd << "   string just 0\n";
   ofcmd << "   string loctype view\n";
   ofcmd << "   string 0.2, 0.8\n";
   ofcmd << "   string char size 0.8\n";
   ofcmd << "string def \"\\8bg\\0 = " << beta_gam << "\"\n";

   // Setting up the strings for the emmittance.
   ofcmd << "with string\n";
   ofcmd << "   string on\n";
   ofcmd << "   string just 0\n";
   ofcmd << "   string loctype view\n";
   ofcmd << "   string 0.2, 0.775\n";
   ofcmd << "   string char size 0.8\n";
   ofcmd << "string def \"\\8e\\0\\sN\\N = " << epsn
         << "\\8p\\0 mm-mrad\"\n";

   // Setting up the strings for the SIGMA(DEL(p) / p).
   if(sigma_p != 0.0) {
      ofcmd << "with string\n";
      ofcmd << "   string on\n";
      ofcmd << "   string just 0\n";
      ofcmd << "   string loctype view\n";
      ofcmd << "   string 0.2, 0.75\n";
      ofcmd << "   string char size 0.8\n";
      ofcmd << "string def \"\\8s\\0\\sp\\N = " << sigma_p << "\"\n";
   }

   // Setting up the title and subtitle of the graph.
   ofcmd << "title " << _title << "\n";
   if (beta_star_x < 0.0 || beta_star_y < 0.0) {
      ofcmd << "subtitle \"\\8n\\0\\sx\\N = " << _vx << "  \\8n\\0\\sy\\N = "
            << _vy << "\"\n";
   } else {
      ofcmd << "subtitle \"\\8n\\0\\sx\\N = " << _vx << "  \\8n\\0\\sy\\N = "
            << _vy << "  \\8b\\0\\S*\\N = (" << beta_star_x << ", "
	    << beta_star_y << ")\"\n";
   }
   ofcmd << "move g0.s2 to g1.s0\n";

   // The upper (beta functions) graph.
   ofcmd << "with g0\nview ymin 0.35\n";
   ofcmd << "   autoscale\n";

   // Setting up the labels
   ofcmd << "   xaxis label \"S [m]\"\n";
   ofcmd << "   xaxis ticklabel prec 0\n";
   ofcmd << "   yaxis label \"\\8s\\0 [mm]\"\n";

   // Setting up the legends.
   ofcmd << "   legend on\n";
   ofcmd << "   legend x1 0.7\n";
   ofcmd << "   legend y1 0.8\n";
   ofcmd << "   legend char size 0.8\n";
   ofcmd << "   legend string 0 \"X\"\n";
   ofcmd << "   legend string 1 \"Y\"\n";

   // Setting up the linestyles.
   ofcmd << "   s0 linestyle 1\n";
   ofcmd << "   s0 linewidth 2\n";
   ofcmd << "   s0 color 1\n";
   ofcmd << "   s1 linestyle 3\n";
   ofcmd << "   s1 linewidth 2\n";
   ofcmd << "   s1 color 2\n";

   // Setting up the magnet descriptions.
   ofcmd << "with g1\nview ymin 0.1\nview ymax 0.3\n";
   ofcmd << "   autoscale\n";
   ofcmd << "   world ymin -1\nworld ymax 1\n";
   ofcmd << "   frame off\n";
   ofcmd << "   xaxis off\n";
   ofcmd << "   yaxis off\n";
   ofcmd << "   zeroxaxis on\n";
   ofcmd << "   zeroxaxis bar on\n";
   ofcmd << "   zeroxaxis tick major on\n";
   ofcmd << "   zeroxaxis tick minor on\n";
   ofcmd << "   s0 linestyle 1\n";
   ofcmd << "   s0 linewidth 2\n";
   ofcmd << "   s0 color 14\n";

   // Choosing the symbols.
   ofcmd.close();

   // Run XMGR.
   strcpy(cmd_str, "xmgrace -nxy ");
   strcat(cmd_str, plot_data);
   strcat(cmd_str, " -nxy ");
   strcat(cmd_str, plot_box);
   strcat(cmd_str, " -param ");
   strcat(cmd_str, plot_param);
   strcat(cmd_str, "; \\rm ");
   strcat(cmd_str, plot_data);
   strcat(cmd_str, "; \\rm ");
   strcat(cmd_str, plot_box);
   strcat(cmd_str, "; \\rm ");
   strcat(cmd_str, plot_param);
   strcat(cmd_str, " &");
   system(cmd_str);
}

/* $Header: /usr/local/lambda/matrix/RCS/matrix_vector.c,v 1.1 1994/05/14 18:47:12 mackay Exp $ */

void matrix_vector(double *y, double **a, double *x, int n)
{
  int i;
  int k;

  for(i = 0; i < n; i++) {
    y[i] = 0.0;
    for(k = 0; k < n; k++ ) {
      y[i] =  y[i] + a[i][k] * x[k];
    }
  }
}

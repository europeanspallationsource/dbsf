/* $Header: /usr/local/lambda/matrix/RCS/dim2.c,v 1.2 1994/07/15 15:21:55 peggs Exp $ */

/* see the routine "dmatrix" in Numerical Recipes for further (possible) 
   enlightenment. */

/* PURIFY has been known to report that memory leaks emanate from "dim2",
   but I believe this is erroneous.  If you don't think so, please 
   tell me so that dim2 can be fixed.  (Preferably, please tell me how 
   to fix it as well  ..)  steve peggs July 94 */

#include <stdlib.h>

double **dim2(int grid, int row)
{
  int i; 
  register double **pgrid, *prow;

  prow  = (double *) calloc(grid * row , sizeof(double));
  pgrid = (double **) calloc(grid, sizeof(double *));
  for(i = 0; i < grid ; i++)
    {
      pgrid[i] = prow;
      prow += row;
    }

  return pgrid;
}
/*==========================*/
void free2(double **pa)
{
  free(*pa);
  free(pa);
}

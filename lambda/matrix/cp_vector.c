/* $Header: /usr/local/lambda/matrix/RCS/cp_vector.c,v 1.1 1994/05/14 18:47:12 mackay Exp $ */

void cp_vector(double *x, double * y, int n)
{
  int i;

  for(i = 0; i < n; i++) x[i] = y[i];
}

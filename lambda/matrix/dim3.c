/* $Header: /usr/local/lambda/matrix/RCS/dim3.c,v 1.1 1994/05/14 18:47:12 mackay Exp $ */

#include <stdlib.h>

double ***dim3(int grid, int row, int col)
{
  int i; 
  register double ***pgrid, **prow, *pdata;

  pdata = (double *) calloc(grid * row * col, sizeof(double));
  prow  = (double **) calloc(grid * row , sizeof(double *));
  pgrid = (double ***) calloc(grid, sizeof(double *));

  for(i = 0; i < grid * row; i++)
    {
      prow[i] = pdata;
      pdata += col;
    }
  for(i = 0; i < grid ; i++)
    {
      pgrid[i] = prow;
      prow += row;
    }

  return pgrid;
}
/*=======================*/
void free3(double ***pa)
{
  free(**pa);
  free(*pa);
  free(pa);
}

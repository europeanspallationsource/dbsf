/* $Header: /usr/local/lambda/matrix/RCS/matrix_matrix.c,v 1.1 1994/05/14 18:47:12 mackay Exp $ */

void matrix_matrix(double **c, double **a, double **b, int n)
{
  int i;
  int j;
  int k;

  for(i = 0; i < n; i++) {
    for(j = 0; j < n; j++ ) {
      c[i][j] = 0.0;
      for(k = 0; k < n; k++ ) {
	c[i][j] =  c[i][j] + a[i][k] * b[k][j];
      }
    }
  }
}

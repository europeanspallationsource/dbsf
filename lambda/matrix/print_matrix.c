/* $Header: /rap/lambda/matrix/RCS/print_matrix.c,v 1.2 1994/09/11 18:35:52 mackay Exp $ */

#include <stdio.h>

void print_matrix(FILE *file_ptr, double **m, int n)
{
  int i, j;

  fprintf(file_ptr,"\n");
  for(i = 0; i < n; i++){
    for(j = 0; j < n; j++){
      fprintf(file_ptr,"%12.6f \t ", m[i][j]);
    }
    fprintf(file_ptr,"\n");
  }
  fprintf(file_ptr,"\n");
}

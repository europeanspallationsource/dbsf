/* $Header: /usr/local/lambda/matrix/RCS/unit_matrix.c,v 1.1 1994/05/14 18:47:12 mackay Exp $ */

void unit_matrix(double **m, int n)
{
  int i, j;

  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      m[i][j] = 0.0;
    }
    m[i][i] = 1.0;
  }
}

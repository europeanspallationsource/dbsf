/* $Header: /usr/local/lambda/matrix/RCS/print_vector.c,v 1.1 1994/05/14 18:47:12 mackay Exp $ */

#include <stdio.h>

void print_vect(FILE *file_ptr, double *x, int n)
{
  int i;

  fprintf(file_ptr,"\n");
  for(i = 0; i < n; i++) fprintf(file_ptr,"\t \t %f \n", x[i]);
  fprintf(file_ptr,"\n");
}

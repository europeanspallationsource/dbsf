/* $Header: /usr/local/lambda/matrix/RCS/cp_matrix.c,v 1.1 1994/05/14 18:47:12 mackay Exp $ */

void cp_matrix(double **a,double **b,int n)
{
  int i,j;

  for(i = 0; i < n; i++) {
    for(j = 0; j < n; j++)
      {  
	a[i][j] = b[i][j];
      }
  }
}

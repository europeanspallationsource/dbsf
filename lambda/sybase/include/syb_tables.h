/* $Header: /usr/local/lambda/sybase/include/RCS/syb_tables.h,v 1.5 1994/05/11 14:33:21 mackay Exp $ */
/* Header files for Sybase lattice database */

typedef struct {
  char    name[21];
  char    deltax[41];
  char    deltay[41];
  char    deltaz[41];
  char    theta[41];
  char    phi[41];
  char    psi[41];
} survey_data_row; 

typedef struct 
{
  char name[21];
  char t0[81];
  char t1[81];
  char t2[81];
  char t3[81];
  char t4[81];
  char t5[81];
  char t6[81];
  char t7[81];
} optics_data_row;


/* Creation of main tables for the LAMBDA optics databases */
/* use waldo_test */
/* go */

create table geometry
(name 		char(20),
value 		char(15) 	null,
definition 	varchar(90) 	null,
comment 	varchar(130) 	null)
create unique clustered index geometry_index on geometry(name)
go

create table strength
(name 		char(20),
value 		char(15) 	null,
definition 	varchar(90) 	null,
comment 	varchar(130) 	null)
create unique clustered index strength_index on strength(name)
go

create table magnet_piece
(name 		  char(20),
type 		  varchar(20),
tilt		  varchar(10)	null,
length_defn       varchar(60) 	null,
strength_defn 	  varchar(60) 	null,
engineering_type  varchar(20)   null,
comment 	  varchar(130) 	null)
create unique clustered index magnet_piece_index on magnet_piece(name)
go

create table beam_line
(name 		char(20),
elements 	varchar(150) 	null,
comment 	varchar(130) 	null)
create unique clustered index beam_line_index on beam_line(name)
go

create table name_location
(name		char(20),
table_name	char(30)	null)
create unique clustered index name_location_index on name_location(name)
go

create table slot
(name           char(20),
pieces          varchar(150)    null,
comment         varchar(130)    null)
create unique clustered index slot_index on slot(name)
go

create table superslot
(name           char(20),
pieces          varchar(150)    null,
comment         varchar(130)    null)
create unique clustered index superslot_index on superslot(name)
go 

create table name_alias
(standard_name   char(20),
 synch_name      char(5) null,
 comment         varchar(130) null)
create unique clustered index standard_name_index on 
name_alias(standard_name)
go

create table bend
(
 name            char(20),
 length           varchar(60)    null,
 angle            varchar(60)    null,
 tilt             varchar(10)    null,
 quad_strength    varchar(20)    null,
 sxtp_strength    varchar(20)    null,
 octp_strength    varchar(20)    null,
 entrance_angle   varchar(20)    null,
 exit_angle       varchar(20)    null,
 field_integral   varchar(20)    null,
 half_gap         varchar(20)    null,
 entrance_curv    varchar(20)    null,
 exit_curv        varchar(20)    null
)
create unique clustered index bend_index on bend(name)
go

create table rfcavity
(name            char(20),
length           varchar(60)    null,
voltage          varchar(20)    null,
phase_lag        varchar(20)    null,
harmonic_number  varchar(20)    null,
rf_coupling      varchar(20)    null,
rf_power         varchar(20)    null,
shunt_imped      varchar(20)    null,
fill_time        varchar(20)    null)
create unique clustered index rfcavity_index on rfcavity(name)
go

create table multipole
(name            char(20),
K0L              varchar(20)    null,
K1L              varchar(20)    null,
K2L              varchar(20)    null,
K3L              varchar(20)    null,
K4L              varchar(20)    null,
K5L              varchar(20)    null,
K6L              varchar(20)    null,
K7L              varchar(20)    null,
K8L              varchar(20)    null,
K9L              varchar(20)    null,
T0               varchar(20)    null,
T1               varchar(20)    null,
T2               varchar(20)    null,
T3               varchar(20)    null,
T4               varchar(20)    null,
T5               varchar(20)    null,
T6               varchar(20)    null,
T7               varchar(20)    null,
T8               varchar(20)    null,
T9               varchar(20)    null)
create unique clustered index multipole_index on multipole(name)
go

create table closed_orbit_corrector
(name         char(20),
length        varchar(60)    null,
tilt          varchar(10)    null,
horz_angle    varchar(20)    null,
vert_angle    varchar(20)    null)
create unique clustered index closed_orbit_corrector_index 
 on closed_orbit_corrector(name)
go

create table collimator
(name         char(20),
length        varchar(60)    null,
xsize         varchar(20)    null,
ysize         varchar(20)    null)
create unique clustered index collimator_index on collimator(name)
go

create table drift
(name         char(20),
length        varchar(60)    null)
create unique clustered index drift_index on drift(name)
go

create table quadrupole
(name         char(20),
length        varchar(60)    null,
strength      varchar(60)    null,
tilt          varchar(10)    null)
create unique clustered index quadrupole_index on quadrupole(name)
go

create table sextupole
(name         char(20),
length        varchar(60)    null,
strength      varchar(60)    null,
tilt          varchar(10)    null)
create unique clustered index sextupole_index on sextupole(name)
go

create table octupole
(name         char(20),
length        varchar(60)    null,
strength      varchar(60)    null,
tilt          varchar(10)    null)
create unique clustered index octupole_index on octupole(name)
go

create table solenoid
(name         char(20),
length        varchar(60)    null,
strength      varchar(60)    null)
create unique clustered index solenoid_index on solenoid(name)
go

create table monitor
(name         char(20),
length        varchar(60)    null)
create unique clustered index monitor_index on monitor(name)
go

create table elseparator
(name            char(20),
length           varchar(60)    null,
efield_strength  varchar(60)    null,
tilt             varchar(10)    null)
create unique clustered index elseparator_index on elseparator(name)
go

create table aperture
(name		char(20),
shape		char(1),
xsize		varchar(30)	null,
ysize		varchar(30)	null,
centerx		varchar(30)	null,
centery		varchar(30)	null)
create unique clustered index aperture_index on aperture(name)
go

create table magnet_size
(name		char(20),
shape		char(1),
xsize		varchar(30)	null,
ysize		varchar(30)	null,
centerx		varchar(30)	null,
centery		varchar(30)	null)
create unique clustered index magnet_size_index on magnet_size(name)
go

create table survey_data
(name         char(20),
deltax        varchar(40)    null,
deltay        varchar(40)    null,
deltaz        varchar(40)    null,
theta         varchar(40)    null,
phi           varchar(40)    null,
psi           varchar(40)    null)
create unique clustered index survey_data_index
 on survey_data(name)
go

create table optics_data
(name         char(20),
t0           varchar(80)    null,
t1           varchar(80)    null,
t2           varchar(80)    null,
t3           varchar(80)    null,
t4           varchar(80)    null,
t5           varchar(80)    null,
t6           varchar(80)    null,
t7           varchar(80)    null)
create unique clustered index optics_data_index
 on optics_data(name)
go


sp_adduser lattice_reader
go

sp_adduser guest
grant select on geometry to public
grant select on strength to public
grant select on magnet_piece to public
grant select on beam_line to public
grant select on name_location to public
grant select on slot to public
grant select on name_alias to public
grant select on rfcavity to public
grant select on bend to public
grant select on closed_orbit_corrector to public
grant select on multipole to public
grant select on collimator to public
grant select on drift to public 
grant select on quadrupole to public
grant select on sextupole to public
grant select on octupole to public
grant select on solenoid to public
grant select on monitor to public
grant select on elseparator to public
grant select on aperture to public
grant select on magnet_size to public
grant select on survey_data to public
grant select on optics_data to public
go

quit

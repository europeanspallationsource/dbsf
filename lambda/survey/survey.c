/* $Header: /usr/local/lambda/survey/RCS/survey.c,v 1.13 1994/07/14 15:07:30 peggs Exp $ */

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <malloc.h>

#include "flatin.h"
#include "survey.h"
#include "survey_dict.h"
#include "Sds/sdsgen.h"
#include "survout.h"
#include "survout_sds.h"
#include "lambda_matrix.h"

extern int  read_mag_size_sybase();
int database  = FALSE;

/*----------------------------------------------------------------------*/
/*     Global  Objects                                                  */
/*----------------------------------------------------------------------*/

FILE *slog, *cmd_file;
array_of_tokens *cmd_token_ptr, *parsef();

int token_count;
int header    = TRUE;
int size_data = NONE;
int units     = METERS;


double pi, twopi;
double m_to_ft = 100.0 / (12.0 * 2.54); /* meters to international feet */
double m_to_us_ft  = 39.37 / 12.0;      /* meters to US survey feet */

int errout = 0;
int shared_mem = FALSE;

/************************************************************************/

void mark_print(survey_data *s_ptr)
{
  flatin_data *f_ptr = s_ptr->flatin_ptr;
  int found, i, k, n;
  enum mark_type mark_dict;
  
  n = 1  ;
  while (n < token_count) {
    mark_dict = mark_named_element ;
    for (k=0; k<DICT_SIZE; k++)  {
      if( strcmp(cmd_token_ptr->piece[n], mark.word[k]) == 0 )  {
	mark_dict = mark.dict[k];
	break;
      }
    }
    
    switch(mark_dict)  {
    case all:
      for(i = 0; i < f_ptr->number_of_elements; i++) 
	s_ptr->locmat_ptr[i].repout = TRUE ;
      n = token_count; /* may as well exit immediately! */
      break;
    case mark_named_element:
      found = FALSE ;
      for(i = 0; i < f_ptr->number_of_elements && !found; i++) {
	if(strcmp(cmd_token_ptr->piece[n], f_ptr->element_ptr[i].name) == 0) {
	  found = TRUE ;
	  s_ptr->locmat_ptr[i].repout = TRUE ;
	}
      }
      if (!found) {
	fprintf(slog,"*** Could not find an element named '%s' to mark\n",
		cmd_token_ptr->piece[n]);
      }
      ++n;
      break;      
    }
  }
  
}

/************************************************************************/

void move_origin(survey_data *s_ptr)
{
  int k, n;
  enum origin_type origin_dict;
  
  n = 1  ;
  while (n<token_count) {
    origin_dict = origin_error ;
    for(k = 0; k <= DICT_SIZE; k++)
      if( strcmp(cmd_token_ptr->piece[n], origin.word[k]) == 0 ) 
	origin_dict = origin.dict[k];
    
    switch(origin_dict){
    case x:
      sscanf( cmd_token_ptr->piece[n+1], "%lf", &s_ptr->coord_ptr->X );
      n += 2 ;
      break;
    case y:
      sscanf( cmd_token_ptr->piece[n+1], "%lf", &s_ptr->coord_ptr->Y );
      n += 2 ;
      break;
    case z:
      sscanf( cmd_token_ptr->piece[n+1], "%lf", &s_ptr->coord_ptr->Z );
      n += 2 ;
      break;
    case fi:
      sscanf( cmd_token_ptr->piece[n+1], "%lf", &s_ptr->coord_ptr->fi );
      n += 2 ;
      break;
    case si:
      sscanf( cmd_token_ptr->piece[n+1], "%lf", &s_ptr->coord_ptr->si );
      n += 2 ;
      break;
    case theta:
      sscanf( cmd_token_ptr->piece[n+1], "%lf", &s_ptr->coord_ptr->theta );
      n += 2 ;
      if( fabs(s_ptr->coord_ptr->theta) >= pi )  {
	fprintf(stderr,"survey: absolute value of initial theta0 must be ");
	fprintf(stderr,"<= PI - please use another angle\n");
	exit(1);
      }
      break;
    case s_off:
      sscanf( cmd_token_ptr->piece[n+1], "%lf", &s_ptr->s_off );
      n += 2 ;
      break;
    case origin_error:
      fprintf(stderr,"survey: illegal origin setting option ` %s '\n",
	      cmd_token_ptr->piece[n]);
      exit(1);
    }
  }
  
}

/************************************************************************/
array_of_tokens *parsef(char *line, int *t_count)
     
     /*This function parses a string, returning 't_count' valid tokens,
       from 0 thru' (t_count-1) :  item.piece[t_count] is NOT valid */
{
  int counter, place, length;
  static array_of_tokens item;
  
  place = 0;
  counter = 0;
  while (counter < MAX_TOKEN_COUNT) {
    place += strspn(&line[place],        " \t,&=%");
    if (sscanf(&line[place], "%s", item.piece[counter]) == EOF) break;    
    length = strcspn(item.piece[counter]," \t,&=%!");
    strncpy(&item.piece[counter][length],"\0",1);
    if (length == 0) break; /* rest of line is a comment starting with '!' */
    place   += length;
    counter += 1;
  }
  *t_count = counter;
  
  return &item;
}

/************************************************************************/

void read_mag_size_ascii(survey_data *s_ptr)
{
  flatin_data  *f_ptr = s_ptr->flatin_ptr;
  local_matrix *l_ptr = s_ptr->locmat_ptr;
  char sizeline[BUFF_MAX];
  char name[NAME_MAX];
  char shape[1];
  double xsize, ysize, centerx, centery;
  int found, i;
  FILE *size_file;
  
  size_file = fopen(s_ptr->size_name, "r");
  if (size_file == NULL) {
    fprintf(stderr,"survey: couldn't find magnet size file %s\n", 
	    s_ptr->size_name);
    exit(1);
  }  
  
  while( fgets(sizeline, BUFF_MAX, size_file) != NULL )  {
    if (sscanf(sizeline, "%s %s %lf %lf %lf %lf", 
	       name, shape, &xsize, &ysize, &centerx, &centery) != 6) {
      fprintf(stderr,"survey: something went wrong reading size info %s\n",
	      sizeline);
      fprintf(stderr,"cols: NAME  SHAPE  WIDTH  HEIGHT  CENTERX  CENTERY \n");
      exit(1);
    }
    
    found = FALSE;
    for(i = 0; i < f_ptr->number_of_elements && !found; i++)  {
      if( strcmp(name, f_ptr->element_ptr[i].name) == 0 )  {
	found = TRUE;
	l_ptr[i].sizeout = TRUE ;
	strcpy(&l_ptr[i].magnet_shape,shape);
	l_ptr[i].magnet_xsize = xsize;
	l_ptr[i].magnet_ysize = ysize;
	l_ptr[i].magnet_centerx = centerx;
	l_ptr[i].magnet_centery = centery;
	if (ysize > s_ptr->height_max) s_ptr->height_max = ysize;
	if (xsize > s_ptr->width_max)  s_ptr->width_max  = xsize;
      }
    }
    
    if (!found) fprintf(slog,"*** could not find an element '%s' %s", name,
			"to attach a size to !\n");
  }
  
  fclose(size_file);
  
}

/************************************************************************/

void write_header()
{
  
  printf("--------------------------------------------------");
  printf("---------------------");
  printf("--------------------------------------------------\n");
  printf("      element         occurence     sum(L)         x            y            z");
  printf("         theta        phi         psi\n");
  if(units == METERS)  {
    printf("      name            number         [m]          [m]          [m]          [m]");
    printf("        [rad]       [rad]       [rad]\n");   
  } else  {
    printf("      name            number         [ft]         [ft]         [ft]         [ft]");
    printf("       [rad]       [rad]       [rad]\n");   
  }
  printf("--------------------------------------------------");
  printf("---------------------");
  printf("--------------------------------------------------\n");  
}

/************************************************************************/

void write_summary(survey_data *s_ptr)
{
  coord       *c_ptr = s_ptr->coord_ptr;
  flatin_data *f_ptr = s_ptr->flatin_ptr;
  int n_atoms = f_ptr->number_of_atoms;
  char label[16];
  double conv;
  
  switch (units) {
  case METERS:
    strcpy(label,"[m] ");
    conv = 1.0;
    break;
  case INTERNATIONAL_FEET:
    strcpy(label,"[ft]");
    conv = m_to_ft;
    break;
  case US_FEET:
    strcpy(label,"[ft]");
    conv = m_to_us_ft;
    break;
  default:
    fprintf(stderr,"survey: don't recognize the units");
    exit(1);
    break;
  }
  
  printf("-------------------------------------------------------------");
  printf("------------------------------------------------------------\n");
  
  printf("Delta path length %s    = %22.13f\n", label,
	 f_ptr->atom_ptr[n_atoms-1].s*conv);
  
  printf("Delta (x,y,z) %s        = %22.13f %22.13f %22.13f\n", label, 
	 (c_ptr[n_atoms].X - c_ptr[0].X)*conv,
	 (c_ptr[n_atoms].Y - c_ptr[0].Y)*conv,
	 (c_ptr[n_atoms].Z - c_ptr[0].Z)*conv);
  
  printf("Delta (theta,fi,si) [rad] = %22.13f %22.13f %22.13f\n",
	 c_ptr[n_atoms].theta - c_ptr[0].theta,
	 c_ptr[n_atoms].fi    - c_ptr[0].fi,
	 c_ptr[n_atoms].si    - c_ptr[0].si);
  printf("Delta theta / 2*PI        = %22.13f\n",
	 (c_ptr[n_atoms].theta-c_ptr[0].theta)/twopi);
  
  printf("X_min, X_max %s         = %22.13f %22.13f\n", label,
	 s_ptr->xmin*conv, s_ptr->xmax*conv);
  printf("Y_min, Y_max %s         = %22.13f %22.13f\n", label,
	 s_ptr->ymin*conv, s_ptr->ymax*conv);
  printf("Z_min, Z_max %s         = %22.13f %22.13f\n", label,
	 s_ptr->zmin*conv, s_ptr->zmax*conv);
  
  printf("-------------------------------------------------------------");
  printf("------------------------------------------------------------\n");
  
}

/************************************************************************/

void write_stdout(survey_data *s_ptr)
{
  coord        *c_ptr = s_ptr->coord_ptr;
  flatin_data  *f_ptr = s_ptr->flatin_ptr;
  local_matrix *l_ptr = s_ptr->locmat_ptr;
  char buff[BUFF_MAX];
  double conv;
  double s_off = s_ptr->s_off;
  int i, count, mid;
  
  if (header) write_header();
  
  switch (units) {
  case METERS:
    conv = 1.0;
    break;
  case INTERNATIONAL_FEET:
    conv = m_to_ft;
    break;
  case US_FEET:
    conv = m_to_us_ft;
    break;
  default:
    fprintf(stderr,"survey: don't recognize the units\n");
    exit(1);
    break;
  }
  
  count = 0;
  for (i = 0; i <= f_ptr->number_of_atoms; i++)  {
    sprintf(buff,"%13.6f%13.6f%13.6f%13.9f%13.9f%13.9f",
	    c_ptr[i].X*conv,
	    c_ptr[i].Y*conv,
	    c_ptr[i].Z*conv,
	    c_ptr[i].theta,
	    c_ptr[i].fi,
	    c_ptr[i].si);
    
    if (i == 0) { 
      ++count;
      printf("BEGIN                         %13.6f%s\n", s_off*conv, buff);
    }
    else {
      mid = f_ptr->atom_ptr[i-1].element_index;
      if(l_ptr[mid].repout || i == f_ptr->number_of_atoms)  {
    	l_ptr[mid].nrep += 1; /* Increment the number of times this element has been reported */
	
	if (header && (++count%HEADER_PERIOD == 0)) write_header();
	printf("%-26s%-4d%13.6f%s\n", 
	       f_ptr->element_ptr[mid].name, 
	       l_ptr[mid].nrep, 
	       (s_off + f_ptr->atom_ptr[i-1].s) * conv,
	       buff);
      }
    }    
  }
  
  if (header) write_summary(s_ptr);
  
}

/************************************************************************/

void write_surv_sds(survey_data *s_ptr)
{
  coord        *c_ptr = s_ptr->coord_ptr;
  flatin_data  *f_ptr = s_ptr->flatin_ptr;
  double conv;
  double s_off = s_ptr->s_off;
  int i;
  sds_handle sds;
  sds_code tc;
  struct survey_out *so;
  struct delta *de;
  int n_atoms;
  
  switch (units) {
  case METERS:
    conv = 1.0;
    break;
  case INTERNATIONAL_FEET:
    conv = m_to_ft;
    break;
  case US_FEET:
    conv = m_to_us_ft;
    break;
  default:
    fprintf(stderr,"survey: don't recognize the units\n");
    exit(1);
    break;
  }
  n_atoms = f_ptr->number_of_atoms;
  
  sds_init();
  sds = sds_new("SurveyOut");
  tc = sds_define_structure(sds,so_tl, so_nm);
  sds_declare_structure(sds,SDS_ALLOCATE,"Survey",n_atoms + 1,tc);
  tc = sds_define_structure(sds,de_tl, de_nm);
  sds_declare_structure(sds,SDS_ALLOCATE,"Delta",1L,tc);
  
  sds_ass(sds,"Survey",SDS_PROC_MEM);
  if (sds_last_error() < 0) {
    sds_perror("survey: assembling 'Survey' failed"); 
    sds_error = 0;
  }

  so = (struct survey_out *)sds_obname2ptr(sds,"Survey");
  de = (struct delta *)sds_obname2ptr(sds,"Delta");
  
  for (i = 0; i < n_atoms + 1; i++)  
    {
      so[i].lattice_index = latindices[i];
      so[i].x =  c_ptr[i].X*conv;
      so[i].y =  c_ptr[i].Y*conv;
      so[i].z =  c_ptr[i].Z*conv;
      so[i].theta =  c_ptr[i].theta;
      so[i].phi =  c_ptr[i].fi;
      so[i].psi =  c_ptr[i].si;
      so[i].suml =  
	i>0?(s_off + f_ptr->atom_ptr[i-1].s) * conv:s_off * conv;
    }
  de->path = f_ptr->atom_ptr[n_atoms-1].s*conv;
  
  de->x = (c_ptr[n_atoms].X - c_ptr[0].X)*conv;
  de->y  = (c_ptr[n_atoms].Y - c_ptr[0].Y)*conv;
  de->z = (c_ptr[n_atoms].Z - c_ptr[0].Z)*conv;
  
  de->theta = c_ptr[n_atoms].theta - c_ptr[0].theta;
  de->fi = c_ptr[n_atoms].fi    - c_ptr[0].fi;
  de->si = c_ptr[n_atoms].si    - c_ptr[0].si;
  
  sds_ass(sds,"Survey",SDS_FILE);
  if (sds_last_error() < 0) {
    sds_perror("survey: assembling 'Survey' failed"); 
    sds_error = 0;
  }
  
}

/************************************************************************/

int main(int argc, char *argv[])
{
  int c, cmdline_counter, i, k, next;
  char cmdline[BUFF_MAX];
  enum command_type command_dict;
  
  survey_data s_data;
  flatin_data f_data;
  sds_handle flat_sds;
  
  initialize_survey_data(&s_data,&f_data);
  
  slog  = fopen("survey.log" , "w");
  pi    = 4.0 * atan(1.0);
  twopi = 2.0 * pi;
  
  /*---------------------------------------------------------------------*/
  
  while( --argc > 0  &&  (*++argv)[0] == '-') {
    while( (c = *++argv[0]) ) {
      next = FALSE;
      switch(c) {
      case 'f':
	units = INTERNATIONAL_FEET;
	fprintf(slog,"Converting to international feet (2.54 cm/inch)\n");
	break;
      case 'h':
	header = FALSE;
	break;
      case 'u':
	units = US_FEET;
	break;
      case 'A':
	--argc;
	size_data = ASCII;
	strcpy(s_data.size_name,*++argv);
	next = TRUE;
	break;
      case 's':
	shared_mem = TRUE;
	break;
      case 'S':
	--argc;
	database = TRUE;
	strcpy(s_data.database_name,*++argv);
	next = TRUE;
	break;
      case 'e':
	errout = SDS_ERROR;
	break;
      default:
	fprintf(stderr,"survey: illegal command line option %c \n", c);
	exit(1);
      }
      if (next) break;
    }
  }
/*  sds_output_errors(errout);
  sds_output_proginfo(errout);
*/  
  if (argc == 1) {
    strcpy(s_data.flat_file_name,*argv);
  }
  else {
    printf("\nUsage: survey [-efhu] [-A mag_size_file] ");
    printf("[-S database] ");
    printf("flat_file\n\n");
    printf("   'flat_file' contains lattice information in ASCII or SDS ");
    printf("'flat' format\n\n");
    printf("   Default output is in meters\n");
    printf("   -f  for output in international feet [1 inch = 2.54 cm]\n");
    printf("   -u  for output in US survey feet     [39.37 inches = 1 m]\n");
    printf("   -e  to print out SDS error messages\n");
    printf("   -h  turns off file headers\n");
    printf("   -s  read flat_SDS file from shared memory\n");
    printf("   -A  mag_size_file to read transverse magnet sizes ");
    printf("from an ASCII file\n");
    printf("   -S  sybase database to search for data (eg magnet sizes) ");
    printf("if available\n\n");
    printf("   'survey' also expects to find a 'survey.cmd' file ");
    printf("containing commands\n\n");
    exit(1);
  }
  
  /*---------------------------------------------------------------------*/
  /*       Read the flat lattice file and do initializations             */
  /*---------------------------------------------------------------------*/
  
  time(&s_data.time_stamp);
  fprintf(slog,"%s%s\n",ctime(&s_data.time_stamp),s_data.flat_file_name);
  if (units == INTERNATIONAL_FEET) {
    fprintf(slog,"Converting to international feet (2.54 cm/inch)\n");
  } else if (units == US_FEET) {
    fprintf(slog,"Converting to US survey feet (39.37 inches/meter)\n");
  }
  
  sds_init();
  flat_sds=SDS_NOT_SDS;
  /*
  if(shared_mem)
    {
      flat_sds = sds_access(s_data.flat_file_name,SDS_SHARED_MEM,SDS_READ);
      if(flat_sds<=0)
	{
	  printf("Could not locate shared memory for %s\n",
		 s_data.flat_file_name);
	  exit(0);
	}
    } else {
      flat_sds = sds_access(s_data.flat_file_name,SDS_FILE,SDS_READ);
    }
  */

  if (flat_sds == SDS_NOT_SDS) {
    flatin(s_data.flat_file_name, s_data.flatin_ptr);
    fprintf(slog,"flat file is ASCII\n");
  } else if (flat_sds >= 0) {
    flatin_sds(flat_sds, s_data.flatin_ptr);
    fprintf(slog,"flat file is SDS\n");
  } else {
    fprintf(stderr,"survey: couldn't find ASCII or SDS flat file %s\n", 
	    s_data.size_name);
    exit(1);
  }
  alloc_survey(&s_data);
  
  if (size_data == ASCII) {
    fprintf(slog,"size data from ascii file %s\n",s_data.size_name);
    read_mag_size_ascii(&s_data);
  } else if (database) {
    fprintf(slog,"size data from table %s.magnet_size\n",s_data.database_name);
    read_mag_size_sybase(&s_data);
  } else {
    fprintf(slog,"no size data\n");
  }
  
  create_local_matrices(&s_data);
  
  /* --------------------------------------------------------------------*/
  /*      Read the command file SURVEY.CMD                               */
  /*---------------------------------------------------------------------*/
  
  cmd_file = fopen("survey.cmd", "r");
  if (cmd_file == NULL) {
    fprintf(stderr,"survey: couldn't find a 'survey.cmd' file\n");
    exit(1);
  }
  
  cmdline_counter = 0;
  while( fgets(cmdline, BUFF_MAX, cmd_file) != NULL )  {
    cmd_token_ptr = parsef(cmdline, &token_count);
    
    if (token_count) {
      fprintf(slog,"survey.cmd %-3d: ", ++cmdline_counter);
      for(i = 0; i < token_count; i++) 
	fprintf(slog,"%s ", cmd_token_ptr->piece[i] );
      fprintf(slog,"\n");
      
      /*------------------------------------------------------------------*/
      /*               Interpret the command line                         */
      /*------------------------------------------------------------------*/
      
      command_dict = cmd_error ;
      for(k = 0; k < DICT_SIZE; k++)  {
	if( strcmp(cmd_token_ptr->piece[0], command.word[k]) == 0) {
	  command_dict = command.dict[k];
	  break;
	}
      }
      
      switch(command_dict)  {
      case no_keyword:
	break;
      case print:
	mark_print(&s_data);
	break; 
      case mv_origin:
	move_origin(&s_data);
	break;
      case stop:
	exit(0);
	break;
      case survey:
	initialize_matrices(&s_data);
	propagate_survey(&s_data);
	write_stdout(&s_data);
	if (flat_sds >= 0)
	  write_surv_sds(&s_data);
	break;
      case topdrawer:
	write_survey_top(&s_data);
	break;
      case cmd_error:
	fprintf(slog,"*** Unrecognized keyword '%s'\n", 
		cmd_token_ptr->piece[0]);
	break;                
      }
    }
  }
  
  /*----------------------------------------------------------------------*/
  
  fclose(cmd_file);
  fclose(slog);
  exit(0);
}

/************************************************************************/

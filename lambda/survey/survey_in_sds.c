/* $Header: /usr/local/lambda/survey/RCS/survey_in_sds.c,v 1.4 1994/06/17 22:34:46 mackay Exp $ */

#include <stdio.h>
#include <stdlib.h>
#include <Survey_data.h>

int survey_in_sds(sds_handle sds, Survey_data *sdp)
{
/* Reads in the Survey file */

  sds_handle ob_index;

  sdp->survey_ptr = (struct survey_out *) sds_obname2ptr(sds,"Survey");
  if(!sdp->survey_ptr)
    {
      fprintf(stderr,"? survey_in: Not reading an SDS file.\n");
      return 1;
    }

  ob_index = sds_name2ind(sds,"Survey");
  sdp->number_of_survey = sds_array_size(sds,ob_index);

  sdp->delta_ptr = (struct delta *) sds_obname2ptr(sds,"Delta");
  ob_index = sds_name2ind(sds,"Delta");
  sdp->number_of_delta = sds_array_size(sds,ob_index);

  return 0;
}

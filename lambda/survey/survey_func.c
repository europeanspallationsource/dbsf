/* $Header: /rap/lambda/survey/RCS/survey_func.c,v 1.8 1994/07/15 20:06:44 peggs Exp mackay $ */

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include "flatin.h"
#include "survey.h"
#include "lattice_defines.h"
#include "lambda_matrix.h"

#if !defined(NOSYBASE)
#include <sybfront.h>
#include <sybdb.h>
extern DBPROCESS* sds_db_init();
#endif

/****************************************************************************/

void alloc_survey(survey_data *s_ptr)
{
  flatin_data  *f_ptr = s_ptr->flatin_ptr;
  coord        *c_ptr = s_ptr->coord_ptr;
  local_matrix *l_ptr = s_ptr->locmat_ptr;
  int i;

  /* first, free memory, if any has been allocated ................*/

  if (c_ptr != NULL) {
    for (i = 0; i <= s_ptr->flatin_ptr->number_of_atoms; i++) 
      free2(c_ptr[i].W);
    free(c_ptr);
  }

  if (l_ptr != NULL) {
    for (i = 0; i < s_ptr->flatin_ptr->number_of_elements; i++) {
      free (l_ptr[i].R);
      free2(l_ptr[i].S);
      free2(l_ptr[i].T);
    }
    free(l_ptr);
  }

  /* ................. and then allocate memory */

  s_ptr->coord_ptr = 
    (coord *) calloc(f_ptr->number_of_atoms + 1, sizeof(coord));
  c_ptr = s_ptr->coord_ptr;
  for (i = 0; i <= s_ptr->flatin_ptr->number_of_atoms; i++) 
    c_ptr[i].W = dim2(3,3);

  s_ptr->locmat_ptr = 
    (local_matrix *) calloc(f_ptr->number_of_elements, sizeof(local_matrix));
  l_ptr = s_ptr->locmat_ptr;
  for (i = 0; i < f_ptr->number_of_elements; i++) {
    l_ptr[i].R = dim1(3);
    l_ptr[i].S = dim2(3,3);
    l_ptr[i].T = dim2(3,3);
  }

}

/************************************************************************/

void initialize_matrices(survey_data *s_ptr)
{
  coord *c_ptr = s_ptr->coord_ptr;
  double fi    = c_ptr[0].fi;
  double si    = c_ptr[0].si;
  double theta = c_ptr[0].theta;
  

  s_ptr->init_FI[0][0]    =  1.0;
  s_ptr->init_FI[1][1]    =  cos(fi);
  s_ptr->init_FI[1][2]    =  sin(fi);
  s_ptr->init_FI[2][1]    = -sin(fi);
  s_ptr->init_FI[2][2]    =  cos(fi);
  s_ptr->init_SI[0][0]    =  cos(si);
  s_ptr->init_SI[0][1]    = -sin(si);
  s_ptr->init_SI[1][0]    =  sin(si);
  s_ptr->init_SI[1][1]    =  cos(si);
  s_ptr->init_SI[2][2]    =  1.0;
  
  s_ptr->init_THETA[0][0] =  cos(theta);
  s_ptr->init_THETA[0][2] =  sin(theta);
  s_ptr->init_THETA[1][1] =  1.0;
  s_ptr->init_THETA[2][0] = -sin(theta);
  s_ptr->init_THETA[2][2] =  cos(theta);

}

/************************************************************************/

void initialize_survey_data(survey_data *s_ptr, flatin_data *flatin_ptr)
{

  strcpy(s_ptr->size_name,"\0");
  strcpy(s_ptr->database_name,"\0");

  s_ptr->s_off = 0.0;
  s_ptr->xmin = 0.0;
  s_ptr->xmax = 0.0;
  s_ptr->ymin = 0.0;
  s_ptr->ymax = 0.0;
  s_ptr->zmin = 0.0;
  s_ptr->zmax = 0.0;

  s_ptr->height_max = 0.0;
  s_ptr->width_max  = 0.0;

  s_ptr->init_FI    = dim2(3,3);
  s_ptr->init_SI    = dim2(3,3);
  s_ptr->init_THETA = dim2(3,3);

  s_ptr->coord_ptr   = NULL;
  s_ptr->flatin_ptr  = flatin_ptr;
  s_ptr->locmat_ptr  = NULL;
  
}

/************************************************************************/

void create_local_matrices(survey_data *s_ptr)
{
  flatin_data  *f_ptr = s_ptr->flatin_ptr;
  element      *e_ptr;
  local_matrix *l_ptr;
  int i;
  double angle, length, rho, tilt;
  double **Ttemp = dim2(3,3);
  double **Tinv  = dim2(3,3);
  double *Rtemp  = dim1(3);
  
  for (i = 0; i < f_ptr->number_of_elements; i++) {
    l_ptr = s_ptr->locmat_ptr + i;
    e_ptr = f_ptr->element_ptr + i;

    /*-------------------------------------------------------------------*/

    angle  = e_ptr->strength;
    length = e_ptr->length;
    l_ptr->R[2] = length;
    l_ptr->S[0][0] = 1.0;
    l_ptr->S[1][1] = 1.0;
    l_ptr->S[2][2] = 1.0;

    switch(e_ptr->type_index) {
    case ATTR_RBEND:
      /* convert to arc length from chord length */
      if (angle != 0.0) {
	length = ((0.5 * angle) / sin(0.5 * angle)) * length;
	rho = length / angle;
	l_ptr->R[0] = rho * (cos(angle) - 1.0) ;
	l_ptr->R[2] = rho * sin(angle) ; 
	l_ptr->S[0][0] =  cos(angle);
	l_ptr->S[0][2] = -sin(angle);
	l_ptr->S[2][0] =  sin(angle);
	l_ptr->S[2][2] =  cos(angle);
      }
      break;
    case ATTR_SBEND:
      if (angle != 0.0) {
	rho = length / angle;
	l_ptr->R[0] = rho * (cos(angle) - 1.0) ;
	l_ptr->R[2] = rho * sin(angle) ; 
	l_ptr->S[0][0] =  cos(angle);
	l_ptr->S[0][2] = -sin(angle);
	l_ptr->S[2][0] =  sin(angle);
	l_ptr->S[2][2] =  cos(angle);
      }
      break;
    case ATTR_SROT:
      l_ptr->S[0][0] =  cos(angle);
      l_ptr->S[0][1] = -sin(angle);
      l_ptr->S[1][0] =  sin(angle);
      l_ptr->S[1][1] =  cos(angle);
      break;
    case ATTR_YROT:
      l_ptr->S[0][0] =  cos(angle);
      l_ptr->S[0][2] = -sin(angle);
      l_ptr->S[2][0] =  sin(angle);
      l_ptr->S[2][2] =  cos(angle);
      break;
    case ATTR_TCELEMENT:
      tc_survey_data(s_ptr->database_name, e_ptr->name, l_ptr);
      break;
    default:
      break;
    }

    /*------------------------------------------------------------------
    Tilt */

    tilt = e_ptr->tilt;
    l_ptr->T[0][0] =  cos(tilt);
    l_ptr->T[0][1] = -sin(tilt);
    l_ptr->T[1][0] = -l_ptr->T[0][1];
    l_ptr->T[1][1] =  l_ptr->T[0][0];
    l_ptr->T[2][2] =  1.0;

    if (tilt != 0.0) {
      Tinv[0][0] =  l_ptr->T[0][0];
      Tinv[0][1] = -l_ptr->T[0][1];
      Tinv[1][0] = -l_ptr->T[1][0];
      Tinv[1][1] =  l_ptr->T[1][1];
      Tinv[2][2] =  1.0;
      
      matrix_vector(Rtemp, l_ptr->T, l_ptr->R, 3);
      l_ptr->R[0] = Rtemp[0];
      l_ptr->R[1] = Rtemp[1];
      l_ptr->R[2] = Rtemp[2];
      
      matrix_matrix(Ttemp, l_ptr->S, Tinv, 3);
      matrix_matrix(l_ptr->S, l_ptr->T, Ttemp, 3);
    }

    /*--------------------------------------------------------*/
    
  }

  free2(Ttemp);
  free2(Tinv);
  free(Rtemp);

}

/************************************************************************/

void propagate_survey(survey_data *s_ptr)
{
  coord        *c_ptr;
  flatin_data  *f_ptr = s_ptr->flatin_ptr;
  local_matrix *l_ptr;
  int index, lid, tid;
  double theta;
  double theta_offset = 0.0;
  double *dV     = dim1(3);
  double **Wtemp = dim2(3,3);
  
  /* Survey location zero, c_ptr[0], is at the BEGINNING of atom zero.  So,
     location one, c_ptr[1], is at the end of atom 0, and
     location i,   c_ptr[i], is at the end of atom i-1
     Therefore, magnet index lid is fencepost offset by -1 */
  
    /*---------------------------------------------------------------------*/
  
  c_ptr = s_ptr->coord_ptr;
  matrix_matrix(Wtemp, s_ptr->init_FI, s_ptr->init_SI, 3);
  matrix_matrix(c_ptr->W, s_ptr->init_THETA, Wtemp, 3);

  for (index = 1; index <= f_ptr->number_of_atoms; index++) {
    c_ptr++;
    lid = f_ptr->atom_ptr[index-1].element_index;
    tid = f_ptr->element_ptr[lid].type_index;

    if (f_ptr->element_ptr[lid].length == 0.0 
	&& tid != ATTR_SROT 
	&& tid != ATTR_YROT
	&& tid != ATTR_TCELEMENT) {

      c_ptr[0] = (c_ptr - 1)[0];

    } else {

      l_ptr = s_ptr->locmat_ptr + lid;
      matrix_vector(dV, (c_ptr-1)->W, l_ptr->R, 3);
      c_ptr->X = (c_ptr-1)->X + dV[0];
      c_ptr->Y = (c_ptr-1)->Y + dV[1];
      c_ptr->Z = (c_ptr-1)->Z + dV[2];
      if(c_ptr->X > s_ptr->xmax) s_ptr->xmax = c_ptr->X;
      if(c_ptr->X < s_ptr->xmin) s_ptr->xmin = c_ptr->X;
      if(c_ptr->Y > s_ptr->ymax) s_ptr->ymax = c_ptr->Y;
      if(c_ptr->Y < s_ptr->ymin) s_ptr->ymin = c_ptr->Y;
      if(c_ptr->Z > s_ptr->zmax) s_ptr->zmax = c_ptr->Z;
      if(c_ptr->Z < s_ptr->zmin) s_ptr->zmin = c_ptr->Z;

      if (tid == ATTR_RBEND || tid == ATTR_SBEND || 
	  tid == ATTR_SROT  || tid == ATTR_YROT  ||
	  tid == ATTR_TCELEMENT) {
	matrix_matrix(Wtemp, (c_ptr-1)->W, l_ptr->S, 3);
	cp_matrix(c_ptr->W, Wtemp, 3);
	
	if (fabs(c_ptr->fi = asin(c_ptr->W[1][2])) > 1.0)
	  fprintf(stderr,"survey: pitch fi is unusually large!\n");
	
	if (fabs(c_ptr->si = atan2(c_ptr->W[1][0], c_ptr->W[1][1])) > 1.0)
	  fprintf(stderr,"survey: roll si is unusually large!\n");
	
	theta = atan2(c_ptr->W[0][2], c_ptr->W[2][2]) + theta_offset;
	if (fabs(theta - (c_ptr-1)->theta) > pi) {
	  if (theta > (c_ptr-1)->theta) {
	    theta_offset -= twopi;
	    theta -= twopi;
	  } else {
	    theta_offset += twopi;
	    theta += twopi;
	  }
	}
	c_ptr->theta = theta;

      } else {
	cp_matrix(c_ptr->W,  (c_ptr-1)->W,  3);
	c_ptr->fi    = (c_ptr-1)->fi;
	c_ptr->si    = (c_ptr-1)->si;
	c_ptr->theta = (c_ptr-1)->theta;
      }
    }
  }

  free(dV);
  free2(Wtemp);
  
}

/************************************************************************/
#if defined(NOSYBASE)

int tc_survey_data(char database_name[], char element_name[], local_matrix *l_ptr)
{
  fprintf(stderr,"\n!survey: this executable compiled without SYBASE\n");
  fprintf(stderr,"TCELEMENT '%s' becomes a drift!\n\n", element_name);
  return(-1);
}

#else

int tc_survey_data(char database_name[], char element_name[], local_matrix *l_ptr)
{
  sds_handle sds;
  DBPROCESS *dbp;
  sds_code object;
  char sql[256];
  double theta, fi, si;
  double **Temp  = dim2(3,3);
  double **Theta = dim2(3,3);
  double **Fi    = dim2(3,3);
  double **Si    = dim2(3,3);
  int struct_size, number_of_rows, object_size;

  struct survey_data_row {
    char    name[20];
    char    deltax[40];
    char    deltay[40];
    char    deltaz[40];
    char    theta[40];
    char    phi[40];
    char    psi[40];
  } *row_ptr;
  
  if (!strcmp(database_name,"\0")) {
    fprintf(stderr,"survey: no database! - TCELEMENT '%s' becomes a drift\n",
	    element_name);
    return(-1);
  } 
  if ((sds = good_sds("SurveyData")) >= 0) sds_destroy(sds);
  sds_clear_errors();
  sds = sds_new("SurveyData");

  sprintf(sql,"select * from survey_data where name = '%s'", element_name);
  dbp = sds_db_init("lattice_reader", "not_beans_again", getenv("DSQUERY"));
  sds_load_db_table(sds, dbp, database_name, sql);  
  dbexit();
  if (sds_last_error() < 0) {
    sds_perror("survey: sql query of survey_data failed"); return(-1); }

  sds_ass(sds,"SurveyData",SDS_FILE);
  if (sds_last_error() < 0) {
    sds_perror("survey: assembling 'SurveyData' failed");
    sds_error = 0;
  }

  row_ptr = sds_obname2ptr(sds,"survey_data");
  object  = sds_name2ind(sds,"survey_data");
  if (sds_last_error() < 0) {
    sds_perror("survey: object 'survey_data' failed"); 
    return(-1); 
  }

  number_of_rows = sds_array_size(sds,object);
  if (number_of_rows == 0) {
    fprintf(stderr,"survey: no row in '%s'.survey_data for TCELEMENT '%s'\n", 
	    database_name, element_name);
    return(-1);
  }

  object_size = sds_sizeof_object(sds,object)/number_of_rows;
  struct_size = sizeof(struct survey_data_row);
  if (struct_size != object_size) {
    fprintf(stderr,"survey: object size %d ", object_size);
    fprintf(stderr,"!= expected row size %d in survey_data table\n",
	    struct_size);
    return(-1);
  }

  sscanf(row_ptr->deltax,"%lf",&l_ptr->R[0]);
  sscanf(row_ptr->deltay,"%lf",&l_ptr->R[1]);
  sscanf(row_ptr->deltaz,"%lf",&l_ptr->R[2]);
  sscanf(row_ptr->theta, "%lf",&theta);
  sscanf(row_ptr->phi,   "%lf",&fi);
  sscanf(row_ptr->psi,   "%lf",&si);

  Theta[0][0] = cos(theta);
  Theta[0][2] = sin(theta);
  Theta[1][1] = 1.0;
  Theta[2][0] = -Theta[0][2];
  Theta[2][2] = Theta[0][0];

  Fi[0][0] = 1.0;
  Fi[1][1] = cos(fi);
  Fi[1][2] = sin(fi);
  Fi[2][1] = -Fi[1][2];
  Fi[2][2] = Fi[1][1];

  Si[0][0] = cos(si);
  Si[0][1] = -sin(si);
  Si[1][0] = -Si[0][1];
  Si[1][1] = Si[0][0];
  Si[2][2] = 1.0;

  matrix_matrix(Temp, Fi, Si, 3);
  matrix_matrix(l_ptr->S, Theta, Temp, 3);

  free2(Temp);
  free2(Theta);
  free2(Fi);
  free2(Si);

  return(0);
}

#endif
/************************************************************************/
#if defined(NOSYBASE)

int read_mag_size_sybase(survey_data *s_ptr)
{
  fprintf(stderr,"\n!survey: this executable compiled without SYBASE - ");
  fprintf(stderr,"cannot read magnet sizes!\n\n");
  return(-1);
}

#else

int read_mag_size_sybase(survey_data *s_ptr)
{
  flatin_data  *f_ptr = s_ptr->flatin_ptr;
  local_matrix *l_ptr = s_ptr->locmat_ptr;
  int found, object_size, number_of_rows, struct_size, i, j;
  sds_handle sds;
  DBPROCESS *dbp;
  sds_code object;
  char sql[256];

  struct mag_size_row {
    char    name[20];
    char    shape;
    char    xsize[30];
    char    ysize[30];
    char    centerx[30];
    char    centery[30];
  } *row_ptr;

  if ((sds = good_sds("MagnetSize")) >= 0) sds_destroy(sds);
  sds_clear_errors();
  sds = sds_new("MagnetSize");

  strcpy(sql,"select * from magnet_size");
  dbp = sds_db_init("lattice_reader", "not_beans_again", getenv("DSQUERY"));
  sds_load_db_table(sds, dbp, s_ptr->database_name, sql);
  dbexit();
  if (sds_last_error() < 0) {
    sds_perror("survey: sql query of magnet_size failed"); return(-1); }

  sds_ass(sds,"MagnetSize",SDS_FILE);
  if (sds_last_error() < 0) {
    sds_perror("survey: assembling 'MagnetSize' failed"); 
    sds_error = 0;
  }

  row_ptr = sds_obname2ptr(sds,"magnet_size");
  object  = sds_name2ind(sds,"magnet_size");
  if (sds_last_error() < 0) {
    sds_perror("survey: object 'magnet_size' failed"); 
    return(-1);
  }

  number_of_rows = sds_array_size(sds,object);
  if (number_of_rows == 0) {
/*    fprintf(stderr,"survey: no rows in magnet_size table\n"); 
*/
    return(-1); 
  }

  object_size = sds_sizeof_object(sds,object)/number_of_rows;
  struct_size = sizeof(struct mag_size_row);
  if (struct_size != object_size) {
    fprintf(stderr,"survey: object size %d != expected ", object_size);
    fprintf(stderr,"row size %d in magnet_size table\n", struct_size);
    return(-1);
  }

  fprintf(slog,"Table has %d row(s) of length %d\n", 
	  number_of_rows, object_size);
  fprintf(slog,"NAME      SHAPE  XSIZE   YSIZE   CENTERX CENTERY\n");
  for (i=0; i<number_of_rows; i++, row_ptr++) {
    strcpy(row_ptr->name + strcspn(row_ptr->name," "), "\0");

    found = FALSE;
    for(j = 0; j < f_ptr->number_of_elements && !found; j++)  {
      if( strcmp(row_ptr->name, f_ptr->element_ptr[j].name) == 0 )  {
	found = TRUE;
	l_ptr[j].sizeout = TRUE ;

	strcpy(&l_ptr[j].magnet_shape,&row_ptr->shape);
	sscanf(row_ptr->xsize,"%lf",&l_ptr[j].magnet_xsize);
	sscanf(row_ptr->ysize,"%lf",&l_ptr[j].magnet_ysize);
	sscanf(row_ptr->centerx,"%lf",&l_ptr[j].magnet_centerx);
	sscanf(row_ptr->centery,"%lf",&l_ptr[j].magnet_centery);
	fprintf(slog,"%-11s %.1s %9.4f %7.4f %7.4f %7.4f\n",
		f_ptr->element_ptr[j].name, &l_ptr[j].magnet_shape,
		l_ptr[j].magnet_xsize,   l_ptr[j].magnet_ysize,
		l_ptr[j].magnet_centerx, l_ptr[j].magnet_centery);

	if (l_ptr[j].magnet_xsize > s_ptr->width_max)  
	       s_ptr->width_max  = l_ptr[j].magnet_xsize;
	if (l_ptr[j].magnet_ysize > s_ptr->height_max) 
	       s_ptr->height_max = l_ptr[j].magnet_ysize;
      }
    }
    if (!found) fprintf(slog,"*** could not find an element '%s' %s", 
			row_ptr->name, "to attach a size to !\n");
  }
  
  return(0);
}
#endif
/************************************************************************/

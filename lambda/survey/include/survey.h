#ifndef SURVEY_H
#define SURVEY_H

/* $Header: /usr/local/lambda/survey/include/RCS/survey.h,v 1.4 1994/05/31 19:36:14 peggs Exp $ */

#define FALSE              0
#define TRUE               1
#define ARC_SEGMENTS       4
#define BUFF_MAX           128
#define HEADER_PERIOD      50
#define MAX_TOKEN_SIZE     132
#define MAX_TOKEN_COUNT    100
#define METERS             0
#define INTERNATIONAL_FEET 1
#define US_FEET            2
#define NONE               0
#define ASCII              1
#define NAME_MAX           64

typedef struct  { 
  double *R;
  double **S;
  double **T;
  double aperture_xsize;
  double aperture_ysize;
  double aperture_centerx;
  double aperture_centery;
  double magnet_xsize;
  double magnet_ysize;
  double magnet_centerx;
  double magnet_centery;
  char aperture_shape;
  char magnet_shape;
  int repout;
  int sizeout;
  int nrep;
} local_matrix;

typedef struct  {
  double X;
  double Y;
  double Z;
  double fi;
  double si;
  double theta;
  double **W;
} coord;

typedef struct {
  time_t time_stamp;
  char flat_file_name[NAME_MAX];
  char size_name[NAME_MAX];
  char database_name[NAME_MAX];
  double s_off;
  double xmin;
  double xmax;
  double ymin;
  double ymax;
  double zmin;
  double zmax;
  double height_max;
  double width_max;
  double **init_FI;
  double **init_SI;
  double **init_THETA;
  coord        *coord_ptr;
  flatin_data  *flatin_ptr;
  local_matrix *locmat_ptr;
} survey_data;


/*----------------------------------------------------------------------*/
/*     external functions and objects                                   */
/*----------------------------------------------------------------------*/

extern int    *latindices;
extern double pi, twopi;
extern legal  legal_type[];
extern FILE   *slog;

extern char* getenv();
extern void  sds_load_db_table();

extern int  tc_survey_data();
extern void initialize_survey_data();
extern void alloc_survey();
extern void create_local_matrices();
extern void initialize_matrices();
extern void propagate_survey();
extern void write_survey_top();

#endif /* SURVEY_H */

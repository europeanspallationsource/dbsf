#ifndef SURVEY_DICT_H
#define SURVEY_DICT_H

/* $Header: /usr2/local/lambda/survey/include/RCS/survey_dict.h,v 1.1 1993/07/01 19:48:33 peggs Exp $ */

#define DICT_SIZE       64
#define MAX_TOKEN_SIZE  132
#define MAX_TOKEN_COUNT 100

typedef char token[MAX_TOKEN_SIZE];
typedef struct {
  token piece[MAX_TOKEN_COUNT];
} array_of_tokens;

enum command_type {no_keyword, print, mv_origin,stop,survey,topdrawer,cmd_error};
typedef struct {
  enum command_type dict[DICT_SIZE];
  token             word[DICT_SIZE];
} command_struct;
command_struct command = {
  {no_keyword, print, mv_origin, stop, survey, topdrawer, cmd_error },
  {"", "print", "origin", "stop", "survey", "topdrawer" } 
};

enum origin_type { x, y, z, fi, si, theta, s_off, origin_error };
typedef struct {
  enum origin_type dict[DICT_SIZE];
  token            word[DICT_SIZE];
} origin_struct;
origin_struct origin = {
  {x, y, z, fi, si, theta, s_off, origin_error},
  {"x", "y", "z", "fi", "si", "theta", "s"}
};

enum mark_type {all, mark_named_element};
typedef struct {
  enum mark_type dict[DICT_SIZE];
  token          word[DICT_SIZE];
} mark_struct;
mark_struct mark = {
  {all, all, mark_named_element},
  {"all", "#s/e"}
};

#endif /* SURVEY_DICT_H */

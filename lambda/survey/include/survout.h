#if !defined(survout_h)
#define survout_h 1

struct survey_out 
{
	int lattice_index;
	double suml;
	double x;
	double y;
	double z;
	double theta;
	double phi;
	double psi;
};

struct delta
{
	double path;
	double x;
	double y;
	double z;
	double theta;
	double fi;
	double si;
};

#endif

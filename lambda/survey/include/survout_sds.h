#if !defined(survout_sds_h)
#define survout_sds_h 1

struct type_list so_tl[] = 
{
  {(long)1, SDS_INT},
  {(long)1, SDS_DOUBLE},
  {(long)1, SDS_DOUBLE},
  {(long)1, SDS_DOUBLE},
  {(long)1, SDS_DOUBLE},
  {(long)1, SDS_DOUBLE},
  {(long)1, SDS_DOUBLE},
  {(long)1, SDS_DOUBLE},
  {(long)0, SDS_RETLIST},
  {(long)0, SDS_ENDLIST}
};

char so_nm[] = "lattice_index,suml,x,y,z,theta,phi,psi";

struct type_list de_tl[] = 
{
  {(long)1, SDS_DOUBLE},
  {(long)1, SDS_DOUBLE},
  {(long)1, SDS_DOUBLE},
  {(long)1, SDS_DOUBLE},
  {(long)1, SDS_DOUBLE},
  {(long)1, SDS_DOUBLE},
  {(long)1, SDS_DOUBLE},
  {(long)0, SDS_RETLIST},
  {(long)0, SDS_ENDLIST}
};

char de_nm[] = "path,x,y,z,theta,fi,si";

#endif

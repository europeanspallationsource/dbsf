!print bpm
!print mcr
print all
!print obwl, oekn, xki1, oskn

!origin x 31694.295102 y 0.0 z 30209.627602 theta  3.1066878488
!For origin value derivation, see RHIC/AP/3 and ~peggs/locate6

!origin x -590.581639 y 0.0 z 0.0 theta 3.141592653589792
!origin x -590.581639 y 0.0 z 0.0
!origin x -.072280 theta .00461271
origin x 0.0 y 0.0 z 0.0 theta 0.0
survey 
topdrawer
stop

-----
notes
-----
1.	anything written after 'stop' is unused
2.	anything after ! is a comment
3.	breaks between tokens in command are the following symbols { ' ' \t , & = % }
4.	blanks lines are ignored
5.	dictionaries of keywords are defined in $LAMBDA/include/survey.h

------------------------------------------
commands syntax - examples of legal syntax
------------------------------------------
print location_1 location_2
print all
print #s/e

origin y 1.2
origin y = 1.2
origin, y, 1.2
origin x 1.2 y 3.4 z 5.6 theta 7.8 fi 9.0 si 0.1 s 2.3

stop

survey

topdrawer

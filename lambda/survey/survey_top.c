/* $Header: /usr/local/lambda/survey/RCS/survey_top.c,v 1.6 1994/07/15 19:48:49 peggs Exp $ */

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include "flatin.h"
#include "survey.h"
#include "survey_top.h"
#include "lattice_defines.h"
#include "lambda_matrix.h"

/****************************************************************************/

void plot_point(top *t_ptr, double *P, double *Ptran)
{

  /* take a LOCALLY defined point P (relative to the upstream end of
     an atom at t_ptr->c_ptr), transform it to GLOBAL coordinates, Ptran,
     and write its 3-D projection to 3 topdrawer files */

  if (t_ptr->e_ptr->tilt == 0) {
    matrix_vector(Ptran, t_ptr->c_ptr->W, P, 3);
  } else {
    matrix_vector(t_ptr->temp, t_ptr->l_ptr->T, P,           3);
    matrix_vector(Ptran,       t_ptr->c_ptr->W, t_ptr->temp, 3);
  }
  Ptran[0] += t_ptr->c_ptr->X;
  Ptran[1] += t_ptr->c_ptr->Y;
  Ptran[2] += t_ptr->c_ptr->Z;

  fprintf(t_ptr->three_d,"%.6f %.6f %.6f\n", Ptran[0], Ptran[1], Ptran[2]);
  fprintf(t_ptr->topx,"%.6f %.6f\n", Ptran[2], Ptran[1]);
  fprintf(t_ptr->topy,"%.6f %.6f\n", Ptran[2], Ptran[0]);
  fprintf(t_ptr->topz,"%.6f %.6f\n", Ptran[0], Ptran[1]);

}

/****************************************************************************/

void write_survey_top(survey_data *s_ptr)
{
  coord       *c_ptr;
  element     *e_ptr;
  flatin_data *f_ptr = s_ptr->flatin_ptr;
  top  t;
  char t_stamp[BUFF_MAX];

  int eid, i, j, count, line_count;
  double eps = 0.1;
  double angle, arc_length, bend_angle, cc, rho, ss;
  double half_height, half_width, half_sagitta;
  double *nlu = dim1(3);  /* Near Left Up, et cetera */
  double *nld = dim1(3);
  double *nru = dim1(3);
  double *nrd = dim1(3);
  double *flu = dim1(3);  /* Far Left Up, et cetera */
  double *fld = dim1(3);
  double *fru = dim1(3);
  double *frd = dim1(3);

  double *P     = dim1(3);
  double *dP    = dim1(3);
  double *Ptran = dim1(3);

  FILE *three_d = fopen("three_d", "w");
  FILE *topx    = fopen("side.top", "w");
  FILE *topy    = fopen("plan.top", "w");
  FILE *topz    = fopen("end.top", "w");

  t.three_d = three_d;
  t.topx  = topx;
  t.topy  = topy;
  t.topz  = topz;
  t.temp  = dim1(3);
  t.c_ptr = NULL;
  t.e_ptr = NULL;
  t.l_ptr = NULL;
  
  /*----------------------------------------------------------------------*/

  count = strcspn(ctime(&s_ptr->time_stamp),"\n");
  strncpy(t_stamp,ctime(&s_ptr->time_stamp),count);
  t_stamp[count] = 0;

  /*
  fprintf(topx,"SET FONT DUPLEX \n");
  fprintf(topx,"SET SIZE 13.2 BY 10.2\n");
  fprintf(topx,"SET WINDOW Y FROM 1.0 TO 8.5 \n");
  fprintf(topx,"SET WINDOW X FROM 1.0 TO 12.3 \n");
  fprintf(topx,"TITLE  3.5 9.3 SIZE 3 'Side view of beamline %s'\n",
	  s_ptr->flat_file_name);
  fprintf(topx,"TITLE 0.1 5.0 ANGLE 90 'Y (m) ' \n");
  fprintf(topx,"TITLE BOTTOM 'Z (m) ' \n");
  fprintf(topx,"TITLE 1.0 0.2 SIZE 1.5 '%s'\n", t_stamp);
  fprintf(topx,"SET LIMITS X FROM %f TO %f \n", 
	  floor(s_ptr->zmin - s_ptr->width_max - eps), 
	  ceil (s_ptr->zmax + s_ptr->width_max + eps));
  fprintf(topx,"SET LIMITS Y FROM %f TO %f \n", 
	  floor(s_ptr->ymin - s_ptr->height_max - eps), 
	  ceil (s_ptr->ymax + s_ptr->height_max + eps));

  fprintf(topy,"SET FONT DUPLEX \n");
  fprintf(topy,"SET SIZE 13.2 BY 10.2\n");
  fprintf(topy,"SET WINDOW Y FROM 1.0 TO 8.5 \n");
  fprintf(topy,"SET WINDOW X FROM 1.0 TO 12.3 \n");
  fprintf(topy,"TITLE  3.5 9.3 SIZE 3 'Plan view of beamline %s'\n",
	  s_ptr->flat_file_name);
  fprintf(topy,"TITLE 0.1 5.0 ANGLE 90 'X (m) ' \n");
  fprintf(topy,"TITLE BOTTOM 'Z (m) ' \n");
  fprintf(topy,"TITLE 1.0 0.2 SIZE 1.5 '%s'\n", t_stamp);
  fprintf(topy,"SET LIMITS X FROM %f TO %f \n", 
	  floor(s_ptr->zmin - s_ptr->width_max - eps), 
	  ceil (s_ptr->zmax + s_ptr->width_max + eps));
  fprintf(topy,"SET LIMITS Y FROM %f TO %f \n", 
	  floor(s_ptr->xmin - s_ptr->width_max - eps),
	  ceil (s_ptr->xmax + s_ptr->width_max + eps));

  fprintf(topz,"SET FONT DUPLEX \n");
  fprintf(topz,"SET SIZE 13.2 BY 10.2\n");
  fprintf(topz,"SET WINDOW Y FROM 1.0 TO 8.5 \n");
  fprintf(topz,"SET WINDOW X FROM 1.0 TO 12.3 \n");
  fprintf(topz,"TITLE  3.5 9.3 SIZE 3 'End view of beamline %s'\n",
	  s_ptr->flat_file_name);
  fprintf(topz,"TITLE 0.1 5.0 ANGLE 90 'Y (m) ' \n");
  fprintf(topz,"TITLE BOTTOM 'X (m) ' \n");
  fprintf(topz,"TITLE 1.0 0.2 SIZE 1.5 '%s'\n", t_stamp);
  fprintf(topz,"SET LIMITS X FROM %f TO %f \n", 
	  floor(s_ptr->xmin - s_ptr->width_max - eps),
	  ceil (s_ptr->xmax + s_ptr->width_max + eps));
  fprintf(topz,"SET LIMITS Y FROM %f TO %f \n", 
	  floor(s_ptr->ymin - s_ptr->height_max - eps), 
	  ceil (s_ptr->ymax + s_ptr->height_max + eps));
  */

  /*----------------------------------------------------------------------*/

  fprintf(three_d,"%.6f %.6f %.6f\n", 
	  s_ptr->coord_ptr->X, s_ptr->coord_ptr->Y, s_ptr->coord_ptr->Z);
  fprintf(topx,"%.6f %.6f\n", s_ptr->coord_ptr->Z, s_ptr->coord_ptr->Y);
  fprintf(topy,"%.6f %.6f\n", s_ptr->coord_ptr->Z, s_ptr->coord_ptr->X);
  fprintf(topz,"%.6f %.6f\n", s_ptr->coord_ptr->X, s_ptr->coord_ptr->Y);
  line_count = 1;

  for(i = 1; i <= f_ptr->number_of_atoms; i++)  {
    eid = f_ptr->atom_ptr[i-1].element_index;
    c_ptr = s_ptr->coord_ptr   + i;
    e_ptr = f_ptr->element_ptr + eid;

    if (e_ptr->length != 0.0) {
      if (e_ptr->type_index == ATTR_RBEND || e_ptr->type_index == ATTR_SBEND) {
	bend_angle = e_ptr->strength;
	if (e_ptr->type_index == ATTR_RBEND ) {
	  arc_length = (0.5*bend_angle/sin(0.5 * bend_angle)) * e_ptr->length;
	} else {
	  arc_length = e_ptr->length;
	}
	rho = arc_length / bend_angle;

	t.c_ptr = c_ptr - 1;
	t.e_ptr = e_ptr;
	t.l_ptr = s_ptr->locmat_ptr + eid;
	//	fprintf(three_d,"JOIN 1\n");
	//	fprintf(topx,"JOIN 1\n"); 
	//	fprintf(topy,"JOIN 1\n"); 
	//	fprintf(topz,"JOIN 1\n");
	fprintf(three_d,"%.6f %.6f %.6f\n", t.c_ptr->X,t.c_ptr->Y,t.c_ptr->Z);
	fprintf(topx,"%.6f %.6f\n", t.c_ptr->Z, t.c_ptr->Y);
	fprintf(topy,"%.6f %.6f\n", t.c_ptr->Z, t.c_ptr->X);
	fprintf(topz,"%.6f %.6f\n", t.c_ptr->X, t.c_ptr->Y);
	for (j = 1; j <= ARC_SEGMENTS; j++) {
	  angle = ((double)j / ARC_SEGMENTS) * bend_angle;
	  P[0] = rho * (cos(angle) - 1.0);
	  P[1] = 0.0;
	  P[2] = rho * sin(angle); 
	  plot_point(&t,P,Ptran);
	}
	//	fprintf(three_d,"JOIN\n");	
	//	fprintf(topx,"JOIN\n");	
	//	fprintf(topy,"JOIN\n");	
	//	fprintf(topz,"JOIN\n");
	if (i != f_ptr->number_of_atoms) { /* avoid bug in topdrawer ! */
	  fprintf(three_d,"%.6f %.6f %.6f\n", c_ptr->X, c_ptr->Y, c_ptr->Z);
	  fprintf(topx,"%.6f %.6f\n", c_ptr->Z, c_ptr->Y);
	  fprintf(topy,"%.6f %.6f\n", c_ptr->Z, c_ptr->X);
	  fprintf(topz,"%.6f %.6f\n", c_ptr->X, c_ptr->Y);
	  line_count = 1;
	}

      } else {
	fprintf(three_d,"%.6f %.6f %.6f\n", c_ptr->X, c_ptr->Y, c_ptr->Z);
	fprintf(topx,"%.6f %.6f\n", c_ptr->Z, c_ptr->Y);
	fprintf(topy,"%.6f %.6f\n", c_ptr->Z, c_ptr->X);
	fprintf(topz,"%.6f %.6f\n", c_ptr->X, c_ptr->Y);
	if (++line_count > TOP_BUFF_MAX) {
	  //	  fprintf(three_d,"JOIN 1\n");
	  //	  fprintf(topx,"JOIN 1\n");
	  //	  fprintf(topy,"JOIN 1\n");
	  //	  fprintf(topz,"JOIN 1\n");
	  fprintf(three_d,"%.6f %.6f %.6f\n", c_ptr->X, c_ptr->Y, c_ptr->Z);
	  fprintf(topx,"%.6f %.6f\n", c_ptr->Z, c_ptr->Y);
	  fprintf(topy,"%.6f %.6f\n", c_ptr->Z, c_ptr->X);
	  fprintf(topz,"%.6f %.6f\n", c_ptr->X, c_ptr->Y);
	  line_count = 0;
	}
      }
    }
  }
  //  fprintf(three_d,"JOIN 1\n");
  //  fprintf(topx,"JOIN 1\n");
  //  fprintf(topy,"JOIN 1\n");
  //  fprintf(topz,"JOIN 1\n");

    /*-------------------------------------------------------------------*/

  for(i = 1; i <= f_ptr->number_of_atoms; i++)  {
    eid = f_ptr->atom_ptr[i-1].element_index;
    t.l_ptr = s_ptr->locmat_ptr  + eid;
    
    if(t.l_ptr->sizeout)  {
      t.c_ptr = s_ptr->coord_ptr + i -1;
      t.e_ptr = f_ptr->element_ptr + eid;

      /* ..............................................................*/
      /* calculate near point coordinates */

      half_width  = 0.5 * t.l_ptr->magnet_xsize;
      half_height = 0.5 * t.l_ptr->magnet_ysize;
      switch(t.e_ptr->type_index) {
      case ATTR_RBEND:
	bend_angle = t.e_ptr->strength;
	cc = cos(0.5 * bend_angle);
	ss = sin(0.5 * bend_angle);
	rho = 0.5 * t.e_ptr->length / ss;
	half_sagitta = 0.5 * rho * (1.0 - cc);
	nlu[0] =  (half_width + half_sagitta) * cc;
	nlu[1] =  half_height;
	nlu[2] =  (half_width + half_sagitta) * ss;
	nld[0] =  (half_width + half_sagitta) * cc;
	nld[1] = -half_height;
	nld[2] =  (half_width + half_sagitta) * ss;
	nru[0] = -(half_width - half_sagitta) * cc;
	nru[1] =  half_height;
	nru[2] = -(half_width - half_sagitta) * ss;
	nrd[0] = -(half_width - half_sagitta) * cc;
	nrd[1] = -half_height;
	nrd[2] = -(half_width - half_sagitta) * ss;
	break;
      default:
	nlu[0] =  half_width;
	nlu[1] =  half_height;
	nlu[2] =  0.0;
	nld[0] =  half_width;
	nld[1] = -half_height;
	nld[2] =  0.0;
	nru[0] = -half_width;
	nru[1] =  half_height;
	nru[2] =  0.0;
	nrd[0] = -half_width;
	nrd[1] = -half_height;
	nrd[2] =  0.0;
	break;
      }

      /* ..............................................................*/
      /* calculate far point coordinates */

      switch(t.e_ptr->type_index) {
      case ATTR_RBEND:
	dP[0] = rho * (cos(bend_angle) - 1.0);
	dP[1] = 0.0; 
	dP[2] = rho * sin(bend_angle);
	for (j = 0; j < 3; j++) {
	  flu[j] = nlu[j] + dP[j];
	  fld[j] = nld[j] + dP[j];
	  fru[j] = nru[j] + dP[j];
	  frd[j] = nrd[j] + dP[j];
	}
	break;
      case ATTR_SBEND:
	/* SBEND far point coords calculated on the fly in the next switch */
	break;
      default:
	dP[0] = 0.0; dP[1] = 0.0; dP[2] = t.e_ptr->length;
	for (j = 0; j < 3; j++) {
	  flu[j] = nlu[j] + dP[j];
	  fld[j] = nld[j] + dP[j];
	  fru[j] = nru[j] + dP[j];
	  frd[j] = nrd[j] + dP[j];
	}
	break;
      }

      /* ..............................................................*/
      /* draw box lines (arcs only if SBEND */

      switch(t.e_ptr->type_index) {
      case ATTR_SBEND:
	bend_angle = t.e_ptr->strength;
	rho = t.e_ptr->length / bend_angle;

	for (j = 0; j <= ARC_SEGMENTS; j++) {
	  angle = ((double)j / ARC_SEGMENTS) * bend_angle;
	  P[0] = nlu[0] + (rho + half_width) * (cos(angle) - 1.0);
	  P[1] = nlu[1];
	  P[2] = nlu[2] + (rho + half_width) * sin(angle); 
	  plot_point(&t,P,Ptran); }
	//	fprintf(three_d,"JOIN\n");	
	//	fprintf(topx,"JOIN\n");	
	//	fprintf(topy,"JOIN\n");	
	//	fprintf(topz,"JOIN\n");
	for (j = 0; j < 3; j++) flu[j] = P[j];

	for (j = 0; j <= ARC_SEGMENTS; j++) {
	  angle = ((double)j / ARC_SEGMENTS) * bend_angle;
	  P[0] = nld[0] + (rho + half_width) * (cos(angle) - 1.0);
	  P[1] = nld[1];
	  P[2] = nld[2] + (rho + half_width) * sin(angle); 
	  plot_point(&t,P,Ptran); }
	//	fprintf(three_d,"JOIN\n");	
	//	fprintf(topx,"JOIN\n");	
	//	fprintf(topy,"JOIN\n");	
	//	fprintf(topz,"JOIN\n");
	for (j = 0; j < 3; j++) fld[j] = P[j];

	for (j = 0; j <= ARC_SEGMENTS; j++) {
	  angle = ((double)j / ARC_SEGMENTS) * bend_angle;
	  P[0] = nru[0] + (rho - half_width) * (cos(angle) - 1.0);
	  P[1] = nru[1];
	  P[2] = nru[2] + (rho - half_width) * sin(angle); 
	  plot_point(&t,P,Ptran); }
	//	fprintf(three_d,"JOIN\n");	
	//	fprintf(topx,"JOIN\n");	
	//	fprintf(topy,"JOIN\n");	
	//	fprintf(topz,"JOIN\n");
	for (j = 0; j < 3; j++) fru[j] = P[j];

	for (j = 0; j <= ARC_SEGMENTS; j++) {
	  angle = ((double)j / ARC_SEGMENTS) * bend_angle;
	  P[0] = nrd[0] + (rho - half_width) * (cos(angle) - 1.0);
	  P[1] = nrd[1];
	  P[2] = nrd[2] + (rho - half_width) * sin(angle); 
	  plot_point(&t,P,Ptran); }
	//	fprintf(three_d,"JOIN\n");	
	//	fprintf(topx,"JOIN\n");	
	//	fprintf(topy,"JOIN\n");	
	//	fprintf(topz,"JOIN\n");
	for (j = 0; j < 3; j++) frd[j] = P[j];

	plot_point(&t,nlu,Ptran);
	plot_point(&t,nru,Ptran);
	plot_point(&t,nrd,Ptran);
	plot_point(&t,nld,Ptran);
	plot_point(&t,nlu,Ptran);
	//	fprintf(three_d,"JOIN 1\n");	
	//	fprintf(topx,"JOIN 1\n"); 
	//	fprintf(topy,"JOIN 1\n"); 
	//	fprintf(topz,"JOIN 1\n");

	plot_point(&t,flu,Ptran);
	plot_point(&t,fru,Ptran);
	plot_point(&t,frd,Ptran);
	plot_point(&t,fld,Ptran);
	plot_point(&t,flu,Ptran);
	//	fprintf(three_d,"JOIN 1\n");	
	//	fprintf(topx,"JOIN 1\n");
	//	fprintf(topy,"JOIN 1\n");
	//	fprintf(topz,"JOIN 1\n");

	break;
      default:
	plot_point(&t,nlu,Ptran);
	plot_point(&t,flu,Ptran);
	plot_point(&t,fru,Ptran);
	plot_point(&t,frd,Ptran);
	plot_point(&t,fld,Ptran);
	plot_point(&t,nld,Ptran);
	plot_point(&t,nrd,Ptran);
	plot_point(&t,nru,Ptran);
	plot_point(&t,nlu,Ptran);
	plot_point(&t,nld,Ptran);
	plot_point(&t,nrd,Ptran);
	plot_point(&t,frd,Ptran);
	plot_point(&t,fld,Ptran);
	plot_point(&t,flu,Ptran);
	plot_point(&t,fru,Ptran);
	plot_point(&t,nru,Ptran);
	//	fprintf(three_d,"JOIN 1\n");	
	//	fprintf(topx,"JOIN 1\n");
	//	fprintf(topy,"JOIN 1\n");
	//	fprintf(topz,"JOIN 1\n");
	break;
      }
    }
    
  }

  /*----------------------------------------------------------------------*/

  fclose(three_d);
  fclose(topx);
  fclose(topy);
  fclose(topz);

  free(nlu);
  free(nld);
  free(nru);
  free(nrd);
  free(flu);
  free(fld);
  free(fru);
  free(frd);

  free(P);
  free(dP);
  free(Ptran);
  
  free(t.temp);

}

/************************************************************************/

/* $Header: /ride/ISTK/new/sds/RCS/sds_read.c,v 1.1 1996/10/07 15:05:08 satogata Exp $*/

#include "Sds/sdsgen.h"

#include <unistd.h>
#include <stdlib.h>

#if !defined(VXWORKS)
#if !defined(__MSDOS__) 
//#include <malloc.h>
#include <memory.h>
#else
#include <alloc.h>
#include <mem.h>
#endif
#endif

#if defined(__SUN4)
extern void bcopy(char *, char *,int);
#define memmove(a,b,n) bcopy(b,a,n)
#endif

/*********************************************************************
 *                                                                   *
 *  Make a new read buffer for the given fdc control structure.      *
 *                                                                   *
 *********************************************************************/
void
new_buffer(struct fd_control *fdc, int size)
{
  char *tembuff = 0;
  if (fdc->buffer && fdc->valid) /* There are valid data in the old buffer */
    tembuff = fdc->buffer;
  fdc->bsize = (size>sds_maxbufsize())?size:sds_maxbufsize();
  fdc->buffer = fdc->user = malloc(fdc->bsize);
  if (tembuff) /* Copy the valid stuff from the old buffer */
  {
    memcpy(fdc->buffer,(tembuff + fdc->end - fdc->valid),fdc->valid);
    free(tembuff);
  }
  else         /* All is empty */
    fdc->valid = 0;
  fdc->end = 0;
}

/*********************************************************************
 *                                                                   *
 *                                                                   *
 *                                                                   *
 *                                                                   *
 *********************************************************************/
int
sds_close_fd(int fd)
{
  struct fd_control *fdc;
  if (!fd)
    return 1;
  close(fd);
  if ((fdc = sds_fdc(fd)))
    sds_empty_fdc(fdc);
  return 1;
}


/*********************************************************************
 *                                                                   *
 *                                                                   *
 *                                                                   *
 *                                                                   *
 *********************************************************************/
void
sds_dump_fdc(int fd, char *com, int arg)
{
#if defined(READDBUG)
  struct fd_control *fdc = sds_fdc(fd);

  printf("%d:%s %d\n%d end, %d valid\n",
  fd,
  com,
  arg,
  fdc->end,
  fdc->valid );
#endif
}


/*********************************************************************
 *                                                                   *
 *                                                                   *
 *                                                                   *
 *                                                                   *
 *********************************************************************/
int 
sds_read(int fd, int requested, char *buffer)
{
  struct fd_control *fdc;
  int maxb = sds_maxbufsize();
  int returned = 0,try_for;
  int internal,copyto;
  char *readb;

  if (!(fdc = sds_fdc(fd)))
    new_buffer((fdc = sds_new_fdc(fd)),maxb);

  /* use the target buffer unless it is smaller than internal */
  internal =  (buffer && requested >= maxb)?0:1;
  /* If there is a buffer passed in, but I'm reading to internal, I
     must copy out at the end of this business */
  copyto = (buffer && internal)?1:0;

  if (!fdc->valid)
    fdc->end = 0;

  if (fdc->valid >= requested) /* Easy, no read to do */
  {
    returned = requested;
    fdc->user = fdc->buffer + fdc->end - fdc->valid;
    if (copyto)
      memcpy(buffer, fdc->user, returned);
    fdc->valid -= returned;
    return returned;
  }

  readb = internal?fdc->buffer:buffer;
  fdc->user = internal?fdc->buffer:buffer;
  if (fdc->valid)
  {
    returned += fdc->valid;
    readb += fdc->valid;
    if (!internal)
    {
      memmove(buffer, fdc->buffer + fdc->end - fdc->valid, fdc->valid);
      fdc->valid = fdc->end = 0;
    }
    else
    {
      if (requested > maxb - fdc->end + fdc->valid)
      {
        memmove(fdc->buffer, fdc->buffer + fdc->end - fdc->valid, fdc->valid);
        fdc->end = fdc->valid;
      }
      fdc->user = readb - fdc->valid;
    }
  }

  try_for = requested - returned;
  if (sds_fd_buffer() && try_for && internal)
    try_for = maxb - fdc->end;

  if (try_for)          /* will be zero if all data was not already read */
  {
    int newread;
    while ((try_for) && ((newread = read(fd,readb,try_for)) > 0))
    {
      returned += newread;
      try_for -= newread;
      if (returned >= requested)
        try_for = 0;
      readb += newread;
      if (internal)
      {
        fdc->valid += newread;
        fdc->end += newread;
      }
    }
    if (newread < 0)
    {
      if ( errno != EPIPE && errno != ECONNRESET )
      {
        fprintf( stderr, "problem reading buffer, errno = %d\n",errno );
      }
    }
  }

  returned = requested <= returned?requested:returned;

  if (copyto && returned)    /* return address given -  copy data */
    memcpy(buffer, fdc->buffer + fdc->end - fdc->valid, returned);
  if (fdc->valid)
    fdc->valid -= returned;
  return returned;
}

/***********************************************************************/
int
sds_write(int fd,char *ptr,int nbytes)
/***********************************************************************/
{
  int nleft, nwritten;

  nleft = nbytes;
  while (nleft != 0) 
  {
    nwritten = write(fd,ptr,nleft);
#ifdef VXWORKS
    if (nwritten < 0) 
      return nwritten;    /* error */
#else
    if (nwritten <= 0) 
      return nwritten;    /* error */
#endif
    nleft -= nwritten;
    ptr   += nwritten;
  }
  return(nbytes);
}

/* $Header: /ride/ISTK/new/sds/RCS/strtrunc.c,v 1.1 1996/10/07 15:05:14 satogata Exp $ */
/* strtrunc.c : remove trailing white space from a string.
**   White space is defined as (according to K&R 2nd ed. and ANSI)
**	1) ' '  a space
**	2) '\t' a horizontal tab
**	3) '\v' a vertical tab
**	4) '\f' a form feed
**	5) '\n' a newline or
**	6) '\r' a carriage return
**
** The function truncates the passed char* and also returns the length of
** the resulting string.
*/

#include <ctype.h>
#include <string.h>

/* =============================== */
size_t strtrunc(char *s)
{
  int ln;

  ln = strlen(s);
  if(ln<1) return 0;

  while(ln-->-1)
    {
      if(!isspace(s[ln]))break;	  
      s[ln] = '\0';
    }

  ln++;
  return (size_t)ln;
}
